1:[VALID CHECK REQUIRED] first in/inout in a loop must have a first-in connection

2:[BUG] bfg can not cope with models using different timestep units e.g. minutes and hours

3:[BUG] makefile generator does not create an appropriate makefile for Oasis4
