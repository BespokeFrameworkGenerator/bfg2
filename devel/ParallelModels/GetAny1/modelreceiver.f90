module modelreceiver
use mpi
use bfg2
private
public receiverinit,receiver
integer :: rank,nprocs,ierr

contains

subroutine receiverinit()
  integer :: COMM_LOCAL
  COMM_LOCAL = get_local_communicator()
  call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
  call MPI_Comm_size (COMM_LOCAL, nprocs, ierr)
end subroutine receiverinit

subroutine receiver()
  integer :: data(3)
  integer :: data2(6)
  integer :: nthings
  integer :: tag
  character*20 model
  logical :: endofts
  write(*,'("(",I1,"/",I1,") ",A)') rank+1,nprocs,"I am a receiver thread"
  do while (getprobe(tag))
    write(*,'("(",I1,"/",I1,") ",A,I)') rank+1,nprocs,"getprobe tag=",tag
    select case ( tag )
      case (1) ! I am data from model sender
        call get(data,tag)
        write(*,'("(",I1,"/",I1,") ",A,3I)') rank+1,nprocs,"received data",data
      case (2) ! I am data2 from model sender
        call get(data2,tag)
        write(*,'("(",I1,"/",I1,") ",A,6I)') rank+1,nprocs,"received data2",data2
      case (3) ! I am data3 from model sender
        call get(data,tag)
        write(*,'("(",I1,"/",I1,") ",A,3I)') rank+1,nprocs,"received data3",data
    end select
  end do
end subroutine receiver

end module modelreceiver
