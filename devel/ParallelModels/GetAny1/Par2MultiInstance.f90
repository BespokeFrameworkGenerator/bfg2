program par2multiinstance
  use mpi
  use bfg2
  use modelsender
  use modelreceiver
  IMPLICIT NONE
  integer :: COMM_LOCAL
  integer :: i,ierr,rank,nprocs
  integer colour,key
  integer :: nsenderthreads,nreceiverthreads
  namelist /threads/ nsenderthreads,nreceiverthreads

  call MPI_INIT(ierr)
  call MPI_Comm_rank (MPI_COMM_WORLD, rank, ierr);
  call MPI_Comm_size (MPI_COMM_WORLD, nprocs, ierr);

  open(unit=10,file='Threads.nam')
  read(10,threads)
  close(10)

  if (rank.eq.0) then
    print *,"There are ",nsenderthreads," sender threads"
    print *,"There are ",nreceiverthreads," receiver threads"
  end if

  if (.not.(nprocs.gt.1)) then
    write(*,*) "Two model concurrent example only runs with at least two processes"
    call mpi_finalize(ierr)
    stop
  end if

  if (.not.(nprocs.eq.5).or..not.(nsenderthreads.eq.3)) then
    write(*,*) "Sorry, I am hardcoded to use 5 procs, with 3 senders - yuk!"
    call mpi_finalize(ierr)
    stop
  end if

  if (rank.lt.(nsenderthreads)) then
    colour=0
    key=rank
  else
    colour=1
    key=rank
  end if
  call mpi_comm_split(MPI_COMM_WORLD,colour,key,COMM_LOCAL,ierr)
!  print *,"mpi_comm_split: COMM_LOCAL=",COMM_LOCAL
  call set_comm_local(COMM_LOCAL)

  if (rank.lt.nsenderthreads) call senderinit()
  if (rank.ge.nsenderthreads) call receiverinit()

  do i=1,2

    if (rank.lt.(nsenderthreads)) then
      call sender()
      call put(3,999) ! indicate this is eots for sender
    else
      call receiver()
    end if

    call mpi_barrier(MPI_COMM_WORLD,ierr)
    if (rank.eq.0) print *,"******** End of Timestep ",i,"********"
    call mpi_barrier(MPI_COMM_WORLD,ierr)
  end do

  if (rank.eq.0) print *,"End of simulation"
  call mpi_finalize(ierr)

end program par2MultiInstance
