module bfg2
use mpi
implicit none
private
public put,get,getprobe,get_local_communicator,set_comm_local,endofts
integer :: COMM_LOCAL
integer :: i,ierr

interface put
  module procedure iput1d,iputscalar
end interface

interface get
  module procedure iget1d,igetscalar
end interface

integer :: thistarget=1 ! (number of threads in target)

contains
  subroutine init()
  end subroutine init

  integer function gettarget()
  thistarget=mod(thistarget+1,2)
  gettarget=thistarget+3
  end function gettarget

  logical function endofts()
  endofts=.false.
  end function endofts

  subroutine iputscalar(data,tag)
    integer data,tag,ierr,rank
    integer :: size,target
    size=1
    ! if rank 0 I am a parallel model sending a scalar to a sequential
    !  model (scalar) so just send from master
    target=gettarget()
    call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
    if (tag.ne.999) then
    ! print *,"Infrastructure (",rank,"/?) target is ",target
    call mpi_send(data,size,MPI_INTEGER,target,tag,MPI_COMM_WORLD,ierr)
    else ! bcast termination to each thread
      if (rank.eq.0) then
        ! optimisation only a send from one thread is required
        ! print *,"Infrastructure (",rank,"/?) targets are 3,4"
        call mpi_send(data,size,MPI_INTEGER,3,tag,MPI_COMM_WORLD,ierr)
        call mpi_send(data,size,MPI_INTEGER,4,tag,MPI_COMM_WORLD,ierr)
      end if
    end if
  end subroutine iputscalar

  subroutine iput1d(data,tag)
    integer data(:),tag,ierr,rank
    integer :: size,target
    size=2
    target=gettarget()
    call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
    ! print *,"Infrastructure (",rank,"/3) target is ",target
    call mpi_send(data,size,MPI_INTEGER,target,tag,MPI_COMM_WORLD,ierr)
  end subroutine iput1d

  subroutine igetscalar(data,tag)
  integer :: data
  integer :: tag
  integer status(MPI_STATUS_SIZE),ierr
  ! nexpected is 1 here as it is a scalar and we are sequential
  ! should really have a mapping of bfg tags to mpi tags. This will
  ! be needed when we have multiple models
  call mpi_recv(data,1,MPI_INTEGER,MPI_ANY_SOURCE,tag,MPI_COMM_WORLD,status,ierr)
  end subroutine igetscalar

  subroutine iget1d(data,tag)
    ! maxsize is maxsize of integer data we may get
    integer MAXSIZE
    PARAMETER (MAXSIZE=2)
    integer :: size
    integer :: data(:),datain(MAXSIZE),tag
    integer status(MPI_STATUS_SIZE)
    ! note in general datasize may be different for different pe's
    if (tag.eq.2) then
      size=2
    else
      size=1
    end if
    ! nexpected is **hardwired** to 3 here
    do i=1,3
      call mpi_recv(datain,size,MPI_INTEGER,MPI_ANY_SOURCE,tag,MPI_COMM_WORLD,status,ierr)
      ! rank assumed to start at 0 here - needs a model offset in general
      ! need to pass sending model id through getany too so
      ! appropriate action can be taken (as id's may be the same for
      ! different sending models). We can work this out from model id's
      if (tag.eq.2) then
        data((status(MPI_SOURCE)*2+1))=datain(1)
        data((status(MPI_SOURCE)*2+2))=datain(2)
      else
        data(status(MPI_SOURCE)+1)=datain(1)
      end if
    end do
  end subroutine iget1d

  logical function getprobe(tag)
    integer :: tag
    integer :: status(MPI_STATUS_SIZE)
    integer :: source
    integer :: dummy
    integer :: rank,ierr

    call mpi_probe(MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,status,ierr)
    tag=status(MPI_TAG)
    source=status(MPI_SOURCE)
    call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
    getprobe=.true.
    if (tag.eq.999) then ! end of timestep message
      getprobe=.false.
      ! optimisation: only receive one message (from the master thread)
      ! print *,"Infrastructure (",rank,"/3) received eots, consuming my eots message "
      call mpi_recv(dummy,1,MPI_INTEGER,MPI_ANY_SOURCE,999,MPI_COMM_WORLD,status,ierr)
    end if
  end function getprobe

  subroutine set_comm_local(COMM_LOCAL_IN)
  integer :: COMM_LOCAL_IN
  COMM_LOCAL=COMM_LOCAL_IN
  end subroutine set_comm_local

  function get_local_communicator()
  integer :: get_local_communicator
  get_local_communicator=COMM_LOCAL
  end function get_local_communicator

end module bfg2

