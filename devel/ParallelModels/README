Parallel model test examples:

1: A parallel model passing data to a sequential model

In MPI it appears that a local communicator must be passed into a
library, the library can not create a local communicator. BFG2
therefore needs to create an appropriate communicator (this can be
simply done using mpi_comm_split) and a model needs to be able to get
access to it. There are two ways I can think of doing this.

a: have a "use BFG" statement required in a model. This provides
COMM_LOCAL to a model. Note, we could change the name based on
metadata. An example of this is given in LocalCommEG1/

b: support an accessor function (get_local_communicator) which returns
the communicator. An example of this is given in LocalCommEG2/ with
use BFG and in LocalCommEG3/ without "use BFG". I prefer an accessor
function as it means that we don't necessarily need "use BFG" and we
don't need extra metadata to decide what name to call the local
communicator. ** Note, the LocalCommEG3 example is not currently
working for some reason. **

2: A getprobe implementation for a parallel model passing data to a
sequential model, where the order is unknown but each thing can be
processed independently (so we want to get the next available). Has
support for data from different models (although only uses one
sending model)
