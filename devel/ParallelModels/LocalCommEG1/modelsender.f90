module modelsender
use mpi
use bfg2
private
public senderinit,sender
integer :: rank,nprocs,ierr
contains
subroutine senderinit()
  call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
  call MPI_Comm_size (COMM_LOCAL, nprocs, ierr)
end subroutine senderinit

subroutine sender
  integer :: data
  data=rank
  write(*,'("(",I1,"/",I1,") ",A)') rank+1,nprocs,"I am a sender thread"
  call put(data,1)
end subroutine sender

end module modelsender
