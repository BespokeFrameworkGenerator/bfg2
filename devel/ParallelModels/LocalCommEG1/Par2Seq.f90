program par2seq
  use mpi
  use bfg2
  use modelsender
  use modelreceiver

  integer :: i,ierr,rank,nprocs
  integer colour,key

  call MPI_INIT(ierr)
  call MPI_Comm_rank (MPI_COMM_WORLD, rank, ierr);
  call MPI_Comm_size (MPI_COMM_WORLD, nprocs, ierr);

  if (rank.lt.(nprocs-1)) then
    colour=0
    key=rank
  else
    colour=1
    key=rank
  end if
  call mpi_comm_split(MPI_COMM_WORLD,colour,key,COMM_LOCAL,ierr)

  if (.not.(nprocs.gt.1)) then
    write(*,*) "Two model concurrent example only runs with at least two processes"
    call mpi_abort()
  end if

  if (.not.(nprocs.eq.4)) then
    write(*,*) "I am hardcoded to use 4 procs - nice!"
    call mpi_abort()
  end if

  if (rank.lt.(nprocs-1)) call senderinit()
  if (rank.eq.(nprocs-1)) call receiverinit()

  do i=1,2

    if (rank.lt.(nprocs-1)) then
      call sender()
    else
      call receiver()
    end if

    call mpi_barrier(MPI_COMM_WORLD,ierr)
  end do

  call mpi_finalize(ierr)

end program par2seq
