module modelreceiver
use mpi
use bfg2
private
public receiverinit,receiver
integer :: rank,nprocs,ierr
contains
subroutine receiverinit()
  call MPI_Comm_rank (COMM_LOCAL, rank, ierr)
  call MPI_Comm_size (COMM_LOCAL, nprocs, ierr)
end subroutine receiverinit

subroutine receiver()
  integer :: data(3)
  write(*,'("(",I1,"/",I1,") ",A)') rank+1,nprocs,"I am a receiver thread"
  call get(data,1)
  write(*,'("(",I1,"/",I1,") ",A,3I)') rank+1,nprocs,"received",data
end subroutine receiver

end module modelreceiver
