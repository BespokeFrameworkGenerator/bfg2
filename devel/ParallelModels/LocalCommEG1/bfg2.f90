module bfg2
use mpi
private
public COMM_LOCAL,put,get
integer :: COMM_LOCAL
contains
  subroutine init()
  end subroutine init

  subroutine put(data,tag)
    integer data,tag
    call mpi_send(data,1,MPI_INTEGER,3,1,MPI_COMM_WORLD,ierr)
  end subroutine put

  subroutine get(data,tag)
    ! datasize **hardwired** to nprocs-1 here
    integer :: data(3),datain,tag
    integer status(MPI_STATUS_SIZE)
    ! nexpected is **hardwired** to 3 here
    do i=1,3
      call mpi_recv(datain,1,MPI_INTEGER,MPI_ANY_SOURCE,1,MPI_COMM_WORLD,status,ierr)
      ! rank assumed to start at 0 here - needs a model offset in general
      data(status(MPI_SOURCE)+1)=datain
    end do
  end subroutine get

end module bfg2
