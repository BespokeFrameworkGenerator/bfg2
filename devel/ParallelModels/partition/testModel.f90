module testModel

contains

  subroutine work(a)
    real :: a(:)
    a=1.0
  end subroutine work

  subroutine partition(dataSize,nthreads,myid,lo,hi)
    integer, intent(in)  :: dataSize, nthreads, myid
    integer, intent(out) :: lo, hi
    real :: size
    size=real(dataSize)/real(nthreads)
    lo= int(size*myid)+1
    hi= int(size*(myid+1))
  end subroutine

end module testModel

