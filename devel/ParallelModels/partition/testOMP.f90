! OMP partition with no communication
program testProgram

use testModel

implicit none

integer, parameter :: globalDataSize=100
real :: a(globalDataSize)
integer :: lo,hi
integer :: numThreads, myid, omp_get_num_threads, omp_get_thread_num

!$omp parallel default(none) private(numThreads,myid,lo,hi) shared(a)

numThreads=omp_get_num_threads()
myid=omp_get_thread_num()

print *, "Thread ", myid, " of ", numThreads, " is alive"

call partition(globalDataSize,numThreads,myid,lo,hi)

print *, "Thread ", myid, " has partition ",lo,hi

call work(a(lo:hi))

print *,"Thread ",myid," has the following values for a  ",a(lo:hi)

!$omp end parallel

end

