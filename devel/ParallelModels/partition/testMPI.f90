! MPI partition with no communication
program testProgram

use testModel
use mpi

implicit none

real,allocatable :: a(:)
integer :: lo,hi
integer :: globalDataSize=100
integer :: ierr, myid, numnodes

call mpi_init(ierr)
call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
call MPI_COMM_SIZE( MPI_COMM_WORLD, numnodes, ierr )
print *, "Process ", myid, " of ", numnodes, " is alive"

call partition(globalDataSize,numnodes,myid,lo,hi)

print *, "Process ", myid, " has partition ",lo,hi
allocate(a(hi-lo+1))

call work(a)
print *,"Process ",myid," has the following values for a  ",a
call mpi_finalize(ierr)

end program
