module modelModule
use BFG, only : put
contains
  subroutine model()
    integer :: ia=0,ib(10),ic(15,20)
    real    :: ra=0.0,rb(10),rc(15,20)
    logical :: la=.true.
    call put(ia,1)
    call put(ib,2)
    call put(ic,3)
    call put(ra,4)
    call put(rb,5)
    call put(rc,6)
    call put(la,7)
  end subroutine model
end module modelModule

