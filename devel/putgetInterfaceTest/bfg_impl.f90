subroutine put_integer(data,tag)
  integer,intent(in) :: data
  integer,intent(in) :: tag
  print *,"put_integer called with data ",data," and tag ",tag
end subroutine put_integer

subroutine put_integer_array_1d(data,tag)
  integer,intent(in) :: data(:)
  integer,intent(in) :: tag
  print *,"put_integer_array_1d called with data size ",size(data,1)," and tag ",tag
end subroutine put_integer_array_1d

subroutine put_integer_array_2d(data,tag)
  integer,intent(in) :: data(:,:)
  integer,intent(in) :: tag
  print *,"put_integer_array_2d called with data size (",size(data,1),",",size(data,2),") and tag ",tag
end subroutine put_integer_array_2d

subroutine put_real(data,tag)
  real,intent(in) :: data
  integer,intent(in) :: tag
  print *,"put_real called with data ",data," and tag ",tag
end subroutine put_real

subroutine put_real_array_1d(data,tag)
  real,intent(in) :: data(:)
  integer,intent(in) :: tag
  print *,"put_real_array_1d called with data size ",size(data,1)," and tag ",tag
end subroutine put_real_array_1d

subroutine put_real_array_2d(data,tag)
  logical,intent(in) :: data(:,:)
  integer,intent(in) :: tag
  print *,"put_real_array_2d called with data size (",size(data,1),",",size(data,2),") and tag ",tag
end subroutine put_real_array_2d
