module BFG
private
public put
interface put
!
  subroutine put_integer(data,tag)
    integer,intent(in) :: data
    integer,intent(in) :: tag
  end subroutine put_integer
  subroutine put_integer_array_1d(data,tag)
    integer,intent(in) :: data(:)
    integer,intent(in) :: tag
  end subroutine put_integer_array_1d
  subroutine put_integer_array_2d(data,tag)
    integer,intent(in) :: data(:,:)
    integer,intent(in) :: tag
  end subroutine put_integer_array_2d
!
  subroutine put_real(data,tag)
    real,intent(in) :: data
    integer,intent(in) :: tag
  end subroutine put_real
  subroutine put_real_array_1d(data,tag)
    real,intent(in) :: data(:)
    integer,intent(in) :: tag
  end subroutine put_real_array_1d
  subroutine put_real_array_2d(data,tag)
    real,intent(in) :: data(:,:)
    integer,intent(in) :: tag
  end subroutine put_real_array_2d
!
end interface
end module BFG
