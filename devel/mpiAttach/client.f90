program client
  use MPI, only : MPI_MAX_PORT_NAME,MPI_INFO_NULL,mpi_init,mpi_comm_connect,mpi_finalize,MPI_COMM_SELF
  implicit none
  integer :: ierr
  !character(len=MPI_MAX_PORT_NAME) :: port_name="1123811328\.0\;tcp:\/\/193.62\.112\.247\:42846\+1123811329\.0\;tcp\:\/\/193\.62\.112\.247\:59937\:300"
  character(len=MPI_MAX_PORT_NAME) :: port_name="927924224.0;tcp://193.62.112.247:41960+927924225.0;tcp://193.62.112.247:45647:300"
  integer :: newcomm
  call mpi_init(ierr)
  call mpi_comm_connect(port_name, MPI_INFO_NULL, 0, MPI_COMM_SELF, newcomm,ierr);
  print *,"HELLO WORLD!"
  call mpi_finalize(ierr)
end program client
