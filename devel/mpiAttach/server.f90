program server
  !!!use MPI, only : MPI_MAX_PORT_NAME,MPI_Info,MPI_INFO_NULL,mpi_init,mpi_open_port,mpi_finalize
  use MPI, only : MPI_MAX_PORT_NAME,MPI_INFO_NULL,mpi_init,mpi_open_port,mpi_finalize,MPI_COMM_SELF
  implicit none
  integer :: ierr
  character(len=MPI_MAX_PORT_NAME) :: port_name
  integer :: newcomm
  !!! type(MPI_Info) :: info=MPI_INFO_NULL
  call mpi_init(ierr)
  !!!call mpi_open_port(info,port_name,ierr)
  call mpi_open_port(MPI_INFO_NULL,port_name,ierr)
  print *,"PORT_NAME=",port_name
  call mpi_comm_accept(port_name,MPI_INFO_NULL,0,MPI_COMM_SELF,newcomm,ierr)
  print *,"ERROR=",ierr
  print *,"HELLO WORLD!"
  call mpi_close_port(port_name,ierr)
  call mpi_finalize(ierr)
end program server
