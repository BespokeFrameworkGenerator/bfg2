       module BFG2Target1

       ! esmf wrapped "single" model
       use single_esmf, only : user_register_single=>user_register
       use multiple_esmf, only : user_register_multiple=>user_register

       use ESMF

       implicit none

       integer :: activemodelid
       type(ESMF_GridComp) :: comp1, comp2
       type(ESMF_State) :: imp, exp
       type(ESMF_VM):: vm
       integer :: threadID

       contains

       ! in sequence support routines start
       subroutine setActiveModel(idIN)
         integer , intent(in) :: idIN
         activeModelID=idIN
       end subroutine setActiveModel

       integer function getActiveModel()
         getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end

       ! concurrency support routines start
       logical function singleThread()
         if (threadID.eq.0) then
           singleThread=.true.
         else
           singleThread=.false.
         end if
       end function singleThread

       logical function multipleThread()
         if (threadID.eq.1) then
           multipleThread=.true.
         else
           multipleThread=.false.
         end if
       end function multipleThread

       subroutine commsSync()
         integer :: rc
         call ESMF_VMBarrier(vm,rc=rc)
       end subroutine commsSync
       ! concurrency support routines end

       subroutine initComms()

         integer :: rc
         character(len=ESMF_MAXSTR) :: cname
         integer :: localPet, petCount, peCount

         call ESMF_Initialize(vm=vm, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 single model wrapped in esmf"
         comp1 = ESMF_GridCompCreate(name=cname, petlist=(/0/), rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 multiple model wrapped in esmf"
         comp2 = ESMF_GridCompCreate(name=cname, petlist=(/1/), rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(comp1, userRoutine=user_register_single, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(comp2, userRoutine=user_register_multiple, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         imp = ESMF_StateCreate(name="igrid import state", stateintent=ESMF_STATEINTENT_IMPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return
         exp = ESMF_StateCreate(name="igrid export state", stateintent=ESMF_STATEINTENT_EXPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         ! Check we have the correct number of threads
         call ESMF_VMGet(vm, localPet=localPet, petCount=petCount, &
                             peCount=peCount, rc=rc)

         ! store my thread ID
         threadID=localPet
         print *, "Running on ",petCount," threads."
         ! RF Not sure of the difference between petCount and peCount
         !!print *, "There are ", peCount," PEs referenced by this VM"
         if (petCount.ne.2) then
           print *,"Error, expecting 2 threads, terminating"
           call ESMF_finalize(rc=rc)
         end if

       end subroutine initComms

       subroutine single_singlesub_iteration()
         integer :: rc

         print *,"[",threadID,"] in single_singlesub_iteration"

         call ESMF_GridCompRun(comp1, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine single_singlesub_iteration

       subroutine multiple_init1_init()
         integer :: rc

         print *,"[",threadID,"] in multiple_init1_init"

         call ESMF_GridCompInitialize(comp2, importState=imp, exportState=exp, phase=1, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine multiple_init1_init

       subroutine multiple_init2_init()
         integer :: rc

         print *,"[",threadID,"] in multiple_init2_init"

         call ESMF_GridCompInitialize(comp2, importState=imp, exportState=exp, phase=2, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine multiple_init2_init

       subroutine multiple_tstep1_iteration()
         integer :: rc

         print *,"[",threadID,"] in multiple_tstep1_iteration"

         call ESMF_GridCompRun(comp2, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine multiple_tstep1_iteration

       subroutine multiple_finish_final()
         integer :: rc

         print *,"[",threadID,"] in multiple_finish_final"

         call ESMF_GridCompFinalize(comp2, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine multiple_finish_final

       subroutine finaliseComms()

         integer :: rc

         call ESMF_GridCompFinalize(comp1, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10

         call ESMF_GridCompDestroy(comp1, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompDestroy(comp2, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(imp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
      
 10      continue

         call ESMF_Finalize(rc=rc)

       end subroutine finaliseComms

       end module BFG2Target1
