! manugen example of an esmf wrapped model
module multiple_esmf

! include the original BFG2 model code
use multiple, only : init1,init2,tstep1,finish
use ESMF

implicit none

private
public user_register
contains

  subroutine user_register(comp,rc)
    type(ESMF_GridComp) :: comp
    integer, intent(out) :: rc

    print *,"entered multiple_esmf:user_register"

    ! register our callback routine
    call ESMF_GridCompSetEntryPoint(comp, ESMF_METHOD_INITIALIZE, &
                                    userRoutine=init1_esmf, phase=1, rc=rc)
    call ESMF_GridCompSetEntryPoint(comp, ESMF_METHOD_INITIALIZE, &
                                    userRoutine=init2_esmf, phase=2, rc=rc)
    call ESMF_GridCompSetEntryPoint(comp, ESMF_METHOD_RUN, &
                                    userRoutine=tstep1_esmf, rc=rc)
    call ESMF_GridCompSetEntryPoint(comp, ESMF_METHOD_FINALIZE, &
                                    userRoutine=finish_esmf, rc=rc)
    print *, "completed multiple_esmf:user_register"

  end subroutine user_register

  subroutine init1_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered multiple_esmf:init1_esmf"

    ! call the original bfg model
    call init1()

    print *, "completed multiple_esmf:init1_esmf"

    rc = ESMF_SUCCESS

  end subroutine init1_esmf

  subroutine init2_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered multiple_esmf:init2_esmf"

    ! call the original bfg model
    call init2()

    print *, "completed multiple_esmf:init2_esmf"

    rc = ESMF_SUCCESS

  end subroutine init2_esmf

  subroutine tstep1_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered multiple_esmf:tstep1_esmf"

    ! call the original bfg model
    call tstep1()

    print *, "completed multiple_esmf:tstep1_esmf"

    rc = ESMF_SUCCESS

  end subroutine tstep1_esmf

  subroutine finish_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered multiple_esmf:finish_esmf"

    ! call the original bfg model
    call finish()

    print *, "completed multiple_esmf:finish_esmf"

    rc = ESMF_SUCCESS

  end subroutine finish_esmf

end module multiple_esmf
