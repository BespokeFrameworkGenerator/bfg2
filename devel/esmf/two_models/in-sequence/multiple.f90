module multiple
private
public init1,init2,tstep1,finish
contains
      subroutine init1()
      print *,"f90:multiple/init1 is called"
      end subroutine init1

      subroutine init2()
      print *,"f90:multiple/init2 is called"
      end subroutine init2

      subroutine tstep1()
      print *,"f90:multiple/tstep1 is called"
      end subroutine tstep1

      subroutine finish()
      print *,"f90:multiple/finish is called"
      end subroutine finish
end module multiple
