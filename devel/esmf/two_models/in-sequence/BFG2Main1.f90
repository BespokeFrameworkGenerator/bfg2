       program BFG2Main
       use BFG2Target1
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !single__freq
       integer :: single__freq
       namelist /time/ nts1,single__freq,multiple__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read

       if (multipleThread()) then
         call setActiveModel(1)
         call multiple_init1_init()
       end if

       if (multipleThread()) then
         call setActiveModel(2)
         call multiple_init2_init()
       end if

       do its1=1,nts1

         print *, "Iteration ",its1," of ",nts1

         if (singleThread()) then
           if(mod(its1,single__freq).eq.0)then
             call setActiveModel(3)
             call single_singlesub_iteration()
           end if
         end if

         if (multipleThread()) then
           if(mod(its1,multiple__freq).eq.0)then
             call setActiveModel(4)
             call multiple_tstep1_iteration()
           end if
         end if

         call commsSync()

       end do

       if (multipleThread()) then
         call setActiveModel(5)
         call multiple_finish_final()
       end if

      call finaliseComms()

       end program BFG2Main

