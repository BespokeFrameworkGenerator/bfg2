       module BFG2Target1

       ! esmf wrapped "single" model
       use single_esmf, only : user_register

       use ESMF

       implicit none

       integer :: activemodelid
       type(ESMF_GridComp) :: comp1
       type(ESMF_State) :: imp, exp

       contains

       ! in sequence support routines start
       subroutine setActiveModel(idIN)
         integer , intent(in) :: idIN
         activeModelID=idIN
       end subroutine setActiveModel

       integer function getActiveModel()
         getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end

       ! concurrency support routines start
       logical function singleThread()
       implicit none
       !Add in PET logic test here for multiple threads
       !if (bfgSUID==1) then
       singleThread=.true.
       !else
       !singleThread=.false.
       !end if
       end function singleThread

       subroutine commsSync()
         !????
       end subroutine commsSync
       ! concurrency support routines end

       subroutine initComms()

         integer :: rc
         character(len=ESMF_MAXSTR) :: cname

         call ESMF_Initialize(rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 coupled model wrapped in esmf"
         comp1 = ESMF_GridCompCreate(name=cname, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(comp1, userRoutine=user_register, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         imp = ESMF_StateCreate(name="igrid import state", stateintent=ESMF_STATEINTENT_IMPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return
         exp = ESMF_StateCreate(name="igrid export state", stateintent=ESMF_STATEINTENT_EXPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine initComms

       subroutine single_singlesub_iteration()
         integer :: rc

         call ESMF_GridCompRun(comp1, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine single_singlesub_iteration

       subroutine finaliseComms()

         integer :: rc

         call ESMF_GridCompFinalize(comp1, importState=imp, exportState=exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10

         call ESMF_GridCompDestroy(comp1, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(imp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
      
 10      continue

         call ESMF_Finalize(rc=rc)

       end subroutine finaliseComms

       end module BFG2Target1
