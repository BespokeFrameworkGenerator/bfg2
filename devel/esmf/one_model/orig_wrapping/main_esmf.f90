    program esmf_main

    ! ESMF Framework module
    use ESMF
    use single_esmf

    implicit none
    
!   Local variables
    integer :: my_pet, rc
    type(ESMF_VM):: vm
    type(ESMF_GridComp) :: comp1
    type(ESMF_State) :: imp, exp
    character(len=ESMF_MAXSTR) :: cname
    integer :: i
        
    call ESMF_Initialize(rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

    ! Get the default global VM
    call ESMF_VMGetGlobal(vm, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

    ! Get our pet number for output print statements
    call ESMF_VMGet(vm, localPet=my_pet, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

    cname = "bfg2 coupled model wrapped in esmf"
    comp1 = ESMF_GridCompCreate(name=cname, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10
    call ESMF_GridCompPrint(comp1)

    print *, "Comp Create finished, name = ", trim(cname)


!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!  Register section
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
      !call ESMF_GridCompSetVM(comp1, userRoutine=user_setvm, &
      !  userRc=userrc, rc=rc)
      !if ((rc .ne. ESMF_SUCCESS) .or. (userrc .ne. ESMF_SUCCESS)) goto 10
      call ESMF_GridCompSetServices(comp1, userRoutine=user_register, &
         rc=rc)
      !  userRc=userrc, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10
      print *, "Comp Register finished, rc= ", rc

!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!  Init section
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
 
      imp = ESMF_StateCreate(name="igrid import state", stateintent=ESMF_STATEINTENT_IMPORT, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10
      exp = ESMF_StateCreate(name="igrid export state", stateintent=ESMF_STATEINTENT_EXPORT, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10

!      call ESMF_GridCompInitialize(comp1, imp, exp, phase=1, rc=rc)
!      if (rc .ne. ESMF_SUCCESS) goto 10
!      print *, "Comp Initialize 1 finished"
 
!      call ESMF_GridCompInitialize(comp1, imp, exp, phase=2, rc=rc)
!      if (rc .ne. ESMF_SUCCESS) goto 10
!      print *, "Comp Initialize 2 finished"
 
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!     Run section
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------

      do i=1,10

        print *, "Call ",i," of ",10
 
        call ESMF_GridCompRun(comp1, importState=imp,exportState=exp, rc=rc)
        if (rc .ne. ESMF_SUCCESS) goto 10

      end do
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!     Finalize section
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!     Print result

      call ESMF_GridCompFinalize(comp1, importState=imp, exportState=exp, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10


!
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!     Destroy section
!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
!     Clean up

      call ESMF_GridCompDestroy(comp1, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10
      call ESMF_StateDestroy(imp, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10
      call ESMF_StateDestroy(exp, rc=rc)
      if (rc .ne. ESMF_SUCCESS) goto 10
      print *, "All Destroy routines done"

!-------------------------------------------------------------------------
!-------------------------------------------------------------------------
 
 10   print *, "System Test CompCreate complete"

      call ESMF_Finalize(rc=rc)

      end program esmf_main
