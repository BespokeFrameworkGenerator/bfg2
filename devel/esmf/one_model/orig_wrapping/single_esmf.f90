! manugen example of an esmf wrapped model
module single_esmf

! include the original BFG2 model code
use single, only : singlesub
use ESMF

implicit none

private
public user_register
contains

  subroutine user_register(comp,rc)
    type(ESMF_GridComp) :: comp
    integer, intent(out) :: rc

    print *,"entered single_esmf:user_register"

    ! register our callback routine
    call ESMF_GridCompSetEntryPoint(comp, ESMF_METHOD_RUN,&
                 userRoutine=singlesub_esmf,rc=rc)
    print *, "completed single_esmf:user_register"

  end subroutine user_register

  subroutine singlesub_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered single_esmf:singlesub_esmf"

    ! call the original bfg model
    call singlesub()

    print *, "completed single_esmf:singlesub_esmf"

    rc = ESMF_SUCCESS

  end subroutine singlesub_esmf

end module single_esmf
