module modINOUT2
!
implicit none
private
public :: run
!
contains
!
  subroutine run(complexArg,charArg,logicalArg,intArg,realArg)
  real,intent(inout) :: realArg
  integer,intent(inout) :: intArg
  logical,intent(inout) :: logicalArg
  character,intent(inout) :: charArg
  complex,intent(inout) :: complexArg

  print *,"modINOUT2:run called"
  print *,"on entry realArg is",realArg
  print *,"on entry intArg is",intArg
  print *,"on entry logicalArg is",logicalArg
  print *,"on entry charArg is",charArg
  print *,"on entry complexArg is",complexArg
  realArg=realArg+1
  intArg=intArg+1
  logicalArg=.true.
  charArg='c'
  complexArg=complexArg+(1.0,1.0)
  print *,"on exit realArg is",realArg
  print *,"on exit intArg is",intArg
  print *,"on exit logicalArg is",logicalArg
  print *,"on exit charArg is",charArg
  print *,"on exit complexArg is",complexArg
  print *,"modINOUT2:run complete"
  end subroutine run
!
end module modINOUT2
