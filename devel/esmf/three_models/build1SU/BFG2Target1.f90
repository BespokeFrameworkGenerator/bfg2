       module BFG2Target1

       use modIN_esmf,     only : user_register_modIN=>user_register
       use modINOUT1_esmf, only : user_register_modINOUT1=>user_register
       use modINOUT2_esmf, only : user_register_modINOUT2=>user_register
       use modOUT_esmf,    only : user_register_modOUT=>user_register

       use Coupler_to_modINOUT1_run, only : &
           user_register_to_modINOUT1_run=>user_register

       use ESMF_Mod

       implicit none

       integer :: activemodelid
       type(ESMF_GridComp) :: modIN_comp, modINOUT1_comp, &
                              modINOUT2_comp, modOUT_comp
       type(ESMF_CplComp) :: cpl_to_modINOUT1_run
       type(ESMF_State) :: imp, exp, exp_modOUT, imp_modINOUT1

       contains

       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end

       ! concurrency support routines start
       logical function modOUTThread()
       ! only one thread so always true
       modOUTThread=.true.
       end function modOUTThread
       logical function modINOUT1Thread()
       ! only one thread so always true
       modINOUT1Thread=.true.
       end function modINOUT1Thread
       logical function modINOUT2Thread()
       ! only one thread so always true
       modINOUT2Thread=.true.
       end function modINOUT2Thread
       logical function modINThread()
       ! only one thread so always true
       modINThread=.true.
       end function modINThread

       subroutine commsSync()
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end

       subroutine modIN_run_iteration()
         integer :: rc

         call ESMF_GridCompRun(modIN_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine modIN_run_iteration

       subroutine modINOUT1_run_iteration()
         integer :: rc

         ! for arg passing we will have to choose which state we expect to get
         ! data from here

         call ESMF_CplCompRun(cpl_to_modINOUT1_run, exp_modOUT, imp_modINOUT1, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompRun(modINOUT1_comp, imp_modINOUT1, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine modINOUT1_run_iteration

       subroutine modINOUT2_run_iteration()
         integer :: rc

         call ESMF_GridCompRun(modINOUT2_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine modINOUT2_run_iteration

       subroutine modOUT_run_iteration()
         integer :: rc

         call ESMF_GridCompRun(modOUT_comp, imp, exp_modOUT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

       end subroutine modOUT_run_iteration


       subroutine initComms()

         integer :: rc
         character(len=ESMF_MAXSTR) :: cname
         type(ESMF_VM):: vm
         integer :: localPet, petCount, peCount

         call ESMF_Initialize(vm=vm, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 modIN model wrapped in esmf"
         modIN_comp = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 modINOUT1 model wrapped in esmf"
         modINOUT1_comp = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 modINOUT2 model wrapped in esmf"
         modINOUT2_comp = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 modOUT model wrapped in esmf"
         modOUT_comp = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 coupler from modOUT to modINOUT1"
         cpl_to_modINOUT1_run = ESMF_CplCompCreate(name=cname, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         
         call ESMF_GridCompSetServices(modIN_comp, user_register_modIN, rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(modINOUT1_comp, user_register_modINOUT1, rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(modINOUT2_comp, user_register_modINOUT2, rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(modOUT_comp, user_register_modOUT, rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_CplCompSetServices(cpl_to_modINOUT1_run, &
                                       user_register_to_modINOUT1_run, rc)
         if (rc .ne. ESMF_SUCCESS) return

         imp = ESMF_StateCreate("igrid import state", ESMF_STATE_IMPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return
         exp = ESMF_StateCreate("igrid export state", ESMF_STATE_EXPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         imp_modINOUT1 = ESMF_StateCreate("modINOUT1 import state", ESMF_STATE_IMPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return
         exp_modOUT = ESMF_StateCreate("modOUT export state", ESMF_STATE_EXPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         ! Check we have the correct number of threads
         call ESMF_VMGet(vm, localPet=localPet, petCount=petCount, &
                                                peCount=peCount, rc=rc)

         print *, "Running on ",petCount," threads."
         ! RF Not sure of the difference between petCount and peCount
         !!print *, "There are ", peCount," PEs referenced by this VM"
         if (petCount.ne.1) then
           print *,"Error, expecting 1 thread, terminating"
           call ESMF_finalize(rc=rc)
         end if

       end subroutine initComms

       subroutine finaliseComms()

         integer :: rc

         ! shut down modOUT esmf state
         call ESMF_GridCompFinalize(modOUT_comp, imp, exp_modOUT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         ! shut down modINOUT1 esmf state
         call ESMF_GridCompFinalize(modINOUT1_comp, imp_modINOUT1, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompFinalize(modIN_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompFinalize(modINOUT1_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompFinalize(modINOUT2_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompFinalize(modOUT_comp, imp, exp, rc=rc)
         if (rc .ne. ESMF_SUCCESS) goto 10

         call ESMF_GridCompDestroy(modIN_comp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompDestroy(modINOUT1_comp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompDestroy(modINOUT2_comp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_GridCompDestroy(modOUT_comp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10

         call ESMF_StateDestroy(imp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(exp, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(imp_modINOUT1, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
         call ESMF_StateDestroy(exp_modOUT, rc)
         if (rc .ne. ESMF_SUCCESS) goto 10
      
 10      continue

         call ESMF_Finalize(rc=rc)

       end subroutine finaliseComms

       end module BFG2Target1
