module Coupler_to_modINOUT1_run

use ESMF_Mod

implicit none

private
public user_register
contains

  subroutine user_register(comp,rc)
    type(ESMF_CplComp),intent(inout) :: comp
    integer, intent(out) :: rc

    print *,"entered Coupler_to_modINOUT1_run:user_register"

    ! register work callback routine
    call ESMF_CplCompSetEntryPoint(comp, ESMF_SETRUN, run, &
                                                      ESMF_SINGLEPHASE, rc)
    print *, "completed Coupler_to_modINOUT1_run:user_register"

  end subroutine user_register

  subroutine run(comp,importState,exportState,clock,rc)
    type(ESMF_CplComp), intent(inout) :: comp
    type(ESMF_State), intent(inout) :: importState, exportState
    type(ESMF_Clock), intent(inout) :: clock
    integer, intent(out) :: rc

    type(ESMF_Array) :: src_array, dst_array
    type(ESMF_VM) :: vm
    character(ESMF_MAXSTR) :: statename

    real, pointer :: farrayPtr(:)

    print *,"entered Coupler_to_modINOUT1_run:run"

    call ESMF_StateGet(importState, name=statename, rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)
    print *,"Import state name is ",trim(statename)
    
    call ESMF_StatePrint(importState,rc=rc)

    !dst_array=src_array
    !call ESMF_StateAdd(exportState, dst_array, rc=rc)
    !if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    !if (ESMF_StateIsNeeded(importState,"realArg",rc)) then
    !  print *, "RealArg is required"
      call ESMF_StateGet(importState, "realArg", src_array, rc=rc)
      if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)
    !else
    !  print *, "RealArg is set to not required in import state"
    !end if

    call ESMF_ArrayGet(src_array, localDe=0, farrayPtr=farrayPtr, rc=rc)
    print *,"farrayPtr=",farrayPtr

    call ESMF_StatePrint(exportState,rc=rc)

    print *, "completed Coupler_to_modINOUT1_run:run"

    rc=ESMF_SUCCESS

  end subroutine run

end module Coupler_to_modINOUT1_run
