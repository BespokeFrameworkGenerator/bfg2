! manugen example of an esmf wrapped model
module modIN_esmf

! include the original BFG2 model code
use modIN, only : run
use ESMF_Mod

implicit none

private
public user_register
contains

  subroutine user_register(comp,rc)
    type(ESMF_GridComp) :: comp
    integer, intent(out) :: rc

    print *,"entered modIN_esmf:user_register"

    ! register our callback routine
    call ESMF_GridCompSetEntryPoint(comp, ESMF_SETRUN, run_esmf, &
                                                      ESMF_SINGLEPHASE, rc)
    print *, "completed modIN_esmf:user_register"

  end subroutine user_register

  subroutine run_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    real      :: realArg
    integer   :: intArg
    logical   :: logicalArg
    character :: charArg
    complex   :: complexArg

    print *, "entered modIN_esmf:run_esmf"

    ! call the original bfg model
    call run(complexArg,charArg,logicalArg,intArg,realArg)

    print *, "completed modIN_esmf:run_esmf"

    rc = ESMF_SUCCESS

  end subroutine run_esmf

end module modIN_esmf
