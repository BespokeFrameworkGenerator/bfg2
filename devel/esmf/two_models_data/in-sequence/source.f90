module source
private
public singlesub
real :: b=0.0
contains
  subroutine singlesub(a)
    real, intent(out) :: a
    b=b+1.0
    a=b
    print *,"f90:source/singlesub is called"
    print *,"f90:source/singlesub on exit a=",a
  end subroutine singlesub
end module source
