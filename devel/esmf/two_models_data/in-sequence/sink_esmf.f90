module sink_esmf

! include the original BFG2 model code
use sink, only : singlesub
use ESMF_Mod

implicit none

private
public user_register

real :: realArg1d(1)=0.0
real :: a=0.0

contains

  subroutine user_register(comp,rc)
    type(ESMF_GridComp) :: comp
    integer, intent(out) :: rc

    print *,"entered single_esmf:user_register"

    ! register our callback routines
    call ESMF_GridCompSetEntryPoint(comp, ESMF_SETINIT, init_esmf, &
                                                      ESMF_SINGLEPHASE, rc)
    call ESMF_GridCompSetEntryPoint(comp, ESMF_SETRUN, singlesub_esmf, &
                                                      ESMF_SINGLEPHASE, rc)
    print *, "completed sink_esmf:user_register"

  end subroutine user_register

  subroutine init_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    type(ESMF_DistGrid) :: distgrid    ! DistGrid object
    type(ESMF_Array)    :: array       ! Array object

    print *, "entered sink_esmf:init_esmf"

    ! create a dist grid with the global array size
    distgrid = ESMF_DistGridCreate(minIndex=(/1/), maxIndex=(/1/), rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    ! create array object which references our local data array
    array = ESMF_ArrayCreate(name="realArgIn", farray=realArg1D, distgrid=distgrid, rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    call ESMF_StateAdd(importState, array, rc=rc)

    print *, "completed sink_esmf:init_esmf"

    rc = ESMF_SUCCESS

  end subroutine init_esmf

  subroutine singlesub_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    real, pointer :: farrayPtr(:)
    type(ESMF_Array) :: src_array


    print *, "entered sink_esmf:singlesub_esmf"

    !call ESMF_StateGet(importState, "realArg", src_array, rc=rc)
    !if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)
    !call ESMF_ArrayGet(src_array, localDe=0, farrayPtr=farrayPtr, rc=rc)
    !if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    ! copy our new value of a
    !a=farrayPtr(1)
    a=realArg1D(1)

    ! call the original bfg model
    call singlesub(a)

    print *, "completed sink_esmf:singlesub_esmf"

    rc = ESMF_SUCCESS

  end subroutine singlesub_esmf

end module sink_esmf
