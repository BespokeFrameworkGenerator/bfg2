program driver

  use source_esmf, only : user_register_source=>user_register
  use sink_esmf,   only : user_register_sink=>user_register
  use cpl_esmf,    only : user_register_cpl=>user_register

  use ESMF_Mod

  implicit none

  type(ESMF_GridComp) :: comp1, comp2
  type(ESMF_CplComp)  :: cpl
  type(ESMF_State)    :: imp, exp

  integer :: rc
  character(len=ESMF_MAXSTR) :: cname
  type(ESMF_VM):: vm
  integer :: localPet, petCount, peCount
  integer :: i

  call ESMF_Initialize(vm=vm, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  cname = "bfg2 source model wrapped in esmf"
  comp1 = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  cname = "bfg2 sink model wrapped in esmf"
  comp2 = ESMF_GridCompCreate(name=cname, gridcompType=ESMF_ATM, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  cname = "bfg2 coupler from source to sink"
  cpl = ESMF_CplCompCreate(name=cname, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10


  call ESMF_GridCompSetServices(comp1, user_register_source, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  call ESMF_GridCompSetServices(comp2, user_register_sink, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  call ESMF_CplCompSetServices(cpl, user_register_cpl, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  imp = ESMF_StateCreate("igrid import state", ESMF_STATE_IMPORT, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  exp = ESMF_StateCreate("igrid export state", ESMF_STATE_EXPORT, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  ! Check we have the correct number of threads
  call ESMF_VMGet(vm, localPet=localPet, petCount=petCount, &
                                         peCount=peCount, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  print *, "Running on ",petCount," threads."
  ! RF Not sure of the difference between petCount and peCount
  !!print *, "There are ", peCount," PEs referenced by this VM"

  if (petCount.ne.1) then
    print *,"Error, expecting 1 thread, terminating"
    goto 10
  end if

  call ESMF_GridCompInitialize(comp1, imp, exp, rc=rc)
  call ESMF_GridCompInitialize(comp2, imp, exp, rc=rc)

  do i=1,3

    call ESMF_GridCompRun(comp1, imp, exp, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

    call ESMF_CplCompRun(cpl, imp, exp, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

    call ESMF_GridCompRun(comp2, exp, imp, rc=rc)
    if (rc .ne. ESMF_SUCCESS) goto 10

  end do

  call ESMF_GridCompFinalize(comp1, imp, exp, rc=rc)
  if (rc .ne. ESMF_SUCCESS) goto 10

  call ESMF_GridCompDestroy(comp1, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10
  call ESMF_GridCompDestroy(comp2, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10
  call ESMF_StateDestroy(imp, rc)
  if (rc .ne. ESMF_SUCCESS) goto 10
  call ESMF_StateDestroy(exp, rc)
      
 10 continue

  call ESMF_Finalize(rc=rc)

end program driver
