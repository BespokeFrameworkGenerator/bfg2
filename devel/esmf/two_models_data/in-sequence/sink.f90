module sink
private
public singlesub
contains
  subroutine singlesub(a)
    real, intent(in) :: a
    print *,"f90:sink/singlesub is called"
    print *,"f90:sink/singlesub on entry a=",a
  end subroutine singlesub
end module sink
