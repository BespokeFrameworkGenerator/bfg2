module cpl_esmf

use ESMF_Mod

implicit none

private
public user_register
contains

  subroutine user_register(comp,rc)
    type(ESMF_CplComp),intent(inout) :: comp
    integer, intent(out) :: rc

    print *,"entered cpl_esmf:user_register"

    ! register work callback routine
    call ESMF_CplCompSetEntryPoint(comp, ESMF_SETRUN, run, &
                                                      ESMF_SINGLEPHASE, rc)
    print *, "completed cpl_esmf:user_register"

  end subroutine user_register

  subroutine run(comp,importState,exportState,clock,rc)
    type(ESMF_CplComp), intent(inout) :: comp
    type(ESMF_State), intent(inout) :: importState, exportState
    type(ESMF_Clock), intent(inout) :: clock
    integer, intent(out) :: rc

    type(ESMF_Array) :: src_array, dest_array
    type(ESMF_VM) :: vm
    character(ESMF_MAXSTR) :: statename

    real, pointer :: fOutArrayPtr(:),fInArrayPtr(:)

    print *,"entered cpl_esmf:run"

    !RF We do not actually need a coupler when running in sequence as we can just
    !RF extract the required data from the import/export state.

    call ESMF_StateGet(exportState, "realArg", src_array, rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)
    call ESMF_StateGet(importState, "realArgIn", dest_array, rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    call ESMF_ArrayGet(src_array, localDe=0, farrayPtr=fOutArrayPtr, rc=rc)
    call ESMF_ArrayGet(dest_array, localDe=0, farrayPtr=fInArrayPtr, rc=rc)
    fInArrayPtr=fOutArrayPtr

    print *, "completed cpl_esmf:run"

    rc=ESMF_SUCCESS

  end subroutine run

end module cpl_esmf
