module source_esmf

! include the original BFG2 model code
use source, only : singlesub
use ESMF_Mod

implicit none

private
public user_register

real :: realArg1d(1)
real :: a=0.0

contains

  subroutine user_register(comp,rc)
    type(ESMF_GridComp) :: comp
    integer, intent(out) :: rc

    print *,"entered source_esmf:user_register"

    ! register our callback routines
    call ESMF_GridCompSetEntryPoint(comp, ESMF_SETINIT, init_esmf, &
                                                      ESMF_SINGLEPHASE, rc)
    call ESMF_GridCompSetEntryPoint(comp, ESMF_SETRUN, singlesub_esmf, &
                                                      ESMF_SINGLEPHASE, rc)

    print *, "completed single_esmf:user_register"

  end subroutine user_register

  subroutine init_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    type(ESMF_DistGrid) :: distgrid    ! DistGrid object
    type(ESMF_Array)    :: array       ! Array object

    print *, "entered source_esmf:init_esmf"

    ! create a dist grid with the global array size
    distgrid = ESMF_DistGridCreate(minIndex=(/1/), maxIndex=(/1/), rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    ! create array object which references our local data array
    array = ESMF_ArrayCreate(name="realArg", farray=realArg1D, distgrid=distgrid, rc=rc)
    if (rc /= ESMF_SUCCESS) call ESMF_Finalize(terminationflag=ESMF_ABORT)

    call ESMF_StateAdd(exportState, array, rc=rc)

    print *, "completed source_esmf:init_esmf"

    rc = ESMF_SUCCESS

  end subroutine init_esmf

  subroutine singlesub_esmf(comp,importState,exportState,clock,rc)
    type(ESMF_GridComp) :: comp
    type(ESMF_State) :: importState, exportState
    type(ESMF_Clock) :: clock
    integer, intent(out) :: rc

    print *, "entered source_esmf:singlesub_esmf"

    ! call the original bfg model
    call singlesub(a)
    ! copy into array as ESMF only supports coupling of arrays
    realArg1d(1)=a

    print *, "completed source_esmf:singlesub_esmf"

    rc = ESMF_SUCCESS

  end subroutine singlesub_esmf

end module source_esmf
