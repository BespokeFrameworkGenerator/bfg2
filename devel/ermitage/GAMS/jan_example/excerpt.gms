*reads data from gdx file and writes it into csv file 

$gdxin 'test.gdx'
set s1,s2;
$load s1,s2
parameter test(s1,s2);
$load test
$gdxin

file tfile /"data_out.csv" /  ;
tfile.pc=5 ;
tfile.pw=1000 ;
tfile.nd=7;
put tfile;
put "";
loop(s2, put s2.tl) ;
loop(s1,
     put / s1.tl ;
     loop(s2, put test(s1,s2)) ;
);
putclose;

display test;

