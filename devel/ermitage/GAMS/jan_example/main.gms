*dummy main model
*data is set, data is fed to output, new data is read from input


set s1 /bla,blub/;
set s2 /a,b/;

parameter test(s1,s2);

test(s1,s2) = 1;

display test;

*output into a gdx file
execute_unload 'test.gdx',s1,s2,test;
*dummy lines necessary for the put_utility function
file dummy;
put  dummy;
*execution of external script
put_utility 'shell' / 'test.cmd ' ;
*load input from gdx
execute_load 'test.gdx', test;

display test;

