*Feeds data back to the gdx file which is then read by main.gms

$gdxin 'test.gdx'
set s1,s2;
$load s1,s2
$gdxin

table test(s1,s2) test data set
$ondelim
$include "data_in.csv"
$offdelim ;


execute_unload 'test.gdx', test;
