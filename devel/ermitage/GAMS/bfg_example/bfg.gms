*example BFG GAMS model interface

execute 'bfg_init' ;

set i /i1*i5/;
alias(i,j);

parameter p(i);
scalar iteration;

p(i)=0;

loop(i,
*
*    bfg put start
*
     execute_unload 'test.gdx',p;
     execute 'put' ;
*
*    bfg put end
*
     p(i)=1+sum(j$(ord(j)<ord(i)),p(j));
     iteration=ord(i);
     display "inside loop",iteration,p;
*
*    bfg get start
*
     execute 'get' ;
     execute_load 'test.gdx', p;
*
*    bfg get end
*
*    bfg eos
     execute 'bfg_eos' ;
);


display "final",p;

execute 'bfg_finalise' ;
