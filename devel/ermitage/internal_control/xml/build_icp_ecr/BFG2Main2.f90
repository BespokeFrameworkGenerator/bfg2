       program BFG2Main
       use BFG2Target2
       use simpleArg2, only : simpleArg2_init_init=>init,&
simpleArg2_ts_iteration=>ts
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !simpleArg1_IC__freq
       integer :: simpleArg1_IC__freq
       !simpleArg2__freq
       integer :: simpleArg2__freq
       namelist /time/ nts1,simpleArg1_IC__freq,simpleArg2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (simpleArg2Thread()) then
       call setActiveModel(2)
       call simpleArg2_init_init()
       end if
       do its1=1,nts1
       if (simpleArg2Thread()) then
       if(mod(its1,simpleArg2__freq).eq.0)then
       call setActiveModel(4)
       call simpleArg2_ts_iteration()
       end if
       end if
       call commsSync()
       end do
       call finaliseComms()
       end program BFG2Main

