module simpleArg1
private
public init,ts
contains

  subroutine init(r1a,r1b,r1c)
  real,intent(out) :: r1a,r1b,r1c
  print *,"Hello from module simpleArg1 subroutine init"
  print *,"Internally setting r1a,r1b and r1c to 1.0 and -1.0 and 3.14 respectively"
  r1a=1.0
  r1b=-1.0
  r1c=3.14
  end subroutine init

  subroutine ts(r2a)
  real,intent(inout) :: r2a
  print *,"Hello from module simpleArg1 subroutine ts"
  print *,"inout arg r2a on input is ",r2a
  r2a=r2a+1.0
  print *,"inout arg r2a on output is ",r2a
  end subroutine ts

end module simpleArg1
