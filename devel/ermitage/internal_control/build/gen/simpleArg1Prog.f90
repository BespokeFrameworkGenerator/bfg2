program simpleArg1Prog

use simpleArg1
use BFG2

real :: r1a,r1b,r1c
integer :: nts=4

call bfginit()

call init(r1a,r1b,r1c)
call put(r1a,1)
call put(r1b,2)
call put(r1c,3)

do i=1,nts
  !RF no get here even though ts is in/out as we have internal coupling
  !RF This decision is obviously a user (i.e. my!) choice at this point
  !!call get(r1a,1)
  call ts(r1a)
  call put(r1a,4)
end do

call bfgfinalise()

end program simpleArg1Prog
