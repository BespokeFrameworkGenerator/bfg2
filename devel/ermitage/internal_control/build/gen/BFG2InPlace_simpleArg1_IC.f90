       module BFG2
       use BFG2Target1, bfginit=>initComms,bfgfinalise=>finaliseComms
       use mpi
       private 
       public :: get,put,bfginit,bfgfinalise
       interface get
       end interface
       interface put
       module procedure putreal__0
       end interface
       contains
       subroutine putreal__0(arg1,arg2)
       implicit none
       real   :: arg1
       integer   :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       integer, save :: offset=0
       !!!currentModel=getActiveModel()
       currentModel=13
       if (currentModel==13) then
       ! I am model=simpleArg1_IC and ep=simpleArg1_ICx and instance=
       if (arg2==1) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=1
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,5,mpi_comm_world,ierr)
       end if
       if (arg2==2) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=2
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,6,mpi_comm_world,ierr)
       end if
       if (arg2==3) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=3
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,7,mpi_comm_world,ierr)
       end if
       if (arg2==4) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=ts id=4
       mysize=1
       offset=offset+1
       call mpi_send(arg1,mysize,mpi_real,du,8+offset,mpi_comm_world,ierr)
       end if
       end if
!!!       if (currentModel==13) then
       if (.false.) then
       ! I am model=simpleArg1_IC and ep=simpleArg1_ICx and instance=
       if (arg2==1) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=1
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,5,mpi_comm_world,ierr)
       end if
       if (arg2==2) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=2
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,6,mpi_comm_world,ierr)
       end if
       if (arg2==3) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=init id=3
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,7,mpi_comm_world,ierr)
       end if
       if (arg2==4) then
       ! I am simpleArg1_IC.simpleArg1_ICx..13
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=simpleArg2_IC ep=ts id=4
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,8,mpi_comm_world,ierr)
       end if
       end if
       end subroutine putreal__0
       end module BFG2
