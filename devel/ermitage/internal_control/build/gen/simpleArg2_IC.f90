module simpleArg2_IC
use BFG2
private
public init,ts
contains

!  subroutine init(r1a,r1b,r1c)
  subroutine init()
!  real,intent(in) :: r1a,r1b,r1c
  real :: r1a,r1b,r1c
  print *,"Hello from module simpleArg2 subroutine init"
  call get(r1a,1)
  call get(r1b,2)
  call get(r1c,3)
  print *,"in arg r1a=",r1a
  print *,"in arg r1b=",r1b
  print *,"in arg r1c=",r1c
  end subroutine init

!  subroutine ts(r2a)
  subroutine ts()
!  real,intent(in) :: r2a
  real :: r2a
  call get(r2a,4)
  print *,"Hello from module simpleArg2 subroutine ts"
  print *,"in arg r2a on input is ",r2a
  end subroutine ts

end module simpleArg2_IC
