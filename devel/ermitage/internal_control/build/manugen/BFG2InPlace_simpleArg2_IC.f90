       module BFG2
       use BFG2Target2
       use mpi
       private 
       public :: get,put
       interface get
       module procedure getreal__0
       end interface
       interface put
       end interface
       contains
       subroutine getreal__0(arg1,arg2)
       implicit none
       real   :: arg1
       integer   :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(4) :: pp2p=(/0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=simpleArg2_IC and ep=init and instance=
       if (arg2==1) then
       ! I am simpleArg2_IC.init..2
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=simpleArg1_IC ep=init id=1
       du=b2mmap(1)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_real,du,5,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==2) then
       ! I am simpleArg2_IC.init..2
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=simpleArg1_IC ep=init id=2
       du=b2mmap(1)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_real,du,6,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==3) then
       ! I am simpleArg2_IC.init..2
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=simpleArg1_IC ep=init id=3
       du=b2mmap(1)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_real,du,7,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (currentModel==4) then
       ! I am model=simpleArg2_IC and ep=ts and instance=
       if (arg2==4) then
       ! I am simpleArg2_IC.ts..4
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=simpleArg1_IC ep=ts id=4
       du=b2mmap(1)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_real,du,8,mpi_comm_world,istatus,ierr)
       end if
       end if
       end subroutine getreal__0
       end module BFG2
