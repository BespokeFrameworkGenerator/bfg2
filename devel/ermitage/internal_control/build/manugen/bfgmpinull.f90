module bfg2
private
public bfginit, bfgfinalise, put, get_local_communicator
interface put
  module procedure putreal
  module procedure putinteger
end interface put

contains
  !
  subroutine bfginit()
    print *,"dummy bfginit called"
  end subroutine bfginit
  !
  subroutine bfgfinalise()
    print *,"dummy bfgfinalise called"
  end subroutine bfgfinalise
  !
  integer function get_local_communicator()
    get_local_communicator=-1
  end function get_local_communicator
  !
  subroutine putreal(data,tag)
    real :: data
    integer :: tag
    print *,"dummy putreal called"
  end subroutine putreal
  !
  subroutine putinteger(data,tag)
    integer :: data
    integer :: tag
    print *,"dummy putinteger called"
  end subroutine putinteger
  !
end module bfg2
