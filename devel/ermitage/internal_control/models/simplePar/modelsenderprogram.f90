program ModelSenderProgram

use modelsender
use BFG2

integer :: i,nts=4

call bfginit()

call senderinit()

do i=1,nts
  call sender()
end do

call bfgfinalise()

end program ModelSenderProgram
