module simpleExample1
private
public init_inplace,init_argpass,ts_inplace,ts_argpass
contains

  subroutine init_inplace()
  use bfg2, only put
  real :: r1a,r1b,r1c
  print *,"Hello from module simpleExample1 subroutine init_inplace"
  call init_argpass(r1a,r1b,r1c)
  call put(r1a,1)
  call put(r1b,2)
  call put(r1c,3)
  end subroutine init_inplace

  subroutine init_argpass(r1a,r1b,r1c)
  real,intent(out) :: r1a,r1b,r1c
  print *,"Hello from module simpleExample1 subroutine init_argpass"
  print *,"Internally setting r1a,r1b and r1c to 1.0 and -1.0 and 3.14 respectively"
  r1a=1.0
  r1b=-1.0
  r1c=3.14
  end subroutine init_argpass

  subroutine ts_inplace()
  use bfg2, only put
  real :: r2a
  print *,"Hello from module simpleExample1 subroutine ts_inplace"
  call ts_argpass(r2a)
  call put(r2a,1)
  end subroutine ts_inplace

  subroutine ts_argpass(r2a)
  real,intent(inout) :: r2a
  print *,"Hello from module simpleExample1 subroutine ts_argpass"
  print *,"inout arg r2a on input is ",r2a
  r2a=r2a+1.0
  print *,"inout arg r2a on output is ",r2a
  end subroutine ts_argpass

end module simpleExample1
