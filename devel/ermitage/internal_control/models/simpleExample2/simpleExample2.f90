module simpleExample2
private
public init_inplace,init_argpass,ts_inplace,ts_argpass
contains

  subroutine init_inplace()
  use bfg2, only : get
  real :: r1a,r1b,r1c
  print *,"Hello from module simpleExample2 subroutine init_inplace"
  call get(r1a,1)
  call get(r1b,2)
  call get(r1c,3)
  call init_argpass(r1a,r1b,r1c)
  end subroutine init_inplace

  subroutine init_argpass(r1a,r1b,r1c)
  real,intent(in) :: r1a,r1b,r1c
  print *,"Hello from module simpleExample2 subroutine init_argpass"
  print *,"in arg r1a=",r1a
  print *,"in arg r1b=",r1b
  print *,"in arg r1c=",r1c
  end subroutine init_argpass

  subroutine ts_inplace()
  use bfg2, only get
  real :: r2a
  print *,"Hello from module simpleExample2 subroutine ts_inplace"
  call get(r2a,4)
  call ts_argpass(r2a)
  end subroutine ts_inplace

  subroutine ts_argpass(r2a)
  real,intent(in) :: r2a
  print *,"Hello from module simpleExample2 subroutine ts_argpass"
  print *,"in arg r2a on input is ",r2a
  end subroutine ts_argpass

end module simpleExample2
