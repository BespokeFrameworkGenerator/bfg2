<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 3rd July 2006
    TargetDefined: check that a target is defined unless only one su and all models are argpassing
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that a target is defined unless only one su and all models are argpassing
</xsl:text>
</xsl:if>

<xsl:variable name="NSUs" select="count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit)"/>

<!-- add an x into InPlaceCalls for each inplace call that is connected or connects. Do not look in set notation as inplace can not be used in set notation terminology (currently a hypothesis!)-->
<xsl:variable name="InPlaceCalls">
<xsl:for-each select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data[form='inplace']">
  <xsl:variable name="modelName" select="ancestor::name"/>
  <xsl:variable name="EPName" select="ancestor::entryPoint/@name"/>
  <xsl:variable name="id" select=".//scalar/@id"/>
  <xsl:variable name="direction" select=".//scalar/@direction"/>
  <xsl:if test="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$modelName]/epChannel[@outEP=$EPName]/connection[@outID=$id] or document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@inModel=$modelName]/epChannel[@inEP=$EPName]/connection[@inID=$id] or document($root/coupled/composition)/composition/connections/priming/model[@name=$modelName]/entryPoint[@name=$EPName]/primed[@id=$id] or document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed/model[@name=$modelName and @entryPoint=$EPName and @id=$id] ">
  <xsl:text>x</xsl:text>
  </xsl:if>
</xsl:for-each>
</xsl:variable>

<xsl:variable name="target" select="document($root/coupled/deployment)/deployment/target"/>

<xsl:choose>
<xsl:when test="$NSUs='0'">
  <xsl:message terminate="yes">
    <xsl:text>Fatal Internal Error: TargetDefined.xsl expected to find at least one sequence unit but found none.
</xsl:text>
  </xsl:message>
</xsl:when>
<xsl:when test="not($target)">
  <xsl:if test="$NSUs!='1'">
    <xsl:message terminate="yes">
      <xsl:text>A target is not specified in the deployment yet there is more than one sequence unit requested.
</xsl:text>
    </xsl:message>
  </xsl:if>
  <xsl:if test="contains($InPlaceCalls,'x')">
    <xsl:message terminate="yes">
      <xsl:text>A target is not specified in the deployment yet there exists at least one connected in-place (put or get) call.
</xsl:text>
    </xsl:message>
  </xsl:if>
</xsl:when>
<xsl:otherwise> <!-- target exists -->
  <xsl:if test="$NSUs='1' and not(contains($InPlaceCalls,'x'))">
    <xsl:message terminate="yes">
      <xsl:text>A target is specified in the deployment yet there is only one sequence unit and all connections are performed by argument passing.
</xsl:text>
    </xsl:message>
  </xsl:if>
</xsl:otherwise>
</xsl:choose>

</xsl:template>
</xsl:stylesheet>
