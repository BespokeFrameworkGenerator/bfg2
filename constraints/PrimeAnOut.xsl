<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 12th Dec 2005
    PrimeAnOut: check that we do not attempt to prime an out.
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that we do not prime an out
</xsl:text>
</xsl:if>

<xsl:for-each select="document(/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[@direction='out' and not(ancestor::data/@form='funcret')]">

<xsl:variable name="modelName" select="ancestor::definition/name"/>
<xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
<xsl:variable name="form" select="@form"/>
<xsl:variable name="id" select="@id"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Model </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> ep </xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$form"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$id"/>
<xsl:text> direction out
</xsl:text>
</xsl:if>

<!-- skip this check if this ep has an "in" or "inout" with the same id -->
<xsl:choose>
<xsl:when test="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[(@direction='in' or @direction='inout') and @id=$id and ancestor::definition/name=$modelName and ancestor::entryPoint/@name=$epName]">
  <xsl:text>Skipping as the entrypoint has an in/inout with the same id
</xsl:text>
</xsl:when>
<xsl:otherwise>

<xsl:for-each select="document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed[ancestor::model/@name=$modelName and ancestor::entryPoint/@name=$epName and @id=$id]">

<xsl:message terminate="no">
<xsl:text>Error: Model </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> ep </xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$form"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$id"/>
<xsl:text> is declared with direction out but is primed in the composition document</xsl:text>
</xsl:message>

</xsl:for-each>

</xsl:otherwise>
</xsl:choose>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>
