<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 24th Nov 2005
    AllInsConnected: check that all model inputs
    have at least one connection
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">
<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that all model inputs have at least, and at most, one connection
</xsl:text>
</xsl:if>
<!-- iterate over each in or inout data in all entrypoints in all models -->
<xsl:for-each
select="document(/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[@direction='in' or @direction='inout']">
<xsl:variable name="myModelName" select="ancestor::definition/name"/>
<xsl:variable name="myEPName" select="ancestor::entryPoint/@name"/>
<xsl:variable name="myForm" select="ancestor::data/@form"/>
<xsl:variable name="myID" select="@id"/>

<xsl:choose>
  <!-- make sure that the modelName,epName tuple is specified in the schedule as entryPoints do
       not have to be called -->
  <xsl:when test="not(document($root//deployment)//schedule//model[@name=$myModelName and @ep=$myEPName])">
    <!-- skip tests as this entrypoint is not called -->
  </xsl:when>
  <!-- make sure that the model,EP tuple have at least one connection defined or the model,EP,id has a priming defined -->
  <xsl:when test="document($root//composition)//modelChannel[(@inModel=$myModelName and epChannel/@inEP=$myEPName)] or document($root//composition)//primed[ancestor::model/@name=$myModelName and ancestor::entryPoint/@name=$myEPName and @id=$myID]">

<xsl:if test="$verbose='yes'">
<xsl:text>Checking model </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$myForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>
</xsl:text>
</xsl:if>
<!-- is it coupled ??? -->
<!-- debug test
<xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection">
<xsl:value-of select="@inID"/>
<xsl:value-of select="ancestor::epChannel/@inEP"/>
<xsl:value-of select="ancestor::modelChannel/@inModel"/>
</xsl:for-each>
-->
<xsl:variable name="coupled"
select="count(document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[@inID=$myID
and ancestor::epChannel/@inEP=$myEPName and ancestor::modelChannel/@inModel=$myModelName])"/>
<xsl:variable name="primed"
select="count(document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed[@id=$myID
and ancestor::entryPoint/@name=$myEPName and ancestor::model/@name=$myModelName])"/>
<xsl:variable name="inaset"
select="document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$myModelName and @epName=$myEPName and @id=$myID]"/>
<!-- skip this check for the moment as ep's can get their first data from outside a loop and subsequent data from within a loop
<xsl:if test="$coupled>1">
<xsl:message terminate="no"><xsl:text>Error: My input is coupled to more than one thing. Model </xsl:text><xsl:value-of select="$myModelName"/><xsl:text> ep </xsl:text><xsl:value-of select="$myEPName"/><xsl:text> id </xsl:text><xsl:value-of select="$myID"/></xsl:message>
</xsl:if>
-->
<xsl:if test="$primed>1">
<xsl:message terminate="no">My input is primed by more than one thing</xsl:message>
</xsl:if>
<xsl:if test="$primed=0 and $coupled=0 and not($inaset)">
<xsl:message terminate="no">
<xsl:text>modelName='</xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text>' epName='</xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text>' id='</xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>' requires input but is neither coupled nor primed</xsl:text>
</xsl:message>
</xsl:if>

  </xsl:when>
  <xsl:when test="document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$myModelName and @epName=$myEPName and @id=$myID]">
    <!-- basic set notation check -->
    <!-- Need additional checks here? e.g. first in set is not an in? -->
  </xsl:when>
  <xsl:otherwise>
    <xsl:message terminate="no"><xsl:text>Error, '</xsl:text><xsl:value-of select="@direction"/><xsl:text>' data, id '</xsl:text><xsl:value-of select="$myID"/><xsl:text>' in ep '</xsl:text><xsl:value-of select="$myEPName"/><xsl:text>' in model '</xsl:text><xsl:value-of select="$myModelName"/><xsl:text>' is not referenced in the composition</xsl:text></xsl:message>
  </xsl:otherwise>
</xsl:choose>

</xsl:for-each>

</xsl:template>
</xsl:stylesheet>
