<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 30th Nov 2005
    PrimingAndSequencing: check that only the 'first'
    data in a coupled sequence is primed.
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that primings are valid
</xsl:text>
</xsl:if>

<!-- first loop throught all init sequences -->
<xsl:if test="$verbose='yes'">
<xsl:text>Check init sequences not yet implemented
</xsl:text>
</xsl:if>

<!-- next loop throught all finalise sequences -->
<xsl:if test="$verbose='yes'">
<xsl:text>Check finalise sequences not yet implemented
</xsl:text>
</xsl:if>

<!-- finally loop through all ts sequences -->
<xsl:if test="$verbose='yes'">
<xsl:text>Checking ts sequences
</xsl:text>
</xsl:if>

<xsl:for-each
select="document(/coupled/deployment)/deployment/sequences/sequence[@type='ts']">
<xsl:if test="$verbose='yes'">
<xsl:text>Sequence </xsl:text>
<xsl:value-of select="position()"/>
<xsl:text>/</xsl:text>
<xsl:value-of select="last()"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:variable name="mySequence" select="."/>

<!-- loop through the sequence of entrypoints -->
<xsl:for-each select="entryPoint">

<xsl:variable name="modelName" select="@modelName"/>
<xsl:variable name="epName" select="."/>
<xsl:variable name="myPosition" select="position()"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Model </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> ep </xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text> position </xsl:text>
<xsl:value-of select="$myPosition"/>
<xsl:text>/</xsl:text>
<xsl:value-of select="last()"/>
<xsl:text>
</xsl:text>
</xsl:if>

<!-- for each of my in's/inout's-->
<xsl:for-each select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[ancestor::definition/name=$modelName and ancestor::entryPoint/@name=$epName and (@direction='in' or @direction='inout')]">

<xsl:variable name="id" select="@id"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Data direction </xsl:text>
<xsl:value-of select="@direction"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="@form"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$id"/>
<xsl:text>
</xsl:text>
</xsl:if>

<!-- if not coupled then primed is already checked in AllInsConnected.xsl-->
<!-- if in or inout and coupled to something after it in sequence then primed -->
<!-- if in or inout and coupled to something before it in sequence then not primed -->
<!-- if not in a sequence then either priming or not priming is valid -->

<xsl:variable name="nCoupled"
select="count(document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[@inID=$id
and ancestor::epChannel/@inEP=$epName and ancestor::modelChannel/@inModel=$modelName])"/>
<xsl:variable name="nPrimed"
select="count(document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed[@id=$id
and ancestor::entryPoint/@name=$epName and ancestor::model/@name=$modelName])"/>

<xsl:if test="$nCoupled=1">

<xsl:variable name="remoteModelName">
<xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[@inID=$id
and ancestor::epChannel/@inEP=$epName and ancestor::modelChannel/@inModel=$modelName]">
<xsl:value-of select="ancestor::modelChannel/@outModel"/>
</xsl:for-each>
</xsl:variable>

<xsl:variable name="remoteEPName">
<xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[@inID=$id
and ancestor::epChannel/@inEP=$epName and ancestor::modelChannel/@inModel=$modelName]">
<xsl:value-of select="ancestor::epChannel/@outEP"/>
</xsl:for-each>
</xsl:variable>

<xsl:variable name="remoteID">
<xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[@inID=$id
and ancestor::epChannel/@inEP=$epName and ancestor::modelChannel/@inModel=$modelName]">
<xsl:value-of select="@outID"/>
</xsl:for-each>
</xsl:variable>

<xsl:if test="$verbose='yes'">
<xsl:text>I am coupled to model </xsl:text>
<xsl:value-of select="$remoteModelName"/>
<xsl:text> ep </xsl:text>
<xsl:value-of select="$remoteEPName"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$remoteID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:variable name="remotePosition">
<xsl:for-each select="$mySequence/entryPoint">
<xsl:if test="@modelName=$remoteModelName and .=$remoteEPName">
<xsl:value-of select="position()"/>
</xsl:if>
</xsl:for-each>
</xsl:variable>

<xsl:choose>
<xsl:when test="$remotePosition=''">

<xsl:if test="$verbose='yes'">
<xsl:text>The remote model has not been found in this sequence
so it may or may not be primed depending on how the simulation
is set up.
</xsl:text>
</xsl:if>

</xsl:when>
<xsl:otherwise>

<xsl:if test="$verbose='yes'">
<xsl:text>The remote model has been found at position </xsl:text>
<xsl:value-of select="$remotePosition"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:choose>

<xsl:when test="$myPosition>$remotePosition">
<!-- coupling is before me so I must not be primed -->
<xsl:if test="$nPrimed=1">
<xsl:message terminate="yes">
<xsl:text>Error: I (m=</xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text>,ep=</xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text>,id=</xsl:text>
<xsl:value-of select="$id"/>
<xsl:text>) am coupled to data (m=</xsl:text>
<xsl:value-of select="$remoteModelName"/>
<xsl:text>,ep=</xsl:text>
<xsl:value-of select="$remoteEPName"/>
<xsl:text>,id=</xsl:text>
<xsl:value-of select="$remoteID"/>
<xsl:text>) that appears before me in a sequence (mypos=</xsl:text>
<xsl:value-of select="$myPosition"/>
<xsl:text>,remotepos=</xsl:text>
<xsl:value-of select="$remotePosition"/>
<xsl:text>) and so I must not be primed</xsl:text>
</xsl:message>
</xsl:if>
</xsl:when>

<xsl:when test="$remotePosition>$myPosition">
<!-- coupling is after me so I must be primed -->
<xsl:if test="$nPrimed=0">
<xsl:message terminate="yes">
<xsl:text>Error: I (m=</xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text>,ep=</xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text>,id=</xsl:text>
<xsl:value-of select="$id"/>
<xsl:text>) am coupled to data (m=</xsl:text>
<xsl:value-of select="$remoteModelName"/>
<xsl:text>,ep=</xsl:text>
<xsl:value-of select="$remoteEPName"/>
<xsl:text>,id=</xsl:text>
<xsl:value-of select="$remoteID"/>
<xsl:text>) that appears after me in a sequence (mypos=</xsl:text>
<xsl:value-of select="$myPosition"/>
<xsl:text>,remotepos=</xsl:text>
<xsl:value-of select="$remotePosition"/>
<xsl:text>) and so I must be primed</xsl:text>
</xsl:message>
</xsl:if>
</xsl:when>

</xsl:choose>

</xsl:otherwise>
</xsl:choose>

</xsl:if>

</xsl:for-each>

</xsl:for-each>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>
