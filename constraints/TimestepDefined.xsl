<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 3rd July 2006
    TimestepDefined: check that a model defines a timestep if and only if the model is a scientific model and it has at least one timestepping entry point
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that a model defines a timestep if and only if required
</xsl:text>
</xsl:if>

<xsl:for-each select="document($root/coupled/models/model)/definition">
<xsl:choose>
<xsl:when test="timestep">
  <xsl:if test="type!='scientific'">
  <xsl:message terminate="no">
    <xsl:text>A timestep is defined for model '</xsl:text>
    <xsl:value-of select="name"/>
    <xsl:text>' but model type is not 'scientific'
</xsl:text>
  </xsl:message>
  </xsl:if>
  <xsl:if test="not(entryPoints/entryPoint[@type='iteration'])">
  <xsl:message terminate="no">
    <xsl:text>A timestep is defined for model '</xsl:text>
    <xsl:value-of select="name"/>
    <xsl:text>' but this model does not have an 'iteration' entry point
</xsl:text>
  </xsl:message>
  </xsl:if>
</xsl:when>
<xsl:otherwise><!-- timestep is not defined -->
  <xsl:if test="type='scientific' and entryPoints/entryPoint[@type='iteration']">
  <xsl:message terminate="no">
    <xsl:text>A timestep is not defined for model '</xsl:text>
    <xsl:value-of select="name"/>
    <xsl:text>' but model type is 'scientific' and it has at least one 'iteration' entry point
</xsl:text>
  </xsl:message>
  </xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:for-each>

</xsl:template>
</xsl:stylesheet>
