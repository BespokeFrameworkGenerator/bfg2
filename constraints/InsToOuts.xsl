<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 16th Dec 2005
    InsToOuts: check that an out isn't connected as an input and that an in isn't connected as an output
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that an out isn't connected as an input and that an in isn't connected as an output
</xsl:text>
</xsl:if>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking in's
</xsl:text>
</xsl:if>
<xsl:for-each
select="document(/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[@direction='in']">
<xsl:variable name="myModelName" select="ancestor::definition/name"/>
<xsl:variable name="myEPName" select="ancestor::entryPoint/@name"/>
<xsl:variable name="myForm" select="ancestor::data/@form"/>
<xsl:variable name="myID" select="@id"/>
<xsl:if test="$verbose='yes'">
<xsl:text>Checking model </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$myForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<!-- Does it prime? -->
<xsl:variable name="myNode" select="document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed/model[@name=$myModelName and @entryPoint=$myEPName and @id=$myID]"/>
<xsl:if test="$myNode">
<xsl:message terminate="no">
<xsl:text>modelName='</xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text>' epName='</xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text>' id='</xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>' is an 'in' but is specified as providing a priming to </xsl:text>
<xsl:text>modelName='</xsl:text>
<xsl:value-of select="$myNode/../../../@name"/>
<xsl:text>' epName='</xsl:text>
<xsl:value-of select="$myNode/../../@name"/>
<xsl:text>' id='</xsl:text>
<xsl:value-of select="$myNode/../@id"/>
<xsl:text>'</xsl:text>
</xsl:message>
</xsl:if>

<!-- Does it provide coupling data? -->
<xsl:variable name="myNode2" select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$myModelName]/epChannel[@outEP=$myEPName]/connection[@outID=$myID]"/>
<xsl:if test="$myNode2">

<xsl:message terminate="no">
<xsl:text>modelName='</xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text>' epName='</xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text>' id='</xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>' is an 'in' but is specified as providing a coupling connection to </xsl:text>
<xsl:text>modelName='</xsl:text>
<xsl:value-of select="$myNode2/../../@inModel"/>
<xsl:text>' epName='</xsl:text>
<xsl:value-of select="$myNode2/../@inEP"/>
<xsl:text>' id='</xsl:text>
<xsl:value-of select="$myNode2/@inID"/>
<xsl:text>'</xsl:text>
</xsl:message>
</xsl:if>

</xsl:for-each>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking out's
</xsl:text>
</xsl:if>
<xsl:for-each
select="document(/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[@direction='out']">
<xsl:variable name="myModelName" select="ancestor::definition/name"/>
<xsl:variable name="myEPName" select="ancestor::entryPoint/@name"/>
<xsl:variable name="myForm" select="ancestor::data/@form"/>
<xsl:variable name="myID" select="@id"/>
<xsl:if test="$verbose='yes'">
<xsl:text>Checking model </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$myForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<!-- for the moment skip if I am a funcret. Need to rewrite this test in python as it is getting too complicated for xslt (or at least my skills in xslt) -->
<xsl:if test="not($myForm='funcret')">

<!-- Is it primed? -->
<xsl:if test="document($root/coupled/composition)/composition/connections/priming/model[@name=$myModelName]/entryPoint[@name=$myEPName]/primed[@id=$myID]">
<xsl:message terminate="no">
<xsl:text>modelName </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> epName </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text> is an out but is primed</xsl:text>
</xsl:message>
</xsl:if>

</xsl:if>

<!-- Does it require coupling data? -->
<xsl:if test="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@inModel=$myModelName]/epChannel[@inEP=$myEPName]/connection[@inID=$myID]">
ERROR
<xsl:message terminate="no">
<xsl:text>modelName </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> epName </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text> is an out but is specified as requiring an input coupling connection from </xsl:text>
<xsl:text>modelName </xsl:text>
<xsl:value-of select="ancestor::modelChannel/@outModel"/>
<xsl:text> epName </xsl:text>
<xsl:value-of select="ancestor::epChannel/@outEP"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="@outID"/>
</xsl:message>
</xsl:if>

</xsl:for-each>

</xsl:template>
</xsl:stylesheet>
