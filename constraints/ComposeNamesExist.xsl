<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 29th Nov 2005
    ComposeNamesExist: check that all model names,
    epnames and id's in the composition actually exist.
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">
<xsl:if test="$verbose='yes'">
<xsl:text>***Checking that all model names, epnames and ids in the composition exist
</xsl:text>
</xsl:if>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking set notation timestepping connections ...
</xsl:text>
</xsl:if>
<xsl:for-each select="document(/coupled/composition)/composition/connections/timestepping/set/field">
<xsl:if test="$verbose='yes'">
<xsl:text>Checking model </xsl:text>
<xsl:value-of select="@modelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="@epName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="@form"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="@id"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:call-template name="tupleDefined">
<xsl:with-param name="modelName" select="@modelName"/>
<xsl:with-param name="epName" select="@epName"/>
<xsl:with-param name="form" select="@form"/>
<xsl:with-param name="direction" select="''"/>
<xsl:with-param name="id" select="@id"/>
</xsl:call-template>

</xsl:for-each>

<!-- check p2p timestepping connections -->
<xsl:if test="$verbose='yes'">
<xsl:text>Checking p2p timestepping connections ...
</xsl:text>
</xsl:if>
<xsl:for-each select="document(/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection">
<xsl:variable name="inModelName" select="ancestor::modelChannel/@inModel"/>
<xsl:variable name="inEPName" select="ancestor::epChannel/@inEP"/>
<xsl:variable name="inForm" select="@inForm"/>
<xsl:variable name="inID" select="@inID"/>
<xsl:variable name="outModelName" select="ancestor::modelChannel/@outModel"/>
<xsl:variable name="outEPName" select="ancestor::epChannel/@outEP"/>
<xsl:variable name="outForm" select="@outForm"/>
<xsl:variable name="outID" select="@outID"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking (in) model </xsl:text>
<xsl:value-of select="$inModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$inEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$inForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$inID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:call-template name="tupleDefined">
<xsl:with-param name="modelName" select="$inModelName"/>
<xsl:with-param name="epName" select="$inEPName"/>
<xsl:with-param name="form" select="$inForm"/>
<xsl:with-param name="direction" select="'in'"/>
<xsl:with-param name="id" select="$inID"/>
</xsl:call-template>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking (out) model </xsl:text>
<xsl:value-of select="$outModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$outEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$outForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$outID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:call-template name="tupleDefined">
<xsl:with-param name="modelName" select="$outModelName"/>
<xsl:with-param name="epName" select="$outEPName"/>
<xsl:with-param name="form" select="$outForm"/>
<xsl:with-param name="direction" select="'out'"/>
<xsl:with-param name="id" select="$outID"/>
</xsl:call-template>

</xsl:for-each>

<!-- next check primed connections -->
<xsl:if test="$verbose='yes'">
<xsl:text>Checking primed connections ...
</xsl:text>

</xsl:if><xsl:for-each select="document(/coupled/composition)/composition/connections/priming/model/entryPoint/primed">
<xsl:variable name="myModelName" select="ancestor::model/@name"/>
<xsl:variable name="myEPName" select="ancestor::entryPoint/@name"/>
<xsl:variable name="myForm" select="@form"/>
<xsl:variable name="myID" select="@id"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking primed model </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$myForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:call-template name="tupleDefined">
<xsl:with-param name="modelName" select="$myModelName"/>
<xsl:with-param name="epName" select="$myEPName"/>
<xsl:with-param name="form" select="$myForm"/>
<xsl:with-param name="direction" select="'in'"/>
<xsl:with-param name="id" select="$myID"/>
</xsl:call-template>

</xsl:for-each>

<!-- finally check priming connections -->
<xsl:if test="$verbose='yes'">
<xsl:text>Checking priming connections ...
</xsl:text>
</xsl:if>

<xsl:for-each select="document(/coupled/composition)/composition/connections/priming/model/entryPoint/primed/model">
<xsl:variable name="myModelName" select="@name"/>
<xsl:variable name="myEPName" select="@entryPoint"/>
<xsl:variable name="myForm" select="@form"/>
<xsl:variable name="myID" select="@id"/>

<xsl:if test="$verbose='yes'">
<xsl:text>Checking priming model </xsl:text>
<xsl:value-of select="$myModelName"/>
<xsl:text> entrypoint </xsl:text>
<xsl:value-of select="$myEPName"/>
<xsl:text> form </xsl:text>
<xsl:value-of select="$myForm"/>
<xsl:text> id </xsl:text>
<xsl:value-of select="$myID"/>
<xsl:text>
</xsl:text>
</xsl:if>

<xsl:call-template name="tupleDefined">
<xsl:with-param name="modelName" select="$myModelName"/>
<xsl:with-param name="epName" select="$myEPName"/>
<xsl:with-param name="form" select="$myForm"/>
<xsl:with-param name="direction" select="'out'"/>
<xsl:with-param name="id" select="$myID"/>
</xsl:call-template>

</xsl:for-each>

</xsl:template>

<xsl:template name="tupleDefined">
<xsl:param name="modelName"/>
<xsl:param name="epName"/>
<xsl:param name="form"/>
<xsl:param name="direction"/>
<xsl:param name="id"/>


<xsl:choose>
<xsl:when test="translate($modelName,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='bfg:'">
  <!-- skip as it is an internal reference -->
</xsl:when>
<xsl:otherwise>

<!-- first check if modelName exists -->
<xsl:variable name="countModelName" select="count(document($root/coupled/models/model)/definition[name=$modelName])"/>

<xsl:if test="$countModelName='0'">
<xsl:message terminate="yes">
<xsl:text>Model name </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> specified in the composition does not exist</xsl:text>
</xsl:message>
</xsl:if>

<xsl:if test="$countModelName>'1'">
<xsl:message terminate="yes">
<xsl:text>Model name </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> specified in the composition is defined more than once</xsl:text>
</xsl:message>
</xsl:if>

<!-- next check if modelName/epName exists -->
<xsl:variable name="countEPName" select="count(document($root/coupled/models/model)/definition/entryPoints/entryPoint[ancestor::definition/name=$modelName and @name=$epName])"/>

<xsl:if test="$countEPName='0'">
<xsl:message terminate="yes">
<xsl:text>EntryPoint name </xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text> in model </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> specified in the composition does not exist</xsl:text>
</xsl:message>
</xsl:if>

<xsl:if test="$countEPName>'1'">
<xsl:message terminate="yes">
<xsl:text>EntryPoint name </xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text> in model </xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text> specified in the composition is defined more than once</xsl:text>
</xsl:message>
</xsl:if>

<xsl:variable name="myDirection">
  <xsl:choose>
    <xsl:when test="$direction='in'">
      <xsl:text>out</xsl:text>
    </xsl:when>
    <xsl:when test="$direction='out'">
      <xsl:text>in</xsl:text>
    </xsl:when>
    <xsl:when test="$direction=''">
      <xsl:text></xsl:text>
      <xsl:message terminate="true">
        <xsl:text>Error, this test is not yet supported</xsl:text>
      </xsl:message>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="true">
        <xsl:text>Error</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<!-- finally check if modelName/epName/id exists -->
<xsl:variable name="countID">
<xsl:choose>
<xsl:when test="$form!=''">
<xsl:value-of select="count(document($root/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[ancestor::definition/name=$modelName and ancestor::entryPoint/@name=$epName and ancestor::data/@form=$form and contains(@direction,$myDirection) and @id=$id])"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="count(document($root/coupled/models/model)/definition/entryPoints/entryPoint/data//scalar[ancestor::definition/name=$modelName and ancestor::entryPoint/@name=$epName and contains(@direction,$myDirection) and @id=$id])"/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:if test="$countID='0'">
<xsl:message terminate="yes">
<xsl:text>ID '</xsl:text>
<xsl:value-of select="$id"/>
<xsl:text>' of direction '*</xsl:text>
<xsl:value-of select="$myDirection"/>
<xsl:text>*' of form '</xsl:text>
<xsl:value-of select="$form"/>
<xsl:text>' in entryPoint '</xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text>' in model '</xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text>' specified in the composition does not exist</xsl:text>
</xsl:message>
</xsl:if>

<xsl:if test="$countID>'1'">
<xsl:message terminate="yes">
<xsl:text>ID '</xsl:text>
<xsl:value-of select="$id"/>
<xsl:text>' of direction '*</xsl:text>
<xsl:value-of select="$myDirection"/>
<xsl:text>*' of form '</xsl:text>
<xsl:value-of select="$form"/>
<xsl:text>' in entryPoint '</xsl:text>
<xsl:value-of select="$epName"/>
<xsl:text>' in model '</xsl:text>
<xsl:value-of select="$modelName"/>
<xsl:text>' specified in the composition is defined more than once</xsl:text>
</xsl:message>
</xsl:if>

</xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>
