<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 11 November 2008
    TargetSpecific: check any Target specific constraints
-->

<xsl:output method="text"/>

<xsl:param name="verbose" select="'yes'"/>
<xsl:variable name="root" select="/"/>

<xsl:template match="/">

<xsl:if test="$verbose='yes'">
<xsl:text>***Checking any target specific constraints
</xsl:text>
</xsl:if>

<xsl:variable name="target" select="document($root/coupled/deployment)/deployment/target"/>

<xsl:choose>
<xsl:when test="$target">

  <xsl:choose>

    <xsl:when test="$target='esmf'">

      <xsl:if test="$verbose='yes'">
      <xsl:text>***Checking that only one DeploymentUnit is specified for ESMF deployment
      </xsl:text>
      </xsl:if>

      <xsl:variable name="NDUs" select="count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit)"/>

      <xsl:choose>

        <xsl:when test="$NDUs='0'">
          <xsl:message terminate="yes">
            <xsl:text>Fatal Internal Error: TargetSpecific.xsl expected to find at least one deployment unit but found none.
            </xsl:text>
          </xsl:message>
        </xsl:when>

        <xsl:when test="$NDUs='1'">

          <!-- test passed, return silently -->

        </xsl:when>

        <xsl:when test="$NDUs>'1'">
          <xsl:message terminate="yes">
            <xsl:text>Error: ESMF only supports one deployment unit. Found </xsl:text>
            <xsl:value-of select="$NDUs"/>
            <xsl:text>
            </xsl:text>
          </xsl:message>
        </xsl:when>

        <xsl:otherwise>
          <xsl:message terminate="yes">
            <xsl:text>Fatal Internal Error: TargetSpecific.xsl expected to find at least one deployment unit but found a negative number.
            </xsl:text>
          </xsl:message>          
        </xsl:otherwise>

      </xsl:choose>

    </xsl:when>

    <xsl:otherwise>

      <xsl:if test="$verbose='yes'">
      <xsl:text>***No target specific constraints specified for </xsl:text>
      <xsl:value-of select="$target"/>
      <xsl:text>
      </xsl:text>
      </xsl:if>

    </xsl:otherwise>

  </xsl:choose>

</xsl:when>
<xsl:otherwise>

  <xsl:if test="$verbose='yes'">
  <xsl:text>***No target specified in this Deployment
  </xsl:text>
  </xsl:if>

</xsl:otherwise>
</xsl:choose>

</xsl:template>
</xsl:stylesheet>
