#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue
import f77tof90inplaceinterfacegen
import ctof90inplaceinterfacegen
import pytof90inplaceinterfacegen
import ctof77programconformanceinterfacegen
import f77tof90programconformanceinterfacegen
import f90toccontrolinterfacegen
import f90topycontrolinterfacegen
import f90topyprograminterfacegen

def genAll(coupledDocInfo,BFG2ROOT):
  print
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
  for deploymentUnitInfo in deploymentUnits :
    genDU(deploymentUnitInfo,BFG2ROOT)
  return

def genDU(deploymentUnitInfo,BFG2ROOT):

    # first determine what interface code is required (if any)
    languageMappingRequired=[]
    # what is the base language for this du
    duLanguage=deploymentUnitInfo.language
    # what is the language of each model in this du?
    for modelInfo in deploymentUnitInfo.getModels() :
       modelLanguage=modelInfo.language
       if modelLanguage!=duLanguage:
         if modelLanguage not in languageMappingRequired:
           languageMappingRequired.append(modelLanguage)

    # the required interface code is now stored in languageMappingRequired

    # for each interface code that this du requires
    for language in languageMappingRequired:
      print "Deployment Unit",deploymentUnitInfo.getID(),"requires interfacing from",language,"to",duLanguage

      assert duLanguage in ["f90"], "Error, deployment language must be f90"
# [PS]	Added python...
      assert language in ["f77","c","python"]," Error, we only support [f77,c,python]-to-f90 mapping"

      if language=="f77" and duLanguage=="f90":
        if deploymentUnitInfo.programConformance():
          f77tof90programconformanceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
        if deploymentUnitInfo.requiresInPlace():
          f77tof90inplaceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
        # f90 to f77 control interface (component conformance) is sorted out
        # at the control code generation phase

# [PS]	Requires equivalent elif for language="python"...
      elif language=="c" and duLanguage=="f90":
        if deploymentUnitInfo.requiresInPlace():
          ctof90inplaceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
        if deploymentUnitInfo.programConformance():
          ctof77programconformanceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
          f77tof90programconformanceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
        else: # component conformance
          f90toccontrolinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
# [PS]	Added elif for python case...
      elif language == "python" and duLanguage == "f90":
        if deploymentUnitInfo.requiresInPlace():
          pytof90inplaceinterfacegen.run(deploymentUnitInfo,BFG2ROOT)
          #assert False, "No deploymentUnitInfo.requiresInPlace() option is defined for Python"
        if deploymentUnitInfo.programConformance():
          f90topyprograminterfacegen.run(deploymentUnitInfo,BFG2ROOT)
        else:
          f90topycontrolinterfacegen.run(deploymentUnitInfo,BFG2ROOT)

      else:
        assert False,"Should not reach here"
