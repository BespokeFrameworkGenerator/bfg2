import sys
import os
from lxml import etree as ET
from copy import deepcopy

class GenerationError(Exception):
    def __init__(self, value):
        self.value = "Generation Error: "+value
    def __str__(self):
        return repr(self.value)

def runPhase(xsltdir,OutDirNoQuote,et_result,options,phase,index,xsldoc,xmldoc,args,writeFile):

  outFile=os.path.join(OutDirNoQuote,phase+"Phase"+index+"Result.xml")
  if xsldoc :
    xslfile=None
  else :
    xslfile=os.path.join(xsltdir,phase+"/"+index+"/"+phase+"Phase"+index+".xsl")
  if not xmldoc :
    xmldoc=et_result

  if options.verbose:
    print "["+index+"]",
    sys.stdout.flush()
  et_result=xmlprocess(xsldoc=xsldoc,xslfile=xslfile,
                       xmldoc=xmldoc,
                       args=args)
  if options.keep and writeFile:
    et_result.write(outFile,pretty_print=True)

  return et_result

def xmlread(xmlfile=None) :
  try:
    et_doc = ET.parse(xmlfile)
    return et_doc
  except ET.XMLSyntaxError,e:
    print
    print "XML syntax error in the input document, aborting"
    log = e.error_log.filter_from_level(ET.ErrorLevels.FATAL)
    print(log)
    raise e
  except Exception, e :
    raise e

def xmlprocess(xslfile=None,xsldoc=None,xmlfile=None,xmldoc=None,args={}) :
  try:
    if xsldoc :
      et_styledoc=xsldoc
    else :
      et_styledoc=ET.parse(xslfile)
    et_style = ET.XSLT(et_styledoc)
    if xmldoc :
      et_doc=xmldoc
    else :
      et_doc = ET.parse(xmlfile)
    et_result = et_style(et_doc,**args)
    return et_result
  except ET.XMLSyntaxError,e:
    print
    print "XML syntax error in one of the input documents for ???.xsl, aborting"
    log = e.error_log.filter_from_level(ET.ErrorLevels.FATAL)
    print(log)
    print
    sys.exit(1)
  except Exception, e :
    print
    print "Error generated in ",xslfile,", aborting"
    print e
    print
    sys.exit(1)

def getEPCallXML(epInfo,BFG2ROOT,epName=None):

  templatedir = os.path.join(BFG2ROOT,"templates")
  programTemplate=xmlread(os.path.join(templatedir,"fortran90_control.xml"))
  modelLanguage=epInfo.getModelInfo().language

  if epName:
    uniqueEPName=epName
  elif modelLanguage=="f77":
    # no need to rename for f77
    uniqueEPName=epInfo.getName()
  else:
    uniqueEPName=epInfo.getUniqueName()

  # add our arguments
  epArgsList=epInfo.getArgsList()
  # now append any funcret arguments to the end of our argument list.
  epFuncRetsList=epInfo.getFuncRetsList()
  # If there are funcrets defined then check the language is Python as funcret is only supported in python at the moment.
  assert (len(epFuncRetsList)==0 or (len(epFuncRetsList)>0 and epInfo.getModelInfo().language=="python")), "Error, if funcrets are defined then the language must be python. However, I found model "+epInfo.getModelInfo().name+" with a funcret defined"

  for epFuncRet in epFuncRetsList:
    epArgsList.append(epFuncRet)

  epArgNamesList=[]
  for epArg in epArgsList:
    epArgNamesList.append(epArg.getDataObject().getName())

  epArgs=listString(epArgNamesList)

  content={"epName":uniqueEPName,
           "epArgs":epArgs}
  resultEntryPoint=map(content,programTemplate,snippetName="ep",copy=True)

  return resultEntryPoint

class fileInfo:
  def __init__(self,XMLroot,myField):
    self.__name__="fileInfo"
    self.field=myField
    self.XMLroot=XMLroot
    self.type=XMLroot.get("type")
    assert self.type in ["namelist","netcdf"],"Error, found unsupported file type "+self.type
    self.dataRef=XMLroot.get("dataRef")
    if self.type=="namelist":
      self.nlName=XMLroot.get("nlName")
    else:
      self.nmlName=""
    self.name=XMLroot.get("name")
    #TBD
    self.unit="1012"

class dataException(Exception):
  def __init__( self, message ):
    Exception.__init__(self, '{0}'.format(message))

class fieldInfo(object):


  def __str__(self):
    print "class fieldInfo in bfgutils.py"
    print "instance info ..."
    print "  Parent ModelName '"+self.epInfo.modelInfo.name+"'"
    print "  Parent EPName '"+self.epInfo.epName+"'"
    print "  id '"+self.fieldID+"'"
    print "  direction '"+self.direction+"'"
    print "  form '"+self.form+"'"
    print "  requiresInPlace '"+str(self.requiresInPlaceValue)+"'"
    return ""

  def __init__(self,XMLroot,epParent,debug=False):
    self.verbose=False
    self.__name__="fieldInfo"
    self.XMLroot=XMLroot
    self.form=XMLroot.get("form")

    isScalar=XMLroot.find("scalar")
    isArray=XMLroot.find("array")

    self.arrayDataType=None
    if isArray is not None: # I am an array
      self.dimension=isArray.get("dimension")
      self.arrayDataType=isArray.get("dataType")
      scalar=isArray.find("scalar")
    elif isScalar is not None: # I am a scalar
      self.dimension='0'
      scalar=isScalar
    else: # I am an unsupported type
      print "Error, in class fieldInfo in bfgutils.py. Scalars and arrays only supported so far"
      exit(1)

    self.dataType=scalar.get("dataType")
    self.normDataType=self.dataType.replace(" ","_").replace("*","")
    self.fieldID=scalar.get("id")
    self.direction=scalar.get("direction")
    self.fieldName=scalar.get("name")

    # support strings
    stringSizeList=[child for child in scalar.iterchildren(tag='stringSize')]
    assert (len(stringSizeList)<2), "Error"
    self.stringSize=1
    self.stringSizeExists=False
    if len(stringSizeList)==1:
      self.stringSizeExists=True
      stringSize=stringSizeList[0]
      #only support integers for the moment
      integerList=[child for child in stringSize.iterchildren(tag='integer')]
      assert len(integerList)==1, "Error, we only support stringsizes that are defined as integers at the moment, please hassle us."
      self.stringSize=integerList[0].text

    if self.dimension=='0':
      self.knownSize=True
      self.size=1
    else: self.knownSize,self.size=self.getFieldSize(isArray)
    self.dimSizes=[]
    if self.knownSize and int(self.dimension)>0:
      self.dimSizes=self.getFieldSizes(isArray)
    self.epInfo=epParent
    self.requiresInPlaceValue=False
    self.requiresInPlaceValid=False
    # check that the datatype is valid for my language
    self.languageDataTypeMap={"f90":["integer","integer*1","integer*2","integer*4","integer*8","real","real*4","real*8","real*16","double precision","logical","logical*1","logical*2","logical*4","logical*8","character","complex","double complex","byte"],
                              "f77":["integer","integer*1","integer*2","integer*4","integer*8","real","real*4","real*8","real*16","double precision","logical","logical*1","logical*2","logical*4","logical*8","character","complex","double complex","byte"],
                              "c"  :["short","int","long","short int","long int","long long","long long int","signed short","unsigned short","signed int","unsigned int","signed long","unsigned long","signed short int","unsigned short int","signed long int","unsigned long int","signed long long","unsigned long long","signed long long int","unsigned long long int","float","double","long double","char","signed char","unsigned char","_Bool"],
                              "python_array" : ["int", "int32", "int64", "float32", "float"],
                              "python_scalar" : ["int", "float", "bool", "str"],
                              "python" : [ "int", "int32", "int64", "float32", "float", "bool", "str" ] }

    # provide a default mapping between languages
    self.py2f={"int":"integer",
               "int32":"integer*4",
               "int64":"integer*8",
               "float32":"real*4",
               "float":"real*8",
               "bool":"logical",
               "str":"character" }

    # perhaps we should not support unsigned?
    self.c2f={"short":"integer*2",
              "short int":"integer*2",
              "signed short":"integer*2",
              "signed short int":"integer*2",
              "unsigned short":"integer*2",
              "unsigned short int":"integer*2",
              #
              "int":"integer",
              "signed int":"integer",
              "unsigned int":"integer",
              #
              "long":"integer*4",
              "long int":"integer*4",
              "signed long":"integer*4",
              "unsigned long":"integer*4",
              "signed long int":"integer*4",
              "unsigned long int":"integer*4",
              #
              "long long":"integer*8",
              "long long int":"integer*8",
              "signed long long":"integer*8",
              "unsigned long long":"integer*8",
              "signed long long int":"integer*8",
              "unsigned long long int":"integer*8",
              #
              "float":"real",
              "double":"real*8",
              "long double":"real*16",
              #
              "char":"character",
              "signed char":"character",
              "unsigned char":"character",
              #
              "_Bool":"logical" }

    self.dataObject=None

  def check(self,language,verbose=False):
    ''' model data definition checks that the schema validation is not able to pick up '''

    if verbose:
      print "    checking data name="+str(self.fieldName)+", id="+str(self.fieldID)

    # check language and datatype are consistent
    if not self.dataType in self.languageDataTypeMap[language]:
      raise dataException, self.context("BFG does not support datatype "+self.dataType+" for language "+language+". Supported datatypes are "+str(self.languageDataTypeMap[language]))

    # check stringSize is used correctly (a)
    if self.stringSizeExists and not self.dataType in ["character","str","char","unsigned char","signed char"]:
      raise dataException, self.context("A stringsize only makes sense if you have specified a string type")

    # check stringSize is used correctly (b)
    if not self.stringSizeExists and self.dataType in ["str"]:
      raise dataException, self.context("A stringSize is required if you have specified the 'str' type (see test 1SingleModel/8funcret/5outinall/python for an example)")

    # check numpy.ndarray is used correctly
    if not language=="python" and self.arrayDataType=="numpy.ndarray":
      raise dataException, self.context("It only makes sense to use 'numpy.ndarray' if the language is python")

    # check that funcret is used correctly
    if not language=="python" and self.form=="funcret":
      raise dataException, self.context("funcret is only supported in python")

    if language=="python": # python specific checks

      # check datatypes for arrays are supported
      if self.dataType not in self.languageDataTypeMap["python_array"] and int(self.dimension)>0:
        raise dataException, self.context("BFG does not currently support python arrays of type "+self.dataType+", supported types are "+str(self.languageDataTypeMap["python_array"]))

      # check datatypes for scalars are supported
      if self.dataType not in self.languageDataTypeMap["python_scalar"] and int(self.dimension)==0:
        raise dataException, self.context("BFG does not support python scalars of type "+self.dataType+", supported types are "+str(self.languageDataTypeMap["python_scalar"]))

      # check all python arrays are of type numpy.ndarray
      if int(self.dimension)>0 and not self.arrayDataType=="numpy.ndarray":
        raise dataException, self.context("Python arrays must have an array datatype of 'numpy.ndarray' (see test  1SingleModel/8funcret/6arrays/3outindatatypes/python for an example)")

      # check that funcret is an out
      if self.form=="funcret" and not self.direction=="out":
        raise dataException, self.context("A function return ('funcret') must be specified as an 'out'")

  def context(self,inString):
    return "data[name='"+str(self.fieldName)+"' id='"+str(self.fieldID)+"']: "+inString

  def isPrimed(self):
    if self.form=="inplace":
      compositionInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(self):
        return True
    elif self.form=="argpass" or self.form=="funcret":
      myDataObject=self.getDataObject()
      if myDataObject.isPrimed():
        return True
    else:
      assert False, "Internal error"
    return False

  def primingType(self):
    if self.form=="inplace":
      compositionInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(self):
        return compositionInfo.primingType(self)
    elif self.form=="argpass" or self.form=="funcret":
      myDataObject=self.getDataObject()
      return myDataObject.primingType()
    else:
      assert False, "Internal error"
    raise AssertionError("No priming type found. Please guard with isPrimed() first")

  def primingFileType(self):
    if self.form=="inplace":
      compositionInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(self):
        if compositionInfo.primingType(self)=="file":
          return compositionInfo.primingFileType(self)
    elif self.form=="argpass" or self.form=="funcret":
      myDataObject=self.getDataObject()
      return myDataObject.primingFileType()
    else:
      assert False, "Internal error"
    return AssertionError("No priming file type found. Please guard with primingType()='file' first")

  def getPrimingFileInfo(self):
    if self.form=="inplace":
      compositionInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(self):
        if compositionInfo.primingType(self)=="file":
          return compositionInfo.getPrimingFile(self)
    elif self.form=="argpass" or self.form=="funcret":
      myDataObject=self.getDataObject()
      return myDataObject.getPrimingFileInfo()
    else:
      assert False, "Internal error"
    return AssertionError("No priming file type found. Please guard with primingType()='file' first")

    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        if compositionInfo.primingType(field)=="file":
          return compositionInfo.getPrimingFile(field)
    return AssertionError("No priming namelist file type found. Please guard with primingFileType()=='namelist' first")

  def usesNetcdf(self):

    if self.isPrimed():
      if self.primingType()=="file":
        if self.primingFileType()=="netcdf":
          return True
    return False

  def getLocalName(self):
    if self.fieldName:
      return self.fieldName
    else:
      # create a locally unique name
      return self.getForm()+"_"+self.direction+"_"+self.fieldID

  def getName(self):
    if self.fieldName:
      return self.fieldName+"_"+str(self.getGlobalID())
    else:
      # create a globally unique name
      return self.getForm()+"_"+str(self.getGlobalID())

  # return the dataObject class that creates my space and datatype if I am an argument
  def getDataObject(self):
    if not (self.getForm()=="argpass" or self.getForm()=="funcret"):
      raise AssertionError("Internal error: getDataObject is only valid if data is argpassing or returned from a function")
    if not(self.dataObject):
      # create the required data object using the composition information
      compositionDocInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      self.dataObject=compositionDocInfo.createDataObject(self)
    return self.dataObject

  def setDataObject(self,dataObject):
    self.dataObject=dataObject

  def getID(self):
    return self.fieldID

  def getForm(self):
    return self.form

  def getStringSize(self):
    return self.stringSize

  def getMPIDataType(self,language):
    return "mpi_"+self.getDataType(language).replace(" ","_").replace("*","")

  def getDataType(self,language):
    # try to give the equivalent datatype for the specified language
    self.myLanguage=self.getEPInfo().getModelInfo().language
    assert self.dataType in self.languageDataTypeMap[self.myLanguage], "Error, unknown datatype specified. Found '"+self.dataType+"' but we only support "+str(self.languageDataTypeMap[self.myLanguage])

    print("[myLanguage] {0} language: {1}".format(self.myLanguage, language) )

    if self.myLanguage==language:
      return self.dataType
    if self.myLanguage in ["f77","f90"] and language in ["f77","f90"]:
#      print("[not C] returning: {0}".format(self.dataType))
      return self.dataType

    if self.myLanguage in ["c"] and language in ["f77", "f90"]:
       print("[myLanguage] {0} language: {1}  dataType: {2} returning: {3}".format(self.myLanguage, language, self.dataType, self.c2f[self.dataType]) )
       return self.c2f[self.dataType]

    if self.myLanguage in ["python"] and language in ["f77", "f90"]:
       print("[myLanguage] {0} language: {1}  dataType: {2} returning: {3}".format(self.myLanguage, language, self.dataType, self.py2f[self.dataType]) )
       return self.py2f[self.dataType]

    assert True, "getDataTyoe: mapping not implemented for {0} and {1}".format(language, self.myLanguage)
#    assert self.myLanguage=="c" and language in ["f77","f90"], "Only implemented c to [f77,f90] datatype mapping so far but found "+language+" and "+self.myLanguage

    print("[C] returning: {0}".format(self.c2f[self.dataType]))
#    return self.c2f[self.dataType]
    return self.dataType

  def setRequiresInPlace(self,value,debug=False):
    self.requiresInPlaceValue=value
    self.requiresInPlaceValid=True
    if debug:
      print "data::setRequiresInPlace is called for ",self.printLocation()

  def requiresInPlace(self,debug=False):
    if debug:
      print "data::requiresInPlace is called for ",self.printLocation()
    assert self.requiresInPlaceValid, "Error, I should have been set"
    if debug:
      print "data::requiresInPlace is OK"
    return self.requiresInPlaceValue

  def printLocation(self):
    return "ModelName '"+self.epInfo.modelInfo.name+"' epName '"+self.epInfo.epName+"' id '"+self.fieldID+"'"+"' requiresInPlace '"+str(self.requiresInPlaceValue)+"'"+"' requiresInPlaceValid '"+str(self.requiresInPlaceValid)+"'"

  def getGlobalID(self):
    coupledInfo=self.epInfo.getModelInfo().getCoupledInfo()
    fieldID=coupledInfo.getFieldID(self)
    return fieldID

  def getDirection(self,functionName):
    if "get" in functionName : return "in"
    elif "put" in functionName : return "out"
    else : assert "False", "Internal error: Should not get to here"

  def getGenericFunctionName(self,direction):
    if direction=="in":
      assert self.direction in ["in","inout"],"Error"
      return "get"
    if direction=="out":
      assert self.direction in ["out","inout"],"Error"
      return "put"
    assert False,"I should never get to here"

  def getFunctionName(self,direction):
    if direction=="in":
      assert self.direction in ["in","inout"],"Error"
      return "get"+str(self.normDataType)+"_dim"+str(self.dimension)
    if direction=="out":
      assert self.direction in ["out","inout"],"Error"
      return "put"+str(self.normDataType)+"_dim"+str(self.dimension)
    assert False,"I should never get to here"

  def getUniqueFunctionName(self):
    assert self.direction in "in,out","Error"
    return self.getFunctionName(self.direction)+"_gtag"+str(self.getGlobalID())

  def getEPInfo(self):
    return self.epInfo

  def getFieldSize(self,XMLroot):
    totalSize=1
    myKnownSize=0
    count=0
    for XMLdim in XMLroot.iterchildren(tag='dim'):
      count+=1
      
      # just deal with simple lower/integer and upper/integer for the moment
      myKnownSize,size=self.getDimSize(XMLdim)
      if not myKnownSize:
        self.knownSize=False
        break
      totalSize=totalSize*size
    if count==0:
      print "ERROR: internal logic error in getFieldSize"
      exit(1)
    return myKnownSize,totalSize

  def getFieldSizes(self,XMLroot):
    dims=[]
    for XMLdim in XMLroot.iterchildren(tag='dim'):
      # TBD just deal with simple lower/integer and upper/integer for the moment
      myKnownSize,size=self.getDimSize(XMLdim)
      dims.append(size)
    return dims

  def getInPlaceTagDatatype(self,language=None):
    # this is the *tag* datatype used by put/get

    if language:
      testLanguage=language
    else:
      testLanguage=self.getEPInfo().getModelInfo().language
    if testLanguage=="f90" or testLanguage=="f77":
      return "integer"
    elif testLanguage=="c":
      return "int"
    elif testLanguage == "python":
      return "int"
    else:
      assert False,"Error, unsupported language"

  def arrayDimsSize(self):
    ndims=int(self.dimension)
    if ndims>0 :
      outString="("
      assert len(self.dimSizes)==ndims,"Error"
      count=0
      for dim in self.dimSizes:
        count+=1
        outString+=str(dim)
        if count<ndims: outString+=","
      outString+=")"
    else : outString=""
    return outString

  def f90ArrayDims(self):
    ndims=int(self.dimension)
    if ndims>0 :
      outString="("
      for i in range(0,ndims):
        outString+=":"
        if i<(ndims-1): outString+=","
      outString+=")"
    else : outString=""
    return outString

  def getDimSize(self,XMLdim):
    upperValue=XMLdim.xpath("upper/integer")
    if upperValue: assert len(upperValue)==1
    lowerValue=XMLdim.xpath("lower/integer")
    if lowerValue: assert len(lowerValue)==1
    lowerExists=XMLdim.xpath("lower")
    if not upperValue:
      print "Warning: fieldInfo:getDimSize I have only implemented dim/upper/integer so far. Returning knownSize=False."
      return False,0
    if lowerExists and not lowerValue:
      print "Warning: fieldInfo:getDimSize I have only implemented dim/lower/integer so far. Returning knownSize=False."
      return False,0

    if lowerValue: return True,(int(upperValue[0].text)-int(lowerValue[0].text))+1
    elif upperValue: return True,int(upperValue[0].text)
    else: return False,0

  def genInPlace(self):
    return "      call "+self.getGenericFunctionName(self.direction)+"("+self.getLocalName()+","+self.fieldID+")\n"

  def genDecl(self):
    stringInfo=""
    if self.stringSizeExists:
      stringInfo="(len=*)"
    intent=""
    if self.form=="argpass":
      intent=", intent("+self.direction+")"
    return "       "+self.dataType+stringInfo+intent+" :: "+self.getLocalName()+self.f90ArrayDims()+"\n"

class controlFieldInfo(fieldInfo):
  def __init__(self,myID,myDirection,myForm,epParent,debug=False):

    self.__name__="fieldInfo"
    self.form=myForm
    self.dimension='0'
    self.dataType="logical"
    self.normDataType=self.dataType
    self.fieldID=myID
    self.direction=myDirection
    self.fieldName="not_converged_"+str(myID)
    self.knownSize=True
    self.size=1
    self.dimSizes=[]
    self.epInfo=epParent
    self.requiresInPlaceValue=False
    self.requiresInPlaceValid=False
    self.dataObject=None

  def check(self,language,verbose=False):
    ''' control model data definition checks that the schema validation is not able to pick up '''
    pass

  def getName(self):
    return self.fieldName

  # return the dataObject class that creates my space and datatype if I am an argument
  def getDataObject(self):
    if not self.getForm()=="argpass":
      print "Internal error: getDataObject is only valid if data is argpassing"
      exit(1)
    if not(self.dataObject):
      # create the required data object using the composition information
      compositionDocInfo=self.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      self.dataObject=compositionDocInfo.createDataObject(self)
    return self.dataObject

  def setDataObject(self,dataObject):
    self.dataObject=dataObject

  def getDataType(self,language):
    assert language=="f90", "Only f90 supported for control models"
    # try to give the equivalent datatype for the specified language
    return self.dataType

  def arrayDimsSize(self):
    return ""

class epException(Exception):
  def __init__( self, message ):
    Exception.__init__(self, '{0}'.format(message))

class epInfo:
  epRunTypeLocation='@type'
  # old style BFGtoESMFRunType={"init":"ESMF_SETINIT","iteration":"ESMF_SETRUN","final":"ESMF_SETFINAL"}
  BFGtoESMFRunType={"init":"ESMF_METHOD_INITIALIZE","iteration":"ESMF_METHOD_RUN","final":"ESMF_METHOD_FINALIZE"}

  def __init__(self,XMLroot,parentModelInfo,myID,debug=False):
    self.verbose=False
    if debug: print "entered epInfo:__init__"
    self.XMLroot=XMLroot
    self.epName=XMLroot.get("name")
    self.runType=XMLroot.get("type")
    self.fields=[]
    self.fieldsInfoLookup={}
    for field in XMLroot.iterchildren(tag='data'):
      _myFieldInfo=fieldInfo(field,self)
      self.fieldsInfoLookup[_myFieldInfo.fieldID,_myFieldInfo.form]=_myFieldInfo
      self.fields.append(_myFieldInfo)
    self.modelInfo=parentModelInfo
    self.myID=myID
    self.requiresInPlaceValid=False
    if debug: print "completed epInfo:__init__"

  def check(self,language,verbose=False):
    if verbose:
      print "  checking ep "+self.epName
    try:
      for fieldInfo in self.getFields():
        fieldInfo.check(language,verbose=verbose)
    except dataException, e:
      raise epException, "ep[epName='"+self.epName+"']:"+str(e)

  def getPosition(self,activeEPNames):
    # get the position of this type of entrypoint
    # at the moment we just return the xml position order
    # we only consider active entry point names
    # ***** however, we should really look at the constraints to determine this
    myType=self.runType
    myName=self.epName
    assert myName in activeEPNames, "Error"
    count=0
    for epInfo in self.getModelInfo().getEPs():
      if epInfo.epName in activeEPNames and epInfo.runType==myType:
        count+=1
        if epInfo.epName==myName:
          break
    return count

  def isConvergenceControl(self):
    return False

  def getArgsMap(self):
    argFields={}
    for field in self.fields:
      if field.getForm()=="argpass":
        argFields[int(field.getID())]=field
    return argFields

  def getFuncRetsMap(self):
    funcRetFields={}
    for field in self.fields:
      if field.getForm()=="funcret":
        funcRetFields[int(field.getID())]=field
    return funcRetFields

  def getFuncRetsList(self):
    funcRetMap=self.getFuncRetsMap()
#    print("[ funcRetMap ] {0}".format(funcRetMap))
#    print("[ len(funcRetMap)+1 ] {0}".format(len(funcRetMap)+1))
    funcRetList=[]
    for i in range(1,len(funcRetMap)+1):
      funcRetList.append(funcRetMap[i])
    return funcRetList
    
  def getArgsList(self):
    return [ v for v in self.getArgsMap().values() ]

  def getArgsListString(self):
    result=[]
    for idx,v in enumerate(self.getArgsMap().values()):
      result+=v.getLocalName()
      if not idx==len(self.getArgsMap().values())-1:
        result+=","
    return result

#    argsMap=self.getArgsMap()
#    argsList=[]
    #for iter, arg in enumerate(len(argsMap)):
#    for i in range(1,len(argsMap)+1):
#      argsList.append(argsMap[i])
#    print("len(argsMap) = {0}".format(len(argsMap)))
#    print(argsMap)
    
#    return argsList

  def getArgsFuncsList(self):
    return self.getArgsList()+self.getFuncRetsList()

  def getUniqueName(self):
    # the model name + ep name should be unique
    # it shouldn't clash with any data names as I append
    # a unique id to these (although someone could call
    # an entrypoint with _<int> at the end which could
    # in theory clash
    #if self.getModelInfo().getCoupledInfo().getDeploymentInfo().target=="esmf":
    #  return self.modelInfo.name+"_"+self.epName+"_ESMF"
    #else:
    #  return self.modelInfo.name+"_"+self.epName #+"_"+str(self.getGlobalID())
    return self.modelInfo.name+"_"+self.epName #+"_"+str(self.getGlobalID())

  def getName(self):
    return self.epName

  def getGlobalID(self):
    coupledInfo=self.modelInfo.getCoupledInfo()
    epList=coupledInfo.getDeploymentInfo().getScheduleEPs()
    count=0
    for ep in epList:
      count+=1
      if ep.epName==self.epName and ep.modelName==self.getModelInfo().name:
        return count
    assert False, "Error: Failed to find ep "+self.epName+" in schedule"
    return -1
  def requiresInPlace(self):
    if not self.requiresInPlaceValid:
      # This function provides a summary of my data so if any of my data requires inplace then set me to true
      self.requiresInPlaceValue=False
      for dataInfo in self.getFields():
        if dataInfo.requiresInPlace():
          self.requiresInPlaceValue=True
          break
      self.requiresInPlaceValid=True
    return self.requiresInPlaceValue
  def getID(self):
    return self.myID

  def getFields(self,fieldtype="",direction=""):
    assert fieldtype in ["argpass","funcret","inplace",""], "Error in fieldtype specification."
    assert direction in ["in","out","inout",""], "Error in fieldtype specification."
    if fieldtype=="" and direction=="":
      return self.fields
    if fieldtype=="":
      myfields=self.fields
    else:
      myfields=[]
      for field in self.fields:
        if field.getForm()==fieldtype:
          myfields.append(field)
    if direction=="":
      return myfields
    else:
      myfields2=[]
      for field in myfields:
        if field.direction==direction:
          myfields2.append(field)
      return myfields2

  def fieldExists(self,myID,myForm,directions):
    if not(myForm) and len(directions)==0:
      for field in self.fields:
        if field.fieldID==myID:
          return True
      return False
    elif myForm and len(directions)==0 :
      for field in self.fields:
        if field.fieldID==myID and field.form==myForm:
          return True
      return False
    elif not(myForm) and len(directions)>0:
      for field in self.fields:
        if field.fieldID==myID and field.direction in directions:
          return True
      return False
    elif myForm and len(directions)>0:
      for field in self.fields:
        if field.fieldID==myID and field.form==myForm and field.direction in directions:
          return True
      return False
    else:
      print "Error"
      exit(1)

  def fieldUnique(self,myID,myForm,directions):
    if not(myForm) and len(directions)==0:
      count=0
      for field in self.fields:
        if field.fieldID==myID:
          count+=1
      if count==1:
        return True
      else:
        return False
    elif not(myForm) and len(directions)>0 :
      count=0
      for field in self.fields:
        if field.fieldID==myID and field.direction in directions:
          count+=1
      if count==1:
        return True
      else:
        return False
    elif myForm and len(directions)==0 :
      count=0
      for field in self.fields:
        if field.fieldID==myID and field.form==myForm:
          count+=1
      if count==1:
        return True
      else:
        return False
    elif myForm and len(directions)>0 :
      count=0
      for field in self.fields:
        if field.fieldID==myID and field.form==myForm and field.direction in directions:
          count+=1
      if count==1:
        return True
      else:
        return False
        
  def getField(self,myID,myForm,myDirection):

    if myDirection=="in":
      myDirectionList=["in","inout"]
    elif myDirection=="out":
      myDirectionList=["out","inout"]
    elif myDirection=="inout":
      myDirectionList=["inout"]
    else:
      assert False, "Error"

    if not(myForm):
      # check that myID is unique, if so, return this field.
      count=0
      for field in self.fields:
        if field.fieldID==myID and field.direction in myDirectionList:
          count+=1
          fieldFound=field
      assert count==1, "Error, expecting to find one field but found '"+str(count)+"'. id is '"+str(myID)+"' form is '"+str(myForm)+"' model is '"+self.getModelInfo().name+"' ep is '"+self.epName+"'"
      return fieldFound
    else:
      return self.fieldsInfoLookup[myID,myForm]

  def getModelInfo(self):
    return self.modelInfo

  def usesNetcdf(self):
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="netcdf":
              return True
    return False

  def usesNamelist(self):
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      print "LOOKING AT FIELD id "+dataInfo.fieldID+" form "+dataInfo.form+" direction "+dataInfo.direction
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="namelist":
              return True
    return False

  def getNamelistList(self,namelistName):
    namelistListString=""
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="namelist":
              if remoteFileInfo.nlName==namelistName:
                namelistListString+=remoteFileInfo.dataRef+","
    assert namelistListString!="", "Error, there should be at least one match"
    return namelistListString[:len(namelistListString)-1]

  def getNamelistNames(self):
    names=[]
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="namelist":
              if remoteFileInfo.nlName not in names:
                names.append(remoteFileInfo.nlName)
    return names

  def gen(self):
    subroutineTemplate=ET.XML(
"<code>\
    subroutine <Name/>(<Args/>)\n \
      <Use/>\n \
      <Include/>\n \
      implicit none\n\
<Declarations/>\n \
<Gets/>\n \
<Work/>\n \
<Puts/>\n \
    end subroutine <Name/>\n \
</code>")

    declarations=[]
    for field in self.getFields():
      print type(field)
      declarations.append(field.genDecl())
    gets=[]
    for field in self.getFields(fieldtype="inplace",direction="in"):
      print type(field)
      gets.append(field.genInPlace())
    puts=[]
    for field in self.getFields(fieldtype="inplace",direction="out"):
      puts.append(field.genInPlace())
    content={
      "Name":self.epName,
      "Args":self.getArgsListString(),
      "Use":"",
      "Include":"",
      "Declarations":declarations,
      "Gets":gets,
      "Work":"",
      "Puts":puts
    }
    result=map(content,subroutineTemplate)
    return result


class controlEPInfo(epInfo):
  def __init__(self,parentModelInfo,myID,name,debug=False):
    assert name=="converge", "Only convergence control supported so far"
    if debug: print "entered epInfo:__init__"
    self.epName=name
    self.runType="iteration"
    self.fields=[]
    self.fieldsInfoLookup={}
    # look up all instances of type name
    # add a field for each of these
    scheduleXML=parentModelInfo.getCoupledInfo().getDeploymentInfo().getScheduleInfo().getScheduleTypeXML()
    for convergeElement in scheduleXML.xpath("//converge"):
      myID=convergeElement.get("id")
      _myFieldInfo=controlFieldInfo(myID,"in","argpass",self)
      self.fieldsInfoLookup[_myFieldInfo.fieldID,_myFieldInfo.form]=_myFieldInfo
      self.fields.append(_myFieldInfo)
    self.modelInfo=parentModelInfo
    self.myID=myID
    self.requiresInPlaceValid=False
    if debug: print "completed epInfo:__init__"

  def isConvergenceControl(self):
    if self.epName=="converge":
      return True
    else:
      return False

  def getUniqueName(self):
    assert False, "Error, I should not be called"
    return self.epName+"_"+str(self.getGlobalID())

  def getName(self):
    return self.epName

  def getGlobalID(self):
    coupledInfo=self.modelInfo.getCoupledInfo()
    epList=coupledInfo.getDeploymentInfo().getScheduleEPs()
    count=0
    for ep in epList:
      count+=1
      if ep.epName==self.epName and ep.modelName==self.getModelInfo().name:
        return count
    if self.isConvergenceControl():
      # get a list of convergence constructs
      # count until it is this one
      # return "-" the value
      # temporary hack : assumes only one convergence loop is used
      return -1
      
    assert False, "Error: Failed to find ep "+self.epName+" in schedule"
    return -1
  def requiresInPlace(self):
    if not self.requiresInPlaceValid:
      # This function provides a summary of my data so if any of my data requires inplace then set me to true
      self.requiresInPlaceValue=False
      for dataInfo in self.getFields():
        if dataInfo.requiresInPlace():
          self.requiresInPlaceValue=True
          break
      self.requiresInPlaceValid=True
      assert not self.requiresInPlaceValue,"Error, control code should not contain in-place data"
    return self.requiresInPlaceValue
  def getID(self):
    return self.myID
  def getFields(self):
    return self.fields
  def getField(self,myID,myForm,myDirection):

    if myDirection=="in":
      myDirectionList=["in","inout"]
    elif myDirection=="out":
      myDirectionList=["out","inout"]
    elif myDirection=="inout":
      myDirectionList=["inout"]
    else:
      assert False, "Error"

    if not(myForm):
      # check that myID is unique, if so, return this field.
      count=0
      for field in self.fields:
        if field.fieldID==myID and field.direction in myDirectionList:
          count+=1
          fieldFound=field
      assert count==1, "Error, expecting to find one field but found '"+str(count)+"'. id is '"+str(myID)+"' form is '"+str(myForm)+"' model is '"+self.getModelInfo().name+"' ep is '"+self.epName+"'"
      return fieldFound
    else:
      return self.fieldsInfoLookup[myID,myForm]

  def getModelInfo(self):
    return self.modelInfo
  def usesNetcdf(self):
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="netcdf":
              return True
    return False
  def usesNamelist(self):
    assert False, "Error, I should not be called"
    #TBD
    return True
  def getNamelistList(self,namelistName):
    assert False, "Error, I should not be called"
    namelistListString=""
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="namelist":
              if remoteFileInfo.nlName==namelistName:
                namelistListString+=remoteFileInfo.dataRef+","
    assert namelistListString!="", "Error, there should be at least one match"
    return namelistListString[:len(namelistListString)-1]

  def getNamelistNames(self):
    assert False, "Error, I should not be called"
    names=[]
    compositionDocInfo=self.modelInfo.getCoupledInfo().getCompositionInfo()
    for dataInfo in self.fields:
      if dataInfo.direction=="in":
        if compositionDocInfo.putgetPriming(dataInfo,"in"):
          if compositionDocInfo.primingType(dataInfo)=="file":
            remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
            if remoteFileInfo.type=="namelist":
              if remoteFileInfo.nlName not in names:
                names.append(remoteFileInfo.nlName)
    return names

class modelException(Exception):
  def __init__( self, message ):
    Exception.__init__(self, '{0}'.format(message))

class model(object):
  ''' Create new bfg model descriptions.
      Read existing bfg model descriptions from xml.
      Modify bfg model descriptions.
      Output model descriptions to xml.
      Generate model stubs.
      Generate wrapper code.
  '''

  modelType='/definition/type'
  timestep='/definition/timestep'
  timeUnits='/definition/timestep/@units'
  modelLanguageLocation='/definition/language'
  modelNameLocation='/definition/name'
  entryPointLocation='/definition/entryPoints/entryPoint'
  epNameLocation='@name'
  f77String='f77'
  f90String='f90'
  f90ModuleNameLocation='/definition/languageSpecific/fortran/moduleName'

  def __init__(self):
    self.verbose=False
    self.coupledInfo=None
    self.XMLroot=None
    self._name="unset"
    self._language="unset"
    self.entryPoints=[]
    self.epInfoLookup={}
    self.fields=[]
    self._type="unset"
    self._description=""

  def __str__(self):
    mystr="model:"+os.linesep
    mystr+="  name="+self._name+os.linesep
    mystr+="  language="+self._language+os.linesep
    mystr+="  type="+self._type+os.linesep
    mystr+="  description="+self._description
    return mystr

  @property
  def name(self):
    ''' Return the name of the model. '''
    return self._name
  @name.setter
  def name(self,value):
    ''' Set the name of the model. '''
    self._name=value

  @property
  def language(self):
    ''' Return the language the model is written in. '''
    return self._language
  @language.setter
  def language(self,value):
    ''' Set the language the model is written in. '''
    self._language=value

  @property
  def type(self):
    ''' Return the type of the model. '''
    return self._type
  @type.setter
  def type(self,value):
    ''' Set the type of the model. '''
    self._type=value

  @property
  def description(self):
    ''' Return the models textual description. '''
    return self._description
  @description.setter
  def description(self,value):
    ''' Set the models textual description. '''
    self._description=value

  # TBD
  # def writexml(self):
  #  pass
  #def cimgen(self):
  #  raise NotImplementedError("No implementation yet")
  #def wrappergen(self,target="esmf"):
  #  raise NotImplementedError("No implementation yet")

  def readxmlfile(self,filename):
    ''' populate the object from BFG definition xml '''
    try:
      XMLroot=xmlread(filename)
      self.readxml(XMLroot)
    except Exception, e:
      print e

  def readxml(self,XMLroot,myID=0,coupledInfo=None):
    self.verbose=False
    self.coupledInfo=coupledInfo
    self.XMLroot=XMLroot
    self._name=getValue(XMLroot,self.modelNameLocation)
    self._language=getValue(XMLroot,self.modelLanguageLocation)
    self.entryPoints=[]
    self.epInfoLookup={}
    self.fields=[]
    self.myID=myID
    self._type=getValue(XMLroot,self.modelType)
    if locationExists(XMLroot,self.timestep):
      self.timestep=getValue(XMLroot,self.timestep)
    else:
      self.timestep=None
    if locationExists(XMLroot,self.timeUnits):
      self.timeUnits=getValue(XMLroot,self.timeUnits)
    else:
      self.timeUnits=None
    count=0
    for XMLep in self.XMLroot.xpath(self.entryPointLocation):
      _myEPInfo=epInfo(XMLep,self,count)
      # do we need a list of entrypoints, or is the lookup sufficient?
      self.entryPoints.append(_myEPInfo)
      #print _myEPInfo.epName
      self.epInfoLookup[_myEPInfo.epName]=_myEPInfo
      count+=1
    if count!=1:
      self.myProgramConformance=False
    else:
      ep=self.entryPoints[0]
      if ep.runType=="program":
        self.myProgramConformance=True
      else:
        self.myProgramConformance=False

  def check(self,verbose=False):
    ''' Check that this model is valid and complete. '''

    if verbose:
      print "checking model "+self._name

    # perform any ep specific checks
    for epInfo in self.getEPs():
      try:
        epInfo.check(self._language,verbose=verbose)
      except epException, e:
        raise modelException, "model[name='"+self._name+"']:"+str(e)

  def isControlModel(self):
    return False

  def getTimestep(self):
    return self.timestep

  def getTimestepUnits(self):
    return self.timeUnits

  def programConformance(self):
    return self.myProgramConformance

  def getEPListString(self):
    # create my entrypoint list
    EntryPoints=self.entryPoints
    assert len(EntryPoints)>0
    # create a list of entryPoint names
    epNames=""
    for iter, epName in enumerate(self.getEPNames()):
      #epNames+=getValue(entryPoint,epNameLocation)
      epNames+=epName
      if iter != len(self.getEPNames())-1: 
        epNames+=','
    return epNames

  def getCoupledInfo(self):
    return self.coupledInfo

  def getID(self):
    return self.myID

  def getEPNames(self):
    return self.epInfoLookup.keys()

  def getEPs(self):
    return self.entryPoints

  def epNameExists(self,epName):
    return self.epInfoLookup.has_key(epName)

  def getEP(self,epName):
    #print "model is "+self.name
    #print str(self.epInfoLookup)
    return self.epInfoLookup[epName]

  def getField(self,epName,myID,myForm,direction):
    return self.epInfoLookup[epName].getField(myID,myForm,direction)

  def codegen(self):
    ''' Generate code for this model. '''
    code=self.gen("")
    text=ET.tostring(code,method="text")
    # remove any blank lines (including those with white space)
    lines=[]
    for line in text.splitlines():
      stripline=line.strip() # remove any trailing blank space
      if stripline: # ignore blank lines
        lines.append(line)
    return os.linesep.join(lines)

  # at some point subclass definition for a language specific gen()
  def gen(self,BFG2ROOT):
    import os
    from bfgutils import xmlread,map
    from lxml import etree as ET
    
    moduleTemplate=ET.XML(
"<code> \
module <Name/>\n \
!\n \
  <Use/>\n \
  <Include/>\n \
  implicit none\n \
  private\n \
  <Public/>\n \
!\n \
  contains\n \
<Subroutines/>\n \
end module <Name/>\n \
</code>")

    eps=[]
    for ep in self.getEPs():
      eps.append(ep.gen())

    publicInfo=""
    if len(self.getEPs())>0:
      publicInfo="public :: "+self.getEPListString()

    content={
      "Name":self._name,
      "Use":"",
      "Include":"",
      "Public":publicInfo,
      "Subroutines":eps
    }
    return map(content,moduleTemplate)

class controlDefinitionInfo(model):

  def __init__(self,myID,coupledInfo):
    self.coupledInfo=coupledInfo
    self.name="bfg:"
    self.myID=myID
    self._type="control"
    # TBD: hardcoded assumption here that language for all du's is the same and that the language is "f90"
    for deploymentUnit in coupledInfo.getDeploymentInfo().getDeploymentUnitsInfo():
      if deploymentUnit.language!="f90":
        raise "Error, hardcoded assumption in controlDefinitionInfo that the control language for all du's will be f90, but found ",deploymentUnit.language
    self._language="f90"
    self.entryPoints=[]
    self.epInfoLookup={}
    # set up a convergence ep
    count=1
    _myEPInfo=controlEPInfo(self,count,name="converge")
    self.entryPoints.append(_myEPInfo)
    self.epInfoLookup[_myEPInfo.epName]=_myEPInfo

  def isControlModel(self):
    return True

  def getTimestep(self):
    assert False, "Error, not yet implemented"

  def getTimestepUnits(self):
    assert False, "Error, not yet implemented"

  def programConformance(self):
    assert False, "Error, I should not be called"

  def getEPListString(self):
    assert False, "Error, I should not be called"

  def getCoupledInfo(self):
    return self.coupledInfo

  def getID(self):
    return self.myID

  def getEPNames(self):
    return self.epInfoLookup.keys()

  def getEPs(self):
    return self.entryPoints

  def epNameExists(self,epName):
    return self.epInfoLookup.has_key(epName)

  def getEP(self,epName):
    return self.epInfoLookup[epName]

  def getField(self,epName,myID,myForm,direction):
    return self.epInfoLookup[epName].getField(myID,myForm,direction)

class dataObject:
  def __init__(self,dataInfoList,name="",debug=True):
    if debug:
      print "entered dataObject instance"
      print "here is where I should associate this dataObject with all required arguments and name them appropriately"

    # keep our reference to all fields that are associated with our name
    self.dataInfoList=dataInfoList
    # determine my name
    # 1: is it provided by the constructor?
    if name!="":
      if debug:
        print "Name is supplied and is "+name
      self.name=name
      self.field=dataInfoList[0]
    else:
      if debug:
        print "No name supplied so determining my name"
      found=False
      # 2: if we are primed by file then use the file name
      if self.isPrimed():
        if self.primingType()=="file":
          self.field=dataInfoList[0]
          primingFileInfo=self.getPrimingFileInfo()
          self.name=primingFileInfo.dataRef
          found=True
      if not found:
        # 3: name it after the first named data item
        for dataInfo in dataInfoList:
          if dataInfo.getName()!=None and dataInfo.getName()!="":
            self.name=dataInfo.getName()
            self.field=dataInfo
            found=True
            if debug:
              print "Found a name: "+self.name
            break
      if not found:
        # 4: no data items are named so generate a unique name
        # base the name on the global id of the first field in our list
        self.name=dataInfoList[0].form+str(dataInfoList[0].getGlobalID())+"_BFG"
        self.field=dataInfoList[0]
        if debug:
          print "No name found so setting name to first on the list: "+self.name

      # now associate me with all datainfo types
      for dataInfo in dataInfoList:
        if debug:
          print "Associating this object with "+dataInfo.printLocation()
        dataInfo.setDataObject(self)
    
  def getFieldInfo(self):
    return self.field

  def getName(self):
    return self.name

  def getUniqueName(self):
    # should return a unique name here? Simply add on "_"+uniqueID()
    return self.name

  def isPrimed(self):
    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        return True
    return False

  def primingType(self):
    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        return compositionInfo.primingType(field)
    raise AssertionError("No priming type found. Please guard with isPrimed() first")

  def primingData(self):
    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        if compositionInfo.primingType(field)=="data":
          return compositionInfo.primingValue(field)
        raise AssertionError("Priming type is not data. Please guard with primingType() first")
    raise AssertionError("No priming type found. Please guard with isPrimed() first")
    
  def primingFileType(self):
    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        if compositionInfo.primingType(field)=="file":
          return compositionInfo.primingFileType(field)
    return AssertionError("No priming file type found. Please guard with primingType()='file' first")

  def getPrimingFileInfo(self):
    for field in self.dataInfoList:
      compositionInfo=field.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
      if compositionInfo.primed(field):
        if compositionInfo.primingType(field)=="file":
          return compositionInfo.getPrimingFile(field)
    return AssertionError("No priming namelist file type found. Please guard with primingFileType()=='namelist' first")
    
class compositionInfo:
  def __init__(self,XMLroot,coupledInfo,debug=False):
    if debug: print "entered compositionInfo:__init__"
    self.XMLroot=XMLroot
    self.coupledInfo=coupledInfo
    self.setList=[]
    self.p2pList=[]
    self.primingList=[]
    if debug: print "completed compositionInfo:__init__"

  def createDataObject(self,dataInfo):
    # determine all funcret/argpassing data that is coupled to dataInfo by argument passing/funcret and pass these dataobject constructor so they will point to it.
    # WARNING : WE SHOULD BE USING INSTANCES HERE AS DATAINFO DOES NOT SAY WHICH INSTANCE IT IS. THIS WILL NOT WORK WHEN WE HAVE MULTIPLE INSTANCES

    dataInfoList=[]
    dataInfoList.append(dataInfo)

    # make a list of all data that dataInfo is coupled to using argument passing
    # recurse through multiple outputs and any inouts where appropriate
    compositionDocInfo=dataInfo.getEPInfo().getModelInfo().getCoupledInfo().getCompositionInfo()
    self.getConnectedData(dataInfo,compositionDocInfo,dataInfoList)

    myDataObject=dataObject(dataInfoList)
    return myDataObject

  def getConnectedData(self,dataInfo,compositionDocInfo,dataInfoList):
    myConnections=[]
    myPrimedConnections=[]
    if dataInfo.direction in ["in","inout"]:
      # find all models that pass data to me
      myConnections+=compositionDocInfo.connections(dataInfo,"in")
      # check if I am primed
      if compositionDocInfo.primed(dataInfo):
        if compositionDocInfo.primingType(dataInfo)=="model":
          myPrimedConnections+=compositionDocInfo.getPrimingData(dataInfo)
    if dataInfo.direction in ["out","inout"]:
      # find all models that I pass data to
      myConnections+=compositionDocInfo.connections(dataInfo,"out")
      # check if I prime
      if compositionDocInfo.prime(dataInfo):
        myPrimedConnections+=compositionDocInfo.getPrimedData(dataInfo)

    for remoteDataInfo in myConnections:
      connectionType=compositionDocInfo.connectionType(dataInfo,remoteDataInfo)
      print "Connection type is ",connectionType
      if connectionType in ["argpass","funcret"]:
        if remoteDataInfo not in dataInfoList:
          dataInfoList.append(remoteDataInfo)
          self.getConnectedData(remoteDataInfo,compositionDocInfo,dataInfoList)

    for remoteDataInfo in myPrimedConnections:
      connectionType=compositionDocInfo.primingForm(dataInfo,remoteDataInfo)
      if connectionType in ["argpass","funcret"]:
        if remoteDataInfo not in dataInfoList:
          dataInfoList.append(remoteDataInfo)
          self.getConnectedData(remoteDataInfo,compositionDocInfo,dataInfoList)

  def getPrimingFile(self,dataInfo):
    coupledModel=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
    fieldID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    node=self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+fieldID+"\"]/file")
    assert len(node)<2,"Error expecting 0 or 1 value but found "+str(len(node))
    if len(node)==0:
      return []
    else :
      return fileInfo(node[0],dataInfo)
      #remoteDataInfo.append(coupledModel.getDefinitionInfo(remoteModelName).getEP(remoteEPName).getField(remoteFieldID))
      #return remoteDataInfo

  # do I prime something?
  def prime(self,dataInfo):
    if dataInfo.direction=="in" : return False
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    #print "modelname ",modelName," epName ",epName," id ",dataID," Checking if I prime anything"
    if self.XMLroot.xpath("connections/priming/model/entryPoint/primed/model[@name=\""+modelName+"\" and @entryPoint=\""+epName+"\" and @id=\""+dataID+"\"]"):
      return True
    else:
      return False

  # List of data I prime
  def getPrimedData(self,dataInfo):
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    #print "getPrimedData modelName ",modelName," epName ",epName," id ",dataID
    remoteDataInfoList=[]
    for node in self.XMLroot.xpath("connections/priming/model/entryPoint/primed/model[@name=\""+modelName+"\" and @entryPoint=\""+epName+"\" and @id=\""+dataID+"\"]"):
      #print "  HELLO from a node"
      remoteModelName=node.xpath("ancestor::model/@name")
      remoteEPName=node.xpath("ancestor::entryPoint/@name")
      remoteFieldID=node.xpath("ancestor::primed/@id")

      assert len(remoteModelName)==1 and len(remoteEPName)==1 and len(remoteFieldID)==1,"Error"
      # find our remote data
      coupledModel=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
      # HELLO
      #assert False, "Priming from model no longer supported. Specify as a coupling instead"
      #the two lines below will need to be specified
      # @form is not provided in metadata at the moment so line below will return nothing
      remoteForm=node.xpath("ancestor::primed/@form")
      remoteDirection="out"
      remoteDataInfoList.append(coupledModel.getDefinitionInfo(remoteModelName[0]).getEP(remoteEPName[0]).getField(remoteFieldID[0],remoteForm,remoteDirection))
    return remoteDataInfoList

  # am I primed by something?
  def primed(self,dataInfo):
    if dataInfo.direction=="out" : return False
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    # RF: includes instance logic here ... if self.XMLroot.xpath("/connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\" and (not($instance) or @instance=$instance)]/primed[@id=\""+dataid"\"]"):
    if self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+dataID+"\"]"):
      return True
    else:
      return False

  def primingValue(self,dataInfo):
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    valueList=self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+dataID+"\"]/data/@value")
    return valueList[0]

  def primingType(self,dataInfo):
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    primeXMLList=self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+dataID+"\"]")
    assert len(primeXMLList)==1,"Error, found "+str(len(primeXMLList))+" primings"
    if primeXMLList[0].xpath("model"):
      return "model"
    elif primeXMLList[0].xpath("file"):
      return "file"
    elif primeXMLList[0].xpath("data"):
      return "data"
    elif primeXMLList[0].xpath("internal"):
      return "internal"
    else:
      assert False,"Error"

  def primingFileType(self,dataInfo):
    dataID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    primeXMLList=self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+dataID+"\"]")
    assert len(primeXMLList)==1,"Error, found "+str(len(primeXMLList))+" primings"
    if primeXMLList[0].xpath("model"):
      raise "Error in primingFileType. Expected a priming of type file but found 'model'"
      return "model"
    elif primeXMLList[0].xpath("file"):
      return primeXMLList[0].xpath("file")[0].get("type")
    elif primeXMLList[0].xpath("data"):
      raise "Error in primingFileType. Expected a priming of type file but found 'data'"
      return "data"
    elif primeXMLList[0].xpath("internal"):
      raise "Error in primingFileType. Expected a priming of type file but found 'internal'"
      return "internal"
    else:
      assert False,"Error"

  def connected(self,dataInfo,remoteDataInfo,direction):

    fieldID=dataInfo.fieldID
    epName=dataInfo.getEPInfo().epName
    modelName=dataInfo.getEPInfo().getModelInfo().name
    remoteFieldID=remoteDataInfo.fieldID
    remoteEPName=remoteDataInfo.getEPInfo().epName
    remoteModelName=remoteDataInfo.getEPInfo().getModelInfo().name

    assert direction in ["in","out"],"Error"
    if direction=="in" : assert dataInfo.direction in ["in","inout"] and remoteDataInfo.direction in ["out","inout"],"Error, direction="+direction+" dataInfo direction="+dataInfo.direction+" remote data info="+remoteDataInfo.direction
    if direction=="out" : assert dataInfo.direction in ["out","inout"] and remoteDataInfo.direction in ["in","inout"],"Error"

    # is the data connected? if not return False
    if not ((direction=="in" and self.XMLroot.xpath("connections/timestepping/modelChannel/epChannel/connection[ancestor::modelChannel/@inModel=\""+modelName+"\" and ancestor::modelChannel/@outModel=\""+remoteModelName+"\" and ancestor::epChannel/@inEP=\""+epName+"\" and ancestor::epChannel/@outEP=\""+remoteEPName+"\" and @inID=\""+fieldID+"\" and @outID=\""+remoteFieldID+"\"]")) or \
            (direction=="out" and self.XMLroot.xpath("connections/timestepping/modelChannel/epChannel/connection[ancestor::modelChannel/@outModel=\""+modelName+"\" and ancestor::modelChannel/@inModel=\""+remoteModelName+"\" and ancestor::epChannel/@outEP=\""+epName+"\" and ancestor::epChannel/@inEP=\""+remoteEPName+"\" and @outID=\""+fieldID+"\" and @inID=\""+remoteFieldID+"\"]"))):
      return False
    # passed the test so our data is connected
    return True

  def connectionType(self,dataInfo,remoteDataInfo,checkConnected=False,includeControl=False):
    # add buffering as a proper switch when we add support for it
    # actually, when buffering is not true we can get into problems as
    # validation does not currently allow a target when there is one DU
    # yet we need a target if there is a put/get pair when buffering is False.
    # Need to fix the validation check at some point.
    self.buffering=False
    if checkConnected:
      if dataInfo.direction=="inout" and remoteDataInfo.direction=="inout":
        assert self.connected(dataInfo,remoteDataInfo,direction="in") or self.connected(dataInfo,remoteDataInfo,direction="out"),"Error"
      elif dataInfo.direction=="in" or remoteDataInfo.direction=="out":
        assert self.connected(dataInfo,remoteDataInfo,direction="in")
      elif dataInfo.direction=="out" or remoteDataInfo.direction=="in":
        assert self.connected(dataInfo,remoteDataInfo,direction="out")
      else:
        assert False,"Error"

    modelInfo=dataInfo.getEPInfo().getModelInfo()
    modelName=modelInfo.name
    remoteModelInfo=remoteDataInfo.getEPInfo().getModelInfo()
    remoteModelName=remoteModelInfo.name
    deploymentInfo=self.coupledInfo.getDeploymentInfo()
    if includeControl and (modelInfo.isControlModel() or remoteModelInfo.isControlModel()):
      # if we have more than one su then set sameSU to false as
      # we will have to receive data from the model
      # that provides it
      if deploymentInfo.getNumSUs()>1:
        sameSU=False
      else:
        sameSU=True
    elif modelInfo.isControlModel() or remoteModelInfo.isControlModel():
      # control model can argpass to any connected model
      sameSU=True
    else:
      sameSU=(deploymentInfo.getSequenceID(modelName)==deploymentInfo.getSequenceID(remoteModelName))
    if not(sameSU):
      return "putget"
    elif dataInfo.form in ["argpass","funcret"] and remoteDataInfo.form in ["argpass","funcret"]:
      return "argpass"
    elif self.buffering:
      return "buffer"
    else:
      return "putget"

  def primingForm(self,dataInfo,remoteDataInfo,checkConnected=False):
    # add buffering as a proper switch when we add support for it
    self.buffering=False
    #if checkConnected:
    #  if dataInfo.direction=="inout" and remoteDataInfo.direction=="inout":
    #    assert self.connected(dataInfo,remoteDataInfo,direction="in") or self.connected(dataInfo,remoteDataInfo,direction="out"),"Error"
    #  elif dataInfo.direction=="in" or remoteDataInfo.direction=="out":
    #    assert self.connected(dataInfo,remoteDataInfo,direction="in")
    #  elif dataInfo.direction=="out" or remoteDataInfo.direction=="in":
    #    assert self.connected(dataInfo,remoteDataInfo,direction="out")
    #  else:
    #    assert False,"Error"

    modelName=dataInfo.getEPInfo().getModelInfo().name
    remoteModelName=remoteDataInfo.getEPInfo().getModelInfo().name
    deploymentInfo=self.coupledInfo.getDeploymentInfo()
    sameSU=(deploymentInfo.getSequenceID(modelName)==deploymentInfo.getSequenceID(remoteModelName))
    if not(sameSU):
      return "putget"
    elif dataInfo.form=="argpass" and remoteDataInfo.form=="argpass":
      return "argpass"
    elif self.buffering:
      return "buffer"
    else:
      return "putget"

  def getPrimingData(self,dataInfo):
    coupledModel=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
    fieldID=dataInfo.fieldID
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    node=self.XMLroot.xpath("connections/priming/model[@name=\""+modelName+"\"]/entryPoint[@name=\""+epName+"\"]/primed[@id=\""+fieldID+"\"]/model")
    assert len(node)<2,"Error expecting 0 or 1 value but found "+str(len(node))
    if len(node)==0:
      return []
    else :
      remoteModelName=node[0].get("name")
      remoteEPName=node[0].get("entryPoint")
      remoteFieldID=node[0].get("id")
      # HELLO
      #assert False, "Priming from model no longer supported. Specify as a coupling instead"
      #the two lines below will need to be specified
      remoteForm=node[0].get("form")
      remoteDirection="out"
      remoteDataInfo=[]
      remoteDataInfo.append(coupledModel.getDefinitionInfo(remoteModelName).getEP(remoteEPName).getField(remoteFieldID,remoteForm,remoteDirection))
      return remoteDataInfo
    
  def controlConnection(self,dataInfo):
    remoteDataInfoList=self.connections(dataInfo,"out")
    for remoteDataInfo in remoteDataInfoList:
      if remoteDataInfo.getEPInfo().isConvergenceControl():
        return True
    return False

  def putgetConnections(self,dataInfo,direction,nesting=False,includeControl=False):
    remoteDataInfoList=self.connections(dataInfo,direction)
    putgetList=[]
    for remoteDataInfo in remoteDataInfoList:
      if self.nesting(dataInfo,remoteDataInfo,nesting,includeControl):
        if self.connectionType(dataInfo,remoteDataInfo,direction,includeControl)=="putget":
          putgetList.append(remoteDataInfo)
    return putgetList

  def putgetConnections2(self,dataInfo,direction,nesting=False,includeControl=False):
    assert nesting==False, "Nesting not yet supported"
    assert direction=='out', "direction==in not yet supported"
    # when nesting==False:
    #   when direction=='out':
    #     check for connections at the same nesting level or more nested
    remoteDataInfoList=self.connections(dataInfo,direction)
    putgetList=[]
    for remoteDataInfo in remoteDataInfoList:
      if not self.moreNested(dataInfo,remoteDataInfo):
        if self.connectionType(dataInfo,remoteDataInfo,direction,includeControl)=="putget":
          putgetList.append(remoteDataInfo)
    return putgetList

  def connections(self,dataInfo,direction):
    assert direction in ["in","out"],"Error, assumption is that data will be in or out not inout"
    remoteDirection="in"
    if remoteDirection==direction: remoteDirection="out"
    if dataInfo.direction==remoteDirection:
      #print "connections: returning [] as we are looking for a connection from an 'in' or to an 'out'"
      return []
    fieldID=dataInfo.fieldID
    form=dataInfo.form
    epInfo=dataInfo.getEPInfo()
    epName=epInfo.epName
    modelName=epInfo.getModelInfo().name
    nodes=self.XMLroot.xpath("connections/timestepping/modelChannel/epChannel/connection[ancestor::modelChannel/@"+direction+"Model=\""+modelName+"\" and ancestor::epChannel/@"+direction+"EP=\""+epName+"\" and (@"+direction+"Form=\""+form+"\" or not(@"+direction+"Form)) and @"+direction+"ID=\""+fieldID+"\"]")
    # get the details of our remote data connection(s)
    remoteDataInfoList=[]
    for node in nodes :
      remoteModelName=node.xpath("ancestor::modelChannel/@"+remoteDirection+"Model")
      remoteEPName=node.xpath("ancestor::epChannel/@"+remoteDirection+"EP")
      remoteFormList=node.xpath("@"+remoteDirection+"Form")
      remoteFieldID=node.xpath("@"+remoteDirection+"ID")
      assert len(remoteModelName)==1 and len(remoteEPName)==1 and len(remoteFormList)<2 and len(remoteFieldID)==1,"Error"
      if len(remoteFormList)==0 : remoteForm=None
      else : remoteForm=remoteFormList[0]
      
      # find our remote data
      coupledModel=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
      remoteDataInfoList.append(coupledModel.getDefinitionInfo(remoteModelName[0]).getEP(remoteEPName[0]).getField(remoteFieldID[0],remoteForm,remoteDirection))
    return remoteDataInfoList

  # return the primed data I am connected to
  def primings(self,dataInfo,direction):
    assert direction in ["in","out"],"Error"
    if direction=="in":
      return self.getPrimingData(dataInfo)
    else:
      return self.getPrimedData(dataInfo)

  def lessNested(self,dataInfo,remoteDataInfo):
    # returns true if dataInfo is less nested than remoteDataInfo
    scheduleInfo=self.coupledInfo.getDeploymentInfo().getScheduleInfo()
    nestingLevel=scheduleInfo.getNesting(dataInfo)
    remoteNestingLevel=scheduleInfo.getNesting(remoteDataInfo)
    if nestingLevel<remoteNestingLevel:
      return True
    else:
      return False

  def moreNested(self,dataInfo,remoteDataInfo):
    # returns true if dataInfo is more nested than remoteDataInfo
    return self.lessNested(remoteDataInfo,dataInfo)

  def nesting(self,dataInfo,remoteDataInfo,nesting,includeControl=False):
    # if we have a control model and a broadcast is required then always return true
    deploymentInfo=self.coupledInfo.getDeploymentInfo()
    modelInfo=dataInfo.getEPInfo().getModelInfo()
    remoteModelInfo=remoteDataInfo.getEPInfo().getModelInfo()
    #if includeControl and (modelInfo.isControlModel() or remoteModelInfo.isControlModel()):
    #  if deploymentInfo.getNumSUs()>1:
    #    return True

    # If nesting is True return True if dataInfo and remoteDataInfo are at different control nestings
    # If nesting is False return True if dataInfo and remoteDataInfo are at the same control nesting
    # else return False
    # Implementing this by checking if we share the same parent loop id
    scheduleInfo=self.coupledInfo.getDeploymentInfo().getScheduleInfo()
    myParentLoopID,loopType=scheduleInfo.getParentIterationIDAndType(dataInfo)
    remoteParentLoopID,remoteLoopType=scheduleInfo.getParentIterationIDAndType(remoteDataInfo)
    print "parentLoopID,loopType=",myParentLoopID,loopType
    print "remote parentLoopID,loopType=",remoteParentLoopID,remoteLoopType
    if (myParentLoopID==remoteParentLoopID) and (loopType==remoteLoopType): # we are at the same nesting level
      if nesting: return False
      else: return True
    else: # we are at different levels of nesting
      #print dataInfo.printLocation()
      #print remoteDataInfo.printLocation()
      #print "NESTING is set to ",str(nesting)
      if nesting: return True
      else: return False

  def putgetConnection2(self,dataInfo,direction,nesting=False,includeControl=False):
    assert nesting==False, "Nesting not yet supported"
    assert direction=='out', "direction==in not yet supported"
    # when nesting==False:
    #   when direction=='out':
    #     check for connections at the same nesting level or more nested
    print "putgetConnection2 called ..."
    for remoteDataInfo in self.connections(dataInfo,direction):
      print "looking at remote data",remoteDataInfo
      # return true if an out and same nesting or local is less nested
      if not self.moreNested(dataInfo,remoteDataInfo):
        print "nesting test returns true"
        if self.connectionType(dataInfo,remoteDataInfo,direction,includeControl)=="putget":
          print "connection type is putget"
          return True
    return False

  def putgetConnection(self,dataInfo,direction,nesting=False,includeControl=False):
    # only check for connections at the same nesting level when nesting=False
    # otherwise only check for connections at a different nesting
    print "putgetConnection called ..."
    for remoteDataInfo in self.connections(dataInfo,direction):
      print "looking at remote data",remoteDataInfo
      # want to return true if an out and same nesting or more nested
      # want to return true if an in and same nesting or less nested
      if self.nesting(dataInfo,remoteDataInfo,nesting):
        print "nesting test returns true"
        if self.connectionType(dataInfo,remoteDataInfo,direction,includeControl)=="putget":
          print "connection type is putget"
          #if includeControl:
          #  print "putgetConnection called with includeControl=True"
          #  print "Function is returning True"
          #  print dataInfo
          #  assert False, "Aborting"
          return True
    #if includeControl:
    #  print "putgetConnection called with includeControl=True"
    #  print "Function is returning False"
    #  print dataInfo
    #  assert False, "Aborting"
    return False

  def putgetPriming(self,dataInfo,direction):
    
    # Am I primed at all?
    if not self.primed(dataInfo):
      return False

    # putget means a putget priming
    if dataInfo.form=="inplace":
      return True

    # is my priming inplace (from another model)?
    for remoteDataInfo in self.primings(dataInfo,"out"):
      if self.primingForm(dataInfo,remoteDataInfo,direction)=="putget":
        return True

    # is my coupling inplace?
    for remoteDataInfo in self.connections(dataInfo,"in"):
      if self.connectionType(dataInfo,remoteDataInfo,direction)=="putget":
        return True

    return False

  def getSets(self):
    if not self.setList:
      xmlSets=self.XMLroot.xpath("/composition/connections//set")
      for xmlSet in xmlSets:
        self.setList.append(compositionSet(xmlSet,self.coupledInfo))
    return self.setList

  # return all specified point to point couplings
  def getP2P(self):
    if not self.p2pList:
      xmlp2ps=self.XMLroot.xpath("/composition/connections//connection")
      self.p2pList=[]
      for xmlp2p in xmlp2ps:
        self.p2pList.append(compositionP2P(xmlp2p,self.coupledInfo))
    return self.p2pList

  # return all specified primings
  def getPrimedFields(self):
    if not self.primingList:
      xmlPrimings=self.XMLroot.xpath("/composition/priming//primed")
      self.primingList=[]
      for xmlPriming in xmlPrimings:
        myID=XMLroot.get("id")
        myEPNames=XMLroot.xpath("ancestor::entryPoint/@name")
        myModelNames=XMLroot.xpath("ancestor::model/@name")
        assert len(myEPNames)==1 and len(myModelNames)==1, "Error"
        self.primingList.append({"modelName":myModelNames[0],"epName":myEPNames[0],"id":myID})
    return self.p2pList


                            
class compositionP2P:
  def __init__(self,XMLroot,coupledInfo,debug=False):
    self.XMLroot=XMLroot
    self.coupledInfo=coupledInfo
    inModel=XMLroot.xpath("ancestor::modelChannel/@inModel")
    outModel=XMLroot.xpath("ancestor::modelChannel/@outModel")
    inEP=XMLroot.xpath("ancestor::epChannel/@inEP")
    outEP=XMLroot.xpath("ancestor::epChannel/@outEP")
    inForm=XMLroot.get("inForm")
    outForm=XMLroot.get("outForm")
    inID=XMLroot.get("inID")
    outID=XMLroot.get("outID")
    assert len(inModel)==1 and len(outModel)==1 and len(inEP)==1 and len(outEP)==1, "Error"
    modelObjectIn=self.coupledInfo.getDefinitionInfo(inModel[0])
    epObjectIn=modelObjectIn.getEP(inEP[0])
    fieldObjectIn=epObjectIn.getField(inID,inForm,"in")
    modelObjectOut=self.coupledInfo.getDefinitionInfo(outModel[0])
    epObjectOut=modelObjectOut.getEP(outEP[0])
    fieldObjectOut=epObjectOut.getField(outID,outForm,"out")
    self.fieldOut=fieldRef(fieldObjectOut)
    self.fieldIn=fieldRef(fieldObjectIn)
  def getInField(self):
    return self.fieldIn
  def getOutField(self):
    return self.fieldOut

class compositionSet:
  def __init__(self,XMLroot,coupledInfo,debug=False):
    if debug: print "entered compositionSet:__init__"
    self.XMLroot=XMLroot
    self.coupledInfo=coupledInfo
    self.name=XMLroot.get("name")
    self.fields=[]
    for field in XMLroot.iterchildren(tag='field'):
      modelObject=self.coupledInfo.getDefinitionInfo(field.get("modelName"))
      epObject=modelObject.getEP(field.get("epName"))
      fieldObject=epObject.getField(field.get("id"))
      self.fields.append(fieldRef(field.get("modelName"),field.get("epName"),field.get("id"),fieldObject,instance=field.get("instance")))
    self.numFields=len(self.fields)
    if debug: print "Found a set called",self.name,"containing",self.numFields,"fields"
    if debug: print "completed compositionSet:__init__"

  def getFields(self):
    return self.fields

class deploymentUnitInfo:
  def __init__(self,deploymentUnitXML,position,deploymentInfo,debug=False):
    if debug:
      print "entered deploymentUnitInfo:__init__"
      print "deploymentUnitXML ",deploymentUnitXML
    self.deploymentInfo=deploymentInfo
    self.id=position
    self.XMLroot=deploymentUnitXML
    #self.modelNames={}
    self.modelNames=[]
    self.models=[]
    self.language=getValue(self.XMLroot,"@language")
    self.functionNamesMapValid=False
    self.functionInfoValid=False
    self.sequenceUnitsInfo=[]
    self.myRequiresInPlace=None
    self.myUsesNetcdf=None

  def getNumSUs(self):
    return len(self.getSequenceUnitsInfo())

  def getSequenceUnit(self,modelInfo):
    for sequenceUnit in self.getSequenceUnitsInfo():
      if modelInfo.name in sequenceUnit.getModelNames():
        return sequenceUnit
    assert False, "Error model not found"

  def requiresInPlace(self):
    if not self.myRequiresInPlace:
      self.myRequiresInPlace=False
      scheduleInfo=self.getDeploymentInfo().getScheduleInfo()
      for model in self.getModels():
        print "*****************************************"
        print "requiresInPlace looking at model ",model.name
        print "*****************************************"
        for epName in scheduleInfo.getEPNames(model.name):
          epInfo=model.getEP(epName)
          for fieldInfo in epInfo.getFields():
            if fieldInfo.requiresInPlace():
              self.myRequiresInPlace=True
              return self.myRequiresInPlace
    return self.myRequiresInPlace

  def getSequenceUnitsInfo(self):
    if not self.sequenceUnitsInfo:
      self.count=0
      for sequenceUnit in self.XMLroot.xpath("sequenceUnit"):
        self.count+=1
        self.sequenceUnitsInfo.append(sequenceUnitInfo(sequenceUnit,self.count,self))
    return self.sequenceUnitsInfo

  def getDeploymentInfo(self):
    return self.deploymentInfo

  def programConformance(self):
    models=self.getModels()
    if len(models)!=1 : return False
    model=models[0]
    return model.programConformance()

  def setFunctionInfo(self,functionInfo):
    self.functionInfoValid=True
    self.functionInfo=functionInfo

  def getFunctionInfo(self):
    assert self.functionInfoValid, "Error, setFunctionInfo must be called before getFunctionInfo"
    return self.functionInfo

  def setFunctionNamesMap(self,functionNamesMap):
    self.functionNamesMapValid=True
    self.functionNamesMap=functionNamesMap

  def getFunctionNamesMap(self):
    assert self.functionNamesMapValid, "Error, setFunctionNamesMap must be called before getFunctionNamesMap"
    return self.functionNamesMap

  def getUniqueName(self):
    models=self.getModelNames()
    if len(models)==1:
        duName=models[0]
    else:
        duName=""
        count=0
        for model in models:
            count+=1
            duName+=model[:3]
            if count<len(models): duName+="_"
    return duName

  def getID(self):
    return self.id

  def sameDU(self,modelInfo):
    if modelInfo in self.getModels():
      return True
    else:
      return False

  def getModelNames(self):
    if not self.modelNames :
      modelNamesXML=self.XMLroot.xpath("sequenceUnit/model/@name")
      for modelNameXML in modelNamesXML:
        self.modelNames.append(str(modelNameXML))
      convergencesXML=self.XMLroot.xpath("ancestor::deployment/schedule//converge")
      for convergeXML in convergencesXML:
        self.modelNames.append("bfg:")
      
    return self.modelNames

  def getModels(self):
    if not self.models:
      myModelNames=self.getModelNames()
      coupledInfo=self.deploymentInfo.coupledDoc
      for possibleModel in coupledInfo.getDefinitionsInfo():
        if possibleModel.name in myModelNames:
          self.models.append(possibleModel)
    return self.models

  def usesNetcdf(self):
    if not self.myUsesNetcdf:
      self.myUsesNetcdf=False
      scheduleInfo=self.getDeploymentInfo().getScheduleInfo()
      for model in self.getModels():
        for epName in scheduleInfo.getEPNames(model.name):
          epInfo=model.getEP(epName)
          for fieldInfo in epInfo.getFields():
            if not fieldInfo.direction=="out":
              if fieldInfo.usesNetcdf():
                self.myUsesNetcdf=True
                return self.myUsesNetcdf
    return self.myUsesNetcdf

class sequenceUnitInfo:

  def __init__(self,sequenceUnitXML,position,deploymentUnitInfo):
    self.deploymentUnitInfo=deploymentUnitInfo
    self.id=position
    self.XMLroot=sequenceUnitXML
    self.modelNames=[]
    self.models=[]
    self.nThreads=getValue(self.XMLroot,"@threads")

  def getNThreads(self):
    return int(self.nThreads)

  # use this as an offset to get a global thread ID
  def getThreadIndex(self):
    index=0
    cont=True
    for deploymentUnit in self.deploymentUnitInfo.getDeploymentInfo().getDeploymentUnitsInfo():
      for sequenceUnit in deploymentUnit.getSequenceUnitsInfo():
        if self.deploymentUnitInfo.getID()==deploymentUnit.getID() and self.getID()==sequenceUnit.getID():
          cont=False
          break
        else:
          index+=sequenceUnit.getNThreads()
        if not cont: break
      if not cont: break
    return index

  def getID(self):
    return self.id

  def getDeploymentUnitInfo(self):
    return self.deploymentUnitInfo

  def getModelNames(self):
    if not self.modelNames :
      self.modelNames=self.XMLroot.xpath("model/@name")
    return self.modelNames

  def getModels(self):
    if not self.models:
      myModelNames=self.getModelNames()
      coupledInfo=self.getDeploymentUnitInfo().getDeploymentInfo().getCoupledInfo()
      for possibleModel in coupledInfo.getDefinitionsInfo():
        if possibleModel.name in myModelNames:
          self.models.append(possibleModel)
    return self.models

class functionInfo:
  def __init__(self,name,data,intent,tagDatatype,EPMap):
    self.name=name
    self.data=data
    self.intent=intent
    self.tagDatatype=tagDatatype
    self.EPMap=EPMap
  def getEPMap(self):
    return self.EPMap
  def uniqueName(self,epID,tag):
    return self.name+"_ep"+str(epID)+"_tag"+str(tag)

class deploymentDoc:
  def __init__(self,XMLroot,coupledDoc,debug=False):
    if debug: print "entered deploymentDoc:__init__"
    self.XMLroot=XMLroot
    self.coupledDoc=coupledDoc # to set up references to other objects
    self.myDeploymentModelInfoList=[]
    self.myScheduleEPInfoList=[]
    self.myScheduleEPInfoList2=[]
    self.deploymentUnitsInfo=[]
    if self.XMLroot.xpath("target"):
      self.target=getValue(self.XMLroot,"target")
    else:
      self.target=""
    self.myScheduleInfo=None

  #def numScheduleIterators(self):
  #  return self.XMLroot.xpath("count(//schedule//loop)")

  def sameDU(self,modelInfoIn1,modelInfoIn2):
    print "RF**********************************"
    print "model1In ",modelInfoIn1.name
    print "model2In ",modelInfoIn2.name

    if modelInfoIn1.isControlModel():
      # use the associated model
      modelInfo1=self.associatedModel(modelInfoIn1)
    else:
      modelInfo1=modelInfoIn1

    if modelInfoIn2.isControlModel():
      # use the associated model
      modelInfo2=self.associatedModel(modelInfoIn2)
    else:
      modelInfo2=modelInfoIn2

    print "model1 ",modelInfo1.name
    print "model2 ",modelInfo2.name
    for deploymentUnit in self.getDeploymentUnitsInfo():
      print "  Models on current DU are ..."
      for xxx in deploymentUnit.getModelNames():
        print "  ",xxx
      if modelInfo1.name in deploymentUnit.getModelNames():
        print "Found model ",modelInfo1.name," in this DU"
        if modelInfo2.name in deploymentUnit.getModelNames():
          return True
        else:
          # debug
          found=False
          for deploymentUnit in self.getDeploymentUnitsInfo():
            if modelInfo2 in deploymentUnit.getModels():
              found=True
              break
          if not found:
            assert False, "Error, model 2 should be found in deployment"
          return False
    assert False, "Error, model 1 should be found in deployment"
    return True

  def getNumSUs(self):
    nSUs=0
    for deploymentUnit in self.getDeploymentUnitsInfo():
      nSUs+=deploymentUnit.getNumSUs()
    return nSUs

  def getNumDUs(self):
    return len(self.getDeploymentUnitsInfo())

  def getNumThreads(self):
    nThreads=0
    for deploymentUnit in self.getDeploymentUnitsInfo():
      for sequenceUnit in deploymentUnit.getSequenceUnitsInfo():
        nThreads+=sequenceUnit.getNThreads()
    return nThreads

  def getScheduleInfo(self):
    if not self.myScheduleInfo:
      xpathScheduleList=self.XMLroot.xpath("//schedule")
      if not len(xpathScheduleList)==1:
        print "Error, expecting a single schedule element but found "+len(xpathScheduleList)
        exit(1)
      self.myScheduleInfo=scheduleInfo(xpathScheduleList[0],self)
    return self.myScheduleInfo

  def getCoupledInfo(self):
    return self.coupledDoc

  def associatedModel(self,modelInfo,nesting=False):
    if not modelInfo.isControlModel():
      return modelInfo
    else:    
      eps=modelInfo.getEPs()
      assert len(eps)==1, "Error, expecting control model to have one ep"
      ep=eps[0]
      assert ep.isConvergenceControl(), "Error, expecting ep to be of type convergence control"
      dataInfos=ep.getFields()
      assert len(dataInfos)==1,"Error, expecting a single field to be associated with a convergence control"
      dataInfo=dataInfos[0]
      assert dataInfo.getForm()=="argpass", "Error, expecting field to be argument passing"
      assert dataInfo.getDataType("f90")=="logical", "Error, expecting field to be a logical value"
      assert dataInfo.direction=="in", "Error, expecting field to be an in"
      # find the model that is connected to this argument
      myConnections=self.coupledDoc.getCompositionInfo().connections(dataInfo,"in")
      assert len(myConnections)>0 and len(myConnections)<4, "Error, should have a maximum of 3 connections"
      associatedData=None
      found=False
      for remoteDataInfo in myConnections:
        if self.coupledDoc.getCompositionInfo().nesting(dataInfo,remoteDataInfo,nesting):
          print "Found a model with specified nesting"
          found=True
          associatedData=remoteDataInfo
          break
        else:
          print "Found a model at an incorrect nesting level, so skipping"
      assert found and associatedData, "Error, did not find a connection to the convergence loop from directly within the loop"
      return associatedData.getEPInfo().getModelInfo()

  def getSequenceID(self,modelName):
    modelInfo=self.coupledDoc.getDefinitionInfo(modelName)
    if modelInfo.isControlModel():
      assert False, "getSequenceID called for a control model but this makes no sense"
      # I am not directly associated with a sequenceID, however, for performance reasons I will default to the model whose ep passes data to me within the convergence loop.
      associatedModel=self.associatedModel(modelInfo)
      #update model info to point to our associated model
      modelName=associatedModel.name
      #print "Change the name to the model that supplies the data to the control model as the control model effectively 'lives' on the same su"
    _sequenceUnits=self.XMLroot.xpath("/deployment/deploymentUnits/deploymentUnit/sequenceUnit")
    count=0
    for _sequenceUnit in _sequenceUnits:
      count+=1
      if modelName in _sequenceUnit.xpath("model/@name"):
        return count
    assert False, "Error: Failed to find ep "+modelName+" in sequence units"
    return -1

  def getDeploymentUnitsInfo(self):
    if not self.deploymentUnitsInfo:
      self.count=0
      for deploymentUnit in self.XMLroot.xpath("/deployment/deploymentUnits/deploymentUnit"):
        self.count+=1
        self.deploymentUnitsInfo.append(deploymentUnitInfo(deploymentUnit,self.count,self))
    return self.deploymentUnitsInfo

  def getDeploymentModelNames(self):
    # TODO remove hardcoded xpath expression
    return self.XMLroot.xpath("/deployment/deploymentUnits//model/@name")

  def getDeploymentModels(self):
    if not self.myDeploymentModelInfoList:
      # TODO remove hardcoded xpath expression
      _models=self.XMLroot.xpath("/deployment/deploymentUnits//model")
      for _model in _models:
        # TODO remove hardcoded xpath expressions
        _myModelRef=modelRef(self.coupledDoc.getDefinitionInfo(_model.get("name")),instanceID=_model.get("instance"))
        self.myDeploymentModelInfoList.append(_myModelRef)
    return self.myDeploymentModelInfoList

  def getScheduleModelNames(self):
    _models=self.XMLroot.xpath("/deployment/schedule//model")
    _modelNames=[]
    for _model in _models:
      _modelName=_model.get("name")
      if _modelName not in _modelNames:
        _modelNames.append(_modelName)
    return _modelNames

  # this method is only used for the initial consistency checks
  def getScheduleEPAndModelNames(self):
    _result=[]
    _models=self.XMLroot.xpath("/deployment/schedule//model")
    for _model in _models:
      _modelName=_model.get("name")
      _EPName=_model.get("ep")
      _result.append([_modelName,_EPName])
    return _result

  def getScheduleEPs(self):
    if not self.myScheduleEPInfoList:
      # TODO remove hardcoded xpath expression
      _models=self.XMLroot.xpath("/deployment/schedule//model")
      for _model in _models:
        # TODO remove hardcoded xpath expressions
        _myEPRef=epRef(self.coupledDoc.getDefinitionInfo(_model.get("name")).getEP(_model.get("ep")),_model.get("instance"))
        self.myScheduleEPInfoList.append(_myEPRef)
    return self.myScheduleEPInfoList

  def getScheduleEPs2(self):
    if not self.myScheduleEPInfoList2:
      # TODO remove hardcoded xpath expression
      _models=self.XMLroot.xpath("/deployment/schedule//model")
      for _model in _models:
        modelName=_model.get("name")
        epName=_model.get("ep")
        instance=_model.get("instance")
        epInfo=self.getCoupledInfo().getDefinitionInfo(modelName).getEP(epName)
        self.myScheduleEPInfoList2.append(epInfo)
    return self.myScheduleEPInfoList2

class scheduleInfo:
  def __init__(self,XMLroot,deploymentUnitInfo):
    self.myDeploymentUnitInfo=deploymentUnitInfo
    self.XMLroot=XMLroot
    self.nScheduleIterators=None
    # perform some checks on the schedule ...
    unknownTypes=[]
    for scheduleType in self.XMLroot:
        if scheduleType.tag=="bfgiterate":
            break
        else :
            unknownTypes.append(scheduleType.tag)
    if not scheduleType.tag=="bfgiterate":
        print "Error, unsupported schedule found. Supported type is 'bfgiterate'. Found "+str(unknownTypes)
        exit(1)
    self.scheduleTypeXML=scheduleType

  def getScheduleTypeXML(self):
    return self.scheduleTypeXML

  def getDeploymentUnitInfo(self):
    return self.myDeploymentUnitInfo

  def numScheduleIterators(self):
    if not self.nScheduleIterators:
      self.nScheduleIterators=self.XMLroot.xpath("count(.//loop)")
    return self.nScheduleIterators

  def getRate(self,fieldRef):
    modelName=fieldRef.getModelInfo().name
    epName=fieldRef.getEPInfo().epName
    instance=fieldRef.getInstanceID()
    if instance is not None:
      print "ERROR: I am not yet coded to support instances"
      exit(1)
    result=self.XMLroot.xpath("bfgiterate//model[@name='"+modelName+"' and @ep='"+epName+"']/@frequency")
    if len(result)==0:
      rate=1
    else:
      #TBD: make sure only one result returned
      assert len(result)==1, "Error, len result is "+str(len(result))
      rate=int(result[0])
    return rate

  def getFixedLoopID(self,loopXML):
    for index,possible in enumerate(self.XMLroot.xpath("bfgiterate//loop")):
      if loopXML==possible:
        return index+1
    assert False,"Error"
    
  def getNesting(self,dataInfo):
    # first find the ep in the schedule
    epInfo=dataInfo.getEPInfo()
    modelInfo=epInfo.getModelInfo()
    modelName=modelInfo.name
    epName=epInfo.epName
    dataID=dataInfo.getID()
    if modelName=="bfg:":
      resultList=self.XMLroot.xpath("bfgiterate//converge[@id='"+dataID+"']")
      assert len(resultList)==1, "Error: expecting single match for convergence loop in schedule. Model name is "+modelName+" ep name is "+epName+" id is "+dataID
      resultXML=resultList[0]
      # for some reason the following logic is incorrect so I don't need to set count to 1!!!
      # set count to 1 here as the converge nesting level should be the same as the models within it
      # count=1
    else:
      resultList=self.XMLroot.xpath("bfgiterate//model[@name='"+modelName+"' and @ep='"+epName+"']/..")
      assert len(resultList)==1, "Error: presumably there are multiple instances, but I've not yet coded for that. Model name is "+modelName+" ep name is "+epName
      resultXML=resultList[0]

    # now find out how many parents I have
    count=0
    while resultXML.getparent() is not None:
      resultXML=resultXML.getparent()
      count+=1
    print "getNesting : count=",count
    return count

  def getParentIterationIDAndType(self,dataInfo):
    # should I be using a fieldRef instead of dataInfo so I have instance information?
    epInfo=dataInfo.getEPInfo()
    modelInfo=epInfo.getModelInfo()
    modelName=modelInfo.name
    epName=epInfo.epName
    if modelName=="bfg:":
      # I am a convergence loop
      # Should I return my level or my parents?
      # Returning my level
      return int(dataInfo.fieldID),"converge"
    else:
      resultList=self.XMLroot.xpath("bfgiterate//model[@name='"+modelName+"' and @ep='"+epName+"']/..")
      assert len(resultList)==1, "Error: presumably there are multiple instances, but I've not yet coded for that. Model name is "+modelName+" ep name is "+epName
      resultXML=resultList[0]
      if resultXML.tag=="loop":
        print "FOUND A LOOP for data",dataInfo.getID()," in ep ",epName," in model ",modelName
        fixedLoopID=self.getFixedLoopID(resultXML)
        print "Loop ID is ",fixedLoopID
        return int(fixedLoopID),"fixed"
      if resultXML.tag=="converge":
        print "FOUND A CONVERGENCE"
        #assert False, "NOT IMPLEMENTED YET!!!"
        print "Warning, not implemented yet. I will only work with argument passing"
        return 1,"converge"
      else:
        # not contained in a loop
        return 0,None

  def getNormRate(self,fieldRef):
    modelInfo=fieldRef.getModelInfo()
    epInfo=fieldRef.getEPInfo()
    modelName=modelInfo.name
    epName=epInfo.epName
    instance=fieldRef.getInstanceID()
    if instance is not None:
      print "ERROR: I am not yet coded to support instances"
      exit(1)
    if modelInfo.isControlModel():
      if epInfo.isConvergenceControl():
        myID=fieldRef.getFieldInfo().getID()
        locations=self.XMLroot.xpath("bfgiterate//converge[@id='"+str(myID)+"']")
      else:
        assert False, "Unsupported type of control model entry point found. I support convergence models but found "+epName
    else:
      locations=self.XMLroot.xpath("bfgiterate//model[@name='"+modelName+"' and @ep='"+epName+"']")
    assert len(locations)==1, "Error, looking for model "+modelName+" and ep "+epName+". I should match one and only one of these but found "+str(len(locations))
    location=locations[0]
    localRateString=location.get("frequency")
    if localRateString is None : rate=1
    else : rate=int(localRateString)
    parentLoops=location.xpath("ancestor::loop")
    for loop in parentLoops:
      iterations=loop.get("niters")
      rate=rate/float(iterations)
    return rate
      
  def getEPNames(self,modelName):
    epNamesList=[]
    #for model in self.XMLroot.xpath("//model[@name='"+modelName+"']"):
    #print(ET.tostring(self.XMLroot,method="xml",pretty_print=True))
    # some sort of bug in xpath here. I need to specify bfgiterate for
    # it to return what we expect (//model or model both fail).
    for model in self.XMLroot.xpath("bfgiterate//model[@name='"+modelName+"']"):
      epName=model.get("ep")
      assert epName!=None, "Error"
      assert epName!="", "Error"
      epNamesList.append(epName)
    coupledInfo=self.myDeploymentUnitInfo.getCoupledInfo()
    modelInfo=coupledInfo.getDefinitionInfo(modelName)
    if modelInfo.isControlModel():
      for epName in modelInfo.getEPNames():
        epNamesList.append(epName)
    return epNamesList

def listString(names,separator=","):
  outString=""
  count=0
  for name in names:
    count+=1
    outString+=name
    if count<len(names):
      outString+=separator
  return outString

class modelRef(object):
  def __init__(self,modelObject,instanceID=None):
    self.modelName=modelObject.name
    self.modelObject=modelObject
    self.instanceID=instanceID
    self.hasInstanceID=False
    if instanceID: self.hasInstanceID=True
  def getModelInfo(self):
    return self.modelObject
  def hasInstanceID(self):
    return self.hasInstanceID
  def getInstanceID(self):
    return self.instanceID

class epRef(modelRef):
  def __init__(self,epObject,instance=None):
    super(epRef,self).__init__(epObject.getModelInfo(),instance)
    self.epObject=epObject
    self.epName=epObject.epName
  def getModelInfo(self):
    return super(epRef,self).getModelInfo()
  def hasInstanceID(self):
    return super(epRef,self).hasInstanceID()
  def getInstanceID(self):
    return super(epRef,self).getInstanceID()
  def getEPInfo(self):
    return self.epObject

class fieldRef(epRef):
  def __init__(self,fieldObject,instance=None,debug=False):
    if debug: print "entered fieldRef:__init__"
    super(fieldRef,self).__init__(fieldObject.getEPInfo(),instance)
    self.id=fieldObject.getID()
    self.fieldObject=fieldObject
    if debug: print "completed fieldRef:__init__"
  def getModelInfo(self):
    return super(fieldRef,self).getModelInfo()
  def hasInstanceID(self):
    return super(fieldRef,self).hasInstanceID()
  def getInstanceID(self):
    return super(fieldRef,self).getInstanceID()
  def getEPInfo(self):
    return super(fieldRef,self).getEPInfo()
  def getFieldInfo(self):
    return self.fieldObject

class coupledInfo:
  definitionDocsLocation='/coupled/models/model'
  composeDocLocation='/coupled/composition'
  deployDocLocation='/coupled/deployment'
  def __init__(self,XMLin,CoupledDocumentDir,debug=False):
    if debug: print "DEBUG:entered coupledInfo:__init__"
    # is XMLroot a file or an lxml etree
    if isinstance(XMLin, str): # assume it is a file reference
      self.XMLroot=xmlread(os.path.join(CoupledDocumentDir,XMLin))
    else: # assume it is an etree instance
      self.XMLroot=XMLin
    self.CoupledDocumentDir=CoupledDocumentDir
    self.defnDocs=None
    self.defnDocLookup={}
    count=0
    self.setDefinitionsInfo(count)
    if debug:print "DEBUG: set up definition info"
    self.composeDoc=None
    self.setCompositionInfo()
    if debug:print "DEBUG: set up composition info"
    self.deployDoc=None
    self.setDeploymentInfo()
    if debug:print "DEBUG: set up deployment info"
    # need to initialise me after everything else is set up
    self.setControlDefinitionInfo(count)

  def getFieldID(self,dataInfo):
    count=0
    modelsInfo=self.getDefinitionsInfo()
    for modelInfo in modelsInfo:
      for entryPoint in modelInfo.getEPs():
        for data in entryPoint.getFields():
          count+=1
          if dataInfo.fieldID==data.fieldID and dataInfo.getEPInfo().epName==entryPoint.epName and dataInfo.getEPInfo().getModelInfo().name==modelInfo.name :
            return count
    assert False,"Error, I should never get to here"
    return -1

  def getCompositionInfo(self):
    return self.composeDoc

  def getDeploymentInfo(self):
    return self.deployDoc

  def getDefinitionsInfo(self,internal=True):
    # skip control models if internal=False
    if internal:
      return self.defnDocs
    else:
      definitionsInfo=[]
      for doc in self.defnDocs:
        if not doc.isControlModel():
          definitionsInfo.append(doc)
      return definitionsInfo

  def definitionInfoExists(self,modelName):
    return self.defnDocLookup.has_key(modelName)
  
  def getDefinitionInfo(self,modelName):
    if not self.definitionInfoExists(modelName):
      raise Exception("ERROR, model "+modelName+" is not defined in this coupled model")
    return self.defnDocLookup[modelName]

  def getDefinitionModelNames(self):
    return self.defnDocLookup.keys()

  def setCompositionInfo(self):
    composeDocFileList=self.XMLroot.xpath(self.composeDocLocation)
    assert len(composeDocFileList)==1
    composeDocFile=composeDocFileList[0]
    composeXML=xmlread(os.path.join(self.CoupledDocumentDir,composeDocFile.text))
    #print "about to set composition information"
    self.composeDoc=compositionInfo(composeXML,self)
    #print "composition information set complete"

  def setDeploymentInfo(self):
    _deployDocFileList=self.XMLroot.xpath(self.deployDocLocation)
    assert len(_deployDocFileList)==1
    _deployDocFile=_deployDocFileList[0]
    _deployXML=xmlread(os.path.join(self.CoupledDocumentDir,_deployDocFile.text))
    self.deployDoc=deploymentDoc(_deployXML,self)

  def setDefinitionsInfo(self,count):
    _defnDocLocations=self.XMLroot.xpath(self.definitionDocsLocation)
    self.defnDocs=[]
    for _defnDocLocation in _defnDocLocations:
      _defnXML=xmlread(os.path.join(self.CoupledDocumentDir,_defnDocLocation.text))
      _defnDoc=model()
      _defnDoc.readxml(_defnXML,count,self)
      self.defnDocs.append(_defnDoc)
      self.defnDocLookup[_defnDoc.name]=_defnDoc
      count+=1

  def setControlDefinitionInfo(self,count):
    # add in a BFG control model as models can communicate with it
    _defnDoc=controlDefinitionInfo(count,self)
    self.defnDocs.append(_defnDoc)
    self.defnDocLookup[_defnDoc.name]=_defnDoc

def getValue(XMLroot,location) :
  nodes = XMLroot.xpath(location)
  assert len(nodes)==1, "Error expecting to find a single xml location '"+location+"' but found "+str(len(nodes))
  node=nodes[0]
  if type(node).__name__=="_ElementStringResult": # I am an attribute
    return str(node)
  else: # I am an element
    return node.text

#def getNodes(XMLroot,location) :
#  return XMLroot.xpath(location)

def locationExists(XMLroot,location) :
  nodes=XMLroot.xpath(location)
  if len(nodes)==0:
    return False
  else:
    return True

#def getLanguage(DefinitionDoc) :
#  return getValue(DefinitionDoc,modelLanguageLocation)

#def getModelNames(DefinitionDocs) :
#  modelNamesList=[]
#  for definitionDoc in DefinitionDocs :
#    modelNamesList.append(getValue(definitionDoc,modelNameLocation))
#  return modelNamesList

# this name is confusing - change it
#def getModelName(DefinitionDoc) :
#  if getLanguage(DefinitionDoc)==f90String and \
#        locationExists(DefinitionDoc,f90ModuleNameLocation) :
#    return getValue(DefinitionDoc,f90ModuleNameLocation)
#  else :
#    return getValue(DefinitionDoc,modelNameLocation)


#TODO (perhaps?) check there are no elements in the xml that are missing in the mapping.
# simplest way is to add in all mappings to flatmapping as we execute the process function
# then run the following code
#for element in ESMFTemplate.iter():
#  if element.tag!="code" and element.tag!="instance" :
#    assert element.tag in flatmapping, "Error element '"+str(element)+"' is not specified in the mapping"
def map(mapping,XMLroot,snippetName="",debug=False,copy=False,recurse=True,overwrite=False) :
  #import pdb; pdb.set_trace()

  if copy:
    myXMLroot=deepcopy(XMLroot)
  else:
    myXMLroot=XMLroot

  
  if snippetName!="":
    rootList=myXMLroot.xpath("/*/snippet[@name='"+snippetName+"']")
    if len(rootList)!=1:
      print"Error, expecting to find one instance of snippet '"+snippetName+"' but found "+str(len(rootList))
      print str(rootList)
      print(ET.tostring(XMLroot, method="xml",pretty_print=True))
      exit(1)
    myXMLroot=rootList[0]

  for lhs in mapping :
    rhs=mapping[lhs]
    if debug : print "DEBUG",lhs,rhs
    if not recurse:
      elements=list(myXMLroot.iterfind(lhs))
    else:
      elements=list(myXMLroot.iter(lhs))
    # check that mapping elements exist in the xml
    assert len(elements)>0 ,"Error : mapping element \""+lhs+"\" does not exist in template"
    if type(rhs).__name__=='str' : # we have a literal string mapping
      for element in elements :
        if rhs:
          if element.text==None or overwrite:
            element.text=rhs # append the specified value to the match
    elif type(rhs).__name__=='int' : # we have a literal integer mapping
      for element in elements :
        if element.text==None or overwrite:
          element.text=str(rhs) # set the match to the specified value
    elif type(rhs).__name__=='_Element' : # we have some new xml to add
      for element in elements :
          if element.text==None or overwrite:
            element.append(rhs) # append our xml
    elif type(rhs).__name__=='list' : # we have a group of mappings

      for element in elements : # for each xml element in the template that matches

        print("===> element.tag = {0}    len(element) = {1} ".format(element.tag, len(element)))

        if len(element)==1 and element[0].tag=="instance": # this is an instance
          for (counter,listValues) in enumerate(rhs) : # for each mapping value specified for this element
            if not type(listValues).__name__=="dict":
              print "Error, the element has one child, therefore expecting type dict"
              print "element child is '"+element[0].tag+"'"
              print "element content is ..."
              print(ET.tostring(element[0],method="xml",pretty_print=True))
              print "found type '"+str(type(listValues))+"'"
              print "found value '"+str(listValues)+"'"
              assert False
            # make a copy of our instance template
            localElement=deepcopy(element[0])
            # add the copy of the template to the XML
            element.append(localElement)
            # insert the required values in the copy
            map(listValues,localElement)
          # now delete our template as we do not need it anymore.
          element.remove(element[0])
        else: # len(element)!=0:
          for (counter,listValues) in enumerate(rhs) : # for each mapping value specified for this element
            if type(listValues).__name__=='str':
              value=ET.Element("i")
              value.text=listValues
            elif type(listValues).__name__=='_Element':
              value=listValues
            else:
              assert False, "Error, expecting type 'str' or '_Element' but found "+type(listValues).__name__
            for element in elements :
              element.append(value) # append our xml
        #else :
        #  assert False, "Error, expecting the size of the element to be 0 or 1 but found "+str(len(element))+" "+str(element)
       
    else :
      print "Error in map function, found a ",type(rhs),"with name",type(rhs).__name__
      print mapping
      sys.exit(1)
  return myXMLroot

