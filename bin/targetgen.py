#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue, functionInfo
import f90OASIS3TargetGen
import f90ESMFTargetGen

def genAll(coupledDocInfo,BFG2ROOT):
  print
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
  # generate a target routine for each deployment unit
  for deploymentUnitInfo in deploymentUnits :
    genDU(deploymentUnitInfo,BFG2ROOT)


def genDU(deploymentUnitInfo,BFG2ROOT):

    # limit code generation to what we support
    assert deploymentUnitInfo.language=="f90","Only f90 supported so far, but found "+str(deploymentUnitInfo.language)

    target=deploymentUnitInfo.getDeploymentInfo().target
    if target=="oasis3":
      f90OASIS3TargetGen.genDU(deploymentUnitInfo,BFG2ROOT)
    elif target=="esmf":
      f90ESMFTargetGen.genDU(deploymentUnitInfo,BFG2ROOT)
    else:
      assert False, "Error, unexpected target "+target+" found in targetgen.py"
