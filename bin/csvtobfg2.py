#! /usr/bin/env python

# CSV to BFG2 python script
# CNC RF Created on 14th January 2013
import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as ET
#import bfgutils
from bfgutils import map,xmlread
import check
import csv
import re

usage = "usage: %prog [options] input.csv"
version = "1.0alpha"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-d", "--directory", dest="OutDir",
                  help="output generated files to DIR", metavar="DIR")
parser.add_option("-n", "--novalidate",
                  action="store_false", dest="validate", default=True,
                  help="do not validate bfg2 output documents")

(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
wd=os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if options.OutDir and not(os.path.isdir(options.OutDir)):
  print "error, non-existant target directory '%s'" % (options.OutDir)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

if options.OutDir:
  OutDirNoQuote = os.path.abspath(options.OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
else:
  OutDirNoQuote = os.path.abspath(os.path.dirname(CoupledDocumentPathNoQuote))
  OutDir = "'" + OutDirNoQuote + "'"
  
if options.verbose:
  print "CSVtoBFG2 ... autogentastic."
  print "Output dir is",OutDirNoQuote

class CommentedFile:
    def __init__(self, f, commentstring="#"):
        self.f = f
        self.commentstring = commentstring
    def next(self):
        line = self.f.next()
        while line.startswith(self.commentstring):
            line = self.f.next()
        return line
    def __iter__(self):
        return self

def modelFileName(modelName):
  return re.sub('/','-',re.sub(r'\s', '', modelName)+".xml")

class BFG2CoupledDocument:
  def __init__(self):
    self.modelNames=[]
    self.xml=ET.XML(" \
<coupled>\
<models/>\
<composition/>\
<deployment/>\
</coupled>\
    ")
    self.modelPosition=self.xml.find(".//models")
    assert self.modelPosition is not None, "Error"
    self.compositionElement=self.xml.find(".//composition")
    assert self.compositionElement is not None, "Error"
    self.deploymentElement=self.xml.find(".//deployment")
    assert self.deploymentElement is not None, "Error"
  def __str__(self):
    print "Coupled document:"
    for modelName in self.modelNames:
      print "  Model:"
      print "    name:",modelName
      print "    fileName:",modelFileName(modelName)
    return ""
  def addModel(self,modelName):
    if modelName not in self.modelNames and modelName!="":
      self.modelNames.append(modelName)
      ET.SubElement(self.modelPosition, "model").text=modelFileName(modelName)
  def toXML(self):
    return self.xml
  def writeXML(self,fileName="coupled.xml",compositionFileName="compose.xml",deploymentFileName="deploy.xml"):
    self.compositionElement.text=compositionFileName
    self.deploymentElement.text=deploymentFileName
    tree = ET.ElementTree(self.toXML())
    tree.write(fileName,method="xml",pretty_print=True)
  def printXML(self):
    print(ET.tostring(self.toXML(), method="xml",pretty_print=True))

class BFG2CompositionDocument:
  def __init__(self):
    self.connections=[]
    self.xml=ET.XML(' \
<composition>\
<connections>\
<timestepping/>\
</connections>\
</composition>\
    ')
    self.timesteppingElement=self.xml.find(".//timestepping")
    assert self.timesteppingElement is not None, "Error"
  def __str__(self):
    print "Composition document:"
    for connection in self.connections:
      print str(connection)
    return ""
  def addConnection(self,srcModelName,srcModelID,targetModelName,targetModelID):
    if srcModelName!="" and targetModelName!="" and srcModelID!="" and targetModelID!="":
      self.connections.append([srcModelName,srcModelID,targetModelName,targetModelID])
      self.timesteppingElement.append(ET.XML('\
<modelChannel outModel="'+srcModelName+'" inModel="'+targetModelName+'">\
<epChannel outEP="run" inEP="run">\
<connection outID="'+str(srcModelID)+'" inID="'+str(targetModelID)+'"/>\
</epChannel>\
</modelChannel>\
      '))
  def toXML(self):
    return self.xml
  def writeXML(self,fileName="compose.xml"):
    tree = ET.ElementTree(self.toXML())
    tree.write(fileName,method="xml",pretty_print=True)
  def printXML(self):
    print(ET.tostring(self.toXML(), method="xml",pretty_print=True))

class BFG2DeploymentDocument:
  def __init__(self):
    self.modelNames=[]
    self.xml=ET.XML(' \
<deployment>\
<deploymentUnits>\
<deploymentUnit language="f90">\
<sequenceUnit threads="1"/>\
</deploymentUnit>\
</deploymentUnits>\
<schedule>\
<bfgiterate>\
<iterate/>\
</bfgiterate>\
</schedule>\
</deployment>\
    ')
    self.sequenceUnitElement=self.xml.find(".//sequenceUnit")
    assert self.sequenceUnitElement is not None, "Error"
    self.iterateElement=self.xml.find(".//iterate")
    assert self.iterateElement is not None, "Error"
  def __str__(self):
    print "Deployment document:"
    for modelName in self.modelNames:
      print "  modelName: ",modelName
    return ""
  def addModel(self,modelName):
    if modelName not in self.modelNames and modelName!="":
      self.modelNames.append(modelName)
      self.sequenceUnitElement.append(ET.XML('<model name="'+modelName+'"/>'))
      self.iterateElement.append(ET.XML('<model name="'+modelName+'" ep="run"/>'))
  def toXML(self):
    return self.xml
  def writeXML(self,fileName="compose.xml"):
    tree = ET.ElementTree(self.toXML())
    tree.write(fileName,method="xml",pretty_print=True)
  def printXML(self):
    print(ET.tostring(self.toXML(), method="xml",pretty_print=True))

class BFG2DefinitionDocument:
  def __init__(self):
    self.modelName=""
    self.variableIDs={}
    self.xml=ET.XML(' \
<definition>\
<name/>\
<type>scientific</type>\
<language>f90</language>\
<entryPoints>\
<entryPoint type="iteration" name="run"/>\
</entryPoints>\
<timestep units="hours">1</timestep>\
</definition>\
    ')
    self.entryPointPosition=self.xml.find(".//entryPoint")
    assert self.entryPointPosition is not None, "Error"
    self.modelNameElement=self.xml.find(".//name")
    assert self.modelNameElement is not None, "Error"
    self.dataCount=0
  def __str__(self):
    print "Definition document:"
    print "  modelName: "+self.modelName
    return ""
  def addSourceField(self,srcModelName,srcVariableName,direction,cfName,units,description,alwaysOn):
    assert srcModelName!="", "Error"
    self.addModelName(srcModelName)
    self.addVariable(srcVariableName,"out",alwaysOn)
  def addTargetField(self,targetModelName,targetVariableName,direction,cfName,units,description):
    assert targetModelName!="", "Error"
    self.addModelName(targetModelName)
    self.addVariable(targetVariableName,"in")
  def addModelName(self,modelName):
    if self.modelName=="" :
      self.modelName=modelName
      self.modelNameElement.text=modelName
    else:
      assert self.modelName==modelName, "Error, expecting modelName "+self.modelName+" but found "+modelName
  def addVariable(self,variableName,direction,alwaysOn="YES"):
    self.dataCount+=1
    self.variableIDs[variableName]=self.dataCount
    availabilityAttr=""
    if alwaysOn=="NO" and direction=="out":
      availabilityAttr=' availability="AtRisk"'
    self.entryPointPosition.append(ET.XML('\
<data form="argpass"'+availabilityAttr+'>\
<scalar id="'+str(self.dataCount)+'" direction="'+direction+'" dataType="float" name="'+variableName+'"/>\
</data>\
    '))
  def getVariableID(self,variableName):
    return self.variableIDs[variableName]
  def toXML(self):
    return self.xml
  def writeXML(self,baseDir):
    fileName=os.path.join(OutDirNoQuote,modelFileName(self.modelName))
    tree = ET.ElementTree(self.toXML())
    print "fileName="+fileName
    tree.write(fileName,method="xml",pretty_print=True)
  def printXML(self):
    print(ET.tostring(self.toXML(), method="xml",pretty_print=True))
    
class BFG2DefinitionDocuments:
  def __init__(self):
    self.modelNames=[]
    self.docList={}
  def __str__(self):
    for definitionDocument in self.docList.values():
      print str(definitionDocument)
    return ""
  def addSourceField(self,srcModelName,srcVariableName,direction,cfName,units,description,srcAlwaysOn):
    if srcModelName!="":
      definitionDocumentInstance=self.getDocumentInstance(srcModelName)
      definitionDocumentInstance.addSourceField(srcModelName,srcVariableName,direction,cfName,units,description,srcAlwaysOn)
  def addTargetField(self,targetModelName,targetVariableName,direction,cfName,units,description):
    if targetModelName!="":
      definitionDocumentInstance=self.getDocumentInstance(targetModelName)
      definitionDocumentInstance.addTargetField(targetModelName,targetVariableName,direction,cfName,units,description)
  def getDocumentInstance(self,modelName):
    if not modelName in self.modelNames:
      self.modelNames.append(modelName)
      definitionDocumentInstance=BFG2DefinitionDocument()
      self.docList[modelName]=definitionDocumentInstance
    else:
      definitionDocumentInstance=self.docList[modelName]
    return definitionDocumentInstance
  def getVariableID(self,modelName,variableName):
    if modelName=='' : return ""
    assert modelName in self.modelNames, "Error in getVariableID, modelName '"+modelName+"' is not in "+str(self.modelNames)
    definitionDocumentInstance=self.docList[modelName]
    return definitionDocumentInstance.getVariableID(variableName)
  def writeXML(self,baseDir):
    for definitionDocument in self.docList.values():
      definitionDocument.writeXML(baseDir)
  def printXML(self):
    for definitionDocument in self.docList.values():
      definitionDocument.printXML()

BFG2CoupledDocumentInstance=BFG2CoupledDocument()
BFG2DefinitionDocumentsInstance=BFG2DefinitionDocuments()
BFG2CompositionDocumentInstance=BFG2CompositionDocument()
BFG2DeploymentDocumentInstance=BFG2DeploymentDocument()

reader = csv.reader(CommentedFile(f))
lineNumber=0
for row in reader:
  lineNumber+=1
  assert len(row)==19, "Error in CSV file format, Expected 19 entries per line but found "+str(len(row))+" in line '"+str(row)+"'"
  section=row[0]
  item=row[1]
  form=row[2] # diagnostic or prognostic
  description=row[3]
  srcModelName=row[4]
  srcVariableName=row[5]
  targetModelName=row[6]
  targetVariableName=row[7]
  direction=row[8] # in,out,inout
  cfName=row[9]
  units=row[10]
  srcAlwaysOn=row[11]
  # last 7 entries unused at the moment
  if cfName!="":
    name=cfName
  else:
    name=targetVariableName

  BFG2CoupledDocumentInstance.addModel(srcModelName)
  BFG2CoupledDocumentInstance.addModel(targetModelName)
  BFG2DefinitionDocumentsInstance.addSourceField(srcModelName,srcVariableName,direction,cfName,units,description,srcAlwaysOn)
  BFG2DefinitionDocumentsInstance.addTargetField(targetModelName,targetVariableName,direction,cfName,units,description)
  srcVariableID=BFG2DefinitionDocumentsInstance.getVariableID(srcModelName,srcVariableName)
  targetVariableID=BFG2DefinitionDocumentsInstance.getVariableID(targetModelName,targetVariableName)
  BFG2CompositionDocumentInstance.addConnection(srcModelName,srcVariableID,targetModelName,targetVariableID)
  BFG2DeploymentDocumentInstance.addModel(srcModelName)
  BFG2DeploymentDocumentInstance.addModel(targetModelName)

f.close()

coupledFileName=os.path.join(OutDirNoQuote,"CoupledGen.xml")
compositionFileName=os.path.join(OutDirNoQuote,"composition.xml")
deploymentFileName=os.path.join(OutDirNoQuote,"deployment.xml")
BFG2CoupledDocumentInstance.writeXML(fileName=coupledFileName,\
                                     compositionFileName=compositionFileName,\
                                     deploymentFileName=deploymentFileName)
BFG2DefinitionDocumentsInstance.writeXML(OutDirNoQuote) # fileName is derived from modelName
BFG2CompositionDocumentInstance.writeXML(fileName=compositionFileName)
BFG2DeploymentDocumentInstance.writeXML(fileName=deploymentFileName)
# TBD
#print str(BFG2DeploymentDocumentInstance)

if options.validate:
  if options.verbose:
      print "Checking that the generated BFG2 xml is valid and consistent..."
  check.run(os.path.join(OutDirNoQuote,coupledFileName))
  if options.verbose:
      print "Done"
      print ""

if options.verbose:
  print "CSVtoBFG2 has left the building."
