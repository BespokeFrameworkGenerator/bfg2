import os
import bfgutils
from bfgutils import map as BFGmap


def run(deploymentUnitInfo,BFG2ROOT):

  templatedir = os.path.join(BFG2ROOT,"templates")

  # Read in our XML template
  controlTemplate = bfgutils.xmlread(os.path.join(templatedir,"fortran90_to_python_main.xml"))

  scheduleInfo = deploymentUnitInfo.getDeploymentInfo().getScheduleInfo()
  epInfoList = []
#  for modelInfo in [x for x in deploymentUnitInfo.getModels() if x.language == "python" and not x.programConformance() ]:
  for modelInfo in [x for x in deploymentUnitInfo.getModels() if x.language == "python"]:
      print("[MODELINFO] in {0}\n{1}\n[/MODELINFO]".format(modelInfo.__module__, dir(modelInfo)))
#      if modelInfo.language=="python":
#          if not modelInfo.programConformance():
      for epName in scheduleInfo.getEPNames(modelInfo.name):
          epInfo = modelInfo.getEP(epName)
          uniqueEPName = epInfo.getUniqueName()
          epInfoList.append( { "pyModule" : modelInfo.name })
#                               "pyMethod" : epName } )
#                               "epNamef90": f90toclink(uniqueEPName) } )

#                  epInfoList.append({"epNamef90":f90toclink(uniqueEPName),
#                                 "epArgsDefns":"",
#                                 "epNamec":epName,
#                                 "epArgs":""})

  mapping = {"code" : epInfoList}
  result = BFGmap(mapping,controlTemplate)
  result.write(modelInfo.name + ".c", method = "text")
#  result.write("BFG2Control"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")


def f90toclink(name):
    # assume that we add an underscore
    # need to add other options that can be specified by the user
    return name+"_"
