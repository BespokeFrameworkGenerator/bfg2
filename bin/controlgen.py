import f90controlgen

def genAll(coupledDocInfo,BFG2ROOT):
    # generate an inplace routine for each deployment unit
    deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
    deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
    for deploymentUnitInfo in deploymentUnits :
        genDU(deploymentUnitInfo,BFG2ROOT)

def genDU(deploymentUnitInfo,BFG2ROOT):

    # control code not required if program conformance specified
    if deploymentUnitInfo.programConformance() : return

    # determine the requested control language
    duLanguage=deploymentUnitInfo.language

    # current supported languages
    supportedDULanguages=["f90"]

    # check to see if the requested language is supported
    assert duLanguage in supportedDULanguages, "Error, deployment language must be one of "+supportedDULanguages

    # call our language specific control code generation routine
    if duLanguage=="f90":
        f90controlgen.run(deploymentUnitInfo,BFG2ROOT)
    else:
        print "Error in controlgen.py. Internal coding error."
        exit(1)

