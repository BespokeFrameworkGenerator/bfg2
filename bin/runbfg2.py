#! /usr/bin/env python

# BFG2 python script
# CNC RF Created July 2011

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as ET
import bfgutils
import check
import inplacegen
import interfacegen
import bfginterfacegen
from bfgutils import coupledInfo
import esmfwrap
import controlgen
import targetgen

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-n", "--novalidate",
                  action="store_false", dest="validate", default=True,
                  help="do not validate xml documents before code generation")
parser.add_option("-d", "--directory", dest="OutDir",
                  help="output generated files to DIR", metavar="DIR")
parser.add_option("-m", "--mpivis",
                  action="store_true", dest="mpivis", default=False,
                  help="generate mpivis logging commands")
parser.add_option("-o", "--oasisvis",
                  action="store_true", dest="oasisvis", default=False,
                  help="generate oasisvis logging commands")
parser.add_option("-k", "--keep",
                  action="store_true", dest="keep", default=False,
                  help="don't delete any temporary xml documents")
parser.add_option("-s", "--shell",
                  action="store_true", dest="shell", default=False,
                  help="python is being called from shell; do not generate data structure")
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
wd=os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
xsltdir = os.path.join(BFG2ROOT,"xslt")

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if options.OutDir and not(os.path.isdir(options.OutDir)):
  print "error, non-existant target directory '%s'" % (options.OutDir)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

# validate before we change directory with -d option as it messes up
# the determination of the base directory BFG2ROOT
if options.validate:
  print "Checking that the xml is valid and consistent..."
  check.run(args[0])
  print "Done"
  print ""

if options.OutDir:
  OutDirNoQuote = os.path.abspath(options.OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
  os.chdir(OutDirNoQuote)
else:
  OutDirNoQuote = os.path.abspath(".")
  OutDir = "'" + OutDirNoQuote + "'"
  
CommFormsPathNoQuote = OutDirNoQuote + "/" + "DS.xml"
CommFormsPath = "'" + CommFormsPathNoQuote + "'"

if options.mpivis:
  visualisempi = "'true'"
else:
  visualisempi = "'false'"
  
if options.oasisvis:
  visualiseoasis = "'true'"
else:
  visualiseoasis = "'false'"

# START
if options.verbose:
  print "BFG2 ... autogentastic."
  sys.stdout.flush()

# READ OUR XML DOCUMENT AND CREATE ASSOCIATED PYTHON OBJECTS
CoupledDocumentDir = os.path.dirname(CoupledDocumentPathNoQuote)
coupledDocInfo=coupledInfo(CoupledDocumentPathNoQuote,CoupledDocumentDir,debug=False)

target=coupledDocInfo.getDeploymentInfo().target

if not (target=="oasis3" or target=="esmf"):

  # USE OLD STYLE XSLT TO GENERATE
  # 1: TARGET SPECIFIC CODE
  # 2: BFG2 CONTROL FILE
  # 3: TARGET SPECIFIC CONFIG FILES

  # READ IN ANY RE-USED XML
  epTemplate=bfgutils.xmlread(os.path.join(xsltdir,"Utils/EPTemplate.xml"))
  codeGenXSL=bfgutils.xmlread(os.path.join(xsltdir,"CodeGen/CodeGen.xsl"))
  coupledDoc=bfgutils.xmlread(CoupledDocumentPathNoQuote)

  # GENERATE DATA STRUCTURE (USED TO IMPROVE XSL PROCESSING EFFICIENCY)
  if not options.shell:
    if options.verbose:
      print "Generating Data structure"
      sys.stdout.flush()
    et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Data-struct/ConstructDS.xsl"),
                                  xmldoc=epTemplate,
                                  args={"CoupledDocument":CoupledDocumentPath})
    et_result.write(CommFormsPathNoQuote,pretty_print=True)

  # GENERATE BFG CONTROL FILE
  if options.verbose:
    print "Generating Control File",
    sys.stdout.flush()
  et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Control/f90ControlFile_template.xsl"),
                                xmldoc=coupledDoc,
                                args={"CoupledDocument" : CoupledDocumentPath , "OutDir" : OutDir })
  if options.verbose:
    print '"BFG2Control.nam"'
    sys.stdout.flush()

  # GENERATE BFG TARGET SPECIFIC CODE
  if options.verbose:
    print "Generating Target Specific Code",
  TargetCodePhases=[['1',None,epTemplate,{"CoupledDocument":CoupledDocumentPath,"mpivis":visualisempi,"oasisvis":visualiseoasis},True],\
                    ['2',None,None,{"CoupledDocument":CoupledDocumentPath,"CommForms":CommFormsPath,"OutDir":OutDir,"mpivis":visualisempi,"oasisvis":visualiseoasis},True],\
                    ['3',None,None,{"CoupledDocument":CoupledDocumentPath},True],\
                    ['4',None,None,{"CoupledDocument":CoupledDocumentPath},True],\
                    ['gen',codeGenXSL,None,{"FileName":"'BFG2Target.f90'","OutDir":OutDir,"CoupledDocument":CoupledDocumentPath,"CommForms":CommFormsPath},False]]
  for args in TargetCodePhases :
    et_result=bfgutils.runPhase(xsltdir,OutDirNoQuote,et_result,options,"Target",*args)
  if options.verbose:
    print '"BFG2Target.f90"'
    sys.stdout.flush()

  # GENERATE ANY ASSOCIATED TARGET CONFIG FILES
  if options.verbose:
    print "Target Config. Phase",
    sys.stdout.flush()
  et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Config/1/ConfigPhase1.xsl"),
                                xmldoc=coupledDoc,
                                args={ "CoupledDocument" : CoupledDocumentPath , "OutDir" : OutDir , "CommForms" : CommFormsPath})
  if options.verbose:
    print '"BFG2Config Files"'
    sys.stdout.flush()

  # END
  if not options.keep:
    os.remove(CommFormsPathNoQuote)

else:

  # TARGET IS oasis3 OR esmf AND (AS THESE ARE NEW) WE
  # USE TEMPLATING TO GENERATE EVERYTHING
  # 1: TARGET SPECIFIC CODE
  # 2: BFG2 CONTROL FILE [TBD]
  # 3: TARGET SPECIFIC CONFIG FILES [TBD]

  if options.verbose:
    print "Generating Target Specific Code using template",
    sys.stdout.flush()

  targetgen.genAll(coupledDocInfo,BFG2ROOT)

  if options.verbose:
    print '"BFG2Target*.f90"'
    sys.stdout.flush()

  print "Here is where oasis3 and esmf should create required BFG control files"

  print "Here is where oasis3 and esmf should create all required target specific config files"

# GENERATE BFG INPLACE CODE
if options.verbose:
  print "Generating f90 InPlace Code using template",
  sys.stdout.flush()

inplacegen.genAll(coupledDocInfo,BFG2ROOT)

if options.verbose:
  print '"BFG2InPlace*.f90"'
  sys.stdout.flush()

# GENERATE BFG CONTROL CODE
# Warning: due to bad coding by me, the requiresInPlace() method used in controlgen
# only works after the inPlace generation phase has completed
if options.verbose:
  print "Generating f90 Control Code using template",
  sys.stdout.flush()

controlgen.genAll(coupledDocInfo,BFG2ROOT)

if options.verbose:
  print '"BFG2Main*.f90"'
  sys.stdout.flush()

# GENERATE BFG INTERFACE CODE
if options.verbose:
  print "Generating f90 Interface Code using template",
  sys.stdout.flush()

bfginterfacegen.genAll(coupledDocInfo,BFG2ROOT)

if options.verbose:
  print '"BFG2Interface*.f90"'
  sys.stdout.flush()

# GENERATE ANY REQUIRED BFG LANGUAGE INTERFACE CODE
if options.verbose:
  print "Generating any required Control Interface Code using template",
  sys.stdout.flush()

interfacegen.genAll(coupledDocInfo,BFG2ROOT)

if target=="esmf":
  # TBD: create put/get module to handle comms
  # TBD: create a coupler component to communicate between sequence units
  # TBD: create a main program to invoke the sequence unit and coupler
  # 1: export each sequence unit as an esmf component
  deploymentUnits=coupledDocInfo.getDeploymentInfo().getDeploymentUnitsInfo()
  for deploymentUnit in deploymentUnits:
    sequenceUnits=deploymentUnit.getSequenceUnitsInfo()
    for sequenceUnit in sequenceUnits:
      for model in sequenceUnit.getModels():
        esmfwrap.genModel(model,BFG2ROOT)
  
if options.verbose:
  print "BFG2 has left the building."
  sys.stdout.flush()
