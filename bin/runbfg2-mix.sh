#!/bin/sh -e

BFG2=$HOME/bfg2
XSLT=$BFG2/xslt
JAVA=java
XSLT_CLASS=org.apache.xalan.xslt.Process
IN=-IN
OUT=-OUT
XSL=-XSL
#OPTIONS=-TT
PARAM=-PARAM

while getopts "atkd:f:q" flag; do
  case $flag in
    a ) ARCHIVE="true";;
    t ) TIME="time";;
    k ) KEEP="-k";;
    d ) OUT_DIR=$OPTARG;;
    f ) INPUT=$OPTARG;;
    q ) QUIET="-q";;
  esac
done

if [ ! $OUT_DIR ]
then
echo 'Usage: runbfg2.sh [-atkq] -f <input xml> -d <destination directory>'
exit
fi

if [ ! $INPUT ]
then
echo 'Usage: runbfg2.sh [-atkq] -f <input xml> -d <destination directory>'
exit
fi

#Handle relative input paths
if [ ${INPUT:0:1} != '~' ] && [ ${INPUT:0:1} != '/' ]
then
INPUT=$PWD'/'$INPUT
fi
if [ ${OUT_DIR:0:1} != '~' ] && [ ${OUT_DIR:0:1} != '/' ]
then
OUT_DIR=$PWD'/'$OUT_DIR
fi

if [ ! $QUIET ]
then
echo 'Generating communicationForms XML doc'
fi
$TIME $JAVA $XSLT_CLASS $XSL $XSLT/Data-struct/ConstructDS.xsl $OUT $OUT_DIR/DS.xml $PARAM CoupledDocument $INPUT $OPTIONS

$TIME $BFG2/bin/runbfg2.py -s $QUIET $KEEP -d $OUT_DIR $INPUT


if [ ! $KEEP ]
then
if [ ! $QUIET ]
then
echo 'Removing temporary XML files'
fi
 rm -f $OUT_DIR/DS.xml
fi

#Archive metadata into OUT_DIR
if [ $ARCHIVE ]
 then
 if [ ! $QUIET ]
 then
  echo 'Archiving metadata and BFG subversion revision number'
  echo -n 'Enter the current subversion revision number for BFG2 '
  read VERSION
  TARFILE='metadata-r'$VERSION'.tar'
  #MYROOT=`expr match "$INPUT" '\(.*\)/.*'`
  #FILE=`expr match "$INPUT" '.*/\(.*\)'`
  MYROOT=`dirname $INPUT`
  FILE=`basename $INPUT`
  CWD=$PWD
  cd $MYROOT
  echo archiving $FILE
  tar cf $TARFILE -C $MYROOT $FILE
  FILES=`$TIME $JAVA $XSLT_CLASS $XSL $XSLT/Utils/ArchiveList.xsl $IN $INPUT`
  for FILE in $FILES
  do
   MYROOT=`dirname $FILE`
   FILE=`basename $FILE`
   echo archiving $FILE
   tar rf $TARFILE -C $MYROOT $FILE
  done
 fi
 mv $TARFILE $OUT_DIR
 cd $CWD
fi
