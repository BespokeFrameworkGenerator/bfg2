#! /usr/bin/env python

# BFG2 alpha python script
# CNC RF Created on 8th April 2006

import shutil
import sys
import os
from optparse import OptionParser
import libxml2
import libxslt

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0alpha"
parser = OptionParser(usage)

(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
# print "Working dir", os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
# print "BFG2ROOT",BFG2ROOT
# xsltdir = os.path.join(BFG2ROOT,"xslt")
svgdir = os.path.join(BFG2ROOT,"svg")
# print "xsltdir",xsltdir

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

def libxsltErrorCallback(ctx, str):
  global error_msg
  print "%s %s" % (ctx, str)
  sys.exit(1)

libxml2.registerErrorHandler(libxsltErrorCallback, "")

try:
  styledoc = libxml2.parseFile(os.path.join(svgdir,"svggen2.xsl"))
  style = libxslt.parseStylesheetDoc(styledoc)
  doc = libxml2.parseFile("CoupledLayout.svg")
  result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
  style.saveResultToFilename("CoupledConnections.svg", result, 0)
  style.freeStylesheet()
  doc.freeDoc()
  result.freeDoc()
except:
  print "Parse error in svggen2.xsl, aborting"
  sys.exit(1)

