#
#  Generates SVG representation of coupling from metadata.
#  Paul Slavin  <slavinp@cs.man.ac.uk>
#  Rupert Ford updated
#

import os
import sys
import subprocess
from argparse import ArgumentParser, RawDescriptionHelpFormatter, ArgumentError



prog = os.path.splitext(sys.argv[0])[0]


class HelpArgumentParser(ArgumentParser):

	def error(self, msg):
		action = self._actions.pop()
		raise ArgumentError(action, msg)


def failWith(message, errno):

	print >> sys.stderr, ("[{prog}] {msg}".format(prog=prog, msg=message))
	sys.exit(errno)


def writeFile(outfile, output):

	try:
		with open(outfile, 'w') as fp:
			fp.write(output)
	except EnvironmentError as e:
		failWith("Unable to write output to {0}\n{1}".format(outfile, e), 1)



try:
	from lxml import etree as et
except ImportError:
	failWith("This program requires the LXML module (www.lxml.de)", 1)
	

desc = """\
This program accepts a Coupling Description file as its <coupledFile> parameter
and generates an SVG representation of the coupling described therein.
By default, this SVG is wrapped in HTML so that it may be formatted with custom
CSS and Javascript."""

usage = "%(prog)s [-f FILENAME | -s] [-g | -r] [options] coupledFile"

parser = HelpArgumentParser(formatter_class=RawDescriptionHelpFormatter, description=desc, usage=usage)

destgroup = parser.add_mutually_exclusive_group()
destgroup.add_argument("-f", "--file", dest="filename", help="Output filename")
destgroup.add_argument("-s", "--stdout", action="store_true", default=False, dest="writestdout", help="Write output to stdout")

parser.add_argument("-d", "--directory", dest="outdir", help="Output generated files to outdir")
parser.add_argument("-c", "--css", dest="css", help="Generate HTML which links to provided CSS URI")
parser.add_argument("-j", "--javascript", dest="javascript", help="Generate HTML which links to provided Javascript URI")
parser.add_argument("-x", "--xslt", dest="xsltdoc", help="Use alternate XSLT document")
layoutOptions=["dot","twopi","circo"]
parser.add_argument("-l", "--layout", dest="dotlayout", default="dot", help="choose layout algorithm. Options are "+str(layoutOptions))
parser.add_argument("-o", "--options", dest="dotconfig", help="Pass graph layout option")
parser.add_argument("coupledFile", help="The Coupling Description file to depict with SVG")

outputgroup = parser.add_mutually_exclusive_group()
outputgroup.add_argument("-g", "--graphviz", action="store_true", dest="graphviz", help="Output only Graphviz dot source")
outputgroup.add_argument("-r", "--rawsvg", action="store_true", dest="rawsvg", help="Do not embed SVG output in HTML tags.")



try:
	args = parser.parse_args()
except ArgumentError as e:
	parser.print_help(sys.stderr)
	sys.exit(1)


bfgroot = os.environ.get("BFG2ROOT")
if not bfgroot: bfgroot=os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),".."))
visdir = os.path.join(bfgroot, "vis")
xslfile = "dotgen_id.xsl"
xsltdoc = os.path.join(visdir, xslfile) if not args.xsltdoc else args.xsltdoc

if not args.dotlayout in layoutOptions:
	failWith("-l argument error, expected one of "+str(layoutOptions),1)
dot=args.dotlayout

try:
	xsltroot = et.parse(xsltdoc)
	xslt = et.XSLT(xsltroot)
except (et.LxmlError, IOError):
	failWith("Error in constructing XSLT transform from file {0}".format(xsltdoc), 1)

try:
	xmlroot = et.parse(args.coupledFile)
except et.XMLSyntaxError as e:
	failWith("XML Syntax Error in Coupled file {0}: {1}".format(args.coupledFile, e), 1)
except IOError:
	failWith("Unable to access Coupling Description file {0}".format(args.coupledFile), 1)


absfile = "'{0}'".format(os.path.abspath(args.coupledFile))

try:
	transformed = xslt(xmlroot, CoupledDocument=absfile)
except et.XSLTError:
	failWith("Error in applying XSLT transformation:\n{0}".format(xslt.error_log), 1)


dotsrc = str(transformed)
del xsltroot, xslt, xmlroot, transformed

if args.dotconfig!=None:
	dotsrclines=dotsrc.splitlines()
	dotsrclines.insert(2,args.dotconfig)
	dotsrc=os.linesep.join(dotsrclines)

if not args.graphviz:
	dotargs = [ dot, "-Tsvg" ]
	os.environ["LD_LIBRARY_PATH"] = "/usr/local/lib"

	try:
		proc = subprocess.Popen(dotargs, stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=os.environ)
		svgsrc = proc.communicate(dotsrc)[0]
	except OSError as e:
		failWith("Unable to execute dot at {0}\n{1}".format(dot, e), 1)

	if args.rawsvg:
		output = svgsrc
		ext = "svg"
	else:
		csstemplate = "<link rel='stylesheet' type='text/css' href='{0}' />".format(args.css) if args.css else ""
		jstemplate = "<script type='text/javascript' src='{0}'></script>".format(args.javascript) if args.javascript else ""

		svgsrclines=svgsrc.splitlines()
		line=0
		while svgsrclines[line].find("<svg")==-1: line+=1
		
		output = "<html><head>{0}{1}<title>SVG output</title></head><body>{2}</body></html>".format(csstemplate, jstemplate, os.linesep.join(svgsrclines[line:]))
		ext = "html"
else:
	output = dotsrc
	ext = "dot"

if args.writestdout:
	print(output)
	sys.exit(0)

if not args.filename:
	base = os.path.splitext(os.path.basename(args.coupledFile))[0]
	args.filename = os.path.extsep.join( (base, ext) )

outfile = os.path.join(args.outdir, args.filename) if args.outdir else args.filename

writeFile(outfile, output)
