import os
import bfgutils
import shutil


def run(deploymentUnitInfo, BFG2ROOT):

  templatedir = os.path.join(BFG2ROOT, "templates")
  templateFile = "fortran90_to_python_varargs.xml"

  scheduleInfo = deploymentUnitInfo.getDeploymentInfo().getScheduleInfo()
#  epInfoList = []

  proxyFunc = "vacall"
  pyInit = "pycallInit();"
  pyTidy = "pycallTidy();"

  funcChars = { "int" : "i" , "double" : "d", "float" : "f", "array" : "O", "bool" : "i", "logical" : "int", "str" : "s", "char" : "s" , "float" : "d", "long" : "i"}
  py2c = {"int" : "int" , "double" : "double", "float" : "double", "bool" : "bool", "str" : "char", "float32" : "float", "int64" : "long"}

  arrayFmtTemplate = "ndarrayFmt(\"N[{0},{1}]\", {2})"
  arrayRetTemplate = "ndarray{0}Ret({1}, {2});"
#  arrayiRetTemplate = "ndarrayiRet({0}, {1});"

  pyoDec = "PyObject *pyr{0};"

#  shutil.copyfile(os.path.join(BFG2ROOT, "include/python/pycall.h"), "pycall.h") 

  for modelInfo in [x for x in deploymentUnitInfo.getModels() if x.language == "python" and not x.programConformance() ]:
      print("[MODELINFO] in {0}\n{1}\n[/MODELINFO]".format(modelInfo.__module__, dir(modelInfo)))
      print("coupledInfo: {0}\n".format(dir(modelInfo.coupledInfo)))
      print("coupledInfo: {0}\n".format(modelInfo.coupledInfo.CoupledDocumentDir))
      print("defnDocLookup: {0}\n".format(dir(modelInfo.coupledInfo.defnDocLookup[modelInfo.name])))
      print("getDefinitionInfo: {0}\n".format(dir(modelInfo.coupledInfo.getDefinitionInfo(modelInfo.name) )) )
      print("deploymentUnitInfo: {0}\n".format(dir(deploymentUnitInfo)))

      epInfoList = []

      for epName in scheduleInfo.getEPNames(modelInfo.name):
          epInfo = modelInfo.getEP(epName)
          argsMap = epInfo.getArgsMap()

#          print("[epInfo] is type {0}\n{1}\n[/epInfo]".format(type(epInfo), dir(epInfo) ))
          uniqueEPName = epInfo.getUniqueName()
#          print("--=: epInfo.getArgsMap() len={0} :==-\n".format(len(argsMap)) )
          argTypes = []
          argNames = [ "*arg"+str(i) for i in range(len(argsMap)) ]
          argString = ""
          nulltermStr = ""           
 #         print("[ARGNAMES] {0}".format(argNames))


          for i, (k,v) in enumerate(argsMap.items()):
#              print("[argsMap]")
#              print("    {0} : {1}  -- {2} -- {3}\n".format(k, v.getInPlaceTagDatatype(), v.dataType, v.__dict__ ) )
#              print("\tdimension: {0}\tdimSizes: {1}\tarrayDimsSize: {2}".format(v.dimension, v.dimSizes, v.arrayDimsSize()))

              argName = argNames[i]
              nulltermStr = ""           


              print("v.getDataType(): {0}".format( v.getDataType("f90")) )
              print("v.getStringSize(): {0}".format( v.getStringSize() ) )


# Null-terminate F90 string destined for Python
              if (v.getDataType("f90") == "character"):
                  strlen = int(v.getStringSize())
                  nulltermStr = "{0}[{1}] = 0;".format(argName.replace("*",""), strlen)


              if v.getEPInfo().getModelInfo().language == "python":
                # map from python datatypes to c datatypes
                v.dataType=py2c[v.dataType]
                # char is not a pointer so remove *
                if(v.dataType == "char"):
                  argName = argNames[i].replace("*","")


              if (int(v.dimension) > 0):
#                  print("Constructing arrayFmt for with dimension: {0} ({1})".format(v.dimension, type(v.dimension)) )
                  arrayFmt = arrayFmtTemplate.format( ",".join(map(str, v.dimSizes)), funcChars[v.dataType], argNames[i].replace("*", ""))
                  argString += 'O'
                  argTypes.append( "{0} {1}".format(v.dataType, argNames[i]) )
                  argNames[i] = arrayFmt
              else:
                  argString += funcChars[v.dataType]
                  argTypes.append( "{0} {1}".format(v.dataType, argNames[i]) )
                  argNames[i] = argName


#          print("[argString] {0} (len: {1})".format(argString, len(argString)))
          if len(argString) < 2:
              argString = "({0})".format(argString)

#          print("[ARGTYPES] {0}".format(argTypes))

	  funcs = epInfo.getFuncRetsList()

#          print("[FUNCRETS]\n{0}\n{1}\n{2}".format(epInfo.getFuncRetsMap(), funcs, epInfo.getArgsFuncsList() ))

          epArgsDefns = []
          epArgIDs = {}

          retVars = []
          retChars = []

          comma = ","

          pyoReturns = []
          pyoReturnCopies = []
          arrayRetTypeChar = ""
          addrOperator = ''
          charpSave = ""
          charpStrlen = ""
          charpCopy = ""

          for i,f in enumerate(funcs):
              print("\t[function {0}] dimension: {1}\tdataType: {2}\tfieldName: {3}".format(i, f.dimension, f.dataType, f.fieldName) )
#              print("dir(f): {0}".format(dir(f)))

              arrayRetTypeChar = ""
              addrOperator = ''
              charpSave = ""
              charpStrlen = ""
              charpCopy = ""

              if(f.getEPInfo().getModelInfo().language == "python" and f.dataType == "float"):
                  f.dataType = "double"

              if(f.getEPInfo().getModelInfo().language == "python" and f.dataType == "str"):
#                  print("getDataType(): {0}".format( f.getDataType("f90")) )
#                  print("getStringSize(): {0}".format( f.getStringSize() ) )
                  f.dataType = "char"
                  addrOperator = '&'
                  charpSave = "char *const orig = r{0};".format(i)
                  charpStrlen = "size_t len = strlen(r{0});".format(i)
                  charpCopy = "memcpy(orig, r{0}, len);".format(i)

              if(f.getEPInfo().getModelInfo().language == "python" and f.dataType == "int"):
                  arrayRetTypeChar = "i"


              if(f.getEPInfo().getModelInfo().language == "python" and f.dataType == "bool"):
                  shutil.copyfile(os.path.join(BFG2ROOT, "include/python/bfg2decorators.py"), "bfg2decorators.py")

              idName = "*r{0}".format(i)

              if(int(f.dimension) > 0):
                  pyoReturns.append(pyoDec.format(i))
                  retVars.append("&pyr{0}".format(i))
                  retChars.append('O')
                  pyoReturnCopies.append(arrayRetTemplate.format(arrayRetTypeChar, "pyr{0}".format(i), idName.replace("*", "")) )
              else:
                  retVars.append("{0}r{1}".format(addrOperator, i))
                  retChars.append(funcChars[f.dataType])

              idIndex = "epArg{0}".format(i)
              epArgsDefns.append("{0} {1}".format(f.dataType, idName))
              epArgIDs[idIndex] = idName

#          print("[/FUNCRETS]")
#          print("[retVars] {0}".format(retVars))

          retFmt = "".join(retChars)

#          print("==> retChars: {0}".format(retFmt))

          for k,v in epArgIDs.items():
              print("epArgID\t{0} : {1}".format(k, v))

#          if not funcs:
#              epArgsDefns = []

          if not (argNames or retVars):
              comma = ""

          templateParams = { "pyModule" : modelInfo.name,
                             "epNamef90" : f90toclink(uniqueEPName),
                             "proxyFunc" : proxyFunc }

          templateParams.update( {     "charpSave" : charpSave,
                                       "charpStrlen" : charpStrlen,
                                       "charpCopy" : charpCopy,
                                       "nulltermStr" : nulltermStr
                                     } )


          templateParams.update( {    "pyClass" : modelInfo.name,
                                      "pyMethod" : "{0}".format(epName),
                                      "pyoReturnDeclarations" : "\n\t".join(pyoReturns),
                                      "pyoReturnCopies" : "\n\t".join(pyoReturnCopies),
                                      "pyInit" : pyInit,
                                      "pyTidy" : pyTidy,
                                      "fmt" : argString + ":" + retFmt,
                                      "comma" : comma,
                                      "va_list" : ", ".join(argNames + retVars),
                                      "epArgsDefns" : ", ".join(argTypes + epArgsDefns) } )

          epInfoList.append(templateParams)


          mapping = {"code" : epInfoList}
          controlTemplate = bfgutils.xmlread(os.path.join(templatedir, templateFile))
          result = bfgutils.map(mapping, controlTemplate)
          result.write(modelInfo.name + ".c", method="text")

#  print("[RESULT] is type {0}\n{1}[\RESULT]".format(type(result), bfgutils.ET.tostring(result, pretty_print=True)) )
#  result.write("BFG2Control"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")


def f90toclink(name):
    # assume that we add an underscore
    # need to add other options that can be specified by the user
    return name.lower() + "_"
