import os
import bfgutils
from bfgutils import map, getEPCallXML
from lxml import etree as ET
import f90controlgen

def genModel(modelInfo,BFG2ROOT):

    # READ IN OUR XML
    templatedir = os.path.join(BFG2ROOT,"templates")
    ESMFTemplate=bfgutils.xmlread(os.path.join(templatedir,"esmf_fortran_component.xml"))

    coupledInfo=modelInfo.getCoupledInfo()
    deploymentInfo=coupledInfo.getDeploymentInfo()
    #deploymentUnitInfo=sequenceUnit.getDeploymentUnitInfo()

    # perform basic checks
    # 1: make sure that our model is program compliant
    if modelInfo.programConformance():
        print "Error, esmf gen : can't export models that are program compliant"
        print "Model ",modelInfo.name," is program compliant"
        exit(1)

    # create this module's name
    ESMFModuleName=modelInfo.name+"_esmf"

    BFG2Modules=[]
    # Only include ep's that are specified in the deployment
    EPListString=""
    for ep in deploymentInfo.getScheduleEPs2():
        if modelInfo==ep.getModelInfo():
            if EPListString!="" :EPListString+=", "
            EPListString+=ep.epName

    BFG2Modules.append({"BFG2Module":modelInfo.name,
                        "BFG2EntryPoints":EPListString})

    # create the ESMF callback routines
    ESMFCallBackRoutinesList=[]

    ESMFBFG2WrappersList=[]
    for entryPoint in deploymentInfo.getScheduleEPs2():
        if modelInfo==entryPoint.getModelInfo():

            # Need to sort out BFG2EPPosition. This needs orders to be mandatory in BFG2 metadata.
            BFG2EPPosition=entryPoint.getPosition(EPListString)
            BFG2RunType=entryPoint.runType
            ESMFRunType=entryPoint.BFGtoESMFRunType[BFG2RunType]
            ESMFWrapperEP=entryPoint.epName+"_esmf"
            ESMFCallBackRoutinesList.append({"ESMFRunType":ESMFRunType,
                                             "ESMFWrapperEP":ESMFWrapperEP,
                                             "ESMFPhase":BFG2EPPosition})

            EPControl=bfgutils.getEPCallXML(entryPoint,BFG2ROOT,epName=entryPoint.epName)

            # create the ESMF routines
            ESMFBFG2WrappersList.append({"ESMFWrapperEP":ESMFWrapperEP,"EPControl":EPControl})

            
    ## determine the runtypes required
    #BFG2RunTypeList=[]
    #for entryPoint in modelInfo.getEPs():
    #    BFG2RunType=entryPoint.runType
    #    if BFG2RunType not in BFG2RunTypeList:
    #        BFG2RunTypeList.append(BFG2RunType)
    #BFG2EPPosition="1"
    ## add a callback and generate wrapper subroutine for each runtype
    #ESMFBFG2WrappersList=[]
    #for BFG2RunType in BFG2RunTypeList:

    #    ESMFRunType=entryPoint.BFGtoESMFRunType[BFG2RunType]
    #    ESMFWrapperEP=BFG2RunType+"_esmf"
    #    ESMFCallBackRoutinesList.append({"ESMFRunType":ESMFRunType,
    #                                     "ESMFWrapperEP":ESMFWrapperEP,
    #                                     "ESMFPhase":BFG2EPPosition})

    #    EPControl=bfgutils.getEPCallXML(entryPoint,BFG2ROOT)

    #    # create the ESMF routines
    #    ESMFBFG2WrappersList.append({"ESMFWrapperEP":ESMFWrapperEP,"EPControl":EPControl})

    mapping={"ESMFModuleName":ESMFModuleName,
             "BFG2Modules":BFG2Modules,
             "ESMFEntryPoint":"user_register",
             "ESMFCallBackRoutines":ESMFCallBackRoutinesList,
             "ESMFBFG2Wrappers":ESMFBFG2WrappersList}

    # RF: ONLY WRAPPING F90 CODES SO FAR
    #     NO SURE HOW TO SUPPORT DIFFERENT LANGUAGES YET
    language=modelInfo.language
    assert language==modelInfo.f90String

    BFG2Module=modelInfo.name

    result=map(mapping,ESMFTemplate)
    result.write(modelInfo.name+"_esmf.f90",method="text")

def genSU(sequenceUnit,BFG2ROOT):

    # READ IN OUR XML
    templatedir = os.path.join(BFG2ROOT,"templates")
    ESMFTemplate=bfgutils.xmlread(os.path.join(templatedir,"esmf_fortran_component.xml"))

    models=sequenceUnit.getModels()
    deploymentUnitInfo=sequenceUnit.getDeploymentUnitInfo()
    deploymentInfo=deploymentUnitInfo.getDeploymentInfo()
    coupledInfo=deploymentInfo.getCoupledInfo()


    # perform basic checks
    # 1: make sure that none of our models are program compliant
    for model in models:
        if model.programConformance():
            print "Error, esmf gen : can't export models that are program compliant"
            print "Model ",model.name," is program compliant"
            exit(1)

    # create this module's name
    ESMFModuleName=getESMFModuleName(models,sequenceUnit)

    BFG2Modules=[]
    for model in models:

        # Only include ep's that are specified in the deployment
        EPListString=""
        for ep in deploymentInfo.getScheduleEPs2():
            if model==ep.getModelInfo():
                if EPListString!="" :EPListString+=", "
                EPListString+=ep.epName

        BFG2Modules.append({"BFG2Module":model.name,
                            "BFG2EntryPoints":EPListString})

    # create the ESMF callback routines
    ESMFCallBackRoutinesList=[]
    # determine the runtypes required
    BFG2RunTypeList=[]
    for model in models:
        for entryPoint in model.getEPs():
            BFG2RunType=entryPoint.runType
            if BFG2RunType not in BFG2RunTypeList:
                BFG2RunTypeList.append(BFG2RunType)
    BFG2EPPosition="1"
    # add a callback and generate wrapper subroutine for each runtype
    ESMFBFG2WrappersList=[]
    for BFG2RunType in BFG2RunTypeList:

        ESMFRunType=entryPoint.BFGtoESMFRunType[BFG2RunType]
        ESMFWrapperEP=BFG2RunType+"_esmf"
        ESMFCallBackRoutinesList.append({"ESMFRunType":ESMFRunType,
                                         "ESMFWrapperEP":ESMFWrapperEP,
                                         "ESMFPhase":BFG2EPPosition})

        EPControl=f90controlgen.genSUControl(sequenceUnit,BFG2RunType,BFG2ROOT)
        # create the ESMF routines
        ESMFBFG2WrappersList.append({"ESMFWrapperEP":ESMFWrapperEP,"EPControl":EPControl})

    mapping={"ESMFModuleName":ESMFModuleName,
             "BFG2Modules":BFG2Modules,
             "ESMFEntryPoint":"user_register",
             "ESMFCallBackRoutines":ESMFCallBackRoutinesList,
             "ESMFBFG2Wrappers":ESMFBFG2WrappersList}









    # temporary hack
    definitionDocInfo=models[0]

    language=definitionDocInfo.language

    # RF: ONLY WRAPPING F90 CODES SO FAR
    #     NO SURE HOW TO SUPPORT DIFFERENT LANGUAGES YET
    assert language==definitionDocInfo.f90String

    BFG2Module=definitionDocInfo.name
    #EntryPoints=getNodes(DefinitionDoc,entryPointLocation)

    result=map(mapping,ESMFTemplate)
    #ETresult=ET.ElementTree(result)
    result.write(ESMFModuleName+".f90",method="text")
    #print(ET.tostring(ESMFTemplate, method="text",pretty_print=True))

def getESMFModuleName(models,sequenceUnit):
    deploymentUnitInfo=sequenceUnit.getDeploymentUnitInfo()
    if len(models)==1:
        ESMFModuleName=models[0].name
    else:
        ESMFModuleName=""
        count=0
        for model in models:
            count+=1
            ESMFModuleName+=model.name[:3]
            if count<len(models): ESMFModuleName+="_"
    ESMFModuleName+="_du"+str(deploymentUnitInfo.getID())+"_su"+str(sequenceUnit.getID())+"_wrap_esmf"
    return ESMFModuleName
