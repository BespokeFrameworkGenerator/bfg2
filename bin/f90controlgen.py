import os
import bfgutils
from bfgutils import map,listString
from lxml import etree as ET
from copy import deepcopy

def run(deploymentUnitInfo,BFG2ROOT):

    deploymentInfo=deploymentUnitInfo.getDeploymentInfo()
    coupledInfo=deploymentInfo.getCoupledInfo()
    compositionInfo=coupledInfo.getCompositionInfo()

    # collect all dataObjects for this deployment unit
    # we can get duplicate names here as two different couplings can have the same priming. Should I filter these out here, or later? Currently done later for namelist priming.
    dataObjectList=[]
    scheduleInfo=deploymentInfo.getScheduleInfo()
    for modelName in deploymentUnitInfo.getModelNames():
        modelInfo=deploymentInfo.getCoupledInfo().getDefinitionInfo(modelName)
        epNamesList=scheduleInfo.getEPNames(modelName)
        for epName in epNamesList:
            epInfo=modelInfo.getEP(epName)
            for arg in epInfo.getArgsFuncsList():
                potentialDataObject=arg.getDataObject()
                if potentialDataObject not in dataObjectList:
                    dataObjectList.append(potentialDataObject)

    # add all convergence dataobjects as their priming is replicated???? Will there be side effects????
    for modelInfo in coupledInfo.getDefinitionsInfo():
        if modelInfo.isControlModel():
            for epInfo in modelInfo.getEPs():
                for arg in epInfo.getArgsFuncsList():
                     potentialDataObject=arg.getDataObject()
                     if potentialDataObject not in dataObjectList:
                         dataObjectList.append(potentialDataObject)

    templatedir = os.path.join(BFG2ROOT,"templates")
    origProgramTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_control.xml"))
    programTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_control.xml"))

    ######################################
    # add basic program name information #
    ######################################
    duID=deploymentUnitInfo.getID()
    content={"programName":"BFG2Main"+str(duID),
             "DUID":duID}
    template=map(content,programTemplate,snippetName="program")

    #################################
    # add in the BFG2 namelist read #
    #################################
    scheduleInfo=deploymentInfo.getScheduleInfo()
    nIterators=int(scheduleInfo.numScheduleIterators())
    namelistTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_namelist.xml"))
    iterationsList=[]
    for i in range(1,nIterators+1):
        iterationsList.append("nts"+str(i))
    varNameListString=listString(iterationsList)

    declarationList=[]
    if nIterators>0:
        declarationList.append({"variables":"integer :: "+varNameListString,"nlName":"time","varNameList":varNameListString})

    # namelist content
    contentList=[]
    if nIterators>0:
        contentList.append({"fileUnit":"1011","fileName":"BFG2Control.nam","nlName":"time"})

    #####################################
    # add in any priming namelist reads #
    #####################################
    namelistInfoList=[]
    for dataObject in dataObjectList :
        if dataObject.isPrimed():
            if dataObject.primingType()=="file":
                fileInfo=dataObject.getPrimingFileInfo()
                if fileInfo.type=="namelist":
                    print "Found a priming from a namelist file"
                    namelistInfoList.append(fileInfo)

    fileNameList=[]
    for namelistInfo in namelistInfoList:
        if namelistInfo.name not in fileNameList:
            fileNameList.append(namelistInfo.name)
            print "Found namelist file ",namelistInfo.name
            nlNameList=[]
            for namelistInfo2 in namelistInfoList:
                if namelistInfo2.name==namelistInfo.name:
                    if namelistInfo2.nlName not in nlNameList:
                        nlNameList.append(namelistInfo2.nlName)
                        print "Found nlname "+namelistInfo2.nlName
                        dataNames=[]
                        for namelistInfo3 in namelistInfoList:
                            if namelistInfo3.name==namelistInfo2.name and namelistInfo3.nlName==namelistInfo2.nlName and namelistInfo3.dataRef not in dataNames:
                                dataNames.append(namelistInfo3.dataRef)
                                print "Found data "+namelistInfo3.dataRef
                        declarationList.append({"variables":"","nlName":namelistInfo2.nlName,"varNameList":listString(dataNames)})
                        if deploymentInfo.getNumDUs()>1:
                            # there may be different namelist content in each du. Rather than replicate all content on all du's I have chosen to use a du specific namelist file. We should perhaps write a utility that splits a single namelist file appropriately.
                            namelistFileName=namelistInfo2.name+"_du"+str(deploymentUnitInfo.getID())
                        else:
                            namelistFileName=namelistInfo2.name
                        contentList.append({"fileUnit":namelistInfo2.unit,"fileName":namelistFileName,"nlName":namelistInfo2.nlName})

    declarationsTemplate=map({"snippet":declarationList},namelistTemplate,snippetName="declarations",copy=True)

    contentTemplate=map({"snippet":contentList},namelistTemplate,snippetName="contents",copy=True)

    # add namelist to main program
    content={"declarations":declarationsTemplate,
             "controlCode":contentTemplate}
    template=map(content,template,snippetName="program")

    #exit(1)

    ############################
    # add in any netcdf primings
    ############################

    # do we use netcdf?
    if deploymentUnitInfo.usesNetcdf():

        # determine all dataobjects that are primed via a netcdf read
        netcdfInfoList=[]
        for dataObject in dataObjectList :
            if dataObject.isPrimed():
                if dataObject.primingType()=="file":
                    fileInfo=dataObject.getPrimingFileInfo()
                    if fileInfo.type=="netcdf":
                        print "Found a priming from a netcdf file"
                        netcdfInfoList.append(fileInfo)

        contentList=[]
        fileNameList=[]
        for netcdfInfo in netcdfInfoList:
            if netcdfInfo.name not in fileNameList:
                fileNameList.append(netcdfInfo.name)
                print "Found netcdf file ",netcdfInfo.name
                dataNames=[]
                for netcdfInfo2 in netcdfInfoList:
                    if netcdfInfo2.name==netcdfInfo.name:
                        dataNames.append({"dataID":netcdfInfo2.dataRef,"varName":netcdfInfo2.dataRef})
                        print "Found data "+netcdfInfo2.dataRef
                #dataID, varName
                contentList.append({"fileName":netcdfInfo.name,"vars":dataNames})

        netcdfTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_netcdf.xml"))
        declarationsTemplate=map({},netcdfTemplate,snippetName="declarations",copy=True)
        contentTemplate=map({"snippet":contentList},netcdfTemplate,snippetName="contents",copy=True)

        # add netcdflist to main program
        content={"declarations":declarationsTemplate,
                 "controlCode":contentTemplate}
        template=map(content,template,snippetName="program")

    # determine if any put/get inplace code is generated and, if so, include the generated inplace module
    # in case some of the arguments also pass data via in-place calls
    # warning: due to bad coding by me the requiresInPlace() method only works after the inPlace generation phase has completed
    useNameList=[]
    if deploymentUnitInfo.requiresInPlace():
        useNameList.append("use "+"BFG2InPlace"+str(deploymentUnitInfo.getID())+"\n")

    # do we use netcdf?
    if deploymentUnitInfo.usesNetcdf():
        useNameList.append("use netcdf\n")

    ###############################
    # add in schedule information #
    ###############################
    coupledInfo=deploymentInfo.getCoupledInfo()
    scheduleInfo=deploymentInfo.getScheduleInfo()

    for modelName in deploymentUnitInfo.getModelNames():
        modelInfo=coupledInfo.getDefinitionInfo(modelName)
        # esmf does not call models directly.
        # We call them indirectly via BFG2Target1 so
        # do not want to declare ep's here.
        if modelInfo.language=="f90" and not deploymentInfo.target=="esmf" and not modelInfo.isControlModel():
            # get a list of the models ep's that are used by the schedule
            # We need to do this as it may be a subset of the models ep's.
            scheduleInfo=deploymentInfo.getScheduleInfo()
            epNamesList=scheduleInfo.getEPNames(modelName)
            renameList=[]
            # rename each epName
            for epName in epNamesList:
                epInfo=modelInfo.getEP(epName)
                uniqueEPName=epInfo.getUniqueName()
                renameList.append(uniqueEPName+"=>"+epName)
            epListString=listString(renameList)
            useNameList.append("use "+modelName+", only : "+epListString+"\n")
    content={"includes":useNameList}
    template=map(content,template)

    # declare any required data declarations
    root=ET.Element("i")
    for i in range(1,nIterators+1):
        ET.SubElement(root,"i"+str(i)).text="integer :: i"+str(i)+"\n"
    content={"declarations":root}
    template=map(content,template)

    # add in any required argument declarations
    # now declare our data objects
    root=ET.Element("i")
    count=0
    dataObjectNames=[]
    for dataObject in dataObjectList :
        if dataObject.getUniqueName() not in dataObjectNames:
            # only add name if it does not already exist. This
            # happens when two or more data objects have the same priming.
            dataObjectNames.append(dataObject.getUniqueName())
            count+=1
            fieldInfo=dataObject.getFieldInfo()
            dataType=fieldInfo.getDataType("f90")
            if dataType=="character":
                stringSize=int(fieldInfo.getStringSize()) + 1		# Plus one for null-termination when passed.
                dataType+="(len="+str(stringSize)+")"
            # prime any data objects if required
            primingString=""
            if dataObject.isPrimed():
                if dataObject.primingType()=="data":
                    primingString="="+dataObject.primingData()
            ET.SubElement(root,"i"+str(count)).text=dataType+" :: "+dataObject.getUniqueName()+fieldInfo.arrayDimsSize()+primingString+"\n"
    content={"declarations":root}
    template=map(content,template)

    # add in our control code
    myModelNames=deploymentUnitInfo.getModelNames()

    scheduleType=scheduleInfo.getScheduleTypeXML()

    # make sure we output phases in the correct order!
    for phase in ["init","iterate","final"]:
        for schedulePhase in scheduleType:
            if schedulePhase.tag==phase:
                # create and map the content for the current phase
                scheduleContent,dummy=(outputSchedule(deploymentUnitInfo,schedulePhase,programTemplate,myModelNames,1))
                content={"controlCode":scheduleContent}
                template=map(content,template)
            pass

    ETresult=ET.ElementTree(template)
    ETresult.write("BFG2Main"+str(deploymentUnitInfo.getID())+".f90",method="text")
    #print(ET.tostring(ETresult,method="xml",pretty_print=True))

def outputSchedule(deploymentUnitInfo,scheduleInfo,programTemplate,myModelNames,varid):
    convergeModelName=None
    root=ET.Element("i"+str(varid))
    for element in scheduleInfo:
        if element.tag=="loop":
            xmlContent,dummy=outputSchedule(deploymentUnitInfo,element,programTemplate,myModelNames,varid+1)
            content={"index":"i"+str(varid),
                     "start":"1",
                     "end":"nts"+str(varid),
                     "id":varid,
                     "content":xmlContent}
            result=map(content,programTemplate,snippetName="controlloop",copy=True)
            root.append(result)
        elif element.tag=="converge":
            xmlContent,convergeModelName=outputSchedule(deploymentUnitInfo,element,programTemplate,myModelNames,varid)
            myID=element.get("id")
            # we always define converge as being form argpass
            myForm="argpass"
            # we always define converge as being an in
            myDirection="in"
            myField=deploymentUnitInfo.getDeploymentInfo().getCoupledInfo().getDefinitionInfo("bfg:").getField("converge",myID,myForm,myDirection)
            myDataObject=myField.getDataObject()
            myName=myDataObject.getName()

            #  where required add gets to receive the convergence value
            if deploymentUnitInfo.getDeploymentInfo().getNumSUs()==1:
                getConditionValue=""
                initialGetConditionValue=""
            else:
                myID=myField.getEPInfo().getID()
                epArgs=myName+","+myID
                getCall=map({"snippet":[{"epName":"setActiveModel","epArgs":(int(myID)*-1)},{"epName":"get","epArgs":epArgs}]},programTemplate,snippetName="eps",copy=True)
                if convergeModelName is not None:
                    # we are on the same deployment unit
                    getConditionValue=map({"clause":".not.("+convergeModelName+"Thread())","content":getCall},programTemplate,snippetName="condition",copy=True)
                else:
                    # we are on a different deployment unit
                    getConditionValue=getCall
                # ***************************************
                deploymentInfo=deploymentUnitInfo.getDeploymentInfo()
                compositionInfo=deploymentInfo.getCoupledInfo().getCompositionInfo()
                if compositionInfo.putgetConnection(myField,"in",nesting=True,includeControl=True):
                    print "Convergence : Found a putget connection with nesting=True"
                    remoteInfos=compositionInfo.putgetConnections(myField,"in",nesting=True,includeControl=True)
                    assert len(remoteInfos)==1, "Error"
                    remoteInfo=remoteInfos[0]
                    print remoteInfo

                    myID=myField.getEPInfo().getID()
                    epArgs=myName+","+myID
                    initialGetCall=map({"snippet":[{"epName":"setActiveModel","epArgs":(int(myID)*-1)},{"epName":"get","epArgs":epArgs}]},programTemplate,snippetName="eps",copy=True)
                    localModel=myField.getEPInfo().getModelInfo()
                    remoteModel=remoteInfo.getEPInfo().getModelInfo()
                    if deploymentUnitInfo.sameDU(remoteModel):
                        # we are on the same deployment unit
                        remoteModelName=remoteInfo.getEPInfo().getModelInfo().name
                        initialGetConditionValue=map({"clause":".not.("+remoteModelName+"Thread())","content":initialGetCall},programTemplate,snippetName="condition",copy=True)
                    else:
                        # we are on a different deployment unit
                        initialGetConditionValue=initialGetCall
                else:
                    print "Convergence : No putget connection with nesting=True is found"
                    initialGetConditionValue=""
                    # skip

                print myField
            content={"initialGetCondition":initialGetConditionValue,
                     "condition":myName,
                     "id":myID,
                     "content":xmlContent,
                     "getCondition":getConditionValue
                     }

            result=map(content,programTemplate,snippetName="controlconverge",copy=True)
            root.append(result)
        elif element.tag=="model":
            supportedLanguages=["f77","f90","c","python"]
            modelName=element.get("name")
            if modelName in myModelNames:
                
                modelInfo=deploymentUnitInfo.getDeploymentInfo().getCoupledInfo().getDefinitionInfo(modelName)
                modelLanguage=modelInfo.language
                if modelLanguage not in supportedLanguages:
                    print "Error in control gen. Unsupported component language '"+modelLanguage+"' found. Supported langauges are "+str(supportedLanguages)
                    exit(1)
                epName=element.get("ep")
                epInfo=modelInfo.getEP(epName)
                modelInstance=element.get("instance")
                frequencyString=element.get("frequency")
                if frequencyString is None:
                    frequencyString="1"
                offsetString=element.get("offset")
                if offsetString is None:
                    offsetString="0"
                #timestep : not sure what this is for!

                # set current model to active
                content={"epName":"setActiveModel",
                         "epArgs":epInfo.getGlobalID()}
                resultActiveModel=map(content,programTemplate,snippetName="ep",copy=True)

                # call the model
                if modelLanguage=="f77":
                    # no need to rename for f77
                    uniqueEPName=epInfo.getName()
                else:
                    uniqueEPName=epInfo.getUniqueName()

                # add our arguments
                epArgsList=epInfo.getArgsList()
                # now append any funcret arguments to the end of our argument list.
                epFuncRetsList=epInfo.getFuncRetsList()
                # If there are funcrets defined then check the language is Python as funcret is only supported in python at the moment.
                assert (len(epFuncRetsList)==0 or (len(epFuncRetsList)>0 and epInfo.getModelInfo().language=="python")), "Error, if funcrets are defined then the language must be python. However, I found model "+epInfo.getModelInfo().name+" with a funcret defined"

                for epFuncRet in epFuncRetsList:
                    epArgsList.append(epFuncRet)

                epArgNamesList=[]
                for epArg in epArgsList:
                    epArgNamesList.append(epArg.getDataObject().getName())


                epArgs=listString(epArgNamesList)

                content={"epName":uniqueEPName,
                         "epArgs":epArgs}
                resultEntryPoint=map(content,programTemplate,snippetName="ep",copy=True)

                compositionInfo=deploymentUnitInfo.getDeploymentInfo().getCoupledInfo().getCompositionInfo()
                # add any required gets
                epContent=[]
                epArgsList=epInfo.getArgsList()
                for epArg in epArgsList:
                    remoteDataInfoList=compositionInfo.connections(epArg,"in")
                    for remoteData in remoteDataInfoList:
                        if compositionInfo.connectionType(epArg,remoteData)=="putget":
                            epContent.append({"epName":"get"
                                              ,"epArgs":epArg.getDataObject().getName()+","+epArg.getID()})
                            break # we only need one get to be specified as any other logic is handled internally
                content={"snippet":epContent}
                resultGetMap=map(content,programTemplate,snippetName="eps",copy=True)

                # add any required puts
                epContent=[]
                epArgsList=epInfo.getArgsFuncsList()
                for epArg in epArgsList:
                    remoteDataInfoList=compositionInfo.connections(epArg,"out")
                    for remoteData in remoteDataInfoList:
                        if compositionInfo.connectionType(epArg,remoteData)=="putget":
                            epContent.append({"epName":"put"
                                              ,"epArgs":epArg.getDataObject().getName()+","+epArg.getID()})
                            break
                        elif remoteData.getEPInfo().getModelInfo().name=="bfg:" and compositionInfo.coupledInfo.getDeploymentInfo().getNumSUs()>1:
                            # If remote name is bfg: then it is a convergence.
                            # If, in addition, numSUs>1 then I need to "put" my
                            # value to make sure other SU's receive the
                            # condition value.
                            epContent.append({"epName":"put"
                                              ,"epArgs":epArg.getDataObject().getName()+","+epArg.getID()})
                            convergeModelName=modelName
                            break

                content={"snippet":epContent}
                resultPutMap=map(content,programTemplate,snippetName="eps",copy=True)

                # determine my rate
                loopIndex=varid-1
                if loopIndex>0: # I am in a loop
                    if int(frequencyString)>1: # I am called less than every iteration
                        content=[]
                        content.append(resultActiveModel)
                        content.append(resultGetMap)
                        content.append(resultEntryPoint)
                        content.append(resultPutMap)
                        content={"condition":"mod(i"+str(loopIndex)+","+frequencyString+")=="+offsetString,"content":content}
                        result=map(content,programTemplate,snippetName="if",copy=True)
                        content={"condition":modelInfo.name+"Thread()","content":result}
                        result=map(content,programTemplate,snippetName="if",copy=True)
                        root.append(result)
                    else:
                        content=[]
                        content.append(resultActiveModel)
                        content.append(resultGetMap)
                        content.append(resultEntryPoint)
                        content.append(resultPutMap)
                        content={"condition":modelInfo.name+"Thread()","content":content}
                        result=map(content,programTemplate,snippetName="if",copy=True)
                        root.append(result)
                else:
                    content=[]
                    content.append(resultActiveModel)
                    content.append(resultGetMap)
                    content.append(resultEntryPoint)
                    content.append(resultPutMap)
                    content={"condition":modelInfo.name+"Thread()","content":content}
                    result=map(content,programTemplate,snippetName="if",copy=True)
                    root.append(result)


        #else: # silently ignore
    return root,convergeModelName

def genSUControl(sequenceUnit,BFG2RunType,BFG2ROOT):
    templatedir = os.path.join(BFG2ROOT,"templates")
    template=bfgutils.xmlread(os.path.join(templatedir,"fortran90_control.xml"))

    deploymentInfo=sequenceUnit.getDeploymentUnitInfo().getDeploymentInfo()

    models=sequenceUnit.getModels()
    modelNames=sequenceUnit.getModelNames()
    eps=deploymentInfo.getScheduleEPs2()

    epContent=[]
    for ep in eps:
        if ep.getModelInfo() in models:
            epContent.append({"epName":ep.epName
                             ,"epArgs":""})
    content={"snippet":epContent}
    suInfo=map(content,template,snippetName="eps")
    return suInfo
