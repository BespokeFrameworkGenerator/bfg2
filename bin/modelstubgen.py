import f77modelstubgen
import f90modelstubgen

class GenerationError(Exception):
    def __init__(self, value):
        self.value = "Generation Error: "+value
    def __str__(self):
        return repr(self.value)

def genAll(coupledInfo,BFG2ROOT):
    definitionInfoList=coupledInfo.getDefinitionsInfo(internal=False)
    # generate a model stub for each model definition
    for definitionInfo in definitionInfoList :
        genModel(definitionInfo,BFG2ROOT)
      
def genModel(modelInfo,BFG2ROOT):

    from lxml import etree as ET
    code=modelInfo.gen(BFG2ROOT)
    result=ET.ElementTree(code)
    #result.write("test.f90",method="text")
    print(ET.tostring(code,method="text",pretty_print=True))
    return
