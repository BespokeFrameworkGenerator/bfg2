#! /usr/bin/env python

# BFG2 alpha python script
# CNC RF Created on 4th April 2006

import shutil
import sys
import os
from optparse import OptionParser
import libxml2
import libxslt

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0alpha"
parser = OptionParser(usage)

parser.add_option("-d", "--directory", dest="OutDir", help="Output generated files to dir")
parser.add_option("-f", "--file", dest="Filename", help="Output filename")
parser.add_option("-n", "--names", dest="Names", action="store_true", default=False, help="Output variable names rather than the number of variables")

(options, args) = parser.parse_args()

#if len(args) != 1:
#  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
# print "Working dir", os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
# print "BFG2ROOT",BFG2ROOT
# xsltdir = os.path.join(BFG2ROOT,"xslt")
visdir = os.path.join(BFG2ROOT,"vis")
# print "xsltdir",xsltdir

destfile = options.Filename if options.Filename else "CoupledGraph.graphml"
outdir = options.OutDir if options.OutDir else ""


try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

def libxsltErrorCallback(ctx, str):
  global error_msg
  print "%s %s" % (ctx, str)
  sys.exit(1)

libxml2.registerErrorHandler(libxsltErrorCallback, "")

try:
  styledoc = libxml2.parseFile(os.path.join(visdir,"graphmlgen.xsl"))
  style = libxslt.parseStylesheetDoc(styledoc)
  doc = libxml2.parseFile(CoupledDocumentPathNoQuote)
  result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath, "Names" : '"'+str(options.Names)+'"' })
  style.saveResultToFilename(os.path.join(outdir, destfile), result, 0)
  style.freeStylesheet()
  doc.freeDoc()
  result.freeDoc()
except:
  print "Parse error in graphml.xsl, aborting"
  sys.exit(1)
print("Created file {0}".format(os.path.join(outdir, destfile)))


