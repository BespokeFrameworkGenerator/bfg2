#! /usr/bin/env python

# BFG1toBFG2 alpha python script
# CNC RF Created on 30th May 2006
# RF Modified on 1st December 2010
#    use lxml and use a separate xmlprocess function

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as ET
import bfgutils
import check

usage = "usage: %prog [options] BFG1CoupledDoc.xml"
version = "1.0alpha"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-d", "--directory", dest="OutDir",
                  help="output generated files to DIR", metavar="DIR")
parser.add_option("-n", "--novalidate",
                  action="store_false", dest="validate", default=True,
                  help="do not validate bfg2 output documents")

(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
wd=os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
xsltdir = os.path.join(BFG2ROOT,"xslt/bfg1tobfg2")

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if options.OutDir and not(os.path.isdir(options.OutDir)):
  print "error, non-existant target directory '%s'" % (options.OutDir)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

print "CoupledDocumentPath",CoupledDocumentPath
print "CoupledDocumentPathNoQuote",CoupledDocumentPathNoQuote

if options.OutDir:
  OutDirNoQuote = os.path.abspath(options.OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
else:
  OutDirNoQuote = os.path.abspath(os.path.dirname(CoupledDocumentPathNoQuote))
  OutDir = "'" + OutDirNoQuote + "'"
  
print "Output dir is",OutDirNoQuote
os.chdir(OutDirNoQuote)

if options.verbose:
  print "BFG1toBFG2 alpha ... autogentastic."

if options.verbose:
  print "Creating BFG2 Coupled Document ",
  print ""
result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Coupled.xsl"),
                           xmlfile=CoupledDocumentPathNoQuote)
result.write(os.path.join(OutDirNoQuote,"bfg2coupled.xml"),pretty_print=True)

if options.verbose:
  print "Creating BFG2 Composition Document ",
  print ""
result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Composition.xsl"),
                           xmlfile=CoupledDocumentPathNoQuote)
result.write(os.path.join(OutDirNoQuote,"bfg2composition.xml"),pretty_print=True)

if options.verbose:
  print "Creating BFG2 Deployment Document ",
  print ""
result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Deployment.xsl"),
                           xmlfile=CoupledDocumentPathNoQuote)
result.write(os.path.join(OutDirNoQuote,"bfg2deployment.xml"),pretty_print=True)

if options.verbose:
  print "Creating BFG2 Model Documents ",
  print ""
bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Models.xsl"),
                    xmlfile=CoupledDocumentPathNoQuote)

if options.validate:
  print "Checking that the generated BFG2 xml is valid and consistent..."
  os.chdir(wd)
  check.run(os.path.join(OutDirNoQuote,"bfg2coupled.xml"))
  print "Done"
  print ""

if options.verbose:
  print "BFG1toBFG2 has left the building."
