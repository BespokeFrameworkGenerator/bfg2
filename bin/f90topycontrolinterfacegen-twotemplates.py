import os
import bfgutils


def run(deploymentUnitInfo, BFG2ROOT):

  print("[MODULE] {0}".format(__name__))

  templatedir = os.path.join(BFG2ROOT, "templates")
  templateFile = "fortran90_to_python_mep.xml"
#  templateFile = "fortran90_to_python_args.xml"

  scheduleInfo = deploymentUnitInfo.getDeploymentInfo().getScheduleInfo()
  epInfoList = []

  for modelInfo in [x for x in deploymentUnitInfo.getModels() if x.language == "python" and not x.programConformance() ]:
      print("[MODELINFO] in {0}\n{1}\n[/MODELINFO]".format(modelInfo.__module__, dir(modelInfo)))

      for epName in scheduleInfo.getEPNames(modelInfo.name):
          epInfo = modelInfo.getEP(epName)
          argsMap = epInfo.getArgsMap()

          print("[epInfo] is type {0}\n{1}\n[/epInfo]".format(type(epInfo), dir(epInfo) ))
          uniqueEPName = epInfo.getUniqueName()
          print("--=: epInfo.getArgsMap() len={0} :==-\n".format(len(argsMap)) )
          argTypes = []
          argNames = [ "arg"+str(i) for i in range(len(argsMap)) ]

          for i, (k,v) in enumerate(argsMap.items()):
              print("    {0} : {1}\n".format(k, v.getInPlaceTagDatatype() ) )
              argTypes.append( "{0} *{1}".format(v.getInPlaceTagDatatype(), argNames[i-1]) )

          if not argsMap:
              templateFile = "fortran90_to_python_mep.xml"
              epInfoList.append( { "pyModule" : modelInfo.name,
                                   "pyClass" : modelInfo.name,	# Should be class name
                                   "pyMethod" : epName,
                                   "epNamef90": f90toclink(uniqueEPName) } )
          else:
              templateFile = "fortran90_to_python_args.xml"
              epInfoList.append( { "pyModule" : modelInfo.name,
                                   "pyMethod" : epName,
                                   "epNamef90": f90toclink(uniqueEPName),
                                   "epArgsDefns" : ", *".join(argTypes),
                                   "epArgs" : ", ".join(argNames)  } )


  mapping = {"code" : epInfoList}
  controlTemplate = bfgutils.xmlread(os.path.join(templatedir, templateFile))
  result = bfgutils.map(mapping, controlTemplate)

  headerFiles = ["pyobcall.h"]
  headerText = "#include <{0}>\n"

  headersRoot = bfgutils.ET.Element("headers")

  for f in headerFiles:
      header = bfgutils.ET.Element("header")
      header.text = headerText.format(f)
      headersRoot.append(header)

  result.getroot().insert(0, headersRoot)

#  print("[RESULT] is type {0}\n{1}[\RESULT]".format(type(result), bfgutils.ET.tostring(result, pretty_print=True)) )
  result.write(modelInfo.name + ".c", method="text")
#  result.write("BFG2Control"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")


def f90toclink(name):
    # assume that we add an underscore
    # need to add other options that can be specified by the user
    return name+"_"
