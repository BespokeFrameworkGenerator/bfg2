#! /usr/bin/env python

# BFG2 model stub generation python script
# RF Created on 31st May 2008
# RF Modified on 25th November 2010
#    use lxml and use a separate xmlprocess function
import sys
import os
import traceback
from optparse import OptionParser
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo,xmlread,model,GenerationError
from check import validXML,validate
import modelstubgen

usage = "usage: %prog [options] BFG2[Definition|Coupled]Document.xml"
version = "1.0"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-d", "--directory", dest="OutDir",
                  help="output generated files to DIR", metavar="DIR")
parser.add_option("-a", "--append", dest="AppendStub",
                  help="append specified name to output filename(s)", metavar="STUB")
parser.add_option("-k", "--keepOrigName", action="store_true", dest="NoStub", default=False,
                  help="do not append anything to filename(s)")
parser.add_option("-m", "--modifyvars", action="store_true", dest="ModifyVars", default=False,
                  help="add code that modifies any inout data")
parser.add_option("-p", "--printvars", action="store_true", dest="PrintVars", default=False,
                  help="add code that prints out the data values")
parser.add_option("-v", "--validate", action="store_true", dest="validate", default=False,
                  help="validate BFG2 xml before generating stubs")
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

wd=os.getcwd()
# work out locations relative to our script
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if options.OutDir and not(os.path.isdir(options.OutDir)):
  print "error, non-existant target directory '%s'" % (options.OutDir)
  sys.exit(1)

if options.OutDir:
  OutDirNoQuote = os.path.abspath(options.OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
else:
  OutDirNoQuote = os.path.abspath(".")
  OutDir = "'" + OutDirNoQuote + "'"

if options.NoStub:
  modifier=""
elif options.AppendStub:
  modifier=AppendStub
else:
  modifier="_gen"

DocumentPathNoQuote = os.path.abspath(args[0])
DocumentPath = "'" + DocumentPathNoQuote + "'"

if options.verbose:
  print "BFG2 ... autogentastic."
  sys.stdout.flush()

# if coupled then iterate over each model document
doc=validXML(DocumentPathNoQuote)

if validate(doc,os.path.normpath(os.path.join(BFG2ROOT,'schema/coupled.xsd')),silent=True):
  print "This is a BFG2 coupled document."
  print "Generating a stub for each model in this coupling."
  if options.validate:
    print "Checking that the xml is valid and consistent..."
    check.run(args[0])
    print "Done"
    print ""

  DocumentDir = os.path.dirname(DocumentPathNoQuote)
  coupledDocInfo=coupledInfo(DocumentPathNoQuote,DocumentDir,debug=False)
  try:
    definitionInfoList=coupledDocInfo.getDefinitionsInfo(internal=False)
    # generate a model stub for each model definition
    for definitionInfo in definitionInfoList :
        code=definitionInfo.gen(BFG2ROOT)
        result=ET.ElementTree(code)
        location=os.path.join(OutDirNoQuote,definitionInfo.name+modifier+".f90")
        result.write(location,method="text")
  except (OSError, IOError, GenerationError) as e:
    print "Error:",e
    exit(1)
  except Exception as e:
    print "Error, unexpected exception:\n"
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print exc_type
    print exc_value
    traceback.print_tb(exc_traceback)
    exit(1)

elif validate(doc,os.path.normpath(os.path.join(BFG2ROOT,'schema/definition.xsd')),silent=True):
  print "This is a BFG2 definition document."
  mymodel=model()
  mymodel.readxmlfile(DocumentPathNoQuote)
  try:
    code=mymodel.codegen()
    f = open(mymodel.name+".f90", 'w')
    f.write(code)
    f.close()
  except (OSError, IOError, GenerationError) as e:
    print "Error:",e
    exit(1)
  except Exception as e:
    print "Error, unexpected exception:\n"
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print exc_type
    print exc_value
    traceback.print_tb(exc_traceback)
    exit(1)
else:
  print "Error: document is not a valid BFG2 coupled or definition document"
  exit(1)

if options.verbose:
  print "BFG2 has left the building."
  sys.stdout.flush()
