import os
import bfgutils
from bfgutils import coupledInfo, map, getValue
from lxml import etree as ET

def run(deploymentUnitInfo,BFG2ROOT):

  print

  templatedir = os.path.join(BFG2ROOT,"templates")

  # Read in our XML template
  inplaceTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran77_to_fortran90.xml"))

  functionInfo=[]
  f90functionInfo=[]
  # create our "get" interface, and our "put" interface separately
  for direction in ["in","out"]:
    if direction=="in" : dataDirection="out"
    elif direction=="out" : dataDirection="in"
    else: assert False, "Error"

    keepDataInfo=None

    # Create the information for the required template
    # for each model in this du
    dataInThisEP=[]
    for model in deploymentUnitInfo.getModels():
      # for each entrypoint in this model
      epInThisDU=[]
      for epInfo in model.getEPs():
        # skip this ep if it has no inplace requirements
        if epInfo.requiresInPlace():
          # for each field in this entrypoint
          dataInThisEP=[]
          for dataInfo in epInfo.getFields():
            # skip this field if it has no inplace requirements or is not the right direction
            if dataInfo.requiresInPlace() and dataInfo.direction==direction:

              keepDataInfo=dataInfo
              f90functionInfo.append({"UniqueInPlaceProcedureName":dataInfo.getUniqueFunctionName(),
                                      "PutOrGet":dataInfo.getGenericFunctionName(direction),
                                      "InPlaceDatatype":dataInfo.dataType,
                                      "InPlaceIntent":dataDirection,
                                      "InPlaceDimensionDeclaration":dataInfo.arrayDimsSize(),
                                      "InPlaceTagDatatype":dataInfo.getInPlaceTagDatatype("f90")})
              dataInThisEP.append({"DataID":dataInfo.fieldID,
                                   "ModelName":epInfo.getModelInfo().name,
                                   "EPName":epInfo.epName,
                                   "InstanceID":"0",
                                   "UniqueInPlaceProcedureName":dataInfo.getUniqueFunctionName()})

          epInThisDU.append({"GlobalEPID":epInfo.getGlobalID(),
                             "DataInThisEP":dataInThisEP})

    # only output if we have some content
    if dataInThisEP:
      # TBD we currently assume that all datainfo uses the same generic function name
      # TBD we currently assume that all datainfo uses the same inplace tag datatype

      functionInfo.append({"PutOrGet":keepDataInfo.getGenericFunctionName(direction),
                           "DUID":str(deploymentUnitInfo.getID()),
                           "InPlaceIntent":dataDirection,
                           "InPlaceTagDatatype":keepDataInfo.getInPlaceTagDatatype("f90"),
                           "EPInThisDU":epInThisDU})

  f77mapping={"snippet":functionInfo}
  result=map(f77mapping,inplaceTemplate,snippetName="f77interface")
  ETresult=ET.ElementTree(result)
  ETresult.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+"_f77wrapper.f90",method="text")

  f90mapping={"snippet":f90functionInfo}
  result=map(f90mapping,inplaceTemplate,snippetName="f90interface")
  ETresult=ET.ElementTree(result)
  ETresult.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+"_f90wrapper.f90",method="text")

