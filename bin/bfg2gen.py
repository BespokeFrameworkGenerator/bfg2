import shutil
import sys
import os
from optparse import OptionParser
import libxml2
import libxslt
import check

def run(BFG2ROOT,fileName,verbose,validate,OutDir,keep):

  # work out BFG2ROOT from script location
  # print "Working dir", os.getcwd()
  # pathname=os.path.dirname(sys.argv[0])
  # BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
  # print "BFG2ROOT",BFG2ROOT
  xsltdir = os.path.join(BFG2ROOT,"xslt")
  # print "xsltdir",xsltdir

  try:
    f = open(fileName, 'r')
  except IOError, (errno, strerror):
    print "I/O error(%s) for file %s: %s" % (errno, fileName, strerror)
    sys.exit(1)

  if OutDir and not(os.path.isdir(OutDir)):
    print "error, non-existant target directory '%s'" % (OutDir)
    sys.exit(1)

  CoupledDocumentPathNoQuote = os.path.abspath(fileName)
  CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

  # print "CoupledDocumentPath",CoupledDocumentPath

  # validate before we change directory with -d option as it messes up
  # the determination of the base directory BFG2ROOT
  if validate:
    print ""
    print "Here is where we should validate the xml wrt the schema"
    print "Unfortunatly the appropriate python bindings are not yet available"
    print ""
    print "Checking that the xml is consistent..."
    check.run(fileName)
    print "Done"
    print ""

  if OutDir:
    OutDirNoQuote = os.path.abspath(OutDir)
    OutDir = "'" + OutDirNoQuote + "'"
    os.chdir(OutDirNoQuote)
  else:
    OutDirNoQuote = os.path.abspath(".")
    OutDir = "'" + OutDirNoQuote + "'"
  
  # print "Output dir is",OutDirNoQuote

  CommFormsPath = "'" + OutDirNoQuote + "/" + "CommForms.xml" + "'"
  
  if verbose:
    print "BFG2 beta ... autogentastic."

  shutil.copyfile(os.path.join(xsltdir,"EPTemplate.xml"),"EPTemplate.xml")

  def libxsltErrorCallback(ctx, str):
    global error_msg
    print "%s %s" % (ctx, str)
    sys.exit(1)

  libxml2.registerErrorHandler(libxsltErrorCallback, "")

  if verbose:
    print "Generating CommunicationForms XML doc"
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"NewArgPassingTests.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("EPTemplate.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("CommForms.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in NewArgPassingTests.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "Generating Control Code ",
    print "[0]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase0.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("EPTemplate.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    #result = style.applyStylesheet(doc, {})
    style.saveResultToFilename("MainPhase0Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase0.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[1]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase1.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase0Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("MainPhase1Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase1.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[1.5]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase15.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase1Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("MainPhase15Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase15.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[2]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase2.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase15Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath})
    style.saveResultToFilename("MainPhase2Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase2.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[3]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase3.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase2Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath})
    style.saveResultToFilename("MainPhase3Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase3.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[3.5]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase35.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase3Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath})
    style.saveResultToFilename("MainPhase35Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase35.xsl, aborting"

  if verbose:
    print "[4]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"MainPhase4.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase35Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath , "inRoutine" : "'get'" , "outRoutine" : "'put'" })
    style.saveResultToFilename("MainPhase4Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in MainPhase4.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[gen]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"CodeGen.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("MainPhase4Result.xml")
    result = style.applyStylesheet(doc, { "OutDir" : OutDir , "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath })
    style.freeStylesheet()
    doc.freeDoc()
  except:
    print "Parse error in CodeGen.xsl, aborting"
    sys.exit(1)

  result.freeDoc()
  if verbose:
    print '"BFG2Main*.f90"'

  if verbose:
    print "Generating f90 Control File",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"f90ControlFile.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile(CoupledDocumentPathNoQuote)
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath , "OutDir" : OutDir })
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in f90ControlFile.xsl, aborting"
    sys.exit(1)
  if verbose:
    print '"BFG2Control.nam"'

  #shutil.copyfile(os.path.join(xsltdir,"TargetTemplate.xml"),"TargetTemplate.xml")

  if verbose:
    print "Generating f90 Target Specific Code",
    print "[0]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"TargetPhase0.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("EPTemplate.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("TargetPhase0Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in TargetPhase0.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[1]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"TargetPhase1.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("TargetPhase0Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("TargetPhase1Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in TargetPhase1.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[1.5]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"TargetPhase15.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("TargetPhase1Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("TargetPhase15Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in TargetPhase15.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[2]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"TargetPhase2.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("TargetPhase15Result.xml")
    result = style.applyStylesheet(doc, { "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("TargetPhase2Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in TargetPhase2.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[gen]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"CodeGen.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("TargetPhase2Result.xml")
    result = style.applyStylesheet(doc, { "FileName" : "'BFG2Target.f90'" , "OutDir" : OutDir , "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath })
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in CodeGen.xsl, aborting"
    sys.exit(1)
  if verbose:
    print '"BFG2Target.f90"'

  if verbose:
    print "Generating f90 InPlace Code [1]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"InPlacePhase1.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("EPTemplate.xml")
    result = style.applyStylesheet(doc, { "EPName" : "'BFG2InPlace'" , "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("InPlacePhase1Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in InPlacePhase1.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[2]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"InPlacePhase2.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("InPlacePhase1Result.xml")
    result = style.applyStylesheet(doc, { "inRoutine" : "'get'" , "outRoutine" : "'put'" , "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath })
    style.saveResultToFilename("InPlacePhase2Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in InPlacePhase2.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[3]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"InPlacePhase3.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("InPlacePhase2Result.xml")
    result = style.applyStylesheet(doc, { "inRoutine" : "'get'" , "outRoutine" : "'put'" , "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath })
    style.saveResultToFilename("InPlacePhase3Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in InPlacePhase3.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[4]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"InPlacePhase4.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("InPlacePhase3Result.xml")
    result = style.applyStylesheet(doc, { "inRoutine" : "'get'" , "outRoutine" : "'put'" , "CoupledDocument" : CoupledDocumentPath, "CommForms" : CommFormsPath })
    style.saveResultToFilename("InPlacePhase4Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in InPlacePhase4.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[5]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"InPlacePhase5.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("InPlacePhase4Result.xml")
    result = style.applyStylesheet(doc, { "inRoutine" : "'get'" , "outRoutine" : "'put'" , "CoupledDocument" : CoupledDocumentPath })
    style.saveResultToFilename("InPlacePhase5Result.xml", result, 0)
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in InPlacePhase5.xsl, aborting"
    sys.exit(1)

  if verbose:
    print "[gen]",
    sys.stdout.flush()
  try:
    styledoc = libxml2.parseFile(os.path.join(xsltdir,"CodeGen.xsl"))
    style = libxslt.parseStylesheetDoc(styledoc)
    doc = libxml2.parseFile("InPlacePhase5Result.xml")
    result = style.applyStylesheet(doc, { "FileName" : "'BFG2InPlace.f90'" , "OutDir" : OutDir , "CoupledDocument" : CoupledDocumentPath , "CommForms" : CommFormsPath})
    style.freeStylesheet()
    doc.freeDoc()
    result.freeDoc()
  except:
    print "Parse error in CodeGen.xsl, aborting"
    sys.exit(1)

  if verbose:
    print '"BFG2InPlace*.f90"'
    sys.stdout.flush()

  if not(keep):
    os.remove("EPTemplate.xml")
    os.remove("CommForms.xml")
    os.remove("MainPhase0Result.xml") 
    os.remove("MainPhase1Result.xml")
    os.remove("MainPhase15Result.xml")
    os.remove("MainPhase2Result.xml")
    os.remove("MainPhase3Result.xml")
    os.remove("MainPhase35Result.xml")
    os.remove("MainPhase4Result.xml")
    os.remove("TargetPhase0Result.xml")
    os.remove("TargetPhase1Result.xml")
    os.remove("TargetPhase15Result.xml")
    os.remove("TargetPhase2Result.xml")
    os.remove("InPlacePhase1Result.xml")
    os.remove("InPlacePhase2Result.xml")
    os.remove("InPlacePhase3Result.xml")
    os.remove("InPlacePhase4Result.xml")
    os.remove("InPlacePhase5Result.xml")

  if verbose:
    print "BFG2 has left the building."
    sys.stdout.flush()
