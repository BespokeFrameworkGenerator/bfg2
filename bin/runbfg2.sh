#!/bin/sh 

# IRH MOD: Added -o oasisvis command line argument and associated
# oasisvis parameters to XSL processing calls
# IRH MOD: Added -Xss2m option to increase JVM stack size to 2Mb
# from default 256Kb.  Xalan doesn't currently optimise tail recursion
# (as at version 2.7.1) so there's a danger of stack overflow with
# some of my tail-recursive functions in oasis4.xsl.  The depth of
# recursion could be reduced using various divide-and-conquer approaches
# if it proves necessary (on some systems, a maximum stack size of 1Mb
# per Java thread is enforced, which could prove a problem).

trap "exit" 1 2 3 6 15
BFG2=$HOME/bfg2
XSLT=$BFG2/xslt
JAVA=java
JAVA_OPTIONS=-Xss2m
XSLT_CLASS=org.apache.xalan.xslt.Process
#OUT_DIR=$HOME/metadata-bfg2/genie/XML2/src
IN=-IN
OUT=-OUT
XSL=-XSL
#OPTIONS=-TT
PARAM=-PARAM
MPIVIS='false'
OASISVIS='false'
TONULL='' #'>& /dev/null'
STATEDUMP='false'

while getopts "amotkd:f:qs" flag; do
  case $flag in
    a ) ARCHIVE="true";;
    m ) MPIVIS='true';;
    o ) OASISVIS='true';;
    t ) TIME="time";;
    k ) KEEP="-k";;
    d ) OUT_DIR=$OPTARG;;
    f ) INPUT=$OPTARG;;
    q ) QUIET="-q";;
    s ) STATEDUMP="true";;
  esac
done

if [ ! $OUT_DIR ]
then
echo 'Usage: runbfg2.sh [-amtkqs] -f <input xml> -d <destination directory>'
exit
fi

if [ ! $INPUT ]
then
echo 'Usage: runbfg2.sh [-amtkqs] -f <input xml> -d <destination directory>'
exit
fi

#Handle relative input paths
if [ ${INPUT:0:1} != '~' ] && [ ${INPUT:0:1} != '/' ]
then
INPUT=$PWD'/'$INPUT
fi
if [ ${OUT_DIR:0:1} != '~' ] && [ ${OUT_DIR:0:1} != '/' ]
then
OUT_DIR=$PWD'/'$OUT_DIR
fi

if [ ! $QUIET ]
then
echo 'Generating data structure'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Data-struct/ConstructDS.xsl $OUT $OUT_DIR/DS.xml $PARAM CoupledDocument $INPUT $OPTIONS  $TONULL

if [ ! $QUIET ]
then
echo 'Generating main code'
echo -n 'Main phase 1'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/1/MainPhase1.xsl $IN $XSLT/Utils/EPTemplate.xml $OUT $OUT_DIR/MainPhase1Result.xml $PARAM CoupledDocument $INPUT $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 2'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/2/MainPhase2.xsl $IN $OUT_DIR/MainPhase1Result.xml $OUT $OUT_DIR/MainPhase2Result.xml $PARAM CoupledDocument $INPUT $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 3'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/3/MainPhase3.xsl $IN $OUT_DIR/MainPhase2Result.xml $OUT $OUT_DIR/MainPhase3Result.xml $PARAM CoupledDocument $INPUT $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 4'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/4/MainPhase4.xsl $IN $OUT_DIR/MainPhase3Result.xml $OUT $OUT_DIR/MainPhase4Result.xml $PARAM CoupledDocument $INPUT $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 5'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/5/MainPhase5.xsl $IN $OUT_DIR/MainPhase4Result.xml $OUT $OUT_DIR/MainPhase5Result.xml $PARAM CoupledDocument $INPUT $PARAM CommForms $OUT_DIR/DS.xml $PARAM stateDump $STATEDUMP $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 6'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/6/MainPhase6.xsl $IN $OUT_DIR/MainPhase5Result.xml $OUT $OUT_DIR/MainPhase6Result.xml $PARAM CoupledDocument $INPUT $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo ' 7'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/7/MainPhase7.xsl $IN $OUT_DIR/MainPhase6Result.xml $OUT $OUT_DIR/MainPhase7Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM mpivis $MPIVIS $PARAM oasisvis $OASISVIS $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL

if [ $STATEDUMP = 'true' ]
then
 if [ ! $QUIET ]
 then
 echo ' 8'
 fi
 eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Main/8/MainPhase8.xsl $IN $OUT_DIR/MainPhase7Result.xml $OUT $OUT_DIR/MainPhase8Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
 if [ ! $QUIET ]
 then
 echo 'Main phase code gen'
 fi
 eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/CodeGen/CodeGen.xsl $IN $OUT_DIR/MainPhase8Result.xml $PARAM CoupledDocument $INPUT $PARAM OutDir $OUT_DIR $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
else 
 if [ ! $QUIET ]
 then
 echo 'Main phase code gen'
 fi
 eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/CodeGen/CodeGen.xsl $IN $OUT_DIR/MainPhase7Result.xml $PARAM CoupledDocument $INPUT $PARAM OutDir $OUT_DIR $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
fi

if [ ! $QUIET ]
then
echo 'Generating f90 Control file'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Control/f90ControlFile.xsl $IN $INPUT $PARAM CoupledDocument $INPUT $PARAM OutDir $OUT_DIR $OPTIONS $TONULL

if [ ! $QUIET ]
then
echo 'Generating f90 target-specific code'
echo -n 'Target Phase 1'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Target/1/TargetPhase1.xsl $IN $XSLT/Utils/EPTemplate.xml $OUT $OUT_DIR/TargetPhase1Result.xml $PARAM CoupledDocument $INPUT $PARAM mpivis $MPIVIS $PARAM oasisvis $OASISVIS $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 2'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Target/2/TargetPhase2.xsl $IN $OUT_DIR/TargetPhase1Result.xml $OUT $OUT_DIR/TargetPhase2Result.xml $PARAM CoupledDocument $INPUT $PARAM CommForms $OUT_DIR/DS.xml $PARAM OutDir $OUT_DIR $PARAM mpivis $MPIVIS $PARAM oasisvis $OASISVIS $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 3'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Target/3/TargetPhase3.xsl $IN $OUT_DIR/TargetPhase2Result.xml $OUT $OUT_DIR/TargetPhase3Result.xml $PARAM CoupledDocument $INPUT $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo ' 4'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Target/4/TargetPhase4.xsl $IN $OUT_DIR/TargetPhase3Result.xml $OUT $OUT_DIR/TargetPhase4Result.xml $PARAM CoupledDocument $INPUT $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo 'Target Phase code gen'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/CodeGen/CodeGen.xsl $IN $OUT_DIR/TargetPhase4Result.xml $PARAM CoupledDocument $INPUT $PARAM FileName BFG2Target.f90 $PARAM OutDir $OUT_DIR $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL

if [ ! $QUIET ]
then
echo 'Generating InPlace code'
echo -n 'InPlace phase 1'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/InPlace/1/InPlacePhase1.xsl $IN $XSLT/Utils/EPTemplate.xml $OUT $OUT_DIR/InPlacePhase1Result.xml $PARAM CoupledDocument $INPUT $PARAM EPName BFG2InPlace $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 2'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/InPlace/2/InPlacePhase2.xsl $IN $OUT_DIR/InPlacePhase1Result.xml $OUT $OUT_DIR/InPlacePhase2Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 3'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/InPlace/3/InPlacePhase3.xsl $IN $OUT_DIR/InPlacePhase2Result.xml $OUT $OUT_DIR/InPlacePhase3Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo -n ' 4'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/InPlace/4/InPlacePhase4.xsl $IN $OUT_DIR/InPlacePhase3Result.xml $OUT $OUT_DIR/InPlacePhase4Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM mpivis $MPIVIS $PARAM oasisvis $OASISVIS $PARAM CommForms $OUT_DIR/DS.xml $PARAM OutDir $OUT_DIR $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo ' 5'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/InPlace/5/InPlacePhase5.xsl $IN $OUT_DIR/InPlacePhase4Result.xml $OUT $OUT_DIR/InPlacePhase5Result.xml $PARAM CoupledDocument $INPUT $PARAM inRoutine get $PARAM outRoutine put $PARAM mpivis $MPIVIS $PARAM oasisvis $OASISVIS $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL
if [ ! $QUIET ]
then
echo 'InPlace code gen'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/CodeGen/CodeGen.xsl $IN $OUT_DIR/InPlacePhase5Result.xml $PARAM CoupledDocument $INPUT $PARAM FileName BFG2InPlace.f90 $PARAM OutDir $OUT_DIR $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL


if [ ! $QUIET ]
then
echo 'Generate Config files'
fi
eval $TIME $JAVA $JAVA_OPTIONS $XSLT_CLASS $XSL $XSLT/Config/1/ConfigPhase1.xsl $IN $INPUT $PARAM CoupledDocument $INPUT $PARAM FileName BFG2InPlace.f90 $PARAM OutDir $OUT_DIR $PARAM CommForms $OUT_DIR/DS.xml $OPTIONS $TONULL


if [ ! $KEEP ]
then
if [ ! $QUIET ]
then
echo 'Removing temporary XML files'
fi
 eval rm -f $OUT_DIR/DS.xml $OUT_DIR/MainPhase*Result.xml $OUT_DIR/TargetPhase*Result.xml $OUT_DIR/InPlacePhase*Result.xml
fi

#Archive metadata into OUT_DIR
if [ $ARCHIVE ]
 then
 if [ ! $QUIET ]
 then
  echo 'Archiving metadata and BFG subversion revision number'
  echo -n 'Enter the current subversion revision number for BFG2 '
  read VERSION
  TARFILE='metadata-r'$VERSION'.tar'
  #MYROOT=`expr match "$INPUT" '\(.*\)/.*'`
  #FILE=`expr match "$INPUT" '.*/\(.*\)'`
  MYROOT=`dirname $INPUT`
  FILE=`basename $INPUT`
  CWD=$PWD
  cd $MYROOT
  echo archiving $FILE
  tar cf $TARFILE -C $MYROOT $FILE
  FILES=`$TIME $JAVA $XSLT_CLASS $XSL $XSLT/Utils/ArchiveList.xsl $IN $INPUT`
  for FILE in $FILES
  do
   MYROOT=`dirname $FILE`
   FILE=`basename $FILE`
   echo archiving $FILE
   tar rf $TARFILE -C $MYROOT $FILE
  done
 fi
 mv $TARFILE $OUT_DIR
 cd $CWD
fi
