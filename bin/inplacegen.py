#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue, functionInfo, listString

def genAll(coupledDocInfo,BFG2ROOT):
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
  # generate an inplace routine for each deployment unit
  for deploymentUnitInfo in deploymentUnits :
    genDU(deploymentUnitInfo,BFG2ROOT)


def genDU(deploymentUnitInfo,BFG2ROOT):

    templatedir = os.path.join(BFG2ROOT,"templates")

    deploymentInfo=deploymentUnitInfo.getDeploymentInfo()
    coupledDocInfo=deploymentInfo.getCoupledInfo()
    compositionDocInfo=coupledDocInfo.getCompositionInfo()

    # limit code generation to what we support
    assert deploymentUnitInfo.language=="f90","Only f90 supported so far, but found "+str(deploymentUnitInfo.language)

    # Read in our f90 XML template
    # should be based on required language but this is only f90 at the moment
    # At some point create an f90inplacegen.py
    inplaceTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_inplace.xml"))

    # determine the specific put/get functions required by this coupling
    functionNamesMap={}
    # functionNamesMap2 is used to store the mapping information in the deploymentUnit object for use later
    functionNamesMap2={}
    # functionObjectList supercedes functionNamesMap as it creates and uses a separate function class
    functionObjectList=[]
    functionNamesInList=[]
    functionNamesOutList=[]
    # for each model in this du
    for modelName in deploymentUnitInfo.getModelNames() :
       print "LOOKING AT MODEL ",modelName
       modelInfo=coupledDocInfo.getDefinitionInfo(modelName)
       # for each ep in this model
       for epInfo in modelInfo.getEPs() :
         print "LOOKING AT EP ",epInfo.epName
         # for each field in this ep
         for dataInfo in epInfo.getFields() :

           #if dataInfo.direction=="out" and dataInfo.form=="inplace":
           #  # I need an inplace routine whether I am connected or not.
           #  dataInfo.setRequiresInPlace(True)
           #else:
           #  # default to false and set to true if required
           #  dataInfo.setRequiresInPlace(False)
           dataInfo.setRequiresInPlace(False)
           # check for any required get's
           if dataInfo.direction in ["in","inout"] and \
                 (compositionDocInfo.putgetConnection(dataInfo,"in",nesting=False) or \
                    compositionDocInfo.putgetConnection(dataInfo,"in",nesting=True) or \
                    compositionDocInfo.putgetPriming(dataInfo,"in") or \
                    (modelInfo.isControlModel() and deploymentInfo.getNumSUs()>1)):
             print dataInfo.printLocation()," is put/get primed or put/get connected"
             dataInfo.setRequiresInPlace(True)
             # data is connected and/or primed by inplace call
             functionName=dataInfo.getFunctionName("in")
             if functionName not in functionNamesInList:
               functionNamesMap[functionName]=[]
               functionNamesInList.append(functionName)
             functionNamesMap[functionName].append(dataInfo)
           # check for any required put's
           if dataInfo.direction in ["out","inout"] and \
                 (compositionDocInfo.putgetConnection(dataInfo,"out",nesting=False) or \
                    compositionDocInfo.putgetConnection(dataInfo,"out",nesting=True) or \
                    compositionDocInfo.putgetPriming(dataInfo,"out") or \
                    dataInfo.form=="inplace" or \
                    (compositionDocInfo.controlConnection(dataInfo) and deploymentInfo.getNumSUs()>1)):
             print dataInfo.printLocation()," is put/get primed or put/get connected"
             dataInfo.setRequiresInPlace(True)
             # data is connected and/or primes one or more remote data
             functionName=dataInfo.getFunctionName("out")
             if functionName not in functionNamesOutList:
               functionNamesMap[functionName]=[]
               functionNamesOutList.append(functionName)
             functionNamesMap[functionName].append(dataInfo)

    if not(deploymentUnitInfo.requiresInPlace()) :
      print "THIS DU DOES NOT NEED INPLACE CODE"
      return

    functionInfoList=[]
    for functionName in functionNamesMap.keys():

      namelistInfoList=[]

      connectionDirection=dataInfo.getDirection(functionName)

      # sort the coupled data within this function by entry point
      EPMap={}
      for dataInfo in functionNamesMap[functionName]:

        print "Looking at",functionName,"and data",dataInfo.printLocation()

        index=0
        LocalPrimedInitString=""

        # associate the data with entrypoints
        ep=dataInfo.getEPInfo()
        if not ep in EPMap:
          EPMap[ep]=[]
        EPMap[ep].append(dataInfo)

      functionNamesMap2[functionName]=EPMap

      # TBD, fix hardcoded datatagtype here.
      functionObjectList.append(functionInfo(functionName,functionNamesMap[functionName],connectionDirection,"integer",EPMap))

      # for each EP that makes use of the current function
      EPInThisDU=[]
      for ep in EPMap.keys():

        print "Examining entryPoint : "+ep.epName
        # loop over data within this EP
        dataInThisEP=[]
        for dataInfo in EPMap[ep]:
          direction=dataInfo.direction
          if direction=="inout":
            direction=connectionDirection

          index+=1
          primingCodeList=[]
          if compositionDocInfo.putgetPriming(dataInfo,direction):
            # Am I either primed or I prime via put/get
            assert direction in ["in","out"], "Error"
            LocalPrimedInitString+=".true.,"
            if direction=="in" :
              # I am primed
              primingType=compositionDocInfo.primingType(dataInfo)
              if primingType=="internal":
                print dataInfo.printLocation(),"is a get that is primed internally"
                primingCodeList.append({"TargetSpecificPrimingCodeInstance":[{"TargetSpecificPrimingCodeInstanceLine":"! I am internally primed"}]})
              elif primingType=="data":
                print dataInfo.printLocation(),"is a get that is primed by data"
                primingCodeList.append({"TargetSpecificPrimingCodeInstance":[{"TargetSpecificPrimingCodeInstanceLine":"arg1="+compositionDocInfo.primingValue(dataInfo)}]})
              elif primingType=="model":
                print dataInfo.printLocation(),"is a get that is primed by a model"
                remoteDataInfoList=compositionDocInfo.getPrimingData(dataInfo)
                assert len(remoteDataInfoList)==1,"Error"
                if compositionDocInfo.primingForm(dataInfo,remoteDataInfoList[0],"in")=="argpass":
                  primingCodeList.append({"TargetSpecificPrimingCodeInstance":[{"TargetSpecificPrimingCodeInstanceLine":"! primed by argument passing"}]})
                else:
                  primingCodeList.append({"TargetSpecificPrimingCodeInstance":[{"TargetSpecificPrimingCodeInstanceLine":"HELLO1"}]})#targetSpecificComms(dataInfo,remoteDataInfoList[0],templatedir)}]})
                  assert False, "I NEED TO BE FIXED"
              elif primingType=="file":
                print dataInfo.printLocation(),"is a get that is primed by a file"
                remoteFileInfo=compositionDocInfo.getPrimingFile(dataInfo)
                primingCodeList.append({"TargetSpecificPrimingCodeInstance":targetSpecificComms(dataInfo,remoteFileInfo,templatedir,"in",line="TargetSpecificPrimingCodeInstanceLine")})
                fileInfo=dataInfo.getPrimingFileInfo()
                if fileInfo.type=="namelist":
                  # store namelist file information ready for our delarations
                  print "Found a priming from a namelist file"
                  namelistInfoList.append(fileInfo)
              else:
                print "Sorry, not yet implemented inplace priming of type ",primingType
            elif direction=="out" :
              # I prime others
              print dataInfo.printLocation(),"is a put that primes one or more gets"
              for primed in compositionDocInfo.getPrimedData(dataInfo):
                primingCodeList.append({"TargetSpecificPrimingCodeInstance":targetSpecificComms(dataInfo,primed,templatedir,"out")})
          else: # data is not associated with priming
            LocalPrimedInitString+=".false.,"
            primingCodeList.append({"TargetSpecificPrimingCodeInstance":"! I am not involved in priming"})
          if direction in ["in","inout"] and connectionDirection=="in":
            checkDirection="in"
            connectionCodeList=[]
            if compositionDocInfo.putgetConnection(dataInfo,checkDirection,includeControl=True):
              print dataInfo.printLocation(),"is a get that is connected to a put"
              remoteDataInfoList=compositionDocInfo.putgetConnections(dataInfo,checkDirection,includeControl=True)
              # data that is coupled using put/get
              for remoteDataInfo in remoteDataInfoList:
                if dataInfo.getEPInfo().isConvergenceControl() or remoteDataInfo.getEPInfo().isConvergenceControl():
                  connectionCodeList.append({"TargetSpecificCommsCodeInstance":targetSpecificControlComms(dataInfo,remoteDataInfo,"in")})
                else:
                  connectionCodeList.append({"TargetSpecificCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"in")})
            else: # I am not connected to anything
              connectionCodeList.append({"TargetSpecificCommsCodeInstance":"! I am not connected to anything"})
            firstValue=".false."
            firstIteration=".false."
            firstConnectionCodeList=[]
            lastConnectionCodeList=""
            if compositionDocInfo.putgetConnection(dataInfo,"in",nesting=True,includeControl=True):
              #print dataInfo.printLocation(),"is a get that is connected to a put"
              remoteDataInfoList=compositionDocInfo.putgetConnections(dataInfo,"in",nesting=True,includeControl=True)
              # data that is coupled using put/get
              for remoteDataInfo in remoteDataInfoList:
                if dataInfo.getEPInfo().isConvergenceControl() or remoteDataInfo.getEPInfo().isConvergenceControl():
                  firstConnectionCodeList.append({"TargetSpecificFirstCommsCodeInstance":targetSpecificControlComms(dataInfo,remoteDataInfo,"in")})
                else:
                  firstConnectionCodeList.append({"TargetSpecificFirstCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"in")})

              firstValue=".true."
              myParentLoopID,loopType=compositionDocInfo.coupledInfo.getDeploymentInfo().getScheduleInfo().getParentIterationIDAndType(dataInfo)
              if loopType is None:
                firstIteration=".true."
              elif loopType=="fixed":
                firstIteration="firstFixedIteration("+str(myParentLoopID)+")"
              elif loopType=="converge":
                if dataInfo.getEPInfo().isConvergenceControl():
                  firstIteration="firstConvergeControl("+str(dataInfo.getEPInfo().getID())+")"
                else:
                  firstIteration="firstConvergeIteration("+str(myParentLoopID)+")"
              else:
                assert False,"Error, unexpected loop type returned '"+loopType+"'"
            dataInThisEPMap={"DataID":dataInfo.fieldID,
                           "ModelName":ep.getModelInfo().name,
                           "EPName":ep.epName,
                           "InstanceID":"0",
                           "DataSize":dataInfo.size,
                           "LocalPrimedIndex":index,
                           "firstValue":firstValue,
                           "lastValue":".false.", # always false as this is a get
                           "firstIteration":firstIteration,
                           "lastIteration":".false.", # always false as this is a get
                           "TargetSpecificPrimingCode":primingCodeList,
                           "TargetSpecificCommsCode":connectionCodeList,
                           "TargetSpecificFirstCommsCode":firstConnectionCodeList,
                           "TargetSpecificLastCommsCode":lastConnectionCodeList}
            dataInThisEP.append(dataInThisEPMap)


          if direction in ["out","inout"] and connectionDirection=="out":
            checkDirection="out"
            connectionCodeList=[]
            print "++++++ ",dataInfo
            # test for same nesting and more nested ..
            if compositionDocInfo.putgetConnection2(dataInfo,checkDirection,includeControl=True):
              # I require at least one put at the same nesting level as me
              print dataInfo.printLocation(),"is a put that is connected to one or more gets"
              # find all gets connected to my put at the same nesting level as me
              remoteDataInfoList=compositionDocInfo.putgetConnections2(dataInfo,checkDirection,includeControl=True)
              for remoteDataInfo in remoteDataInfoList:
                if dataInfo.getEPInfo().isConvergenceControl() or remoteDataInfo.getEPInfo().isConvergenceControl():
                  connectionCodeList.append({"TargetSpecificCommsCodeInstance":targetSpecificControlComms(dataInfo,remoteDataInfo,"out")})
                else:
                  # For each of the gets I connect to at the same nesting level as me
                  connectionCodeList.append({"TargetSpecificCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"out")})
            else: # I am not connected to anything at the same nesting level as me
              connectionCodeList.append({"TargetSpecificCommsCodeInstance":"! I am not connected to anything"})
              print "No put/get connection found!!!"
            lastValue=".false."
            lastIteration=".false."
            firstConnectionCodeList=""
            lastConnectionCodeList=[]
            if compositionDocInfo.putgetConnection(dataInfo,"out",nesting=True):
              print "a put to a get at a different nesting level is required"
              remoteDataInfoList=compositionDocInfo.putgetConnections(dataInfo,"out",nesting=True)
              for remoteDataInfo in remoteDataInfoList:
                lastConnectionCodeList.append({"TargetSpecificLastCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"out")})
                #if compositionDocInfo.lessNested(dataInfo,remoteDataInfo):
                #  print "I am less nested so am a first connection"
                #  firstConnectionCodeList.append({"TargetSpecificLastCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"out")})
                #elif compositionDocInfo.moreNested(dataInfo,remoteDataInfo):
                #  print "I am more nested so am a last connection"
                #  exit(1)
                #  lastConnectionCodeList.append({"TargetSpecificLastCommsCodeInstance":targetSpecificComms(dataInfo,remoteDataInfo,templatedir,"out")})

              # I require at least one put to a get at a different nesting level to me
              lastValue=".true."
              myParentLoopID,loopType=compositionDocInfo.coupledInfo.getDeploymentInfo().getScheduleInfo().getParentIterationIDAndType(dataInfo)
              if loopType is None:
                # I am not nested within a fixed or convergence iteration loop so will only be called once
                lastIteration=".true."
              elif loopType=="fixed":
                lastIteration="lastFixedIteration("+str(myParentLoopID)+")"
              else:
                print "Error for data ..."
                print dataInfo
                if loopType=="converge":
                  assert False,"Error, a model in a convergence loop is connected to a model at a different nesting level. This is not currently supported. You will need to add an appropriate (last_value) transformation"
                else:
                  assert False,"Error, found a lastValue connection but an unexpected loop type was returned '"+loopType+"'"
            dataInThisEPMap={"DataID":dataInfo.fieldID,
                           "ModelName":ep.getModelInfo().name,
                           "EPName":ep.epName,
                           "InstanceID":"0",
                           "DataSize":dataInfo.size,
                           "LocalPrimedIndex":index,
                           "firstValue":".false.", # always false as this is a put
                           "lastValue":lastValue,
                           "firstIteration":".false.", # always false as this is a put
                           "lastIteration":lastIteration,
                           "TargetSpecificPrimingCode":primingCodeList,
                           "TargetSpecificCommsCode":connectionCodeList,
                           "TargetSpecificFirstCommsCode":firstConnectionCodeList,
                           "TargetSpecificLastCommsCode":lastConnectionCodeList}
            dataInThisEP.append(dataInThisEPMap)

        if deploymentUnitInfo.programConformance():
          # Just set to true as it is always this EP for program
          # compliance and also currentEP is not set when it is
          # program compliance. Perhaps I should have separate
          # template snippets for the two cases but this works.
          EPTest=".true."
        else:
          EPTest="currentEP=="+str(ep.getGlobalID())
        EPInThisDU.append({"EPTest":EPTest,
                           "DataInThisEP":dataInThisEP})

      InPlaceDeclarations=[]
      declarationList=[]
      # declare any namelist input data used for priming
      fileNameList=[]
      for namelistInfo in namelistInfoList:

        if namelistInfo.name not in fileNameList:
            fileNameList.append(namelistInfo.name)
            print "Found namelist file ",namelistInfo.name
            nlNameList=[]
            for namelistInfo2 in namelistInfoList:
                if namelistInfo2.name==namelistInfo.name:
                    if namelistInfo2.nlName not in nlNameList:
                        nlNameList.append(namelistInfo2.nlName)
                        print "Found nlname "+namelistInfo2.nlName
                        dataNames=[]
                        dataDecs=[]
                        for namelistInfo3 in namelistInfoList:
                            if namelistInfo3.name==namelistInfo2.name and namelistInfo3.nlName==namelistInfo2.nlName and namelistInfo3.dataRef not in dataNames:
                                dataNames.append(namelistInfo3.dataRef)
                                fieldInfo=namelistInfo3.field
                                dataType=fieldInfo.getDataType("f90")
                                if dataType=="character":
                                  stringSize=fieldInfo.getStringSize()
                                  dataType+="(len="+str(stringSize)+")"
                                dataDecs.append(dataType+" :: "+namelistInfo3.dataRef+fieldInfo.arrayDimsSize()+"\n")
                                print "Found data "+namelistInfo3.dataRef
                        declarationList.append({"variables":dataDecs,"nlName":namelistInfo2.nlName,"varNameList":listString(dataNames)})

      namelistTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_namelist.xml"))
      declarationsTemplate=map({"snippet":declarationList},namelistTemplate,snippetName="declarations",copy=True)
      InPlaceDeclarations.append(declarationsTemplate)


      #if connectionDirection=="in": # routine may contains primed data
      #  for epInfo in EPMap.keys():
      #    print "LOOKING AT EP "+epInfo.epName
      #    if epInfo.usesNamelist(): # ep may contain primed data
      #      print "THIS EP DOES USE NAMELISTS"
      #      namelistNames=epInfo.getNamelistNames()
      #      for namelistName in namelistNames :
      #        namelistList=epInfo.getNamelistList(namelistName)
      #        namelistContent={"varNameList":namelistList,
      #                         "nlName"     :namelistName}
      #        namelistTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_namelist.xml"))
      #        namelistDeclarations=map(namelistContent,namelistTemplate,snippetName="declaration")
      #        InPlaceDeclarations.append(namelistDeclarations)
      #    else:
      #      print "THIS EP DOES NOT USE NAMELISTS"

      if epInfo.usesNetcdf():
        netcdfTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_netcdf.xml"))
        netcdfDeclarations=map({},netcdfTemplate,snippetName="declarations")
        InPlaceDeclarations.append(netcdfDeclarations)

      InPlaceDeclarations.append(targetSpecificDeclaration(coupledDocInfo))

      # shortcut : use the last dataInfo object to provide function info as any one will do
      if connectionDirection=="in" : InPlaceIntent="out"
      elif connectionDirection=="out" : InPlaceIntent="in"
      else : assert False, "Error"
      functionInfoList.append({"InPlaceProcedureName":dataInfo.getFunctionName(connectionDirection),
                           "InPlaceDatatype":dataInfo.getDataType("f90"),
                           "InPlaceDimensionDeclaration":dataInfo.f90ArrayDims(),
                           "InPlaceIntent":InPlaceIntent,
                           "InPlaceTagDatatype":dataInfo.getInPlaceTagDatatype(language="f90"),
                           "InPlaceDeclarations":InPlaceDeclarations,
                           "LocalNPrimed":index,
                           "LocalPrimedInit":LocalPrimedInitString[:len(LocalPrimedInitString)-1],
                           "EPInThisDU":EPInThisDU})

    deploymentUnitInfo.setFunctionInfo(functionObjectList)

    deploymentUnitInfo.setFunctionNamesMap(functionNamesMap2)

    # a list of unique input function names
    functionNamesIn=[]
    for functionName in functionNamesInList:
      functionNamesIn.append({"InPlaceProcedureName":functionName})

    # a list of unique output function names
    functionNamesOut=[]
    for functionName in functionNamesOutList:
      functionNamesOut.append({"InPlaceProcedureName":functionName})

    mapping={"DUID":deploymentUnitInfo.getID(),
             "InPlaceGetProcedureDefinitions":functionNamesIn,
             "InPlacePutProcedureDefinitions":functionNamesOut,
             "InPlaceProcedures":functionInfoList}

    #print mapping

    map(mapping,inplaceTemplate)

    # BEGIN TARGET SPECIFIC
    deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
    target=deploymentDocInfo.target
    if target=="mpi":
      mapping={"ExtraTarget":", b2mmap",
               "TargetSpecificIncludes":"use mpi"}
    elif target=="oasis3":
      mapping={"TargetSpecificIncludes":"use mod_prism_proto"}
    elif target=="oasis4":
      mapping={"TargetSpecificIncludes":"use mod_prism_proto"}
    elif target=="":
      mapping={"TargetSpecificIncludes":""}
    else:
      print "Error: inplacegen: target '"+target+"' not supported"
      exit(1)
    # END TARGET SPECIFIC

    map(mapping,inplaceTemplate)

    if deploymentUnitInfo.usesNetcdf():
      netcdfTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_netcdf.xml"))
      result={"TargetSpecificIncludes":map({},netcdfTemplate,snippetName="includes")}
      map(result,inplaceTemplate)

    #print(ET.tostring(inplaceTemplate, method="xml",pretty_print=True))
    #print(ET.tostring(inplaceTemplate, method="text",pretty_print=True))
    #print(ET.tostring(inplaceTemplate,method="xml",pretty_print=True))
    inplaceTemplate.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+".f90",method="text")


def targetSpecificControlComms(dataInfo,remoteDataInfo,direction):
  coupledDocInfo=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  target=deploymentDocInfo.target
  if target=="mpi":
    return targetSpecificControlComms_mpi(dataInfo,remoteDataInfo,direction)
  else:
    assert False,"Specified target '"+target+"' not yet supported for control loops."
  

def targetSpecificComms(dataInfo,remoteDataInfo,templatedir,direction,line="",argName="arg1"):
  coupledDocInfo=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
  compositionDocInfo=coupledDocInfo.getCompositionInfo()
  if remoteDataInfo.__name__=="fileInfo":
    if compositionDocInfo.primingType(dataInfo)=="file":
      if line!="":
        if remoteDataInfo.type=="namelist":

          namelistContent={"fileUnit":remoteDataInfo.unit,
                           "fileName":remoteDataInfo.name,
                           "nlName"  :remoteDataInfo.nlName,
                           "variableDeclaration" :"arg1="+remoteDataInfo.dataRef}

          namelistTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_namelist.xml"))
          return map({"snippet":[namelistContent]},namelistTemplate,snippetName="contents")

        elif remoteDataInfo.type=="netcdf":

          # create our content
          netcdfContent={"fileName":remoteDataInfo.name,
                         "dataID"  :remoteDataInfo.dataRef,
                         "varName" :argName}
  
          netcdfTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_netcdf.xml"))
          return map(netcdfContent,netcdfTemplate,snippetName="content")
        else:
          assert False,"Error"
    else:
      assert False, "Error"
  else:
    deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
    target=deploymentDocInfo.target
    if target=="mpi":
      return targetSpecificComms_mpi(dataInfo,remoteDataInfo,direction)
    elif target=="oasis4":
      return targetSpecificComms_oasis4(dataInfo,remoteDataInfo,direction)
    elif target=="oasis3":
      return targetSpecificComms_oasis3(dataInfo,remoteDataInfo,direction)
    elif target=="":
      return ""
    else:
      assert False,"Specified target '"+target+"' not supported"

def targetSpecificDeclaration(coupledDocInfo):
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  target=deploymentDocInfo.target
  if target=="mpi":
    return targetSpecificDeclaration_mpi()
  elif target=="oasis4":
    return targetSpecificDeclaration_oasis4()
  elif target=="oasis3":
    return targetSpecificDeclaration_oasis3()
  elif target=="file":
    return "FILE INFO"
  elif target=="":
    return ""
  else:
    assert False,"Specified target '"+target+"' not supported"

def targetSpecificDeclaration_mpi():
  return "integer :: istatus(mpi_status_size),ierr"

def targetSpecificControlComms_mpi(dataInfo,remoteDataInfo,direction):
  localModelInfo=dataInfo.getEPInfo().getModelInfo()
  remoteModelInfo=remoteDataInfo.getEPInfo().getModelInfo()
  coupledDocInfo=localModelInfo.getCoupledInfo()
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  assert direction in ["in","out"], "Error in targetSpecificControlComms, direction has unexpected value '"+direction+"'"
  # more checks TBD 1: logical data value, 2: appropriate model is a control model
  MPITYPE=dataInfo.getMPIDataType("f90")
  if direction=="in":
    # never use the convergence model as ID, use the one connecting to it instead
    remoteModelName=remoteModelInfo.name
    B2MMapValue=deploymentDocInfo.getSequenceID(remoteModelName)
  else:
    myModelName=localModelInfo.name
    B2MMapValue=deploymentDocInfo.getSequenceID(myModelName)
  return "call mpi_bcast(arg1,mysize,"+MPITYPE+",b2mmap("+str(B2MMapValue)+"),mpi_comm_world,ierr)"

def targetSpecificComms_mpi(dataInfo,remoteDataInfo,direction):
  coupledDocInfo=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  #compositionDocInfo=coupledDocInfo.getCompositionInfo()
  #if compositionDocInfo.putgetConnection(dataInfo,"in"):
  if direction=="in":
    MPICall="mpi_recv"
    MPISTATUS="istatus,"
  elif direction=="out":
    MPICall="mpi_send"
    MPISTATUS=""
  else:
    assert False, "ERROR"
  MPITYPE=dataInfo.getMPIDataType("f90")
  #remoteDataInfo=compositionDocInfo.getPrimingData(dataInfo)
  #assert len(remoteDataInfo)==1,"Error"
  #remoteModelName=remoteDataInfo[0].getEPInfo().getModelInfo().name
  remoteModelName=remoteDataInfo.getEPInfo().getModelInfo().name
  B2MMapValue=deploymentDocInfo.getSequenceID(remoteModelName)
  # choose to use the id of the receiver as the tag for both sender and receiver
  #if compositionDocInfo.putgetConnection(dataInfo,"in"):
  if direction=="in":
    TAGValue=dataInfo.getGlobalID()
  elif direction=="out":
    TAGValue=remoteDataInfo.getGlobalID()
  else:
    assert False,"ERROR"
  code="call "+MPICall+"(arg1,mysize,"+MPITYPE+",b2mmap("+str(B2MMapValue)+"),"+str(TAGValue)+",mpi_comm_world,"+MPISTATUS+"ierr)\n"
  return code

def targetSpecificComms_oasis4(dataInfo,remoteDataInfo,direction):
  if direction=="in":
    Oasis4Call="prism_get"
    direction="Recvd"
    direction2="get"
  elif direction=="out":
    Oasis4Call="prism_put"
    direction="Sent"
    direction2="put"
  else:
    assert False, "ERROR"
  code = "call "+Oasis4Call+" ( var_id, model_time, model_time_bounds, \""+dataInfo.getName()+"\", &\n                 info, ierror )\n"
  return code
def targetSpecificDeclaration_oasis4():
  return ""

def targetSpecificComms_oasis3(dataInfo,remoteDataInfo,direction):
  coupledDocInfo=dataInfo.getEPInfo().getModelInfo().getCoupledInfo()
  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  compositionDocInfo=coupledDocInfo.getCompositionInfo()
  if direction=="in":
    Oasis3Call="prism_get_proto"
    direction="Recvd"
    direction2="get"
  elif direction=="out":
    Oasis3Call="prism_put_proto"
    direction="Sent"
    direction2="put"
  else:
    assert False, "ERROR"

  code="call "+Oasis3Call+"(\""+dataInfo.getName()+"\", oasis_time, arg1, ierror)\n \
    if (ierror.ne.PRISM_Ok .and. ierror .LT. PRISM_"+direction+") then\n \
      call prism_abort_proto(arg1, '"+dataInfo.getEPInfo().getModelInfo().name+"',' ep "+dataInfo.getEPInfo().epName+" "+direction2+" tag "+dataInfo.fieldID+"')\n \
    end if"
  return code

def targetSpecificDeclaration_oasis3():
  return "integer :: ierror\n \
          integer :: oasis_time=0"
