#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue, functionInfo, listString

def genDU(deploymentUnitInfo,BFG2ROOT):

    BFGtoESMFRunType={"init":"Initialize","iteration":"Run","final":"Finalize"}

    # limit code generation to what we support
    assert deploymentUnitInfo.language=="f90","Only f90 supported so far, but found "+str(deploymentUnitInfo.language)

    deploymentInfo=deploymentUnitInfo.getDeploymentInfo()
    coupledInfo=deploymentInfo.getCoupledInfo()
    compositionInfo=coupledInfo.getCompositionInfo()
    scheduleInfo=deploymentInfo.getScheduleInfo()

    templatedir = os.path.join(BFG2ROOT,"templates")

    # Read in our f90 XML template
    # should be based on required language but this is only f90 at the moment
    targetTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_esmf_target.xml"))

    # set up entrypoint calls information
    # keep a list of component handles so we can declare them
    # keep a list of the entrypoint names to use in the parallelism functions
    # create initModels list
    # include the esmf wrapped models
    # create finalise calls information
    epcalls=[]
    compHandleList=[]
    concThreads=[]
    initModels=[]
    modelIncludes=[]
    finaliseCalls=[]
    for modelName in deploymentUnitInfo.getModelNames():
        modelInfo=coupledInfo.getDefinitionInfo(modelName)
        modelIncludes.append({"origModelName":modelInfo.name,
                              "esmfModelName":modelInfo.name+"_esmf"})
        compHandle=componentHandle(modelInfo)
        compHandleList.append(compHandle)
        finaliseCalls.append({"compHandle":compHandle})
        mySequenceUnit=deploymentUnitInfo.getSequenceUnit(modelInfo)
        assert mySequenceUnit.getNThreads()==1, "Error, not coded for more than one thread in an sequence unit"
        initModels.append({"compHandle":compHandle,
                           "compThreads":mySequenceUnit.getThreadIndex(),
                           "origModelName":modelInfo.name})
        epNamesList=scheduleInfo.getEPNames(modelName)
        renameList=[]
        # rename each epName
        for epName in epNamesList:
            epInfo=modelInfo.getEP(epName)
            uniqueEPName=epInfo.getUniqueName()
            epcalls.append({"epName":uniqueEPName,
                            "phase":BFGtoESMFRunType[epInfo.runType],
                            "compHandle":compHandle,})
            concThreads.append({"epName":uniqueEPName})

    # Create template mapping file
    mapping={"DUID":deploymentUnitInfo.getID(),
             "modelIncludes":modelIncludes,
             "concThreads":concThreads,
             "initModels":initModels,
             "TotalThreads":deploymentInfo.getNumThreads(),
             "epcalls":epcalls,
             "finalise":finaliseCalls,
             "compHandles":listString(compHandleList)}

    # apply template
    map(mapping,targetTemplate)

    # output result
    targetTemplate.write("BFG2Target"+str(deploymentUnitInfo.getID())+".f90",method="text")

def componentHandle(modelInfo):
    return "comp"+str(modelInfo.getID())
