import os
import bfgutils
from bfgutils import coupledInfo, map, getValue
from lxml import etree as ET

def run(deploymentUnitInfo,BFG2ROOT):

  print

  templatedir = os.path.join(BFG2ROOT,"templates")

  # Read in our XML template
  programConformanceTemplate=bfgutils.xmlread(os.path.join(templatedir,"c_to_fortran90.xml"))
  cmapping={}
  result=map(cmapping,programConformanceTemplate,snippetName="c_program_interface")
  ETresult=ET.ElementTree(result)
  ETresult.write("BFG2ProgramConformance"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")
