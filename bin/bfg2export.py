#! /usr/bin/env python

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as ET
import bfgutils
from bfgutils import definitionInfo, map, coupledInfo
import check
from check import validXML,validate,rootElement
import inplacegen
#import inplaceinterfacegen
import controlgen
import bfginterfacegen
import esmfwrap
import targetgen

# our supported targets for export ...
#targets=["esmf","oasis3","oasis4","tdt","webservice","cpl5"]
targets=["esmf","oasis3","oasis4","mpi"]

usage = "usage: %prog [options] BFG2[Definition|Coupled]Document.xml"
version = "1.0"
parser = OptionParser(usage)
parser.add_option("-t", "--target", dest="target",
                  help="target framework, supported targets are "+str(targets), metavar="target")
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-n", "--novalidate",
                  action="store_false", dest="validate", default=True,
                  help="do not validate xml documents before code generation")
parser.add_option("-k", "--keep",
                  action="store_true", dest="keep", default=False,
                  help="don't delete any temporary xml documents")
parser.add_option("-o", "--directory", dest="OutDir",
                  help="output generated files to DIR", metavar="DIR")
parser.add_option("-d", "--deploymentunit", type="int", dest="DUIndex",
                  help="Specify a particular deployment unit to export", metavar="INDEX")
parser.add_option("-s", "--sequenceunit", dest="SUIndex",
                  help="Specify a particular sequence unit to export", metavar="INDEX")
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
wd=os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
xsltdir = os.path.join(BFG2ROOT,"xslt")
templatedir=os.path.join(BFG2ROOT,"templates")

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if not options.target:
  print "Error, a target must be specified"
  sys.exit(1)

if options.target not in targets :
  print "error, unsupported target option '"+options.target+"', supported targets are "+str(targets)
  sys.exit(1)

if options.OutDir and not(os.path.isdir(options.OutDir)):
  print "error, non-existant target directory '%s'" % (options.OutDir)
  sys.exit(1)


DocumentPathNoQuote = os.path.abspath(args[0])
DocumentPath = "'" + DocumentPathNoQuote + "'"

if options.OutDir:
  OutDirNoQuote = os.path.abspath(options.OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
  os.chdir(OutDirNoQuote)
else:
  OutDirNoQuote = os.path.abspath(".")
  OutDir = "'" + OutDirNoQuote + "'"

CommFormsPathNoQuote = OutDirNoQuote + "/" + "DS.xml"
CommFormsPath = "'" + CommFormsPathNoQuote + "'"

doc=validXML(DocumentPathNoQuote)

if rootElement(doc,"coupled"):
  if options.verbose: print "This appears to be a coupled document"
  if not validate(doc,os.path.normpath(os.path.join(BFG2ROOT,'schema/coupled.xsd'))):
    print "coupled document is invalid, aborting"
    exit(1)
  if options.validate:
    if options.verbose: print "Checking the xml is valid and consistent"
    check.run(DocumentPathNoQuote)
    if options.verbose: print "Done"
  elif options.verbose: print "Skipping validation as '-n' is specified"

  if not options.DUIndex:
    print "Error, you must specify a Deployment Unit index to export"
    exit(1)

  # parse our xml
  DocumentDir = os.path.dirname(DocumentPathNoQuote)
  coupledDocInfo=coupledInfo(DocumentPathNoQuote,DocumentDir,debug=False)
  # if deployment target is not the requested target then bail out for the moment
  if options.target!=coupledDocInfo.getDeploymentInfo().target:
    # we can support different targets when we get the validation checks working using coupledDocInfo as we can change the target info and it can check the modified document
    print "Error, we currently only support a document specifying the same target as specified on the command line. Specified target is '"+options.target+"' but deployment document specifies '"+coupledDocInfo.getDeploymentInfo().target+"'"
    exit(1)

  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
  # check that the DUIndex is a valid value
  if not (options.DUIndex>=1 and options.DUIndex<=len(deploymentUnits)):
    print "Error: DUIndex must be in the range 1 to N-DeploymentUnits. Found DUIndex='"+str(options.DUIndex)+"' NDeploymentUnits='"+str(len(deploymentUnits))+"'"
    exit(1)
  deploymentUnit=deploymentUnits[options.DUIndex-1]

  if options.SUIndex:
    if options.target in ["mpi","oasis4","oasis3"]:
      print "Error, "+options.target+" does not allow a sequence unit to be specified in the argument list"
      exit(1)
    if not options.target in ["esmf"]:
      print "Error, if a sequence unit is selected then the target must be esmf"
      exit(1)
    sequenceUnits=deploymentUnit.getSequenceUnitsInfo()
    # check that the DUIndex is a valid value
    if not (int(options.SUIndex)>=1 and int(options.SUIndex)<=len(sequenceUnits)):
      print "Error: SUIndex must be in the range 1 to N DeploymentUnits. Found SUIndex='"+str(options.SUIndex)+"' NSequenceUnits='"+str(len(sequenceUnits))+"'"
      exit(1)
    sequenceUnit=sequenceUnits[int(options.SUIndex)-1]
    esmfwrap.genSU(sequenceUnit,BFG2ROOT)
  else:

    if options.target in ["esmf"]:
      print "Error, esmf requires a sequence unit to be defined in the argument list"
      exit(1)
    

    if options.verbose: print "Generating inplace code"
    inplacegen.genDU(deploymentUnit,BFG2ROOT)

#    if options.verbose: print "Generating inplace interface code"
#    inplaceinterfacegen.genDU(deploymentUnit,BFG2ROOT)

    if options.verbose: print "Generating f90 interface code"
    bfginterfacegen.genDU(deploymentUnit,BFG2ROOT)

    if options.verbose: print "Generating Control Code"
    controlgen.genDU(deploymentUnit,BFG2ROOT)

    if options.target=="oasis3":
      if options.verbose: print "Generating Control Code"
      targetgen.genDU(deploymentUnit,BFG2ROOT)
      # skip the rest of the code as it uses the old xslt approach
      exit(0)


    # for the moment we modify the existing xsl generation. We will eventually change this to template code.
    if options.verbose:
      print "Generating Data structure"
      sys.stdout.flush()
    epTemplate=bfgutils.xmlread(os.path.join(xsltdir,"Utils/EPTemplate.xml"))
    et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Data-struct/ConstructDS.xsl"),
                                  xmldoc=epTemplate,
                                  args={"CoupledDocument":DocumentPath})
    et_result.write(CommFormsPathNoQuote,pretty_print=True)
    #
    codeGenXSL=bfgutils.xmlread(os.path.join(xsltdir,"CodeGen/CodeGen.xsl"))
    visualisempi = "'false'"
    visualiseoasis = "'false'"
    DUIndex = "'"+str(options.DUIndex)+"'"
    TargetCodePhases=[['1',None,epTemplate,{"CoupledDocument":DocumentPath,"mpivis":visualisempi,"oasisvis":visualiseoasis,"DUIndex":DUIndex},True],\
                    ['2',None,None,{"CoupledDocument":DocumentPath,"CommForms":CommFormsPath,"OutDir":OutDir,"mpivis":visualisempi,"oasisvis":visualiseoasis},True],\
                    ['3',None,None,{"CoupledDocument":DocumentPath},True],\
                    ['4',None,None,{"CoupledDocument":DocumentPath},True],\
                    ['gen',codeGenXSL,None,{"FileName":"'BFG2Target.f90'","OutDir":OutDir,"CoupledDocument":DocumentPath,"CommForms":CommFormsPath},False]]
    if options.verbose:
      print "Generating target specific code",
      sys.stdout.flush()
    for args in TargetCodePhases :
      et_result=bfgutils.runPhase(xsltdir,OutDirNoQuote,et_result,options,"Target",*args)
    if options.verbose:
      print ""

    # GENERATE BFG CONTROL FILE
    coupledDoc=bfgutils.xmlread(DocumentPathNoQuote)
    if options.verbose:
      print "Generating Control File",
      sys.stdout.flush()
    et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Control/f90ControlFile.xsl"),
                                xmldoc=coupledDoc,
                                args={"CoupledDocument" : DocumentPath , "OutDir" : OutDir })
    if options.verbose:
       print ""
       sys.stdout.flush()

    # GENERATE ANY ASSOCIATED TARGET CONFIG FILES
    if options.verbose:
      print "Generating config files",
      sys.stdout.flush()
    et_result=bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Config/1/ConfigPhase1.xsl"),
                                xmldoc=coupledDoc,
                                args={ "CoupledDocument" : DocumentPath , "OutDir" : OutDir , "CommForms" : CommFormsPath , "DUIndex" : DUIndex})
    if options.verbose:
      print ""
      sys.stdout.flush()

    if not options.keep:
      os.remove(CommFormsPathNoQuote)

    # end of generation from a coupled document


elif rootElement(doc,"definition"):
  if options.verbose: print "This appears to be a BFG2 definition document."
  if not validate(doc,os.path.normpath(os.path.join(BFG2ROOT,'schema/definition.xsd'))):
    print "definition document is invalid, aborting"
    exit(1)
  if options.validate:
    if options.verbose:
      print "Checking the xml is valid and consistent"
      print "No definition specific checks supported yet"
      print "Done"
  elif options.verbose: print "Skipping validation as '-n' is specified"

  if options.DUIndex and options.verbose:
    print "Warning, ignoring specified deploymentunit index."
  if options.SUIndex and options.verbose:
    print "Warning, ignoring specified sequenceunit index."

  print "Export from a definition document not yet implemented, sorry"
  exit(0)

else:
  print "Error: document is not a valid BFG2 coupled or definition document"
  exit(1)


print "BFG2 has left the building."
exit(0)

# Possibly use the code below for exporting a definition document?

if options.verbose:
  print "BFG2 ... autogentastic."
  sys.stdout.flush()

