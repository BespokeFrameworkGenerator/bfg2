#! /usr/bin/env python

# BFG2 file compile list
# RF Created May 2013

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as et
import libxml2
import libxslt
import check
import ConfigParser
import string

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-o", "--outputfile", dest="FileName",
                  help="name of the output file", default="compilelist.txt")
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
xsltdir = os.path.join(BFG2ROOT,"xslt")

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

DefinitionDocumentPathNoQuote = os.path.abspath(args[0])
DefinitionDocumentPath = "'" + DefinitionDocumentPathNoQuote + "'"

if options.verbose:
  print "BFG2 ... autogentastic."
  sys.stdout.flush()

def libxsltErrorCallback(ctx, str):
  global error_msg
  print "%s %s" % (ctx, str)
  sys.exit(1)

libxml2.registerErrorHandler(libxsltErrorCallback, "")

if options.verbose:
  # BFG2 Metadata is not designed to cover the build phase.
  # The generated Makefile is therefore a guideline and may
  # require manual additions/changes.
  print "Generating file list ... "
  print "[gen]"
  sys.stdout.flush()
try:
  styledoc = libxml2.parseFile(os.path.join(xsltdir,"Makefile","Makefile_list.xsl"))
  style = libxslt.parseStylesheetDoc(styledoc)
  doc = libxml2.parseFile(DefinitionDocumentPathNoQuote)

  result = style.applyStylesheet(doc,{})
  style.saveResultToFilename(os.path.basename(options.FileName), result, 0)
  style.freeStylesheet()

except et.XSLTApplyError as e :
  print "Check "+CurrentTest+" found errors in the xml, aborting"
  print e
  sys.exit(1)
except Exception, e:
  print "Parse error in Makefile_template.xsl, aborting"
  print e
  sys.exit(1)

if options.verbose:
  print "BFG2 has left the building."
  sys.stdout.flush()
