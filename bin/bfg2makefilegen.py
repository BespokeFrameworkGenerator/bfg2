#! /usr/bin/env python

# BFG2 makefile generation python script
# RF Created July 2011

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as et
import check
import ConfigParser
import string
import bfgutils

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0"
parser = OptionParser(usage)
parser.add_option("-q", "--quiet",
                  action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")
parser.add_option("-d", "--deploymentunit", type="int", dest="DUIndex",
                  help="Specify a particular deployment unit to export", metavar="INDEX")
parser.add_option("-b", "--backslash",
                  action="store_true", dest="BackSlash", default=False,
                  help="file separator is \ (default is /)")
parser.add_option("-o", "--outputfile", dest="FileName",
                  help="name of the generated makefile", default="Makefile.gen")
#parser.add_option("-c", "--configfile", dest="ConfigFileName",
#                  help="location of the user config file")
parser.add_option("-s", "--sameDir",
                  action="store_true", dest="SameDir", default=False,
                  help="assume code is in the same directory as definition.xml")
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
# print "Working dir", os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
# print "BFG2ROOT",BFG2ROOT
xsltdir = os.path.join(BFG2ROOT,"xslt")
# print "xsltdir",xsltdir

OutDir=os.path.dirname(options.FileName)

ConfigFileName="bfg2makefilegen.cfg"
ConfigEnvVar="BFG2CONFIGPATH"

config = ConfigParser.ConfigParser()

# read in compiler specific info from our default config file.
# It comes with the distribution so if it does not exist then
# raise an error.
try:
  config.read(os.path.join(pathname,ConfigFileName))
  items=config.items('CompilerOptions')
  print "Read in the following default compiler options:"
  print items
except :
  print "Fatal error, default compiler option config file not found."
  exit(1)

# TBD override defaults from a command line reference to config file if specified
# if options.ConfigFileName:
# if file exists
# for each entry, override default values
# else:

# look for file (bfg2makefilegen.cfg) pointed to by environment variable
paths=os.getenv(ConfigEnvVar)
if paths is not None:
#  print ConfigEnvVar+" environment variable is set"
  print("{0} environment variable is set to {1}".format(ConfigEnvVar, paths))
  found=False
  for path in paths.split(os.pathsep):
    filepath=os.path.join(path, ConfigFileName)
#    filepath = path
    if os.path.exists(filepath):
      found=True
      try:
        config.read(os.path.join(filepath))
        newitems=config.items('CompilerOptions')
        print "Overriding default compiler options with the following user supplied values:"
        print newitems
        # now merge these with the default values
        for newitem in newitems:
          try:
            loc = items.index(newitem)
            #print "Skip as this value is the same as the default"
          except ValueError:
            index=0
            for item in items:
              if item[0]==newitem[0]:
                print "Replacing "+item[1]+" with "+newitem[1]+" for variable "+item[0]
                items[index]=newitem
                break
              index+=1
        print "New values are:"
        print items
      except :
        print "Error in user supplied compiler option config file."
      break
  if not found:
    print "No user supplied "+ConfigFileName+" file found in environment variable path"
else:
  print ConfigEnvVar+" environment variable not set"

# TBD else:
# if not env var or not found in env var then look in current dir and home dir
#for bfg2makefilegen.cfg
#  os.curdir - is there an os.homedir?

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

if OutDir and not(os.path.isdir(OutDir)):
  print "error, non-existant target directory '%s'" % (OutDir)
  sys.exit(1)

DefinitionDocumentPathNoQuote = os.path.abspath(args[0])
DefinitionDocumentPath = "'" + DefinitionDocumentPathNoQuote + "'"

if OutDir:
  OutDirNoQuote = os.path.abspath(OutDir)
  OutDir = "'" + OutDirNoQuote + "'"
  os.chdir(OutDirNoQuote)
else:
  OutDirNoQuote = os.path.abspath(".")
  OutDir = "'" + OutDirNoQuote + "'"
  
print "Output dir is",OutDirNoQuote

if options.verbose:
  print "BFG2 ... autogentastic."
  sys.stdout.flush()

if options.verbose:
  # BFG2 Metadata is not designed to cover the build phase.
  # The generated Makefile is therefore a guideline and may
  # require manual additions/changes.
  print "Generating *outline* Makefile ... "
  print "[gen]"
  sys.stdout.flush()
try:
  args = {}
#  args={ "BFG2ROOT" : "'{0}'".format(BFG2ROOT) }
  if options.DUIndex : args["DUIndex"]="'"+str(options.DUIndex)+"'"
  if not options.SameDir: args["PathToCoupledDoc"]="'"+os.path.dirname(DefinitionDocumentPathNoQuote)+"'"
  if options.BackSlash : args["FileSep"]="'\\'"

  for item in items:
    args[string.upper(item[0])]="'"+item[1]+"'"

  args["BFG2ROOT"] = "'{0}'".format(BFG2ROOT)
#  print("args['BFG2ROOT'] = {0}".format(args["BFG2ROOT"]))

  definition_doc = bfgutils.xmlread(DefinitionDocumentPathNoQuote)
  et_result = bfgutils.xmlprocess(xslfile=os.path.join(xsltdir,"Makefile","Makefile_template.xsl"),
                                  xmldoc=definition_doc,
                                  args=args)
  with open(os.path.basename(options.FileName), "w") as text_file:
    text_file.write(str(et_result))

except et.XSLTApplyError as e :
  print "Check "+CurrentTest+" found errors in the xml, aborting"
  print e
  sys.exit(1)
except Exception, e:
  print "Parse error in Makefile_template.xsl, aborting"
  print e
  sys.exit(1)

if options.verbose:
  print "BFG2 has left the building."
  sys.stdout.flush()
