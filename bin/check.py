import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as et
from bfgutils import coupledInfo,modelException
import string

from bfgvalidationerrors import *


# TBD can't return basic datatypes by argument in python as pass by value
# TBD check datasizes between inputs and outputs and give a warning/error

class checkException(Exception):
  def __init__( self, message ):
    Exception.__init__(self, '{0}'.format(message))

def run(fileName):

  # work out BFG2ROOT from script location
  pathname=os.path.dirname(sys.argv[0])
  BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))

  # work out our xslt and constraints directories
  xsltdir = os.path.join(BFG2ROOT,"xslt")
  constraintsdir = os.path.join(BFG2ROOT,"constraints")

  # work out our coupled document directory
  CoupledDocumentPathNoQuote = os.path.abspath(fileName)
  CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"
  CoupledDocumentDir = os.path.dirname(CoupledDocumentPathNoQuote)

  # open our input file and check it actually exists
  try:
    f = open(fileName, 'r')
  except IOError, (errno, strerror):
    print "I/O error(%s) for file %s: %s" % (errno, fileName, strerror)
    exit(1)

  # check our file is an xml document and return as an lxml document
  doc=validXML(CoupledDocumentPathNoQuote)

  # check our document is a BFG coupled document and determine which version
  version=bfgVersion(doc)

  if doc.getroot().tag == "definition" :
    print "checking definition document"
    if not validate(doc,os.path.normpath(os.path.join(BFG2ROOT,'schema/definition.xsd'))):
      print "Error in definition document"
      raise BFGSyntaxSchemaError()
    else:
      print "Definition document is valid"
      exit(0)

  # check our document is a BFG coupled document and determine which version
  # bfg2 checks
  if version==2 :
    # check document and referenced documents validate against the schemas.
    print "ABOUT TO VALIDATE"
    validAgainstBFG2Schemas(doc,CoupledDocumentDir)
    print "VALIDATE COMPLETE"

    # we mostly use python rather than xsl to perform checks
    # this is to (hopefully) make BFG2 both more maintainable and more efficient
    # the checkConstraint function is used for any remaining xslt checks
    # eventually modify the above examples to use python instead

    print "DEBUG: setting up coupledInfo"
    coupledDocInfo = coupledInfo(doc,CoupledDocumentDir)
    compositionInfo = coupledDocInfo.getCompositionInfo()
    deploymentInfo = coupledDocInfo.getDeploymentInfo()
    print "DEBUG: setting up coupledInfo complete"

    print "ABOUT TO CHECK MODELS"
    # model specific checks (a number of other tests should be migrated here)
    for modelInfo in coupledDocInfo.getDefinitionsInfo():
      try:
        modelInfo.check(verbose=True)
      except modelException, e:
        raise checkException, e
    print "CHECK COMPLETE"

    # basic definition document checks

    print "***Checking that definition ep and model names are distinct"
    for definitionDocInfo in coupledDocInfo.getDefinitionsInfo():
        if not checkDefinitionNames(definitionDocInfo):
            raise BFGNameUniquenessError()

    print("[definitionDocInfo] {0}".format(definitionDocInfo.myID))
    print "Checks are OK"

    # basic compose document checks

    print "***Checking that composition {model,ep,form,direction,id} tuples exist and are unique"
    abort=False
    print "Set Notation not yet supported"
    print "P2P Coupling"
    xmlp2ps=compositionInfo.XMLroot.xpath("/composition/connections//connection")
    for XMLroot in xmlp2ps:
      inModel=XMLroot.xpath("ancestor::modelChannel/@inModel")
      outModel=XMLroot.xpath("ancestor::modelChannel/@outModel")
      inEP=XMLroot.xpath("ancestor::epChannel/@inEP")
      outEP=XMLroot.xpath("ancestor::epChannel/@outEP")
      inForm=XMLroot.get("inForm")
      outForm=XMLroot.get("outForm")
      inID=XMLroot.get("inID")
      outID=XMLroot.get("outID")
      assert len(inModel)==1 and len(outModel)==1 and len(inEP)==1 and len(outEP)==1, "Error"
      success=checkModelExistsUnique(coupledDocInfo,inModel[0],inEP[0],inForm,["in","inout"],inID)
      if not success:
          raise BFGNameUniquenessError(coupledDocInfo)

      success=checkModelExistsUnique(coupledDocInfo,outModel[0],outEP[0],outForm,["out","inout"],outID)
      if not success:
          raise BFGNameUniquenessError(coupledDocInfo)

    print "Priming"
    xmlprimes=compositionInfo.XMLroot.xpath("/composition//primed")
    for XMLroot in xmlprimes:
      primedModel=XMLroot.xpath("ancestor::model/@name")
      primedEP=XMLroot.xpath("ancestor::entryPoint/@name")
      primedForm=None
      primedID=XMLroot.get("id")
      assert len(primedModel)==1 and len(primedEP)==1, "Error"

      if not checkModelExistsUnique(coupledDocInfo,primedModel[0],primedEP[0],primedForm,["in","inout"],primedID):
          raise BFGConsistencyEPError(primedEP[0], primedModel[0])

#    print "[primedModel[0]] {0}".format(primedModel[0])
#    print "[primedEP[0]] {0}".format(primedEP[0])
    print "Checks are OK"

    # basic deploy document checks

    print "***Checking that deploy deployment model names exist"

    if not checkDeployNames1(coupledDocInfo):
        raise BFGNameReferentialError("name", coupledDocInfo)
    
    print "***Checking that deploy deployment model names are distinct"
    deploymentDocInfo=coupledDocInfo.getDeploymentInfo()

    if not checkDeployNames2(deploymentDocInfo):
        raise BFGNameUniquenessError(deploymentDocInfo)

    print "Checks are OK"

    print "***Checking that deploy schedule model+ep names exist"

    if not checkDeployNames3(coupledDocInfo):
        raise BFGNameUniquenessError(coupledDocInfo)
    
    print "Checks are OK"

    print "***Checking that deploy DU model names are in the schedule"

    if not checkDeployNames4(deploymentDocInfo):
        raise BFGNameReferentialError(deploymentDocInfo)

    print "Checks are OK"

    print "***Checking that deploy schedule model+ep names are distinct"

    if not checkDeployNames5(deploymentDocInfo):
        raise BFGNameUniquenessError(deploymentDocInfo)

    print "Checks are OK"


    # tests for convergence loops
    # 1: no model names can be called bfg:
    print "*** Checking that definition model names are not called bfg: as this name indicates the bfg control code"

    for modelInfo in coupledDocInfo.getDefinitionsInfo():
        if modelInfo.name.lower()=="bfg:" and modelInfo.type!="control":
            raise BFGNameReservedError(modelInfo.name)

    print "Checks are OK"


    # TBD Convergence Checks
    # TBD if converge and firsttag specified then it must be connected
    # TBD if converge then looptag must be connected (to an out/inout)
    # TBD firsttag and looptag must be unique in the schedule
    # TBD firsttag must be connected to something that runs before it
    # TBD lasttag must be connected to something within the convergence loop

    # TBD funcret checks
    # TBD id's start at 1 and form a series of numbers each being one more than the previous

    # TBD Priming Checks
    # TBD namelist names are distinct across different files

    print "*** Checking that all definition models are called"
#    abort=False
#    deploymentModelNames=coupledDocInfo.getDeploymentInfo().getDeploymentModelNames()
#    for modelInfo in coupledDocInfo.getDefinitionsInfo():
#      if modelInfo.name not in deploymentModelNames and modelInfo.type!="control":
#        print "Error: the model "+modelInfo.name+" is specified in the coupled document but is not named in a deployment unit"
#        abort=True
#    if abort: exit(1)
#    else : print "Checks are OK"

    deploymentModelNames=coupledDocInfo.getDeploymentInfo().getDeploymentModelNames()
    for modelInfo in coupledDocInfo.getDefinitionsInfo():
      if modelInfo.name not in deploymentModelNames and modelInfo.type!="control":
        print "Error: the model "+modelInfo.name+" is specified in the coupled document but is not named in a deployment unit"
        raise BFGConsistencyGeneralError(": No deployment unit uses model {0}".format(modelInfo.name))

    print "Checks are OK"


    print "***Checking that all in's for ep's specified in the schedule have at least one connection"
    for model in coupledDocInfo.getDefinitionsInfo():
      for epName in deploymentInfo.getScheduleInfo().getEPNames(model.name):
          epInfo=model.getEP(epName)
          for fieldInfo in epInfo.getFields():
            if fieldInfo.direction in ["in","inout"] and not (compositionInfo.primed(fieldInfo) or len(compositionInfo.connections(fieldInfo,"in"))>0):
              print "Error, id '"+str(fieldInfo.fieldID)+"' and form '"+str(fieldInfo.form)+"' and possible directions '"+"[\"in\",\"inout\"]"+"' in EntryPoint '"+epName+"' in Model '"+epInfo.getModelInfo().name+"' requires input but is neither coupled nor primed"
    # RF need to check that only one connection or priming if one exists in above check

    # perform (old style) additional consistency checks that schemas are not able to perform
    checkConstraint(doc,os.path.join(constraintsdir,"AllInsConnected.xsl"),"3")
    checkConstraint(doc,os.path.join(constraintsdir,"PrimingAndSequencing.xsl"),"4")
    checkConstraint(doc,os.path.join(constraintsdir,"PrimeAnOut.xsl"),"5")
    checkConstraint(doc,os.path.join(constraintsdir,"TargetDefined.xsl"),"6")
    checkConstraint(doc,os.path.join(constraintsdir,"TimestepDefined.xsl"),"7")
    checkConstraint(doc,os.path.join(constraintsdir,"TargetSpecific.xsl"),"8")

    print "***TBD Checking for multi-instance support if multi-instances are used"
    print "***TBD Checking that deployment and schedule are consistent"
    print "***TBD Checking that phases are respected"
    print "***TBD Checking that if multiple names then instance is used and that instance numbers are correct"
    print "***TBD Checking that if program compliance then model is in its own DU and SU"
    print "***TBD Checking that inplace data is not specified as inout"
    print "***TBD Checking that frequencies in schedule match with expected values e.g. rate matching transformations have expected rate and timestep info for scientific models is honoured"

    print "***Checking that the sizes of data connected together are consistent."

    if not checkConsistentSizes(coupledDocInfo):
        raise BFGConsistencySizeError()
    print "Checks are OK"

    print "***Checking that the rates of data connected together are consistent."

    if not checkConsistentRates(coupledDocInfo, debug=True):
        raise BFGConsistencyRateError()
    print "Checks are OK"

    # OASIS3 specific checks
    # OASIS3 must have a maximum of one SU per DU
#    if deploymentDocInfo.target=='oasis3':
#      success=True
#      for deploymentUnit in deploymentDocInfo.getDeploymentUnitsInfo():
#        if len(deploymentUnit.getSequenceUnitsInfo())>1:
#          success=False
#          print "OASIS3 only supports 1 SU per DU. However, DU "+str(deploymentUnit.getID())+" has "+str(len(deploymentUnit.getSequenceUnitsInfo()))+" SU's"
#      if not success: exit(1)
#      else : print "Checks are OK"

#  Test...
#    for deploymentUnit in deploymentDocInfo.getDeploymentUnitsInfo():
#        raise BFGTargetOASIS3Error(deploymentUnit.getID())

    if deploymentDocInfo.target.lower() == 'oasis3':
        for deploymentUnit in deploymentDocInfo.getDeploymentUnitsInfo():
            if any(map(lambda x: x>1, deploymentUnit.getSequenceUnitsInfo())):
                raise BFGTargetOASIS3Error(deploymentUnit.getID())

  else:
    raise BFGSyntaxVersionError(deploymentDocInfo)

  print "All checks successfully passed"

def checkModelExistsUnique(coupledDocInfo,modelName,epName,form,directions,id):
  success=True
  if not coupledDocInfo.definitionInfoExists(modelName):
    print "Model '"+modelName+"' defined in composition does not exist"
    success=False
  else:
    modelInfo=coupledDocInfo.getDefinitionInfo(modelName)
    if not modelInfo.epNameExists(epName):
      print "EntryPoint '"+epName+"' in Model '"+modelName+"' defined in composition does not exist"
      success=False
    else:
      epInfo=modelInfo.getEP(epName)
      if not epInfo.fieldExists(id,form,directions):
        print "id '"+id+"' and form '"+str(form)+"' and possible directions '"+str(directions)+"' in EntryPoint '"+epName+"' in Model '"+modelName+"' defined in composition does not exist"
        success=False
      elif not epInfo.fieldUnique(id,form,directions):
        print "id '"+id+"' and form '"+str(form)+"' and possible directions '"+str(directions)+"' in EntryPoint '"+epName+"' in Model '"+modelName+"' defined in composition do not uniquely define a field"
        success=False
  return success



# for all fields in a composition set, make sure the sizes are the same.
def checkConsistentRates(coupledDocInfo,debug=False):

  # assume the best
  success=True
  # get out composition object
  compositionDocInfo=coupledDocInfo.getCompositionInfo()
  sets=compositionDocInfo.getSets()
  if len(sets)>0:
    print "Warning, there are currently no checks for rate matching with set notation"
  p2pConnections=compositionDocInfo.getP2P()
  for p2pConnection in p2pConnections:

    inFieldRef=p2pConnection.getInField()
    outFieldRef=p2pConnection.getOutField()

    deploymentInfo=coupledDocInfo.getDeploymentInfo()
    scheduleInfo=deploymentInfo.getScheduleInfo()

    inRate=scheduleInfo.getNormRate(inFieldRef)
    outRate=scheduleInfo.getNormRate(outFieldRef)
    # TBD: this check needs to be improved as it neglects the location of the ep in the schedule. Hence models coupled from different loops will not flag an error if their local rates are the same.
    if inRate!=outRate:
      print "Warning: specified rates do not match for two models coupled using point-to-point notation"
      print "outModel '"+outFieldRef.modelName+"' outEP '"+outFieldRef.epName+"' outID '"+outFieldRef.id+"' rate '"+str(outRate)
      print "inModel '"+inFieldRef.modelName+"' inEP '"+inFieldRef.epName+"' inID '"+inFieldRef.id+"' rate '"+str(inRate)
      #success=False
  return success

# for all fields in a composition set, make sure the sizes are the same.
def checkConsistentSizes(coupledDocInfo,debug=False):

  # assume the best
  success=True
  # get out composition object
  compositionDocInfo=coupledDocInfo.getCompositionInfo()
  # get a list of set objects from our composition object
  sets=compositionDocInfo.getSets()
  # look at each set individually
  if debug: print "found",len(sets),"sets in the composition document"
  for mySet in sets:
    if debug: print "set has",mySet.numFields,"fields"
    # only check if we have more than one field in a set
    if mySet.numFields>1:
      # iterate over each fieldref object in the set
      previousSize=None
      currentSize=None
      for fieldRef in mySet.getFields():
        if debug: print "Checking size for field id",fieldRef.id,"in ep",fieldRef.epName,"in model",fieldRef.modelName,"instance",fieldRef.instanceID
        # find the associated field information
        fieldInfo=fieldRef.fieldObject
        if fieldInfo.knownSize:
          currentSize=fieldInfo.size
        if previousSize:
          if currentSize!=previousSize:
            print "Error: sizes do not match in set '"+mySet.name+"'"
            print "Field id '"+prevFieldRef.id+"' in ep '"+prevFieldRef.epName+"' in model '"+prevFieldRef.modelName+"' instance '"+str(prevFieldRef.instanceID)+"' has size '"+str(previousSize)+"'"
            print "Field id '"+fieldRef.id+"' in ep '"+fieldRef.epName+"' in model '"+fieldRef.modelName+"' instance '"+str(fieldRef.instanceID)+"' has size '"+str(currentSize)+"'"
            success=False
        previousSize=currentSize
        prevFieldRef=fieldRef
  return success
          
def checkDeployNames1(CoupledDocInfo):

  deploymentDocInfo=CoupledDocInfo.getDeploymentInfo()
  defnModelNames=CoupledDocInfo.getDefinitionModelNames()

  success=True
  # for each model defined in our deployment deploymentunits
  for unitModelName in deploymentDocInfo.getDeploymentModelNames():
    if not unitModelName in defnModelNames:
      print "ERROR, model '"+unitModelName+"' specified in deployment document (deploymentUnits) has no associated definition."
      success=False
  return success

def checkDeployNames2(deploymentDocInfo):

  success=True
  uniqueList=[]
  for unitModelRef in deploymentDocInfo.getDeploymentModels():
    unitModelTuple=(unitModelRef.modelName,unitModelRef.instanceID)
    if unitModelTuple in uniqueList:
      if unitModelRef.hasInstanceID:
        print "Error, model '"+unitModelRef.modelName+"' instance '"+str(unitModelRef.instanceID)+"' must only be specified once in deploymentUnits within a deployment document"
      else:
        print "Error, model '"+unitModelRef.modelName+"' must only be specified once in deploymentUnits within a deployment document"
      success=False
    else:
      uniqueList.append(unitModelTuple)
  # RF TODO delete uniqueList here?
  return success

def checkDeployNames3(coupledDocInfo):

  deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
  definitionDocsInfo=coupledDocInfo.getDefinitionsInfo()
  success=True

  # for each model defined in our deployment schedule
  for modelName in deploymentDocInfo.getScheduleModelNames():
    found=False
    if not coupledDocInfo.definitionInfoExists(modelName):
      print "Error: deployment schedule specifies model '"+modelName+"' but there is no associated model defined."
      success=False

  # don't bother with checks on eps if models fail
  if not success : return success

  # for each ep defined in our deployment schedule
  for nameTuple in deploymentDocInfo.getScheduleEPAndModelNames():
    modelName=nameTuple[0]
    epName=nameTuple[1]
    definitionDocInfo=coupledDocInfo.getDefinitionInfo(modelName)
    if not definitionDocInfo.epNameExists(epName):
      print "Error: deployment schedule specifies model '"+modelName+"' ep '"+epName+"' but there is no associated entry point in the definition of that model."
      success=False
  return success

def checkDeployNames4(deploymentDocInfo):

  success=True
  # loop over deployment DU/SU model names
  for unitModelRef in deploymentDocInfo.getDeploymentModels():
    found=False
    # loop over deployment schedule model names
    for scheduleEP in deploymentDocInfo.getScheduleEPs():
      # make sure that all deployment DU/SU model names,id tuples are
      # specified in the deployment schedule
      if unitModelRef.modelName==scheduleEP.modelName and unitModelRef.instanceID==scheduleEP.instanceID:
        found=True
        break
    if not found:
      success=False
      print "Error, model '"+unitModelRef.modelName+"' instance '"+str(unitModelRef.instanceID)+"' is specified in the deployment document deployment units but does not exist in the deployment document schedule"
  return success

def checkDeployNames5(deploymentDocInfo):

  success=True
  # loop over deployment schedule model names
  modelEPList=[]
  for scheduleEP in deploymentDocInfo.getScheduleEPs():
    if scheduleEP.instanceID:
      raise("Error, instances not yet supported, sorry")
    name=scheduleEP.modelName+scheduleEP.epName+str(scheduleEP.instanceID)
    if name not in modelEPList:
      modelEPList.append(name)
    else:
      success=False
      print "Error, there is more than one instance of model '"+scheduleEP.modelName+"' ep '"+scheduleEP.epName+"' instance '"+str(scheduleEP.instanceID)+"' specified in the deployment document schedule."
  return success

def checkDefinitionNames(definitionDocInfo,debug=False):
  modelName=definitionDocInfo.name
  if debug: print "DEBUG: model name",modelName
  epNames=definitionDocInfo.getEPNames()
  if debug: print "DEBUG: model entry points",epNames
  modelLanguage=definitionDocInfo.language
  if debug: print "DEBUG: model language",modelLanguage
  success=True
  # are epNames unique?
  uniqueEPNames=[]
  for name in epNames:
    if name not in uniqueEPNames:
      uniqueEPNames.append(name)
    else:
      print "Error: the entry point name",name,"is used more than once in this definition document"
      success=False

  # is model name distinct from epnames
  if not modelLanguage==definitionDocInfo.f77String: # f77 can have the same ep and model name so skip this test
    if modelName in epNames:
      print "Error: the model name (or module name if appropiate)",modelName,"has the same name as one of its entry points."
      success=False
  return success

def checkConstraint(doc,CurrentTest,id):

  xsltTree = et.parse(CurrentTest)
#  try:
#    xsltTree = et.parse(CurrentTest)
#  except IOError,e:
#    print "IO error: Unable to read XSLT source file {0}".format(CurrentTest)
#    exit(1)
#  except et.XMLSyntaxError,e:
#    print "XML Syntax error: Unable to parse source file {0}".format(CurrentTest)
#    exit(1)

  xslt = et.XSLT(xsltTree)
  result = xslt(doc)

#  try :
#    result = xslt(doc)
#  except et.XSLTApplyError as e :
#    print "Check "+CurrentTest+" found errors in the xml, aborting"
#    print e
#    exit(1)
#  except :
#    print "Check "+CurrentTest+" found unknown errors in the xml, aborting"
#    exit(1)

  # remove any blank lines in our output
  text = os.linesep.join([s for s in str(result).strip().splitlines() if s])
  print text
  log = xslt.error_log
  if len(log) == 0:
    print "Checks are OK"
  else:
    print "ERROR: documents are invalid"
    for line in str(log).splitlines() :
      lhs,sep,rhs=line.partition(' ')
      if sep=='':
        # our split has failed so just output the whole line
        print "MESSAGE:",line
      else:
        # we have hopefully removed the preamble before our error message
        print "MESSAGE:",rhs
    raise BFGConsistencyGeneralError(CurrentTest)


def validate(BFGDoc,schemaFile,silent=False) :
  # load our locally cached schema
  try:
    schema = et.XMLSchema(et.parse(schemaFile))
  except et.XMLSyntaxError as e:
    print "Oh dear, internal BFG error. The schema file is not valid XML: {0}".format(e.text)
    exit(1)
  except et.XMLSchemaError as e:
    print "Oh dear, internal BFG error. We are getting an invalid schema format:",e
    exit(1)

  # check document against our local cached schema
  schema.validate(BFGDoc)
  log = schema.error_log
  if len(log) == 0:
    if not silent:
      print "document conforms to BFG2 schema"
    return True
  else:
    if not silent:
      print "document does not conform to BFG2 schema"
      print log
    return False

def validXML(documentPath):
  # is this document valid XML?
  try:
    BFGDoc = et.parse(documentPath)
  except IOError as e:
    print "Error, input document does not exist"
    print e
    sys.exit(1)
  except et.XMLSyntaxError as e:
    print "Error, input document is not valid XML"
    print e
    sys.exit(1)
  return BFGDoc

def bfg2DefinitionDoc(BFGDoc,verbose=False):
  if BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:] != "definition" :
    if verbose:
      print "invalid bfg2 definition document : root document is "+ BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:]+" but expected 'definition'"
    return False
  return True

def rootElement(BFGDoc,element):
  if BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:] != element:
    return False
  return True
# basic check to see if root element is correct
def bfg2CoupledDoc(BFGDoc,verbose=False) :
  if BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:] != "coupled" :
    if verbose:
      print "invalid bfg2 coupled document : root document is "+ BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:]+" but expected 'coupled'"
    return False
  return True
  #BFG2CoupledElements=['models','composition','deployment']
  #BFG2Doc=True
  #for element in BFGDoc.getroot() :
  #  if isinstance(element.tag, str) : # then I am an element (not a comment)
  #    # remove any namespace before checking
  #    if not element.tag[element.tag.rfind('}')+1:] in BFG2CoupledElements :
  #      if verbose:
  #        print "invalid bfg2 coupled document : found element 'coupled/"+element.tag[element.tag.rfind('}')+1:]+"' which is not one of", BFG2CoupledElements
  #      BFG2Doc=False
  #return BFG2Doc



  
def bfgVersion(BFGDoc):
  # does this document look like a BFG coupled document?
  # remove any namespace before checking
  if BFGDoc.getroot().tag[BFGDoc.getroot().tag.rfind('}')+1:] != "coupled" :
    print "Error : Expecting a BFG coupled document which has 'coupled' as its root element, but found '"+BFGDoc.getroot().tag+"'"
    sys.exit(1)
	
  # does this document look like a BFG1 or BFG2 coupled document?
  BFG1CoupledElements=['component','compose','deploy','run']
  BFG2CoupledElements=['models','composition','deployment','name','description']

  BFG1Doc=True
  BFG2Doc=True
  for element in BFGDoc.getroot() :
    if isinstance(element.tag, str) : # then I am an element (not a comment)
      # remove any namespace before checking
      if not element.tag[element.tag.rfind('}')+1:] in BFG1CoupledElements :
        BFG1Doc=False
      if not element.tag[element.tag.rfind('}')+1:] in BFG2CoupledElements :
        BFG2Doc=False

  if BFG1Doc and BFG2Doc :
    print "Error : expecting a valid BFG coupled document but no content was found within the top level coupled element"
    sys.exit(1)

  if (not BFG1Doc) and (not BFG2Doc) :
    print "Error : Expecting a valid BFG coupled document but at least one element does not conform to either a BFG1 or a BFG2 document"
    sys.exit(1)

  if BFG1Doc :
    print "File content implies I am a BFG1 coupled document"
    print "Error : BFG1 documents not currently directly supported. Please run the translator first."
    sys.exit(1)
  else :
    print "File content implies I am a BFG2 coupled document"
  if BFG1Doc:
    return 1
  else:
    return 2


def validAgainstBFG2Schemas(BFGDoc,CoupledDocumentDir):

  pathname=os.path.dirname(sys.argv[0])
  BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))

  print "checking coupled document"
  if not validate(BFGDoc,os.path.normpath(os.path.join(BFG2ROOT,'schema/coupled.xsd'))):
    print "Error in coupled document"
    exit(1)

  for docs in BFGDoc.getroot() :
    if isinstance(docs.tag,str):
      if docs.tag == 'models' :
        for model in docs :
          if model.tag=='model' :
            print "checking definition document '"+model.text+"'"
            tmpDoc=validXML(os.path.join(CoupledDocumentDir,model.text))
            if not validate(tmpDoc,os.path.normpath(os.path.join(BFG2ROOT,'schema/definition.xsd'))):
              print "Error in definition document"
              exit(1)

      elif docs.tag == "composition" :
        print "checking composition document '"+docs.text+"'"
        tmpDoc=validXML(os.path.join(CoupledDocumentDir,docs.text))
        if not validate(tmpDoc,os.path.normpath(os.path.join(BFG2ROOT,'schema/composition.xsd'))):
          print "Error in composition document"
          exit(1)
      elif docs.tag == "deployment" :
        print "checking deployment document '"+docs.text+"'"
        tmpDoc=validXML(os.path.join(CoupledDocumentDir,docs.text))
        if not validate(tmpDoc,os.path.normpath(os.path.join(BFG2ROOT,'schema/deployment.xsd'))):
          print "Error in deployment document"
          exit(1)
      elif docs.tag == "name" or docs.tag == "description":
        pass

      else :
        print "Error, found unknown document type",docs.tag
        sys.exit(1)
