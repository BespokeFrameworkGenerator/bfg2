
class BFGValidationError(Exception):


	def _get_msg(self):
		return self.__message

	def _set_msg(self, msg):
		self.__message = msg

	message = property(_get_msg, _set_msg)


	def __init__(self, msg=""):
		self.message = msg
		Exception.__init__(self, msg)

	def bfgerrtype(self):
		return self.__class__.__name__

	def __str__(self):
		return self.message

	def __repr__(self):
		return "[{0}] {1}".format(self.__class__.__name__, self.message)



#  Name Errors...

class BFGNameReferentialError(BFGValidationError):

	def __init__(self, name, doc):
		text = "The entity {0} declared in document {1} does not exist.".format(name, doc)
		BFGValidationError.__init__(self, text)



class BFGNameUniquenessError(BFGValidationError):

	def __init__(self, name, *docs):
		text = "Multiple declarations of entity {0}.".format(name)
		BFGValidationError.__init__(self, text)



class BFGNameReservedError(BFGValidationError):

	def __init__(self, name, doc=""):
		text = "Invalid name for entity {0}: this name is reserved for internal BFG usage.".format(name)
		BFGValidationError.__init__(self, text)



#  Consistency Errors...

class BFGConsistencyEPError(BFGValidationError):

	def __init__(self, epname, model=""):
		text = "EntryPoint {0} in Model {1} must have at least one connection".format(epname, model)
		BFGValidationError.__init__(self, text)


class BFGConsistencySizeError(BFGValidationError):

	def __init__(self, msg=""):
		text = "Inconsistent sizes for connected data."		#  Add data names
		BFGValidationError.__init__(self, text)

class BFGConsistencyRateError(BFGValidationError):

	def __init__(self, msg=""):
		text = "Inconsistent rates for connected data."		#  Add data names
		BFGValidationError.__init__(self, text)


class BFGConsistencyGeneralError(BFGValidationError):

	def __init__(self, msg="", test=""):
		text = "Inconsistency detected in application of test {0}.".format(test)
		BFGValidationError.__init__(self, text)



#  Target-specific errors...

class BFGTargetOASIS3Error(BFGValidationError):

	def __init__(self, du):
		text = "Deployment Unit {0} has multiple Sequence Units, only one per DU supported.".format(du)
		BFGValidationError.__init__(self, text)



#  Metadata Syntax errors...

class BFGSyntaxSchemaError(BFGValidationError):

	def __init__(self, docname, doctype):
		text = "Document {0} does not conform to schema for type {1}".format(docname, doctype)
		BFGValidationError.__init__(self, text)



class BFGSyntaxVersionError(BFGValidationError):

	def __init__(self, docname):
		text = "Document {0} has unexpected BFG version number.".format(docname)
		BFGValidationError.__init__(self, text)



if __name__ == "__main__":

	err = BFGTargetOASIS3Error("<duname>")
	print(repr(err))
	print(err)
	raise err
