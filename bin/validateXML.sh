#!/bin/sh
for arg in "$@"
do
 java -cp ../java Validator $arg
 if [ $? -ne 0 ] ; then
     echo "Error in validation"
     exit 1
 fi

done
