#! /usr/bin/env python

# BFG2 alpha python script
# CNC RF Created on 15th Feb 2013

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as et

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0"
parser = OptionParser(usage)

(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

# work out BFG2ROOT from script location
# print "Working dir", os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
# print "BFG2ROOT",BFG2ROOT
# xsltdir = os.path.join(BFG2ROOT,"xslt")
visdir = os.path.join(BFG2ROOT,"vis")
# print "xsltdir",xsltdir

try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

def validXML(documentPath):
  # is this document valid XML?
  try:
    BFGDoc = et.parse(documentPath)
  except IOError as e:
    print "Error, input document does not exist"
    print e
    sys.exit(1)
  except et.XMLSyntaxError as e:
    print "Error, input document is not valid XML"
    print e
    sys.exit(1)
  return BFGDoc

try:
  currentFile=os.path.join(visdir,"dotgen_id.xsl")
  xsltTree = et.parse(currentFile)
except IOError,e:
  print "IO error: Unable to read XSLT source file {0}".format(currentFile)
  exit(1)
except et.XMLSyntaxError,e:
  print "XML Syntax error: Unable to parse source file {0}".format(currentFile)
  exit(1)
xslt = et.XSLT(xsltTree)
try :
  doc=validXML(CoupledDocumentPathNoQuote)
  result = xslt(doc, CoupledDocument=CoupledDocumentPath)
except et.XSLTApplyError as e :
  print "Check "+currentFile+" found errors in the xml, aborting"
  print e
  exit(1)
except :
  print "Check "+currentFile+" found unknown errors in the xml, aborting"
  exit(1)
file=open('CoupledGraph.dot','w')
file.write(str(result))
print "Created file CoupledGraph.dot"

try:
  currentFile=os.path.join(visdir,"dotgen2.xsl")
  xsltTree = et.parse(currentFile)
except IOError,e:
  print "IO error: Unable to read XSLT source file {0}".format(currentFile)
  exit(1)
except et.XMLSyntaxError,e:
  print "XML Syntax error: Unable to parse source file {0}".format(currentFile)
  exit(1)
xslt = et.XSLT(xsltTree)
try :
  doc=validXML(CoupledDocumentPathNoQuote)
  result = xslt(doc, CoupledDocument=CoupledDocumentPath)
except et.XSLTApplyError as e :
  print "Check "+currentFile+" found errors in the xml, aborting"
  print e
  exit(1)
except :
  print "Check "+currentFile+" found unknown errors in the xml, aborting"
  exit(1)
file=open('CoupledSchedule.dot','w')
file.write(str(result))
print "Created file CoupledSchedule.dot"
