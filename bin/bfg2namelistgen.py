import sys
import os
import glob

from pprint import pprint
from argparse import ArgumentParser, RawDescriptionHelpFormatter, ArgumentError

PATTERN = "BFG2Main*.f90"
SUFFIX = "_du{0}"

prog = os.path.splitext(sys.argv[0])[0]



class Namelist_MissingParameterError(Exception):
	
	def __init__(self, namelist, param):
		self.namelist = namelist
		self.param = param

	def __str__(self):
		return "Parameter {0} not found for namelist {1}.\n".format(self.param, self.namelist.mainfile)
	



class Namelist_UnusedArgumentError(Exception):
	pass



class HelpArgumentParser(ArgumentParser):

	def error(self, msg):
		action = self._actions.pop()
		raise ArgumentError(action, msg)


def failWith(message, errno):

	print >> sys.stderr, ("[{prog}] {msg}".format(prog=prog, msg=message))
	sys.exit(errno)


class Namelist(object):


	def __init__(self, mainfile, srcline):
		self.mainfile = mainfile
		self.srcline = srcline
		self.params = dict()
		self.setParams()


	def setParams(self):
		params_token_start = self.srcline.rfind(" ") + 1
		for param in self.srcline[params_token_start:].split(","):
			self.params[param] = None


	def __str__(self):
		retstr = ""

		for param, arg in self.params.items():
			retstr += "{0} = {1}\n".format(param, arg)

		return retstr


	def writeFile(self, namelist_name):
		namelist_file = MASTER_NAMELIST + SUFFIX.format(self.mainfile[8:-4])

		if parser_args.outputdir:
			namelist_file = os.path.join(parser_args.outputdir, namelist_file)


		filedata = "&{0}\n".format(namelist_name)
		filedata += str(self)
		filedata += "/\n"
	
		try:
			with open(namelist_file, 'w') as outfile:
				outfile.write(filedata)
		except EnvironmentError as e:
			failWith("Unable to write to file {0}\n{1}".format(outfile, e), 1)



desc = """Decomposes a sequential deployment's namelist into a set of namelists
corresponding to a specified parallel deployment."""

usage = "%(prog)s [-n <namelist>] [-d <output_directory>]"

parser = HelpArgumentParser(formatter_class=RawDescriptionHelpFormatter, description=desc, usage=usage)
parser.add_argument("-n", "--namelist", dest="namelist", help="Filename of master namelist")
parser.add_argument("-d", "--directory", dest="outputdir", help="Directory to which namelists will be written")

try:
	parser_args = parser.parse_args()
except ArgumentError as e:
	parser.print_help(sys.stderr)
	sys.exit(1)


MASTER_NAMELIST = parser_args.namelist if parser_args.namelist else "bfg2Data.in" 

mainfiles = sorted(glob.glob(PATTERN), key=lambda s: int(s[8:-4]))

namelist_srclines = []

for mf in mainfiles:
	try:
		with open(mf, 'r') as fp:
			for line in fp.readlines():
				if "namelist" in line:
					namelist_srclines.append( Namelist(mf, line.rstrip()) )
					break
	except EnvironmentError as e:
		failWith("Unable to read from Main file {0}\n{1}".format(mf, e), 1)
			

try:
	with open(MASTER_NAMELIST, 'r') as fp:
		namelist_name = fp.readline().lstrip('&').strip()
		master_namelist_src = fp.readlines()

except EnvironmentError as e:
	failWith("Unable to read master namelist file {0}\n{1}".format(MASTER_NAMELIST, e), 1)



master_arglist = dict()

for line in master_namelist_src:
	if '=' in line:
		tokens = line.split('=')
		k = tokens[0].strip()
		v = tokens[1].strip()

		master_arglist[k] = v


ma_items = len(master_arglist)

for n in namelist_srclines:
	for param in n.params.keys():
		arg = master_arglist.get(param)
		if arg:
			n.params[param] = arg
			del master_arglist[param]
		else:
			raise Namelist_MissingParameterError(n, param)

	n.writeFile(namelist_name)

if len(master_arglist) > 0:
	for unused in master_arglist:
		print >> sys.stderr, ("Warning: Unused parameter {0} in namelist {1}".format(unused, MASTER_NAMELIST))
