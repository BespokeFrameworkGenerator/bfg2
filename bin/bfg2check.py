#! /usr/bin/env python

# BFG2 alpha python script
# CNC RF Created on 24th November 2005

import shutil
import sys
import os
from optparse import OptionParser
import libxml2
import libxslt
import check
from check import checkException

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0alpha"
parser = OptionParser(usage)

(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")

try:
  check.run(args[0])
except checkException, e:
  print "Check error: "+str(e)
  exit(1)
