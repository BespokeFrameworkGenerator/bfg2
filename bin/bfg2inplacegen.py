#! /usr/bin/env python

import shutil
import sys
import os
from optparse import OptionParser
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map
#from bfgutils import definitionInfo, map
#from bfgutils import getValue
import check

# work out BFG2ROOT from script location
wd=os.getcwd()
pathname=os.path.dirname(sys.argv[0])
BFG2ROOT = os.path.normpath(os.path.join(os.path.abspath(pathname),".."))
templatedir = os.path.join(BFG2ROOT,"templates")

usage = "usage: %prog [options] BFG2CoupledDocument.xml"
version = "1.0"
parser = OptionParser(usage)
(options, args) = parser.parse_args()

if len(args) != 1:
  parser.error("incorrect number of arguments")


try:
  f = open(args[0], 'r')
except IOError, (errno, strerror):
  print "I/O error(%s) for file %s: %s" % (errno, args[0], strerror)
  sys.exit(1)

debug=False

CoupledDocumentPathNoQuote = os.path.abspath(args[0])
CoupledDocumentPath = "'" + CoupledDocumentPathNoQuote + "'"

# Parse our coupled model metadata (assuming it is valid)
coupledDocInfo=coupledInfo(CoupledDocumentPathNoQuote,"",debug=debug)
deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
compositionDocInfo=coupledDocInfo.getCompositionInfo()

deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
for deploymentUnitInfo in deploymentUnits :

  print "HELLO"
  exit(1)
  assert deploymentUnitInfo.language=="f90","Only f90 supported so far, but found "+str(deploymentUnitInfo.language)

  # Read in our XML template
  f90inplaceTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_inplace.xml"))

  # determine the specific put/get functions required by this coupling
  functionNamesMap={}
  functionNamesInList=[]
  functionNamesOutList=[]
  ModelEPInstanceInThisDU=[]
  # for each model in this du
  for modelName in deploymentUnitInfo.getModelNames() :
     modelInfo=coupledDocInfo.getDefinitionInfo(modelName)
     # for each ep in this model
     for epInfo in modelInfo.getEPs() :
       # for each field in this ep
       for dataInfo in epInfo.getFields() :
         assert dataInfo.form=="inplace", "Only coded for inplace so far, but found "+dataInfo.form
         # is this field connected using p2p?
         if compositionDocInfo.p2pConnection(dataInfo):
           functionName=dataInfo.getFunctionName()
           if not functionName in functionNamesMap:
             functionNamesMap[functionName]=[]
           functionNamesMap[functionName].append(dataInfo)
           if dataInfo.direction=="in":
             if functionName not in functionNamesInList:
               functionNamesInList.append(functionName)
           if dataInfo.direction=="out":
             if functionName not in functionNamesOutList:
               functionNamesOutList.append(functionName)
           if (debug) : print "Found p2p connection:",modelName,epInfo.epName,dataInfo.fieldID,dataInfo.direction
         else:
           if (debug) : print "Not found p2p connection:",modelName,epInfo.epName,dataInfo.fieldID,dataInfo.direction

  functionInfo=[]
  for functionName in functionNamesMap.keys():

    # sort the coupled data within this function by entry point
    EPMap={}
    for dataInfo in functionNamesMap[functionName]:

      ep=dataInfo.getEPInfo()
      if not ep in EPMap:
        EPMap[ep]=[]
      EPMap[ep].append(dataInfo)

      # loop over each EP
      EPInThisDU=[]
      for ep in EPMap.keys():

        # loop over data within this EP
        dataInThisEP=[]
        for dataInfo in EPMap[ep]:

          remoteDataInfo=compositionDocInfo.p2pRemoteField(dataInfo)
          # START TARGET SPECIFIC
          # MPI
          if dataInfo.direction=="in" :
            MPICall="mpi_recv"
            MPISTATUS="istatus,"
          else :
            MPICall="mpi_send"
            MPISTATUS=""
          MPITYPE="mpi_"+dataInfo.dataType
          remoteModelName=remoteDataInfo.getEPInfo().getModelInfo().name
          B2MMapValue=deploymentDocInfo.getSequenceID(remoteModelName)
          # choose to use the id of the receiver as the tag for both sender and receiver
          if dataInfo.direction=="in":
            TAGValue=dataInfo.getGlobalID()
          else:
            TAGValue=remoteDataInfo.getGlobalID()

          TargetSpecificCommsCode="call "+MPICall+"(arg1,mysize,"+MPITYPE+",b2mmap("+str(B2MMapValue)+"),"+str(TAGValue)+",mpi_comm_world,"+MPISTATUS+"ierr)"
          # END TARGET SPECIFIC

          dataInThisEP.append({"DataID":dataInfo.fieldID,
                               "DataSize":dataInfo.size,
<!--
                               "firstValue":"HELLO",
                               "lastValue":"HELLO",
-->
                               "TargetSpecificCommsCode":TargetSpecificCommsCode})

        EPInThisDU.append({"GlobalEPID":ep.getGlobalID(),
                           "EPName":ep.epName,
                           "ModelName":ep.getModelInfo().name,
                           "DataInThisEP":dataInThisEP})

      #EPInThisDU.append({"ModelID":model.myID,
      #                   "ModelName":model.name,
      #                   "EPInThisDU":EPInThisDU})

    # START TARGET SPECIFIC
    # MPI
    InPlaceDeclarations="integer :: istatus(mpi_status_size),ierr"
    # END TARGET SPECIFIC


    # shortcut : use the last dataInfo object to provide function info as any one will do
    functionInfo.append({"InPlaceProcedureName":dataInfo.getFunctionName(),
                         "InPlaceDimensionDeclaration":dataInfo.f90ArrayDims(),
                         "InPlaceIntent":dataInfo.direction,
                         "InPlaceTagDatatype":dataInfo.getf90InPlaceTagDatatype(),
                         "InPlaceDeclarations":InPlaceDeclarations,
                         "EPInThisDU":EPInThisDU})

  # a list of unique input function names
  functionNamesIn=[]
  for functionName in functionNamesInList:
    functionNamesIn.append({"InPlaceProcedureName":functionName})

  # a list of unique output function names
  functionNamesOut=[]
  for functionName in functionNamesOutList:
    functionNamesOut.append({"InPlaceProcedureName":functionName})

# generic f90 mapping
  mapping={"DUID":deploymentUnitInfo.getID(),
           "InPlaceGetProcedureDefinitions":functionNamesIn,
           "InPlacePutProcedureDefinitions":functionNamesOut,
           "InPlaceProcedures":functionInfo}

  map(mapping,f90inplaceTemplate)

  # BEGIN TARGET SPECIFIC
  # MPI
  mpi_mapping={"TargetSpecificIncludes":"use mpi",
          }

  map(mpi_mapping,f90inplaceTemplate)
  # END TARGET SPECIFIC


  #print(ET.tostring(f90inplaceTemplate, method="xml",pretty_print=True))
  #print(ET.tostring(f90inplaceTemplate, method="text",pretty_print=True))
  f90inplaceTemplate.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+".f90",method="text")
