#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue, functionInfo

def genDU(deploymentUnitInfo,BFG2ROOT):

    # limit code generation to what we support
    assert deploymentUnitInfo.language=="f90","Only f90 supported so far, but found "+str(deploymentUnitInfo.language)

    coupledDocInfo=deploymentUnitInfo.getDeploymentInfo().getCoupledInfo()
    compositionDocInfo=coupledDocInfo.getCompositionInfo()

    templatedir = os.path.join(BFG2ROOT,"templates")

    # Read in our f90 XML template
    # should be based on required language but this is only f90 at the moment
    targetTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_oasis3_target.xml"))

    # get all coupling fields in this du that require put/get
    fieldGetList=[]
    fieldPutList=[]
    scheduleInfo=deploymentUnitInfo.getDeploymentInfo().getScheduleInfo()
    # get each model in this deployment unit
    for model in deploymentUnitInfo.getModels():
      # get each active entry points
      for epName in scheduleInfo.getEPNames(model.name):
        epInfo=model.getEP(epName)
        # get each field
        for fieldInfo in epInfo.getFields():
          if len(compositionDocInfo.putgetConnections(fieldInfo,"in"))>0:
            # keep get connected fields
            fieldGetList.append(fieldInfo)
          if len(compositionDocInfo.putgetConnections(fieldInfo,"out"))>0:
            # keep put connected fields
            fieldPutList.append(fieldInfo)

    declarationList=[]
    count=1
    for field in fieldGetList:
      declarationList.append({"index":count,
                              "VarName":field.getName(),
                              "lo":1,
                              "hi":field.size,
                              "direction":"In",
                              "datatype":field.getDataType("f90")})
      count+=1

    for field in fieldPutList:
      declarationList.append({"index":count,
                              "VarName":field.getName(),
                              "lo":1,
                              "hi":field.size,
                              "direction":"Out",
                              "datatype":field.getDataType("f90")})
      count+=1

    supportRoutinesList=[]
    for model in deploymentUnitInfo.getModels():
      supportRoutinesList.append({"modelName":model.name})

    mapping={"DUID":deploymentUnitInfo.getID(),
             "DUName":deploymentUnitInfo.getUniqueName(),
             "nFields":len(fieldGetList)+len(fieldPutList),
             "supportRoutines":supportRoutinesList,
             "declarations":declarationList}

    map(mapping,targetTemplate)

    targetTemplate.write("BFG2Target"+str(deploymentUnitInfo.getID())+".f90",method="text")
