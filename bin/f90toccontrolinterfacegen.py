import os
import bfgutils
from bfgutils import map,listString

def run(deploymentUnitInfo,BFG2ROOT):

  print("[MODULE] {0}".format(__name__))
  print

  templatedir = os.path.join(BFG2ROOT,"templates")

  # Read in our XML template
  controlTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_to_c.xml"))

  scheduleInfo=deploymentUnitInfo.getDeploymentInfo().getScheduleInfo()
  epInfoList=[]
  for modelInfo in deploymentUnitInfo.getModels():
      if modelInfo.language=="c":
          if not modelInfo.programConformance():
              for epName in scheduleInfo.getEPNames(modelInfo.name):
                  epInfo=modelInfo.getEP(epName)
                  uniqueEPName=epInfo.getUniqueName()

                  # add our arguments
                  epArgsList=epInfo.getArgsList()
                  epArgNamesList=[]
                  epDefnsList=[]
                  for epArg in epArgsList:
                    epArgNamesList.append(epArg.getDataObject().getName())
                    datatype=epArg.getDataType("c")
                    epDefnsList.append(datatype+"* "+epArg.getDataObject().getName())
                  epArgs=listString(epArgNamesList)
                  epDefns=listString(epDefnsList)

                  # fortran symbols are cast to lower case but C symbols are not
                  uniqueEPName=uniqueEPName.lower()
                  epInfoList.append({"epNamef90":f90toclink(uniqueEPName),
                                 "epArgsDefns":epDefns,
                                 "epNamec":epName,
                                 "epArgs":epArgs})

  mapping={"code":epInfoList}
  result=map(mapping,controlTemplate)
  result.write("BFG2Control"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")

def f90toclink(name):
    # assume that we add an underscore
    # need to add other options that can be specified by the user
    return name+"_"
