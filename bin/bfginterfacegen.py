#! /usr/bin/env python

import os
from lxml import etree as ET
import bfgutils
from bfgutils import coupledInfo, map, getValue, functionInfo

def genAll(coupledDocInfo,BFG2ROOT):
    print
    deploymentDocInfo=coupledDocInfo.getDeploymentInfo()
    deploymentUnits=deploymentDocInfo.getDeploymentUnitsInfo()
    # generate an inplace routine for each deployment unit
    for deploymentUnitInfo in deploymentUnits :
        genDU(deploymentUnitInfo,BFG2ROOT)

def genDU(deploymentUnitInfo,BFG2ROOT):

    templatedir = os.path.join(BFG2ROOT,"templates")

    coupledDocInfo=deploymentUnitInfo.getDeploymentInfo().getCoupledInfo()
    compositionDocInfo=coupledDocInfo.getCompositionInfo()

    # Read in our f90 XML template
    # should be based on required language but this is only f90 at the moment
    bfgInterfaceTemplate=bfgutils.xmlread(os.path.join(templatedir,"fortran90_bfginterface.xml"))

    if deploymentUnitInfo.requiresInPlace():
        InPlaceString="  use bfg2inplace"+str(deploymentUnitInfo.getID())+", only : put,get"
    else:
        InPlaceString=""
    if deploymentUnitInfo.programConformance():
        TargetString="  use bfg2target"+str(deploymentUnitInfo.getID())+", only  : bfg_init=>initComms,bfg_finalise=>finaliseComms,bfg_eots=>commsSync"
    else:
        TargetString=""
    mapping={"InPlaceRoutines":InPlaceString,
             "TargetRoutines":TargetString}

    map(mapping,bfgInterfaceTemplate)

    bfgInterfaceTemplate.write("BFG2Interface"+str(deploymentUnitInfo.getID())+".f90",method="text")
