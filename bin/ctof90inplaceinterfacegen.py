import os
import bfgutils
from bfgutils import coupledInfo, map, getValue
from lxml import etree as ET

def run(deploymentUnitInfo,BFG2ROOT):

  print("[MODULE] {0}".format(__name__))
  print

  templatedir = os.path.join(BFG2ROOT,"templates")

  # Read in our XML template
  inplaceTemplate=bfgutils.xmlread(os.path.join(templatedir,"c_to_fortran90.xml"))

  # determine whether conformance is via program interface or component interface
  #if not deploymentUnitInfo.programConformance():
  #    print "C mapping only supports program conformance at the moment, sorry."
  #    exit(1)

  functionInfo=[]
  f90PutGetInfo=[]
  for direction in ["in","out"]:
    if direction=="in" : dataDirection="out"
    elif direction=="out" : dataDirection="in"
    else: assert False, "Error"
    keepDataInfo=None
    dataInThisEP=[]
    # Create the information for the required template
    # for each model in this du
    for model in deploymentUnitInfo.getModels():
      # for each entrypoint in this model
      for epInfo in model.getEPs():
        # skip this ep if it has no inplace requirements
        if epInfo.requiresInPlace():
          # for each field in this entrypoint
          dataInThisEP=[]
          for dataInfo in epInfo.getFields():
            # skip this field if it has no inplace requirements or is not the right direction
            if dataInfo.requiresInPlace() and dataInfo.direction==direction:

              keepDataInfo=dataInfo
              f90PutGetInfo.append({"UniqueInPlaceProcedureName":dataInfo.getUniqueFunctionName(),
                                    "PutOrGet":dataInfo.getGenericFunctionName(direction),
                                    "InPlaceDatatype":dataInfo.getDataType("f90"),
                                    "InPlaceIntent":dataDirection,
                                    "InPlaceDimensionDeclaration":dataInfo.arrayDimsSize(),
                                    "InPlaceTagDatatype":dataInfo.getInPlaceTagDatatype(language="f90")})

              dataInThisEP.append({"DataID":dataInfo.fieldID,
                                   "ModelName":epInfo.getModelInfo().name,
                                   "EPName":epInfo.epName,
                                   "InstanceID":"0",
                                   "UniqueInPlaceProcedureName":dataInfo.getUniqueFunctionName(),
                                   "Datatype":dataInfo.dataType})
    if dataInThisEP:
      functionInfo.append({"PutOrGet":keepDataInfo.getGenericFunctionName(direction),
                           "InPlaceTagDatatype":keepDataInfo.getInPlaceTagDatatype(),
                           "DataInThisEP":dataInThisEP})
  cmapping={"putget":functionInfo}
  result=map(cmapping,inplaceTemplate,snippetName="c_putget_interface")
  ETresult=ET.ElementTree(result)
  ETresult.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+"_cwrapper.c",method="text")

  f90mapping={"putget":f90PutGetInfo}
  result=map(f90mapping,inplaceTemplate,snippetName="f90_putget_interface")
  ETresult=ET.ElementTree(result)
  ETresult.write("BFG2InPlace"+str(deploymentUnitInfo.getID())+"_f90wrapper.f90",method="text")
