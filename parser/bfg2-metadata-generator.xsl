<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" indent="yes"/>
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

<xsl:template match="/">
 <definition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:noNamespaceSchemaLocation="/home/armstroc/bfg2/schema/definition.xsd">
  <name><xsl:value-of select="procedure/name"/></name>
  <type>?</type>
  <xsl:choose>
   <xsl:when test="//contains">
    <language>f90</language>
   </xsl:when>
   <xsl:otherwise>
    <language>f77</language>
   </xsl:otherwise>
  </xsl:choose>
  <timestep>?</timestep>
  <entryPoints>
   <xsl:apply-templates select="//procedure"/>
  </entryPoints>
 </definition>
</xsl:template>

<xsl:template match="procedure">
 <xsl:if test="code">
  <xsl:variable name="myname" select="name"/>
  <entryPoint name="{name}" type="init/iteration/final?">
   <xsl:apply-templates select="//symbol[name=$myname]/arguments/argument">
    <xsl:with-param name="epName" select="name"/>
   </xsl:apply-templates>
  </entryPoint>
 </xsl:if>
</xsl:template>

<xsl:template match="argument">
 <xsl:param name="epName"/>
 <xsl:variable name="myintent">
  <xsl:choose>
   <xsl:when test="contains(intent,'UNKNOWN')">
     <xsl:call-template name="deduce-intent">
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="variable" select="name"/>
     </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="translate(intent,$uppercase,$lowercase)"/>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <data name="{name}" direction="{$myintent}" form="argpass" id="{@position}"
       dataType="{translate(type,$uppercase,$lowercase)}" dimension="{count(array/lower)}">
  <xsl:for-each select="array/lower">
   <xsl:variable name="myrank" select="@rank"/>
   <dim value="{$myrank}">
    <lower>
     <xsl:call-template name="print-expr">
      <xsl:with-param name="expr" select="*"/>
     </xsl:call-template>
    </lower>
    <upper>
     <xsl:call-template name="print-expr">
      <xsl:with-param name="expr" select="../upper[@rank=$myrank]/*"/>
     </xsl:call-template>
    </upper>
<!--
     <xsl:choose>
      <xsl:when test="integer and ../lower[@rank=$myrank]/integer">
       <value><xsl:value-of select="number(integer) - number(../lower[@rank=$myrank]/integer) + 1"/></value>
      </xsl:when>
      <xsl:when test="variable or ../lower[@rank=$myrank]/variable">
       <dependsOn>
        <xsl:choose>
         <xsl:when test="variable and ../lower[@rank=$myrank]/varaible">
          <xsl:variable name="uvarname" select="variable/@name"/>
          <xsl:variable name="lvarname" select="../lower[@rank=$myrank]/varaible/@name"/>
          <xsl:value-of select="//argument[name=$uvarname]/@position"/>.and.<xsl:value-of select="//argument[name=$lvarname]/@position"/>
         </xsl:when>
         <xsl:when test="variable">
          <xsl:variable name="uvarname" select="variable/@name"/>
          <xsl:value-of select="//symbol[name=$epName]/arguments/argument[name=$uvarname]/@position"/>
         </xsl:when>
         <xsl:when test="../lower[@rank]/variable/@name">
          <xsl:variable name="lvarname" select="../lower[@rank=$myrank]/varaible"/>
          <xsl:value-of select="//argument[name=$lvarname]/@position"/>
         </xsl:when>
        </xsl:choose>
       </dependsOn>
      </xsl:when>
     </xsl:choose>
-->
   </dim>
  </xsl:for-each>
 </data>
</xsl:template>

<xsl:template name="deduce-intent">
 <xsl:param name="epName"/>
 <xsl:param name="variable"/>
 <xsl:variable name="uses">
  <!--<xsl:for-each select="//procedure[name=$epName]/code//assign">-->
  <!-- 
   The selection below takes into account all references, 
   including arguments to called subroutines.
  -->
  <xsl:for-each select="//procedure[name=$epName]/code//variable[@name=$variable]">
   <xsl:variable name="intent" select="ancestor::procedure/symbol[name=$variable]/intent"/>
   <xsl:choose>
    <xsl:when test="$intent='IN'">
     <xsl:value-of select="'r:'"/>
    </xsl:when>
    <xsl:when test="$intent='OUT'">
     <xsl:value-of select="'w:'"/>
    </xsl:when>
    <xsl:when test="$intent='INOUT'">
     <xsl:value-of select="'r:w:'"/>
    </xsl:when>
    <xsl:when test="name(../)='lhs'">
     <xsl:choose>
      <xsl:when test="../../rhs//variable[@name=$variable]">
       <xsl:value-of select="'r:w:'"/>
      </xsl:when>
      <xsl:otherwise>
       <xsl:value-of select="'w:'"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="name(../)='argument'">
     <xsl:value-of select="'?:'"/><!--? denotes that the variable is passed to another subroutine, and we don't know how it is used-->
     <xsl:message terminate="no">
      <xsl:text>Variable </xsl:text>
      <xsl:value-of select="$variable"/>
      <xsl:text> is passed as an argument to subroutine </xsl:text>
      <xsl:value-of select="../../@name"/>
      <xsl:text>. Intent will be set to unknown because its intent has not been declared and it cannot be known how it will be used in the called subroutine.</xsl:text>
     </xsl:message>
    </xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="'r:'"/>
    </xsl:otherwise>
   </xsl:choose>
   <!--
   <xsl:choose>
    <xsl:when test="lhs/variable/@name=$variable">
     <xsl:value-of select="'w:'"/>
    </xsl:when>
    <xsl:when test="rhs//variable/@name=$variable">
     <xsl:value-of select="'r:'"/>
    </xsl:when>
   </xsl:choose>
   -->
  </xsl:for-each>
 </xsl:variable>
 <!--<xsl:value-of select="$uses"/>-->
 <xsl:choose>
  <xsl:when test="not(boolean($uses))">
   <xsl:value-of select="'unused'"/>
  </xsl:when>
  <xsl:when test="substring-before($uses,':')='w'">
   <xsl:value-of select="'out'"/>
  </xsl:when>
  <xsl:when test="not(contains($uses,'w') or contains($uses,'?'))">
   <xsl:value-of select="'in'"/>
  </xsl:when>
  <xsl:when test="contains($uses,'r') and contains($uses,'w')">
   <xsl:value-of select="'inout'"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="'?'"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="print-expr">
 <xsl:param name="expr"/>
 <xsl:for-each select="$expr">
  <xsl:choose>
   <xsl:when test="local-name(.)='variable'">
    <xsl:variable name="varname" select="@name"/>
    <argument id="{//argument[name=$varname]/@position}"/>
   </xsl:when>
   <xsl:otherwise>
    <xsl:copy>
     <xsl:if test="./text()">
      <xsl:value-of select="./text()"/>
     </xsl:if>
     <xsl:if test="boolean($expr/*)">
      <xsl:call-template name="print-expr">
       <xsl:with-param name="expr" select="$expr/*"/>
      </xsl:call-template>
     </xsl:if>
    </xsl:copy>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
