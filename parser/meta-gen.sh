#!/bin/sh
#E.g. type './meta-gen.sh src/initialise_fixedicesheet.f90'

#Compile the source code in the way that would be done normally, including defining preprocessor macros.
gfortran -x f95-cpp-input -fdump-parse-tree -c -Dutreal4 -Dreal4 $1

#Setting the flag -fdump-parse-tree produces output_file.xml. This is an XML version 
#of the parse tree. In the step below, this document is transformed into BFG XML metadata using XSLT.
if [ -e output_file.xml ]; then
echo -n 'Name your destination metadata file '
read OUT
java org.apache.xalan.xslt.Process -IN output_file.xml -XSL bfg2-metadata-generator.xsl -OUT $OUT
else
echo 'Failed to generate XML representation of source code'
fi

#The existing files output_file-ref.xml and ini_fixice.xml are what I get after running this script.
