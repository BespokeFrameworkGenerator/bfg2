import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.io.File;
import java.io.FileReader;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;
//import javax.jnlp.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

public class XMLServices {

    //	private static final String JAXP_SCHEMA_SOURCE
    //	    = "http://java.sun.com/xml/jaxp/properties/schemaSource";
	private static final String JAXP_SCHEMA_LANGUAGE
	    = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String W3C_XML_SCHEMA
	    = "http://www.w3.org/2001/XMLSchema";
    JTextArea log;
    DocumentBuilderFactory factory;
    DocumentBuilder builderNoValidate,builderValidate;
    Document coupledDoc,composeDoc,deployDoc;
    File file;
    File outputDirectory=null;
    int BFGVersion=0;
    boolean keepFiles=false;
    ClassLoader classLoader;

    //    public XMLServices(JTextArea log, FileContents fileContents){
    public XMLServices(JTextArea log, File file){
	this.log=log;
	//	this.fileContents=fileContents;
	this.file=file;
	//Obtain the current classloader
	classLoader = this.getClass().getClassLoader();
	factory = DocumentBuilderFactory.newInstance();
	factory.setNamespaceAware(false);
	factory.setValidating(false);
	factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA); // use LANGUAGE here instead of SOURCE

	//	docBuildFactory.setAttribute( JAXP_SCHEMA_SOURCE, schemaSource);
	try {
	    builderNoValidate = factory.newDocumentBuilder();
	    PrintAllErrorHandler mleh = new PrintAllErrorHandler(log);
	    builderNoValidate.setErrorHandler(mleh);
	    //	    builderNoValidate.setFeature ("http://xml.org/sax/features/validation/schema", true);
	}
	catch (Exception e){
	    log.append("[ERROR]: in document builder factory\n");
	    log.setCaretPosition(log.getDocument().getLength());
	}
    }

    public int getBFGVersion() {
	return this.BFGVersion;
    }


    public void setOutputDirectory(File outputDirectory) {
	this.outputDirectory=outputDirectory;
    }

    public void setKeepFiles(boolean keepFiles) {
	this.keepFiles=keepFiles;
    }

    public void RunBFG2() {
	String myOutDir;
	try {
	    if (outputDirectory==null) {
		myOutDir=file.getParent();
	    }
            else {
		myOutDir=outputDirectory.getAbsolutePath();
	    }
	    log.append("BFG2: autogentastic\n");
	    log.append("Generating Control Code ");
	    //Load the company logo image
	    //Stream myStream = classLoader.getResource("xslt/EPTemplate.xml").getStream();
	    //java.net.URL myURL = classLoader.getResource("xslt/EPTemplate.xml");
	    //InputStream myStream = classLoader.getResourceAsStream("xslt/EPTemplate.xml");
	    //String myFile = myURL.toExternalForm();
	    //log.append("\n******* FOUND FILE "+myFile+"\n");
	    //	    File myTmpFile = new File("xslt/EPTemplate.xml");
	    //File myTmpFile = new File(myFile);
	    //if (!myTmpFile.canRead()) log.append("DEBUG:AHAH, we can not find our files using relative pathnames\n");

	    //Source source = new StreamSource(myTmpFile);
	    BufferedReader br2 = null;
	    try {
		br2 = new BufferedReader
		    (new InputStreamReader(classLoader.getResourceAsStream("xslt/EPTemplate.xml")));
 } catch (Exception fnf) { log.append("Error: XMLServices: xslt/EPTemplate.xml not found");}


	    BufferedReader br3 = null;
	    try {
		br3 = new BufferedReader
		    (new InputStreamReader(classLoader.getResourceAsStream("xslt/MatchAll.xsl")));
 } catch (Exception fnf) { log.append("Error: XMLServices: xslt/MatchAll.xsl not found");}
	    if (br3==null) log.append("Error: XMLServices: xslt/MatchAll.xsl br3 is null");

	    //Source source = new StreamSource(myFile);
	    // commented out below is when we are trying to use webstart
	    //Source source = new StreamSource(br2);
	    Source source = new StreamSource(new File("xslt/EPTemplate.xml"));
	    Result result = new StreamResult(myOutDir+File.separator+"Maintmp1.xml");
	    TransformerFactory transFact = TransformerFactory.newInstance( );
	    transFact.setURIResolver(new MyResolver(file.getParent(),classLoader, log));
	    
	    java.net.URL myURL2 = classLoader.getResource("xslt/MainPhase0.xsl");
	    //InputStream myStream2 =classLoader.getResourceAsStream("xslt/MainPhase0.xml");
	    //Source xslSource = new StreamSource(new File("xslt/MainPhase0.xsl"));
	    // commented out below to try to get working in Java only (without webstart
	    //BufferedReader br = null;
	    //try {
	    //br = new BufferedReader
	    //    (new InputStreamReader(classLoader.getResourceAsStream("xslt/MainPhase0.xsl")));
	    // } catch (Exception fnf) { log.append("Error: XMLServices: xslt/MainPhase0.xsl not found");}

	    //Source xslSource = new StreamSource(br);
	    Source xslSource = new StreamSource(new File("xslt/MainPhase0.xsl"));
	    log.append(" creating transformer\n");
	    Transformer trans = transFact.newTransformer(xslSource);
	    log.append(" created transformer\n");
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [0]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"Maintmp1.xml");;
	    result = new StreamResult(myOutDir+File.separator+"Maintmp2.xml");
	    xslSource = new StreamSource(new File("xslt/MainPhase1.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [1]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"Maintmp2.xml");;
	    result = new StreamResult(myOutDir+File.separator+"Maintmp3.xml");
	    xslSource = new StreamSource(new File("xslt/MainPhase2.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [2]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"Maintmp3.xml");;
	    result = new StreamResult(myOutDir+File.separator+"Maintmp4.xml");
	    xslSource = new StreamSource(new File("xslt/MainPhase3.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [3]");
	    trans.transform(source, result);

	    Node target;
	    //String path=myOutDir;
	    Document mainTmp4Doc=builderNoValidate.parse(new File(myOutDir+File.separator+"Maintmp4.xml"));
	    Node codeHome = followToElement(mainTmp4Doc, "framework");
	    ArrayList codeList = followToElements(codeHome, "code");
	    source= new StreamSource(myOutDir+File.separator+"Maintmp4.xml");;
	    xslSource = new StreamSource(new File("xslt/CodeGen.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    trans.setParameter("OutDir",myOutDir);
	    for (int i = 0; i < codeList.size(); i++) {
		Node codeID = followToAttribute((Node) codeList.get(i), "id");
		String CurrentCodeID = getData(codeID);

		result = new StreamResult(myOutDir+File.separator+"BFG2Main"+CurrentCodeID+".f90");
		//trans.clearParameters();
		trans.setParameter("CodeID",CurrentCodeID);
		//trans.setParameter("CoupledDocument",file.getAbsolutePath());
		//trans.setParameter("OutDir",myOutDir);
		log.append(" [gen,\"BFG2Main"+CurrentCodeID+".f90\"]");
		trans.transform(source, result);
	    }

	    log.append("\n");

	    log.append("Generating f90 Control File ");
	    source= new StreamSource(file.getAbsolutePath());
	    result = new StreamResult(myOutDir+File.separator+"BFG2Control.nam");
	    xslSource = new StreamSource(new File("xslt/f90ControlFile.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [gen,\"BFG2Control.nam\"]\n");
	    trans.transform(source, result);

	    log.append("Generating f90 Target Specific Code ");
	    source= new StreamSource(new File("xslt/EPTemplate.xml"));
	    result = new StreamResult(myOutDir+File.separator+"Targettmp1.xml");
	    xslSource = new StreamSource(new File("xslt/TargetPhase0.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [0]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"Targettmp1.xml");
	    result = new StreamResult(myOutDir+File.separator+"Targettmp2.xml");
	    xslSource = new StreamSource(new File("xslt/TargetPhase1.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [1]");
	    trans.transform(source, result);
	    
	    source= new StreamSource(myOutDir+File.separator+"Targettmp2.xml");
	    result = new StreamResult(myOutDir+File.separator+"Targettmp3.xml");
	    xslSource = new StreamSource(new File("xslt/TargetPhase2.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    log.append(" [2]");
	    trans.transform(source, result);

	    Document targetTmp3Doc=builderNoValidate.parse(new File(myOutDir+File.separator+"Targettmp3.xml"));
	    codeHome = followToElement(targetTmp3Doc, "framework");
	    codeList = followToElements(codeHome, "code");
	    source= new StreamSource(myOutDir+File.separator+"Targettmp3.xml");;
	    xslSource = new StreamSource(new File("xslt/CodeGen.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    for (int i = 0; i < codeList.size(); i++) {
		Node codeID = followToAttribute((Node) codeList.get(i), "id");
		String CurrentCodeID = getData(codeID);

		result = new StreamResult(myOutDir+File.separator+"BFG2Target"+CurrentCodeID+".f90");
		trans.setParameter("CodeID",CurrentCodeID);
		log.append(" [gen,\"BFG2Target"+CurrentCodeID+".f90\"]");
		trans.transform(source, result);
	    }

	    log.append("\n");
	    
	    log.append("Generating f90 InPlace Code");
	    source= new StreamSource(new File("xslt/EPTemplate.xml"));
	    result = new StreamResult(myOutDir+File.separator+"InPlacetmp1.xml");
	    xslSource = new StreamSource(new File("xslt/InPlacePhase1.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    trans.setParameter("EPName","BFG2InPlace");
	    log.append(" [1]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"InPlacetmp1.xml");
	    result = new StreamResult(myOutDir+File.separator+"InPlacetmp2.xml");
	    xslSource = new StreamSource(new File("xslt/InPlacePhase2.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    trans.setParameter("inRoutine","get");
	    trans.setParameter("outRoutine","put");
	    log.append(" [2]");
	    trans.transform(source, result);
	    
	    source= new StreamSource(myOutDir+File.separator+"InPlacetmp2.xml");
	    result = new StreamResult(myOutDir+File.separator+"InPlacetmp3.xml");
	    xslSource = new StreamSource(new File("xslt/InPlacePhase3.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    trans.setParameter("inRoutine","get");
	    trans.setParameter("outRoutine","put");
	    log.append(" [3]");
	    trans.transform(source, result);

	    source= new StreamSource(myOutDir+File.separator+"InPlacetmp3.xml");
	    result = new StreamResult(myOutDir+File.separator+"InPlacetmp4.xml");
	    xslSource = new StreamSource(new File("xslt/InPlacePhase4.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    trans.setParameter("inRoutine","get");
	    trans.setParameter("outRoutine","put");
	    log.append(" [4]");
	    trans.transform(source, result);

	    Document inPlaceTmp4Doc=builderNoValidate.parse(new File(myOutDir+File.separator+"InPlacetmp4.xml"));
	    codeHome = followToElement(inPlaceTmp4Doc, "framework");
	    codeList = followToElements(codeHome, "code");
	    source= new StreamSource(myOutDir+File.separator+"InPlacetmp4.xml");;
	    xslSource = new StreamSource(new File("xslt/CodeGen.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    trans.setParameter("CoupledDocument",file.getAbsolutePath());
	    for (int i = 0; i < codeList.size(); i++) {
		Node codeID = followToAttribute((Node) codeList.get(i), "id");
		String CurrentCodeID = getData(codeID);

		result = new StreamResult(myOutDir+File.separator+"BFG2InPlace"+CurrentCodeID+".f90");
		trans.setParameter("CodeID",CurrentCodeID);
		log.append(" [gen,\"BFG2InPlace"+CurrentCodeID+".f90\"]");
		trans.transform(source, result);
	    }

	    log.append("\n");
	    if (!keepFiles) {
		log.append("Removing temporary files\n");
		boolean success = (new File(myOutDir+File.separator+"Maintmp1.xml")).delete();
		success = (new File(myOutDir+File.separator+"Maintmp2.xml")).delete();
		success = (new File(myOutDir+File.separator+"Maintmp3.xml")).delete();
		success = (new File(myOutDir+File.separator+"Maintmp4.xml")).delete();
		success = (new File(myOutDir+File.separator+"Targettmp1.xml")).delete();
		success = (new File(myOutDir+File.separator+"Targettmp2.xml")).delete();
		success = (new File(myOutDir+File.separator+"Targettmp3.xml")).delete();
		success = (new File(myOutDir+File.separator+"InPlacetmp1.xml")).delete();
		success = (new File(myOutDir+File.separator+"InPlacetmp2.xml")).delete();
		success = (new File(myOutDir+File.separator+"InPlacetmp3.xml")).delete();
		success = (new File(myOutDir+File.separator+"InPlacetmp4.xml")).delete();
	    }
	    else {
		log.append("Temporary files have been kept\n");
	    }
	    log.append("BFG2 has left the building\n");
	    
       }

	catch(TransformerConfigurationException e) {
	    log.append("TransformerConfigurationException error in RunBFG2 code generation\n");
	    log.append("Error is "+e.getMessageAndLocation()+"\n");
	    e.printStackTrace();
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch(TransformerException e) {
	    log.append("TransformerException error in RunBFG2 code generation\n");
	    log.append("Error is "+e.getMessageAndLocation()+"\n");
	    e.printStackTrace();
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch (SAXException sxe) {
	    // Error generated during parsing
	    Exception x = sxe;
	    if (sxe.getException () != null)
		x = sxe.getException ();
	    log.append("[ERROR] "+x.toString()+"\n");
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch (IOException ioe) {
	    // I/O error 
	    // ioe.printStackTrace ();
	    log.append("[ERROR] file not found\n");
	    log.setCaretPosition(log.getDocument().getLength());
	}
    }

    public void BFG1toBFG2() {
	try {
	    // Coupled document
	    Source source = new DOMSource(coupledDoc.getDocumentElement());
	    Result result = new StreamResult(file.getParent()+File.separator+"bfg2coupled.xml");
	    TransformerFactory transFact = TransformerFactory.newInstance( );
	    transFact.setURIResolver(new MyResolver(file.getParent(),classLoader,log));
	    Source xslSource = new StreamSource(new File("xslt/bfg1tobfg2/Coupled.xsl"));
	    log.append("DEBUG:Creating transformer\n");
	    Transformer trans = transFact.newTransformer(xslSource);
	    log.append("Writing BFG2 Coupled document\n");
	    trans.transform(source, result);

	    // Composition document
	    result = new StreamResult(file.getParent()+File.separator+"bfg2composition.xml");
	    xslSource = new StreamSource(new File("xslt/bfg1tobfg2/Composition.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    log.append("Writing BFG2 Composition document\n");
	    trans.transform(source, result);

	    // Deployment document
	    result = new StreamResult(file.getParent()+File.separator+"bfg2deployment.xml");
	    xslSource = new StreamSource(new File("xslt/bfg1tobfg2/Deployment.xsl"));
	    trans = transFact.newTransformer(xslSource);
	    log.append("Writing BFG2 Deployment document\n");
	    trans.transform(source, result);

	    // Model documents
	    Node target;
	    String path=file.getParent();
	    Node coupledHome = followToElement(coupledDoc, "coupled");
	    ArrayList componentList = followToElements(coupledHome, "component");
	    for (int i = 0; i < componentList.size(); i++) {
		Node modelHome = followToElement((Node) componentList.get(i), "model");
		target = followToElement(modelHome, "source");
		String xmlModelSourceFile = getData(target);
		Document sourceDoc=builderNoValidate.parse(new File(path + File.separator + xmlModelSourceFile));
		target = followToElement(sourceDoc, "source");
		target = followToElement(target, "name");
		String CurrentModelName = getData(target);

		result = new StreamResult(file.getParent()+File.separator+"bfg2model"+CurrentModelName+".xml");
		xslSource = new StreamSource(new File("xslt/bfg1tobfg2/Models.xsl"));
		trans = transFact.newTransformer(xslSource);
		trans.setParameter("ModelName",CurrentModelName);
		log.append("Writing BFG2 Definition document for model "+CurrentModelName+"\n");
		trans.transform(source, result);
	    }

	    // now change from pointing to bfg1 coupled file and point to newly generated bfg2 coupled file
            file=new File(file.getParent()+File.separator+"bfg2coupled.xml");

	}
	catch(TransformerConfigurationException e) {
	    log.append("TransformerConfigurationException error in BFG12BFG2 coupled document\n");
	    log.append("Error is "+e.getMessageAndLocation()+"\n");
	    e.printStackTrace();
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch(TransformerException e) {
	    log.append("TransformerException error in BFG12BFG2 coupled document conversion\n");
	    log.append("Error is "+e.getMessageAndLocation()+"\n");
	    e.printStackTrace();
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch (SAXException sxe) {
	    // Error generated during parsing
	    Exception x = sxe;
	    if (sxe.getException () != null)
		x = sxe.getException ();
	    log.append("[ERROR] "+x.toString()+"\n");
	    log.setCaretPosition(log.getDocument().getLength());
	}
	catch (IOException ioe) {
	    // I/O error 
	    // ioe.printStackTrace ();
	    log.append("[ERROR] file not found\n");
	    log.setCaretPosition(log.getDocument().getLength());
	}
    }

    public boolean fileOK() {
	boolean ok;
	try {
	    //	    document = builderNoValidate.parse(fileContents.getInputStream());
	    coupledDoc = builderNoValidate.parse(file);
	    Node coupledHome = followToElement(coupledDoc, "coupled");
	    if (coupledHome!=null) {
		Node BFG1test = followToElement(coupledHome, "component");
		Node BFG2test = followToElement(coupledHome, "models");
		if (BFG1test!=null){
		    BFGVersion=1;
		    log.append("This appears to be a BFG1 coupled document\n");
		    log.setCaretPosition(log.getDocument().getLength());
		    ok=true;
		}
		else if (BFG2test!=null){
		    BFGVersion=2;
		    log.append("This appears to be a BFG2 coupled document\n");
		    log.setCaretPosition(log.getDocument().getLength());
		    ok=true;
		}
		else{
		    log.append("[ERROR]: file has <coupled/> as its root but appears to be malformed\n");
		    log.setCaretPosition(log.getDocument().getLength());
		    ok=false;
		}
	    }
	    else {
		// not a valid BFG1 or BFG2 document
		log.append("[ERROR]: file is not a valid BFG1 or BFG2 coupled document\n");
		log.setCaretPosition(log.getDocument().getLength());
		ok=false;
	    }
	}
	catch(Exception e){
	    log.append("[ERROR]: XMLServices.fileOK document parse\n");
	    log.setCaretPosition(log.getDocument().getLength());
	    ok=false;
	}
	return ok;
    }

    public boolean validate() {
	try {
	    factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    factory.setValidating(true);
	    factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA); // use LANGUAGE here instead of SOURCE
	    builderValidate = factory.newDocumentBuilder();
	    PrintAllErrorHandler mleh = new PrintAllErrorHandler(log);
	    builderValidate.setErrorHandler(mleh);
	    //	    String xmlFile = fileContents.getName();
	    String xmlFile = file.getName();
	    String path=file.getParent();
	    log.append("Coupled document location is "+path+"\n");
	    log.append("Validating coupled document "+xmlFile+"\n");
	    log.setCaretPosition(log.getDocument().getLength());
	    coupledDoc=builderValidate.parse (file);
	    log.append("Document "+xmlFile+" is valid\n");
	    if (BFGVersion==1){
		// now pick up the XML docs from the 'coupled.xml'
		Node coupledHome = followToElement(coupledDoc, "coupled");
		Node target;

		//Compose
		target = followToElement(coupledHome, "compose");
		String xmlComposeFile = getData(target);
		log.append("Validating compose document " + xmlComposeFile+"\n");
		composeDoc = builderValidate.parse(new File(path + File.separator + xmlComposeFile));
		log.append("Document "+xmlComposeFile+" is valid\n");

		//Deploy
		target = followToElement(coupledHome, "deploy");
		String xmlDeployFile = getData(target);
		log.append("Validating deploy document " + xmlDeployFile+"\n");
		deployDoc = builderValidate.parse(new File(path + File.separator + xmlDeployFile));
		log.append("Document "+xmlDeployFile+" is valid\n");

		ArrayList componentList = followToElements(coupledHome, "component");

		for (int i = 0; i < componentList.size(); i++) {

		    target = followToElement((Node) componentList.get(i), "interface");
		    String xmlInterfaceFile = getData(target);
		    log.append("Validating interface document " + xmlInterfaceFile+"\n");
		    builderValidate.parse(new File(path + File.separator + xmlInterfaceFile));
		    log.append("Document " + xmlInterfaceFile+" is valid\n");

		    Node modelHome = followToElement((Node) componentList.get(i), "model");

		    target = followToElement(modelHome, "source");
		    String xmlModelSourceFile = getData(target);
		    log.append("Validating source document " + xmlModelSourceFile+"\n");
		    builderValidate.parse(new File(path + File.separator + xmlModelSourceFile));
		    log.append("Document " + xmlModelSourceFile+" is valid\n");
		    
		    target = followToElement(modelHome, "executable");
		    String xmlModelExecutableFile = getData(target);
		    log.append("Validating executable document " +  xmlModelExecutableFile+"\n");
		    builderValidate.parse(new File(path + File.separator + xmlModelExecutableFile));
		    log.append("Document " + xmlModelExecutableFile+" is valid\n");
		}
	    }
	    else if (BFGVersion==2){
		// now pick up the XML docs from the 'coupled.xml'
		Node coupledHome = followToElement(coupledDoc, "coupled");
		Node target;

		//Compose
		target = followToElement(coupledHome, "composition");
		String xmlComposeFile = getData(target);
		log.append("Validating composition document " + xmlComposeFile+"\n");
		Document composeDoc = builderValidate.parse(new File(path + File.separator + xmlComposeFile));
		log.append("Document "+xmlComposeFile+" is valid\n");

		//Deploy
		target = followToElement(coupledHome, "deployment");
		String xmlDeployFile = getData(target);
		log.append("Validating deployment document " + xmlDeployFile+"\n");
		Document deployDoc = builderValidate.parse(new File(path + File.separator + xmlDeployFile));
		log.append("Document "+xmlDeployFile+" is valid\n");

		//Definitions
		target = followToElement(coupledHome, "models");
		ArrayList modelList = followToElements(target, "model");

		for (int i = 0; i < modelList.size(); i++) {

		    String xmlModelFile = getData((Node) modelList.get(i));
		    log.append("Validating model document " + xmlModelFile+"\n");
		    builderValidate.parse(new File(path + File.separator + xmlModelFile));
		    log.append("Document " + xmlModelFile+" is valid\n");

		}
	    }
	    else{
		log.append("[INTERNAL ERROR]: BFGVersion ("+BFGVersion+") is invalid. Valid values are [1,2]\n");
		log.setCaretPosition(log.getDocument().getLength());
	    }
	    log.setCaretPosition(log.getDocument().getLength());
	    return true;
	}
	catch(IllegalArgumentException x) {
	    // Happens if the parser does not support JAXP 1.2
	    log.append("[ERROR] JAXP version 1.2 required in order to validate documents against schema.\n");
	    log.setCaretPosition(log.getDocument().getLength());
	    return false;
	}
	catch (SAXException sxe) {
	    // Error generated during parsing
	    Exception x = sxe;
	    if (sxe.getException () != null)
		x = sxe.getException ();
	    //log.append("[ERROR] "+x.printStackTrace()+"\n");
	    //log.setCaretPosition(log.getDocument().getLength());
	    return false;
	}
	catch (ParserConfigurationException pce) {
	    // Parser with specified options can't be built
	    //log.append("[ERROR] "+pce.printStackTrace()+"\n");
	    //log.setCaretPosition(log.getDocument().getLength());
	    return false;
	}
	catch (IOException ioe) {
	    // I/O error 
	    // ioe.printStackTrace ();
	    log.append("[ERROR] file not found\n");
	    log.setCaretPosition(log.getDocument().getLength());
	    return false;
	}
	catch (Exception e) {
	    return false;
	}
    }

    private Node followToElement(Node start, String tag) {
	if (start == null) {
	    log.append("[INTERNAL ERROR]: followToElement: node is null\n");
	}
	for (Node child = start.getFirstChild(); child != null; child = child.getNextSibling()) {
	    if (child.getNodeName().equals(tag)) {
		return child;
	    }
	}
	return null;
    }

    private Node followToAttribute(Node start, String tag) {
	if (start == null) {
	    log.append("[INTERNAL ERROR]: followToAttribute: node is null\n");
	}
	NamedNodeMap myAttributes=start.getAttributes();
	Node attr = myAttributes.getNamedItem(tag);
	return attr;
    }

    private String getData(Node start) {
	// if any errors return 'null'

	if (start == null) {
	    log.append("[INTERNAL ERROR]: null Node passed to getData(); returning 'null'\n");
	    return null;
	}
	else {
	    Node first = start.getFirstChild();
	    if (first == null) {
		log.append("[INTERNAL ERROR]: first is 'null'; returning 'null'");
		return null;
	    }
	    else {
		String answer = first.getNodeValue();
		return answer;
	    }
	}

	//return start.getFirstChild().getNodeValue();
  }

  private ArrayList followToElements(Node start, String tag)
  {
    ArrayList<Node> elements = new ArrayList<Node>();
    for (Node child = start.getFirstChild(); child != null; child = child.getNextSibling()) {
	//            System.out.println("followtoelements name "+child.getNodeName());
	if (child.getNodeName().equals(tag)) {
	    elements.add(child);
	}
    }
    return elements;
  }

}
