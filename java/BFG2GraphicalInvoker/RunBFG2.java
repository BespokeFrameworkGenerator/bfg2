import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.io.File;
import java.io.FileReader;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;
//import javax.jnlp.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/*
class RunBFG2Worker {
    public RunBFG2Worker() {
	// My worker thread runs this
	System.out.println("Hello from the worker thread\n");
    }
}
*/

public class RunBFG2 implements ActionListener {

    JTextArea log;
    JPanel buttonPanel;
    JButton loadButton;
    JButton outdirButton;
    JButton validateButton;
    JButton runButton;
    //    FileContents fileContents = null;
    JFileChooser fc,dc;
    File outputDirectory=null;
    JFrame frame;
    JCheckBox keepTmpFiles;
    boolean keepFiles;

    XMLServices xmlServices;
    //final SwingWorker worker;

    public static void main(String args[]) throws Exception {
	RunBFG2 runme = new RunBFG2();
	/*
	final SwingWorker worker =  new SwingWorker() {
		public Object construct() {
		    return new
			RunBFG2Worker();
		}
	    };
	*/
	runme.myWindows();
    }

    public RunBFG2() {
	    fc = new JFileChooser();
	    dc = new JFileChooser();
	    dc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public void myWindows() {

    JFrame.setDefaultLookAndFeelDecorated(true);
    frame =  new JFrame("BFG2 Graphical Invoker"); 
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setIconImage(new ImageIcon("images/bfgtitle.gif").getImage());

    log = new JTextArea(20,60);
    log.setMargin(new Insets(5,5,5,5));
    log.setEditable(false);
    JScrollPane logScrollPane = new JScrollPane(log);
    log.append("Welcome to the BFG2 Graphical Invoker 1.0.0.\nAuthor: Rupert Ford.\nReady ...\n");
    log.setCaretPosition(log.getDocument().getLength());

    loadButton = new JButton("Locate File...");
    outdirButton = new JButton("Output Directory...");
    validateButton = new JButton("Validate XML");
    runButton = new JButton("Run BFG2");
    keepTmpFiles = new JCheckBox("Keep Temporary files");
    keepFiles = false;    

    loadButton.addActionListener(this);
    outdirButton.addActionListener(this);
    validateButton.addActionListener(this);
    runButton.addActionListener(this);
    //keepTmpFiles.addItemListener(this);
    keepTmpFiles.addActionListener(this);

    validateButton.setEnabled(false);
    runButton.setEnabled(false);
    keepTmpFiles.setEnabled(false);

    buttonPanel = new JPanel();
    buttonPanel.add(loadButton);
    buttonPanel.add(outdirButton);
    buttonPanel.add(validateButton);
    buttonPanel.add(runButton);
    buttonPanel.add(keepTmpFiles);

    Container content = frame.getContentPane();
    content.add(logScrollPane, BorderLayout.CENTER);
    content.add(buttonPanel, BorderLayout.PAGE_START);

    frame.pack();
    frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == loadButton) {
	    log.append("[DEBUG] locate file button was pressed\n");
	    log.setCaretPosition(log.getDocument().getLength());

            int returnVal = fc.showOpenDialog(buttonPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                log.append("Opening: " + file.getName() + ".\n");
		xmlServices = new XMLServices(log,file);
		if (xmlServices.fileOK()) {
		    validateButton.setEnabled(true);
		    runButton.setEnabled(false);
		    keepTmpFiles.setEnabled(false);
		}
		else {
		    log.append("ERROR: Unsupported file type.\n");
		    validateButton.setEnabled(false);
		    runButton.setEnabled(false);
		    keepTmpFiles.setEnabled(false);
		}
            }
	    else {
                log.append("Locate File cancelled by user.\n");
            }
            log.setCaretPosition(log.getDocument().getLength());


//             FileOpenService fos = null;

//             try {
//                 fos = (FileOpenService)ServiceManager.lookup("javax.jnlp.FileOpenService"); 
//             } catch (UnavailableServiceException exc) { fos=null;
// 	    log.append("File open Service request failed: "
//                                + exc.getLocalizedMessage()
//                                + "\n");
//                     log.setCaretPosition(log.getDocument().getLength());
// 	    }

//             if (fos != null) {
//                 try {
//                     fileContents = fos.openFileDialog(null, null); 
//                 } catch (Exception exc) {
//                     log.append("File open command failed: "
//                                + exc.getLocalizedMessage()
//                                + "\n");
//                     log.setCaretPosition(log.getDocument().getLength());
//                 }
//             }

//             if (fileContents != null) {
//                 try {
//                     log.append("Located file: " + fileContents.getName()
//                                + ".\n");
// 		    xmlServices = new XMLServices(log,fileContents);
// 		    if (xmlServices.fileOK()) {
// 			validateButton.setEnabled(true);
// 		    }
// 		    else {
// 			log.append("ERROR Unknown file type.\n");
// 			validateButton.setEnabled(false);
// 			runButton.setEnabled(false);
// 		    }
//                 } catch (IOException exc) {
//                     log.append("Problem opening file: "
//                                + exc.getLocalizedMessage()
//                                + "\n");
// 		    validateButton.setEnabled(false);
// 		    runButton.setEnabled(false);
//                 }
//             } else {
//                 log.append("User canceled open request.\n");
//             }
//             log.setCaretPosition(log.getDocument().getLength());

	}
	if (e.getSource() == outdirButton) {
	    log.append("[DEBUG] output directory button was pressed\n");
            int returnVal = dc.showOpenDialog(buttonPanel);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                outputDirectory = dc.getSelectedFile();
                log.append("Selected Output Directory is " + outputDirectory.getAbsolutePath() + ".\n");
		xmlServices.setOutputDirectory(outputDirectory);
            }
	    else {
                log.append("Select Output Directory cancelled.\n");
            }
            log.setCaretPosition(log.getDocument().getLength());

	}
	if (e.getSource() == validateButton) {
	    log.append("[DEBUG] validate xml button was pressed\n");
	    boolean ok = xmlServices.validate();
	    if (ok) {
		log.append("Documents are valid\n");
		log.setCaretPosition(log.getDocument().getLength());
		runButton.setEnabled(true);
		keepTmpFiles.setEnabled(true);
	    }
	    else {
		log.append("At least one document is not valid\n");
		log.setCaretPosition(log.getDocument().getLength());
		runButton.setEnabled(false);
		keepTmpFiles.setEnabled(false);
	    }
	}
	if (e.getSource() == runButton) {
	    log.append("run BFG2 button was pressed\n");
	    int n=0;
	    if (outputDirectory==null) {
		log.append("WARNING: output directory is not set, output will be placed in the same\n");
                log.append("         directory as the top level coupled document\n");
		//JOptionPane.showMessageDialog(frame, "output directory is not set, output will be placed in the same directory as the top level coupled document");
		Object[] options = {"That's fine",
                    "Oops"};
		n = JOptionPane.showOptionDialog(frame,
						     "output directory is not set, output will be placed in the same directory as the top level coupled document",
						     "Output directory information",
						     JOptionPane.OK_CANCEL_OPTION,
						     JOptionPane.INFORMATION_MESSAGE,
						     null,
						     options,
						     options[0]);
	    }
	    if (outputDirectory==null && n!=JOptionPane.OK_OPTION) {
		log.append("Run BFG2 cancelled\n");
	    }
	    else {
		if (xmlServices.getBFGVersion()==1) {
		    log.append("Converting BFG1 document to BFG2 format\n");
		    xmlServices.BFG1toBFG2();
		}
		xmlServices.setKeepFiles(keepFiles);
		xmlServices.RunBFG2();
		log.setCaretPosition(log.getDocument().getLength());
	    }
	}
	if (e.getSource() == keepTmpFiles) {
	    log.append("[DEBUG] keep output check box was pressed\n");
	    if (keepFiles) keepFiles=false; else keepFiles=true;
	}	    
    }
}


