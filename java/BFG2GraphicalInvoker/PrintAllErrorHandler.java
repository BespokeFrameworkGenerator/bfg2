import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.io.File;
import java.io.FileReader;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;

//import javax.jnlp.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class PrintAllErrorHandler implements org.xml.sax.ErrorHandler{

    JTextArea log;

    public PrintAllErrorHandler(JTextArea log){
	this.log=log;
    }

    public void warning(SAXParseException spe){
	log.append("WARNING: "+spe.getSystemId()+"\n");
	log.append("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	log.append("PROBLEM: "+spe.toString()+"\n");
	log.setCaretPosition(log.getDocument().getLength());
    }
	
    public void fatalError(SAXParseException spe){
	log.append("FATAL ERROR: "+spe.getSystemId()+"\n");
	log.append("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	log.append("PROBLEM: "+spe.toString()+"\n");
	log.setCaretPosition(log.getDocument().getLength());
    }

    public void error(SAXParseException spe){
	log.append("NON FATAL ERROR: "+spe.getSystemId()+"\n");
	log.append("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	log.append("PROBLEM :"+spe.toString()+"\n");
	log.setCaretPosition(log.getDocument().getLength());
    }
}
