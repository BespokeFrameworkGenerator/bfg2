import javax.xml.transform.*;
import java.io.*;
import javax.xml.transform.stream.*;
import javax.swing.*;
class MyResolver implements URIResolver {
  String basePath;
  JTextArea log;
  ClassLoader classLoader;
  boolean debug;
  public MyResolver(String basePath, ClassLoader classLoader, JTextArea log) {
      this.basePath=basePath;
      this.log=log;
      this.classLoader = classLoader;
      //this.classLoader = this.getClass().getClassLoader();
      debug=true;
  }

  public Source resolve(String href,String base) {
      if (debug) log.append("DEBUG: my URIResolver called with href="+href.toString()+" and base="+base+"\n");
      if (debug) log.setCaretPosition(log.getDocument().getLength());

      File file = new File(href.toString());
      if(file.exists()) {
	  if (debug) log.append("DEBUG: MyResolver found file in standard location\n");
	  return new StreamSource(file);
      }
      else {
	  if (debug) log.append("DEBUG: MyResolver did not find file in standard location\n");
	  if (debug) log.append("DEBUG: MyResolver looking for file from specified dir ="+basePath+"\n");
	  if (debug) log.append("DEBUG: MyResolver full path="+basePath+File.separator+href.toString()+"\n");
	  file= new File(basePath+File.separator+href.toString());
	  if(file.exists()) {
	      if (debug) log.append("DEBUG: MyResolver found file in specified location\n");
	      return new StreamSource(file);
	  }
	  else {
	      // try to get directly as a resource
	      BufferedReader br = null;
	      try {
		  if (debug) log.append("DEBUG: MyResolver looking for file="+href.toString()+" as a resource\n");
		  br = new BufferedReader
		      (new InputStreamReader(classLoader.getResourceAsStream(href.toString())));
		  if (debug) log.append("DEBUG: MyResolver Found it\n");
		  return new StreamSource(br);
	      } catch (Exception fnf) {
		  try {
		      if (debug) log.append("DEBUG: MyResolver looking for file=xslt/"+href.toString()+" as a resource\n");
		      br = new BufferedReader
			  (new InputStreamReader(classLoader.getResourceAsStream("xslt/"+href.toString())));
		      if (debug) log.append("DEBUG: MyResolver Found it\n");
		      return new StreamSource(br);
		  } catch (Exception fnf2) {
		      log.append("WARNING: MyResolver did not find file in default, xslt or specified location\nprocessor should try to resolve it itself; returning null\n");
		      return null;
		  }
	      }
	  }
      }
  }
}
