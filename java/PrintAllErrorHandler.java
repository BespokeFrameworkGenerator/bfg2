import java.io.*;
import java.net.*;
import java.io.File;
import java.io.FileReader;

//import javax.jnlp.*;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class PrintAllErrorHandler implements org.xml.sax.ErrorHandler{

    public PrintAllErrorHandler(){
    }

    public void warning(SAXParseException spe){
	System.out.println("WARNING: "+spe.getSystemId()+"\n");
	System.out.println("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	System.out.println("PROBLEM: "+spe.toString()+"\n");
	System.exit(1);
    }
	
    public void fatalError(SAXParseException spe){
	System.out.println("FATAL ERROR: "+spe.getSystemId()+"\n");
	System.out.println("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	System.out.println("PROBLEM: "+spe.toString()+"\n");
	System.exit(1);
    }

    public void error(SAXParseException spe){
	System.out.println("NON FATAL ERROR: "+spe.getSystemId()+"\n");
	System.out.println("At line number="+spe.getLineNumber()+", column number="+spe.getColumnNumber()+"\n");
	System.out.println("PROBLEM :"+spe.toString()+"\n");
	System.exit(1);
    }
}
