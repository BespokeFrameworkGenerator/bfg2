import java.io.File;
import java.io.FileReader;

import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;

class Validator {

	private static final String JAXP_SCHEMA_LANGUAGE
	    = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	private static final String W3C_XML_SCHEMA
	    = "http://www.w3.org/2001/XMLSchema";

    public static void main(String args[]) {
	Validator myValidator = new Validator();
	File myFile = new File(args[0]);
	myValidator.validate(myFile);
    }

    public void validate(File file) {
	DocumentBuilderFactory factory;	
	DocumentBuilder builderValidate;
	Document doc;

	try {
	    factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    factory.setValidating(true);
	    factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA); // use LANGUAGE here instead of SOURCE
	    builderValidate = factory.newDocumentBuilder();
	    PrintAllErrorHandler mleh = new PrintAllErrorHandler();
	    builderValidate.setErrorHandler(mleh);
	    //	    String xmlFile = fileContents.getName();
	    String xmlFile = file.getName();
	    String path=file.getParent();
	    System.out.println("Document location is "+path+"\n");
	    System.out.println("Validating coupled document "+xmlFile+"\n");
	    doc=builderValidate.parse(file);
	    System.out.println("Document "+xmlFile+" is valid\n");
	}
        catch(Exception e) {
	    System.out.println("Error :"+e);
	}
    }
}

