import functools
import inspect


class Logical(object):

	"""
	Decorator class for client methods.  Casts specified function parameter to bool type
	irrespective of argument type.  E.g.

	@Logical("c")
	def f(a,b,c,d):
		...
	"""

	def __init__(self, *lparams):
		self.lparams = lparams

	def __call__(self, F):
		@functools.wraps(F)
		def fwrap(*args, **kwargs):
			spec = inspect.getargspec(F)

			paramset = set(self.lparams)
			argset = set(spec.args)

			if not (paramset <= argset):
				diff = paramset - argset
				plural = "s" if len(diff) > 1 else ""
				raise Exception("Unknown parameter{0} '{1}' to decorator {2} when applied to function {3} in module {4}".format(\
						plural, ",".join(sorted(diff)), self.__class__.__name__, F.func_code.co_name, F.__module__ ) )

			modparams = list(args)
			kwcount = len(kwargs)

			if(kwcount > 0):
				for param in self.lparams:
					k = kwargs.get(param)
					if(k != None):
						kwargs[param] = bool(k)

			for param in self.lparams[:len(self.lparams)-kwcount]:
				i = spec.args.index(param)
				modparams[i] = bool(args[i])

			args = tuple(modparams)
			F(*args, **kwargs)


		return fwrap
