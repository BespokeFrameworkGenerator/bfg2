#include "pycall.h"

#include <libgen.h>		// for dirname()
#include <unistd.h>
#include <errno.h>

char *get_current_dir_name(void);

const char arraychar = 'N';
const char cbracket = ']';

const char const *pypath = ".";		// Not used 

static Instance *instanceList = NULL;
static int initialised = 0;


/*
const char const *pypath = "/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/gemini_e3:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/em2conc:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/genie_em:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/transformations/convergence_test:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/transformations/emissions_control:\
.";


const char const *pypath = "models/gemini_e3:\
models/em2conc:\
models/genie_em:\
transformations/convergence_test:\
transformations/emissions_control:\
.";
*/
char initialcwd[MAXPATHLEN];


Instance *hasInstance(const char const *module, const char const *class)
{
	Instance *inst;

	for(inst = instanceList; inst; inst = inst->next){
		if(! (strcmp(inst->module, module) || strcmp(inst->class, class) )){
			return inst;
		}
	}

	return NULL;

}


void addInstance(const char const *module, const char const *class, PyObject *pinst, const char const *moduleDir)
{
	Instance *inst, *listInst;
	inst = (Instance *)malloc(sizeof(Instance));

	strncpy(inst->module, module, MAXSYMLEN);
	strncpy(inst->class, class, MAXSYMLEN);
	inst->localdir = moduleDir;

	inst->pinst = pinst;
	inst->next = NULL;


	if(!instanceList){
		instanceList = inst;
		return;
	}

	listInst = instanceList;

	while(listInst->next)
		listInst = listInst->next;

	listInst->next = inst;

	return;
}


void delInstance(const char const *module, const char const *class)
{
        Instance *inst, *prev=NULL;

        for(inst = instanceList; inst; prev = inst, inst = inst->next){
                if(! (strcmp(inst->module, module) && strcmp(inst->class, class) )){
                        if(inst->next){
				if(prev)
					prev->next = inst->next;
				else
					instanceList = inst->next;
			}else{
				if(prev)
					prev->next = NULL;
				else
					instanceList = NULL;
			}
			Py_DECREF(inst->pinst);
			free(inst);
                }
        }

}

void delList(void)
{
	
	Instance *inst = instanceList, *next=NULL;

	if(!inst)
		return;

	puts("[delList()]");

	while(inst->next){
		next = inst->next;
		free(inst);
		inst = next;
	}
	free(inst);

	instanceList = NULL;
}	


void pycallinit_(void)
{
	pycallInit();
}


void pycallInit(void)
{
	if(!initialised){
		if(!getcwd(initialcwd, MAXPATHLEN))
			perror("getcwd() initialcwd");

		Py_Initialize();
		import_array();
		initialised = 1;
	}
}


void pycalltidy_(void)
{
	pycallTidy();
}

void pycallTidy(void)
{
}


void consume(va_list *ap, char *fmt)
{
	while (*fmt != ':'){
        	switch (*fmt++) {
			case 'i':
                                va_arg(*ap, int);
                                break;
			case 'd':
                                va_arg(*ap, double);
                                break;
			case 'f':
				va_arg(*ap, double);	// va promotes float to double
				break;
			case 'O':
				va_arg(*ap, void *);
				break;
			case 's':
				va_arg(*ap, void *);
				break;
			default:
                        	break;
                }
        }


}


char *rewriteFmt(const char *fmt)
{
	int listStartSize = 32;
	int listReallocSize = 2*listStartSize;
	int listCurMax = listStartSize;
	int listIndex = 0;

	char *fmtList = (char *) malloc(listStartSize * sizeof(char));		// Unfreed
	char *arrayend;
	
	while(*fmt != ':'){
		switch(*fmt++){
			case 'N':	arrayend = strchr(fmt, cbracket);
					fmt = arrayend;
					fmtList[listIndex++] = 'O';
					break;

			case 'i':	fmtList[listIndex++] = 'i';
					break;

			case 'd':	fmtList[listIndex++] = 'd';
					break;

			case 'f':	fmtList[listIndex++] = 'f';
					break;

			case 's':	fmtList[listIndex++] = 's';
					break;
			default:	break;
		}
		if(listIndex == listCurMax-1){
			if(!realloc(fmtList, listReallocSize)){
				perror("rewriteFmt(): realloc failed to allocate listReallocSize bytes.");
				exit(1);
			}
			listReallocSize *= 2;
		}
	}

	fmtList[listIndex] = '\0';

	return fmtList;
}

void vacall(const char const *module, const char const *cls, const char const *method, char *fmt, ...)
{
	PyObject *clientMod=NULL, *clientClass=NULL, *pinst=NULL, *clientDict=NULL;
	PyObject *pargs=NULL, *pfunc=NULL, *ret=NULL, *retTuple=NULL, *nparray=NULL;
	char *moduleFile=NULL, *moduleDir=NULL;
	Instance *instance;

	va_list ap;
	double *da=NULL, *dr=NULL;

	int fmtlen = strlen(fmt);
	int arglen = strcspn(fmt, ":");
	int retlen = fmtlen - arglen;

	char *argfmt = (char *)malloc(arglen+1);
	char *retfmt = (char *)malloc(retlen);

	strncpy(argfmt, fmt, arglen);
	argfmt[arglen] = '\0';
	strncpy(retfmt, fmt+arglen+1, retlen); 

	char *objargs = "(O)";
	char *arraystr=NULL, *arrayend=NULL;
	int arraystrlen;

	arraystr = strchr(fmt, arraychar);

	if(arraystr){
		arrayend = strchr(fmt, cbracket);
		arraystrlen = (int) (arrayend - arraystr);
		arraystrlen++;

		char *arrayfmt = (char *)malloc((arraystrlen+1)*sizeof(char));
		strncpy(arrayfmt, arraystr, arraystrlen);
		arrayfmt[arraystrlen] = '\0';


		va_start(ap, fmt);
		da = (double *) va_arg(ap, double *);
		dr = (double *) va_arg(ap, double *);
		nparray = ndarrayFmt(arrayfmt, da );
		va_end(ap);

		free(arrayfmt);

	}



	if(arraystr){
		pargs = Py_BuildValue(objargs, nparray);
	}else if(strcmp(argfmt, "()")){
		va_start(ap, fmt);
		pargs = Py_VaBuildValue(argfmt, ap);
		va_end(ap);
	}

	instance = hasInstance(module, cls);

	if(!instance){

	        clientMod = PyImport_ImportModule(module);
		moduleFile = PyModule_GetFilename(clientMod);
		moduleDir = dirname(strdup(moduleFile));
	        clientDict = PyModule_GetDict(clientMod);
        	clientClass = PyObject_GetAttrString(clientMod, cls);

                if(strcmp(method, "constructor"))
                        pinst = PyEval_CallObject(clientClass, (PyObject*)NULL);     // No instance, no params, silent default constructor
                else
                        pinst = PyEval_CallObject(clientClass, pargs);       // Explicit constructor, use params

		addInstance(module, cls, pinst, moduleDir);
		instance = hasInstance(module, cls);
	}

	if(strcmp(method, "constructor")){

		if(chdir(instance->localdir))
			perror("module import chdir");

		pfunc = PyObject_GetAttrString(instance->pinst, method);
		ret = PyEval_CallObject(pfunc, pargs);
		revertCwd();
	}


	if(*retfmt){
		va_start(ap, fmt);
		if(strcmp(argfmt, "()"))
			consume(&ap, fmt);

		if(strlen(retfmt) == 1){
			retTuple = Py_BuildValue("(O)", ret);
		}else{
			retTuple = ret;
			Py_INCREF(retTuple);
		}

		va_end(ap);
        }else{
        }


	if(clientMod)
	        Py_DECREF(clientMod);

	if(clientDict)
	        Py_DECREF(clientDict);

	if(pfunc)
	        Py_DECREF(pfunc);

	if(pargs && (pargs != nparray))
	        Py_DECREF(pargs);

	if(ret && (ret != retTuple)){
	        Py_DECREF(ret);
	}

	if(!arraystr && (strlen(retfmt) == 1)){
		Py_DECREF(retTuple);
	}

	Py_XDECREF(nparray);
	free(retfmt);
	free(argfmt);

}



PyObject *ndarrayFmt(char *fmt, ...)
{
	char *str, *savestr, *token;
	char *delim = ",";
	int dims[12];
	int i, dimcount=0, t=0, npy_type=0;
	int fmtlen = strlen(fmt);

	va_list ap;
	double *da=NULL;
	int *ia=NULL;
	void *npa=NULL;

	str = (char *)malloc((fmtlen+1)*sizeof(char));
	savestr = str;
	strncpy(str, fmt+2, fmtlen-3);
	str[fmtlen-3] = '\0';
	puts(str);

	va_start(ap, fmt);

	for(i=1; ; i++, str = NULL){
		token = strtok(str, delim);
		if(token == NULL)
			break;

		t = atoi(token);
		printf("%d: %s (%d)\n", i, token, t);

		if(t){
			dims[dimcount] = t;
			dimcount++;
		}else{
			switch(token[0]){

/* Promoted to double */	case 'f':
				case 'd':  puts("Token d");
					   npy_type = NPY_DOUBLE;
					   da = (double *) va_arg(ap, double *);
					   npa = (void *) da;
					   break;

                                case 'i':  puts("Token i");
                                           npy_type = NPY_INT;
                                           ia = (int *) va_arg(ap, int *);
                                           npa = (void *) ia;
                                           break;

				default:   break;
			}
		}
	}
	va_end(ap);
	free(savestr);

	printf("da: %p\n", da);

	PyObject *nparray;

	nparray = PyArray_New(&PyArray_Type, dimcount, dims, npy_type, NULL, npa, 0, NPY_F_CONTIGUOUS | NPY_WRITEABLE, NULL);
        npy_intp argsize = PyArray_NBYTES(nparray);
        printf("argsize = %d\n", argsize);

	return nparray;	

}


void ndarrayRet(PyObject *npret, double *out)
{
	double *resdata;
	int ndims, i;
	size_t itemsize;

	PyObject *nparray = PyArray_FROM_OTF(npret, NPY_DOUBLE, 0);
	ndims = PyArray_NDIM(nparray);


	npy_intp *resdims, *resstrides;

	resdims = PyArray_DIMS(nparray);
	resstrides = PyArray_STRIDES(nparray);
	itemsize = PyArray_ITEMSIZE(nparray);
	resdata = (double *) PyArray_DATA(nparray);

        printf("resdims = %d %d\n", resdims[0], resdims[1]);
        printf("resstrides = %d %d\n", resstrides[0], resstrides[1]);
        printf("itemsize = %d\n", itemsize);

	int area = 1;
	for(i = 0; i < ndims; i++)
		area *= resdims[i];

	memcpy(out, resdata, area * itemsize);


	return;
}


void revertCwd(void)
{
	if(chdir(initialcwd))
		perror("revertCwd");


}
