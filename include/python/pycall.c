#include "pycall.h"

#include <libgen.h>		// for dirname()
#include <unistd.h>
#include <errno.h>

char *get_current_dir_name(void);

const char arraychar = 'N';
const char cbracket = ']';

const char const *pypath = ".";		// Not used 

static Instance *instanceList = NULL;
static int initialised = 0;


/*
const char const *pypath = "/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/gemini_e3:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/em2conc:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/models/genie_em:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/transformations/convergence_test:\
/local/slavinp/svn/bfg2/examples/ermitage/GeminiE3AndGENIEem/transformations/emissions_control:\
.";


const char const *pypath = "models/gemini_e3:\
models/em2conc:\
models/genie_em:\
transformations/convergence_test:\
transformations/emissions_control:\
.";
*/
char initialcwd[MAXPATHLEN];


Instance *hasInstance(const char const *module, const char const *class)
{
	Instance *inst;

	for(inst = instanceList; inst; inst = inst->next){
		if(! (strcmp(inst->module, module) || strcmp(inst->class, class) )){
			fprintf(stderr, "Found instance: %s::%s\n", inst->module, inst->class);
//			return inst->pinst;
			return inst;
		}
	}

	return NULL;

}


void addInstance(const char const *module, const char const *class, PyObject *pinst, const char const *moduleDir)
{
	Instance *inst, *listInst;
	inst = (Instance *)malloc(sizeof(Instance));

	strncpy(inst->module, module, MAXSYMLEN);
	strncpy(inst->class, class, MAXSYMLEN);
	inst->localdir = moduleDir;

	inst->pinst = pinst;
	inst->next = NULL;

	fprintf(stderr, "Created instance: %s::%s\n", module, class);

	if(!instanceList){
		instanceList = inst;
		return;
	}

	listInst = instanceList;

	while(listInst->next)
		listInst = listInst->next;

	listInst->next = inst;

	return;
}


void delInstance(const char const *module, const char const *class)
{
        Instance *inst, *prev=NULL;

        for(inst = instanceList; inst; prev = inst, inst = inst->next){
                if(! (strcmp(inst->module, module) && strcmp(inst->class, class) )){
                        fprintf(stderr, "Deleting instance: %s::%s\n", module, class);
                        if(inst->next){
				if(prev)
					prev->next = inst->next;
				else
					instanceList = inst->next;
			}else{
				if(prev)
					prev->next = NULL;
				else
					instanceList = NULL;
			}
			Py_DECREF(inst->pinst);
			free(inst);
                }
        }

}

void delList(void)
{
	
	Instance *inst = instanceList, *next=NULL;

	if(!inst)
		return;

	puts("[delList()]");

	while(inst->next){
		next = inst->next;
		fprintf(stderr, "\tDeleting instance: %s::%s\n", inst->module, inst->class);
		free(inst);
		inst = next;
	}
	fprintf(stderr, "\tDeleting instance: %s::%s\n", inst->module, inst->class);
	free(inst);

	instanceList = NULL;
}	


void pycallinit_(void)
{
	pycallInit();
}


void pycallInit(void)
{
	if(!initialised){
//	        setenv("PYTHONPATH", pypath, 0);
		fprintf(stderr, "$PYTHONPATH = %s\n", getenv("PYTHONPATH"));
		if(!getcwd(initialcwd, MAXPATHLEN))
			perror("getcwd() initialcwd");

		fprintf(stderr, "initialcwd: %s\n", initialcwd);
		Py_Initialize();
		import_array();
		initialised = 1;
	}
}


void pycalltidy_(void)
{
	pycallTidy();
}

void pycallTidy(void)
{
//        Py_Finalize();
}


void consume(va_list *ap, char *fmt)
{
	while (*fmt != ':'){
        	switch (*fmt++) {
			case 'i':
                                va_arg(*ap, int);
                                break;
			case 'd':
                                va_arg(*ap, double);
                                break;
			case 'f':
				va_arg(*ap, double);	// va promotes float to double
				break;
			case 'O':
				va_arg(*ap, void *);
				fprintf(stderr, "Consumed Object\n");
				break;
			case 's':
				va_arg(*ap, void *);
				fprintf(stderr, "Consumed string\n");
				break;
			default:
                        	break;
                }
        }


}


char *rewriteFmt(const char *fmt)
{
	int listStartSize = 32;
	int listReallocSize = 2*listStartSize;
	int listCurMax = listStartSize;
	int listIndex = 0;

	char *fmtList = (char *) malloc(listStartSize * sizeof(char));		// Unfreed
	char *arrayend;
	
	while(*fmt != ':'){
		switch(*fmt++){
			case 'N':	arrayend = strchr(fmt, cbracket);
					fmt = arrayend;
					fmtList[listIndex++] = 'O';
					break;

			case 'i':	fmtList[listIndex++] = 'i';
					break;

			case 'd':	fmtList[listIndex++] = 'd';
					break;

			case 'f':	fmtList[listIndex++] = 'f';
					break;

			case 's':	fmtList[listIndex++] = 's';
					break;
			default:	break;
		}
		if(listIndex == listCurMax-1){
			if(!realloc(fmtList, listReallocSize)){
				perror("rewriteFmt(): realloc failed to allocate listReallocSize bytes.");
				exit(1);
			}
			listReallocSize *= 2;
		}
	}

	fmtList[listIndex] = '\0';

	return fmtList;
}

void vacall(const char const *module, const char const *cls, const char const *method, char *fmt, ...)
{
	PyObject *clientMod=NULL, *clientClass=NULL, *pinst=NULL, *clientDict=NULL;
	PyObject *pargs=NULL, *pfunc=NULL, *ret=NULL, *retTuple=NULL, *nparray=NULL;
	char *moduleFile=NULL, *moduleDir=NULL;
	Instance *instance;

	va_list ap;
	double *da=NULL, *dr=NULL;

	int fmtlen = strlen(fmt);
	int arglen = strcspn(fmt, ":");
	int retlen = fmtlen - arglen;

	char *argfmt = (char *)malloc(arglen+1);
	char *retfmt = (char *)malloc(retlen);

	strncpy(argfmt, fmt, arglen);
	argfmt[arglen] = '\0';
	strncpy(retfmt, fmt+arglen+1, retlen); 
	fprintf(stderr, "args: %s\trets: %s\n", argfmt, retfmt);

	char *objargs = "(O)";
	char *arraystr=NULL, *arrayend=NULL;
	int arraystrlen;

	arraystr = strchr(fmt, arraychar);

	if(arraystr){
		arrayend = strchr(fmt, cbracket);
		arraystrlen = (int) (arrayend - arraystr);
		arraystrlen++;

		char *arrayfmt = (char *)malloc((arraystrlen+1)*sizeof(char));
		strncpy(arrayfmt, arraystr, arraystrlen);
		arrayfmt[arraystrlen] = '\0';

		fprintf(stderr, "arraystrlen: %d\tarrayfmt: %s\n", arraystrlen, arrayfmt);

		va_start(ap, fmt);
		da = (double *) va_arg(ap, double *);
		dr = (double *) va_arg(ap, double *);
		nparray = ndarrayFmt(arrayfmt, da );
		va_end(ap);

		free(arrayfmt);

	}



	if(arraystr){
//		printf("rewriteFmt(): %s\n", rewriteFmt(fmt));
		pargs = Py_BuildValue(objargs, nparray);
	}else if(strcmp(argfmt, "()")){
		fprintf(stderr, "Building arguments...\n");
		va_start(ap, fmt);
		pargs = Py_VaBuildValue(argfmt, ap);
		va_end(ap);
	}

	instance = hasInstance(module, cls);

	if(!instance){

		fprintf(stderr, "PyImport_ImportModule(%s)\n", module);
		fprintf(stderr, "$PYTHONPATH = %s\n", getenv("PYTHONPATH"));
	        clientMod = PyImport_ImportModule(module);
		fprintf(stderr, "After ImportModule(): module %s is at %p from file %s in dir %s\n", module, clientMod, moduleFile, moduleDir);
		if (clientMod==NULL){
		      fprintf(stderr, "\n[BFG] Error, in import of python module %s. There may be a syntax error in the python, or the file has not been found. If it is the latter, please check the name and make sure your pythonpath is set appropriately.\n", module);
		      PyErr_Print();
		      fprintf(stderr, "[BFG] Aborting execution.\n");
		      Py_Finalize();
		      exit(1);
		}
		moduleFile = PyModule_GetFilename(clientMod);
		moduleDir = dirname(strdup(moduleFile));
		fprintf(stderr, "module %s is at %p from file %s in dir %s\n", module, clientMod, moduleFile, moduleDir);
	        clientDict = PyModule_GetDict(clientMod);
        	clientClass = PyObject_GetAttrString(clientMod, cls);
		if (clientClass==NULL){
		  fprintf(stderr, "\n[BFG] Error, the class %s is not found in the module %s\n", cls, module);
		      PyErr_Print();
		      fprintf(stderr, "[BFG] Aborting execution.\n");
		      Py_Finalize();
		      exit(1);
		}

                // constructor may need relative access
	        printf("***** moduleDir is '%s'\n",moduleDir);
                if(chdir(moduleDir))
                        perror("module import chdir");
                if(strcmp(method, "constructor"))
                        pinst = PyEval_CallObject(clientClass, (PyObject*)NULL);     // No instance, no params, silent default constructor
                else
                        pinst = PyEval_CallObject(clientClass, pargs);       // Explicit constructor, use params
		if (pinst==NULL) {
		  fprintf(stderr, "\n[BFG] Error, something has gone wrong in the following python code %s::%s::%s\n", module, cls, method);
		      PyErr_Print();
		      fprintf(stderr, "[BFG] Aborting execution.\n");
		      Py_Finalize();
		      exit(1);
		}
		addInstance(module, cls, pinst, moduleDir);
		instance = hasInstance(module, cls);
	} else { printf("INSTANCE EXISTS\n");}

	if(strcmp(method, "constructor")){

		printf("NOT CONSTRUCTOR\n");
		fprintf(stderr, "cwd was %s ",  (char *)get_current_dir_name() );		// Leaks...
		if(chdir(instance->localdir))
			perror("module import chdir");
		fprintf(stderr, "cwd now %s\n", (char *)get_current_dir_name() );		// Leaks...

	        printf("pfunc %s %s\n",instance->pinst, method);
		pfunc = PyObject_GetAttrString(instance->pinst, method);
		if (pfunc==NULL){
		      fprintf(stderr, "\n[BFG] Error, method %s is not found in %s::%s\n", method, module, cls);
		      PyErr_Print();
		      fprintf(stderr, "[BFG] Aborting execution.\n");
		      Py_Finalize();
		      exit(1);
		}
		ret = PyEval_CallObject(pfunc, pargs);
		revertCwd();
//		fprintf(stderr, "cwd reverts to  %s\n", (char *)get_current_dir_name() );		// Leaks...

                if(PyErr_Occurred()){
                        fprintf(stderr, "\n[BFG] Error in %s::%s::%s\n", module, cls, method);
                        PyErr_Print();
                        fprintf(stderr, "[BFG] Aborting execution.\n");
                        Py_Finalize();
                        exit(1);
                }

	} else { printf("CONSTRUCTOR\n");}


//	if(*retfmt && arraystr || arrayret){
//		ndarrayRet(ret, dr);	
//	}else if(*retfmt){
	if(*retfmt){
		va_start(ap, fmt);
		if(strcmp(argfmt, "()"))
			consume(&ap, fmt);

		if(strlen(retfmt) == 1){
			retTuple = Py_BuildValue("(O)", ret);
		}else{
			retTuple = ret;
			Py_INCREF(retTuple);
		}

		fprintf(stderr, "PyArg_VaParse(%s) : %d\n", retfmt, PyArg_VaParse(retTuple, retfmt, ap));
		va_end(ap);
        }else{
                fprintf(stderr, "No returns...\n");
        }


	if(clientMod)
	        Py_DECREF(clientMod);

	if(clientDict)
	        Py_DECREF(clientDict);

	if(pfunc)
	        Py_DECREF(pfunc);

	if(pargs && (pargs != nparray))
	        Py_DECREF(pargs);

	if(ret && (ret != retTuple)){
		fprintf(stderr, "Decrementing ret...\n");
	        Py_DECREF(ret);
	}

//	if(!arraystr && (strlen(retfmt) == 1)){
//		fprintf(stderr, "Decrementing retTuple...\n");
//		Py_DECREF(retTuple);
//	}

	Py_XDECREF(nparray);
	free(retfmt);
	free(argfmt);

}



// e.g. ndarrayFmt("N[3,5,d]", a);
PyObject *ndarrayFmt(char *fmt, ...)
{
	char *str, *savestr, *token;
	char *delim = ",";
	npy_intp dims[12];
	int i, dimcount=0, t=0, npy_type=0;
	int fmtlen = strlen(fmt);
	fprintf(stderr, "fmtlen: %d\n", fmtlen);

	va_list ap;
	double *da=NULL;
	int *ia=NULL;
	void *npa=NULL;

	str = (char *)malloc((fmtlen+1)*sizeof(char));
	savestr = str;
	strncpy(str, fmt+2, fmtlen-3);
	str[fmtlen-3] = '\0';
	puts(str);

	va_start(ap, fmt);

	for(i=1; ; i++, str = NULL){
		token = strtok(str, delim);
		if(token == NULL)
			break;

		t = atoi(token);
		printf("%d: %s (%d)\n", i, token, t);

		if(t){
			dims[dimcount] = t;
			dimcount++;
		}else{
			switch(token[0]){

/* Promoted to double */	case 'f':
				case 'd':  puts("Token d");
					   npy_type = NPY_DOUBLE;
					   da = (double *) va_arg(ap, double *);
					   npa = (void *) da;
					   break;

                                case 'i':  puts("Token i");
                                           npy_type = NPY_INT;
                                           ia = (int *) va_arg(ap, int *);
                                           npa = (void *) ia;
                                           break;

				default:   break;
			}
		}
	}
	va_end(ap);
	free(savestr);

	printf("da: %p\n", da);

	PyObject *nparray;
//	import_array();

	nparray = PyArray_New(&PyArray_Type, dimcount, dims, npy_type, NULL, npa, 0, NPY_F_CONTIGUOUS | NPY_WRITEABLE, NULL);
        npy_intp argsize = PyArray_NBYTES(nparray);
        printf("argsize = %d\n", argsize);

	return nparray;	

}


void ndarrayRet(PyObject *npret, double *out)
{
	double *resdata;
	int ndims, i;
	size_t itemsize;

	PyObject *nparray = PyArray_FROM_OTF(npret, NPY_DOUBLE, NPY_F_CONTIGUOUS | NPY_WRITEABLE);
//	PyObject *nparray = PyArray_FROM_OTF(npret, NPY_DOUBLE, 0);
	ndims = PyArray_NDIM(nparray);

//        npy_intp* resdims = (npy_intp *)malloc(ndims * sizeof(npy_int));
//        npy_intp* resstrides = (npy_intp *)malloc(ndims * sizeof(npy_int));

	npy_intp *resdims, *resstrides;

	resdims = PyArray_DIMS(nparray);
	resstrides = PyArray_STRIDES(nparray);
	itemsize = PyArray_ITEMSIZE(nparray);
	resdata = (double *) PyArray_DATA(nparray);

        printf("resdims = %d %d\n", resdims[0], resdims[1]);
        printf("resstrides = %d %d\n", resstrides[0], resstrides[1]);
        printf("itemsize = %d\n", itemsize);

	int area = 1;
	for(i = 0; i < ndims; i++)
		area *= resdims[i];

	memcpy(out, resdata, area * itemsize);

//	free(resdims);
//	free(resstrides);

	return;
}

void ndarrayiRet(PyObject *npret, int *out)
{
	int *resdata;
	int ndims, i;
	size_t itemsize;

	PyObject *nparray = PyArray_FROM_OTF(npret, NPY_DOUBLE, NPY_F_CONTIGUOUS | NPY_WRITEABLE);
//	PyObject *nparray = PyArray_FROM_OTF(npret, NPY_INT, 0);
	ndims = PyArray_NDIM(nparray);

//        npy_intp* resdims = (npy_intp *)malloc(ndims * sizeof(npy_int));
//        npy_intp* resstrides = (npy_intp *)malloc(ndims * sizeof(npy_int));

	npy_intp *resdims=NULL, *resstrides=NULL;

	resdims = PyArray_DIMS(nparray);
	resstrides = PyArray_STRIDES(nparray);
	itemsize = PyArray_ITEMSIZE(nparray);
	resdata = (int *) PyArray_DATA(nparray);

        printf("resdims = %d %d\n", resdims[0], resdims[1]);
        printf("resstrides = %d %d\n", resstrides[0], resstrides[1]);
        printf("itemsize = %d\n", itemsize);

	int area = 1;
	for(i = 0; i < ndims; i++)
		area *= resdims[i];

	memcpy(out, resdata, area * itemsize);

//	free(resdims);
//	free(resstrides);

	return;
}

void revertCwd(void)
{
	if(chdir(initialcwd))
		perror("revertCwd");


}
