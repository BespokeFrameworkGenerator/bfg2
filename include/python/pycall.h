#ifndef PYCALL_H
#define PYCALL_H


#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "Python.h"

#ifdef BFG_USE_PYTHON_27
#include <python2.7/Python.h>
#endif

#ifdef BFG_USE_PYTHON_26
#include <python2.6/Python.h>
#endif

#include "numpy/npy_common.h"
#include "numpy/arrayobject.h"
//#include "numpy/ndarraytypes.h"

#define MAXSYMLEN 32
#define MAXPATHLEN 512

typedef int bool;

typedef struct Instance_t{
	char module[MAXSYMLEN];
	char class[MAXSYMLEN];
	const char *localdir;
	PyObject *pinst;
	struct Instance_t *next;

} Instance;

//extern Instance *instanceList;
//extern int initialised;

extern const char arraychar, cbracket;
extern const char const *pypath;
extern char initialcwd[];


Instance *hasInstance(const char const *module, const char const *class);
void addInstance(const char const *module, const char const *class, PyObject *pinst, const char const *moduleDir);
void delInstance(const char const *module, const char const *class);
void delList(void);

void pycallInit(void);
void pycallTidy(void);
void revertCwd(void);

void consume(va_list *ap, char *fmt);
char *rewriteFmt(const char *fmt);

void vacall(const char const *module, const char const *cls, const char const *method, char *fmt, ...);

PyObject *ndarrayFmt(char *fmt, ...);
void ndarrayRet(PyObject *npret, double *out);
void ndarrayiRet(PyObject *npret, int *out);


#endif
