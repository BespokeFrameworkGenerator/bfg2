<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg="http://www.cs.man.ac.uk/cnc/schema/gcf"
exclude-result-prefixes="bfg"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!--
    RF 1st June 2006
xmlns="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
-->

<xsl:output method="xml" indent="yes"/>

<xsl:template match="bfg:coupled">

<!--
<deployment xsi:schemaLocation = "http://www.cs.manchester.ac.uk/cnc/schema/bfg2 http://www.cs.manchester.ac.uk/cnc/schema/bfg2/deployment.xsd">
-->
<deployment xsi:noNamespaceSchemaLocation="http://www.cs.manchester.ac.uk/cnc/schema/bfg2/deployment.xsd">
	<deploymentUnits>
	<xsl:for-each select="document(bfg:deploy)//bfg:deploymentUnit">
		<deploymentUnit language="f90">
		<xsl:for-each select="bfg:model">
			<!-- BFG1 does not implement sequence units -->
			<sequenceUnit threads="1">
				<model name="{bfg:name}"/>
			</sequenceUnit>
		</xsl:for-each>
		</deploymentUnit>
        </xsl:for-each>
	</deploymentUnits>
	<schedule>
		<bfgiterate>
                        <iterate>
			        <loop niters="{document(bfg:compose)//bfg:value}" units="{document(bfg:compose)//bfg:units}">
				<xsl:for-each select="document(bfg:deploy)//bfg:model">
				        <model name="{bfg:name}" ep="{bfg:name}"/>
				</xsl:for-each>
			        </loop>
                        </iterate>
		</bfgiterate>
	</schedule>
        <xsl:variable name="bfg1target" select="translate(document(bfg:deploy)//bfg:comms,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')"/>
        <xsl:if test="not($bfg1target='seqential')">
	<target><xsl:value-of select="$bfg1target"/></target>
        </xsl:if>
</deployment>

</xsl:template>

</xsl:stylesheet>
