<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg="http://www.cs.man.ac.uk/cnc/schema/gcf"
exclude-result-prefixes="bfg"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!--
    RF 2nd June 2006
xmlns="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="ModelType" select="'scientific'"/>

<xsl:template name="ConvertModelfromBFG1toBFG2">
    <xsl:param name="InterfaceFileRef"/>
    <xsl:param name="SourceFileRef"/>
    <xsl:param name="ExecutableFileRef"/>

<xsl:variable name="SourceRoot" select="document($SourceFileRef)"/>
<xsl:variable name="ExecutableRoot" select="document($ExecutableFileRef)"/>
<!--
<definition  xsi:schemaLocation = "http://www.cs.manchester.ac.uk/cnc/schema/bfg2 http://www.cs.manchester.ac.uk/cnc/schema/bfg2/definition.xsd">
-->
<definition  xsi:noNamespaceSchemaLocation="http://www.cs.manchester.ac.uk/cnc/schema/bfg2/definition.xsd">
        <xsl:variable name="ModelName" select="$SourceRoot/bfg:source/bfg:name"/>
	<name><xsl:value-of select="$ModelName"/></name>
	<type><xsl:value-of select="$ModelType"/></type>
	<xsl:variable name="Language">
	  <xsl:variable name="canonicalSourceLanguage" select="translate($SourceRoot/bfg:source/bfg:language,'ABCDEFGHIJKLMNOPQRSTUVWXYZ ','abcdefghijklmnopqrstuvwxyz')"/>
	<xsl:choose>
	<xsl:when test="$canonicalSourceLanguage='fortran77'">
		<xsl:text>f77</xsl:text>
	</xsl:when>
        <!-- there is no "proper" support for fortran90 in BFG1. All codes described as fortran90 are really fortran77. Therefore map to f77 -->
	<xsl:when test="$canonicalSourceLanguage='fortran90'">
		<xsl:text>f77</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="$SourceRoot/bfg:source/bfg:language"/>
	</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<language><xsl:value-of select="$Language"/></language>
	<entryPoints>
		<entryPoint type="iteration" name="{$ModelName}">
		<xsl:for-each select="$SourceRoot/bfg:source/bfg:fields">
		    <data form="inplace"> <!-- all data is inplace in bfg1 -->
			<xsl:variable name="VarType">
				<xsl:choose>
				<xsl:when test="bfg:type/bfg:array">
					<xsl:value-of select="bfg:type/bfg:array/bfg:dimension/bfg:vartype"/>
				</xsl:when>
				<xsl:otherwise><!-- scalar -->
					<xsl:value-of select="bfg:type/bfg:scalar/bfg:vartype"/>
				</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

                        <xsl:choose>
                          <xsl:when test="bfg:type/bfg:scalar">
			    <scalar id="{bfg:id}" direction="{bfg:direction}" dataType="{$VarType}"/>
                          </xsl:when>
                          <xsl:when test="bfg:type/bfg:array">
                            <array dimension="{count(bfg:type/bfg:array/bfg:dimension)}">
			      <scalar id="{bfg:id}" direction="{bfg:direction}" dataType="{$VarType}"/>
				<xsl:for-each select="bfg:type/bfg:array/bfg:dimension">
				<dim value="{bfg:index}">
					<lower><integer><xsl:text>1</xsl:text></integer></lower>
					<upper><integer><xsl:value-of select="bfg:size"/></integer></upper>
				</dim>
				</xsl:for-each>
                            </array>
                          </xsl:when>
                        </xsl:choose>
		    </data>
		</xsl:for-each>
		</entryPoint>
	</entryPoints>
	<timestep units="{$ExecutableRoot/bfg:executable/bfg:timestep/bfg:units}"><xsl:value-of select="$ExecutableRoot/bfg:executable/bfg:timestep/bfg:value"/></timestep>
</definition>
</xsl:template>

</xsl:stylesheet>
