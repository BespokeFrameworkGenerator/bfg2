<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg="http://www.cs.man.ac.uk/cnc/schema/gcf"
exclude-result-prefixes="bfg"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!--
    RF 1st June 2006
xmlns="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
-->

<xsl:output method="xml" indent="yes"/>

<xsl:template match="bfg:coupled">
<!--
<composition xsi:schemaLocation = "http://www.cs.manchester.ac.uk/cnc/schema/bfg2 http://www.cs.manchester.ac.uk/cnc/schema/bfg2/composition.xsd">
-->
<composition xsi:noNamespaceSchemaLocation="http://www.cs.manchester.ac.uk/cnc/schema/bfg2/composition.xsd">
	<connections>
		<timestepping>
		<xsl:for-each select="document(bfg:compose)//bfg:connect">
			<xsl:variable name="OutModel" select="bfg:providesModel/bfg:name"/>
			<xsl:variable name="InModel" select="bfg:requiresModel/bfg:name"/>
			<modelChannel outModel="{$OutModel}" inModel="{$InModel}">
				<epChannel outEP="{$OutModel}" inEP="{$InModel}">
				<xsl:for-each select="bfg:fieldConnect">
					<connection outID="{bfg:provides/bfg:id}" inID="{bfg:requires/bfg:id}"/>
				</xsl:for-each>
				</epChannel>
			</modelChannel>
		</xsl:for-each>
		</timestepping>
<!--
		<priming/>
 priming not available in BFG1 -->
	</connections>
</composition>

</xsl:template>

</xsl:stylesheet>
