<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg="http://www.cs.man.ac.uk/cnc/schema/gcf"
exclude-result-prefixes="bfg"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
xmlns:common="http://exslt.org/common"
extension-element-prefixes="common xalan">

<!--
    RF 2nd June 2006
xmlns="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
-->

<!-- if this param is set to the name of a model then a: only translate for this model and b: don't use file extensions to output to a named file. This is used for the Java version -->
<xsl:param name="ModelName" select="''"/>

<xsl:output method="xml" indent="yes"/>
<xsl:include href="Model.xsl"/>

<xsl:template match="bfg:coupled/bfg:component">

<xsl:variable name="MyModelName">
<xsl:value-of select="document(bfg:model/bfg:source)/bfg:source/bfg:name"/>
</xsl:variable>

<xsl:choose>
<xsl:when test="$ModelName">
 <!-- ModelName has been set so only call template if MyModelName matches -->
  <xsl:if test="$ModelName=$MyModelName">
    <xsl:call-template name="ConvertModelfromBFG1toBFG2">
      <xsl:with-param name="InterfaceFileRef" select="bfg:interface"/>
      <xsl:with-param name="SourceFileRef" select="bfg:model/bfg:source"/>
      <xsl:with-param name="ExecutableFileRef" select="bfg:model/bfg:executable"/>
    </xsl:call-template>
  </xsl:if>
</xsl:when>

<xsl:otherwise><!-- attempt to output for all models -->

<xsl:variable name="FilePath">
<xsl:text>bfg2model</xsl:text>
<xsl:value-of select="$MyModelName"/>
<xsl:text>.xml</xsl:text>
</xsl:variable>

 <xsl:choose>
  <xsl:when test="element-available('common:document')">
   <common:document href="{$FilePath}" method="xml" indent="yes">
      <xsl:call-template name="ConvertModelfromBFG1toBFG2">
        <xsl:with-param name="InterfaceFileRef" select="bfg:interface"/>
        <xsl:with-param name="SourceFileRef" select="bfg:model/bfg:source"/>
        <xsl:with-param name="ExecutableFileRef" select="bfg:model/bfg:executable"/>
      </xsl:call-template>
   </common:document>
  </xsl:when>
  <xsl:when test="element-available('xalan:write')">
   <xalan:write select="$FilePath">
      <xsl:call-template name="ConvertModelfromBFG1toBFG2">
        <xsl:with-param name="InterfaceFileRef" select="bfg:interface"/>
        <xsl:with-param name="SourceFileRef" select="bfg:model/bfg:source"/>
        <xsl:with-param name="ExecutableFileRef" select="bfg:model/bfg:executable"/>
      </xsl:call-template>
   </xalan:write>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>No file output support found</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>

</xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>
