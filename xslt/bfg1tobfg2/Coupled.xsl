<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg="http://www.cs.man.ac.uk/cnc/schema/gcf"
exclude-result-prefixes="bfg"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<!--
    RF 30th May 2006
xmlns="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
-->

<xsl:output method="xml" indent="yes"/>

<xsl:param name="prepend" select="'bfg2'"/>

<xsl:template match="bfg:coupled">
<!--
<coupled xsi:schemaLocation =
"http://www.cs.manchester.ac.uk/cnc/schema/bfg2
http://www.cs.manchester.ac.uk/cnc/schema/bfg2/coupled.xsd">
-->
<coupled xsi:noNamespaceSchemaLocation="http://www.cs.manchester.ac.uk/cnc/schema/bfg2/coupled.xsd">
    <models>
    <xsl:for-each select="bfg:component/bfg:model/bfg:source">
        <model>
            <xsl:text>bfg2model</xsl:text>
            <xsl:value-of select="document(.)/bfg:source/bfg:name"/>
            <xsl:text>.xml</xsl:text>
        </model>
    </xsl:for-each>
    </models>
    <composition>
	<xsl:text>bfg2composition.xml</xsl:text>
    </composition>
    <deployment>
	<xsl:text>bfg2deployment.xml</xsl:text>
    </deployment>
</coupled>
</xsl:template>

<xsl:template name="RenameFilePath">
<xsl:param name="String"/>
<xsl:param name="Prepend"/>
    <xsl:choose>
    <xsl:when test="substring-before($String,'.xml')='' or substring-after($String,'.xml')!=''">
	<xsl:message terminate="yes">
	    <xsl:text>
Fatal error in bfg1tobfg2 RenameFilePath template.
I'm afraid I've been a bit lazy writing this template so it's not too surprising :-(.
The problem is that I've assumed that all filenames end in .xml (case sensitive) and I've also assumed that the string .xml does not appear anywhere else in the path.
The String that has caused me to fall over in a heap is </xsl:text>
<xsl:value-of select="$String"/>
	</xsl:message>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select="substring-before($String,'.xml')"/>
        <xsl:value-of select="$Prepend"/>
        <xsl:value-of select="'.xml'"/>
    </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
