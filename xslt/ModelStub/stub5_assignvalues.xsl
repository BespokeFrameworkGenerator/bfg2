<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:param name="ModifyVars" select="'false'"/>
<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="AssignVars">
  <AssignVars>

    <xsl:variable name="targetEPName" select="ancestor::EP/@name"/>

    <comment><xsl:text>Initialise out variables to sensible values</xsl:text></comment>

    <xsl:for-each select="$root//entryPoint[@name=$targetEPName]/data[@direction='out']">

      <xsl:variable name="varName">
        <xsl:call-template name="GetName">
          <xsl:with-param name="data" select="."/>
        </xsl:call-template>
      </xsl:variable>

      <comment>
        <xsl:text>Setting </xsl:text>
        <xsl:value-of select="$varName"/>
      </comment>

      <xsl:variable name="varValue">
        <xsl:choose>
          <xsl:when test="@dataType='real' or @dataType='double'">
            <xsl:text>1.0</xsl:text>
          </xsl:when>
          <xsl:when test="@dataType='integer'">
            <xsl:text>100</xsl:text>
          </xsl:when>
          <xsl:when test="@dataType='logical'">
            <xsl:text>.true.</xsl:text>
          </xsl:when>
          <xsl:when test="@dataType='complex'">
            <xsl:text>(1.0,1.0)</xsl:text>
          </xsl:when>
          <xsl:when test="@dataType='character'">
            <xsl:text>'c'</xsl:text>
          </xsl:when>
          <xsl:when test="@dataType='string'">
            <xsl:text>"#"</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:value-of select="$newline"/>
              <xsl:text>Error stub5_assignvalues: AssignVars </xsl:text>
              <xsl:text>Only real, double, integer, logical, complex, string and character supported,</xsl:text>
              <xsl:text>Found ... '</xsl:text>
              <xsl:value-of select="@dataType"/>
              <xsl:text>'</xsl:text>
              <xsl:value-of select="$newline"/>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <assign lhs="{$varName}" rhs="{$varValue}"/>

    </xsl:for-each>

    <xsl:if test="$ModifyVars='true'">

      <comment>Modify inout variables</comment>
      <xsl:for-each select="$root//entryPoint[@name=$targetEPName]/data[@direction='inout']">

        <xsl:variable name="varName">
          <xsl:call-template name="GetName">
            <xsl:with-param name="data" select="."/>
          </xsl:call-template>
        </xsl:variable>

        <comment>
          <xsl:text>Modifying </xsl:text>
          <xsl:value-of select="$varName"/>
        </comment>


        <xsl:choose>
          <xsl:when test="@dataType='character'">
            <assign lhs="{$varName}" rhs="'m'"/>
          </xsl:when>
          <xsl:when test="@dataType='string'">
            <assign lhs="{$varName}" rhs="&quot;!&quot;"/>
          </xsl:when>
          <xsl:when test="@dataType='logical'">
             <assign lhs="{$varName}" rhs=".false."/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="varInc">

              <xsl:choose>
                <xsl:when test="@dataType='real' or @dataType='double'">
                  <xsl:text>0.01</xsl:text>
                </xsl:when>
                <xsl:when test="@dataType='integer'">
                  <xsl:text>1</xsl:text>
                </xsl:when>
                <xsl:when test="@dataType='complex'">
                  <xsl:text>(0.01,-0.01)</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:message terminate="yes">
                    <xsl:value-of select="$newline"/>
                    <xsl:text>Error stub5_assignvalues: AssignVars </xsl:text>
                    <xsl:text>Only real, double integer and complex currently supported,</xsl:text>
                    <xsl:text>Found ... '</xsl:text>
                    <xsl:value-of select="@dataType"/>
                    <xsl:text>'</xsl:text>
                    <xsl:value-of select="$newline"/>
                  </xsl:message>
                </xsl:otherwise>
              </xsl:choose>
              
            </xsl:variable>
            <assign lhs="{$varName}" rhs="{$varName}+{$varInc}"/>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:for-each>

    </xsl:if>

  </AssignVars>
</xsl:template>

<xsl:template name="PrintNTimes">
<xsl:param name="count"/>
<xsl:param name="thing"/>
  <xsl:choose>
    <xsl:when test="$count &gt; 0">
      <xsl:value-of select="$thing"/>
      <xsl:call-template name="PrintNTimes">
        <xsl:with-param name="count" select="($count)-1"/>
        <xsl:with-param name="thing" select="$thing"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise/> <!-- stop recursion -->
  </xsl:choose>
</xsl:template>

<xsl:include href="../Utils/VarName.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>

</xsl:stylesheet>