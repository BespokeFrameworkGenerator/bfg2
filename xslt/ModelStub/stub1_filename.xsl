<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:param name="NoStub" select="'false'"/>
<xsl:param name="AppendStub" select="'.stub'"/>
<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="code">

  <xsl:variable name="language" select="$root/definition/language"/>

  <!-- determine the filename we are going to write to -->
  <xsl:variable name="FileName">
    <xsl:choose>
      <xsl:when test="$language='f90'">
        <xsl:call-template name="ModuleName">
          <xsl:with-param name="modelRoot" select="$root"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="$language='f77'">
	<xsl:value-of select="$root/definition/name"/>
      </xsl:when>
      <xsl:when test="$language='python'">
	<xsl:value-of select="$root/definition/name"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:text>Error stub1.xsl template code : filetype </xsl:text>
          <xsl:value-of select="$language"/>
          <xsl:text> is not yet supported</xsl:text>
          <xsl:value-of select="$newline"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:text>.</xsl:text>

    <xsl:choose>
      <xsl:when test="$language='f90'">
        <xsl:text>f90</xsl:text>
      </xsl:when>
      <xsl:when test="$language='f77'">
        <xsl:text>f</xsl:text>
      </xsl:when>
      <xsl:when test="$language='python'">
	<xsl:text>f90</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:text>Error stub1.xsl template code : filetype </xsl:text>
          <xsl:value-of select="$language"/>
          <xsl:text> is not yet supported</xsl:text>
          <xsl:value-of select="$newline"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="$NoStub='false'">
      <!-- add this extension so we don't overwrite any existing code -->
      <xsl:value-of select="$AppendStub"/>
    </xsl:if>

  </xsl:variable>

  <code fullname="{$FileName}">
    <xsl:apply-templates/>
  </code>

</xsl:template>

<xsl:include href="../Utils/ModuleName.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
