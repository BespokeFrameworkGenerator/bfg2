<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->
<xsl:template name="createSubs">
  <xsl:param name="root"/>

  <xsl:for-each select="$root/definition/entryPoints/entryPoint">

    <EP type="procedure" name="{@name}">
      <!-- for all arg passing data -->
      <xsl:for-each select="data[@form='argpass']">
        <!-- make sure they are in the required order -->
        <xsl:sort select="@id" data-type="number" />
        <xsl:call-template name="DataDeclaration"/>
      </xsl:for-each>

      <DataDecsP/>
      <ContentP>
        <PrintEntry/>
        <Gets/>
        <PrintInputEnd/>
        <AssignVars/>
        <Puts/>
        <PrintExit/>
      </ContentP>

    </EP>
  </xsl:for-each>
</xsl:template>

<xsl:template name="DataDeclaration">

      <!-- obtain the argument name -->
      <xsl:variable name="argName">
        <xsl:call-template name="GetName">
          <xsl:with-param name="data" select="."/>
        </xsl:call-template>
      </xsl:variable>

      <arg name="{$argName}" type="{scalar/@dataType}" rank="{array/@dimension}" intent="{scalar/@direction}" shape="{array/@shape}">

        <!-- manipulate stringsize declaration so that it looks like a
        dimension (i.e. add upper element). This allows us to use the
        dimension template at the codegen phase. -->
        <xsl:if test="stringSize">
          <stringSize>
            <upper>
              <xsl:apply-templates select="stringSize/*"/>
            </upper>
          </stringSize>
        </xsl:if>

        <xsl:call-template name="AddDims">
          <xsl:with-param name="dims" select="dim"/>
        </xsl:call-template>

      </arg>

</xsl:template>

</xsl:stylesheet>
