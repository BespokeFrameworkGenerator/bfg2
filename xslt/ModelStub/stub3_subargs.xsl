<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="argument">
  <xsl:variable name="DataRef" select="scalar/@id"/>
  <xsl:choose>
    <!-- if the argument has an explicit name then use it -->
    <xsl:when test="ancestor::entryPoint/data[position()=$DataRef]/@name">
      <variable ref="{ancestor::entryPoint/data[position()=$DataRef]/@name}"/>
    </xsl:when>
    <!-- use it's reference id (tag) -->
    <xsl:otherwise>
      <variable ref="{scalar/@id}"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- declare all our subroutines -->
<xsl:template match="public">
  <public>
    <xsl:for-each select="$root/definition/entryPoints/entryPoint">
    <procedure name="{@name}"/>
    </xsl:for-each>
  </public>
</xsl:template>

<!-- add in our subroutines -->
<xsl:template match="Content">
  <Separator/>
  <Content>
  <!-- declare each entry point (subroutine) -->
  <xsl:call-template name="createSubs">
    <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>

  </Content>
</xsl:template>

<xsl:include href="../Utils/VarName.xsl"/>
<xsl:include href="../Utils/ArrayDims.xsl"/>
<xsl:include href="createSubs.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
