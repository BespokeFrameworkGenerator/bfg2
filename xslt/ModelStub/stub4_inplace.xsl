<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="DataDecsP">

  <xsl:variable name="epName" select="ancestor::EP/@name"/>

  <comment>Declare in-place data</comment>
  <DataDecsP>
    <xsl:for-each select="$root//entryPoint[@name=$epName]/data[@form='inplace']">

      <xsl:variable name="varName">
        <xsl:call-template name="GetName">
          <xsl:with-param name="data" select="."/>
        </xsl:call-template>
      </xsl:variable>

      <declare name="{$varName}" datatype="{@dataType}" dimension="{@dimension}" >

        <!-- manipulate stringsize declaration so that it looks like a
        dimension (i.e. add upper element). This allows us to use the
        dimension template at the codegen phase. -->
        <xsl:if test="stringSize">
          <stringSize>
            <upper>
              <xsl:apply-templates select="stringSize/*"/>
            </upper>
          </stringSize>
        </xsl:if>

        <xsl:call-template name="AddDims">
          <xsl:with-param name="dims" select="dim"/>
        </xsl:call-template>

      </declare>

    </xsl:for-each>
  </DataDecsP>
</xsl:template>

<xsl:template match="Gets">

  <Gets>

    <xsl:variable name="targetEPName" select="ancestor::EP/@name"/>

    <comment>Perform any gets</comment>

    <xsl:for-each select="$root//entryPoint[@name=$targetEPName]/data[@form='inplace' and @direction='in']">
      <call epName="{$targetEPName}" modelName="{ancestor::definition/name}" name="get">
        <xsl:variable name="varName">
          <xsl:call-template name="GetName">
            <xsl:with-param name="data" select="."/>
          </xsl:call-template>
        </xsl:variable>
        <arg name="{$varName}"/>
        <arg name="{@id}"/>
      </call>
    </xsl:for-each>

  </Gets>

</xsl:template>

<xsl:template match="Puts">

  <Puts>

    <xsl:variable name="targetEPName" select="ancestor::EP/@name"/>

    <comment>Perform any puts</comment>

    <xsl:for-each select="$root//entryPoint[@name=$targetEPName]/data[@form='inplace' and @direction='out']">
      <call epName="{$targetEPName}" modelName="{ancestor::definition/name}" name="put">
        <xsl:variable name="varName">
          <xsl:call-template name="GetName">
            <xsl:with-param name="data" select="."/>
          </xsl:call-template>
        </xsl:variable>
        <arg name="{$varName}"/>
        <arg name="{@id}"/>
      </call>
    </xsl:for-each>

  </Puts>

</xsl:template>

<xsl:include href="../Utils/VarName.xsl"/>
<xsl:include href="../Utils/ArrayDims.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>

</xsl:stylesheet>