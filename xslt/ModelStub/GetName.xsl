<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:output method="text"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:template match="/definition">

<xsl:value-of select="name"/>
<xsl:text>.</xsl:text>
<xsl:choose>
<xsl:when test="language='f90'">
<xsl:text>f90</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:message terminate="yes">
<xsl:value-of select="$newline"/>
<xsl:text>
Error: extension for language </xsl:text>
<xsl:value-of select="language"/>
<xsl:text> is not known
</xsl:text>
</xsl:message>
</xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>