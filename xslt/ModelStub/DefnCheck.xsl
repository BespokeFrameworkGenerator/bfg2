<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:output method="text"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:template match="/">
<xsl:if test="not(definition)">
<xsl:message terminate="yes">
<xsl:text>

Error: The input file is not a model definition document
</xsl:text>
</xsl:message>
</xsl:if>
</xsl:template>

</xsl:stylesheet>