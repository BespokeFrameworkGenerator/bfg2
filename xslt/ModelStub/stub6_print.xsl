<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:param name="PrintVars" select="'false'"/>
<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="PrintEntry">

  <xsl:variable name="language" select="$root/definition/language"/>

  <PrintEntry>
    <!-- <xsl:variable name="targetEPName"
  select="ancestor::EP/@name"/> -->

    <print>
      <string>
        <xsl:text>Entered</xsl:text>
        <xsl:if test="$language='f90'">
          <xsl:text> module </xsl:text>
          <xsl:value-of select="$root/definition/name"/>
        </xsl:if>
        <xsl:text> subroutine </xsl:text>
        <xsl:value-of select="ancestor::EP/@name"/>
      </string>
      <!-- <var name=""/> -->
    </print>

  </PrintEntry>
</xsl:template>

<xsl:template match="PrintInputEnd">
  <PrintInputEnd>

    <xsl:if test="$PrintVars='true'">

      <comment>Printing input vars</comment>

      <print>
        <string>
          <xsl:text>On entry in and inout variables have the following values ...</xsl:text>
        </string>
      </print>

      <xsl:variable name="epName" select="ancestor::EP/@name"/>

      <xsl:for-each select="$root//entryPoint[@name=$epName]/data[@direction='in' or @direction='inout']">

        <xsl:variable name="varName">
          <xsl:call-template name="GetName">
            <xsl:with-param name="data" select="."/>
          </xsl:call-template>
        </xsl:variable>

        <print>
          <string>
            <xsl:text>Variable </xsl:text>
            <xsl:value-of select="$varName"/>
          </string>
        </print>

        <print>
          <var name="{$varName}" sum="true" dimension="{@dimension}"/>
        </print>

      </xsl:for-each>

    </xsl:if>

  </PrintInputEnd>
</xsl:template>

<xsl:template match="PrintExit">

  <xsl:variable name="language" select="$root/definition/language"/>

  <PrintExit>

    <xsl:if test="$PrintVars='true'">

      <comment>Printing output vars</comment>

      <print>
        <string>
          <xsl:text>On exit out and inout variables have the following values ...</xsl:text>
        </string>
      </print>

      <xsl:variable name="epName" select="ancestor::EP/@name"/>

      <xsl:for-each select="$root//entryPoint[@name=$epName]/data[@direction='out' or @direction='inout']">

        <xsl:variable name="varName">
          <xsl:call-template name="GetName">
            <xsl:with-param name="data" select="."/>
          </xsl:call-template>
        </xsl:variable>

        <print>
          <string>
            <xsl:text>Variable </xsl:text>
            <xsl:value-of select="$varName"/>
          </string>
        </print>

        <print>
          <var name="{$varName}" sum="true" dimension="{@dimension}"/>
        </print>

      </xsl:for-each>

    </xsl:if>

    <print>
      <string>
        <xsl:text>Exiting</xsl:text>
        <xsl:if test="$language='f90'">
          <xsl:text> module </xsl:text>
          <xsl:value-of select="$root/definition/name"/>
        </xsl:if>
        <xsl:text> subroutine </xsl:text>
        <xsl:value-of select="ancestor::EP/@name"/>
      </string>
    </print>

  </PrintExit>
</xsl:template>

<xsl:include href="../Utils/VarName.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
