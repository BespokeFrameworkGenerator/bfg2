<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->
<xsl:param name="DefinitionDocument" select="'definition.xml'"/>
<xsl:variable name="root" select="document($DefinitionDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="EP">

  <xsl:variable name="language" select="$root/definition/language"/>

  <xsl:choose>

  <xsl:when test="$language='f77'">
    <!-- create our subroutine(s) now rather than filling in (f90) module details -->
    <xsl:call-template name="createSubs">
      <xsl:with-param name="root" select="$root"/>
    </xsl:call-template>

  </xsl:when>
  <xsl:otherwise>

  <xsl:variable name="moduleName">
    <xsl:call-template name="ModuleName">
      <xsl:with-param name="modelRoot" select="$root"/>
    </xsl:call-template>
  </xsl:variable>
 
  <EP type="entryPoint" name="{$moduleName}">
    <xsl:apply-templates/>
  </EP>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="Includes">
<comment>Created by BFG2 ModelGen</comment>
<Includes>
  <!-- only include bfg2 if we have in-place put's or get's -->
  <xsl:if test="$root//data[@form='inplace']">
    <include name="bfg2"/>
  </xsl:if>
</Includes>
<!-- add in Visibility so that we can declare public and private -->
<Visibility>
  <private/>
  <public/>
</Visibility>
</xsl:template>

<xsl:include href="../Utils/ModuleName.xsl"/>
<xsl:include href="createSubs.xsl"/>
<xsl:include href="../Utils/MatchAll.xsl"/>
<xsl:include href="../Utils/ArrayDims.xsl"/>
<xsl:include href="../Utils/VarName.xsl"/>
</xsl:stylesheet>
