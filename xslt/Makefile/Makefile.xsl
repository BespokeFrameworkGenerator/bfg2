<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<!-- default fileseparator is forward /. This can be changed on the
command line as we've specified it as a parameter-->
<xsl:param name="FileSep" select="'/'"/>

<!-- If PathToCoupledDoc is set then we assume that model code is
found in the same place as the xml definition document -->
<xsl:param name="PathToCoupledDoc" select="''"/>

<xsl:output method="text"/>
<xsl:param name="FileSep" select="'/'"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="/">

<!-- there appears to be a bug in (at least cygwin) python xslt where
we can not access / from within the next for-each statement. I've got
round this by creating a variable (models) outside of the loop which
we use instead -->
<xsl:variable name="models" select="/coupled/models/model"/>

<xsl:variable name="root" select="."/>

<!-- perform some basic checks to make sure we support the requested language -->
<!-- check specified deployment language(s) -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:if test="not(@language='f90')">
    <xsl:message terminate="yes">
      <xsl:text>Error: Makefile only supports a deployment language of 'f90'.</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text>Found </xsl:text>
      <xsl:value-of select="@language"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:message>
  </xsl:if>
</xsl:for-each>

<!-- check specified model languages -->
<xsl:for-each select="document(//model)/definition">
  <xsl:if test="not(language='f90' or language='f77')">
    <xsl:message terminate="yes">
      <xsl:text>Error: Makefile only supports a model languages of 'f90' and 'f77'.</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text>Found </xsl:text>
      <xsl:value-of select="language"/>
      <xsl:text> for model </xsl:text>
      <xsl:value-of select="name"/>
      <xsl:value-of select="$newline"/>
    </xsl:message>
  </xsl:if>
</xsl:for-each>

  <!-- set to true if inplace routine is generated -->
  <xsl:variable name="InPlaceRequired">
    <xsl:call-template name="IsInPlaceRequired">
<!--      <xsl:with-param name="modelName" select="$currName"/> -->
      <xsl:with-param name="root" select="$root"/>
    </xsl:call-template>
  </xsl:variable>


<!-- start of makefile -->

<xsl:text>#Created by BFG2 Makefile Generator</xsl:text>
<xsl:value-of select="$newline"/>

<!-- default to mpif90 -->
<xsl:text>#F90=ifort</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>#F90=gfortran</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>#F90=g95</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>F90=mpif90</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>FFLAGS=</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>LDFLAGS=</xsl:text>
<xsl:value-of select="$newline"/>

<xsl:if test="$PathToCoupledDoc">

  <xsl:text>#</xsl:text>
  <xsl:value-of select="$newline"/>

  <xsl:text>PathToCoupledDoc=</xsl:text>
  <xsl:value-of select="$PathToCoupledDoc"/>
  <xsl:value-of select="$newline"/>

  <xsl:text>#</xsl:text>
  <xsl:value-of select="$newline"/>

</xsl:if>

<xsl:for-each select="document(//model)/definition">
  <xsl:call-template name="VarName">
    <xsl:with-param name="ModelName" select="name"/>
  </xsl:call-template>
  <xsl:text>=</xsl:text>
  <xsl:choose>
    <xsl:when test="$PathToCoupledDoc">
      <xsl:text>${PathToCoupledDoc}</xsl:text>
      <xsl:value-of select="$FileSep"/>
      <xsl:call-template name="GetMyPath">
        <xsl:with-param name="ModelName" select="name"/>
        <xsl:with-param name="models" select="$models"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>.</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="$newline"/>
</xsl:for-each>

<xsl:text>#</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>all:</xsl:text>

<!-- name each executable that we plan to compile -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:text> exe</xsl:text>
  <xsl:value-of select="position()"/>
</xsl:for-each>

<xsl:value-of select="$newline"/>

<!-- create link line for each executable -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:text>exe</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>:</xsl:text>

  <xsl:call-template name="ObjectList">
    <xsl:with-param name="models" select="$models"/>
    <xsl:with-param name="InPlaceRequired" select="$InPlaceRequired"/>
  </xsl:call-template>

  <xsl:value-of select="$newline"/>

  <xsl:text>	${F90} ${FFLAGS} -o exe</xsl:text>
  <xsl:value-of select="position()"/>

  <xsl:call-template name="ObjectList">
    <xsl:with-param name="models" select="$models"/>
    <xsl:with-param name="InPlaceRequired" select="$InPlaceRequired"/>
  </xsl:call-template>

  <xsl:text> ${LDFLAGS}</xsl:text>
  <xsl:value-of select="$newline"/>

</xsl:for-each>

<!-- generate the source to object rules explicitly -->
<xsl:for-each select="document(//deployment)//deploymentUnit">


  <!-- preceding-sibling test is make sure we only generate a rule for one instance -->
  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">


    <!-- what is the current model name? -->
    <xsl:variable name="currName" select="@name"/>

    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="language" select="document($root//model)/definition[name=$currName]/language"/>

    <!-- if so, generate the appropriate compile rule-->
    <xsl:if test="$InPlaceRequired='true'">

      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2InPlace_',$moduleName)"/>
      </xsl:call-template>

      <xsl:if test="$language='f77'">
	<xsl:call-template name="SourceToObjectRule">
          <xsl:with-param name="root" select="concat('BFG2InPlace_',$moduleName,'_wrapper')"/>
	</xsl:call-template>
      </xsl:if>

    </xsl:if>

    <!-- generate rule for this model code -->
    <xsl:call-template name="SourceToObjectRule">
      <xsl:with-param name="root" select="$moduleName"/>
      <xsl:with-param name="language" select="$language"/>
      <xsl:with-param name="addPath" select="'true'"/>
    </xsl:call-template>

  </xsl:for-each>

  <xsl:call-template name="SourceToObjectRule">
    <xsl:with-param name="root" select="concat('BFG2Main',position())"/>
  </xsl:call-template>

  <xsl:call-template name="SourceToObjectRule">
    <xsl:with-param name="root" select="concat('BFG2Target',position())"/>
  </xsl:call-template>

</xsl:for-each>

<xsl:text>clean:</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>	rm -f exe?</xsl:text>

<!-- preceding-sibling test is to make sure we only generate a rule for one instance -->
<xsl:for-each select="document(//deployment)//deploymentUnits//model[not(@name=preceding-sibling::model/@name)]">

  <xsl:variable name="currName" select="@name"/>
  <xsl:variable name="moduleName">
    <xsl:call-template name="ModuleName">
      <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:text> </xsl:text>
  <xsl:value-of select="$moduleName"/>
  <xsl:text>.o</xsl:text>
</xsl:for-each>

<xsl:text> BFG2InPlace_*.o BFG2Target?.o BFG2Main?.o *.mod</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:value-of select="$newline"/>

</xsl:template>

<xsl:template name="SourceToObjectRule">
<xsl:param name="root"/>
<xsl:param name="language" select="'f90'"/>
<xsl:param name="addPath" select="'false'"/>

  <xsl:variable name="fileExt">
    <xsl:text>.</xsl:text>
    <xsl:choose>
      <xsl:when test="$language='f90'">
        <xsl:text>f90</xsl:text>
      </xsl:when>
      <xsl:when test="$language='f77'">
        <xsl:text>f</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>UNSUPPORTED</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="directory">
    <xsl:if test="$addPath='true'">
      <xsl:text>${</xsl:text>
      <xsl:call-template name="VarName">
        <xsl:with-param name="ModelName" select="$root"/>
      </xsl:call-template>
      <xsl:text>}</xsl:text>
      <xsl:value-of select="$FileSep"/>
    </xsl:if>
  </xsl:variable>

  <xsl:value-of select="$root"/>
  <xsl:text>.o : </xsl:text>
  <xsl:if test="$addPath='true'">
    <xsl:value-of select="$directory"/>
  </xsl:if>
  <xsl:value-of select="$root"/>
  <xsl:value-of select="$fileExt"/>
  <xsl:value-of select="$newline"/>
  <xsl:choose>
    <xsl:when test="$language='f90'">
      <xsl:text>	${F90} -c ${FFLAGS} </xsl:text>
    </xsl:when>
    <xsl:when test="$language='f77'">
      <xsl:text>	${F90} -c -nofixed ${FFLAGS} </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>Not implemented.</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$addPath='true'">
    <xsl:value-of select="$directory"/>
  </xsl:if>
  <xsl:value-of select="$root"/>
  <xsl:value-of select="$fileExt"/>
  <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template name="ObjectList">
  <xsl:param name="models"/>
  <xsl:param name="InPlaceRequired"/>

  <!-- need to add proper name selection using module if it exists -->
  <!-- preceding-sibling test is to make sure we only generate a rule for one instance -->
  <!-- respecting dependcies of Target, InPlace_model_x, model_x, Main(needs two loops...) -->

  <xsl:text> BFG2Target</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>.o</xsl:text>

  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">

    <xsl:variable name="currName" select="@name"/>
    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="language" select="document($models)/definition[name=$currName]/language"/>

    <xsl:if test="$InPlaceRequired='true'">
      <xsl:text> BFG2InPlace_</xsl:text>
      <xsl:value-of select="$moduleName"/>
      <xsl:text>.o</xsl:text>

      <xsl:if test="$language='f77'">
	<xsl:text> BFG2InPlace_</xsl:text>
	<xsl:value-of select="$moduleName"/>
	<xsl:text>_wrapper.o</xsl:text>
      </xsl:if>

    </xsl:if>

  </xsl:for-each>

  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">

    <xsl:variable name="currName" select="@name"/>
    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:text> </xsl:text>
    <xsl:value-of select="$moduleName"/>
    <xsl:text>.o</xsl:text>

  </xsl:for-each>

  <xsl:text> BFG2Main</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>.o</xsl:text>

</xsl:template>

<xsl:template name="IsInPlaceRequired">
  <!-- <xsl:param name="modelName"/> -->
  <xsl:param name="root"/>

  <!-- inplace calls mean we generate an inplace routine or multiple
       su's mean we generate an inplace routine **** actually we
       should really have a coupling between su's too ****
   -->
<!--
  <xsl:if test="document($root//model)/definition[name=$modelName]//data[@form='inplace'] or count(document($root//deployment)/deployment//sequenceUnit)&gt;1">
-->
  <xsl:if test="document($root//model)/definition//data[@form='inplace'] or count(document($root//deployment)/deployment//sequenceUnit)&gt;1">
    <xsl:text>true</xsl:text>
  </xsl:if>

</xsl:template>

<!-- Provide a private variable name for each model -->
<xsl:template name="VarName">
  <xsl:param name="ModelName"/>

  <xsl:value-of select="$ModelName"/>
  <xsl:text>MODEL</xsl:text>

</xsl:template>

<xsl:template name="GetMyPath">
  <xsl:param name="ModelName"/>
  <xsl:param name="models"/>

  <xsl:for-each select="$models">
    <xsl:if test="document(.)/definition[name=$ModelName]">
      <xsl:call-template name="ExtractPath">
        <xsl:with-param name="XMLDocLocation" select="."/>
      </xsl:call-template>
    </xsl:if>
  </xsl:for-each>

</xsl:template>

<xsl:template name="ExtractPath">
  <xsl:param name="XMLDocLocation"/>

  <xsl:value-of select="substring-before($XMLDocLocation,$FileSep)"/>

  <xsl:if test="substring-after($XMLDocLocation,$FileSep)">

    <!-- do a second test to make sure we don't leave a trailing FileSep on our path -->
    <!-- do this test afterwards as we may get errors if we try to do substring on null??? -->
    <xsl:if test="substring-after(substring-after($XMLDocLocation,$FileSep),$FileSep)">

      <xsl:value-of select="$FileSep"/>

      <xsl:call-template name="ExtractPath">
        <xsl:with-param name="XMLDocLocation" select="substring-after($XMLDocLocation,$FileSep)"/>
      </xsl:call-template>

    </xsl:if>

  </xsl:if>

</xsl:template>

<xsl:include href="../Utils/ModuleName.xsl"/>

</xsl:stylesheet>
