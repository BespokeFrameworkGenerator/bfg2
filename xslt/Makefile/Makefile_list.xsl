<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<xsl:param name="BFGPYOBJECT" select="'pycall'"/>
<xsl:include href="Makefile_template.xsl"/>

<xsl:template match="/">

<xsl:variable name="root" select="."/>

<xsl:variable name="models" select="/coupled/models/model"/>

<xsl:for-each select="document(//deployment)//deploymentUnit">

  <xsl:variable name="InPlaceRequired">
    <xsl:call-template name="IsInPlaceRequired">
      <xsl:with-param name="root" select="$root"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="ProgramCompliant">
  <xsl:if test="count(.//model)=1">
    <xsl:variable name="modelName" select=".//model/@name"/>
    <xsl:if test="count(document($root//model)/definition[name=$modelName]//entryPoint)=1">
      <xsl:if test="document($root//model)/definition[name=$modelName]//entryPoint[@type='program']">
        <xsl:text>true</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:if>
  </xsl:variable>

  <xsl:call-template name="FileList">
    <xsl:with-param name="models" select="$models"/>
    <xsl:with-param name="InPlaceRequired" select="$InPlaceRequired"/>
    <xsl:with-param name="ProgramCompliant" select="$ProgramCompliant"/>
    <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>

  <xsl:value-of select="$newline"/>

</xsl:for-each>

</xsl:template>

<xsl:template name="FileList">
  <xsl:param name="models"/>
  <xsl:param name="InPlaceRequired"/>
  <xsl:param name="ProgramCompliant"/>
  <xsl:param name="root"/>

  <!-- need to add proper name selection using module if it exists -->
  <!-- preceding-sibling test is to make sure we only generate a rule for one instance -->
  <!-- respecting dependencies of Target, InPlace_model_x, model_x, Main(needs two loops...) -->

  <xsl:variable name="target">
    <xsl:value-of select="document($root//deployment)//target"/>
  </xsl:variable>

  <!-- target must be compiled after model and esmf wrapper code for esmf target -->
  <xsl:if test="not($target='esmf')">
    <xsl:text> BFG2Target</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.f90</xsl:text>
    <xsl:value-of select="$newline"/>
  </xsl:if>

  <xsl:variable name="languages">
    <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">
      <xsl:variable name="currName" select="@name"/>
      <xsl:value-of select="document($root//model)/definition[name=$currName]/language"/>
    </xsl:for-each>
  </xsl:variable>

  <xsl:if test="$InPlaceRequired='true'">

    <xsl:text> BFG2InPlace</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.f90</xsl:text>
    <xsl:value-of select="$newline"/>

  </xsl:if>

  <xsl:text> BFG2Interface</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>.f90</xsl:text>
  <xsl:value-of select="$newline"/>

  <xsl:if test="$InPlaceRequired='true'">

    <xsl:if test="contains($languages,'f77')">
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f77wrapper.f90</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f90wrapper.f90</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>

    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.c</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f90wrapper.f90</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>

  </xsl:if>

  <xsl:choose>
  <xsl:when test="$ProgramCompliant='true'">
    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2ProgramConformance</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.c</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>
    <xsl:if test="contains($languages,'c') or contains($languages,'f77')">
      <xsl:text> BFG2ProgramConformance</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f77wrapper.f90</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>
  </xsl:when>
  <xsl:otherwise>
    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2Control</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.c</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>
  </xsl:otherwise>
  </xsl:choose>

  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">

    <xsl:variable name="currName" select="@name"/>
    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:text> </xsl:text>
    <xsl:value-of select="$moduleName"/>
    <xsl:value-of select="$newline"/>
    <!--
    <xsl:text>.o</xsl:text>
    -->

    <xsl:if test="document($models)/definition[name=$currName]/language='python'">
      <!-- we generate a c wrapper for python code -->
      <xsl:text> </xsl:text>
      <xsl:value-of select="$moduleName"/>
      <xsl:text>.c</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>

    <xsl:if test="$target='esmf'">
      <xsl:text> </xsl:text>
      <xsl:value-of select="$moduleName"/>
      <xsl:text>_esmf.f90</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>

  </xsl:for-each>

  <xsl:variable name="ProgramCompliant">
  <xsl:if test="count(.//model)=1">
    <xsl:variable name="modelName" select=".//model/@name"/>
    <xsl:if test="count(document($root//model)/definition[name=$modelName]//entryPoint)=1">
      <xsl:if test="document($root//model)/definition[name=$modelName]//entryPoint[@type='program']">
        <xsl:text>true</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:if>
  </xsl:variable>

  <xsl:if test="$target='esmf'">
    <xsl:text> BFG2Target</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.f90</xsl:text>
    <xsl:value-of select="$newline"/>
  </xsl:if>

  <xsl:if test="not($ProgramCompliant='true')">
    <xsl:text> BFG2Main</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.f90</xsl:text>
    <xsl:value-of select="$newline"/>
  </xsl:if>

    <xsl:if test="contains($languages,'python')">
      <xsl:text> </xsl:text>  <!-- space -->
      <xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.c</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:if>

</xsl:template>

</xsl:stylesheet>
