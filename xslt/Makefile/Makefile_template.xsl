<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<!-- default fileseparator is forward /. This can be changed on the
command line as we've specified it as a parameter-->
<xsl:param name="FileSep" select="'/'"/>

<!-- If PathToCoupledDoc is set then we assume that model code is
found in the same place as the xml definition document -->
<xsl:param name="PathToCoupledDoc" select="''"/>

<xsl:param name="BFG2ROOT" select="''"/>
<xsl:param name="F90" select="''"/>
<xsl:param name="F77" select="''"/>
<xsl:param name="CC" select="''"/>
<xsl:param name="F90FLAGS" select="''"/>
<xsl:param name="F77FLAGS" select="''"/>
<xsl:param name="CFLAGS" select="''"/>
<xsl:param name="PYFLAGS" select="''"/>
<xsl:param name="LDFLAGS_F90" select="''"/>
<xsl:param name="LDFLAGS_F77" select="''"/>
<xsl:param name="LDFLAGS_CC" select="''"/>
<xsl:param name="LDFLAGS_PY" select="''"/>
<xsl:param name="LDFLAGS_C2F" select="''"/>

<xsl:param name="OASIS3_FLAGS" select="''"/>
<xsl:param name="LDFLAGS_OASIS3" select="''"/>

<xsl:param name="F90FLAGS_NETCDF" select="''"/>
<xsl:param name="LDFLAGS_NETCDF" select="''"/>

<xsl:param name="ESMF_DIR" select="''"/>

<xsl:output method="text"/>
<xsl:param name="FileSep" select="'/'"/>

<xsl:param name="DUIndex" select="'false'"/>

<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="/">

<!-- there appears to be a bug in (at least cygwin) python xslt where
we can not access / from within the next for-each statement. I've got
round this by creating a variable (models) outside of the loop which
we use instead -->
<xsl:variable name="models" select="/coupled/models/model"/>

<xsl:variable name="root" select="."/>

<!-- perform some basic checks to make sure we support the requested language -->
<!-- check specified deployment language(s) -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:if test="not(@language='f90')">
    <xsl:message terminate="yes">
      <xsl:text>Error: Makefile only supports a deployment language of 'f90'.</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text>Found </xsl:text>
      <xsl:value-of select="@language"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="$newline"/>
    </xsl:message>
  </xsl:if>
</xsl:for-each>

<!-- check specified model languages -->
<xsl:for-each select="document(//model)/definition">
  <xsl:if test="not(language='f90' or language='f77' or language='c' or language='python')">
    <xsl:message terminate="yes">
      <xsl:text>Error: Makefile only supports a model languages of 'f90', 'f77', 'c' and Python.</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:text>Found </xsl:text>
      <xsl:value-of select="language"/>
      <xsl:text> for model </xsl:text>
      <xsl:value-of select="name"/>
      <xsl:value-of select="$newline"/>
    </xsl:message>
  </xsl:if>
</xsl:for-each>

<xsl:variable name="target">
  <xsl:value-of select="document(//deployment)//target"/>
</xsl:variable>

  <!-- set to true if inplace routine is generated -->
  <xsl:variable name="InPlaceRequired">
    <xsl:call-template name="IsInPlaceRequired">
<!--      <xsl:with-param name="modelName" select="$currName"/> -->
      <xsl:with-param name="root" select="$root"/>
    </xsl:call-template>
  </xsl:variable>

<xsl:variable name="languages">
  <xsl:for-each select="document(//deployment)//deploymentUnit//model[not(@name=preceding-sibling::model/@name)]">
    <xsl:variable name="currName" select="@name"/>
    <xsl:value-of select="document($root//model)/definition[name=$currName]/language"/>
  </xsl:for-each>
</xsl:variable>

<!-- start of makefile -->

<xsl:text>#Created by BFG2 Makefile Generator</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>BFG2ROOT=</xsl:text><xsl:value-of select="$BFG2ROOT"/>
<xsl:value-of select="$newline"/>

<xsl:if test="$target='esmf'">
  <xsl:text>ESMF_INSTALL_LIBDIR_ABSPATH=</xsl:text>
  <xsl:value-of select="$ESMF_DIR"/>
  <xsl:value-of select="$newline"/>
  <xsl:text>include $(ESMF_INSTALL_LIBDIR_ABSPATH)/esmf.mk</xsl:text>
  <xsl:value-of select="$newline"/>
  <xsl:text>ESMF_FFLAGS=$(ESMF_F90COMPILEOPTS) $(ESMF_F90COMPILEPATHS) $(ESMF_F90COMPILECPPFLAGS)</xsl:text>
  <xsl:value-of select="$newline"/>
  <xsl:text>ESMF_LDFLAGS=$(ESMF_F90LINKOPTS) $(ESMF_F90LINKPATHS) $(ESMF_F90LINKRPATHS) $(ESMF_F90ESMFLINKLIBS)</xsl:text>
  <xsl:value-of select="$newline"/>
</xsl:if>

<xsl:text>F90=</xsl:text><xsl:value-of select="$F90"/>
<xsl:value-of select="$newline"/>
<xsl:text>F77=</xsl:text><xsl:value-of select="$F77"/>
<xsl:value-of select="$newline"/>
<xsl:text>CC=</xsl:text><xsl:value-of select="$CC"/>
<xsl:value-of select="$newline"/>
<xsl:text>F90FLAGS=</xsl:text><xsl:value-of select="$F90FLAGS"/>
<xsl:choose>
<xsl:when test="$target='oasis3'">
  <xsl:text> </xsl:text>
  <xsl:value-of select="$OASIS3_FLAGS"/>
</xsl:when>
<xsl:when test="$target='esmf'">
  <xsl:text> ${ESMF_FFLAGS}</xsl:text>
</xsl:when>
</xsl:choose>
<xsl:value-of select="$newline"/>
<xsl:text>F77FLAGS=</xsl:text><xsl:value-of select="$F77FLAGS"/>
<xsl:value-of select="$newline"/>
<xsl:text>CFLAGS=</xsl:text><xsl:value-of select="$CFLAGS"/>
<xsl:value-of select="$newline"/>

<xsl:if test="contains($languages,'python')">
<xsl:text>BFGPYINCLUDE=</xsl:text><xsl:value-of select="$BFG2ROOT"/><xsl:text>/include/python</xsl:text>
<xsl:value-of select="$newline"/>
<!--
<xsl:text>BFGPYINCLUDE=</xsl:text><xsl:value-of select="$BFGPYINCLUDE"/>
<xsl:value-of select="$newline"/>
-->
<!--
<xsl:text>NUMPYINCLUDE=/usr/local/lib/python2.7/site-packages/numpy-1.6.1-py2.7-linux-i686.egg/numpy/core/include</xsl:text>
-->
<xsl:text>NUMPYINCLUDE=</xsl:text><xsl:value-of select="$NUMPYINCLUDE"/>
<xsl:value-of select="$newline"/>
<xsl:text>PYINCLUDE=</xsl:text><xsl:value-of select="$PYINCLUDE"/>
<xsl:value-of select="$newline"/>
<xsl:text>PYFLAGS=</xsl:text><xsl:value-of select="$PYFLAGS"/>
<xsl:value-of select="$newline"/>
</xsl:if>

<xsl:text>F90FLAGS_NETCDF=</xsl:text><xsl:value-of select="$F90FLAGS_NETCDF"/>
<xsl:value-of select="$newline"/>
<xsl:text>LDFLAGS_NETCDF=</xsl:text><xsl:value-of select="$LDFLAGS_NETCDF"/>
<xsl:value-of select="$newline"/>

<xsl:choose>
  <xsl:when test="$target='oasis4'">
    <xsl:text>OASISDIR=/home/rupert/software/OASIS4_dev4/oasis4/Intel</xsl:text>
    <xsl:value-of select="$newline"/>
    <xsl:text>INCLUDE=-I${OASISDIR}/build/lib/psmile_oa4.MPI1 -I${OASISDIR}/build/lib/common_oa4</xsl:text>
    <xsl:value-of select="$newline"/>
  </xsl:when>
<!--
  <xsl:otherwise>
    <xsl:text>INCLUDE= -I/usr/local/include/python2.7 -I.</xsl:text>
  </xsl:otherwise>
-->
</xsl:choose>
<xsl:value-of select="$newline"/>

<xsl:choose>
<xsl:when test="contains($languages,'c')">
<xsl:text>LDFLAGS_CC=</xsl:text><xsl:value-of select="$LDFLAGS_CC"/>
<xsl:value-of select="$newline"/>
<xsl:text> LDFLAGS_C2F=</xsl:text><xsl:value-of select="$LDFLAGS_C2F"/>
</xsl:when>
<xsl:when test="contains($languages,'python')">
<xsl:text>LDFLAGS_CC=</xsl:text><xsl:value-of select="$LDFLAGS_PY"/><xsl:text> </xsl:text><xsl:value-of select="$LDFLAGS_CC"/>
<xsl:value-of select="$newline"/>
</xsl:when>
<xsl:when test="$target='oasis4'">
<xsl:text>LDFLAGS_F90=-L${OASISDIR}/lib -lpsmile_oa4.MPI1 -lcommon_oa4 -lnetcdf -lxml2</xsl:text>
</xsl:when>
<xsl:when test="$target='esmf'">
<xsl:text>LDFLAGS_F90= ${ESMF_LDFLAGS}</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:text>LDFLAGS_F90=</xsl:text><xsl:value-of select="$LDFLAGS_F90"/>
<xsl:if test="$target='oasis3'">
  <xsl:text> </xsl:text>
  <xsl:value-of select="$LDFLAGS_OASIS3"/>
</xsl:if>
<xsl:value-of select="$newline"/>
<xsl:text>LDFLAGS_F77=</xsl:text><xsl:value-of select="$LDFLAGS_F77"/>
</xsl:otherwise>
</xsl:choose>
<xsl:value-of select="$newline"/>

<xsl:if test="$PathToCoupledDoc">

  <xsl:text>#</xsl:text>
  <xsl:value-of select="$newline"/>

  <xsl:text>PathToCoupledDoc=</xsl:text>
  <xsl:value-of select="$PathToCoupledDoc"/>
  <xsl:value-of select="$newline"/>

  <xsl:text>#</xsl:text>
  <xsl:value-of select="$newline"/>

</xsl:if>

<xsl:for-each select="document(//model)/definition">
  <xsl:call-template name="VarName">
    <xsl:with-param name="ModelName" select="name"/>
  </xsl:call-template>
  <xsl:text>=</xsl:text>
<!--
Selects current dir for Python C-API code, othewise use original
logic for other languages.
-->
  <xsl:variable name="modelLanguage" select="current()/language" />
  <xsl:choose>
<!--
<xsl:when test="$modelLanguage='f90'">
     <xsl:call-template name="GetMyPath">
       <xsl:with-param name="ModelName" select="name"/>
       <xsl:with-param name="models" select="$models"/>
     </xsl:call-template>
</xsl:when>
    <xsl:when test="$modelLanguage='python'">
      <xsl:text>.</xsl:text>
    </xsl:when>
-->
    <xsl:when test="$PathToCoupledDoc and not($modelLanguage='python')">
      <xsl:text>${PathToCoupledDoc}</xsl:text>
      <xsl:value-of select="$FileSep"/>
      <xsl:call-template name="GetMyPath">
        <xsl:with-param name="ModelName" select="name"/>
        <xsl:with-param name="models" select="$models"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>.</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="$newline"/>
</xsl:for-each>

<xsl:text>#</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>all:</xsl:text>

<!-- name each executable that we plan to compile -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:if test="$DUIndex=position() or $DUIndex='false'">
    <xsl:text> exe</xsl:text>
    <xsl:value-of select="position()"/>
  </xsl:if>
</xsl:for-each>

<xsl:value-of select="$newline"/>

<!-- create link line for each executable -->
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:text>exe</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>:</xsl:text>

  <xsl:variable name="ProgramCompliant">
  <xsl:if test="count(.//model)=1">
    <xsl:variable name="modelName" select=".//model/@name"/>
    <xsl:if test="count(document($root//model)/definition[name=$modelName]//entryPoint)=1">
      <xsl:if test="document($root//model)/definition[name=$modelName]//entryPoint[@type='program']">
        <xsl:text>true</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:if>
  </xsl:variable>

  <xsl:call-template name="ObjectList">
    <xsl:with-param name="models" select="$models"/>
    <xsl:with-param name="InPlaceRequired" select="$InPlaceRequired"/>
    <xsl:with-param name="ProgramCompliant" select="$ProgramCompliant"/>
    <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>

  <xsl:value-of select="$newline"/>

  <xsl:choose>
    <xsl:when test="$ProgramCompliant='true'">
      <!-- link using the language of the model -->
      <xsl:variable name="modelName" select=".//model/@name"/>
      <xsl:variable name="modelLanguage" select="document($root//model)/definition[name=$modelName]/language"/>
      <xsl:choose>
        <xsl:when test="$modelLanguage='f90'">
          <xsl:text>	${F90} ${F90FLAGS}</xsl:text>
        </xsl:when>
        <xsl:when test="$modelLanguage='f77'">
          <xsl:text>	${F77} ${F77FLAGS}</xsl:text>
        </xsl:when>
        <xsl:when test="$modelLanguage='c'">
          <xsl:text>	${CC} ${CFLAGS}</xsl:text>
        </xsl:when>
        <xsl:when test="$modelLanguage='python'">
          <xsl:text>	${CC} ${PYFLAGS} ${CFLAGS}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="true"><xsl:text>Error: unsupported language found</xsl:text></xsl:message>
        </xsl:otherwise>
      </xsl:choose>


    </xsl:when>
    <xsl:otherwise>
      <xsl:text>	${F90} ${F90FLAGS}</xsl:text>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:text> -o exe</xsl:text>
  <xsl:value-of select="position()"/>

  <xsl:call-template name="ObjectList">
    <xsl:with-param name="models" select="$models"/>
    <xsl:with-param name="InPlaceRequired" select="$InPlaceRequired"/>
    <xsl:with-param name="ProgramCompliant" select="$ProgramCompliant"/>
    <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>

      <xsl:variable name="modelName" select=".//model/@name"/>
      <xsl:variable name="modelLanguage" select="document($root//model)/definition[name=$modelName]/language"/>
      <xsl:choose>
        <xsl:when test="$modelLanguage='f90'">
          <xsl:text> ${LDFLAGS_F90} ${LDFLAGS_CC}</xsl:text>
          <!--
	  <xsl:if test="XXX">
          -->
            <!-- one of my fields is primed using netcdf -->
<!--            <xsl:text> ${LDFLAGS_NETCDF}</xsl:text> -->
          <!--
          </xsl:if>
          -->
        </xsl:when>
        <xsl:when test="$modelLanguage='f77'">
          <xsl:text> ${LDFLAGS_F77} ${LDFLAGS_CC}</xsl:text>
        </xsl:when>
        <xsl:when test="$modelLanguage='c'">
          <xsl:text> ${LDFLAGS_CC}</xsl:text>
          <xsl:if test="$ProgramCompliant='true'">
            <xsl:text> ${LDFLAGS_C2F}</xsl:text>
          </xsl:if>
        </xsl:when>
        <xsl:when test="$modelLanguage='python'">
          <xsl:text> ${LDFLAGS_CC}</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="true"><xsl:text>Error: unsupported language found</xsl:text></xsl:message>
        </xsl:otherwise>
      </xsl:choose>

  <xsl:value-of select="$newline"/>

</xsl:for-each>

<!-- generate the source to object rules explicitly -->
<xsl:for-each select="document(//deployment)//deploymentUnit">


  <!-- preceding-sibling test is make sure we only generate a rule for one instance -->
  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">


    <!-- what is the current model name? -->
    <xsl:variable name="currName" select="@name"/>

    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="language" select="document($root//model)/definition[name=$currName]/language"/>

    <!-- generate rule for this model code -->
    <xsl:call-template name="SourceToObjectRule">
      <xsl:with-param name="root" select="$moduleName"/>
      <xsl:with-param name="language" select="$language"/>
      <xsl:with-param name="addPath" select="'true'"/>
    </xsl:call-template>

    <xsl:if test="$target='esmf'">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat($moduleName,'_esmf')"/>
        <xsl:with-param name="language" select="$language"/>
        <xsl:with-param name="addPath" select="'false'"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:for-each>

  <xsl:variable name="language">
    <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">
      <xsl:variable name="currName" select="@name"/>
      <xsl:value-of select="document($root//model)/definition[name=$currName]/language"/>
    </xsl:for-each>
  </xsl:variable>

  <xsl:if test="$InPlaceRequired='true'">

    <xsl:call-template name="SourceToObjectRule">
      <xsl:with-param name="root" select="concat('BFG2InPlace',position())"/>
    </xsl:call-template>

    <xsl:if test="contains($language,'f77')">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2InPlace',position(),'_f77wrapper')"/>
      </xsl:call-template>
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2InPlace',position(),'_f90wrapper')"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="contains($language,'c')">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2InPlace',position(),'_cwrapper')"/>
        <xsl:with-param name="language" select="'c'"/>
      </xsl:call-template>
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2InPlace',position(),'_f90wrapper')"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:if>

  <xsl:variable name="ProgramCompliant">
  <xsl:if test="count(.//model)=1">
    <xsl:variable name="modelName" select=".//model/@name"/>
    <xsl:if test="count(document($root//model)/definition[name=$modelName]//entryPoint)=1">
      <xsl:if test="document($root//model)/definition[name=$modelName]//entryPoint[@type='program']">
        <xsl:text>true</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:if>
  </xsl:variable>

  <xsl:choose>

  <xsl:when test="$ProgramCompliant='true'">

    <xsl:if test="contains($language,'c')">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2ProgramConformance',position(),'_cwrapper')"/>
	<xsl:with-param name="language" select="'c'"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="contains($language,'c') or contains($language,'f77')">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2ProgramConformance',position(),'_f77wrapper')"/>
      </xsl:call-template>
    </xsl:if>

  </xsl:when>

  <xsl:otherwise>
    <xsl:if test="contains($language,'c')">
      <xsl:call-template name="SourceToObjectRule">
        <xsl:with-param name="root" select="concat('BFG2Program',position(),'_cwrapper')"/>
	<xsl:with-param name="language" select="'c'"/>
      </xsl:call-template>
    </xsl:if>

    <xsl:call-template name="SourceToObjectRule">
      <xsl:with-param name="root" select="concat('BFG2Main',position())"/>
    </xsl:call-template>


  </xsl:otherwise>
  </xsl:choose>

  <xsl:call-template name="SourceToObjectRule">
    <xsl:with-param name="root" select="concat('BFG2Target',position())"/>
  </xsl:call-template>

  <xsl:call-template name="SourceToObjectRule">
    <xsl:with-param name="root" select="concat('BFG2Interface',position())"/>
  </xsl:call-template>

</xsl:for-each>

<!--
<xsl:text>pycall.o : ${BFGPYINCLUDE}/pycall.h ${BFGPYINCLUDE}/pycall.c</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>	${CC} -c ${CFLAGS} ${INCLUDE} ${BFGPYINCLUDE}/pycall.c</xsl:text>
<xsl:value-of select="$newline"/>
-->
<xsl:if test="contains($languages,'python')">
<xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.o : ${BFGPYINCLUDE}/</xsl:text>
<xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.h ${BFGPYINCLUDE}/</xsl:text>
<xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.c</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>	${CC} -c ${CFLAGS} ${PYFLAGS} ${BFGPYINCLUDE}/</xsl:text>
<xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.c</xsl:text>
<xsl:value-of select="$newline"/>
</xsl:if>

<xsl:text>clean:</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:text>	rm -f exe?</xsl:text>

<!-- preceding-sibling test is to make sure we only generate a rule for one instance -->
<xsl:for-each select="document(//deployment)//deploymentUnits//model[not(@name=preceding-sibling::model/@name)]">

  <xsl:variable name="currName" select="@name"/>
  <xsl:variable name="moduleName">
    <xsl:call-template name="ModuleName">
      <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:text> </xsl:text>
  <xsl:value-of select="$moduleName"/>
  <xsl:text>.o</xsl:text>
</xsl:for-each>

<xsl:text> BFG2InPlace?.o BFG2Target?.o BFG2Main?.o *.mod</xsl:text>
<xsl:text> </xsl:text><xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.o</xsl:text>
<xsl:value-of select="$newline"/>
<xsl:value-of select="$newline"/>

</xsl:template>

<xsl:template name="SourceToObjectRule">
<xsl:param name="root"/>
<xsl:param name="language" select="'f90'"/>
<xsl:param name="addPath" select="'false'"/>

  <xsl:variable name="fileExt">
    <xsl:text>.</xsl:text>
    <xsl:choose>
      <xsl:when test="$language='f90'">
        <xsl:text>f90</xsl:text>
      </xsl:when>
      <xsl:when test="$language='f77'">
        <xsl:text>f</xsl:text>
      </xsl:when>
      <xsl:when test="$language='c'">
        <xsl:text>c</xsl:text>
      </xsl:when>
      <xsl:when test="$language='python'">
        <xsl:text>c</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>UNSUPPORTED</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="directory">
    <xsl:if test="$addPath='true'">
      <xsl:text>${</xsl:text>
      <xsl:call-template name="VarName">
        <xsl:with-param name="ModelName" select="$root"/>
      </xsl:call-template>
      <xsl:text>}</xsl:text>
      <xsl:value-of select="$FileSep"/>
    </xsl:if>
  </xsl:variable>

  <xsl:value-of select="$root"/>
  <xsl:text>.o : </xsl:text>
  <xsl:if test="$addPath='true'">
    <xsl:value-of select="$directory"/>
  </xsl:if>
  <xsl:value-of select="$root"/>
  <xsl:value-of select="$fileExt"/>
  <xsl:value-of select="$newline"/>
  <xsl:choose>
    <xsl:when test="$language='f90'">
      <xsl:text>	${F90} -c ${F90FLAGS} </xsl:text>
    </xsl:when>
    <xsl:when test="$language='f77'">
      <xsl:text>	${F77} -c ${F77FLAGS} </xsl:text>
    </xsl:when>
    <xsl:when test="$language='c'">
      <xsl:text>	${CC} -c ${CFLAGS} </xsl:text>
    </xsl:when>
    <xsl:when test="$language='python'">
      <xsl:text>	${CC} -c ${CFLAGS} ${PYFLAGS} </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>Not implemented.</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$addPath='true'">
    <xsl:value-of select="$directory"/>
  </xsl:if>
  <xsl:value-of select="$root"/>
  <xsl:value-of select="$fileExt"/>
  <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template name="ObjectList">
  <xsl:param name="models"/>
  <xsl:param name="InPlaceRequired"/>
  <xsl:param name="ProgramCompliant"/>
  <xsl:param name="root"/>

  <!-- need to add proper name selection using module if it exists -->
  <!-- preceding-sibling test is to make sure we only generate a rule for one instance -->
  <!-- respecting dependencies of Target, InPlace_model_x, model_x, Main(needs two loops...) -->

  <xsl:variable name="target">
    <xsl:value-of select="document($root//deployment)//target"/>
  </xsl:variable>

  <!-- target must be compiled after model and esmf wrapper code for esmf target -->
  <xsl:if test="not($target='esmf')">
    <xsl:text> BFG2Target</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.o</xsl:text>
  </xsl:if>

  <xsl:variable name="languages">
    <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">
      <xsl:variable name="currName" select="@name"/>
      <xsl:value-of select="document($root//model)/definition[name=$currName]/language"/>
    </xsl:for-each>
  </xsl:variable>

  <xsl:if test="$InPlaceRequired='true'">

    <xsl:text> BFG2InPlace</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.o</xsl:text>

  </xsl:if>

  <xsl:text> BFG2Interface</xsl:text>
  <xsl:value-of select="position()"/>
  <xsl:text>.o</xsl:text>

  <xsl:if test="$InPlaceRequired='true'">

    <xsl:if test="contains($languages,'f77')">
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f77wrapper.o</xsl:text>
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f90wrapper.o</xsl:text>
    </xsl:if>

    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.o</xsl:text>
      <xsl:text> BFG2InPlace</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f90wrapper.o</xsl:text>
    </xsl:if>

  </xsl:if>

  <xsl:choose>
  <xsl:when test="$ProgramCompliant='true'">
    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2ProgramConformance</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.o</xsl:text>
    </xsl:if>
    <xsl:if test="contains($languages,'c') or contains($languages,'f77')">
      <xsl:text> BFG2ProgramConformance</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_f77wrapper.o</xsl:text>
    </xsl:if>
  </xsl:when>
  <xsl:otherwise>
    <xsl:if test="contains($languages,'c')">
      <xsl:text> BFG2Control</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>_cwrapper.o</xsl:text>
    </xsl:if>
  </xsl:otherwise>
  </xsl:choose>

  <xsl:for-each select=".//model[not(@name=preceding-sibling::model/@name)]">

    <xsl:variable name="currName" select="@name"/>
    <xsl:variable name="moduleName">
      <xsl:call-template name="ModuleName">
        <xsl:with-param name="modelRoot" select="document($models)[definition/name=$currName]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:text> </xsl:text>
    <xsl:value-of select="$moduleName"/>
    <xsl:text>.o</xsl:text>

    <xsl:if test="$target='esmf'">
      <xsl:text> </xsl:text>
      <xsl:value-of select="$moduleName"/>
      <xsl:text>_esmf.o</xsl:text>
    </xsl:if>


  </xsl:for-each>

  <xsl:variable name="ProgramCompliant">
  <xsl:if test="count(.//model)=1">
    <xsl:variable name="modelName" select=".//model/@name"/>
    <xsl:if test="count(document($root//model)/definition[name=$modelName]//entryPoint)=1">
      <xsl:if test="document($root//model)/definition[name=$modelName]//entryPoint[@type='program']">
        <xsl:text>true</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:if>
  </xsl:variable>

  <xsl:if test="$target='esmf'">
    <xsl:text> BFG2Target</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.o</xsl:text>
  </xsl:if>

  <xsl:if test="not($ProgramCompliant='true')">
    <xsl:text> BFG2Main</xsl:text>
    <xsl:value-of select="position()"/>
    <xsl:text>.o</xsl:text>
  </xsl:if>

    <xsl:if test="contains($languages,'python')">
      <xsl:text> </xsl:text>  <!-- space -->
      <xsl:value-of select="$BFGPYOBJECT"/><xsl:text>.o</xsl:text>
    </xsl:if>

</xsl:template>

<xsl:template name="IsInPlaceRequired">
  <!-- <xsl:param name="modelName"/> -->
  <xsl:param name="root"/>

  <!-- inplace calls mean we generate an inplace routine or multiple
       su's mean we generate an inplace routine **** actually we
       should really have a coupling between su's too ****
   -->
<!--
  <xsl:if test="document($root//model)/definition[name=$modelName]//data[@form='inplace'] or count(document($root//deployment)/deployment//sequenceUnit)&gt;1">
-->
  <!-- RF: added a simple test to see if there are any p2p connections or primings too -->
  <!-- Full check should use same logic as code generation which will be
       done when we change Makefile generation to simple template -->
  <!-- added test to see if we have any put's as these always require inplace regardless as we have
       to cater for the case of dangling puts -->
  <xsl:if test="(document($root//model)/definition//data[@form='inplace'] or count(document($root//deployment)/deployment//sequenceUnit)&gt;1) and (document($root//composition)//connection or document($root//composition)//primed) or document($root//model)/definition//data[@form='inplace']//scalar[@direction='out']">
    <xsl:text>true</xsl:text>
  </xsl:if>

</xsl:template>

<!-- Provide a private variable name for each model -->
<xsl:template name="VarName">
  <xsl:param name="ModelName"/>

  <xsl:value-of select="$ModelName"/>
  <xsl:text>MODEL</xsl:text>

</xsl:template>

<xsl:template name="GetMyPath">
  <xsl:param name="ModelName"/>
  <xsl:param name="models"/>

  <xsl:for-each select="$models">
    <xsl:if test="document(.)/definition[name=$ModelName]">
      <xsl:call-template name="ExtractPath">
        <xsl:with-param name="XMLDocLocation" select="."/>
      </xsl:call-template>
    </xsl:if>
  </xsl:for-each>

</xsl:template>

<xsl:template name="ExtractPath">
  <xsl:param name="XMLDocLocation"/>

  <xsl:value-of select="substring-before($XMLDocLocation,$FileSep)"/>

  <xsl:if test="substring-after($XMLDocLocation,$FileSep)">

    <!-- do a second test to make sure we don't leave a trailing FileSep on our path -->
    <!-- do this test afterwards as we may get errors if we try to do substring on null??? -->
    <xsl:if test="substring-after(substring-after($XMLDocLocation,$FileSep),$FileSep)">

      <xsl:value-of select="$FileSep"/>

      <xsl:call-template name="ExtractPath">
        <xsl:with-param name="XMLDocLocation" select="substring-after($XMLDocLocation,$FileSep)"/>
      </xsl:call-template>

    </xsl:if>

  </xsl:if>

</xsl:template>

<xsl:include href="../Utils/ModuleName.xsl"/>

</xsl:stylesheet>
