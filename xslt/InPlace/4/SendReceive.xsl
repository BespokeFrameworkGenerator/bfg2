<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
xmlns:str="http://exslt.org/strings"
  extension-element-prefixes="xalan str">

<xsl:param name="outRoutine" select="'put'"/>
<xsl:param name="inRoutine" select="'get'"/>

<!-- IRH MOD: Now included via targets.xsl (which includes
oasis4.xsl, which in turn includes InSequenceContextID.xsl).
TODO: is there a way to prevent to multiple includes in XSL? -->
<!-- <xsl:include href="../../Utils/InSequenceContextID.xsl"/> -->
<xsl:include href="../../Utils/Loops.xsl"/>
<xsl:include href="../../Utils/sorting.xsl"/>
<xsl:include href="../../Utils/targets.xsl"/>
<!-- IRH MOD: Missing include added. -->
<xsl:include href="../../Main/4/GenModelEPid.xsl"/>


<xsl:template match="EP[@type='entryPoint']/Includes">
 <Includes>
  <xsl:apply-templates/>
  <xsl:call-template name="targetIncludes"/>
 </Includes>
</xsl:template>

<!-- start simpler selection to avoid cygwin python libxslt errors
     my version of cygwin fails here with
     "Failed to compile predicate"
<xsl:template match="DataDecs[contains(../@name,$inRoutine) or 
              contains(../@name,$outRoutine)]">
-->
<xsl:template match="DataDecs">
<xsl:choose>
<xsl:when test="contains(../@name,$inRoutine) or contains(../@name,$outRoutine)">
<!-- end simpler selection to avoid cygwin python libxslt errors -->
 <DataDecs>
  <xsl:call-template name="targetDecs"/>
  <declare name="mysize" datatype="integer" dimension="0"/>
  <declare name="myID" datatype="integer" dimension="0"/>
  <declare name="myDU" datatype="integer" dimension="0"/>
  <declare name="remoteID" datatype="integer" dimension="0"/>
  <declare name="remoteDU" datatype="integer" dimension="0"/>
  <declare name="du" datatype="integer" dimension="0"/>
  <declare name="setSize" datatype="integer" dimension="0"/>
  <declare name="newSize" datatype="integer" dimension="0"/>
  <declare name="current" datatype="integer" dimension="0"/>
  <declare name="its" datatype="integer" dimension="0"/>
  <!-- copy any existing data declarations across -->
  <xsl:apply-templates/>
  <xsl:for-each select="document($root/coupled/models/model)/definition//data[number(@dimension)>0]">
   <xsl:sort select="@dimension" order="ascending"/>
   <xsl:if test="position()=last()">
    <xsl:for-each select="dim">
     <declare name="i{position()}" datatype="integer" dimension="0"/>
    </xsl:for-each>
   </xsl:if>
  </xsl:for-each>
  <declare name="set" allocatable="true" datatype="integer" dimension="1">
   <dim order="1">
    <upper>
     <variable name="setSize"/>
    </upper>
   </dim>
  </declare>
  <declare name="newset" allocatable="true" datatype="integer" dimension="1">
   <dim order="1">
    <upper>
     <variable name="setSize"/>
    </upper>
   </dim>
  </declare>
  <declare name="ins" allocatable="true" datatype="integer" dimension="1">
   <dim order="1">
    <upper>
     <variable name="setSize"/>
    </upper>
   </dim>
  </declare>
  <declare name="outs" allocatable="true" datatype="integer" dimension="1">
   <dim order="1">
    <upper>
     <variable name="setSize"/>
    </upper>
   </dim>
  </declare>
  <declare name="pDU" datatype="integer" dimension="0"/>
  <xsl:if test="contains(../@name,$inRoutine)">
   <xsl:variable name="primedP2P">
    <xsl:text>(/</xsl:text>
    <xsl:for-each select="..//connection">
     <xsl:variable name="modName" select="@modelName"/>
     <xsl:variable name="epName" select="@epName"/>
     <xsl:variable name="id" select="@id"/>
     <xsl:variable name="instance" select="@instance"/>
     <xsl:choose>
      <xsl:when test="document($root/coupled/composition)/composition/connections/priming/model[@name=$modName]/entryPoint[@name=$epName and (not(@instance) or $instance=@instance)]/primed[@id=$id]">
       <xsl:text>1</xsl:text>
      </xsl:when>
      <xsl:otherwise>
       <xsl:text>0</xsl:text>
      </xsl:otherwise>
     </xsl:choose>
     <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
     </xsl:if>
    </xsl:for-each>
    <xsl:text>/)</xsl:text>
   </xsl:variable>
   <declare name="pp2p" datatype="integer" dimension="1" value="{$primedP2P}">
    <dim order="1">
     <upper>
      <value><xsl:value-of select="count(..//connection)"/></value>
     </upper>
    </dim>
   </declare>
  </xsl:if>
 </DataDecs>
<!-- start simpler selection to avoid cygwin python libxslt errors -->
</xsl:when>
<xsl:otherwise>
<DataDecs>
  <xsl:apply-templates/>
</DataDecs>
</xsl:otherwise>
</xsl:choose>
<!-- end simpler selection for cygwin python libxslt errors -->
</xsl:template>


<!--need to know when to receive from a remote source-->

<!-- start simpler selection to avoid cygwin python libxslt errors
     my version of cygwin fails here with
     "Failed to compile predicate"
<xsl:template match="connection[contains(../../../../../../@name,$inRoutine)]">
-->
 <xsl:template match="connection">
 <xsl:choose>
 <!-- is this an "in" connection? -->
 <xsl:when test="contains(../../../../../../@name,$inRoutine)">
<!-- end simpler selection to avoid cygwin python libxslt errors -->
 <xsl:variable name="modelName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="id" select="@id"/>
 <xsl:variable name="commsForm" select="@form"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="tag" select="substring-after(../../TagInfo/@id,'-')"/>
 <xsl:variable name="lang" select="document($root/coupled/models/model)/definition[name=$modelName]/language"/>
 <xsl:variable name="me" select="generate-id(.)"/>
 <xsl:variable name="type" select="@type"/>
 <xsl:variable name="myPos">
  <xsl:for-each select="../../../../..//connection">
   <xsl:if test="generate-id(.)=$me">
    <xsl:value-of select="position()"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="myID">
  <xsl:call-template name="InSequenceContextID">
   <xsl:with-param name="modelName" select="@modelName"/>
   <xsl:with-param name="epName" select="@epName"/>
   <xsl:with-param name="instance" select="@instance"/>
  </xsl:call-template>
 </xsl:variable>
 <comment> I am <xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$instance"/>.<xsl:value-of select="$myID"/></comment>

 <!--
 <xsl:message terminate="no">
  <xsl:text>I am input: </xsl:text><xsl:value-of select="$modelName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$epName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$instance"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$id"/>
  <xsl:text>.sched_ep_id.</xsl:text><xsl:value-of select="$myID"/><xsl:value-of select="$newline"/>  
 </xsl:message>
 -->

 <!--output model IDs of connected set elements-->
 <xsl:variable name="set">
  <xsl:for-each select="document($root/coupled/composition)
                /composition//set/field[@modelName=$modelName and 
                @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/../
                field[@modelName!=$modelName or @epName!=$epName or @id!=$id or @instance!=$instance]">
   <xsl:variable name="testMod" select="@modelName"/>
   <xsl:variable name="testEP" select="@epName"/>
   <xsl:variable name="testID" select="@id"/>
   <xsl:variable name="testInst" select="@instance"/>

   <!--
   <xsl:message terminate="no">
    <xsl:text>Field in the input's set: </xsl:text><xsl:value-of select="$testMod"/>
    <xsl:text>.</xsl:text><xsl:value-of select="$testEP"/>
    <xsl:text>.</xsl:text><xsl:value-of select="$testInst"/>
    <xsl:text>.</xsl:text><xsl:value-of select="$testID"/><xsl:value-of select="$newline"/> 
   </xsl:message>
   -->

   <xsl:choose>
    <!--When we meet an intrinsic model, don't add the model id to the list, instead...-->
    <xsl:when test="not(document($root/coupled/models/model)/definition[name=$testMod]/language='intrinsic')">
    <xsl:if test="contains(document($root/coupled/models/model)/definition[normalize-space(name)=$testMod]//entryPoint[@name=$testEP]/data[@id=$testID]/@direction,'out')">
     <xsl:text>,</xsl:text>
     <xsl:call-template name="InSequenceContextID">
      <xsl:with-param name="modelName" select="$testMod"/>
      <xsl:with-param name="epName" select="$testEP"/>
      <xsl:with-param name="instance" select="$testInst"/>
     </xsl:call-template>
    </xsl:if>
    </xsl:when>
    <!--...instead, if the direction of the field is 'out', look into the set of the 'in' field that belongs to the 
        same intrinsic model instance and add to the list each of the non-intrinsic models in that set-->
    <xsl:otherwise>
     <xsl:if test="document($root/coupled/models/model)/definition[name=$testMod]/entryPoints[1]/entryPoint[@name=$testEP]/data[@id=$testID]/@direction='out'">
      <xsl:variable name="intrinsicInID" select="document($root/coupled/models/model)
                                         /definition[name=$testMod]/entryPoints[1]/entryPoint[@name=$testEP]/data[@direction='in']/@id"/>
      <xsl:for-each select="document($root/coupled/composition)
                /composition//set/field[@modelName=$testMod and 
                @epName=$testEP and @id=$intrinsicInID and (not(@instance) or @instance=$testInst)]/../
                field[@modelName!=$testMod or @epName!=$testEP or @id!=$intrinsicInID or @instance!=$testInst]">
       <xsl:if test="contains(document($root/coupled/models/model)/definition[normalize-space(name)=current()/@modelName]
                    //entryPoint[@name=current()/@epName]/data[@id=current()/@id]/@direction,'out')">
        <xsl:text>,</xsl:text>
        <xsl:variable name="test2Mod" select="@modelName"/>
        <xsl:variable name="test2EP" select="@epName"/>
        <xsl:variable name="test2Inst" select="@instance"/>
        <xsl:call-template name="InSequenceContextID">
         <xsl:with-param name="modelName" select="$test2Mod"/>
         <xsl:with-param name="epName" select="$test2EP"/>
         <xsl:with-param name="instance" select="$test2Inst"/>
        </xsl:call-template>
       </xsl:if>
      </xsl:for-each>
     </xsl:if>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:for-each>
  <xsl:value-of select="concat(',',$myID)"/>
 </xsl:variable>
 <!--model.ep ids are assigned based on positions in schedule; sort 
     the set of connected model.ep ids by schedule position-->
 <xsl:variable name="sortedSet">
  <xsl:call-template name="sort">
   <xsl:with-param name="list" select="substring-after($set,',')"/>
  </xsl:call-template>
 </xsl:variable>

 <!--
 <xsl:message terminate="no">
  <xsl:text>Outputs for input: </xsl:text><xsl:value-of select="$modelName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$epName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$instance"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$id"/>
  <xsl:text>.sched_ep_id.</xsl:text><xsl:value-of select="$myID"/><xsl:value-of select="$newline"/>  
  <xsl:text> have sched_ep_ids: set=</xsl:text><xsl:value-of select="$set"/>
  <xsl:text> sortedSet=</xsl:text><xsl:value-of select="$sortedSet"/><xsl:value-of select="$newline"/>   
 </xsl:message>
 -->

 <!--model & ep name of model that primes this receive-->
 <xsl:variable name="pModel" select="document($root/coupled/composition)
               /composition//priming/model[@name=$modelName]
               /entryPoint[@name=$epName and (not($instance) or @instance=$instance)]/primed[@id=$id]/model/@name"/>
 <xsl:variable name="pEP" select="document($root/coupled/composition)
               /composition//priming/model[@name=$modelName]
               /entryPoint[@name=$epName and (not($instance) or @instance=$instance)]/primed[@id=$id]/model/@entryPoint"/>

 <!--model & ep name of model that has a p2p coupling to this receive-->
 <xsl:variable name="cModel" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@inModel=$modelName]
               /epChannel[@inEP=$epName]/../@outModel"/>
 <xsl:variable name="cEP" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@inModel=$modelName]
               /epChannel[@inEP=$epName]/@outEP"/>
 <xsl:variable name="cID" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@inModel=$modelName]
               /epChannel[@inEP=$epName]/connection[@inID=$id]/@outID"/>
 <xsl:variable name="sameSU" select="boolean(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[model/@name=$modelName and model/@name=$cModel])"/>

 <!--determine who to receive from for set notation connections -->

 <!--
 <xsl:message terminate="no">
  <xsl:text> (r) list = </xsl:text><xsl:value-of select="$sortedSet"/>
 </xsl:message>
 -->

 <xsl:variable name="commas" select="translate($sortedSet,',1234567890',',')"/>
 <xsl:variable name="numConnections" select="string-length($commas)+1"/>

 <!--
 <xsl:message terminate="no">
  <xsl:text> numConnections = </xsl:text><xsl:value-of select="$numConnections"/>
 </xsl:message>
 -->
 
 <xsl:choose>
  <xsl:when test="$cModel and $pModel">
  <comment>
  <xsl:text>I am primed and coupled using point to point notation</xsl:text>
  </comment>
  <xsl:message terminate="yes"><xsl:text>priming and coupling with p2p notation is not yet implemented, sorry</xsl:text></xsl:message>
  </xsl:when>
  <xsl:when test="$pModel">
  <comment>
  <xsl:text>I am primed but not coupled</xsl:text>
  </comment>
  <xsl:message terminate="yes"><xsl:text>priming of uncoupled models is not yet implemented, sorry</xsl:text></xsl:message>
  </xsl:when>
  <xsl:when test="$cModel">
  <comment>
  <xsl:text>I am coupled using point to point notation</xsl:text>
  </comment>
   <xsl:choose>
    <xsl:when test="$sameSU">
      <comment>
      <xsl:text>to something on the same sequence unit as me</xsl:text>
      </comment>
      <comment>
      <xsl:text>Use shared buffers for comms</xsl:text>
      </comment>
    </xsl:when>
    <xsl:otherwise>
      <comment>
      <xsl:text>to something on a different sequence unit to me</xsl:text>
      </comment>
      <comment>
      <xsl:text>Use requested target for comms</xsl:text>
      </comment>
      <xsl:variable name="remotesuid">
        <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
          <xsl:if test="model/@name=$cModel">
            <xsl:value-of select="position()"/>
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <comment><xsl:text>sending model: name=</xsl:text><xsl:value-of select="$cModel"/><xsl:text> ep=</xsl:text><xsl:value-of select="$cEP"/><xsl:text> id=</xsl:text><xsl:value-of select="$cID"/></comment>
      <assign lhs="du" rhs="b2mmap({$remotesuid})"/>
      <receive modelName="{@modelName}" epName="{@epName}" id="{@id}" language="{$lang}" form="{$commsForm}" type="{$type}" tag="{$tag}"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="$pModel">
   <assign lhs="myID" rhs="{$myID}"/>
   <assign lhs="myDU" rhs="info(myID)%du"/>
   <if>
    <condition>
     <xsl:choose>
      <xsl:when test="document($root/coupled/deployment)/deployment/schedule//init/model[@name=$modelName and @ep=$epName and (not(@instance) or @instance=$instance)]">
       <equals>
        <value>its1</value>
        <value>0</value>
       </equals>
      </xsl:when>
      <xsl:when test="document($root/coupled/deployment)/deployment/schedule//iterate/loop/model[@name=$modelName and @ep=$epName and (not(@instance) or @instance=$instance)]">
       <equals>
        <value>its1</value>
        <xsl:variable name="ts">
         <xsl:choose>
          <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]/timestep">
           <xsl:value-of select="document($root/coupled/models/model)/definition[name=$modelName]/timestep"/>
          </xsl:when>
          <xsl:otherwise>
           <xsl:text>1</xsl:text>
          </xsl:otherwise>
         </xsl:choose>
        </xsl:variable>
        <value><xsl:value-of select="$ts"/></value>
       </equals>
      </xsl:when>
      <xsl:when test="document($root/coupled/deployment)/deployment/schedule//iterate//loop/model[@name=$modelName and @ep=$epName and (not(@instance) or @instance=$instance)]">
       <xsl:variable name="myloopcounter">
        <xsl:text>its</xsl:text>
        <xsl:call-template name="get_loop_pos"> 
         <xsl:with-param name="myloopid" select="generate-id(document($root/coupled/deployment)
                                         /deployment/schedule//iterate//loop
                                         [model[@name=$modelName and @ep=$epName and (not(@instance) or @instance=$instance)]])"/>
        </xsl:call-template>
       </xsl:variable>
       <xsl:variable name="ts">
        <xsl:choose>
         <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]/timestep">
          <xsl:value-of select="document($root/coupled/models/model)/definition[name=$modelName]/timestep"/>
         </xsl:when>
         <xsl:otherwise>
          <xsl:text>1</xsl:text>
         </xsl:otherwise>
        </xsl:choose>
       </xsl:variable>
       <and>
        <equals>
         <value>its1</value>
         <value>1</value>
        </equals>
        <equals>
         <value><xsl:value-of select="$myloopcounter"/></value>
         <value><xsl:value-of select="$ts"/></value>
        </equals>
       </and>
      </xsl:when>
      <xsl:otherwise>
       <xsl:message terminate="yes">
        <xsl:value-of select="$newline"/>
        <xsl:text>Error in SendReceive.xsl: primed model does not run in init phase or within a loop in ts phase (1).</xsl:text>
        <xsl:value-of select="$newline"/>
       </xsl:message>
      </xsl:otherwise>
     </xsl:choose>
    </condition>
    <action>
     <xsl:variable name="pmid">
      <xsl:call-template name="getModelEPid">
       <xsl:with-param name="modelName" select="$pModel"/>
       <xsl:with-param name="epName" select="$pEP"/>
      </xsl:call-template>
     </xsl:variable>
     <assign lhs="pDU" rhs="info({$pmid})%du"/>
     <if>
      <condition>
       <notEquals>
        <value>pDU</value>
        <value>myDU</value>
       </notEquals>
      </condition>
      <action>
       <comment>receive from priming source: on different DU to receiver</comment>
       <assign lhs="du" rhs="pDU-1"/>
       <receive modelName="{@modelName}" epName="{@epName}" id="{@id}" language="{$lang}" form="{$commsForm}" type="{$type}" tag="{$tag}"/>
      </action>
     </if>
    </action>
    <xsl:if test="number($numConnections) > 1">
     <else>
      <action>
       <assign lhs="setSize" rhs="{number($numConnections)}"/>
       <allocate name="set">
        <dim order="1">
         <lower>
          <value>1</value>
         </lower>
        </dim>
        <dim order="2">
         <upper>
          <variable name="setSize"/>
         </upper>
        </dim>
       </allocate>
       <allocate name="ins">
        <dim order="1">
         <lower>
          <value>1</value>
         </lower>
        </dim>
        <dim order="2">
         <upper>
          <variable name="setSize"/>
         </upper>
        </dim>
       </allocate>
       <assign lhs="set" rhs="(/{$sortedSet}/)"/>
       <call name="getLast" type="function" return="remoteID">
        <arg name="set"/>
        <arg name="setSize"/>
        <arg name="myID"/>
       </call>
       <if>
        <condition>
         <equals>
          <value>remoteID</value>
          <value>-1</value>
         </equals>
        </condition>
        <action>
         <assign lhs="remoteID" rhs="myID"/>
        </action>
       </if>
       <assign lhs="remoteDU" rhs="info(remoteID)%du"/>
       <if>
        <condition>
         <notEquals>
          <value>remoteDU</value>
          <value>myDU</value>
         </notEquals>
        </condition>
        <action>
         <comment>receive from remote source</comment>
         <assign lhs="du" rhs="remoteDU"/>
         <receive modelName="{@modelName}" epName="{@epName}" id="{@id}" language="{$lang}" form="{$commsForm}" type="{$type}" tag="{$tag}"/>
        </action>
       </if>
      </action>
     </else>
    </xsl:if>
   </if>
  </xsl:when>
  <xsl:when test="number($numConnections) > 1">
   <assign lhs="remoteID" rhs="-1"/>
   <assign lhs="setSize" rhs="{$numConnections}"/>
   <allocate name="set">
    <dim order="1">
     <lower>
      <value>1</value>
     </lower>
    </dim>
    <dim order="2">
     <upper>
      <variable name="setSize"/>
     </upper>
    </dim>
   </allocate>
   <assign lhs="set" rhs="(/{$sortedSet}/)"/>
   <assign lhs="myID" rhs="{$myID}"/>
   <assign lhs="myDU" rhs="info(myID)%du"/>
   <if>
    <condition>
     <value>pp2p(<xsl:value-of select="$myPos"/>).eq.1</value>
    </condition>
    <action>
     <assign lhs="pp2p({$myPos})" rhs="0"/>
    </action>
    <else>
     <action>
      <call name="getLast" type="function" return="remoteID">
       <arg name="set"/>
       <arg name="setSize"/>
       <arg name="myID"/>
      </call>
     </action>
    </else>
   </if>
   <!--
   <print>
    <string><xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$id"/> receiving from </string>
    <var name="remoteID"/>
    <string> | Tag=<xsl:value-of select="$tag"/></string>
   </print>
   -->
   <if>
    <condition>
     <equals>
      <value>remoteID</value>
      <value>-1</value>
     </equals>
    </condition>
    <action>
     <assign lhs="remoteID" rhs="myID"/>
    </action>
   </if>
   <assign lhs="remoteDU" rhs="info(remoteID)%du"/>
   <if>
    <condition>
     <notEquals>
      <value>remoteDU</value>
      <value>myDU</value>
     </notEquals>
    </condition>
    <action>
     <comment>receive from remote source</comment>
     <assign lhs="du" rhs="remoteDU"/>
     <receive modelName="{@modelName}" epName="{@epName}" id="{@id}" instance="{$instance}" language="{$lang}" form="{$commsForm}" type="{$type}" tag="{$tag}"/>
    </action>
   </if>
  </xsl:when>
 </xsl:choose>
<!-- start simpler selection to avoid cygwin python libxslt errors -->
</xsl:when>
<!-- is this an "out" connection? -->
<xsl:when test="contains(../../../../../../@name,$outRoutine)">
<!-- end simpler selection to avoid cygwin python libxslt errors -->
 <xsl:variable name="modelName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="id" select="@id"/>
 <xsl:variable name="commsForm" select="@form"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="tag" select="substring-after(../../TagInfo/@id,'-')"/>
 <xsl:variable name="lang" select="document($root/coupled/models/model)/definition[name=$modelName]/language"/>
 <xsl:variable name="type" select="@type"/>

 <xsl:variable name="myID">
  <xsl:call-template name="InSequenceContextID">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:call-template>
 </xsl:variable>

 <comment> I am <xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$instance"/>.<xsl:value-of select="$myID"/></comment>

 <!--
 <xsl:message terminate="no">
  <xsl:text>I am output: </xsl:text><xsl:value-of select="$modelName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$epName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$instance"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$id"/>
  <xsl:text>.sched_ep_id.</xsl:text><xsl:value-of select="$myID"/>
 </xsl:message>
 -->

 <!--output model.ep IDs of connected SET elements specified in composition-->
 <xsl:variable name="set">
  <!-- IRH MOD: Placed code for generating list of sequence IDs
  in separate template. -->
  <xsl:call-template name="getEPDataMatchingOutput">
   <xsl:with-param name="dataToGetType" select="'SEQUENCE_ID_LIST'"/>
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="instance" select="$instance"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
  </xsl:call-template>
 </xsl:variable>

 <xsl:variable name="sortedSet">
  <xsl:call-template name="sort">
   <xsl:with-param name="list" select="substring-after($set,',')"/>
  </xsl:call-template>
 </xsl:variable>

 <xsl:variable name="ins">
  <!-- IRH MOD: Placed code for generating list of inputs
  in separate template that takes account of intrinsics. -->
  <xsl:call-template name="getEPDataMatchingOutput">
   <xsl:with-param name="dataToGetType" select="'INPUT_LIST'"/>
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="instance" select="$instance"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
  </xsl:call-template>
  <!--
  <xsl:call-template name="list-ins">
   <xsl:with-param name="list" select="$sortedSet"/>
   <xsl:with-param name="set" select="document($root/coupled/composition)/composition//set/field[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/.."/>
  </xsl:call-template>
  -->
 </xsl:variable>
 <xsl:variable name="outs">
  <!-- IRH MOD: Placed code for generating list of inputs
  in separate template that takes account of intrinsics. -->
  <xsl:call-template name="getEPDataMatchingOutput">
   <xsl:with-param name="dataToGetType" select="'OUTPUT_LIST'"/>
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="instance" select="$instance"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
  </xsl:call-template>
  <!--
  <xsl:call-template name="list-outs">
   <xsl:with-param name="list" select="$sortedSet"/>
   <xsl:with-param name="set" select="document($root/coupled/composition)/composition//set/field[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/.."/>
  </xsl:call-template>
  -->
 </xsl:variable>

 <!-- IRH MOD: Sort entry point input and output data
 in sequence unit ID order. -->
 <xsl:variable name="sortedIns">
  <xsl:call-template name="sortEPDataBySequenceID">
   <xsl:with-param name="unsortedEPData" select="$ins"/>
   <xsl:with-param name="unsortedEPSequenceIDs" select="$set"/>
   <xsl:with-param name="sortedEPSequenceIDs" select="$sortedSet"/>
  </xsl:call-template>
 </xsl:variable>

 <xsl:variable name="sortedOuts">
  <xsl:call-template name="sortEPDataBySequenceID">
   <xsl:with-param name="unsortedEPData" select="$outs"/>
   <xsl:with-param name="unsortedEPSequenceIDs" select="$set"/>
   <xsl:with-param name="sortedEPSequenceIDs" select="$sortedSet"/>
  </xsl:call-template>
 </xsl:variable>

 <!--
 <xsl:message terminate="no">
  <xsl:text>Corresponding fields for output: </xsl:text><xsl:value-of select="$modelName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$epName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$instance"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$id"/>
  <xsl:text>.sched_ep_id.</xsl:text><xsl:value-of select="$myID"/><xsl:value-of select="$newline"/>  
  <xsl:text> have sched_ep_ids: set=</xsl:text><xsl:value-of select="$set"/>
  <xsl:text> sortedSet=</xsl:text><xsl:value-of select="$sortedSet"/><xsl:value-of select="$newline"/>   
  <xsl:text> have inputs: ins=</xsl:text><xsl:value-of select="$ins"/>
  <xsl:text> sortedIns=</xsl:text><xsl:value-of select="$sortedIns"/><xsl:value-of select="$newline"/>   
  <xsl:text> have outputs: outs=</xsl:text><xsl:value-of select="$outs"/>
  <xsl:text> sortedOuts=</xsl:text><xsl:value-of select="$sortedOuts"/>
 </xsl:message>
 -->

 <xsl:variable name="commas" select="translate($sortedSet,',1234567890',',')"/>
 <xsl:variable name="numConnections" select="string-length($commas)+1"/>

 <!--
 <xsl:message terminate="no">
  <xsl:text> numConnections = </xsl:text><xsl:value-of select="$numConnections"/>
 </xsl:message>
 -->
                                     
 <xsl:variable name="pModel" select="document($root/coupled/composition)
               /composition//priming/model[
               ./entryPoint/primed/model[@name=$modelName and @entryPoint=$epName and @id=$id and (not(@instance) or @instance=$instance)]]/@name"/>
 <xsl:variable name="pEP" select="document($root/coupled/composition)
               /composition//priming/model/entryPoint[
               ./primed/model[@name=$modelName and @entryPoint=$epName and @id=$id and (not(@instance) or @instance=$instance)]]/@name"/>

 <!--model & ep name of model that has a p2p coupling to this send-->
 <xsl:variable name="cModel" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@outModel=$modelName]
               /epChannel[@outEP=$epName]/connection[@outID=$id]/../../@inModel"/>
 <xsl:variable name="cEP" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@outModel=$modelName]
               /epChannel[@outEP=$epName]/connection[@outID=$id]/../@inEP"/>
 <xsl:variable name="cID" select="document($root/coupled/composition)
               /composition//timestepping/modelChannel[@outModel=$modelName]
               /epChannel[@outEP=$epName]/connection[@outID=$id]/@inID"/>

 <xsl:variable name="sameSU" select="boolean(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[model/@name=$modelName and model/@name=$cModel])"/>

 <xsl:choose>
  <xsl:when test="$cModel and $pModel">
  <comment>
  <xsl:text>I am primed and coupled using point to point notation</xsl:text>
  </comment>
  <xsl:message terminate="yes">
    <xsl:text>Not yet implemented priming and coupling using point to point notation</xsl:text>
  </xsl:message>
  </xsl:when>
  <xsl:when test="$cModel">
  <comment>
  <xsl:text>I am coupled using point to point notation</xsl:text>
  </comment>
  <xsl:choose>
    <xsl:when test="$sameSU">
      <comment>
      <xsl:text>to something on the same sequence unit as me</xsl:text>
      </comment>
      <comment>
      <xsl:text>Use shared buffers for comms</xsl:text>
      </comment>
      <comment>
      <xsl:text>I connect to </xsl:text>
      <xsl:value-of select="count($cModel)"/>
      <xsl:text> others</xsl:text>
      </comment>
      <xsl:message terminate="yes">
        <xsl:text>Not yet implemented shared buffer comms code</xsl:text>
      </xsl:message>
    </xsl:when>
    <xsl:otherwise>
      <comment>
      <xsl:text>to something on a different sequence unit to me</xsl:text>
      </comment>
      <comment>
      <xsl:text>Use requested target for comms</xsl:text>
      </comment>
      <comment>
      <xsl:text>I connect to </xsl:text>
      <xsl:value-of select="count($cModel)"/>
      <xsl:text> others</xsl:text>
      </comment>

      <xsl:for-each select="$cModel">
<!--
***************************** sort out setting DU value, Also sort out priming of in-place when combined with coupled, also sort out mixing set and p2p notation ****************************
-->
      <xsl:variable name="currentModel" select="$cModel[position()]"/>
      <!-- check we are sequential models -->
      <xsl:if test="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[@threads!='1']/model/@name=$currentModel or document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[@threads!='1']/model/@name=$modelName">
        <xsl:message terminate="yes">Point to point comms with parallel models not currently supported, sorry</xsl:message>
      </xsl:if>
      <xsl:variable name="remotesuid">
        <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
          <xsl:if test="model/@name=$currentModel">
            <xsl:value-of select="position()"/>
          </xsl:if>
        </xsl:for-each>
      </xsl:variable>
      <assign lhs="du" rhs="b2mmap({$remotesuid})"/>
      <comment><xsl:text>receiving model: name=</xsl:text><xsl:value-of select="$cModel[position()]"/><xsl:text> ep=</xsl:text><xsl:value-of select="$cEP[position()]"/><xsl:text> id=</xsl:text><xsl:value-of select="$cID[position()]"/></comment>

<!--
      <send modelName="{$remoteModel}" epName="{$remoteEP}" id="{$remoteID}"/>
-->
<!-- I was thinking of providing from and to details for further processing rather than embedding the assumption that send provides the receive and receive provides itself so the tags match
      <send modelName={$modelName} epName={$epName} id={$id} remoteModelName="{$cModel[position()]}" remoteEPName="{$cEP[position()]}" remoteID="{$cID[position()]}"/>
-->
      <!-- embed who we send to -->
      <send modelName="{$cModel[position()]}" epName="{$cEP[position()]}" id="{$cID[position()]}" language="{$lang}" form="{$commsForm}" />
      </xsl:for-each>
    </xsl:otherwise>
  </xsl:choose>
  </xsl:when>
  <xsl:when test="$pModel and number($numConnections) > 1">
   <xsl:message terminate="yes">
    <xsl:value-of select="$newline"/>
    <xsl:text>Error in SendReceive.xsl: tuple </xsl:text>
    <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
    <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
    <xsl:value-of select="$id"/>
    <xsl:text> primes and is connected.</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:message>
  </xsl:when>
  <xsl:when test="$pModel">
   <comment><xsl:text>RF: this var primes </xsl:text><xsl:value-of select="count($pModel)"/><xsl:text> vars</xsl:text></comment>
   <assign lhs="myID" rhs="{$myID}"/>
   <assign lhs="myDU" rhs="info(myID)%du"/>

   <xsl:for-each select="document($root/coupled/composition)
               /composition//priming/model/entryPoint/primed/model[@name=$modelName and @entryPoint=$epName and @id=$id and (not(@instance) or @instance=$instance)]">

   <xsl:variable name="remoteModel" select="ancestor::model/@name"/>
   <xsl:variable name="remoteEP" select="ancestor::entryPoint/@name"/>
   <xsl:variable name="remoteID" select="ancestor::primed/@id"/>

   <comment><xsl:text>RF: Primed model </xsl:text><xsl:value-of select="$remoteModel"/><xsl:text>, ep </xsl:text><xsl:value-of select="$remoteEP"/><xsl:text> id </xsl:text><xsl:value-of select="$remoteID"/></comment>
   <xsl:variable name="pmid">
    <xsl:call-template name="getModelEPid">
     <xsl:with-param name="modelName" select="$remoteModel"/>
     <xsl:with-param name="epName" select="$remoteEP"/>
    </xsl:call-template>
   </xsl:variable>
   <assign lhs="pDU" rhs="info({$pmid})%du"/>
   <if>
    <condition>
     <notEquals>
      <value>pDU</value>
      <value>myDU</value>
     </notEquals>
    </condition>
    <action>
     <comment>sending to primed destination: on different DU to receiver</comment>
     <assign lhs="du" rhs="pDU-1"/>
     <send modelName="{$remoteModel}" epName="{$remoteEP}" id="{$remoteID}" language="{$lang}" form="{$commsForm}" type="{$type}"/>
    </action>
   </if>

   </xsl:for-each>

  </xsl:when>
  <xsl:when test="number($numConnections) > 1">
   <assign lhs="setSize" rhs="{number($numConnections)}"/>
   <allocate name="set">
    <dim order="1">
     <lower>
      <value>1</value>
     </lower>
    </dim>
    <dim order="2">
     <upper>
      <variable name="setSize"/>
     </upper>
    </dim>
   </allocate>
   <allocate name="newSet">
    <dim order="1">
     <lower>
      <value>1</value>
     </lower>
    </dim>
    <dim order="2">
     <upper>
      <variable name="setSize"/>
     </upper>
    </dim>
   </allocate>
   <allocate name="ins">
    <dim order="1">
     <lower>
      <value>1</value>
     </lower>
    </dim>
    <dim order="2">
     <upper>
      <variable name="setSize"/>
     </upper>
    </dim>
   </allocate>
   <allocate name="outs">
    <dim order="1">
     <lower>
      <value>1</value>
     </lower>
    </dim>
    <dim order="2">
     <upper>
      <variable name="setSize"/>
     </upper>
    </dim>
   </allocate>
   <assign lhs="set" rhs="(/{$sortedSet}/)"/>
   <!-- IRH MOD: Using new method for getting lists of inputs and outputs 
   that takes account of intrinsics but requires separate sorting. -->
   <assign lhs="ins" rhs="(/{$sortedIns}/)"/>
   <assign lhs="outs" rhs="(/{$sortedOuts}/)"/>
   <!--
   <assign lhs="ins" rhs="(/{$ins}/)"/>
   <assign lhs="outs" rhs="(/{$outs}/)"/>
   -->
   <assign lhs="myID" rhs="{$myID}"/>
   <assign lhs="myDU" rhs="info(myID)%du"/>
   <call name="getNext" type="function" return="remoteID">
    <arg name="set"/>
    <arg name="setSize"/>
    <arg name="myID"/>
   </call>
   <!--
   <print>
    <string><xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$id"/> sending to </string>
    <var name="remoteID"/>
    <string> | Tag=<xsl:value-of select="$tag"/></string>
   </print>
   -->
   
   <!-- IRH MOD: Initialise "current" - loop below may not set it, for 
   instance if the recipient is this process itself. -->
   <assign lhs="current" rhs="-1"/>
   <iterate start="1" end="setSize">
    <if>
     <condition>
      <equals>
       <value>set(its)</value>
       <value>remoteID</value>
      </equals>
     </condition>
     <action>
      <assign lhs="current" rhs="its"/>
     </action>
    </if>
   </iterate>
   <if>
    <condition>
     <equals>
      <value>remoteID</value>
      <value>-1</value>
     </equals>
    </condition>
    <action>
     <assign lhs="remoteID" rhs="myID"/>
    </action>
   </if>
   <assign lhs="remoteDU" rhs="info(remoteID)%du"/>

   <!-- IRH MOD: Must check "current" before using it as an array subscript - 
   unlike C, Fortran compilers give us no guarantee that the second sub-condition
   will be evaluated second (although they may still carry out lazy evaluation) -->
   <comment>Only send data if a recipient (other than this process itself) has been found</comment>
   <if>
    <condition>
     <notEquals>
      <value>current</value>
      <value>-1</value>
     </notEquals>
    </condition>

    <action>
     <if>
      <condition>
       <and>
        <notEquals>
         <value>remoteDU</value>
         <value>myDU</value>
        </notEquals>
        <notEquals>
         <value>outs(current)</value>
         <value>1</value>
        </notEquals>
       </and>
      </condition>
      <action>
       <comment>send to remote source</comment>
       <assign lhs="du" rhs="remoteDU"/>
       <send modelName="{@modelName}" epName="{@epName}" id="{@id}" instance="{$instance}" language="{$lang}" form="{$commsForm}" type="{$type}" tag="{$tag}"/>
      </action>
     </if>
    </action>
   </if><!-- current != -1 -->

   <!-- -->
  <xsl:if test="not($type='sizeref')">
   <assign lhs="newSize" rhs="setSize"/>
   <assign lhs="newSet" rhs="set"/>
<!--iterate was here-->

   <!-- IRH MOD: As with change above, initialise and check value of
   "current" before using it as an array subscript -->
   <comment>Only continue sending data while more recipients</comment>
   <comment>(other than this process itself) can be found</comment>
   <while>
    <condition>
     <notEquals>
      <value>current</value>
      <value>-1</value>
     </notEquals>
    </condition>

    <action>
     <if>
     <!--<while>-->
      <condition>
       <and>
        <and>
         <greaterThan><value>newSize</value><value>2</value></greaterThan>
         <equals><value>ins(current)</value><value>1</value></equals>
        </and>
        <notEquals><value>remoteID</value><value>myID</value></notEquals>
       </and>
      </condition>
      <action>
       <iterate start="current" end="newSize-1">
        <assign lhs="newSet(its)" rhs="newSet(its+1)"/>
        <assign lhs="ins(its)" rhs="ins(its+1)"/>
       </iterate>
       <assign lhs="newSize" rhs="newSize-1"/>
       <call name="getNext" type="function" return="remoteID">
        <arg name="newSet"/>
        <arg name="newSize"/>
        <arg name="myID"/>
       </call>
       <!--
       <print>
        <string><xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$id"/> sending to </string>
        <var name="remoteID"/>
        <string> | Tag=<xsl:value-of select="$tag"/></string>
       </print>
       -->
       <if>
        <condition>
         <equals>
          <value>remoteID</value>
          <value>-1</value>
         </equals>
        </condition>
        <action>
         <assign lhs="remoteID" rhs="myID"/>
        </action>
       </if>
       <assign lhs="remoteDU" rhs="info(remoteID)%du"/>
       <if>
        <condition>
         <notEquals>
          <value>remoteDU</value>
          <value>myDU</value>
         </notEquals>
        </condition>
        <action>
         <comment>send to remote source</comment>
         <assign lhs="du" rhs="remoteDU"/>
         <send modelName="{@modelName}" epName="{@epName}" id="{@id}" instance="{$instance}" language="{$lang}" form="{$commsForm}" tag="{$tag}"/>
        </action>
       </if>
  
       <!-- IRH MOD: Initialise "current" - loop below may not set it, for 
       instance if the recipient is this process itself. -->
       <assign lhs="current" rhs="-1"/>
       <iterate start="1" end="newSize">
        <if>
         <condition>
          <equals>
           <value>newSet(its)</value>
           <value>remoteID</value>
          </equals>
         </condition>
         <action>
          <assign lhs="current" rhs="its"/>
         </action>
        </if>
       </iterate>
      </action>
 
      <!-- IRH MOD: Set "current" to quit the loop
      TODO: this is not pleasant - perhaps the pseudocode can be simplified? -->
      <else>
       <action>
        <comment>Found all recipients, so quit the loop</comment>
        <assign lhs="current" rhs="-1"/>
       </action>
      </else>
 
     </if><!-- newSize > 2 && ins(current) == 1 && remoteID != myID -->

    </action>
   </while><!-- current != -1 -->

  </xsl:if>
   <!-- -->
  </xsl:when>
 </xsl:choose>
<!-- start simpler selection to avoid cygwin python libxslt errors -->
</xsl:when>
<xsl:otherwise>
<DataDecs>
  <xsl:apply-templates/>
</DataDecs>
</xsl:otherwise>
</xsl:choose>
<!-- end simpler selection for cygwin python libxslt errors -->
</xsl:template>

<!-- IRH MOD: NO LONGER USED - DOESN'T TAKE ACCOUNT OF INTRINSICS. -->
<xsl:template name="list-ins">
 <xsl:param name="list"/>
 <xsl:param name="set"/>
 <xsl:variable name="pos">
  <xsl:choose>
   <xsl:when test="substring-before($list,',')">
    <xsl:value-of select="substring-before($list,',')"/>
   </xsl:when>
   <xsl:when test="number($list)">
    <xsl:value-of select="number($list)"/>
   </xsl:when>
  </xsl:choose>
 </xsl:variable>
 <xsl:variable name="final" select="boolean(number($list))"/>
 <xsl:variable name="modelName">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@name"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="epName">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@ep"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="instance">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@instance"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <!--
  This doesn't work for some reason...
 <xsl:variable name="modelName" select="document($root/coupled/deployment)//schedule//model[number($pos)]/@name"/>
 <xsl:variable name="epName" select="document($root/coupled/deployment)//schedule//model[number($pos)]/@ep"/>
 -->

 <!-- IRH MOD: Should check instance too - we might have a field in our set
 that is sent to two different instances of the same model (we do in GENIE). --> 
 <!-- <xsl:variable name="id" select="$set/field[@modelName=$modelName and @epName=$epName]/@id"/> -->
 <xsl:variable name="id" select="$set/field[
   @modelName=$modelName and
   @epName=$epName and
   (not(@instance) or @instance=$instance)]/@id"/>

 <xsl:choose>
  <xsl:when test="count($id) = 1">
   <!--
   <xsl:message terminate="no">
    <xsl:value-of select="$pos"/>
    <xsl:value-of select="$modelName"/>
    <xsl:value-of select="$epName"/>
    <xsl:value-of select="$id"/>
   </xsl:message>
   -->
   <xsl:choose>
    <!-- IRH MOD: Changed to look for inout as well as in arguments.
    The way this works is that outs() is outputs ONLY (it's just used to check
    that we're not trying to send an output to an output - outs(current).ne.1),
    whereas inputs are the opposite - the inverse set - anything that ISN'T output
    only, i.e. ins AND in-outs - we cycle through the getNext()s and check that
    the current next is an input - ins(current)==1 - before doing a put. 
    I'm not sure why this hasn't caused a problem up till now - where multiple
    destinations occur in GENIE configs, a quick look shows no outputs going
    to inouts, they all happen to go to ins (by design or luck, I don't know).
    THIS DOESN'T WORK - I should leave it as it is, delving into getNext()
    to figure out exactly what's going on is not at all pleasant. -->
    <xsl:when test="contains(document($root/coupled/models/model)
      /definition[normalize-space(name)=$modelName]
      //entryPoint[@name=$epName]
      /data[@id=$id]/@direction, 'in')">
    <!-- <xsl:when test="document($root/coupled/models/model)/definition[normalize-space(name)=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@direction='in'"> -->
     <!--
     <xsl:message terminate="no">
      <xsl:text>in</xsl:text>
     </xsl:message>
     -->
     <xsl:text>1</xsl:text><xsl:if test="not($final)"><xsl:text>,</xsl:text></xsl:if>
    </xsl:when>
    <xsl:otherwise>
     <!--
     <xsl:message terminate="no">
      <xsl:text>not in</xsl:text>
     </xsl:message>
     -->
     <xsl:text>0</xsl:text><xsl:if test="not($final)"><xsl:text>,</xsl:text></xsl:if>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="count($id) = 0">
   <!-- IRH MOD: If we can't find the argument ID in the set of the output itself
   then the argument ID *must* be for a field in another set reached via an intrinsic
   transformation.  Model entry points only get added to the list if they're
   1) either in the same set as the output; 2) reached via an intrinsic, in which
   case we already know that they're an 'in' or 'inout' as we checked this when 
   adding the entry point to the list (see how the "list" param was calculated in the
   calling template).  Therefore we can write a '1' to ins() without further checking.
   THIS DOESN'T WORK - only 'in' should be 1 here, if we're to remain consistent
   with the way non-intrinsics are handled. -->
   <xsl:text>1</xsl:text><xsl:if test="not($final)"><xsl:text>,</xsl:text></xsl:if>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: more than one argument ID for this model entry point instance in composition set!</xsl:text>
    <!-- This is technically possible but unlikely.  The design of the models
    would need to pretty odd to have identical data passed as two separate arguments
    to the same entry point.  Therefore I'm assuming this case won't occur - in fact
    if it does, it will undoubtedly cause trouble elsewhere (e.g. oasis4.xsl). -->
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>

 <xsl:if test="substring-after($list,',')">
  <!--
  <xsl:message terminate="no">
   <xsl:text>list is now:</xsl:text><xsl:value-of select="substring-after($list,',')"/>
  </xsl:message>
  -->
  <xsl:call-template name="list-ins">
   <xsl:with-param name="list" select="substring-after($list,',')"/>
   <xsl:with-param name="set" select="$set"/>
  </xsl:call-template>
 </xsl:if>
</xsl:template>

<!-- IRH MOD: NO LONGER USED - DOESN'T TAKE ACCOUNT OF INTRINSICS. -->
<xsl:template name="list-outs">
 <xsl:param name="list"/>
 <xsl:param name="set"/>
 <xsl:variable name="pos">
  <xsl:choose>
   <xsl:when test="substring-before($list,',')">
    <xsl:value-of select="substring-before($list,',')"/>
   </xsl:when>
   <xsl:when test="number($list)">
    <xsl:value-of select="number($list)"/>
   </xsl:when>
  </xsl:choose>
 </xsl:variable>
 <xsl:variable name="final" select="boolean(number($list))"/>
 <xsl:variable name="modelName">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@name"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="epName">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@ep"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="instance">
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:if test="number($pos)=position()">
    <xsl:value-of select="@instance"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <!--
  This doesn't work for some reason...
 <xsl:variable name="modelName" select="document($root/coupled/deployment)//schedule//model[number($pos)]/@name"/>
 <xsl:variable name="epName" select="document($root/coupled/deployment)//schedule//model[number($pos)]/@ep"/>
 -->

 <!-- IRH MOD: Should check instance too - we might have a field in our set
 that is received from two different instances of the same model. --> 
 <!-- <xsl:variable name="id" select="$set/field[@modelName=$modelName and @epName=$epName]/@id"/> -->
 <xsl:variable name="id" select="$set/field[
   @modelName=$modelName and
   @epName=$epName and
   (not(@instance) or @instance=$instance)]/@id"/>

 <!--
 <xsl:message terminate="no">
  <xsl:value-of select="$pos"/>
  <xsl:value-of select="$modelName"/>
  <xsl:value-of select="$epName"/>
  <xsl:value-of select="$id"/>
 </xsl:message>
 -->
 <xsl:choose>
  <xsl:when test="document($root/coupled/models/model)/definition[normalize-space(name)=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@direction='out'">
   <!--
   <xsl:message terminate="no">
    <xsl:text>in</xsl:text>
   </xsl:message>
    -->
   <xsl:text>1</xsl:text><xsl:if test="not($final)"><xsl:text>,</xsl:text></xsl:if>
  </xsl:when>
  <xsl:otherwise>
   <!--
   <xsl:message terminate="no">
    <xsl:text>not in</xsl:text>
   </xsl:message>
   -->
   <xsl:text>0</xsl:text><xsl:if test="not($final)"><xsl:text>,</xsl:text></xsl:if>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:if test="substring-after($list,',')">
  <!--
  <xsl:message terminate="no">
   <xsl:text>list is now:</xsl:text><xsl:value-of select="substring-after($list,',')"/>
  </xsl:message>
  -->
  <xsl:call-template name="list-outs">
   <xsl:with-param name="list" select="substring-after($list,',')"/>
   <xsl:with-param name="set" select="$set"/>
  </xsl:call-template>
 </xsl:if>
</xsl:template>


<!-- IRH MOD: New template that finds all fields matching an output field, 
including those in different sets reached by intrinsic transformations,
and gets a certain type of data associated with those fields (e.g.
sequence number of the entry point instance in the deployment,
whether the field is 'in' only or 'out' only). -->
<xsl:template name="getEPDataMatchingOutput">
 <xsl:param name="dataToGetType"/>
 <xsl:param name="modelName"/>
 <xsl:param name="instance"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>

 <!--
 <xsl:message terminate="no">
  <xsl:value-of select="$newline"/>   
  <xsl:text>Getting data: </xsl:text><xsl:value-of select="$dataToGetType"/>
  <xsl:value-of select="$newline"/>   
  <xsl:text>...for the output: </xsl:text><xsl:value-of select="$modelName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$epName"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$instance"/>
  <xsl:text>.</xsl:text><xsl:value-of select="$id"/>
 </xsl:message>
 -->

 <xsl:for-each select="document($root/coupled/composition)
               /composition//set/field[@modelName=$modelName and 
               @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/../
               field[@modelName!=$modelName or @epName!=$epName or @id!=$id or @instance!=$instance]">
  <xsl:variable name="testMod" select="@modelName"/>
  <xsl:variable name="testEP" select="@epName"/>
  <xsl:variable name="testID" select="@id"/>
  <xsl:variable name="testInst" select="@instance"/>

  <!--
  <xsl:message terminate="no">
   <xsl:value-of select="$newline"/>   
   <xsl:text>Field in the output's set: </xsl:text><xsl:value-of select="$testMod"/>
   <xsl:text>.</xsl:text><xsl:value-of select="$testEP"/>
   <xsl:text>.</xsl:text><xsl:value-of select="$testInst"/>
   <xsl:text>.</xsl:text><xsl:value-of select="$testID"/>
  </xsl:message>
  -->

  <xsl:choose>
   <xsl:when test="not(document($root/coupled/models/model)/definition[name=$testMod]/language='intrinsic')">
    <!--<xsl:if test="contains(document($root/coupled/models/model)/definition[normalize-space(name)=$testMod]//entryPoint[@name=$testEP]/data[@id=$testID]/@direction,'in')">-->
     <xsl:text>,</xsl:text>
     <xsl:call-template name="getEPDataItem">
      <xsl:with-param name="dataToGetType" select="$dataToGetType"/>
      <xsl:with-param name="modelName" select="$testMod"/>
      <xsl:with-param name="instance" select="$testInst"/>
      <xsl:with-param name="epName" select="$testEP"/>
      <xsl:with-param name="id" select="$testID"/>
     </xsl:call-template>
     <!--</xsl:if>-->
   </xsl:when>
   <!--...instead, if the direction of the field is 'in', look into the set of the 'out' field that belongs to the 
       same intrinsic model instance and add to the list each of the non-intrinsic models in that set-->
   <xsl:otherwise>
    <xsl:if test="document($root/coupled/models/model)/definition[name=$testMod]/
            entryPoints[1]/entryPoint[@name=$testEP]/data[@id=$testID]/@direction='in'">
     <xsl:variable name="intrinsicOutID" select="document($root/coupled/models/model)
                                        /definition[name=$testMod]/entryPoints[1]/entryPoint[@name=$testEP]/data[@direction='out']/@id"/>
     <xsl:for-each select="document($root/coupled/composition)
               /composition//set/field[@modelName=$testMod and 
               @epName=$testEP and @id=$intrinsicOutID and (not(@instance) or @instance=$testInst)]/../
               field[@modelName!=$modelName or @epName!=$epName or @id!=$id or @instance!=$instance]">
      <xsl:if test="contains(document($root/coupled/models/model)/definition[normalize-space(name)=current()/@modelName]
                   //entryPoint[@name=current()/@epName]/data[@id=current()/@id]/@direction,'in')">
       <xsl:text>,</xsl:text>
       <xsl:variable name="test2Mod" select="@modelName"/>
       <xsl:variable name="test2EP" select="@epName"/>
       <xsl:variable name="test2ID" select="@id"/>
       <xsl:variable name="test2Inst" select="@instance"/>
       <xsl:call-template name="getEPDataItem">
        <xsl:with-param name="dataToGetType" select="$dataToGetType"/>
        <xsl:with-param name="modelName" select="$test2Mod"/>
        <xsl:with-param name="instance" select="$test2Inst"/>
        <xsl:with-param name="epName" select="$test2EP"/>
        <xsl:with-param name="id" select="$test2ID"/>
       </xsl:call-template>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:for-each>

 <!-- Add the data for the outputting entry point itself to the list end. -->
 <xsl:text>,</xsl:text>
 <xsl:call-template name="getEPDataItem">
  <xsl:with-param name="dataToGetType" select="$dataToGetType"/>
  <xsl:with-param name="modelName" select="$modelName"/>
  <xsl:with-param name="instance" select="$instance"/>
  <xsl:with-param name="epName" select="$epName"/>
  <xsl:with-param name="id" select="$id"/>
 </xsl:call-template>
</xsl:template>


<!-- IRH MOD: New template to get a specific piece of data associated
with the given field (=entry point argument). -->
<xsl:template name="getEPDataItem">
 <xsl:param name="dataToGetType"/>
 <xsl:param name="modelName"/>
 <xsl:param name="instance"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>

 <xsl:choose>
  <xsl:when test="$dataToGetType = 'SEQUENCE_ID_LIST'">
   <xsl:call-template name="InSequenceContextID">
    <xsl:with-param name="modelName" select="$modelName"/>
    <xsl:with-param name="epName" select="$epName"/>
    <xsl:with-param name="instance" select="$instance"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:when test="$dataToGetType = 'INPUT_LIST'">
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
      /definition[normalize-space(name)=$modelName]
      //entryPoint[@name=$epName]/data[@id=$id]/@direction = 'in'">
     <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>0</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="$dataToGetType = 'OUTPUT_LIST'">
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
      /definition[normalize-space(name)=$modelName]
      //entryPoint[@name=$epName]/data[@id=$id]/@direction = 'out'">
     <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>0</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (getEPDataItem): Unrecognised dataToGetType</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<!-- IRH MOD: New templates to sort a list of entry point instance data 
according to the sequence number of each entry point instance in the 
deployment. E.g. can be used to sort 'ins' and 'outs' lists. -->
<xsl:template name="sortEPDataBySequenceID">
 <xsl:param name="unsortedEPData"/>
 <xsl:param name="unsortedEPSequenceIDs"/>
 <xsl:param name="sortedEPSequenceIDs"/>

 <xsl:choose>
  <xsl:when test="function-available('str:tokenize')">
   <xsl:call-template name="doSortEPDataBySequenceID">
    <xsl:with-param name="unsortedEPData" select="str:tokenize($unsortedEPData, ',')"/>
    <xsl:with-param name="unsortedEPSequenceIDs" select="str:tokenize($unsortedEPSequenceIDs, ',')"/>
    <xsl:with-param name="sortedEPSequenceIDs" select="str:tokenize($sortedEPSequenceIDs, ',')"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:when test="function-available('xalan:tokenize')">
   <xsl:call-template name="doSortEPDataBySequenceID">
    <xsl:with-param name="unsortedEPData" select="str:tokenize($unsortedEPData, ',')"/>
    <xsl:with-param name="unsortedEPSequenceIDs" select="str:tokenize($unsortedEPSequenceIDs, ',')"/>
    <xsl:with-param name="sortedEPSequenceIDs" select="str:tokenize($sortedEPSequenceIDs, ',')"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (sortEPDataBySequenceID): no function to tokenize lists.</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>
<xsl:template name="doSortEPDataBySequenceID">
 <xsl:param name="unsortedEPData"/>
 <xsl:param name="unsortedEPSequenceIDs"/>
 <xsl:param name="sortedEPSequenceIDs"/>

 <xsl:for-each select="$sortedEPSequenceIDs">
  <xsl:variable name="unsortedSequenceIDPos">
   <xsl:call-template name="getFirstMatchingSequenceIDPos">
    <xsl:with-param name="sequenceID" select="current()"/>
    <xsl:with-param name="unsortedEPSequenceIDs" select="$unsortedEPSequenceIDs"/>
   </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="$unsortedEPData[ number($unsortedSequenceIDPos) ]"/>
  <xsl:if test="position() != last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
</xsl:template>
<xsl:template name="getFirstMatchingSequenceIDPos">
 <xsl:param name="sequenceID"/>
 <xsl:param name="unsortedEPSequenceIDs"/>
 <xsl:param name="unsortedSequenceIDPos" select="1"/>

 <xsl:choose>
  <xsl:when test="$unsortedEPSequenceIDs[ 1 ] = $sequenceID">
   <xsl:value-of select="$unsortedSequenceIDPos"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:call-template name="getFirstMatchingSequenceIDPos">
    <xsl:with-param name="sequenceID" select="$sequenceID"/>
    <xsl:with-param name="unsortedEPSequenceIDs" select="$unsortedEPSequenceIDs[ position() > 1 ]"/>
    <xsl:with-param name="unsortedSequenceIDPos" select="$unsortedSequenceIDPos + 1"/>
   </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
