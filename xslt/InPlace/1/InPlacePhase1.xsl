<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 8th Dec 2005
    Phase 1 of applying xsl templates to in place code template
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<!--
<xsl:include href="../../Utils/EPName.xsl"/>
-->
<xsl:template match="code">
 <xsl:variable name="HeadEP" select="EP"/>
 <!-- no inplace code needed if only one sequence unit? What about put/get? -->
 <xsl:if test="count(document($root/coupled/deployment)/deployment/deploymentUnits//sequenceUnit)>1">
  <!-- create code for each model - should be each DU? -->
  <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits//model[not(@instance) or @instance=1]">
   <code id="{position()}" name="{@name}">
<!-- for the moment, just call this BFG2 so that we can link RF 24/11/10
    we need this to be called BFG2 as that is the name that the source code
    module expects
    <EP type="entryPoint" name="{concat('BFG2InPlace_',@name)}" model="{@name}">
-->
    <EP type="entryPoint" name="BFG2" model="{@name}">
     <xsl:apply-templates select="$HeadEP/*"/>
    </EP>
   </code>
  </xsl:for-each>
 </xsl:if>
</xsl:template>

<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
