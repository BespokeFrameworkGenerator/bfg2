<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 25/07/06
    adds any required f77 style put/get subroutines which are required to link with models if they do not have a "use bfg" inside them. These put/get subroutines (eventually) call the actual f90 versions implemented in previous phases.

-->
<xsl:template match="interface">
 <xsl:variable name="myModelName" select="../../@name"/>
 <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f90'">
  <interface genericName="{@genericName}">
   <xsl:apply-templates select="*"/>
  </interface>
 </xsl:if>
</xsl:template>

<xsl:template match="code">
<!-- only add if we have at least one in-place put/get -->
  <xsl:variable name="myModelName" select="EP/@model"/>
  <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f77'">
  <code id="{@id}" name="{concat(@name,'_wrapper')}">
  <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints/entryPoint/data[@direction='out' or @direction='inout']">
   <comment><xsl:text>f77 to f90 put/get wrappers start</xsl:text></comment>
   <xsl:if test=".//send[@form='inplace']">
    <EP type="procedure" name="put">
     <arg name="data" type="real" rank="0" intent="in"/>
     <arg name="tag" type="integer" rank="0" intent="in"/>
     <comment><xsl:text>data is really void *</xsl:text></comment>
     <declare name="currentModel" datatype="integer"/>
   <xsl:variable name="dupos">
    <xsl:call-template name="get-du-pos">
     <xsl:with-param name="modelName" select="@name"/>
    </xsl:call-template>
   </xsl:variable>
   <include name="{concat('BFG2Target',$dupos)}"/>
     <assign lhs="currentModel" rhs="getActiveModel()"/>
      <xsl:for-each select="EP/Content/EP[InPlaceInfo/@direction='out']">
        <xsl:variable name="mycallname" select="@name"/>
        <xsl:for-each select="Content/if">
          <xsl:if test="action/if//send[@form='inplace']"> <!-- then I put something -->
          <if><condition><equals><value>currentModel</value><value><xsl:value-of select="ModelInfo/@id"/></value></equals></condition><action>
          <xsl:for-each select="action/if">
              <if><condition><equals><value>tag</value><value><xsl:value-of select="TagInfo/@id"/></value></equals></condition><action>
                <call name="{concat($mycallname,'_',$myModelName)}"><arg name="data"/><arg name="tag"/></call>
              </action></if>
          </xsl:for-each>
          </action>
          </if>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </EP>
   </xsl:if>
   <xsl:if test=".//send[@form='argpass']">
    <EP type="procedure" name="{concat('put_',$myModelName)}">
     <arg name="data" type="real" rank="0" intent="in"/>
     <arg name="tag" type="integer" rank="0" intent="in"/>
     <comment><xsl:text>data is really void *</xsl:text></comment>
     <declare name="currentModel" datatype="integer"/>
   <xsl:variable name="dupos">
    <xsl:call-template name="get-du-pos">
     <xsl:with-param name="modelName" select="@name"/>
    </xsl:call-template>
   </xsl:variable>
   <include name="{concat('BFG2Target',$dupos)}"/>
     <assign lhs="currentModel" rhs="getActiveModel()"/>
      <xsl:for-each select="EP/Content/EP[InPlaceInfo/@direction='out']">
        <xsl:variable name="mycallname" select="@name"/>
        <xsl:for-each select="Content/if">
          <xsl:if test="action/if//send[@form='argpass']"> <!-- then I put something -->
          <if><condition><equals><value>currentModel</value><value><xsl:value-of select="ModelInfo/@id"/></value></equals></condition><action>
          <xsl:for-each select="action/if">
              <if><condition><equals><value>tag</value><value><xsl:value-of select="TagInfo/@id"/></value></equals></condition><action>
                <call name="{concat($mycallname,'_',$myModelName)}"><arg name="data"/><arg name="tag"/></call>
              </action></if>
          </xsl:for-each>
          </action>
          </if>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </EP>
   </xsl:if>
  </xsl:if>
  <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints/entryPoint/data[@direction='in' or @direction='inout']">
   <xsl:if test=".//receive[@form='inplace']">
    <EP type="procedure" name="get">
    <arg name="data" type="real" rank="0" intent="out"/>
    <arg name="tag" type="integer" rank="0" intent="in"/>
    <comment><xsl:text>data is really void *</xsl:text></comment>
    <declare name="currentModel" datatype="integer"/>
   <xsl:variable name="dupos">
    <xsl:call-template name="get-du-pos">
     <xsl:with-param name="modelName" select="@name"/>
    </xsl:call-template>
   </xsl:variable>
   <include name="{concat('BFG2Target',$dupos)}"/>
    <assign lhs="currentModel" rhs="getActiveModel()"/>
     <xsl:for-each select="EP/Content/EP[InPlaceInfo/@direction='in']">
       <xsl:variable name="mycallname" select="@name"/>
       <xsl:for-each select="Content/if">
         <xsl:if test="action/if//receive[@form='inplace']"> <!-- then I get something -->
         <if><condition><equals><value>currentModel</value><value><xsl:value-of select="ModelInfo/@id"/></value></equals></condition><action>
         <xsl:for-each select="action/if">
             <if><condition><equals><value>tag</value><value><xsl:value-of select="TagInfo/@id"/></value></equals></condition><action>
               <call name="{concat($mycallname,'_',$myModelName)}"><arg name="data"/><arg name="tag"/></call>
             </action></if>
         </xsl:for-each>
         </action>
         </if>
         </xsl:if>
       </xsl:for-each>
     </xsl:for-each>
   </EP>
   </xsl:if>
   <xsl:if test=".//receive[@form='argpass']">
    <EP type="procedure" name="{concat('get_',$myModelName)}">
    <arg name="data" type="real" rank="0" intent="out"/>
    <arg name="tag" type="integer" rank="0" intent="in"/>
    <comment><xsl:text>data is really void *</xsl:text></comment>
    <declare name="currentModel" datatype="integer"/>
   <xsl:variable name="dupos">
    <xsl:call-template name="get-du-pos">
     <xsl:with-param name="modelName" select="@name"/>
    </xsl:call-template>
   </xsl:variable>
   <include name="{concat('BFG2Target',$dupos)}"/>
    <assign lhs="currentModel" rhs="getActiveModel()"/>
     <xsl:for-each select="EP/Content/EP[InPlaceInfo/@direction='in']">
       <xsl:variable name="mycallname" select="@name"/>
       <xsl:for-each select="Content/if">
         <xsl:if test="action/if//receive[@form='argpass']"> <!-- then I get something -->
         <if><condition><equals><value>currentModel</value><value><xsl:value-of select="ModelInfo/@id"/></value></equals></condition><action>
         <xsl:for-each select="action/if">
             <if><condition><equals><value>tag</value><value><xsl:value-of select="TagInfo/@id"/></value></equals></condition><action>
               <call name="{concat($mycallname,'_',$myModelName)}"><arg name="data"/><arg name="tag"/></call>
             </action></if>
         </xsl:for-each>
         </action>
         </if>
         </xsl:if>
       </xsl:for-each>
     </xsl:for-each>
   </EP>
   </xsl:if>
  </xsl:if>
  <!-- no need for finding unique names as they are already guaranteed to be unique - hoorah. For the record the plan was to do something like this although I'm not sure it would have worked!
  <comment>**** So now for unique names ***</comment>
  <xsl:for-each select="EP/Content/EP">
    <comment><xsl:text>found entry point </xsl:text>
             <xsl:if test="not(@name = following-sibling::text or @name = preceding-sibling::text)">
             <xsl:value-of select="@name"/>
             </xsl:if>
    </comment>
  </xsl:for-each>
-->
  <xsl:for-each select="EP/Content/EP">
    <EP type="procedure" name="{concat(@name,'_',$myModelName)}">
    <arg name="data" type="{InPlaceInfo/@datatype}" rank="{InPlaceInfo/@rank}" intent="{InPlaceInfo/@direction}" shape="unassumed"/>
    <arg name="tag" type="integer" rank="0" intent="in"/>
<!--
      <include name="{concat('BFG2InPlace_',$myModelName)}">
-->
      <include name="BFG2">
       <rename to="{InPlaceInfo/@directionname}" from="{@name}"/>
      </include>
<!--
      <declare name="data" datatype="{InPlaceInfo/@datatype}" dimension="{InPlaceInfo/@rank}" intent="{InPlaceInfo/@direction}"/>
      <declare name="tag" datatype="integer" dimension="0" intent="in"/>
-->
      <call name="{InPlaceInfo/@directionname}"><arg name="data"/><arg name="tag"/></call>
    </EP>
  </xsl:for-each>
  <comment><xsl:text>f77 to f90 put/get wrappers end</xsl:text></comment>
  </code>
  </xsl:if> <!-- test for f77 model-->
  <code id="{@id}" name="{@name}">
   <xsl:apply-templates select="*"/>
  </code>
</xsl:template>

<xsl:template name="get-du-pos">
 <xsl:param name="modelName"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
  <xsl:if test=".//model/@name=$modelName">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
