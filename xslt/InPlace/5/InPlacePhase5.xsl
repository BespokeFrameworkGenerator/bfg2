<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 25/07/06
    Phase 5 of applying xsl templates to in place code template
    add in f77tof90 put/get wrappers so models do not need to
    add "use bfg" whenever they have call put/get

    IRH: 2/4/07: Moved processing of <send> and <receive> from
    f90gen.xsl to here by including targets.xsl, so that XML 
    pseudocode can be used.  The output of send and receive templates 
    should be pseudocode rather than the raw Fortran for the calls 
    themselves, and that pseudocode can *then* be processed by f90gen,
    in the same way as it is for other target-specific templates in
    targets.xsl such as InitComms etc..  Note that <send> and
    <receive> elements are created in the previous phase, 
    InPlace/4/SendReceive.xsl.
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<!-- IRH MOD: Added these for processing of <send> and <receive> - 
for OASIS4 targets, we call a differently named wrapper function for
profiling (oasisvis_prism_put/get() instead of prism_put/get()) -
unlike the profiling versions of mpi_send/recv(), the prism_put/get()
wrapper functions have extra arguments so I've named them differently. -->
<xsl:param name="mpivis" select="'false'"/>
<xsl:param name="oasisvis" select="'false'"/>
<!-- IRH: Added commFormsRoot - previously, send and receive templates were 
being called from f90Gen.xsl, where commFormsRoot was already available 
(from CodeGen.xsl).  Now the send and receive templates are called from here
via targets.xsl. -->
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:param name="OutDir"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="f772f90putget.xsl"/>
<xsl:include href="../../Utils/targets.xsl"/>

<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
