<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">


<xsl:param name="outRoutine" select="'put'"/>
<xsl:param name="inRoutine" select="'get'"/>
<!--<xsl:include href="ArgPassingTests.xsl"/>-->
<!--<xsl:include href="GenVarIDs.xsl"/>-->
<xsl:include href="../../Utils/InSequenceContextID.xsl"/>
<xsl:include href="../../Utils/DummyVars.xsl"/>

<xsl:variable name="maxTag">
 <xsl:for-each select="$commFormsRoot/communicationForms/*">
  <xsl:sort select="@ref" order="descending"/>
  <xsl:if test="position()=1">
   <xsl:value-of select="@ref"/>
  </xsl:if>
 </xsl:for-each>
</xsl:variable>

<xsl:template match="code">
 <xsl:if test="EP/interface/procedure">
  <code id="{@id}" name="{@name}">
   <xsl:apply-templates select="*"/>
  </code>
 </xsl:if>
</xsl:template>

<!--only make one copy of each required procedure, where uniqueness is determined by datatype, kind and rank-->
<xsl:template match="interface">
 <xsl:variable name="myModelName" select="../../@name"/>
  <interface genericName="{@genericName}">
   <xsl:for-each select="procedure">
    <xsl:variable name="type" select="@dataType"/>
    <xsl:variable name="rank" select="@rank"/>
    <xsl:variable name="kind" select="@kind"/>
    <xsl:variable name="myid" select="generate-id(.)"/>
    <xsl:for-each select="../procedure[@dataType=$type and @rank=$rank and @kind=$kind]">
     <xsl:if test="position()=1 and generate-id(.)=$myid">
      <procedure name="{@name}" dataType="{translate(translate(normalize-space(@dataType),' ','_'),'*','')}" kind="{@kind}" rank="{@rank}" type="{@type}"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:for-each>
  </interface>
</xsl:template>

<xsl:template match="private">
 <xsl:variable name="myModelName" select="../../@name"/>
 <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f90'">
  <private>
   <xsl:apply-templates select="*"/>
  </private>
 </xsl:if>
</xsl:template>

<xsl:template match="public">
 <xsl:variable name="myModelName" select="../../@name"/>
 <xsl:if test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f90'">
  <public>
   <xsl:apply-templates select="*"/>
  </public>
 </xsl:if>
</xsl:template>

<!--
<xsl:template match="">
integer :: currentModel
currentModel=getActiveModel()
</xsl:template>
-->

<xsl:template match="Content">
 <!--<xsl:variable name="myModelName" select="../../@name"/>-->
 <Content>
  <xsl:variable name="myModelName" select="../@model"/>
  <xsl:for-each select="../interface/procedure">
   <xsl:variable name="type" select="@dataType"/>
   <xsl:variable name="rank" select="@rank"/>
   <xsl:variable name="kind" select="@kind"/>
   <xsl:variable name="myid" select="generate-id(.)"/>
   <xsl:for-each select="../procedure[@dataType=$type and @rank=$rank and @kind=$kind]">
    <xsl:if test="position()=1 and generate-id(.)=$myid">
     <xsl:variable name="argType" select="../procedure[@dataType=$type and @rank=$rank]/@type[.='sizeref']"/>
     <!--<xsl:variable name="" select="../procedure[@dataType=$type and @rank=$rank]/@type[.='sizeref']"/>-->
     <EP type="procedure" name="{@name}{translate(translate(normalize-space(@dataType),' ','_'),'*','')}_{@kind}_{@rank}">
       <!--determine the direction in which the current procedure passes data-->
       <xsl:variable name="direction">
        <xsl:choose>
         <xsl:when test="../@genericName=$inRoutine">
          <xsl:text>in</xsl:text>
         </xsl:when>
         <xsl:when test="../@genericName=$outRoutine">
          <xsl:text>out</xsl:text>
         </xsl:when>
         <xsl:otherwise>
          <xsl:message terminate="yes">
           <xsl:text>Error in InOutProcedures.xsl:
           unrecognised procedure name: </xsl:text><xsl:value-of select="@name"/>
          </xsl:message>
         </xsl:otherwise>
        </xsl:choose>
       </xsl:variable>
      <InPlaceInfo direction="{$direction}" directionname="{@name}" datatype="{$type}" rank="{$rank}"/>
      <xsl:choose>
      <xsl:when test="@rank &gt; 0">
       <!--cwa - changed below 
         If we have an f90 model, a dynamically sized array is needed because we need to use the size() function
         within put and get, which requires an assumed shape array.
         For f77 models assumed size arrays (i.e. 'unassumed' shape arrays) are needed for the wrapper subroutine calls.
       -->
       <xsl:choose>
        <xsl:when test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f77'">
         <arg type="{@dataType}" rank="{@rank}" shape="unassumed" kind="{$kind}"/>
        </xsl:when>
        <xsl:when test="document($root/coupled/models/model)/definition[name=$myModelName]/language='f90'">
         <arg type="{@dataType}" rank="{@rank}" shape="assumed" kind="{$kind}"/>
        </xsl:when>
        <xsl:when test="document($root/coupled/models/model)/definition[name=$myModelName]/language='intrinsic'"/>
        <xsl:otherwise>
         <xsl:message terminate="yes">
          <xsl:text>Unrecognised language specification: </xsl:text><xsl:value-of select="document($root/coupled/models/model)/definition[name=$myModelName]/language"/>
         </xsl:message>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
       <arg type="{@dataType}" rank="{@rank}" kind="{$kind}"/>
      </xsl:otherwise>
      </xsl:choose>
      <arg type="integer" rank="0"/>
      <Includes>
       <targetIncludes/>
      </Includes>
      <DataDecs>
       <targetDecs/>
       <declare name="currentModel" datatype="integer" dimension="0"/>
      </DataDecs>
      <Content>
       <assign lhs="currentModel" rhs="getActiveModel()"/>
       <!--translate the direction of the procedure into 'put' or 'get', which
           are output by the templates used in ArgPassingTests.xsl to indicate remote 
           connections-->
       <xsl:variable name="routineType">
        <xsl:choose>
         <xsl:when test="$direction='in'">
          <xsl:text>get</xsl:text>
         </xsl:when>
         <xsl:when test="$direction='out'">
          <xsl:text>put</xsl:text>
         </xsl:when>
        </xsl:choose>
       </xsl:variable>
       <!--find all the required remote connections-->
       <!--
       <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
        <xsl:variable name="modelName" select="@name"/>
        <xsl:variable name="epName" select="@ep"/>
        <xsl:variable name="instance" select="@instance"/>
       -->
       <!-- loop over all (model,entry point) tuples in the schedule -->
       <xsl:for-each select="document($root/coupled/deployment)//schedule//model[@name=$myModelName]">
        <xsl:variable name="modelName" select="@name"/>
        <xsl:variable name="epName" select="@ep"/>
        <xsl:variable name="instance" select="@instance"/>
        <!-- find the particular (model,entry point definition) -->
        <xsl:for-each select="document($root/coupled/models/model)/definition[normalize-space(name)=$modelName and entryPoints/entryPoint/@name=$epName]">

          <xsl:variable name="modid">
          <xsl:call-template name="InSequenceContextID">
            <xsl:with-param name="modelName" select="$modelName"/>
            <xsl:with-param name="epName" select="$epName"/>
            <xsl:with-param name="instance" select="$instance"/>
          </xsl:call-template>
          </xsl:variable>
          <if>
           <ModelInfo id="{$modid}"/>
           <condition>
            <equals>
             <value>currentModel</value>
             <value>
               <xsl:value-of select="$modid"/>
             </value>
            </equals>
           </condition>
           <action>
           <comment><xsl:text>I am model=</xsl:text><xsl:value-of select="$modelName"/><xsl:text> and ep=</xsl:text><xsl:value-of select="$epName"/><xsl:text> and instance=</xsl:text><xsl:value-of select="$instance"/></comment>
           </action>

         <!--CWA Added the block below to generate comms for 'assumed' array sizeRefs-->
         <xsl:if test="$argType='sizeref'">
          <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]/entryPoints[1]/entryPoint[@name=$epName]/data">
           <xsl:variable name="id" select="@id"/>
           <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field
                                         [@modelName=$modelName and @epName=$epName and @id=$id]]"/>
           <xsl:variable name="isAssumedAll">
            <xsl:for-each select="$mySet/field">
             <xsl:variable name="setMod" select="@modelName"/>
             <xsl:variable name="setEP" select="@epName"/>
             <xsl:variable name="setid" select="@id"/>
             <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod]//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
              <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
             </xsl:if>
            </xsl:for-each>
           </xsl:variable>
           <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>
           <xsl:if test="$isAssumed!=''">
            <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
            <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
            <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>
            <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]/
                             entryPoint[@name=$assumedEP]/data[@id=$assumedid]//assumed">
             <xsl:variable name="sizeRefID">
              <xsl:call-template name="getAssumedDummyRef">
               <xsl:with-param name="assumed" select="."/>
              </xsl:call-template>
             </xsl:variable>
             <action>
             <if>
              <TagInfo id="-{$sizeRefID + $maxTag}"/>
              <condition>
               <equals>
                <value>arg2</value>
                <value>-<xsl:value-of select="$sizeRefID + $maxTag"/></value>
               </equals>
              </condition>
              <action>
               <connection type="sizeref" id="{$id}" instance="{$instance}" epName="{$epName}" modelName="{$modelName}"/>
              </action>
             </if>
             </action>
            </xsl:for-each>
           </xsl:if>
          </xsl:for-each>
         </xsl:if>


         <xsl:for-each select=".//entryPoint[@name=$epName]/data[@dimension=$rank and @dataType=$type and ((not(string($kind)) and not(string(@kind))) or @kind=$kind) and contains(@direction,$direction)]">
          <xsl:variable name="myID" select="@id"/>
          <xsl:variable name="commsForm" select="@form"/>
          <!--
          <xsl:message terminate="no">
            <xsl:text>
            model:</xsl:text>
            <xsl:value-of select="$modelName"/>
            <xsl:text>
            ep:</xsl:text>
            <xsl:value-of select="$epName"/>
            <xsl:text>
            id:</xsl:text>
            <xsl:value-of select="$myID"/>
            <xsl:text>
            routineType:</xsl:text>
            <xsl:value-of select="$routineType"/>
            <xsl:text>
            direction:</xsl:text>
            <xsl:value-of select="$direction"/>
            <xsl:text>
            rank:</xsl:text>
            <xsl:value-of select="$rank"/>
            <xsl:text>
            type:</xsl:text>
            <xsl:value-of select="$type"/>
            <xsl:text>
            connectionRequired:</xsl:text>
            <xsl:value-of select="$connectionRequired"/>
          </xsl:message>
          -->
          <xsl:if test="$commFormsRoot/communicationForms
                  /*[@modelName=$modelName and @epName=$epName and 
                  @id=$myID and (not($instance) or @instance=$instance) and contains(@form,$routineType)]">
           <xsl:variable name="connectionid">
            <xsl:choose>
            <xsl:when test="document($root/coupled/models/model)
                      /definition/entryPoints/entryPoint/data[@form='inplace' and 
                      @id=$myID and ancestor::entryPoint/@name=$epName and 
                      ancestor::definition/name=$modelName]">
              <!-- inplace put or get so use its actual id -->
              <xsl:value-of select="$myID"/>
            </xsl:when>
            <xsl:otherwise> <!-- generated put/get for argpassing var -->
            <xsl:variable name="tmpid" select="$commFormsRoot/communicationForms
                          /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not($instance) or @instance=$instance)]/@ref"/>
            <!--
            <xsl:variable name="tmpid">
            <xsl:call-template name="dataidgenvalue">
             <xsl:with-param name="modelName" select="$modelName"/>
             <xsl:with-param name="epName" select="$epName"/>
             <xsl:with-param name="id" select="$myID"/>
            </xsl:call-template>
            </xsl:variable>
            -->
            <xsl:text>-</xsl:text>
            <!--<xsl:value-of select="substring-before($tmpid,':')"/>-->
            <xsl:value-of select="$tmpid"/>
            </xsl:otherwise>
            </xsl:choose>
           </xsl:variable>
           <action>
           <if>
            <TagInfo id="{$connectionid}"/>
            <condition>
             <equals>
              <value>arg2</value>
              <value><xsl:value-of select="$connectionid"/></value>
             </equals>
            </condition>
            <action>
             <connection modelName="{$modelName}" epName="{$epName}" id="{$myID}" instance="{$instance}" form="{$commsForm}"/>
            </action>
           </if>
           </action>
          </xsl:if>
         </xsl:for-each>
           </if>
        </xsl:for-each> <!-- entryPoint -->
       </xsl:for-each> <!-- model -->
      </Content>
     </EP>
    </xsl:if>
   </xsl:for-each>
  </xsl:for-each>
 </Content>
</xsl:template>

</xsl:stylesheet>
