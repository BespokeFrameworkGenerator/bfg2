<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:param name="outRoutine" select="'put'"/>
<xsl:param name="inRoutine" select="'get'"/>
<xsl:param name="UseTarget" select="'BFG2Target'"/>
<!--<xsl:include href="ArgPassingTests.xsl"/>-->

<xsl:template match="Includes">
 <Includes>
  <xsl:variable name="dupos">
   <xsl:call-template name="get-du-pos">
    <xsl:with-param name="modelName" select="../../@name"/>
   </xsl:call-template>
  </xsl:variable>
  <include name="{concat($UseTarget,$dupos)}"/>
 </Includes>
 <!-- rf: test should really be 'if there is one or more connected in-place
      or any cross sequence unit arg passing'. I'll just look for a slightly
      more broad (less accurate) test which is, 'if one or more in-place or
      more than one sequence unit'
 <xsl:if test="count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit) > 1">
 -->

 <xsl:variable name="myModelName" select="../@model"/>
 <xsl:if test="(document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints/entryPoint/data[@form='inplace']) or (count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit) > 1)">

  <private/>
  <public>
   <procedure name="{$inRoutine}"/>
   <procedure name="{$outRoutine}"/>
  </public>
  <interface genericName="{$inRoutine}">
  <!--for each definition, for each field, check whether a get is required; 
      if it is, output a procedure element that takes two types of info: the
      datatype of the field and the rank-->
     <xsl:for-each select="document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints[1]/entryPoint">
      <DEBUG><xsl:text>Looking at model </xsl:text><xsl:value-of select="$myModelName"/><xsl:text> and entry point </xsl:text><xsl:value-of select="@name"/></DEBUG>
      <xsl:variable name="epName" select="@name"/>
      <xsl:for-each select="data">
       <xsl:variable name="myID" select="@id"/>
       <xsl:variable name="myKind" select="@kind"/>
       <DEBUG><xsl:text>Looking at data </xsl:text><xsl:value-of select="@id"/></DEBUG>
       <xsl:if test="$commFormsRoot/communicationForms/*[@modelName=$myModelName and @epName=$epName and @id=$myID and contains(@form,'get')]">
        <DEBUG><xsl:text>Found a communication</xsl:text></DEBUG>
        <!--for generic interfaces, we need to select the right datatype here-->
        <xsl:variable name="datatype">
         <xsl:choose>
          <xsl:when test="count(document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints) > 1">
           <!--iterate through the set and find a field with a non-generic definition-->
           <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field[@modelName=$myModelName and @epName=$epName and @id=$myID]]"/>
           <xsl:variable name="datatypes">
            <xsl:for-each select="$mySet/field">
             <xsl:if test="count(document($root/coupled/models/model)/definition[name=current()/@modelName]/entryPoints) = 1">
              <xsl:value-of select="document($root/coupled/models/model)/definition[name=current()/@modelName]//entryPoint[@name=current()/@epName]/data[@id=current()/@id]/@dataType"/><xsl:text>::</xsl:text>
             </xsl:if>
            </xsl:for-each>
           </xsl:variable>
           <xsl:value-of select="substring-before($datatypes,'::')"/>
          </xsl:when>
          <xsl:otherwise>
           <xsl:value-of select="@dataType"/>
          </xsl:otherwise>
         </xsl:choose>
        </xsl:variable>
        <!--if this field is part of a set that contains a field that is declared of 'assumed' size then we receive the size for each dimension too-->
        <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field
                                           [@modelName=$myModelName and @epName=$epName and @id=$myID]]"/>
        <xsl:variable name="isAssumedAll">
         <xsl:for-each select="$mySet/field">
          <xsl:variable name="setMod" select="@modelName"/>
          <xsl:variable name="setEP" select="@epName"/>
          <xsl:variable name="setid" select="@id"/>
          <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod and language!='intrinsic']//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
           <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
          </xsl:if>
         </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>
        <xsl:if test="$isAssumed!=''">
         <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
         <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
         <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>
         <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]/
                               entryPoint[@name=$assumedEP]/data[@id=$assumedid]//assumed">
          <procedure name="{$inRoutine}" dataType="integer" kind="{$myKind}" rank="0" type="sizeref"/>
         </xsl:for-each>
        </xsl:if>
        <procedure name="{$inRoutine}" dataType="{$datatype}" kind="{$myKind}" rank="{@dimension}"/>
       </xsl:if>
      </xsl:for-each>
     </xsl:for-each>
  </interface>
  <interface genericName="{$outRoutine}">
  <!--for each definition, for each field, check whether a get is required; 
      if it is, output a procedure element that takes two types of info: the
      datatype of the field and the rank-->
   <xsl:for-each select="document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints/entryPoint">
    <xsl:variable name="epName" select="@name"/>
    <xsl:for-each select="data">
     <xsl:variable name="myID" select="@id"/>
     <xsl:variable name="myKind" select="@kind"/>
     <xsl:if test="$commFormsRoot/communicationForms/*[@modelName=$myModelName and @epName=$epName and @id=$myID and contains(@form,'put')]">
      <!--for generic interfaces, we need to select the right datatype here-->
      <xsl:variable name="datatype">
       <xsl:choose>
        <xsl:when test="count(document($root/coupled/models/model)/definition[name=$myModelName]/entryPoints) > 1">
         <!--iterate through the set and find a field with a non-generic definition-->
         <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field[@modelName=$myModelName and @epName=$epName and @id=$myID]]"/>
         <xsl:variable name="datatypes">
          <xsl:for-each select="$mySet/field">
           <xsl:if test="count(document($root/coupled/models/model)/definition[name=current()/@modelName]/entryPoints) = 1">
            <xsl:value-of select="document($root/coupled/models/model)/definition[name=current()/@modelName]//
                                 entryPoint[@name=current()/@epName]/data[@id=current()/@id]/@dataType"/><xsl:text>::</xsl:text>
           </xsl:if>
          </xsl:for-each>
         </xsl:variable>
         <xsl:value-of select="substring-before($datatypes,'::')"/>
        </xsl:when>
        <xsl:otherwise>
         <xsl:value-of select="@dataType"/>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:variable>
      <!--if this field is part of a set that contains a field that is declared of 'assumed' size then we send the size for each dimension too-->
      <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field
                                         [@modelName=$myModelName and @epName=$epName and @id=$myID]]"/>
      <xsl:variable name="isAssumedAll">
       <xsl:for-each select="$mySet/field">
        <xsl:variable name="setMod" select="@modelName"/>
        <xsl:variable name="setEP" select="@epName"/>
        <xsl:variable name="setid" select="@id"/>
        <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod and language!='intrinsic']//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
         <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
        </xsl:if>
       </xsl:for-each>
      </xsl:variable>
      <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>
      <xsl:if test="$isAssumed!=''">
       <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
       <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
       <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>
       <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]/
                             entryPoint[@name=$assumedEP]/data[@id=$assumedid]//assumed">
        <procedure name="{$outRoutine}" dataType="integer" kind="{$myKind}" rank="0" type="sizeref"/>
       </xsl:for-each>
      </xsl:if>
      <procedure name="{$outRoutine}" dataType="{$datatype}" kind="{$myKind}" rank="{@dimension}"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:for-each>
  </interface>
  <Separator/>
 </xsl:if>
</xsl:template>

<xsl:template name="get-du-pos">
 <xsl:param name="modelName"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
  <xsl:if test=".//model/@name=$modelName">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
