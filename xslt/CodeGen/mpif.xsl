<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:param name="mpivis" select="'false'"/>
<!--<xsl:include href="GenVarIDs.xsl"/>
This is a hack because of an incompatability between python and java xslt processors!
-->
<!-- IRH: No longer needed - send/receive are no longer part of source
code generation step. -->
<!--<xsl:variable name="mySevenSp" select="'       '"/>-->

<!-- IRH: 2/5/07: Renamed template from "receive" to "mpiReceive".
Now <receive> element is matched in targets.xsl, and redirected
here if we're using an MPI target. I've also converted the output
from Fortran to XML pseudocode in line with elsewhere.  This template
is now called from InPlacePhase5 rather than f90Gen. -->
<!--<xsl:template match="receive">-->
<!-- This code is destined for BFG2InPlace.f90 -->
<xsl:template name="mpiReceive">
 <xsl:variable name="modelName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="id" select="@id"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="type" select="@type"/>

 <xsl:variable name="messageTag">
   <xsl:call-template name="getMessageTag">
     <xsl:with-param name="modelName" select="@modelName"/>
     <xsl:with-param name="epName" select="@epName"/>
     <xsl:with-param name="id" select="@id"/>
   </xsl:call-template>
 </xsl:variable>

 <xsl:variable name="mysize">
  <xsl:call-template name="getSize">
   <xsl:with-param name="modelName" select="@modelName"/>
   <xsl:with-param name="epName" select="@epName"/>
   <xsl:with-param name="id" select="@id"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="@language='f77' and $mysize='size(arg1)'">
   <xsl:message terminate="yes">
    <xsl:text>Error: model is specified as f77, but dynamic arrays are used. </xsl:text>
    <xsl:text>Model=</xsl:text><xsl:value-of select="ancestor::code/@name"/>
    <xsl:text> Suggested action: declare as f90 but do not declare a module name in the languageSpecific element.</xsl:text>
   </xsl:message>
  </xsl:when>
  <xsl:when test="$type='sizeref'">
   <assign lhs="mysize" rhs="1"/>
  </xsl:when>
  <xsl:when test="@language='f77'">
   <assign lhs="mysize" rhs="{$mysize}"/>
   <!--<xsl:text>mysize=</xsl:text><xsl:value-of select="$mysize"/>-->
  </xsl:when>
  <xsl:when test="@language='f90'">
   <assign lhs="mysize" rhs="{$mysize}"/>
   <!--<xsl:text>mysize=</xsl:text><xsl:value-of select="$mysize"/>-->
  </xsl:when>
  <xsl:when test="@language='intrinsic'"/>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Unrecognised language specification: </xsl:text><xsl:value-of select="@language"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
 <!--hack required to overcome ifort bug!!-->
 <!-- RF just use mysize for the array bounds as we can't use
      'size' anymore as arrays are declared as assumed size
      rather than assumed shape
-->
 <xsl:choose>
  <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]
                /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dimension > 0 and 
                document($root/coupled/models/model)/definition[name=$modelName]
                /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dataType != 'character'">

<!-- rf: the new way
   <xsl:value-of select="$mySevenSp"/>
   <xsl:text>do i1=1,mysize</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:value-of select="$mySevenSp"/>
   <xsl:text>arg1(i1)=</xsl:text>
   <xsl:call-template name="zero">
    <xsl:with-param name="modelName" select="@modelName"/>
    <xsl:with-param name="epName" select="@epName"/>
    <xsl:with-param name="id" select="@id"/>
   </xsl:call-template>
   <xsl:value-of select="$newline"/>
   <xsl:value-of select="$mySevenSp"/>
   <xsl:text>end do</xsl:text>
   <xsl:value-of select="$newline"/>
-->

<!--
   <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]
                         /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/dim">
    <xsl:sort select="value" order="ascending"/>
    <iterate counter="i{position()}" start="1" end="size(arg1,{position()})">
     <xsl:variable name="indices">
      <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]
                            /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/dim">
       <xsl:sort select="value" order="ascending"/>
       <xsl:text>i</xsl:text><xsl:value-of select="position()"/>
       <xsl:if test="position()!=last()">
        <xsl:text>,</xsl:text>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="zeroValue">
      <xsl:call-template name="zero">
       <xsl:with-param name="modelName" select="@modelName"/>
       <xsl:with-param name="epName" select="@epName"/>
       <xsl:with-param name="id" select="@id"/>
      </xsl:call-template>
     </xsl:variable>
     <assign lhs="arg1({$indices})" rhs="{$zeroValue}"/>
    </iterate>
   </xsl:for-each>
-->

  </xsl:when>
 </xsl:choose>

 <xsl:variable name="mpiDataType">
  <xsl:choose>
  <xsl:when test="$type='sizeref'">
   <xsl:text>mpi_integer</xsl:text>
  </xsl:when>
  <xsl:otherwise>
  <xsl:call-template name="getMPItype">
   <xsl:with-param name="modelName" select="@modelName"/>
   <xsl:with-param name="epName" select="@epName"/>
   <xsl:with-param name="id" select="@id"/>
  </xsl:call-template>
  </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
<!--
   <xsl:value-of select="$commFormsRoot/communicationForms
                /*[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/@ref"/>
-->
 <call name="mpi_recv">
  <arg name="arg1"/>
  <arg name="mysize"/>
  <arg name="{$mpiDataType}"/>
  <arg name="du"/>
  <arg name="{$messageTag}"/>
  <arg name="mpi_comm_world"/>
  <arg name="istatus"/>
  <arg name="ierr"/>
 </call>
</xsl:template>


<!-- IRH: 2/5/07: Renamed template from "send" to "mpiSend".
Now <send> element is matched in targets.xsl, and redirected
here if we're using an MPI target. I've also converted the output
from Fortran to XML pseudocode in line with elsewhere.  This template
is now called from InPlacePhase5 rather than f90Gen. -->
<!--<xsl:template match="send">-->
<!-- This code is destined for BFG2InPlace.f90 -->
<xsl:template name="mpiSend">
 <xsl:variable name="modelName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="id" select="@id"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="type" select="@type"/>

 <xsl:variable name="messageTag">
   <xsl:call-template name="getMessageTag">
     <xsl:with-param name="modelName" select="@modelName"/>
     <xsl:with-param name="epName" select="@epName"/>
     <xsl:with-param name="id" select="@id"/>
   </xsl:call-template>
 </xsl:variable>

 <xsl:variable name="mysize">
  <xsl:call-template name="getSize">
   <xsl:with-param name="modelName" select="@modelName"/>
   <xsl:with-param name="epName" select="@epName"/>
   <xsl:with-param name="id" select="@id"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="@language='f77' and $mysize='size(arg1)'">
   <xsl:message terminate="yes">
    <xsl:text>Error: model is specified as f77, but dynamic arrays are used. </xsl:text>
    <xsl:text>Model=</xsl:text><xsl:value-of select="ancestor::code/@name"/>
    <xsl:text> Suggested action: declare as f90 but do not declare a module name in the languageSpecific element.</xsl:text>
   </xsl:message>
  </xsl:when>
  <xsl:when test="$type='sizeref'">
   <assign lhs="mysize" rhs="1"/>
  </xsl:when>
  <xsl:when test="@language='f77'">
   <!--<xsl:text>mysize=</xsl:text><xsl:value-of select="$mysize"/>-->
   <assign lhs="mysize" rhs="{$mysize}"/>
  </xsl:when>
  <xsl:when test="@language='f90'">
   <!--<xsl:text>mysize=</xsl:text><xsl:value-of select="$mysize"/>-->
   <assign lhs="mysize" rhs="{$mysize}"/>
  </xsl:when>
  <xsl:when test="@language='intrinsic'"/>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Unrecognised language specification: </xsl:text><xsl:value-of select="@language"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:variable name="mpiDataType">
  <xsl:choose>
  <xsl:when test="$type='sizeref'">
   <xsl:text>mpi_integer</xsl:text>
  </xsl:when>
  <xsl:otherwise>
  <xsl:call-template name="getMPItype">
   <xsl:with-param name="modelName" select="@modelName"/>
   <xsl:with-param name="epName" select="@epName"/>
   <xsl:with-param name="id" select="@id"/>
  </xsl:call-template>
  </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <call name="mpi_send">
  <arg name="arg1"/>
  <arg name="mysize"/>
  <arg name="{$mpiDataType}"/>
  <arg name="du"/>
  <arg name="{$messageTag}"/>
  <arg name="mpi_comm_world"/>
  <arg name="ierr"/>
 </call>
</xsl:template>


<xsl:template name="getMPItype">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:text>mpi_</xsl:text>
 <xsl:choose>
  <xsl:when test="count(document($root/coupled/models/model)/definition[name=$modelName]/entryPoints) > 1">
     <xsl:variable name="mySet" select="document($root/coupled/composition)/composition//set[field
                                        [@modelName=$modelName and @epName=$epName and @id=$id]]"/>
     <xsl:variable name="isNotAssumedAll">
      <xsl:for-each select="$mySet/field">
       <xsl:variable name="setMod" select="@modelName"/>
       <xsl:variable name="setEP" select="@epName"/>
       <xsl:variable name="setid" select="@id"/>
       <xsl:if test="not(document($root/coupled/models/model)/definition[name=$setMod]//entryPoint[@name=$setEP]/data[@id=$setid]//assumed)">
        <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="isNotAssumed" select="substring-before($isNotAssumedAll,':')"/>
     <xsl:variable name="unassumedMod" select="$mySet/field[number($isNotAssumed)]/@modelName"/>
     <xsl:variable name="unassumedEP" select="$mySet/field[number($isNotAssumed)]/@epName"/>
     <xsl:variable name="unassumedid" select="$mySet/field[number($isNotAssumed)]/@id"/>
     <xsl:value-of select="translate(normalize-space(document($root/coupled/models/model)
               /definition[name=$unassumedMod]/entryPoints
               /entryPoint[@name=$unassumedEP]/data[@id=$unassumedid]/@dataType),' ','_')"/>
  </xsl:when>
  <xsl:otherwise>
 <xsl:value-of select="translate(translate(normalize-space(document($root/coupled/models/model)
               /definition[name=$modelName]/entryPoints
               /entryPoint[@name=$epName]/data[@id=$id]/@dataType),' ','_'),'*','')"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>



<xsl:template name="getSize">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:choose>
  <xsl:when test="document($root/coupled/models/model)
             /definition[name=$modelName]/entryPoints
             /entryPoint[@name=$epName]/data[@id=$id]/@dataType='character'">
   <xsl:text>len(arg1)</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:variable name="list">
    <xsl:choose>
     <xsl:when test="number(document($root/coupled/models/model)
               /definition[name=$modelName]/entryPoints
               /entryPoint[@name=$epName]/data[@id=$id]/@dimension) > 0">
      <xsl:for-each select="document($root/coupled/models/model)
                    /definition[name=$modelName]/entryPoints
                    /entryPoint[@name=$epName]/data[@id=$id]/dim">
       <!-- we must have an upper bound but lower bound is optional -->
       <xsl:choose>
         <xsl:when test="lower/integer and upper/integer">
           <xsl:value-of select="number(upper/integer)-number(lower/integer)+1"/><xsl:text>:</xsl:text>
         </xsl:when>
         <xsl:when test="upper/integer">
           <xsl:value-of select="number(upper/integer)"/><xsl:text>:</xsl:text>
         </xsl:when>
         <xsl:otherwise> <!-- error, we must have an upper, and may have a lower -->
           <xsl:message terminate="yes">
             <xsl:text>Error, expecting a mandatory upper/integer and an optional lower/integer. Offence is by model '</xsl:text>
             <xsl:value-of select="$modelName"/>
             <xsl:text>' ep '</xsl:text>
             <xsl:value-of select="$epName"/>
             <xsl:text>' field id '</xsl:text>
             <xsl:value-of select="$id"/>
             <xsl:text>'</xsl:text>
           </xsl:message>
         </xsl:otherwise>
         
       </xsl:choose>
      </xsl:for-each>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>1:</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
<!--
    <xsl:message terminate="no">
     <xsl:value-of select="$newline"/>
     <xsl:text>in getSize, list=</xsl:text>
     <xsl:value-of select="$list"/>
     <xsl:value-of select="$newline"/>
     <xsl:value-of select="$modelName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$epName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$id"/>
     <xsl:text>. size=</xsl:text>
     <xsl:value-of select="document($root/coupled/models/model)
                  /definition[name=$modelName]/entryPoints
                  /entryPoint[@name=$epName]/data[@id=$id]/@dimension"/>
    </xsl:message>
-->
   <xsl:variable name="prod">
    <xsl:call-template name="product">
     <xsl:with-param name="list" select="$list"/>
    </xsl:call-template>
   </xsl:variable>
<!--
    <xsl:message terminate="no">
     <xsl:value-of select="$newline"/>
     <xsl:text>product=</xsl:text>
     <xsl:value-of select="$prod"/>
    </xsl:message>
-->
   <xsl:choose>
    <xsl:when test="number($prod)">
     <xsl:value-of select="$prod"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>size(arg1)</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>



<xsl:template name="product">
 <xsl:param name="list"/>
 <xsl:param name="total" select="1"/>
 <xsl:choose>
  <xsl:when test="substring-after($list,':')">
   <xsl:call-template name="product">
    <xsl:with-param name="list" select="substring-after($list,':')"/>
    <xsl:with-param name="total" select="$total * substring-before($list,':')"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="$total * substring-before($list,':')"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="zero">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:variable name="mytype" select="document($root/coupled/models/model)/definition[name=$modelName]
                                     /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dataType"/>
 <xsl:choose>
  <xsl:when test="$mytype='logical'">
   <xsl:text>.false.</xsl:text>
  </xsl:when>
  <xsl:when test="$mytype='integer'">
   <xsl:text>0</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>0.0</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!--called from InPlace/4/SendRecv.xsl-->
<xsl:template name="mpiIncludeTarget">
  <include name="mpi"/>
</xsl:template>

<xsl:template name="mpiDeclareSendRecvVars">
  <declare name="ierr" datatype="integer" dimension="0"/>
<!--  integer, dimension(1:mpi_status_size) :: istatus -->
  <declare name="istatus" datatype="integer" dimension="1">
   <dim order="1">
    <lower>
     <value>1</value>
    </lower>
   </dim>
   <dim order="2">
    <upper>
     <value>mpi_status_size</value>
    </upper>
   </dim>
  </declare>
</xsl:template>

<xsl:template name="mpiInitComms">
     <xsl:variable name="TotNumSUs">
   <xsl:value-of select="count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit)"/>
     </xsl:variable>
     <xsl:variable name="TotNumThreads">
   <xsl:value-of select="sum(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit/@threads)"/>
     </xsl:variable>
     <xsl:variable name="myDUID" select="ancestor::code/@id"/>
     <xsl:variable name="LocalNumThreads">
   <!-- needed to add +0 to $myDUID to let the xslt processor know it is an integer - I think this is a bug -->
   <xsl:value-of select="sum(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[($myDUID+0)]/sequenceUnit/@threads)"/>
     </xsl:variable>

    <Includes>
     <include name="mpi"/>
    </Includes>
    <DataDecs>
     <declare name="ierr" datatype="integer"/>
     <declare name="rc" datatype="integer"/>
     <declare name="globalsize" datatype="integer"/>
     <declare name="globalrank" datatype="integer"/>
     <declare name="colour" datatype="integer"/>
     <declare name="key" datatype="integer"/>
     <declare name="localsize" datatype="integer"/>
     <declare name="localrank" datatype="integer"/>
     <declare name="b2mtemp({$TotNumSUs})" datatype="integer"/>
     <declare name="mpi_comm_local" datatype="integer"/>
    </DataDecs>
    <Content>
<!--
    <comment><xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($myDUID)]/sequenceUnit"><xsl:value-of select="@threads"/><xsl:text> </xsl:text></xsl:for-each></comment>
-->
     <call name="mpi_init"><arg name="ierr"/></call>
     <call name="mpi_comm_size"><arg name="mpi_comm_world"/><arg name="globalsize"/><arg name="ierr"/></call>
     <call name="mpi_comm_rank"><arg name="mpi_comm_world"/><arg name="globalrank"/><arg name="ierr"/></call>
     <xsl:if test="$mpivis!='false'">
      <call name="mpivis_init"><arg name="globalrank"/></call>
     </xsl:if>
     <comment>arbitrarily decide on a unique colour for this deployment unit</comment>
     <assign lhs="colour" rhs="{$myDUID}"/>
     <if><condition><notEquals><value>globalsize</value><value><xsl:value-of select="$TotNumThreads"/></value></notEquals></condition>
     <action>
     <print>
       <string>Error: (du </string>
       <var name="colour"/>
       <string>):</string>
       <string><xsl:value-of select="$TotNumThreads"/><xsl:text> threads should be requested</xsl:text></string>
     </print>
     <print>
       <string>aborting ...</string>
     </print>
     <call name="mpi_abort"><arg name="mpi_comm_world"/><arg name="rc"/><arg name="ierr"/></call>
     </action>
     <else>
     <action>
      <assign lhs="key" rhs="0"/>
      <call name="mpi_comm_split"><arg name="mpi_comm_world"/><arg name="colour"/><arg name="key"/><arg name="mpi_comm_local"/><arg name="ierr"/></call>
      <call name="mpi_comm_size"><arg name="mpi_comm_local"/><arg name="localsize"/><arg name="ierr"/></call>
      <call name="mpi_comm_rank"><arg name="mpi_comm_local"/><arg name="localrank"/><arg name="ierr"/></call>
      <if><condition><notEquals><value>localsize</value><value><xsl:value-of select="$LocalNumThreads"/></value></notEquals></condition>
      <action>
       <print>
         <string><xsl:text>Error: </xsl:text><xsl:value-of select="$LocalNumThreads"/><xsl:text> threads expected in du </xsl:text></string>
         <var name="colour"/>
       </print>
       <print>
         <string>aborting ...</string>
       </print>
       <call name="mpi_abort"><arg name="mpi_comm_world"/><arg name="rc"/><arg name="ierr"/></call>
      </action>
      <else>
      <action>
        <comment>arbitrarily bind model threads to a local rank (and therefore a global rank)</comment>
        <assign lhs="b2mtemp" rhs="0"/>
<!--
        <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($myDUID)]/sequenceUnit/model">

        <xsl:variable name="myModelName" select="@name"/>
        <xsl:variable name="allSUOnThisDU" select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[sequenceUnit/model/@name=$myModelName]/sequenceUnit"/>
        <xsl:variable name="mySUPosition">
          <xsl:for-each select="$allSUOnThisDU">
            <xsl:if test="model/@name=$myModelName">
              <xsl:value-of select="position()"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="myHi">
          <xsl:value-of select="sum($allSUOnThisDU[position() &lt;= $mySUPosition]/@threads)-1"/>
        </xsl:variable>
        <xsl:variable name="myLo">
          <xsl:value-of select="sum($allSUOnThisDU[position()+1 &lt;= $mySUPosition]/@threads)"/>
        </xsl:variable>

        <xsl:variable name="bfgSUID">
          <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
            <xsl:if test="model/@name=$myModelName">
              <xsl:value-of select="position()"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
        <if><condition><and><greaterThanOrEqual><value>localrank</value><value><xsl:value-of select="$myLo"/></value></greaterThanOrEqual><lessThanOrEqual><value>localrank</value><value><xsl:value-of select="$myHi"/></value></lessThanOrEqual></and></condition>
        <action>
        <comment><xsl:text>model name is '</xsl:text><xsl:value-of select="$myModelName"/><xsl:text>'</xsl:text></comment>
        <assign lhs="bfgSUID" rhs="{$bfgSUID}"/>
        <if>
        <print><string>[mpiid=</string><value>globalrank</value><string>mpiduid=</string><value>localrank</value><string>] I am allocated to model '{$modelName}'</string></print>
        <condition><equals><value>localrank</value><value><xsl:value-of select="$myLo"/></value></equals></condition>
        <action>
          <assign lhs="b2mtemp(bfgSUID)" rhs="globalrank"/>
        </action>
        </if>
        </action>
        </if>
        </xsl:for-each>
-->
        <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($myDUID)]/sequenceUnit">

        <xsl:variable name="myModelName" select="model[1]/@name"/>
        <xsl:variable name="instance" select="model[1]/@instance"/>
        <xsl:variable name="allSUOnThisDU" select="document($root/coupled/deployment)
                      /deployment/deploymentUnits/deploymentUnit[sequenceUnit/model[@name=$myModelName and 
                      (not($instance) or @instance=$instance)]]/sequenceUnit"/>
        <xsl:variable name="mySUPosition">
          <xsl:for-each select="$allSUOnThisDU">
            <xsl:if test="model[@name=$myModelName and (not($instance) or @instance=$instance)]">
              <xsl:value-of select="position()"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="myHi">
          <xsl:value-of select="sum($allSUOnThisDU[position() &lt;= $mySUPosition]/@threads)-1"/>
        </xsl:variable>
        <xsl:variable name="myLo">
          <xsl:value-of select="sum($allSUOnThisDU[position()+1 &lt;= $mySUPosition]/@threads)"/>
        </xsl:variable>

        <xsl:variable name="bfgSUID">
          <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
            <xsl:if test="model[@name=$myModelName and (not($instance) or @instance=$instance)]">
              <xsl:value-of select="position()"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
        <if><condition><and><greaterThanOrEqual><value>localrank</value><value><xsl:value-of select="$myLo"/></value></greaterThanOrEqual><lessThanOrEqual><value>localrank</value><value><xsl:value-of select="$myHi"/></value></lessThanOrEqual></and></condition>
        <action>
        <xsl:for-each select="model">
        <comment><xsl:text>model name is '</xsl:text><xsl:value-of select="@name"/><xsl:text>'</xsl:text></comment>
        </xsl:for-each>
        <assign lhs="bfgSUID" rhs="{$bfgSUID}"/>
        <if>
        <print><string>[mpiid=</string><value>globalrank</value><string>mpiduid=</string><value>localrank</value><string>] I am allocated to model '{$modelName}'</string></print>
        <condition><equals><value>localrank</value><value><xsl:value-of select="$myLo"/></value></equals></condition>
        <action>
          <assign lhs="b2mtemp(bfgSUID)" rhs="globalrank"/>
        </action>
        </if>
        </action>
        </if>
        </xsl:for-each>

        <if>
          <condition>
            <or>
                <lessThan><value>localrank</value><value>0</value></lessThan>
                <greaterThan><value>localrank</value><value><xsl:value-of select="number($LocalNumThreads)-1"/></value></greaterThan>
            </or>
          </condition>
          <action>
            <print>
              <string>'Error: (du </string>
              <var name="colour"/>
              <string>,0): localrank has unexpected value</string>
            </print>
            <print>
              <string>aborting ...</string>
            </print>
            <call name="mpi_abort"><arg name="mpi_comm_world"/><arg name="rc"/><arg name="ierr"/></call>
          </action>
        <else>
          <action>
<!--
            <comment>generate an index of bfg id's to mpi globalrank</comment>
-->
            <comment>distribute id's to all su's</comment>
            <call name="mpi_allreduce"><arg name="b2mtemp"/><arg name="b2mmap"/><arg name="{$TotNumSUs}"/><arg name="mpi_integer"/><arg name="mpi_sum"/><arg name="mpi_comm_world"/><arg name="ierr"/></call>
            <if><condition><equals><value>localrank</value><value>0</value></equals></condition>
            <action>
              <print><string>du </string><var name="colour"/><string> bfg to mpi id map is </string><var name="b2mmap"/></print>
            </action>
            </if>
          </action>
        </else>
        </if>
      </action>
      </else>
      </if>
    </action>
    </else>
    </if>

    </Content>
</xsl:template>

<xsl:template name="mpiFinaliseComms">
    <Includes>
     <include name="mpi"/>
    </Includes>
    <DataDecs>
     <declare name="globalrank" datatype="integer"/>
     <declare name="ierr" datatype="integer"/>
    </DataDecs>
    <Content>
     <xsl:if test="$mpivis!='false'">
      <call name="mpi_comm_rank"><arg name="mpi_comm_world"/><arg name="globalrank"/><arg name="ierr"/></call>
      <call name="mpivis_final"><arg name="globalrank"/></call>
     </xsl:if>
     <call name="mpi_finalize">
      <arg name="ierr"/>
     </call>
    </Content>
</xsl:template>

<xsl:template name="mpiCommsSync">
   <include name="mpi"/>
   <declare name="ierr" datatype="integer" dimension="0"/>
   <call name="mpi_barrier"><arg name="mpi_comm_world"/><arg name="ierr"/></call>
</xsl:template>

<xsl:template name="getMessageTag">
  <xsl:param name="modelName"/>
  <xsl:param name="epName"/>
  <xsl:param name="id"/>
  <xsl:for-each select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data">
    <xsl:if test="@id=$id and ancestor::entryPoint[@name=$epName] and ancestor::definition[name=$modelName]">
      <xsl:value-of select="position()"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
