<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template name="esmfInitComms">
    <Includes/>
    <DataDecs/>
      <declare name="rc" datatype="integer"/>
      <declare name="cname" datatype="string" dimension="1">
        <stringSize><size><value>ESMF_MAXSTR</value></size></stringSize>
      </declare>
      <declare name="localPet" datatype="integer"/>
      <declare name="petCount" datatype="integer"/>
      <declare name="peCount" datatype="integer"/>
    <Content>
<!--
         call ESMF_Initialize(vm=vm, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 single model wrapped in esmf"
         comp1 = ESMF_GridCompCreate(name=cname, petlist=(/0/), &
                                                 gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         cname = "bfg2 multiple model wrapped in esmf"
         comp2 = ESMF_GridCompCreate(name=cname, petlist=(/1/), &
                                                 gridcompType=ESMF_ATM, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(comp1, user_register_single, rc)
         if (rc .ne. ESMF_SUCCESS) return

         call ESMF_GridCompSetServices(comp2, user_register_multiple, rc)
         if (rc .ne. ESMF_SUCCESS) return

         imp = ESMF_StateCreate("igrid import state", ESMF_STATE_IMPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return
         exp = ESMF_StateCreate("igrid export state", ESMF_STATE_EXPORT, rc=rc)
         if (rc .ne. ESMF_SUCCESS) return

         ! Check we have the correct number of threads
         call ESMF_VMGet(vm, localPet=localPet, petCount=petCount, &
                                                        peCount=peCount, rc=rc)

         ! store my thread ID
         threadID=localPet
         print *, "Running on ",petCount," threads."
         ! RF Not sure of the difference between petCount and peCount
         !!print *, "There are ", peCount," PEs referenced by this VM"
         if (petCount.ne.2) then
           print *,"Error, expecting 2 threads, terminating"
           call ESMF_finalize(rc=rc)
         end if
-->
    </Content>
</xsl:template>

<xsl:template name="esmfFinaliseComms">
    <Includes/>
    <DataDecs/>
    <Content>
     <comment>Need to add ESMF finalise comms specifics here</comment>
    </Content>
</xsl:template>

<xsl:template name="esmfIncludeTarget">
   <include name="ESMF_Mod"/>
</xsl:template>

<xsl:template name="esmfDeclareSendRecvVars">
   <comment>Need to add ESMF specifics here</comment>
</xsl:template>

<xsl:template name="esmfCommsSync">
   <comment>Need to add ESMF comms sync specifics here</comment>
</xsl:template>

<xsl:template name="esmfModuleData">

<declare name="comp1" derived="true" datatype="ESMF_GridComp"/>
<declare name="comp2" derived="true" datatype="ESMF_GridComp"/>
<declare name="imp" derived="true" datatype="ESMF_State"/>
<declare name="exp" derived="true" datatype="ESMF_State"/>
<declare name="vm" derived="true" datatype="ESMF_VM"/>
<declare name="threadID" datatype="integer"/>

</xsl:template>

</xsl:stylesheet>
