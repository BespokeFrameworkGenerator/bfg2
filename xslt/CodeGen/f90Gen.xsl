<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
xmlns:str="http://exslt.org/strings"
  extension-element-prefixes="xalan str">

<!--
    RF 22nd Sept 2005

    By default the units for opening and closing files will
    begin at 10. This base value can be changed
    by the argument -PARAM BaseUnit <value>. Note, in future
    versions of BFG2, appropriate unit values will be calculated
    automatically i.e. automatically avoiding any unit numbers
    required by models.
-->


<xsl:variable name="sevenSp" select="'       '"/>
<!-- IRH: mpif.xsl was included to process <send> and <receive>
elements, but the way we process <send> and <receive> and
the resultant code differs according to target type (OASIS4 uses
prism_put and prism_get, not mpi_send and mpi_recv).  Therefore
we process <send> and <receive> elements in InPlacePhase5.xsl,
which matches <send> and <receive> and redirects to named templates 
mpiSend, mpiReceive, oasis4Send, oasis4Receive instead. These named
templates generate XML pseudocode - <call name="mpi_recv... etc., 
which is translated to Fortran here, rather than going directly from
<send>/<receive> to Fortran as previously. -->
<!--<xsl:include href="mpif.xsl"/>-->
<xsl:param name="BaseUnit" select="'10'"/>
<xsl:key name="declarations" match="declare" use="@ref"/>

<xsl:output method="text"/>
<xsl:variable name="space" select="' '"/>

<xsl:template match="EP">
 <xsl:value-of select="$sevenSp"/>
 <xsl:choose>
  <xsl:when test="@type='entryPoint'">
   <xsl:text>module</xsl:text>
  </xsl:when>
  <xsl:when test="@type='program'">
   <xsl:text>program</xsl:text>
  </xsl:when>
  <xsl:when test="@type='function'">
   <xsl:value-of select="@return"/>
   <xsl:if test="@recursive='true'">
    <xsl:text> recursive</xsl:text>
   </xsl:if>
   <xsl:text> function</xsl:text>
  </xsl:when>
  <xsl:when test="@type='procedure'">
   <xsl:if test="@recursive='true'">
    <xsl:text> recursive</xsl:text>
   </xsl:if>
   <xsl:text>subroutine</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:value-of select="$newline"/>
    <xsl:text>Error in f90Gen EPStart: "</xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>" is not supported</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="$space"/>
 <xsl:value-of select="@name"/>
 <xsl:if test="@type='function' or @type='procedure'">
  <xsl:text>(</xsl:text>
  <xsl:for-each select="arg">
   <xsl:if test="position()=1">
    <!--<xsl:text>(</xsl:text>-->
   </xsl:if>
   <xsl:choose>
    <xsl:when test="boolean(@name)">
     <xsl:value-of select="@name"/>
    </xsl:when>
    <xsl:when test="boolean(@value)">
     <xsl:value-of select="@value"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>arg</xsl:text><xsl:value-of select="position()"/>
    </xsl:otherwise>
   </xsl:choose>
   <xsl:choose>
    <xsl:when test="position()=last()">
     <!--<xsl:text>)</xsl:text>-->
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>,</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
  <xsl:value-of select="$newline"/>

  <xsl:for-each select="include | Includes/include">
 <xsl:value-of select="$sevenSp"/>
 <xsl:choose>
  <xsl:when test="@type='file'">
   <xsl:text>include '</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>'</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>use </xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:for-each select="rename">
    <xsl:if test="position()=1">
     <xsl:text>, only : </xsl:text>
    </xsl:if>
    <xsl:value-of select="@to"/>
    <xsl:text>=&gt;</xsl:text>
    <xsl:value-of select="@from"/>
    <xsl:if test="position()!=last()">
     <xsl:text>,&amp;</xsl:text>
     <xsl:value-of select="$newline"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="$newline"/>
  </xsl:for-each>


  <xsl:value-of select="$sevenSp"/>
  <xsl:text>implicit none</xsl:text>
 </xsl:if>
 <xsl:value-of select="$newline"/>
 <xsl:if test="@type='function' or @type='procedure'">
  <xsl:for-each select="arg">
   <xsl:value-of select="$sevenSp"/>

   <xsl:choose>
     <xsl:when test="@type='string'">

       <xsl:text>character (len=</xsl:text>
       <!-- use the dimension template to specify the string length.
         This works as we use the same schema structure ro specify
         string length as we do for dimension size -->
       <xsl:call-template name="roo-dims">
         <xsl:with-param name="dims" select="stringSize"/>
       </xsl:call-template>
       <xsl:text>)</xsl:text>

     </xsl:when>
     <xsl:when test="@type='double'">
       <xsl:text>double precision</xsl:text>
     </xsl:when>
     <xsl:otherwise>
       <xsl:value-of select="@type"/>
     </xsl:otherwise>
   </xsl:choose>

   <xsl:value-of select="$space"/>

   <xsl:if test="string(@kind)">
    <xsl:text>(kind=</xsl:text>
    <xsl:value-of select="@kind"/>
    <xsl:text>)</xsl:text>
   </xsl:if>

   <xsl:value-of select="$space"/>

   <xsl:if test="boolean(@intent)">
     <xsl:text>, intent(</xsl:text>
     <xsl:value-of select="@intent"/>
     <xsl:text>)</xsl:text>
   </xsl:if>

   <xsl:if test="@rank &gt; 0">
     <xsl:text>, dimension(</xsl:text>

     <xsl:choose>
       <!-- the whole array is assumed shape -->
       <xsl:when test="@shape='assumed'">
         <xsl:call-template name="print-dimensions">
           <xsl:with-param name="count" select="@rank"/>
           <xsl:with-param name="symbol" select="':'"/>
         </xsl:call-template>
       </xsl:when>
       <!-- 1 dimensional array with unknown size -->
       <xsl:when test="@shape='unassumed'">
         <xsl:if test="not(@rank='1')">
           <xsl:message terminate="yes">
             <xsl:text>Error: unassumed used when the dimension is not one</xsl:text>
           </xsl:message>
         </xsl:if>
         <xsl:text>*</xsl:text>
       </xsl:when>
       <!-- go through each argument -->
       <xsl:otherwise>
         <xsl:call-template name="roo-dims">
           <xsl:with-param name="dims" select="dim"/>
         </xsl:call-template>
       </xsl:otherwise>
     </xsl:choose>


     <xsl:text>)</xsl:text>
   </xsl:if>

   <xsl:text> :: </xsl:text>
   <xsl:choose>
    <xsl:when test="boolean(@name)">
     <xsl:value-of select="@name"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:text>arg</xsl:text><xsl:value-of select="position()"/>
    </xsl:otherwise>
   </xsl:choose>
   <xsl:value-of select="$newline"/>
  </xsl:for-each>
 </xsl:if>
 <xsl:apply-templates select="*"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end </xsl:text>
 <xsl:choose>
  <xsl:when test="@type='entryPoint'">
   <!-- module program -->
   <xsl:text>module</xsl:text>
  </xsl:when>
  <xsl:when test="@type='procedure'">
   <xsl:text>subroutine</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="@type"/>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="$space"/>
 <xsl:value-of select="@name"/>
 <!--output subroutines for file reads-->
 <xsl:value-of select="$newline"/>
 <xsl:for-each select=".//file">
  <xsl:value-of select="$newline"/>
  <xsl:variable name="unitNum">
   <xsl:call-template name="unit"/>
  </xsl:variable>
  <xsl:if test="@type='namelist' and namelist/data/@fileref">
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>subroutine nmlinit</xsl:text>
   <xsl:value-of select="$unitNum"/>
   <xsl:text>(</xsl:text>
   <xsl:for-each select=".//data[@used='true']">
    <xsl:value-of select="@fileref"/>
    <xsl:if test="position()!=last()">
     <xsl:text>,</xsl:text>
    </xsl:if>
   </xsl:for-each>
   <xsl:text>)</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text> implicit none</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:for-each select=".//data">
    <xsl:variable name="myref" select="@ref"/>
    <xsl:variable name="myname" select="@fileref"/>
    <xsl:value-of select="$sevenSp"/>

<!--
Need to get a reference to one of the definitions of this variable... use a key!
-->
    <xsl:variable name="declaration" select="key('declarations',$myref)[1]"/>

    <xsl:value-of select="$declaration/@datatype"/>

    <xsl:if test="$declaration/@datatype = 'string'">
      <xsl:text>(len=*)</xsl:text>
    </xsl:if>

    <xsl:if test="$declaration/dim">

      <xsl:if test="$declaration/dim//variable">
         <xsl:text>, allocatable</xsl:text>
      </xsl:if>

      <xsl:text>, dimension(</xsl:text>

      <xsl:variable name="isAllocatable">
        <xsl:if test="$declaration/dim//variable">
          <xsl:text>T</xsl:text>
        </xsl:if>
      </xsl:variable>

      <xsl:for-each select="$declaration/dim">
        <xsl:sort select="@order"/>
        <xsl:choose>

          <xsl:when test="$isAllocatable='T' and upper">
            <xsl:text>:</xsl:text>
            <xsl:choose>
              <xsl:when test="position()=last()">
                <xsl:text>)</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>,</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="$isAllocatable='T' and lower"/>

          <xsl:when test="lower">
            <xsl:apply-templates select="lower/*"/>
            <xsl:text>:</xsl:text>
          </xsl:when>

          <xsl:when test="upper">
            <xsl:apply-templates select="upper/*"/>
            <xsl:choose>
              <xsl:when test="position()=last()">
                <xsl:text>)</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>,</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

        </xsl:choose>

      </xsl:for-each>

    </xsl:if>

    <xsl:text> :: </xsl:text>
    <xsl:value-of select="$myname"/>
    <xsl:value-of select="$newline"/>
   </xsl:for-each>
   <xsl:for-each select="namelist">
    <xsl:value-of select="$sevenSp"/>
    <xsl:text>namelist /</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>/ </xsl:text>
    <xsl:for-each select="data">
     <xsl:value-of select="@fileref"/>
     <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
     </xsl:if>
    </xsl:for-each>
    <xsl:value-of select="$newline"/>
   </xsl:for-each>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>open(unit=</xsl:text>
   <xsl:value-of select="$unitNum"/>
   <xsl:text>,file='</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>')</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:for-each select="namelist">
    <xsl:value-of select="$sevenSp"/>
    <xsl:text>read(</xsl:text>
    <xsl:value-of select="$unitNum"/>
    <xsl:text>,</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>)</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:for-each>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>close(</xsl:text>
   <xsl:value-of select="$unitNum"/>
   <xsl:text>)</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>end subroutine nmlinit</xsl:text>
   <xsl:value-of select="$unitNum"/>
   <xsl:value-of select="$newline"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template match="Separator">
 <!-- only add a separator if we have at least one entrypoint defined -->
 <!-- some codegen keeps the <Content> container some doesn't, hence
      do the two tests -->
 <xsl:if test="boolean(../Content/EP) or boolean(../EP)">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>contains</xsl:text>
 <xsl:value-of select="$newline"/>
 </xsl:if>
</xsl:template>

<xsl:template match="include">
<xsl:if test="not(ancestor::EP/@type='procedure' or ancestor::EP/@type='function')">
 <xsl:value-of select="$sevenSp"/>
 <xsl:choose>
  <xsl:when test="@type='file'">
   <xsl:text>include '</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>'</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>use </xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:for-each select="rename">
    <xsl:if test="position()=1">
     <xsl:text>, only : </xsl:text>
    </xsl:if>
    <xsl:value-of select="@to"/>
    <xsl:text>=&gt;</xsl:text>
    <xsl:value-of select="@from"/>
    <xsl:if test="position()!=last()">
     <xsl:text>,&amp;</xsl:text>
     <xsl:value-of select="$newline"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="$newline"/>
</xsl:if>
</xsl:template>

<xsl:template match="Common">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>common/</xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:text>/</xsl:text>
 <xsl:for-each select="value">
  <xsl:value-of select="@name"/>
  <xsl:if test="position()!=last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="interface">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>interface </xsl:text>
 <xsl:value-of select="@genericName"/>
 <xsl:value-of select="$newline"/>
 <!-- check if we have any procedures to define -->
 <xsl:if test="count(procedure)">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>module procedure </xsl:text>
 <xsl:for-each select="procedure">
  <xsl:value-of select="@name"/>
  <xsl:value-of select="@dataType"/>
  <xsl:text>_</xsl:text><xsl:value-of select="@kind"/><xsl:text>_</xsl:text>
  <xsl:value-of select="@rank"/>
  <xsl:if test="position()!=last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
 </xsl:if>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end interface</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="private">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>private </xsl:text>
 <xsl:for-each select="procedure">
  <xsl:value-of select="@name"/>
  <xsl:if test="position()!=last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="return">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>return</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="continue">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>continue</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="public">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>public :: </xsl:text>
 <xsl:for-each select="procedure">
  <xsl:value-of select="@name"/>
  <xsl:if test="position()!=last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="if">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>if (</xsl:text>
 <xsl:apply-templates select="condition"/>
 <xsl:text>) then</xsl:text>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="action"/>
 <xsl:apply-templates select="elseif"/>
 <xsl:apply-templates select="else"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end if</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="while">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>do while(</xsl:text>
 <xsl:apply-templates select="condition"/>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="action"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end do</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="else">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>else</xsl:text>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="action"/>
</xsl:template>

<xsl:template match="elseif">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>else if (</xsl:text>
 <xsl:apply-templates select="condition"/>
 <xsl:text>) then</xsl:text>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="action"/>
 <xsl:apply-templates select="elseif"/>
 <xsl:apply-templates select="else"/>
</xsl:template>

<!--binary operators-->
<xsl:template match="greaterThan">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.gt.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="lessThan">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.lt.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="greaterThanOrEqual">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.ge.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="lessThanOrEqual">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.le.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="equals">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>==</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="notEquals">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.ne.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="and">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.and.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="or">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>.or.</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="plus">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>+</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="minus">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>-</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="times">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>*</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="divide">
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>/</xsl:text>
 <xsl:apply-templates select="*[2]"/>
</xsl:template>

<xsl:template match="modulo">
 <xsl:text>mod(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>,</xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<!--unary operators-->
<xsl:template match="not">
 <xsl:text>.not.(</xsl:text>
 <xsl:apply-templates select="*"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="negative">
 <xsl:text>-</xsl:text>
 <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="bracket">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="value">
 <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="variable">
 <xsl:choose>
  <xsl:when test="boolean(@name)">
   <xsl:value-of select="@name"/>
  </xsl:when>
  <xsl:when test="number(@ref)">
   <xsl:text>r</xsl:text><xsl:value-of select="@ref"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="@ref"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>



<xsl:template match="iterate">
 <!-- output loop -->
 <xsl:value-of select="$sevenSp"/>
 <!--cwa added for experiments-->
<!--
 <xsl:if test="substring-after(@end,'nts')='1'">
  <xsl:text>call cpu_time(t1)</xsl:text>
  <xsl:value-of select="$newline"/>
  <xsl:value-of select="$sevenSp"/>
 </xsl:if>
-->
 <xsl:text>do </xsl:text>
 <xsl:choose>
  <xsl:when test="boolean(@counter)">
   <xsl:value-of select="@counter"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>its</xsl:text><xsl:value-of select="substring-after(@end,'nts')"/>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:text>=</xsl:text>
 <xsl:value-of select="@start"/>
 <xsl:text>,</xsl:text>
 <xsl:value-of select="@end"/>
 <xsl:if test="boolean(@increment)">
  <xsl:text>,</xsl:text>
  <xsl:value-of select="@increment"/>
 </xsl:if>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="*"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end do</xsl:text>
 <xsl:value-of select="$newline"/>
 <!--cwa added for experiments-->
<!--
 <xsl:if test="substring-after(@end,'nts')='1'">
  <xsl:text>call cpu_time(t2)</xsl:text>
  <xsl:value-of select="$newline"/>
  <xsl:value-of select="$sevenSp"/>
  <xsl:text>print*,"cpu time=",t2-t1</xsl:text>
  <xsl:value-of select="$newline"/>
 </xsl:if>
-->
</xsl:template>

<!-- IRH: Added support for assigning string literals -->
<!-- IRH: TODO: need a more sophisticated assign element - use
subelements for operands and operators, array subscripts etc. - 
currently Fortran operators and array subscripts () are embedded
in higher level XSL files breaking language independence. -->
<xsl:template match="assign">
 <xsl:value-of select="$sevenSp"/>
 <xsl:value-of select="@lhs"/>
 <xsl:text>=</xsl:text>
 <xsl:if test="@type = 'string-literal'">
  <xsl:text>'</xsl:text>
 </xsl:if>
 <!-- IRH: Added support for line wrapping.
 TODO: I introduced this to cope with the land mask assignment required for
 OASIS targets (which otherwise would result in a line several thousand chars
 long).  It could be applied elsewhere in f90gen. -->
 <xsl:call-template name="writeWrappedLine">
  <xsl:with-param name="longLine" select="@rhs"/>
 </xsl:call-template>
 <xsl:if test="@type = 'string-literal'">
  <xsl:text>'</xsl:text>
 </xsl:if>
 <xsl:value-of select="$newline"/>
</xsl:template>


<xsl:template name="writeWrappedLine">
  <xsl:param name="longLine"/>
  <xsl:param name="columnToWrapAt" select="80"/>

  <xsl:choose>
    <xsl:when test="function-available('str:tokenize')">
      <xsl:call-template name="writeLineTokens">
        <xsl:with-param name="lineTokens" select="str:tokenize($longLine)"/>
        <xsl:with-param name="columnToWrapAt" select="$columnToWrapAt"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="function-available('xalan:tokenize')">
      <xsl:call-template name="writeLineTokens">
        <xsl:with-param name="lineTokens" select="xalan:tokenize($longLine)"/>
        <xsl:with-param name="columnToWrapAt" select="$columnToWrapAt"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>Error: writeWrappedLine: no function to tokenize mask values.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="writeLineTokens">
  <xsl:param name="lineTokens"/>
  <xsl:param name="columnToWrapAt"/>
  <xsl:param name="lineLength" select="0"/>

  <xsl:choose>
    <xsl:when test="count( $lineTokens ) = 1">
      <xsl:value-of select="$lineTokens[ 1 ]"/>
    </xsl:when>
    <xsl:when test="$lineLength > $columnToWrapAt">
      <xsl:text>&amp;</xsl:text>
      <xsl:value-of select="$newline"/>
      <xsl:value-of select="$sevenSp"/>
      <xsl:text>&amp;</xsl:text>
      <xsl:call-template name="writeLineTokens">
        <xsl:with-param name="lineTokens" select="$lineTokens"/>
        <xsl:with-param name="columnToWrapAt" select="$columnToWrapAt"/>
        <xsl:with-param name="lineLength" select="0"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="tokenString" select="concat($lineTokens[ 1 ], ' ')"/>
      <xsl:value-of select="$tokenString"/>
      <xsl:call-template name="writeLineTokens">
        <xsl:with-param name="lineTokens" select="$lineTokens[ position() > 1 ]"/>
        <xsl:with-param name="columnToWrapAt" select="$columnToWrapAt"/>
        <xsl:with-param name="lineLength" select="$lineLength + string-length($tokenString)"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- IRH: Added template for "compound" assignments.
These are assignments where the operands consists of other
XML pseudocode statements, e.g.:
  <minus>
    <times>
      <integer>2</integer>
      <integer>3</integer>
    </times>
    <integer>5</integer>
  </minus>
Note that the LHS of an assignment may also be a compound
statement - e.g. references to array/structure elements.
We don't currently use XML pseudocode for arrays/structures though.
-->
<xsl:template match="cassign">
 <xsl:value-of select="$sevenSp"/>
 <xsl:apply-templates select="lhs/*"/>
 <xsl:text>=</xsl:text>
 <xsl:apply-templates select="rhs/*"/>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="point">
 <xsl:value-of select="$sevenSp"/>
 <xsl:value-of select="@lhs"/>
 <xsl:text>=></xsl:text>
 <xsl:value-of select="@rhs"/>
 <xsl:value-of select="$newline"/>
</xsl:template>

<!--cwa:added templates below for modulo timestepping-->
<xsl:template match="timestep">
 <xsl:value-of select="$sevenSp"/>
 <!--cwa: question: what should the mod be relative to, the global outer loop or the parent (possibly inner) loop?
          opted for the latter in the below-->
 <xsl:text>if(mod(its</xsl:text>
 <xsl:for-each select="ancestor::iterate">
  <xsl:if test="position()=last()">
   <xsl:value-of select="substring-after(@end,'nts')"/>
  </xsl:if>
 </xsl:for-each>
 <xsl:text>,</xsl:text>
 <xsl:value-of select="@variable"/>
 <xsl:text>).eq.</xsl:text>
 <xsl:choose>
  <xsl:when test="@offset!=''">
   <xsl:value-of select="@offset"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>0</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:text>)then</xsl:text>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="*"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end if</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<!-- IRH: Added support for string literal arguments
and string variables that should be treated as read-only
(these will be trimmed) -->
<xsl:template match="call">
 <xsl:variable name="modName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:choose>
  <xsl:when test="@type='function'">
   <xsl:value-of select="$sevenSp"/>
   <xsl:if test="boolean(@return)">
    <xsl:value-of select="@return"/>
    <xsl:text>=</xsl:text>
   </xsl:if>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>call </xsl:text>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="@name"/>
 <xsl:text>(</xsl:text>
 <xsl:for-each select="arg">
  <xsl:choose>
   <xsl:when test="boolean(@name)">
    <xsl:if test="@type = 'string' and @readonly = 'true'">
     <xsl:text>trim(</xsl:text>
    </xsl:if>
    <xsl:value-of select="@name"/>
    <xsl:if test="@type = 'string' and @readonly = 'true'">
     <xsl:text>)</xsl:text>
    </xsl:if>
   </xsl:when>
   <xsl:when test="@value">
    <xsl:if test="@type = 'string-literal'">
     <xsl:text>'</xsl:text>
    </xsl:if>
    <xsl:value-of select="@value"/>
    <xsl:if test="@type = 'string-literal'">
     <xsl:text>'</xsl:text>
    </xsl:if>
   </xsl:when>
   <xsl:otherwise>
    <xsl:text>arg</xsl:text><xsl:value-of select="position()"/>
   </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="position()!=last()">
   <xsl:text>,</xsl:text>
  </xsl:if>
 </xsl:for-each>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>


<!-- IRH: Added separate template for definitions of derived types 
as distinct from declarations of instances of derived types. -->
<xsl:template match="derive">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>type </xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:value-of select="$newline"/>
 <xsl:apply-templates select="declare"/>
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>end type </xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:value-of select="$newline"/>
</xsl:template>


<xsl:template match="declare">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>!</xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:value-of select="$newline"/>
 <xsl:value-of select="$sevenSp"/>
 <!-- IRH: Added support for declarations of instances of derived types -->
 <xsl:if test="@derived = 'true'">
  <xsl:text>type(</xsl:text>
 </xsl:if>
 
 <xsl:choose>
   <xsl:when test="@datatype='string'">

     <xsl:text>character (len=</xsl:text>
     <!-- use the dimension template to specify the string length.
          This works as we use the same schema structure ro specify
          string length as we do for dimension size -->
     <xsl:call-template name="roo-dims">
       <xsl:with-param name="dims" select="stringSize"/>
     </xsl:call-template>
     <xsl:text>)</xsl:text>

   </xsl:when>
   <xsl:when test="@datatype='double'">

     <xsl:text>double precision</xsl:text>

   </xsl:when>
   <xsl:otherwise>

     <xsl:value-of select="@datatype"/>

   </xsl:otherwise>
 </xsl:choose>

 <xsl:if test="@kind!=''">
  <xsl:text>(kind=</xsl:text><xsl:value-of select="@kind"/><xsl:text>)</xsl:text>
 </xsl:if>
 <xsl:if test="@derived = 'true'">
  <xsl:text>)</xsl:text>
 </xsl:if>

  <xsl:if test="dim"> 
    <xsl:call-template name="print-declared-dimensions">
      <xsl:with-param name="dims" select="dim"/>
    </xsl:call-template>
  </xsl:if>

  <xsl:if test="boolean(@intent)">
    <xsl:text>, intent(</xsl:text>
    <xsl:value-of select="@intent"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
 <!-- IRH: Added support for pointers and pointer targets (doing 
 datatype="integer, pointer/target" doesn't work for pointers to derived types,
 as well as not being language neutral).  For languages like C that don't
 require a "target" qualifier, the target="true" attribute can be ignored. -->
 <xsl:if test="@pointer='true'">
  <xsl:text>, pointer</xsl:text>
 </xsl:if>
 <xsl:if test="@target='true'">
  <xsl:text>, target</xsl:text>
 </xsl:if>
 <xsl:if test="@parameter='true'">
  <xsl:text>, parameter</xsl:text>
 </xsl:if>
 <xsl:text> :: </xsl:text>
 <xsl:choose>
  <xsl:when test="number(@ref)">
   <xsl:text>r</xsl:text>
   <xsl:value-of select="@ref"/>
  </xsl:when>
  <xsl:when test="@ref">
   <xsl:value-of select="@ref"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="@name"/>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:if test="boolean(@value)">
  <xsl:text>=</xsl:text><xsl:value-of select="@value"/>
 </xsl:if>
 <xsl:value-of select="$newline"/>
</xsl:template>


<xsl:template match="var">
 <xsl:value-of select="$sevenSp"/>
 <xsl:variable name="myref" select="@ref"/>
 <xsl:choose>
  <xsl:when test="not(../../../DataDecs/declare[@ref=$myref]/firstRef)">
   <xsl:text>    </xsl:text>
   <xsl:if test="number(@ref)">
    <xsl:text>r</xsl:text>
   </xsl:if>
   <xsl:value-of select="@ref"/>
   <xsl:text>=</xsl:text>
   <xsl:if test="../../../DataDecs/declare[@ref=$myref]/@datatype='character'
                 or ../../../DataDecs/declare[@ref=$myref]/@datatype='string'">
    <xsl:text>'</xsl:text>
   </xsl:if>
   <xsl:value-of select="@value"/>
   <xsl:if test="../../../DataDecs/declare[@ref=$myref]/@datatype='character'
                 or ../../../DataDecs/declare[@ref=$myref]/@datatype='string'">
    <xsl:text>'</xsl:text>
   </xsl:if>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>    ! </xsl:text><xsl:value-of select="@ref"/><xsl:text>is allocatable; cannot be written to yet</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
 <xsl:value-of select="$newline"/>
</xsl:template>

<!-- IRH: Added template to NULLIFY a pointer -->
<xsl:template match="nullify">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>nullify(</xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<!-- IRH: Added template to check for pointer equality.
Arguments:
 pointer (required)
 target (optional)
If target isn't supplied, pointer is just checked to
see whether it's associated with anything/non-NULL.
If associated() is part of an if or loop condition,
no whitespace is inserted before or after. -->
<xsl:template match="associated">
 <xsl:if test="not(ancestor::condition)">
  <xsl:value-of select="$sevenSp"/>
 </xsl:if>
 <xsl:text>associated(</xsl:text>
 <xsl:value-of select="@pointer"/>
 <xsl:if test="@target">
  <xsl:text>,</xsl:text>
  <xsl:value-of select="@target"/>
 </xsl:if>
 <xsl:text>)</xsl:text>
 <xsl:if test="not(ancestor::condition)">
  <xsl:value-of select="$newline"/>
 </xsl:if>
</xsl:template>

<!-- IRH: Added a check to confirm that the array
is allocated before trying to deallocate (may want 
to add an assert here if it isn't). -->
<xsl:template match="deallocate">
 <xsl:value-of select="$sevenSp"/>
 <xsl:choose>
  <xsl:when test="@type = 'array'">
   <xsl:text>if(allocated(</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>))</xsl:text>
  </xsl:when>
  <xsl:when test="@type = 'pointer'">
   <xsl:text>if(associated(</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>))</xsl:text>
  </xsl:when>
  <!-- Don't test the parameter if we don't know its type. -->
  <xsl:otherwise/>
 </xsl:choose>
 <xsl:text>deallocate(</xsl:text>
 <xsl:value-of select="@name"/>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="allocate">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>allocate(</xsl:text>
 <xsl:value-of select="@name"/>
 <!-- IRH: Only add dimensions if there are any -->
 <xsl:if test="dim">
   <xsl:text>(</xsl:text>

   <xsl:call-template name="roo-dims">
     <xsl:with-param name="dims" select="dim"/>
   </xsl:call-template>

   <xsl:text>)</xsl:text>
 </xsl:if>
 <xsl:text>)</xsl:text>

 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="printOLD">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>print*,"</xsl:text>
 <xsl:value-of select="normalize-space(.)"/>
 <xsl:text>"</xsl:text>
 <xsl:for-each select="var">
  <xsl:text>,</xsl:text>
  <xsl:value-of select="@name"/>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="print">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>print *</xsl:text>
 <xsl:for-each select="*">
   <xsl:choose>
     <xsl:when test="name()='var'">
       <xsl:text>,</xsl:text>
       <xsl:choose>
         <xsl:when test="@sum='true' and @dimension &gt; 0">
	   <xsl:text>sum(</xsl:text>
           <xsl:value-of select="@name"/>
           <xsl:text>)</xsl:text>
         </xsl:when>
         <xsl:otherwise>
           <xsl:value-of select="@name"/>
         </xsl:otherwise>
       </xsl:choose>
     </xsl:when>
     <xsl:when test="name()='string'">
       <xsl:text>,"</xsl:text>
       <xsl:value-of select="text()"/>
       <xsl:text>"</xsl:text>
     </xsl:when>
   </xsl:choose>
 </xsl:for-each>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="comment">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>! </xsl:text>
 <xsl:value-of select="normalize-space(node())"/>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template name="unit">
 <xsl:param name="myName" select="@name"/>
 <xsl:for-each select="//file">
  <xsl:if test="@name=$myName">
   <xsl:value-of select="position()-1+$BaseUnit"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template match="file">
 <xsl:choose>
  <xsl:when test="@type='namelist'">
   <xsl:choose>
    <xsl:when test="namelist/data/@fileref">
     <xsl:value-of select="$sevenSp"/>
     <xsl:text>if(</xsl:text>
      <xsl:for-each select="model">
       <xsl:value-of select="@name"/>
       <xsl:if test="@instance!=''">
        <xsl:text>inst</xsl:text>
        <xsl:value-of select="@instance"/>
       </xsl:if>
       <xsl:text>Thread()</xsl:text>
       <xsl:if test="position()!=last()">
        <xsl:text>.or.</xsl:text>
       </xsl:if>
      </xsl:for-each>
     <xsl:text>)then</xsl:text>
     <xsl:value-of select="$newline"/>
     <xsl:variable name="unitNum">
      <xsl:call-template name="unit"/>
     </xsl:variable>
     <xsl:value-of select="$sevenSp"/>
     <xsl:text>call nmlinit</xsl:text>
     <xsl:value-of select="$unitNum"/>
     <xsl:text>(</xsl:text>
     <xsl:for-each select=".//data[@used='true']">
      <xsl:if test="number(@ref)">
       <xsl:text>r</xsl:text>
      </xsl:if>
      <xsl:value-of select="@ref"/>
      <xsl:if test="position()!=last()">
       <xsl:text>,</xsl:text>
      </xsl:if>
     </xsl:for-each>
     <xsl:text>)</xsl:text>
     <xsl:value-of select="$newline"/>
     <xsl:value-of select="$sevenSp"/>
     <xsl:text>end if</xsl:text>
     <xsl:value-of select="$newline"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:variable name="unitNum">
      <xsl:call-template name="unit"/>
     </xsl:variable>
     <xsl:value-of select="$sevenSp"/>
     <xsl:text>open(unit=</xsl:text>
     <xsl:value-of select="$unitNum"/>
     <xsl:text>,file='</xsl:text>
     <xsl:value-of select="@name"/>
     <xsl:text>')</xsl:text>
     <xsl:value-of select="$newline"/>
     <xsl:for-each select="namelist">
      <xsl:value-of select="$sevenSp"/>
      <xsl:text>read(</xsl:text>
      <xsl:value-of select="$unitNum"/>
      <xsl:text>,</xsl:text>
      <xsl:value-of select="@name"/>
      <xsl:text>)</xsl:text>
      <xsl:value-of select="$newline"/>
     </xsl:for-each>
     <xsl:value-of select="$sevenSp"/>
     <xsl:text>close(</xsl:text>
     <xsl:value-of select="$unitNum"/>
     <xsl:text>)</xsl:text>
     <xsl:value-of select="$newline"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="@type='netcdf'">
   <!-- open the netcdf file -->
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>status = nf90_open("</xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>",nf90_NoWrite,ncid)</xsl:text>
   <xsl:value-of select="$newline"/>
   <xsl:value-of select="$sevenSp"/>
   <xsl:text>if (status /= nf90_noerr) stop "Error in netcdf file open for </xsl:text>
   <xsl:value-of select="@name"/>
   <xsl:text>"</xsl:text>
   <xsl:value-of select="$newline"/>
   <!-- for each data element inquire for our id -->
   <xsl:for-each select="data">
    <xsl:value-of select="$newline"/>
    <xsl:text>status = nf90_inq_varid(ncid, "</xsl:text>
    <xsl:value-of select="@fileref"/>
    <xsl:text>", varid)</xsl:text>
    <xsl:value-of select="$newline"/>
    <xsl:value-of select="$sevenSp"/>
    <xsl:text>if (status /= nf90_noerr) stop "Error in netcdf inquire for </xsl:text>
    <xsl:value-of select="@fileref"/>
    <xsl:text> in </xsl:text>
    <xsl:value-of select="ancestor::file/@name"/>
    <xsl:text>"</xsl:text>
    <xsl:value-of select="$newline"/>
    <xsl:value-of select="$sevenSp"/>
    <xsl:text>status = nf90_get_var(ncid, varid, r</xsl:text>
    <xsl:value-of select="@ref"/>
    <xsl:text>)</xsl:text>
    <xsl:value-of select="$newline"/>
    <xsl:value-of select="$sevenSp"/>
    <xsl:text>if (status /= nf90_noerr) stop "Error in netcdf get_var for r</xsl:text>
    <xsl:value-of select="@ref"/>
    <xsl:text> in </xsl:text>
    <xsl:value-of select="ancestor::file/@name"/>
    <xsl:text>"</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:for-each>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:value-of select="$newline"/>
    <xsl:text>Error in f90Create, template match file, filetype </xsl:text>
    <xsl:value-of select="@type"/>
    <xsl:text>is unsupported</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template match="decnamelist">
 <xsl:if test="count(nmldata/@fileref)=0">
  <xsl:value-of select="$sevenSp"/>
  <xsl:text>namelist /</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>/ </xsl:text>
  <xsl:for-each select="nmldata">
   <xsl:choose>
    <xsl:when test="@name">
     <xsl:value-of select="@name"/>
    </xsl:when>
    <xsl:when test="@fileref">
     <xsl:value-of select="@fileref"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:message terminate="yes">
      <xsl:text>Error in f90Create, template decnamelist</xsl:text>
     </xsl:message>
    </xsl:otherwise>
   </xsl:choose>
   <xsl:if test="position()!=last()">
    <xsl:text>,</xsl:text>
   </xsl:if>
  </xsl:for-each>
  <xsl:value-of select="$newline"/>
 </xsl:if>
</xsl:template>


<!--Catch children of unmatched nodes-->
<xsl:template match="node()">
 <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="arg"/>


<xsl:template name="print-dimensions"> 
 <xsl:param name="count"/>
 <xsl:param name="symbol"/>
 <xsl:choose>
  <xsl:when test="$count=1">
   <xsl:value-of select="$symbol"/>
  </xsl:when>
  <xsl:when test="$count &gt; 0">
   <xsl:value-of select="$symbol"/><xsl:text>,</xsl:text>
   <xsl:call-template name="print-dimensions">
    <xsl:with-param name="count" select="$count - 1"/>
    <xsl:with-param name="symbol" select="$symbol"/>
   </xsl:call-template>
  </xsl:when>
 </xsl:choose>
</xsl:template>

<!-- IRH: Placed code for processing the dimensions of arrays 
in data declaration sections into a named template. -->
<xsl:template name="print-declared-dimensions"> 
 <xsl:param name="dims"/>
 <!-- IRH: Don't use "allocatable" keyword if we are declaring
 a pointer to an array. -->
 <xsl:if test="dim//variable and (not(boolean(@pointer)) or @pointer != 'true')">
  <xsl:text>, allocatable</xsl:text>
 </xsl:if>
 <xsl:variable name="isAllocatable">
  <xsl:if test="dim//variable">
   <xsl:text>T</xsl:text>
  </xsl:if>
 </xsl:variable>
 <xsl:text>, dimension(</xsl:text>

 <xsl:call-template name="roo-dims">
     <xsl:with-param name="dims" select="$dims"/>
     <xsl:with-param name="isAllocatable" select="$isAllocatable"/>
 </xsl:call-template>

 <xsl:text>)</xsl:text>

</xsl:template>

<!--
 <xsl:for-each select="dim">
  <xsl:sort select="@order"/>
  <xsl:choose>
   <xsl:when test="lower">
    <xsl:apply-templates select="lower/*"/>
    <xsl:text>:</xsl:text>
   </xsl:when>
     <xsl:when test="position()=last()">
      <xsl:text>)</xsl:text>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>,</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:when>
  </xsl:choose>
 </xsl:for-each>
-->

<xsl:template name="roo-dims">
<xsl:param name="dims"/>
<xsl:param name="isAllocatable"/>

 <xsl:for-each select="$dims">
  <xsl:sort select="@order"/>
  <xsl:choose>
   
      <xsl:when test="size">
          <xsl:apply-templates select="size/*"/>
          <xsl:if test="position() &lt; last()">
              <xsl:text>,</xsl:text>
          </xsl:if>
      </xsl:when>

      <xsl:when test="lower">
          <xsl:choose>
              <xsl:when test="$isAllocatable='T'"/>
              <xsl:otherwise>

                  <xsl:if test="not(lower/value=1)">
                      <xsl:apply-templates select="lower/*"/>
                      <xsl:text>:</xsl:text>
                  </xsl:if>

              </xsl:otherwise>
          </xsl:choose>
      </xsl:when>

      <xsl:when test="upper">
          <xsl:choose>

              <xsl:when test="$isAllocatable='T'">
                  <xsl:text>:</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:apply-templates select="upper/*"/>
              </xsl:otherwise>

          </xsl:choose>
          <xsl:if test="position() &lt; last()">
              <xsl:text>,</xsl:text>
          </xsl:if>
      </xsl:when>

      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:text>f90Gen.xsl: roo-dims Unexpected element </xsl:text>
          <xsl:text>Expecting lower, upper or size but found </xsl:text>
          <xsl:value-of select="name()"/>
        </xsl:message>
      </xsl:otherwise>

    </xsl:choose>

 </xsl:for-each>

</xsl:template>

<xsl:template match="open">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>open(</xsl:text>
 <xsl:for-each select="*">
  <xsl:value-of select="local-name(.)"/><xsl:text>=</xsl:text><xsl:value-of select="."/>
  <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
 </xsl:for-each>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="write">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>write(</xsl:text>
 <xsl:for-each select="*">
  <xsl:value-of select="local-name(.)"/><xsl:text>=</xsl:text><xsl:value-of select="."/>
  <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
 </xsl:for-each>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>

<xsl:template match="close">
 <xsl:value-of select="$sevenSp"/>
 <xsl:text>close(</xsl:text>
 <xsl:for-each select="*">
  <xsl:value-of select="local-name(.)"/><xsl:text>=</xsl:text><xsl:value-of select="."/>
  <xsl:if test="position()!=last()"><xsl:text>,</xsl:text></xsl:if>
 </xsl:for-each>
 <xsl:text>)</xsl:text>
 <xsl:value-of select="$newline"/>
</xsl:template>


<xsl:template name="print-dims">
  <xsl:param name="dims"/>

 <xsl:text>XXX</xsl:text>
 <xsl:for-each select="$dims">
  <xsl:sort select="@order"/>
  <xsl:choose>
   <xsl:when test="size">
    <xsl:apply-templates select="size/*"/>
    <xsl:if test="position() &lt; last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
   </xsl:when>
   <xsl:when test="lower">
     <xsl:apply-templates select="lower/*"/>
     <xsl:text>:</xsl:text>
   </xsl:when>
   <xsl:when test="upper">
    <xsl:apply-templates select="upper/*"/>
    <xsl:if test="position() &lt; last()">
     <xsl:text>,</xsl:text>
    </xsl:if>
   </xsl:when>
  </xsl:choose>
 </xsl:for-each>
 <xsl:text>XXX</xsl:text>

</xsl:template>


</xsl:stylesheet>
