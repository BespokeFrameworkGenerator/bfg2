<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
xmlns:common="http://exslt.org/common"
  extension-element-prefixes="common xalan">

<!--
  extension-element-prefixes="common">
    RF 19th Sept 2005

    Phase 4 of applying xsl templates to code template

    By default the filename is BFG2Main.f90
    however the argument -PARAM FileName <MyFile>
    will change this to <MyFile>.f90

    xalan redirect output is placed relative to the
    root input xml document, not the current wd !!
    -PARAM OutDir <path> can be used to output where
    required. By default the output is in the same
    directory as the specified (-IN) xml document.
-->

<xsl:output method="text"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>
<xsl:param name="OutDir" select="'.'"/>
<xsl:param name="FileName" select="'BFG2Main.f90'"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:param name="f90Only" select="'false'"/>

<!-- RF: these are not used
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
-->

<xsl:include href="f90Gen.xsl"/>

<xsl:template match="code">
 <xsl:variable name="FilePath">
  <xsl:choose>
   <xsl:when test="@fullname">
    <xsl:value-of select="concat($OutDir, '/', @fullname)"/>
   </xsl:when>
   <xsl:when test="@name">
    <xsl:value-of select="concat($OutDir, '/', substring-before($FileName,'.'), '_', @name, '.', substring-after($FileName,'.'))"/>
   </xsl:when>
   <xsl:when test="@id">
    <xsl:value-of select="concat($OutDir, '/', substring-before($FileName,'.'), @id, '.', substring-after($FileName,'.'))"/>
   </xsl:when>
  </xsl:choose>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="element-available('common:document')">
   <common:document href="{$FilePath}" method="text">
    <xsl:apply-templates select="*"/>
   </common:document>
  </xsl:when>
  <xsl:when test="element-available('xalan:write')">
   <xalan:write select="$FilePath">
    <xsl:apply-templates/>
   </xalan:write>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>No file output support found</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
