<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
  exclude-result-prefixes="bfg2"
xmlns:xalan="http://xml.apache.org/xalan"
xmlns:xalanredirect="org.apache.xalan.xslt.extensions.Redirect"
xmlns:str="http://exslt.org/strings"
xmlns:common="http://exslt.org/common"
  extension-element-prefixes="common xalan str xalanredirect">

<!-- For processing of gridless grid dimensions. -->
<xsl:include href="../Utils/Expr.xsl"/>

<!-- For getting unique IDs for sequence units, deployment units,
and entry point instances in the schedule. -->
<xsl:include href="../Utils/SequenceUnitID.xsl"/>
<xsl:include href="../Utils/DeploymentUnitID.xsl"/>
<xsl:include href="../Utils/InSequenceContextID.xsl"/>

<!-- Define some default values -->
<xsl:param name="thisFileName" select="'oasis4.xsl'"/>
<xsl:param name="sccFileName" select="'scc.xml'"/>
<xsl:param name="nDriverProcs" select="'1'"/>
<xsl:param name="invalidArgumentID" select="'-1'"/>
<xsl:param name="invalidBFGRef" select="'-1'"/>
<xsl:param name="gridlessGridPrefix" select="'gridless_grid_for_set_'"/>
<xsl:param name="gridlessGridType" select="'none'"/>
<xsl:param name="notIntrinsic" select="'not_intrinsic'"/>
<xsl:param name="oasisvis" select="'false'"/>
<xsl:param name="fortranFalse" select="'.false.'"/>
<xsl:param name="fortranTrue" select="'.true.'"/>

<!-- IRH: TODO: Error handling - need to process ierror for prism API calls. -->
<!-- IRH: TODO: Use gridspec that's split into separate files + updated. -->

<!-- IRH: Some assumptions I've made:

1) Every sequence unit in deploy.xml represents an OASIS4-coupled component.
2) All point-to-point comms take place using OASIS4 (prism_put/get) - no
   notion of mixed targets (MPI and OASIS coupled models within the same
   framework).
3) All sequence units/components have only one rank associated with them
   (I am not considering parallel models, which require a partitioning
   scheme - unnecessary for GENIE coupling).
4) All coupling and grids are 2D.
-->


<!-- Create declarations for any "global" (module-level) data required
for OASIS4 communications. -->
<xsl:template name="oasis4GlobalData">
  <xsl:variable name="myDeploymentUnitID" select="ancestor::code/@id"/>
  <xsl:variable name="myDeploymentUnit" select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($myDeploymentUnitID)]"/>
  <xsl:variable name="mySequenceUnits" select="$myDeploymentUnit/sequenceUnit"/>

     <comment>Constant declarations</comment>

     <declare name="name_len_max" value="256" datatype="integer" parameter="true"/>
     <!-- IRH: TODO: GRIDSPEC: 3 dimensions. The number of dimensions
     depends on the grid type, see table 4.8 in OASIS user guide. -->
     <declare name="ndim" value="3" datatype="integer" parameter="true"/>
     <xsl:variable name="modelEntryPointInstanceCount"
       select="count(document($root/coupled/deployment)//schedule//model)"/>
     <declare name="ep_instance_count" value="{$modelEntryPointInstanceCount}" datatype="integer" parameter="true"/>

     <comment>Derived type definitions</comment> 
     
     <!-- The shape type can be used to represent areas or volumes
     of a grid (depending on number of grid dimensions) using bounds 
     that are indexes into arrays of coords. -->
     <derive name="shape_type">
      <declare name="bounds" datatype="integer">
       <dim>
        <upper>
         <!-- A minimum and maximum bound index for each dimension
         of the shape -->
         <value>2</value>
        </upper>
       </dim>
       <dim>
        <upper>
         <value>ndim</value>
        </upper>
       </dim>
      </declare>
     </derive>

     <!-- The sizes for these arrays could be set here, but the XSL is
     simpler if we allocate them and initialise their contents all in one
     place (in the oasis4InitGriddedGrid template) -->
     <derive name="coords_type">
      <declare name="longitudes" datatype="real" allocatable="true">
       <dim>
        <upper>
         <value><variable/></value>
        </upper>
       </dim>
      </declare>
      <declare name="latitudes" datatype="real" allocatable="true">
       <dim>
        <upper>
         <value><variable/></value>
        </upper>
       </dim>
      </declare>
      <declare name="verticals" datatype="real" allocatable="true">
       <dim>
        <upper>
         <value><variable/></value>
        </upper>
       </dim>
      </declare>
     </derive>

     <derive name="corners_type">
      <declare name="actual_shape" datatype="shape_type" derived="true"/>
      <declare name="coords" datatype="coords_type" derived="true"/>
     </derive>

     <derive name="points_type">
      <declare name="name" datatype="character">
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
      </declare>
      <declare name="id" datatype="integer"/>
      <declare name="actual_shape" datatype="shape_type" derived="true"/>
      <declare name="coords" datatype="coords_type" derived="true"/>
     </derive>

     <derive name="mask_type">
      <declare name="id" datatype="integer"/>
      <declare name="actual_shape" datatype="shape_type" derived="true"/>
      <declare name="array" datatype="logical" allocatable="true">
       <dim>
        <upper>
         <value><variable/></value>
        </upper>
       </dim>
       <dim>
        <upper>
         <value><variable/></value>
        </upper>
       </dim>
      </declare>
     </derive>

     <derive name="grid_type"> 
      <declare name="name" datatype="character">
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
      </declare>
      <declare name="id" datatype="integer"/>
      <declare name="type_id" datatype="integer"/>
      <declare name="valid_shape" datatype="shape_type" derived="true"/>
      <!-- Note that we don't need corners, points, etc. if we have
      a "gridless" grid as points on gridless grids aren't associated with
      lats and lons (corners/points). -->
      <declare name="corners" datatype="corners_type" derived="true"/>
      <declare name="point_sets" datatype="points_type" derived="true">
       <dim>
        <upper>
         <!-- IRH: TODO: GRIDSPEC: number of point sets? Only 1 for GENIE.
         The way this will work (according to Balaji) is as follows: the
         gridspec will contain the "supergrid" - the superset of all points
         used by gridded data in a component model.  The NcML/NetCDF
         (or whatever) file that contains information about each field will
         also contain a reference to the gridspec and an array of indexes
         into the supergrid, thereby identifying the subgrid associated
         with the field.  Where will the coupling field info be held for BFG?
         definition.xml contains some info, but perhaps a NcML file would be
         more appropriate for detailed info about a coupling field?? -->
         <value>1</value>
        </upper>
       </dim>
      </declare>
      <declare name="landmask" datatype="mask_type" derived="true"/>
     </derive>
      
     <!-- Coupling field data that can vary according to entry point instance. 
     (It is only possible for a put to go to multiple arguments of an entry point;
     gets can come from one and only one argument of an entry point; however
     for simplicity both are defined as linked lists). -->
     <derive name="coupling_field_ep_instance_type">
      <declare name="get_instance" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
      <declare name="put_instances" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
     </derive>

     <!-- Coupling field data that can vary according to entry point argument instance. -->
     <derive name="coupling_field_arg_instance_type">
      <declare name="prism_id" datatype="integer"/>
      <declare name="bfg_ref" datatype="integer"/>
      <!-- An identifier for the coupling field, which is the same
      whether we're putting or getting it. -->
      <declare name="msg_tag" datatype="character">
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
      </declare>
      <declare name="next_instance" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
     </derive>

     <derive name="coupling_field_type">
      <!-- For fields that can be received from more than one source or sent to
      more than one destination, there will be more than one prism_id.  For such 
      fields, we have to tell OASIS4 that there's a separate transient for each 
      source as OASIS4 doesn't allow a transient to have multiple inputs.  OASIS
      does allow a transient to have multiple outputs, but it MPI_SENDs them all
      at once when the prism_put() is called - we don't want that behaviour, as 
      often only a subset of outputs should be sent when prism_put() is called - 
      it very much depends on the models being coupled, potentially BFG could use
      this feature of OASIS if we knew that all outputs were required at once,
      but this typically isn't the case for GENIE.
      Therefore ep_instances() has an entry for each model entry point instance
      used by BFG (it's the same size as the modelInfo "info" array in
      BFG2Target).  In practice, most of these entries will be unused - in fact
      only one will be used for components that have one input/output.
      N.B. It is not enough to index this array by the sequence unit number of
      the remote PRISM component - it is possible for there to be >1
      source or destination entry point instance for a coupling field within a
      component.  Indeed, there may be more than one destination within a 
      single entry point for a coupling field - in this case, a list of 
      coupling field instances is associated with the entry point 
      (see "put_instances").  It hints at bad design, but this situation does
      actually occur in GENIE.  -->
      <declare name="ep_instances" datatype="coupling_field_ep_instance_type" derived="true">
       <dim>
        <upper>
         <value>ep_instance_count</value>
        </upper>
       </dim>
      </declare>
      <declare name="bfg_id" datatype="character">
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
      </declare>
      <declare name="name" datatype="character">
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
      </declare>
      <declare name="nodims" datatype="integer">
       <dim>
        <upper>
         <value>2</value>
        </upper>
       </dim>
      </declare>
      <declare name="actual_shape" datatype="shape_type" derived="true"/>
      <declare name="type_id" datatype="integer"/>
     </derive>

     <derive name="component_type"> 
      <declare name="name" datatype="string">
       <stringSize><size><value>name_len_max</value></size></stringSize>
<!-- RF this is not an array of characters, it is a string -->
<!--
       <dim>
        <upper>
         <value>name_len_max</value>
        </upper>
       </dim>
-->
      </declare>
      <declare name="id" datatype="integer"/>
      <declare name="local_comm" datatype="integer"/>
      <declare name="gridded_grids" datatype="grid_type" derived="true">
       <dim>
        <upper>
         <!-- The size for this array could be set here, but the XSL is
         simpler if we allocate it and initialise the contents all in one
         place (in the oasis4InitCouplingData template). -->
         <variable/>
        </upper>
       </dim>
      </declare>
      <declare name="gridless_grids" datatype="grid_type" derived="true">
       <dim>
        <upper>
         <!-- The size for this array could be set here, but the XSL is
         simpler if we allocate it and initialise the contents all in one
         place (in the oasis4InitCouplingData template). -->
         <variable/>
        </upper>
       </dim>
      </declare>
      <declare name="coupling_fields" datatype="coupling_field_type" derived="true">
       <dim>
        <upper>
         <!-- The size for this array could be set here, but the XSL is
         simpler if we allocate it and initialise the contents all in one
         place (in the oasis4InitCouplingData template). -->
         <variable/>
        </upper>
       </dim>
      </declare>
      <declare name="coupling_fields_count" datatype="integer"/>
     </derive>

     <derive name="coupling_field_key_type"> 
      <declare name="coupling_field_no" datatype="integer"/>
     </derive>

     <comment>Variable declarations</comment>

     <!-- Generate a "key" for each coupling field that will be initialised
     during oasis4InitComms.  During coupling with prism_put and prism_get,
     this key saves us having to do a search for the field. -->
     <comment>Coupling field keys for this deployment unit</comment>
     <xsl:for-each select="$mySequenceUnits">
      <xsl:call-template name="getCouplingFieldKeys">
        <xsl:with-param name="sequenceUnit" select="."/>
      </xsl:call-template>
     </xsl:for-each>

     <declare name="component" datatype="component_type" derived="true" target="true"/>
     
      <!-- IRH: TODO: not sure whether these belong here... Also, can they
      be kept constant during a model run, or should they be updated
      for each timestep?  One problem with not changing the time is that
      the NetCDF files of coupling fields that are output by OASIS4 in
      debug mode do not have a time axis (since the time was the same every
      time coupling occurred). -->
      <declare name="model_time" datatype="PRISM_Time_Struct" derived="true"/>
      <declare name="model_time_bounds" datatype="PRISM_Time_Struct" derived="true">
       <dim>
        <upper>
         <!-- An upper and a lower bound -->
         <value>2</value>
        </upper>
       </dim>
      </declare>

      <!-- Variables for the rank of the current process and the ranks
      of all processes representing sequence units (assumed to be OASIS4
      components).  Currently assuming only one rank per OASIS4 component 
      process, rather than a range (as when coupling parallel models). -->
      <comment>Current rank, and all ranks for this deployment unit</comment>
      <declare name="my_local_rank" datatype="integer"/>
      <xsl:for-each select="$mySequenceUnits">
       <xsl:variable name="compName"> 
        <xsl:call-template name="createCompName">
         <xsl:with-param name="sequenceUnit" select="."/>
        </xsl:call-template>
       </xsl:variable>
       <declare name="{$compName}_rank" datatype="integer"/>
      </xsl:for-each>

</xsl:template>


<xsl:template name="oasis4InitComms">
  <xsl:variable name="myDeploymentUnitID" select="ancestor::code/@id"/>
  <xsl:variable name="myDeploymentUnit" select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($myDeploymentUnitID)]"/>
  <xsl:variable name="mySequenceUnits" select="$myDeploymentUnit/sequenceUnit"/>

    <Includes>
     <include name="prism"/>
     <comment>RF: FoX_wkml include was here but I removed it for the moment as it is included even if not needed</comment>
     <!--
     <include name="FoX_wkml"/>
     -->
    </Includes>
    <DataDecs>
     <comment>RF: kml_file_handle derived type was here but I removed it for the moment as it is included even if not needed</comment>
     <!--
     <declare name="kml_file_handle" datatype="xmlf_t" derived="true"/>
     -->
     <declare name="RANK_UNKNOWN" value="-1" datatype="integer" parameter="true"/>
     <declare name="ierror" datatype="integer"/>
     <declare name="comp_loop" datatype="integer"/>
     <declare name="points_loop" datatype="integer"/>
     <!-- Temporary pointers that can be used "for convenience" to make 
     the XSL and resultant source code more readable. -->
     <declare name="coupling_field" datatype="coupling_field_type" derived="true" pointer="true"/>
     <declare name="coupling_field_ep_inst" datatype="coupling_field_ep_instance_type" derived="true" pointer="true"/>
     <declare name="coupling_field_arg_inst" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
     <declare name="grid" datatype="grid_type" derived="true" pointer="true"/>
     <declare name="points" datatype="points_type" derived="true" pointer="true"/>
     <declare name="corners" datatype="corners_type" derived="true" pointer="true"/>
     <declare name="mask" datatype="mask_type" derived="true" pointer="true"/>
     <declare name="coord_array" datatype="real" pointer="true">
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
     </declare>

     <!-- Instead of one array with points_num + 1 entries for the corners
     (as in native GENIE coupling), OASIS requires *two* arrays each
     with points_num entries, the first for the left/top edge of each
     grid cell, the second for the right/bottom edge of each grid cell.
     I.e. it's the same coordinates, but stored differently. -->
     <declare name="corners_longitudes_OASIS4" datatype="real" allocatable="true">
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
     </declare>
     <declare name="corners_latitudes_OASIS4" datatype="real" allocatable="true">
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
     </declare>
     <declare name="corners_verticals_OASIS4" datatype="real" allocatable="true">
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
     </declare>

     <!-- An array for holding ALL point coordinates, only used for generation
     of points at runtime by a point generation function. loop variables below
     are used similarly.
     IRH: TODO: How to handle Fortran 90 real "kinds"? -->
     <declare name="all_coords_array" datatype="real" allocatable="true">
      <dim>
       <upper>
        <value><variable/></value>
       </upper>
      </dim>
     </declare>
     <declare name="index_loop" datatype="integer"/>
     <declare name="coord_loop" datatype="integer"/>
     <declare name="lon_loop" datatype="integer"/>
     <declare name="lat_loop" datatype="integer"/>

     <declare name="appl_name" datatype="string">
<!-- RF this is not an array of characters, it is a string -->
       <stringSize><size><value>name_len_max</value></size></stringSize>
<!--
      <dim>
       <upper>
        <value>name_len_max</value>
       </upper>
      </dim>
-->
     </declare>
     <declare name="local_comm" datatype="integer"/>
     <declare name="rank_lists" datatype="integer">
      <dim>
       <upper>
        <!-- Assuming only one ranklist per component -->
        <value>1</value>
       </upper>
      </dim>
      <dim>
       <upper>
        <!-- A minimum, maximum and increment for the ranks -->
        <value>3</value>
       </upper>
      </dim>
     </declare>

    </DataDecs>

    <Content>
     <comment>Set a PRISM application name for this deployment unit</comment>
     <xsl:variable name="deploymentUnitName">
      <xsl:call-template name="createAppName">
       <xsl:with-param name="deploymentUnit" select="$myDeploymentUnit"/>
      </xsl:call-template>
     </xsl:variable>
     <assign lhs="appl_name" rhs="{$deploymentUnitName}" type="string-literal"/>

     <comment>Initialise the coupling environment (must be called by each process)</comment>
     <call name="prism_init">
      <arg name="appl_name" type="string" readonly="true"/>
      <arg name="ierror"/>
     </call>

     <comment>Get the rank for the current process</comment>
     <comment>This rank is relative to the current deployment unit - it is not global</comment>
     <call name="prism_get_localcomm">
      <arg value="PRISM_appl_id"/>
      <arg name="local_comm"/>
      <arg name="ierror"/>
     </call>
     <call name="MPI_Comm_rank">
      <arg name="local_comm"/>
      <arg name="my_local_rank"/>
      <arg name="ierror"/>
     </call>

     <comment>Reset all sequence unit ranks</comment>
     <!-- IRH: TODO: For BFG MPI implementations, b2mmap is filled with the
     ranks for all SUs, regardless of whether they're part of the current DU.
     With OASIS4 I'm not sure how to do this or whether it's possible -
     MPI_COMM_WORLD includes a DU (the OASIS4 transformer app) that we can't
     generate code for - for instance the MPI_Allreduce() call used in mpif.xsl
     to fill the b2mmap array will block indefinitely waiting for the OASIS4
     transformer to also call it.  Therefore, I've decided that b2mmap will
     contain "RANK_UNKNOWN" for any SUs that are outside the current DU.
     Because of the way b2mmap is used for OASIS4 frameworks, we don't actually
     need to know the exact rank for SUs outside the current DU anyway - all we
     need to know is that they *are* outside the current DU, and therefore
     require a prism_put/get in BFG2InPlace.
     UPDATE: Rene Redler has proposed adding a subroutine to PSMILe to 
     expose a global communicator. -->
     <assign lhs="b2mmap" rhs="RANK_UNKNOWN"/>

     <!-- IRH: TODO: Assuming only one rank and one ranklist per component 
     - not coupling parallel models ATM -->
     <comment>Get the ranks for all component processes</comment>
     <xsl:for-each select="$mySequenceUnits">

      <xsl:variable name="compName"> 
       <xsl:call-template name="createCompName">
        <xsl:with-param name="sequenceUnit" select="."/>
       </xsl:call-template>
      </xsl:variable>
      <!-- hack as get_ranklists appears to be broken
      <call name="prism_get_ranklists">
       <arg value="{$compName}" type="string-literal"/>
       <arg value="1"/>
       <arg name="rank_lists"/>
       <arg name="ierror"/>
      </call>
      -->
      <assign lhs="rank_lists(1,1)" rhs="my_local_rank"/>
      <assign lhs="{$compName}_rank" rhs="rank_lists(1,1)"/>

      <xsl:variable name="bfgSUID">
        <xsl:call-template name="getSequenceUnitID">
          <xsl:with-param name="modelName" select="model[1]/@name"/>
          <xsl:with-param name="modelInstance" select="model[1]/@instance"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:if test="$oasisvis!='false'">
       <call name="oasisvis_init">
        <arg value="{$bfgSUID}"/>
       </call>
      </xsl:if>
          
      <comment>Store mapping from sequence unit number to local rank</comment>
      <!-- IRH: TODO: for OASIS4 targets we don't even need to store the rank
      for SUs that are part of the same DU - all we need to know is that they're
      not in the same SU, so require a prism_put/get.  The calculation of
      bfgSUID, above, and the assigment, below, could both be moved inside 
      the "if" below. -->
      <assign lhs="b2mmap({$bfgSUID})" rhs="{$compName}_rank"/>
      
      <comment>Initialise the PRISM component</comment> 
      <if>
       <condition><equals>
        <value>my_local_rank</value><value><xsl:value-of select="concat( $compName, '_rank' )"/></value>
       </equals></condition>
       <action>
        
        <comment>Assign sequence unit number (unique across deployment units)</comment>
        <assign lhs="bfgSUID" rhs="{$bfgSUID}"/>
        
        <assign lhs="component%name" rhs="{$compName}" type="string-literal"/>
        <call name="prism_init_comp">
         <arg name="component%id"/>
         <arg name="component%name" type="string" readonly="true"/>
         <arg name="ierror"/>
        </call>
        <call name="prism_get_localcomm">
         <arg name="component%id"/>
         <arg name="component%local_comm"/>
         <arg name="ierror"/>
        </call>

        <comment>Initialise grids and coupling data for component: <xsl:value-of select="$compName"/></comment>

        <xsl:call-template name="oasis4InitCouplingData">
         <xsl:with-param name="sequenceUnit" select="."/>
        </xsl:call-template>

       </action>
      </if>
     </xsl:for-each>

     <comment>Initialisation phase is complete</comment>

     <!-- RF: Hack : if no variables defined then do not call prism_enddef -->
     <xsl:variable name="content">
     <xsl:for-each select="$mySequenceUnits">
        <xsl:call-template name="oasis4InitCouplingData">
         <xsl:with-param name="sequenceUnit" select="."/>
        </xsl:call-template>
     </xsl:for-each>
     </xsl:variable>

     <xsl:if test="$content!=''">
     <call name="prism_enddef">
      <arg name="ierror"/>
     </call>
     </xsl:if>

     <comment>Set the date/time bounds within which coupling will be valid</comment>
     <!-- IRH: TODO: Usually, the time is incremented for each iteration - this
     information can possibly be got from one of the BFG DCD files.  However
     keeping the time constant seems to work.  Whether that's a good idea is
     another matter?! -->
     <assign lhs="model_time" rhs="PRISM_jobstart_date"/>
     <assign lhs="model_time_bounds(1)" rhs="model_time"/>
     <assign lhs="model_time_bounds(2)" rhs="model_time"/>
     <call name="prism_calc_newdate">
      <arg name="model_time_bounds(1)"/>
      <arg value="-3600.0"/>
      <arg name="ierror"/>
     </call>
     <call name="prism_calc_newdate">
      <arg name="model_time_bounds(2)"/>
      <arg value="3600.0"/>
      <arg name="ierror"/>
     </call>
    </Content>
</xsl:template>


<xsl:template name="getCouplingFieldKeys">
  <xsl:param name="sequenceUnit"/>

  <xsl:variable name="sequenceUnitID">
    <xsl:call-template name="getSequenceUnitID">
      <xsl:with-param name="modelName" select="$sequenceUnit/model[1]/@name"/>
      <xsl:with-param name="modelInstance" select="$sequenceUnit/model[1]/@instance"/>
    </xsl:call-template>
  </xsl:variable>

  <!-- Search for entry point arguments that require point-to-point communications
  (these are currently assumed to be OASIS4 coupling fields) -->
  <xsl:variable name="entryPointArgs" select="$commFormsRoot/communicationForms/tuple[
    @su = $sequenceUnitID and (contains(@form,'get') or contains(@form,'put'))]"/>
  <xsl:for-each select="$entryPointArgs">

    <!-- Get the definition of the coupling field in the definition doc. -->
    <xsl:variable name="couplingField" select="document($root/coupled/models/model)/
      definition[name = current()/@modelName]//
      entryPoint[@name = current()/@epName]/data[@id = current()/@id]"/>

    <!--
    <comment>Entry point name: <xsl:value-of select="current()/@epName"/></comment>
    <comment>Argument ID: <xsl:value-of select="current()/@id"/></comment>
    <comment>Argument name: <xsl:value-of select="$couplingField/@name"/></comment>
    <comment>Argument type: <xsl:value-of select="$couplingField/@dataType"/></comment>
    -->
    
    <!-- Note that the argument's name is optional; although we store it, we also
    generate a name (BFGID, below) guaranteed to be unique using the argument ID,
    model name (and instance number), and entry point name - e.g. "mod1_run_arg1".  -->
    <xsl:variable name="couplingFieldBFGID">
      <xsl:call-template name="getCouplingFieldBFGID">
        <xsl:with-param name="modelName" select="@modelName"/>
        <xsl:with-param name="instanceID" select="@instance"/>
        <xsl:with-param name="entryPointName" select="@epName"/>
        <xsl:with-param name="argumentID" select="@id"/>
      </xsl:call-template>
    </xsl:variable>

    <comment>The BFG ID string for this coupling field is: 
    <xsl:value-of select="$couplingFieldBFGID"/></comment>
    <declare name="{$couplingFieldBFGID}" datatype="coupling_field_key_type" derived="true"/>

  </xsl:for-each>
</xsl:template>


<!-- Initialise the given coupling field. -->
<xsl:template name="oasis4InitCouplingField">
  <xsl:param name="couplingField"/>
  <xsl:param name="entryPointArgTuple"/>
  <xsl:param name="couplingFieldIndex"/>
  <xsl:param name="gridType"/>
  <xsl:param name="gridIndex"/>

  <!-- Note that the argument's name is optional; although we store it, we also
  generate a name (BFGID, below) guaranteed to be unique using the argument ID,
  model name (and instance number), and entry point name - e.g. "mod1_run_arg1".
  This ID is used as the name of the coupling field passed to OASIS via
  prism_def_var and the SMIOC. -->
  <xsl:variable name="couplingFieldBFGID">
    <xsl:call-template name="getCouplingFieldBFGID">
      <xsl:with-param name="modelName" select="$entryPointArgTuple/@modelName"/>
      <xsl:with-param name="instanceID" select="$entryPointArgTuple/@instance"/>
      <xsl:with-param name="entryPointName" select="$entryPointArgTuple/@epName"/>
      <xsl:with-param name="argumentID" select="$entryPointArgTuple/@id"/>
    </xsl:call-template>
  </xsl:variable>

  <comment>The BFG ID string for this coupling field is: 
  <xsl:value-of select="$couplingFieldBFGID"/></comment>

  <assign lhs="{$couplingFieldBFGID}%coupling_field_no" rhs="{$couplingFieldIndex}"/>

  <xsl:choose>
    <xsl:when test="$gridType = $gridlessGridType">
      <point lhs="grid" rhs="component%gridless_grids({$gridIndex})"/>
    </xsl:when>
    <xsl:otherwise>
      <point lhs="grid" rhs="component%gridded_grids({$gridIndex})"/>
    </xsl:otherwise>
  </xsl:choose>

  <point lhs="coupling_field" rhs="component%coupling_fields({$couplingFieldBFGID}%coupling_field_no)"/>

  <assign lhs="coupling_field%bfg_id" rhs="{$couplingFieldBFGID}" type="string-literal"/>

  <xsl:choose>
    <xsl:when test="string($couplingField/@name)">
      <assign lhs="coupling_field%name" 
              rhs="{$couplingField/@name}" type="string-literal"/>
    </xsl:when>
    <xsl:otherwise>
      <assign lhs="coupling_field%name" 
              rhs="NO_NAME" type="string-literal"/>
    </xsl:otherwise>
  </xsl:choose>

  <xsl:variable name="prismDataType">
    <xsl:call-template name="getPRISMDataType">
      <xsl:with-param name="bfgDataType" select="$couplingField/@dataType"/>
    </xsl:call-template>
  </xsl:variable>

  <assign lhs="coupling_field%type_id" rhs="{$prismDataType}"/> 
  <!-- IRH: TODO: Assuming we're not passing vector data
  (how does definition.xml support this/how can we tell 
  whether it's vector data?) -->
  <assign lhs="coupling_field%nodims(1)" rhs="ndim"/> 
  <assign lhs="coupling_field%nodims(2)" rhs="0"/> 
  <!-- IRH: TODO: GRIDSPEC: Assuming no halo region, therefore grid_valid_shape
  = corner_actual_shape/var_actual_shape.  This is fine for GENIE.
  grid_valid_shape might be smaller than the var_actual_shape if 
  this is a parallel model with different threads working on 
  different areas of the grid (each of which has a halo region).
  Gridspec doesn't support halo regions at the moment.
  It's also possible I suppose that var_actual_shape might be smaller
  than corner_actual_shape for some variables - we should probably
  take the dimensions from the <data> element in definition.xml rather
  than assume it = grid_valid_shape? -->
  <assign lhs="coupling_field%actual_shape" rhs="grid%valid_shape"/>

  <iterate start="1" end="ep_instance_count" counter="index_loop">
    <point lhs="coupling_field_ep_inst" rhs="coupling_field%ep_instances(index_loop)"/>
    <nullify name="coupling_field_ep_inst%get_instance"/>
    <nullify name="coupling_field_ep_inst%put_instances"/>
  </iterate>

  <!-- This coupling field has one or more inputs (may also have outputs,
  but we create separate PRISM transients for those). -->
  <xsl:if test="$entryPointArgTuple[contains(@form,'get')]">

    <!-- Initialise a transient for each source -->
    <xsl:call-template name="processCorresFields">
      <xsl:with-param name="processType" select="'initTransients'"/>
      <xsl:with-param name="direction" select="'get'"/>
      <xsl:with-param name="entryPointArg" select="$entryPointArgTuple"/>
      <xsl:with-param name="couplingField" select="$couplingField"/>
      <xsl:with-param name="couplingFieldBFGID" select="$couplingFieldBFGID"/>
    </xsl:call-template>

  </xsl:if>

  <!-- This coupling field has one or more outputs (may also have inputs,
  but we create separate PRISM transients for those). -->
  <xsl:if test="$entryPointArgTuple[contains(@form,'put')]">

    <!-- Initialise a transient for each destination -->
    <xsl:call-template name="processCorresFields">
      <xsl:with-param name="processType" select="'initTransients'"/>
      <xsl:with-param name="direction" select="'put'"/>
      <xsl:with-param name="entryPointArg" select="$entryPointArgTuple"/>
      <xsl:with-param name="couplingField" select="$couplingField"/>
      <xsl:with-param name="couplingFieldBFGID" select="$couplingFieldBFGID"/>
    </xsl:call-template>

  </xsl:if>
</xsl:template>


<xsl:template name="initTransient">
  <xsl:param name="direction"/>
  <xsl:param name="corresDirection"/>
  <xsl:param name="transientName"/>
  <xsl:param name="entryPointOfThisCouplingField"/>
  <xsl:param name="entryPointOfRemoteCouplingField"/>

  <xsl:variable name="entryPointSchedulePosition">
    <xsl:call-template name="InSequenceContextID">
      <xsl:with-param name="modelName" select="$entryPointOfRemoteCouplingField/@modelName"/>
      <xsl:with-param name="epName" select="$entryPointOfRemoteCouplingField/@epName"/>
      <xsl:with-param name="instance" select="$entryPointOfRemoteCouplingField/@instance"/>
    </xsl:call-template>
  </xsl:variable>

  <comment>Create a coupling field instance for this entry point argument</comment>
  <allocate name="coupling_field_arg_inst"/>

  <call name="prism_def_var">
    <arg name="coupling_field_arg_inst%prism_id"/>
    <arg value="{$transientName}" type="string-literal"/>
    <arg name="grid%id"/>
    <!-- IRH: TODO: GRIDSPEC: Currently assuming there is only one
    set of points on the grid.  This is ok for GENIE, but the gridspec
    must support more than one set, and we need a way to associate
    a coupling field with a particular set of points on the grid.
    Wherever the coupling fields are defined (currently in BFG's
    definition.xml, but this may change - e.g. may come from a 
    NetCDF/CF file ultimately?) - should reference the gridspec and 
    particular set of points within the gridspec that the coupling field 
    must be used with. -->
    <arg name="grid%point_sets(1)%id"/>
    <arg name="grid%landmask%id"/>
    <arg name="coupling_field%nodims"/>
    <arg name="coupling_field%actual_shape%bounds"/>
    <arg name="coupling_field%type_id"/>
    <arg name="ierror"/>
  </call>

  <assign lhs="coupling_field_arg_inst%bfg_ref"
          rhs="{$entryPointOfThisCouplingField/@ref}"/>

  <!-- Create an identifier for the message -->
  <xsl:variable name="msgTag">
    <xsl:choose>
      <xsl:when test="$direction = 'get'">
        <xsl:value-of select="concat('put', $entryPointOfRemoteCouplingField/@ref,
                                '_get', $entryPointOfThisCouplingField/@ref)"/>
      </xsl:when>
      <xsl:when test="$direction = 'put'">
        <xsl:value-of select="concat('put', $entryPointOfThisCouplingField/@ref,
                                '_get', $entryPointOfRemoteCouplingField/@ref)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
          <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <assign lhs="coupling_field_arg_inst%msg_tag" rhs="{$msgTag}" type="string-literal"/>

  <comment>Add the coupling field instance to the entry point list</comment>
  <point lhs="coupling_field_ep_inst" rhs="coupling_field%ep_instances({$entryPointSchedulePosition})"/>
  <xsl:choose>
    <xsl:when test="$direction = 'get'">
      <nullify name="coupling_field_arg_inst%next_instance"/>
      <point lhs="coupling_field_ep_inst%get_instance" rhs="coupling_field_arg_inst"/>
    </xsl:when>
    <xsl:when test="$direction = 'put'">
      <point lhs="coupling_field_arg_inst%next_instance" rhs="coupling_field_ep_inst%put_instances"/>
      <point lhs="coupling_field_ep_inst%put_instances" rhs="coupling_field_arg_inst"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
        <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
      </xsl:message>
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
        <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="getPRISMDataType">
 <xsl:param name="bfgDataType"/>
 <xsl:choose>
  <xsl:when test="$bfgDataType = 'real'">
   <xsl:text>PRISM_Real</xsl:text>
  </xsl:when>
  <xsl:when test="$bfgDataType = 'double'">
   <xsl:text>PRISM_Double_Precision</xsl:text>
  </xsl:when>
  <xsl:when test="$bfgDataType = 'integer'">
   <xsl:text>PRISM_Integer</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (</xsl:text>
    <xsl:value-of select="$thisFileName"/>
    <xsl:text>): BFG datatype '</xsl:text>
    <xsl:value-of select="$bfgDataType"/>
    <xsl:text>' has no PRISM equivalent!</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="getXMLSchemaDataType">
 <xsl:param name="bfgDataType"/>
 <xsl:choose>
  <xsl:when test="$bfgDataType = 'real'">
   <xsl:text>xs:real</xsl:text>
  </xsl:when>
  <xsl:when test="$bfgDataType = 'double'">
   <xsl:text>xs:double</xsl:text>
  </xsl:when>
  <xsl:when test="$bfgDataType = 'integer'">
   <xsl:text>xs:integer</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (</xsl:text>
    <xsl:value-of select="$thisFileName"/>
    <xsl:text>): BFG datatype '</xsl:text>
    <xsl:value-of select="$bfgDataType"/>
    <xsl:text>' has no XML Schema equivalent!</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<!-- Initialise coupling information for the given component
including coupling fields and grids. -->
<xsl:template name="oasis4InitCouplingData">
  <xsl:param name="sequenceUnit"/>

  <!-- Get (maximum) number of gridded grids and allocate accordingly. -->
  <xsl:variable name="griddedGridsCount"
                select="count($root/coupled//grid)"/>
  <xsl:if test="$griddedGridsCount > 0">
    <allocate name="component%gridded_grids">
      <dim order="1">
        <lower>
          <value>1</value>
        </lower>
      </dim>
      <dim order="2">
        <upper>
          <value><xsl:value-of select="$griddedGridsCount"/></value>
        </upper>
      </dim>
    </allocate>
  </xsl:if>

  <!-- Get (maximum) number of gridless grids and allocate accordingly. -->
  <xsl:variable name="gridlessGridsCount"
                select="count(document($root/coupled/composition)/composition//set)"/>
  <xsl:if test="$gridlessGridsCount > 0">
    <allocate name="component%gridless_grids">
      <dim order="1">
        <lower>
          <value>1</value>
        </lower>
      </dim>
      <dim order="2">
        <upper>
          <value><xsl:value-of select="$gridlessGridsCount"/></value>
        </upper>
      </dim>
    </allocate>
  </xsl:if>

  <xsl:variable name="sequenceUnitID">
    <xsl:call-template name="getSequenceUnitID">
      <xsl:with-param name="modelName" select="$sequenceUnit/model[1]/@name"/>
      <xsl:with-param name="modelInstance" select="$sequenceUnit/model[1]/@instance"/>
    </xsl:call-template>
  </xsl:variable>
    
  <!-- Search for entry point arguments that require point-to-point communications.
  These are our coupling fields. -->
  <xsl:variable name="entryPointArgs" select="$commFormsRoot/communicationForms/tuple[
    @su = $sequenceUnitID and (contains(@form,'get') or contains(@form,'put'))]"/>

  <xsl:variable name="entryPointArgsCount" select="count($entryPointArgs)"/>
  <xsl:if test="$entryPointArgsCount > 0">
    <allocate name="component%coupling_fields">
      <dim order="1">
        <lower>
          <value>1</value>
        </lower>
      </dim>
      <dim order="2">
        <upper>
          <value><xsl:value-of select="$entryPointArgsCount"/></value>
        </upper>
      </dim>
    </allocate>
  </xsl:if>
  <assign lhs="component%coupling_fields_count" rhs="{$entryPointArgsCount}"/>

  <xsl:call-template name="oasis4InitFieldsAndGrids">
    <xsl:with-param name="entryPointArgTuples" select="$entryPointArgs"/>
  </xsl:call-template>

</xsl:template>


<!-- Initialise data for all grids used by this collection of coupling fields
(entry point argument tuples from DS.xml). -->
<xsl:template name="oasis4InitFieldsAndGrids">
  <xsl:param name="entryPointArgTuples"/>
  <xsl:param name="couplingFieldCount" select="1"/>
  <xsl:param name="initGridNames" select="' '"/>

  <xsl:if test="$entryPointArgTuples">

    <xsl:variable name="currentTuple" select="$entryPointArgTuples[1]"/>

    <!-- Get the definition of the coupling field in the definition doc. -->
    <xsl:variable name="couplingField" select="document($root/coupled/models/model)/
      definition[name = $currentTuple/@modelName]//
      entryPoint[@name = $currentTuple/@epName]/data[@id = $currentTuple/@id]"/>

    <!--
    <comment>Entry point name: <xsl:value-of select="$currentTuple/@epName"/></comment>
    <comment>Argument ID: <xsl:value-of select="$currentTuple/@id"/></comment>
    <comment>Argument name: <xsl:value-of select="$couplingField/@name"/></comment>
    <comment>Argument type: <xsl:value-of select="$couplingField/@dataType"/></comment>
    -->

    <xsl:variable name="setPosition">
      <xsl:call-template name="getCouplingFieldSetNumber">
        <xsl:with-param name="entryPointArgTuple" select="$currentTuple"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$setPosition = ''">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): oasis4InitFieldsAndGrids: coupling field missing set in compose.xml.</xsl:text>
      </xsl:message>
    </xsl:if>
    <!--
    <xsl:message terminate="no">
      <xsl:text>setPosition is: </xsl:text>
      <xsl:value-of select="$setPosition"/>
    </xsl:message>
    -->

    <xsl:variable name="gridType">
      <xsl:call-template name="getCouplingFieldGridType">
        <xsl:with-param name="couplingField" select="$couplingField"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$gridType = ''">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): oasis4InitFieldsAndGrids: coupling field missing grid.</xsl:text>
        <xsl:text> Coupling field was: </xsl:text>
        <xsl:value-of select="$currentTuple/@modelName"/>
        <xsl:value-of select="$currentTuple/@epName"/>
        <xsl:value-of select="$currentTuple/@id"/>
      </xsl:message>
    </xsl:if>
    <!--
    <xsl:message terminate="no">
      <xsl:text>gridType is: </xsl:text>
      <xsl:value-of select="$gridType"/>
    </xsl:message>
    -->

    <xsl:variable name="gridIndex">
      <xsl:choose>
        <xsl:when test="$gridType = $gridlessGridType">
          <xsl:value-of select="$setPosition"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="getGriddedGridIndex">
            <xsl:with-param name="gridType" select="$gridType"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:if test="$gridIndex = ''">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): oasis4InitFieldsAndGrids: could not find index of grid in coupled.xml.</xsl:text>
      </xsl:message>
    </xsl:if>

    <!-- Make a unique grid name - by adding the set number for gridless grid
    coupling fields if necessary. -->
    <xsl:variable name="gridName">
      <xsl:choose>
        <xsl:when test="$gridType = $gridlessGridType">
          <xsl:value-of select="concat($gridlessGridPrefix,$setPosition)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$gridType"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!--
    <xsl:message terminate="no">
      <xsl:text>gridName is: </xsl:text>
      <xsl:value-of select="$gridName"/>
    </xsl:message>
    -->

    <!-- Check that this grid hasn't already been initialised - 
    many coupling fields may of course be passed over the same grid.
    N.B. We must put spaces around the grid name, because we might have
    'gridless_grid_for_set_40' in initGridNames and want to check for
    the existence of 'gridless_grid_for_set_4'. -->
    <xsl:if test="not(contains($initGridNames, concat(' ', $gridName, ' ')))">

      <comment>Initialising grid: <xsl:value-of select="$gridName"/></comment>
      <!-- Now choose what kind of grid to initialise -->
      <xsl:choose>
        <xsl:when test="$gridType = $gridlessGridType">
          <xsl:call-template name="oasis4InitGridlessGrid">
            <xsl:with-param name="gridName" select="$gridName"/>
            <xsl:with-param name="gridIndex" select="$gridIndex"/>
            <xsl:with-param name="couplingField" select="$couplingField"/>
            <xsl:with-param name="entryPointArgTuple" select="$currentTuple"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="oasis4InitGriddedGrid">
            <xsl:with-param name="gridName" select="$gridName"/>
            <xsl:with-param name="gridIndex" select="$gridIndex"/>
            <xsl:with-param name="componentName"
              select="concat('DU', $currentTuple/@du, '_SU', $currentTuple/@su )"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
      <comment>Finished initialising grid: <xsl:value-of select="$gridName"/></comment>

    </xsl:if>

    <comment>Initialising coupling field for grid: <xsl:value-of select="$gridName"/></comment>
    <!-- N.B. coupling field must be initialised after grid, as prism_def_var()
    requires the grid_id returned by prism_def_grid(). -->
    <xsl:call-template name="oasis4InitCouplingField">
      <xsl:with-param name="couplingField" select="$couplingField"/>
      <xsl:with-param name="entryPointArgTuple" select="$currentTuple"/>
      <xsl:with-param name="couplingFieldIndex" select="$couplingFieldCount"/>
      <xsl:with-param name="gridType" select="$gridType"/>
      <xsl:with-param name="gridIndex" select="$gridIndex"/>
    </xsl:call-template>
    <comment>Finished initialising coupling field for grid: <xsl:value-of select="$gridName"/></comment>

    <xsl:call-template name="oasis4InitFieldsAndGrids">
      <xsl:with-param name="entryPointArgTuples" select="$entryPointArgTuples[position() > 1]"/>
      <xsl:with-param name="couplingFieldCount" select="$couplingFieldCount + 1"/>
      <!-- No harm in adding gridName to initGridNames if it's already there
      - string won't get ridiculously long. -->
      <xsl:with-param name="initGridNames" select="concat($initGridNames, $gridName, ' ')"/>
    </xsl:call-template>
  </xsl:if>

</xsl:template>


<xsl:template name="getCouplingFieldSetNumber">
  <xsl:param name="entryPointArgTuple"/>

  <!--
  <xsl:message terminate="no">
    <xsl:text>entryPointArgTuple is: </xsl:text>
    <xsl:value-of select="$entryPointArgTuple/@modelName"/>
    <xsl:value-of select="$entryPointArgTuple/@epName"/>
    <xsl:value-of select="$entryPointArgTuple/@instance"/>
    <xsl:value-of select="$entryPointArgTuple/@id"/>
  </xsl:message>
  -->

  <xsl:for-each select="document($root/coupled/composition)/composition//set">
    <xsl:variable name="setPosition" select="position()"/>
    <xsl:for-each select="field">
      <xsl:if test="@modelName = $entryPointArgTuple/@modelName and
                    @epName = $entryPointArgTuple/@epName and
                    @id = $entryPointArgTuple/@id and
                    (not(@instance) or @instance = $entryPointArgTuple/@instance)">
        <xsl:value-of select="$setPosition"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:for-each>
</xsl:template>


<xsl:template name="getCouplingFieldGridType">
  <xsl:param name="couplingField"/>

  <!--
  <xsl:message terminate="no">
    <xsl:text>gridType at data level: </xsl:text>
    <xsl:value-of select="$couplingField/@gridType"/>
    <xsl:text> gridType at entry point level: </xsl:text>
    <xsl:value-of select="($couplingField/parent::entryPoint)/@gridType"/>
    <xsl:text> gridType at default definition level: </xsl:text>
    <xsl:value-of select="($couplingField/ancestor::definition)/gridType"/>
  </xsl:message>
  -->
  <xsl:choose>
    <xsl:when test="$couplingField/@gridType">
      <xsl:value-of select="$couplingField/@gridType"/>
    </xsl:when>
    <xsl:when test="($couplingField/parent::entryPoint)/@gridType">
      <xsl:value-of select="($couplingField/parent::entryPoint)/@gridType"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="($couplingField/ancestor::definition)/gridType"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- Initialise the given gridless grid. -->
<xsl:template name="oasis4InitGridlessGrid">
  <xsl:param name="gridName"/>
  <xsl:param name="gridIndex"/>
  <xsl:param name="couplingField"/>
  <xsl:param name="entryPointArgTuple"/>

  <point lhs="grid" rhs="component%gridless_grids({$gridIndex})"/>

  <assign lhs="grid%name" rhs="{$gridName}" type="string-literal"/>

  <assign lhs="grid%type_id" rhs="PRISM_Gridless"/>

  <comment>Bounds for each dimension default to 1, if not given below</comment>
  <assign lhs="grid%valid_shape%bounds(:,:)" rhs="1"/>

  <!-- Get the upper and lower bounds for each dimension of the coupling field. -->
  <xsl:for-each select="$couplingField/dim">
    <!-- Use the ../Utils/Expr.xsl templates to process the bounds. -->
    <cassign>
      <lhs>
        <value>grid%valid_shape%bounds(1,<xsl:value-of select="@value"/>)</value>
      </lhs>
      <rhs>
        <!-- IRH: TODO: Where the bounds are given by reference to
        another entry point argument, we need access to that argument.
        The argument is defined in BFG2Main#.f90 though, e.g. as r2, r3.  
        How best to share the arguments between BFG2Main and here (BFG2Target)?
        Note that BFG2Main is a Fortran program, rather than a module, so we
        can't just "use" it.  Pass by a subroutine?  Note that priming
        happens AFTER initComms() is called - this is a problem. -->
        <xsl:apply-templates select="lower/*">
          <xsl:with-param name="modelName" select="$entryPointArgTuple/@modelName"/>
          <xsl:with-param name="epName" select="$entryPointArgTuple/@epName"/>
          <xsl:with-param name="id" select="$entryPointArgTuple/@id"/>
          <xsl:with-param name="instance" select="$entryPointArgTuple/@instance"/>
        </xsl:apply-templates>
      </rhs>
    </cassign>
    <cassign>
      <lhs>
        <value>grid%valid_shape%bounds(2,<xsl:value-of select="@value"/>)</value>
      </lhs>
      <rhs>
        <xsl:apply-templates select="upper/*">
          <xsl:with-param name="modelName" select="$entryPointArgTuple/@modelName"/>
          <xsl:with-param name="epName" select="$entryPointArgTuple/@epName"/>
          <xsl:with-param name="id" select="$entryPointArgTuple/@id"/>
          <xsl:with-param name="instance" select="$entryPointArgTuple/@instance"/>
        </xsl:apply-templates>
      </rhs>
    </cassign>
  </xsl:for-each>

  <!-- Hardcoded dimenions for OASIS4 gridless toy example
  <assign lhs="grid%valid_shape%bounds(1,1)" rhs="1"/>
  <assign lhs="grid%valid_shape%bounds(2,1)" rhs="2"/>
  <assign lhs="grid%valid_shape%bounds(1,2)" rhs="1"/>
  <assign lhs="grid%valid_shape%bounds(2,2)" rhs="4"/>
  <assign lhs="grid%valid_shape%bounds(1,3)" rhs="1"/>
  <assign lhs="grid%valid_shape%bounds(2,3)" rhs="1"/>
  -->

  <comment>No land mask for gridless grids</comment>
  <assign lhs="grid%landmask%id" rhs="PRISM_UNDEFINED"/>

  <call name="prism_def_grid">
    <arg name="grid%id"/>
    <arg name="grid%name" type="string" readonly="true"/>
    <arg name="component%id"/>
    <arg name="grid%valid_shape%bounds"/>
    <arg name="grid%type_id"/>
    <arg name="ierror"/>
  </call>

  <point lhs="points" rhs="grid%point_sets(1)"/>

  <assign lhs="points%name" rhs="{$gridName}_point_set_1" type="string-literal"/> 

  <call name="prism_set_points_Gridless">
    <arg name="points%id"/>
    <arg name="points%name" type="string" readonly="true"/>
    <arg name="grid%id"/>
    <!-- We are adding new points, rather than updating existing ones -->
    <arg value=".true."/>
    <arg name="ierror"/>
  </call>

</xsl:template>


<xsl:template name="getGriddedGridIndex">
  <xsl:param name="gridType"/>

  <xsl:for-each select="$root/coupled/grids/grid">
    <xsl:if test="document(.)/gridspec/mosaic[@id = $gridType]">
      <xsl:value-of select="position()"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


<!-- Initialise the given gridded grid. -->
<xsl:template name="oasis4InitGriddedGrid">
  <xsl:param name="gridName"/>
  <xsl:param name="gridIndex"/>
  <xsl:param name="componentName"/>

  <xsl:variable name="gridMosaic"
    select="document($root/coupled/grids/grid)/gridspec/mosaic[@id = $gridName]"/>
  <xsl:if test="count($gridMosaic) != 1">
    <xsl:message terminate="yes">
      <xsl:text>Error (</xsl:text>
      <xsl:value-of select="$thisFileName"/>
      <xsl:text>): oasis4InitGriddedGrid: gridName unrecognised or not unique.</xsl:text>
    </xsl:message>
  </xsl:if>

  <point lhs="grid" rhs="component%gridded_grids({$gridIndex})"/>

  <assign lhs="grid%name" rhs="{$gridMosaic/@id}" type="string-literal"/>

  <xsl:variable name="prismGridType">
    <xsl:call-template name="getPRISMGridType">
      <xsl:with-param name="gridDescriptor" select="$gridMosaic/grid_descriptor"/>
    </xsl:call-template>
  </xsl:variable>

  <assign lhs="grid%type_id" rhs="{$prismGridType}"/> 

  <!-- IRH: TODO: GRIDSPEC: Not considering parallel grids and halo 
  regions therefore valid shape includes all points associated with the 
  gridspec tile, and we're assuming that there's only one tile in
  with the gridspec (otherwise we need to set up different grids for
  each rank).  Balaji suggests making each partition a separate tile, 
  using overlaps for halo regions. -->
  <xsl:variable name="horizontalCoords" select="$gridMosaic/
    mosaicgrid/horizontal_coord_system"/>
  <assign lhs="grid%valid_shape%bounds(1,1)" rhs="1"/>
  <assign lhs="grid%valid_shape%bounds(2,1)" 
          rhs="{$horizontalCoords/x_axis/indices[@type='point']/@count}"/>
  <assign lhs="grid%valid_shape%bounds(1,2)" rhs="1"/>
  <assign lhs="grid%valid_shape%bounds(2,2)"
          rhs="{$horizontalCoords/y_axis/indices[@type='point']/@count}"/>
  <assign lhs="grid%valid_shape%bounds(1,3)" rhs="1"/>
  <!-- IRH: TODO: GRIDSPEC: Only doing 2D coupling - no vertical axis -->
  <assign lhs="grid%valid_shape%bounds(2,3)" rhs="1"/>

  <call name="prism_def_grid">
    <arg name="grid%id"/>
    <arg name="grid%name" type="string" readonly="true"/>
    <arg name="component%id"/>
    <arg name="grid%valid_shape%bounds"/>
    <arg name="grid%type_id"/>
    <arg name="ierror"/>
  </call>

  <!-- IRH: TODO: GRIDSPEC: Only one set of points currently.
  For GENIE models there is only one set of points, but there may
  be more for other models.  The gridspec is intended the contain
  the supergrid - the grid of all points.  Subgrids for different
  fields are specified alongside the field metadata (as indexes of
  points in the supergrid) - where will this field metadata come from?
  Currently we just use definition.xml, but perhaps the field metadata
  should be stored in NcML files, or the PMIOD (Sophie doesn't see the
  PMIOD as OASIS-specific) along with a reference to the gridspec and
  the indices of the field's subgrid. -->
  <point lhs="points" rhs="grid%point_sets(1)"/>
  <point lhs="corners" rhs="grid%corners"/>

  <!-- IRH: TODO: GRIDSPEC: grid_valid_shape may be smaller than 
  points/corners_actual_shape if there's a halo region (not dealing
  with parallel models ATM though). -->
  <assign lhs="points%actual_shape" rhs="grid%valid_shape"/>
  <assign lhs="corners%actual_shape" rhs="grid%valid_shape"/>

  <xsl:for-each select="$horizontalCoords/x_axis/indices"> 
    <xsl:variable name="indexType">
      <xsl:call-template name="getIndexType">
        <xsl:with-param name="gridspecType" select="@type"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- IRH: TODO: the XML pseudocode below seems a slightly odd way
    of doing it - why two <dim>'s - suggests it's a 2D array, when
    in fact it's a 1D array. -->
    <allocate name="{$indexType}%coords%longitudes">
      <dim order="1">
        <lower>
          <value>1</value>
        </lower>
      </dim>
      <dim order="2">
        <upper>
          <value><xsl:value-of select="@count"/></value>
        </upper>
      </dim>
    </allocate>
  </xsl:for-each>

  <xsl:for-each select="$horizontalCoords/y_axis/indices"> 
    <xsl:variable name="indexType">
      <xsl:call-template name="getIndexType">
        <xsl:with-param name="gridspecType" select="@type"/>
      </xsl:call-template>
    </xsl:variable>

    <allocate name="{$indexType}%coords%latitudes">
      <dim order="1">
        <lower>
          <value>1</value>
        </lower>
      </dim>
      <dim order="2">
        <upper>
          <value><xsl:value-of select="@count"/></value>
        </upper>
      </dim>
    </allocate>
  </xsl:for-each>

  <!-- Get the longitude point and corner coords -->
  <xsl:call-template name="getCoords">
    <xsl:with-param name="indexLists" select="$horizontalCoords/x_axis/indices"/> 
    <xsl:with-param name="coords" select="$horizontalCoords/x_axis/coordinates"/> 
    <xsl:with-param name="coordType" select="'longitudes'"/> 
  </xsl:call-template>

  <!-- Get the latitude point and corner coords -->
  <xsl:call-template name="getCoords">
    <xsl:with-param name="indexLists" select="$horizontalCoords/y_axis/indices"/> 
    <xsl:with-param name="coords" select="$horizontalCoords/y_axis/coordinates"/> 
    <xsl:with-param name="coordType" select="'latitudes'"/> 
  </xsl:call-template>

  <allocate name="points%coords%verticals">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <!-- IRH: TODO: GRIDSPEC: Only doing 2D coupling - no vertical axis -->
        <value>1</value>
      </upper>
    </dim>
  </allocate>
  <allocate name="corners%coords%verticals">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <!-- IRH: TODO: GRIDSPEC: Only doing 2D coupling - no vertical axis -->
        <value>1</value>
      </upper>
    </dim>
  </allocate>

  <!-- IRH: TODO: GRIDSPEC: Set vertical point/corner coordinate
  to 0 - only doing 2D coupling at present. -->
  <assign lhs="points%coords%verticals" rhs="0.0"/>
  <assign lhs="corners%coords%verticals" rhs="0.0"/>

  <!-- Arbitrary name for points - must match optional compute_space
  <points local_name=""> attribute in the SMIOC - not including this 
  in SMIOC ATM though. -->
  <xsl:variable name="pointsName"
    select="concat($componentName, '_', $gridMosaic/@id, '_point_set_1')"/>
  <assign lhs="points%name" rhs="{$pointsName}" type="string-literal"/> 

  <call name="prism_set_points">
    <arg name="points%id"/>
    <arg name="points%name" type="string" readonly="true"/>
    <arg name="grid%id"/>
    <arg name="points%actual_shape%bounds"/>
    <arg name="points%coords%longitudes"/>
    <arg name="points%coords%latitudes"/>
    <arg name="points%coords%verticals"/>
    <!-- We are adding new points, rather than updating existing ones -->
    <arg value=".true."/>
    <arg name="ierror"/>
  </call>
  
  <comment>Write point set to KML file for viewing in GoogleEarth</comment>
  <xsl:variable name="longitudesCount"
    select="$horizontalCoords/x_axis/indices[@type='point']/@count"/>
  <xsl:variable name="latitudesCount"
    select="$horizontalCoords/y_axis/indices[@type='point']/@count"/>
  <xsl:call-template name="writePointsToKML">
    <xsl:with-param name="pointsName" select="$pointsName"/>
    <xsl:with-param name="longitudesCount" select="$longitudesCount"/>
    <xsl:with-param name="latitudesCount" select="$latitudesCount"/>
  </xsl:call-template>

  <comment>Copy corners to OASIS4 format 2D array where</comment>
  <comment>leading/trailing corners are stored separately</comment>

  <allocate name="corners_longitudes_OASIS4">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <value><xsl:value-of select="$longitudesCount"/></value>
      </upper>
    </dim>
    <dim order="3">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="4">
      <upper>
        <!-- A "leading" and "trailing" corner coord for each grid cell -
        this is the format required for input into OASIS. -->
        <value>2</value>
      </upper>
    </dim>
  </allocate>
  
  <assign lhs="corners_longitudes_OASIS4( 1:{$longitudesCount}, 1)"
          rhs="corners%coords%longitudes( 1:{$longitudesCount} )"/>
  <assign lhs="corners_longitudes_OASIS4( 1:{$longitudesCount}, 2)"
          rhs="corners%coords%longitudes( 2:( {$longitudesCount} + 1 ) )"/>

  <allocate name="corners_latitudes_OASIS4">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <value><xsl:value-of select="$latitudesCount"/></value>
      </upper>
    </dim>
    <dim order="3">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="4">
      <upper>
        <!-- A "leading" and "trailing" corner coord for each grid cell -
        this is the format required for input into OASIS. -->
        <value>2</value>
      </upper>
    </dim>
  </allocate>
  
  <assign lhs="corners_latitudes_OASIS4( 1:{$latitudesCount}, 1)"
          rhs="corners%coords%latitudes( 1:{$latitudesCount} )"/>
  <assign lhs="corners_latitudes_OASIS4( 1:{$latitudesCount}, 2)"
          rhs="corners%coords%latitudes( 2:( {$latitudesCount} + 1 ) )"/>

  <allocate name="corners_verticals_OASIS4">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <value>1</value>
      </upper>
    </dim>
    <dim order="3">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="4">
      <upper>
        <!-- A "leading" and "trailing" corner coord for each grid cell -
        this is the format required for input into OASIS. -->
        <value>2</value>
      </upper>
    </dim>
  </allocate>
  <assign lhs="corners_verticals_OASIS4" rhs="0.0"/>

  <call name="prism_set_corners">
    <arg name="grid%id"/>
    <!-- IRH: TODO: GRIDSPEC: "Total number of corners for each 
    volume element."  Not sure how this is used - I was using 
    a value of 8 for both 2D and 3D test grids in my PRISM example. -->
    <arg value="8"/>
    <arg name="corners%actual_shape%bounds"/>
    <arg name="corners_longitudes_OASIS4"/>
    <arg name="corners_latitudes_OASIS4"/>
    <arg name="corners_verticals_OASIS4"/>
    <arg name="ierror"/>
  </call>
  
  <deallocate name="corners_longitudes_OASIS4" type="array"/> 
  <deallocate name="corners_latitudes_OASIS4" type="array"/> 
  <deallocate name="corners_verticals_OASIS4" type="array"/> 

  <xsl:variable name="landmask" select="$horizontalCoords/mask[@type = 'land']"/>
  <point lhs="mask" rhs="grid%landmask"/>
  <xsl:choose>
    <xsl:when test="$landmask">
      <comment>Initialise the land mask for this grid</comment>
      <xsl:call-template name="initMaskArray">
        <xsl:with-param name="mask" select="$landmask"/>
        <xsl:with-param name="longitudesCount" select="$longitudesCount"/>
        <xsl:with-param name="latitudesCount" select="$latitudesCount"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <comment>No land mask found in the gridspec</comment>
      <assign lhs="mask%id" rhs="PRISM_UNDEFINED"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- Extract the mask array from the gridspec and copy it to an
appropriately sized Fortran array for the mask.  -->
<xsl:template name="initMaskArray">
  <xsl:param name="mask"/>
  <xsl:param name="longitudesCount"/>
  <xsl:param name="latitudesCount"/>

  <assign lhs="mask%actual_shape" rhs="grid%valid_shape"/>

  <allocate name="mask%array">
    <dim order="1">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="2">
      <upper>
        <value><xsl:value-of select="$longitudesCount"/></value>
      </upper>
    </dim>
    <dim order="3">
      <lower>
        <value>1</value>
      </lower>
    </dim>
    <dim order="4">
      <upper>
        <value><xsl:value-of select="$latitudesCount"/></value>
      </upper>
    </dim>
  </allocate>

  <!--
  <xsl:message terminate="no">
    <xsl:text>Mask value string: </xsl:text><xsl:value-of select="$mask/values"/>
  </xsl:message>
  -->

  <xsl:choose>
    <xsl:when test="function-available('str:tokenize')">
      <xsl:call-template name="getMaskValues">
        <!-- N.B. default tokenizers are whitespace chars -->
        <xsl:with-param name="maskValueTokens" select="str:tokenize($mask/values)"/>
        <xsl:with-param name="longitudesCount" select="$longitudesCount"/>
        <xsl:with-param name="latitudesCount" select="$latitudesCount"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="function-available('xalan:tokenize')">
      <xsl:call-template name="getMaskValues">
        <xsl:with-param name="maskValueTokens" select="xalan:tokenize($mask/values)"/>
        <xsl:with-param name="longitudesCount" select="$longitudesCount"/>
        <xsl:with-param name="latitudesCount" select="$latitudesCount"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>Error: initMaskArray: no function to tokenize mask values.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>

  <call name="prism_set_mask">
    <arg name="mask%id"/>
    <arg name="grid%id"/>
    <arg name="mask%actual_shape%bounds"/>
    <arg name="mask%array"/>
    <!-- We are adding a new mask, rather than updating an existing one -->
    <arg value=".true."/>
    <arg name="ierror"/>
  </call>
</xsl:template>


<xsl:template name="getMaskValues">
  <xsl:param name="maskValueTokens"/>
  <xsl:param name="longitudesCount"/>
  <xsl:param name="latitudesCount"/>
  <xsl:param name="tokenString" select="''"/>

  <xsl:variable name="thisTokenValue">
    <xsl:choose>
      <xsl:when test="$maskValueTokens[ 1 ] = '1'">
        <xsl:value-of select="$fortranFalse"/>
        <!-- <xsl:value-of select="$fortranTrue"/> -->
      </xsl:when>
      <xsl:when test="$maskValueTokens[ 1 ] = '0'">
        <xsl:value-of select="$fortranTrue"/>
        <!-- <xsl:value-of select="$fortranFalse"/> -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:text>Error: getMaskValues: unrecognised value in mask read from gridspec.</xsl:text>
          <xsl:text> Value was: </xsl:text><xsl:value-of select="$maskValueTokens[1]"/>
          <xsl:text> Count was: </xsl:text><xsl:value-of select="count( $maskValueTokens )"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="count( $maskValueTokens ) = 1">
      <xsl:variable name="maskValues"
        select="concat('(/ ', $tokenString, $thisTokenValue, ' /)')"/>
      <xsl:variable name="maskDims"
        select="concat('(/ ', $longitudesCount, ', ', $latitudesCount, ' /)')"/>
      <assign lhs="mask%array" rhs="reshape( {$maskValues}, {$maskDims} )"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="getMaskValues">
        <xsl:with-param name="maskValueTokens" select="$maskValueTokens[ position() > 1 ]"/>
        <xsl:with-param name="longitudesCount" select="$longitudesCount"/>
        <xsl:with-param name="latitudesCount" select="$latitudesCount"/>
        <xsl:with-param name="tokenString" select="concat($tokenString, $thisTokenValue, ', ')"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!--  Process ALL indices here instead of just edges/just points, so we have
a for-each loop.  Saves us tokenizing the coords multiple times or in case
of algorithm to produce calls, calling that algorithm multiple times. -->
<xsl:template name="getCoords">
  <xsl:param name="indexLists"/>
  <xsl:param name="coords"/>
  <xsl:param name="coordType"/>

  <xsl:choose>

    <!-- Coordinates are specified by start and increment attributes -->
    <xsl:when test="$coords/@start and $coords/@increment">
      <comment>Coordinates specified by start and increment values in gridspec</comment> 
      <xsl:for-each select="$indexLists"> 
        <xsl:variable name="indexType">
          <xsl:call-template name="getIndexType">
            <xsl:with-param name="gridspecType" select="@type"/>
          </xsl:call-template>
        </xsl:variable>

        <comment>Initialise <xsl:value-of select="$coordType"/> for grid <xsl:value-of select="$indexType"/></comment>
        <point lhs="coord_array" rhs="{$indexType}%coords%{$coordType}"/>

        <xsl:call-template name="getCoordsImplicit">
          <xsl:with-param name="indices" select="."/>
          <xsl:with-param name="coordinates" select="$coords"/>
        </xsl:call-template>
      </xsl:for-each>
    </xsl:when>

    <!-- Coordinates are generated at run-time by an algorithm -->
    <xsl:when test="$coords/algorithm">
      <comment>Coordinates generated by a function referenced in gridspec</comment> 
      <allocate name="all_coords_array">
        <dim order="1">
          <lower>
            <value>1</value>
          </lower>
        </dim>
        <dim order="2">
          <upper>
            <value><xsl:value-of select="$coords/@count"/></value>
          </upper>
        </dim>
      </allocate>

      <xsl:variable name="algorithm" select="$coords/algorithm"/>
      <call name="{$algorithm/@name}">
        <arg value="{$coords/@count}"/>
        <arg name="all_coords_array"/>
      </call>

      <xsl:for-each select="$indexLists"> 
        <xsl:variable name="indexType">
          <xsl:call-template name="getIndexType">
            <xsl:with-param name="gridspecType" select="@type"/>
          </xsl:call-template>
        </xsl:variable>

        <comment>Initialise <xsl:value-of select="$coordType"/> for grid <xsl:value-of select="$indexType"/></comment>
        <point lhs="coord_array" rhs="{$indexType}%coords%{$coordType}"/>

        <xsl:call-template name="getCoordsAlgorithm">
          <xsl:with-param name="indices" select="."/>
        </xsl:call-template>
      </xsl:for-each>

      <deallocate name="all_coords_array" type="array"/> 
    </xsl:when>

    <!-- Coordinates are listed explicitly as a string of whitespace-
    delimited numbers. -->
    <xsl:when test="string($coords)">
      <comment>Coordinates listed explicitly in gridspec</comment> 

      <!--
      <xsl:message terminate="no">
        <xsl:text>Coordinate string is: </xsl:text>
        <xsl:value-of select="$coords"/>
      </xsl:message>
      -->

      <xsl:for-each select="$indexLists"> 
        <xsl:variable name="indexType">
          <xsl:call-template name="getIndexType">
            <xsl:with-param name="gridspecType" select="@type"/>
          </xsl:call-template>
        </xsl:variable>

        <comment>Initialise <xsl:value-of select="$coordType"/> for grid <xsl:value-of select="$indexType"/></comment>
        <point lhs="coord_array" rhs="{$indexType}%coords%{$coordType}"/>
        
        <xsl:choose>
          <xsl:when test="function-available('str:tokenize')">
            <xsl:call-template name="getCoordsExplicit">
              <xsl:with-param name="indices" select="."/>
              <xsl:with-param name="coordTokens" select="str:tokenize($coords, ' ')"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="function-available('xalan:tokenize')">
            <xsl:call-template name="getCoordsExplicit">
              <xsl:with-param name="indices" select="."/>
              <xsl:with-param name="coordTokens" select="xalan:tokenize($coords, ' ')"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:text>Error: getCoords: no function to tokenize coordinates.</xsl:text>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:when>

    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): Coordinates not specified in gridspec using supported method.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- getCoordsExplicit extracts a subset of coordinates from a node set
(coordTokens) and places them in an array (coord_array). indices "count",
"start" and "increment" attributes are used to define the coordinate subset. -->
<xsl:template name="getCoordsExplicit">
  <xsl:param name="indices"/>
  <xsl:param name="coordTokens"/>
  <xsl:param name="indexNum" select="0"/>

  <xsl:if test="$indexNum != $indices/@count">
    <!-- item-at() requires a processor supporting XPath 2.0
    (the one we're using for BFG doesn't) -->
    <!-- <xsl:variable name="coord" select="item-at($coordTokens, ($start + ($count * $increment) - 1) )"/> -->
    <xsl:variable name="coord" select="$coordTokens[$indices/@start + ($indexNum * $indices/@increment)]"/>
    <assign lhs="coord_array({$indexNum + 1})" rhs="{$coord}"/>

    <xsl:call-template name="getCoordsExplicit">
      <xsl:with-param name="indices" select="$indices"/>
      <xsl:with-param name="coordTokens" select="$coordTokens"/>
      <xsl:with-param name="indexNum" select="$indexNum + 1"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


<!-- getCoordsImplicit extracts a subset of coordinates from a list of
coordinates specified by "start" and "increment" attributes, and places them
in an array (coord_array). indices "count", "start" and "increment" attributes
are used to define the coordinate subset. -->
<xsl:template name="getCoordsImplicit">
  <xsl:param name="indices"/>
  <xsl:param name="coordinates"/>
  <xsl:param name="indexNum" select="0"/>
  <xsl:param name="coordinateNum" select="0"/>

  <xsl:if test="$coordinateNum != $coordinates/@count">
    <xsl:choose>
      <xsl:when test="$coordinateNum = $indices/@start + ($indexNum * $indices/@increment) - 1"> 
        <xsl:variable name="coord" select="$coordinates/@start + ($coordinateNum * $coordinates/@increment)"/>
        <assign lhs="coord_array({$indexNum + 1})" rhs="{$coord}"/>

        <xsl:call-template name="getCoordsImplicit">
          <xsl:with-param name="indices" select="$indices"/>
          <xsl:with-param name="coordinates" select="$coordinates"/>
          <xsl:with-param name="indexNum" select="$indexNum + 1"/>
          <xsl:with-param name="coordinateNum" select="$coordinateNum + 1"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="getCoordsImplicit">
          <xsl:with-param name="indices" select="$indices"/>
          <xsl:with-param name="coordinates" select="$coordinates"/>
          <xsl:with-param name="indexNum" select="$indexNum"/>
          <xsl:with-param name="coordinateNum" select="$coordinateNum + 1"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>
</xsl:template>


<!-- getCoordsAlgorithm extracts a subset of algorithm-generated coordinates
from an array at run-time (all_coords_array) and places them in the subset
array (coord_array). indices "count", "start" and "increment" attributes are
used to define the coordinate subset. -->
<xsl:template name="getCoordsAlgorithm">
  <xsl:param name="indices"/>
  
  <iterate start="1" end="{$indices/@count}" counter="index_loop">
    <cassign>
      <lhs>
        <value>coord_loop</value>
      </lhs>
      <rhs>
        <plus>
          <value><xsl:value-of select="$indices/@start - 1"/></value>
          <times>
            <value>index_loop</value>
            <value><xsl:value-of select="$indices/@increment"/></value>
          </times>
        </plus>
      </rhs>
    </cassign>
    <assign lhs="coord_array(index_loop)" rhs="all_coords_array(coord_loop)"/>
  </iterate>
</xsl:template>


<xsl:template name="getIndexType">
 <xsl:param name="gridspecType"/>
 <xsl:choose>
  <xsl:when test="$gridspecType = 'point'">
   <xsl:text>points</xsl:text>
  </xsl:when>
  <xsl:when test="$gridspecType = 'edge'">
   <xsl:text>corners</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (</xsl:text>
    <xsl:value-of select="$thisFileName"/>
    <xsl:text>): gridspec index type '</xsl:text>
    <xsl:value-of select="$gridspecType"/>
    <xsl:text>' not supported!</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="getPRISMGridType">
 <xsl:param name="gridDescriptor"/>
 <xsl:choose>
  <xsl:when test="$gridDescriptor = 'regular_lat_lon'">
   <xsl:text>PRISM_reglonlatvrt</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error (</xsl:text>
    <xsl:value-of select="$thisFileName"/>
    <xsl:text>): Gridspec grid_descriptor '</xsl:text>
    <xsl:value-of select="$gridDescriptor"/>
    <xsl:text>' has no PRISM equivalent!</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="getTransientName">
  <xsl:param name="myBFGID"/>
  <xsl:param name="remoteBFGID"/>
  <xsl:param name="remoteSU"/>
  <xsl:param name="direction"/>
  <xsl:param name="suffix" select="''"/>

  <xsl:choose>
    <xsl:when test="$direction = 'get'">
      <xsl:value-of select="concat($myBFGID,'_getFromSU',$remoteSU,'_',$remoteBFGID,$suffix)"/>
    </xsl:when>
    <xsl:when test="$direction = 'put'">
      <xsl:value-of select="concat($myBFGID,'_putToSU',$remoteSU,'_',$remoteBFGID,$suffix)"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="oasis4FinaliseComms">
  <Includes>
    <include name="prism"/>
  </Includes>
  <DataDecs>
    <declare name="ierror" datatype="integer"/>
    <declare name="coupling_field_loop" datatype="integer"/>
    <declare name="ep_instance_loop" datatype="integer"/>
    <declare name="coupling_field" datatype="coupling_field_type" derived="true" pointer="true"/>
    <declare name="coupling_field_ep_inst" datatype="coupling_field_ep_instance_type" derived="true" pointer="true"/>
    <declare name="coupling_field_arg_inst" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
  </DataDecs>
  <Content>
    <xsl:if test="$oasisvis!='false'">
      <xsl:variable name="myDeploymentUnitID" select="ancestor::code/@id"/>
      <xsl:variable name="myDeploymentUnit" select="document($root/coupled/deployment)
        /deployment/deploymentUnits/deploymentUnit[number($myDeploymentUnitID)]"/>
      <xsl:variable name="mySequenceUnits" select="$myDeploymentUnit/sequenceUnit"/>

      <xsl:for-each select="$mySequenceUnits">
        <xsl:variable name="bfgSUID">
          <xsl:call-template name="getSequenceUnitID">
            <xsl:with-param name="modelName" select="model[1]/@name"/>
            <xsl:with-param name="modelInstance" select="model[1]/@instance"/>
          </xsl:call-template>
        </xsl:variable>

        <call name="oasisvis_final">
          <arg value="{$bfgSUID}"/>
        </call>
      </xsl:for-each>
    </xsl:if>
     
    <call name="prism_terminate">
      <arg name="ierror"/>
    </call>

    <comment>Deallocate lists of coupling field instances</comment>
    <iterate start="1" end="component%coupling_fields_count" counter="coupling_field_loop">
      <point lhs="coupling_field" rhs="component%coupling_fields(coupling_field_loop)"/>
      <iterate start="1" end="ep_instance_count" counter="ep_instance_loop">
        <point lhs="coupling_field_ep_inst" rhs="coupling_field%ep_instances(ep_instance_loop)"/>
        <deallocate name="coupling_field_ep_inst%get_instance" type="pointer"/>
        <point lhs="coupling_field_arg_inst" rhs="coupling_field_ep_inst%put_instances"/>
        <while>
          <condition><associated pointer="coupling_field_arg_inst"/></condition>
          <action>
            <point lhs="coupling_field_ep_inst%put_instances" rhs="coupling_field_arg_inst%next_instance"/>
            <deallocate name="coupling_field_arg_inst" type="pointer"/>
            <point lhs="coupling_field_arg_inst" rhs="coupling_field_ep_inst%put_instances"/>
          </action>
        </while>
      </iterate>
    </iterate>

    <comment>Deallocate the arrays of coupling fields and grids</comment>
    <deallocate name="component%coupling_fields" type="array"/>

    <!-- N.B. One of these two arrays may not be allocated. "deallocate"
    checks for allocation status before deallocating, so this is ok.
    Also note that the Fortran standard ensures that all the 
    longitude/latitude/verticals/mask arrays will be deallocated under
    component%gridded_grids. -->
    <!-- In theory, the grid arrays can be deallocated at the end of 
    initialisation - they're not needed after that.  I'm deallocating
    them here because they can be useful for debugging, and shouldn't
    take up much memory. -->
    <deallocate name="component%gridless_grids" type="array"/>
    <deallocate name="component%gridded_grids" type="array"/>

  </Content>
</xsl:template>


<xsl:template name="oasis4CommsSync">
   <comment>oasis4CommsSync</comment>
   <!-- IRH: TODO: This doesn't work on Ruby - probably straightforward to sort. 
   I assume mpi is included via the prism include anyway.
   <Includes>
    <include name="mpi"/>
   </Includes>
   -->
   <DataDecs>
    <declare name="ierror" datatype="integer"/> 
   </DataDecs>
   <Content>
    <call name="mpi_barrier">
     <arg name="component%local_comm"/>
     <arg name="ierror"/>
    </call>
   </Content>
</xsl:template>


<!-- This code is destined for BFG2InPlace.f90 -->
<xsl:template name="oasis4IncludeTarget">
 <comment>oasis4IncludeTarget</comment>
 <Includes>
  <include name="prism"/>
 </Includes>
</xsl:template>


<!-- This code is destined for BFG2InPlace.f90 -->
<xsl:template name="oasis4DeclareSendRecvVars">
  <comment>oasis4DeclareSendRecvVars</comment>
  <declare name="coupling_field" datatype="coupling_field_type" derived="true" pointer="true"/>
  <declare name="coupling_field_ep_inst" datatype="coupling_field_ep_instance_type" derived="true" pointer="true"/>
  <declare name="coupling_field_arg_inst" datatype="coupling_field_arg_instance_type" derived="true" pointer="true"/>
  <declare name="ierror" datatype="integer"/>
  <declare name="coupling_info" datatype="integer"/>
</xsl:template>


<!-- This code is destined for BFG2InPlace.f90 -->
<!-- The TODOs for oasis4Send apply equally here -->
<xsl:template name="oasis4Receive">
  <comment>oasis4Receive</comment>
  <xsl:variable name="modelName" select="@modelName"/>
  <xsl:variable name="epName" select="@epName"/>
  <xsl:variable name="id" select="@id"/>
  <xsl:variable name="instance" select="@instance"/>
  <xsl:variable name="messageTag" select="@tag"/>
  <xsl:variable name="couplingFieldBFGID">
    <xsl:call-template name="getCouplingFieldBFGID">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="instanceID" select="$instance"/>
      <xsl:with-param name="entryPointName" select="$epName"/>
      <xsl:with-param name="argumentID" select="$id"/>
    </xsl:call-template>
  </xsl:variable>
  <comment>The BFG ID string for this coupling field is: 
  <xsl:value-of select="$couplingFieldBFGID"/></comment>

  <point lhs="coupling_field" rhs="component%coupling_fields({$couplingFieldBFGID}%coupling_field_no)"/>
  <point lhs="coupling_field_ep_inst" rhs="coupling_field%ep_instances(remoteID)"/>
  <point lhs="coupling_field_arg_inst" rhs="coupling_field_ep_inst%get_instance"/>

  <comment>Receive data from sending entry point argument</comment>
  <xsl:if test="$oasisvis='false'">
    <call name="prism_get">
      <arg name="coupling_field_arg_inst%prism_id"/>
      <arg name="model_time"/>
      <arg name="model_time_bounds"/>
      <arg name="arg1"/>
      <arg name="coupling_info"/>
      <arg name="ierror"/>
    </call>
  </xsl:if>
  <xsl:if test="$oasisvis!='false'">
    <call name="oasisvis_prism_get">
      <arg name="info(myID)%su"/>
      <arg name="info(remoteID)%su"/>
      <arg name="coupling_field_arg_inst%msg_tag" type="string" readonly="true"/>
      <arg name="coupling_field_arg_inst%prism_id"/>
      <arg name="model_time"/>
      <arg name="model_time_bounds"/>
      <arg name="arg1"/>
      <arg name="coupling_info"/>
      <arg name="ierror"/>
    </call>
  </xsl:if>
 <!-- Code below from hardcoded flumeEg2 BFG2target2.f90
     subroutine get_r2d(var,dim1,dim2,id)
     integer, intent(in) :: id
     integer :: dim1, dim2, info
     real, intent(out) :: var(dim1,dim2)
     real :: hackvar(dim1,dim2,1)
     integer :: i,j
     print *,"Hello from get_r2d with id ",id
     call prism_get ( var_id, model_time, model_time_bounds, hackvar, &
                                          info, ierror )
      print *,"prism_get return code is: ",info
      do i=1,dim1
        do j=1,dim2
            var(i,j)=hackvar(i,j,1)
          end do
      end do
      -->
</xsl:template>


<!-- This code is destined for BFG2InPlace.f90 -->
<xsl:template name="oasis4Send">
  <comment>oasis4Send</comment>
  <xsl:variable name="modelName" select="@modelName"/>
  <xsl:variable name="epName" select="@epName"/>
  <xsl:variable name="id" select="@id"/>
  <xsl:variable name="instance" select="@instance"/>
  <xsl:variable name="messageTag" select="@tag"/>
  <xsl:variable name="couplingFieldBFGID">
    <xsl:call-template name="getCouplingFieldBFGID">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="instanceID" select="$instance"/>
      <xsl:with-param name="entryPointName" select="$epName"/>
      <xsl:with-param name="argumentID" select="$id"/>
    </xsl:call-template>
  </xsl:variable>
  <comment>The BFG ID string for this coupling field is: 
  <xsl:value-of select="$couplingFieldBFGID"/></comment>

  <!-- IRH: TODO: Might be worth asserting that: 
  coupling_field%bfg_id == '{$couplingFieldBFGID}' -->
  <point lhs="coupling_field" rhs="component%coupling_fields({$couplingFieldBFGID}%coupling_field_no)"/>
  <point lhs="coupling_field_ep_inst" rhs="coupling_field%ep_instances(remoteID)"/>
  <point lhs="coupling_field_arg_inst" rhs="coupling_field_ep_inst%put_instances"/>

  <comment>Send data to receiving entry point argument(s)</comment>
  <while>
    <condition><associated pointer="coupling_field_arg_inst"/></condition>
    <action>
      <xsl:if test="$oasisvis='false'">
        <!-- Note: "component", "model_time" etc. are in the BFG2Target module -
        perhaps not good practice to have global data from another module used 
        directly like this.  Not sure how much it matters here though - modules 
        are closely related, and the XSL *is* in the same file, which is more 
        important since that's what programmers work with. -->
        <call name="prism_put">
          <arg name="coupling_field_arg_inst%prism_id"/>
          <arg name="model_time"/>
          <arg name="model_time_bounds"/>
          <arg name="arg1"/>
          <arg name="coupling_info"/>
          <arg name="ierror"/>
        </call>
      </xsl:if>
      <xsl:if test="$oasisvis!='false'">
        <call name="oasisvis_prism_put">
          <arg name="info(myID)%su"/>
          <arg name="info(remoteID)%su"/>
          <arg name="coupling_field_arg_inst%msg_tag" type="string" readonly="true"/>
          <arg name="coupling_field_arg_inst%prism_id"/>
          <arg name="model_time"/>
          <arg name="model_time_bounds"/>
          <arg name="arg1"/>
          <arg name="coupling_info"/>
          <arg name="ierror"/>
        </call>
      </xsl:if>
      <point lhs="coupling_field_arg_inst" rhs="coupling_field_arg_inst%next_instance"/>
    </action>
  </while>
  <!-- IRH: TODO: coupling fields for gridless grids should be passed to 
  prism_put as 3D arrays.  As a first step, we can ensure they're passed
  to BFG put as 3D arrays, i.e. that they're specified as such in definition.xml,
  but this isn't good as definition.xml shouldn't be dependent on the comms type 
  (MPI/OASIS4) that we're using. Currently it works just to send arg1 to
  OASIS4 as a 2D array - hackvar doesn't appear to be necessary.  This musn't
  be relied upon though.  See commented out code below: -->
  
     <!-- Code below from hardcoded flumeEg2 BFG2Target1.f90
     subroutine put_r2d(var,dim1,dim2,id)
     integer, intent(in) :: id
     integer, intent(in) :: dim1,dim2
     real, intent(in) :: var(dim1,dim2)
     real, dimension(dim1,dim2,1) :: hackvar
     integer :: i,j,info

     do i=1,dim1
       do j=1,dim2
           hackvar(i,j,1) = var(i,j)
         end do
     end do
     print *,"Hello from put_r2d with id ",id
     call prism_put ( var_id, model_time, model_time_bounds, hackvar, &
                                                          info, ierror )
      print *,"prism_put return code is: ",info
      -->
</xsl:template>


<xsl:template name="oasis4ConfigFiles">
  <xsl:param name="DUIndex"/>
<!-- RF create all required xml config files -->
<xsl:if test="$DUIndex='false'">
  <xsl:call-template name="sccConfigFileCreate"/>
</xsl:if>
<xsl:for-each select="document(//deployment)//deploymentUnit">
  <xsl:if test="$DUIndex=position() or $DUIndex='false'">
    <xsl:for-each select="sequenceUnit">
      <!-- create a smioc file for each sequence unit in this du -->
      <xsl:call-template name="smiocConfigFileCreate"/>
    </xsl:for-each>
  </xsl:if>
</xsl:for-each>
</xsl:template>


<xsl:template name="sccConfigFileCreate">
 <xsl:variable name="FilePath">
  <xsl:value-of select="concat($OutDir, '/', $sccFileName)"/>
 </xsl:variable>
 <!-- try to find some support for outputing files -->
 <xsl:choose>
  <xsl:when test="element-available('common:document')">
   <common:document href="{$FilePath}" method="xml">
    <xsl:call-template name="sccConfigFileContent"/>
   </common:document>
  </xsl:when>
  <xsl:when test="element-available('xalanredirect:write')">
   <xalanredirect:write select="$FilePath">
    <xsl:call-template name="sccConfigFileContent"/>
   </xalanredirect:write>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>No file output support found</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="sccConfigFileContent">
<scc>
   <experiment local_name="testXXX"
               long_name="Flume testXXX"
               start_mode="not_spawn">
      <driver>
         <nbr_procs type="xs:integer"><xsl:value-of select="$nDriverProcs"/></nbr_procs>
      </driver>

<!-- start and end time calibration with BFG2 still needs to be thought about -->
      <start_date>
         <date>
            <second type="xs:double">0.0</second>
            <minute type="xs:integer">0</minute>
            <hour type="xs:integer">0</hour>
            <day type="xs:integer">1</day>
            <month type="xs:integer">1</month>
            <year type="xs:integer">1994</year>
         </date>
      </start_date>
      <end_date>
         <date>
            <second type="xs:double">0.0</second>
            <minute type="xs:integer">0</minute>
            <hour type="xs:integer">0</hour>
            <day type="xs:integer">1</day>
            <month type="xs:integer">1</month>
            <year type="xs:integer">2094</year>
         </date>
      </end_date>
   </experiment>

<!-- start and end time calibration with BFG2 still needs to be thought about -->
   <run>
      <start_date>
         <date>
            <second type="xs:double">0.0</second>
            <minute type="xs:integer">0</minute>
            <hour type="xs:integer">0</hour>
            <day type="xs:integer">1</day>
            <month type="xs:integer">1</month>
            <year type="xs:integer">1994</year>
         </date>
      </start_date>

      <end_date>
         <date>
            <second type="xs:double">0.0</second>
            <minute type="xs:integer">0</minute>
            <hour type="xs:integer">0</hour>
            <day type="xs:integer">1</day>
            <month type="xs:integer">1</month>
            <year type="xs:integer">1995</year>
         </date>
      </end_date>
   </run>

<xsl:for-each select="document(coupled/deployment)/deployment//deploymentUnit">
<xsl:variable name="appName">
  <xsl:call-template name="createAppName">
    <xsl:with-param name="deploymentUnit" select="."/>
  </xsl:call-template>
</xsl:variable>

<!-- Exe name only used in spawn mode. -->
<xsl:variable name="appExeName" select="'XXX'"/>
<xsl:variable name="npes" select="sum(sequenceUnit/@threads)"/>

   <application local_name="{$appName}"
                executable_name="{$appExeName}"
                redirect="false">

      <argument>args</argument>

      <!-- Host name only used in spawn mode. -->
      <host local_name="host1">
         <nbr_procs type="xs:integer"><xsl:value-of select="$npes"/></nbr_procs>
      </host>

      <!-- Insert the sequence unit data for each component -->
      <xsl:call-template name="sccComponentRanks">
        <xsl:with-param name="sequenceUnits" select="sequenceUnit"/>
      </xsl:call-template>

<!-- Assigning ranks like this won't work where a DU has > 1 components as
<min_value> will not be 0 for the second component.
<xsl:for-each select="sequenceUnit">
<xsl:variable name="compName">
<xsl:call-template name="createCompName">
<xsl:with-param name="sequenceUnit" select="."/>
</xsl:call-template>
</xsl:variable>
<xsl:variable name="nCompPes" select="@threads"/>
      <component local_name="{$compName}">
           <rank>
               <min_value type="xs:integer">0</min_value>
               <max_value type="xs:integer"><xsl:value-of select="$nCompPes - '1'"/></max_value>
           </rank>
      </component>
</xsl:for-each>
-->
   </application>
</xsl:for-each>
</scc>
</xsl:template>


<!-- sccComponentRanks adds component rank information to the 
OASIS4 SCC XML file -->
<xsl:template name="sccComponentRanks">
  <xsl:param name="sequenceUnits"/>
  <xsl:param name="rankCount" select="0"/>

  <xsl:if test="$sequenceUnits">
    <xsl:variable name="thisSequenceUnit" select="$sequenceUnits[1]"/>
    <xsl:variable name="thisRankCount" select="$thisSequenceUnit/@threads"/>

    <!-- Calculate the component rank data --> 
    <xsl:variable name="compName">
      <xsl:call-template name="createCompName">
        <xsl:with-param name="sequenceUnit" select="$thisSequenceUnit"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="minRank" select="$rankCount"/>
    <xsl:variable name="maxRank" select="$rankCount + $thisRankCount - 1"/>

    <!-- Create the SCC XML with the component rank data inserted -->
    <component local_name="{$compName}">
          <rank>
              <min_value type="xs:integer"><xsl:value-of select="$minRank"/></min_value>
              <max_value type="xs:integer"><xsl:value-of select="$maxRank"/></max_value>
              <!-- Rank increment is assumed to be 1 -->
          </rank>
    </component>

    <!-- Process next component -->
    <xsl:call-template name="sccComponentRanks">
      <xsl:with-param name="sequenceUnits" select="$sequenceUnits[position() > 1]"/>
      <xsl:with-param name="rankCount" select="$rankCount + $thisRankCount"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>


<!-- createAppName creates an application name for the given deployment unit -->
<xsl:template name="createAppName">
<xsl:param name="deploymentUnit"/>
  <xsl:text>DU</xsl:text>
  <xsl:call-template name="getDeploymentUnitID">
    <xsl:with-param name="modelName" select="$deploymentUnit/sequenceUnit[1]/model[1]/@name"/>
    <xsl:with-param name="modelInstance" select="$deploymentUnit/sequenceUnit[1]/model[1]/@instance"/>
  </xsl:call-template>
</xsl:template>
<!-- createAppName creates an application name for the given deployment unit
NO: application names are too long (>255 chars) for GENIE configurations. -->
<!--
<xsl:template name="createAppName">
<xsl:param name="deploymentUnit"/>
 <xsl:text>DU</xsl:text>
 -->
 <!-- Add the sequence unit name(s) for this deployment unit -->
 <!--
 <xsl:for-each select="$deploymentUnit/sequenceUnit">
  <xsl:call-template name="createCompName">
   <xsl:with-param name="sequenceUnit" select="."/>
  </xsl:call-template>
  <xsl:if test="position() != last()">
  </xsl:if>
 </xsl:for-each>
</xsl:template>
-->


<!-- createCompName creates a component name for the given sequence unit -->
<xsl:template name="createCompName">
  <xsl:param name="sequenceUnit"/>
  <xsl:text>SU</xsl:text>
  <xsl:call-template name="getSequenceUnitID">
    <xsl:with-param name="modelName" select="$sequenceUnit/model[1]/@name"/>
    <xsl:with-param name="modelInstance" select="$sequenceUnit/model[1]/@instance"/>
  </xsl:call-template>
</xsl:template>
<!-- createCompName creates a component name for the given sequence unit
NO: component names are too long (>255 chars) for GENIE configurations. -->
<!--
<xsl:template name="createCompName">
  <xsl:param name="sequenceUnit"/>
  <xsl:text>SU</xsl:text>
  -->
  <!-- Add the model name(s) for this sequence unit, excluding intrinsic transforms -->
  <!--
  <xsl:for-each select="$sequenceUnit/model">
    <xsl:if test="document($root/coupled/models/model)/definition[name=current()/@name]/language != 'intrinsic'">
      <xsl:value-of select="@name"/>
      -->
      <!-- Add any instance number -->
      <!--
      <xsl:if test="@instance">
        <xsl:value-of select="@instance"/>
      </xsl:if>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
-->


<!-- getCompNameForField gets the name of the component associated with the
given coupling field.
Note: within a deployment unit, every modelName/instance pair is assumed to be unique,
but a modelName/instance pair might be repeated in another deployment unit - currently
I'm assuming it won't be. -->
<xsl:template name="getCompNameForField">
  <xsl:param name="couplingField"/>
  <xsl:for-each select="document($root/coupled/deployment)//sequenceUnit">
    <xsl:if test="model[@name=$couplingField/@modelName and
                  (not(number($couplingField/@instance)) or 
                  @instance=$couplingField/@instance)]">
      <xsl:call-template name="createCompName">
        <xsl:with-param name="sequenceUnit" select="."/>
      </xsl:call-template>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


<!-- create and output smiocConfig file here! -->
<!-- IRH: modified to use the format PRISM specify for SMIOC file
names (previously was [model_name]smioc.xml) -->
<xsl:template name="smiocConfigFileCreate">
 <!-- Get application and component names -->
 <xsl:variable name="applName">
  <xsl:call-template name="createAppName">
   <xsl:with-param name="deploymentUnit" select="ancestor::deploymentUnit"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:variable name="compName">
  <xsl:call-template name="createCompName">
   <xsl:with-param name="sequenceUnit" select="."/>
  </xsl:call-template>
 </xsl:variable>
 <!-- Create SMIOC file name in format [appl_name]_[comp_name]_smioc.xml -->
 <!--<xsl:variable name="localName" select="model/@name"/>-->
 <xsl:variable name="FilePath">
  <xsl:value-of select="concat($OutDir, '/', $applName, '_', $compName, '_', 'smioc.xml')"/>
 </xsl:variable>
 <!-- Get content and write to SMIOC file -->
 <xsl:choose>
  <xsl:when test="element-available('common:document')">
   <common:document href="{$FilePath}" method="xml">
    <xsl:call-template name="smiocConfigFileContent">
     <xsl:with-param name="compName" select="$compName"/>
     <xsl:with-param name="sequenceUnit" select="."/>
    </xsl:call-template>
   </common:document>
  </xsl:when>
  <xsl:when test="element-available('xalanredirect:write')">
   <xalanredirect:write select="$FilePath">
    <xsl:call-template name="smiocConfigFileContent">
     <xsl:with-param name="compName" select="$compName"/>
     <xsl:with-param name="sequenceUnit" select="."/>
    </xsl:call-template>
   </xalanredirect:write>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>No file output support found</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="smiocConfigFileContent">
  <xsl:param name="compName"/>
  <xsl:param name="sequenceUnit"/>

  <component local_name="{$compName}">

    <xsl:variable name="sequenceUnitID">
      <xsl:call-template name="getSequenceUnitID">
        <xsl:with-param name="modelName" select="$sequenceUnit/model[1]/@name"/>
        <xsl:with-param name="modelInstance" select="$sequenceUnit/model[1]/@instance"/>
      </xsl:call-template>
    </xsl:variable>
      
    <!-- Search for entry point arguments that require point-to-point communications.
    These are our coupling fields. -->
    <xsl:variable name="entryPointArgs" select="$commFormsRoot/communicationForms/tuple[
      @su = $sequenceUnitID and (contains(@form,'get') or contains(@form,'put'))]"/>

    <xsl:call-template name="smiocConfigFileGrids">
      <xsl:with-param name="entryPointArgTuples" select="$entryPointArgs"/>
    </xsl:call-template>

<!-- for each field that I output **inplace** -->
<!-- then for each field that I input **inplace** -->
<!-- RF - For simplicity I'm thinking of inputting the phase4 or
     phase5 temp xml from the InPlace generation as this has
     the calculated inplace puts and gets.-->
<!-- RF - the BIG problem is that the Oasis4 will only work
     if we have static connections - thus our set notation
     and Genie models are not easily supported in Oasis4. I think
     we can get away with creating an appropriate transformation
     but this is going to significantly complicate code gen -->

    <xsl:for-each select="$entryPointArgs">

      <!-- Get the definition of the coupling field in the definition doc. -->
      <xsl:variable name="couplingField" select="document($root/coupled/models/model)/
        definition[name = current()/@modelName]//
        entryPoint[@name = current()/@epName]/data[@id = current()/@id]"/>

      <!-- Note that the argument's name is optional; although we store it, we also
      generate a name (BFGID, below) guaranteed to be unique using the argument ID,
      model name (and instance number), and entry point name - e.g. "mod1_run_arg1".  -->
      <xsl:variable name="couplingFieldBFGID">
        <xsl:call-template name="getCouplingFieldBFGID">
          <xsl:with-param name="modelName" select="@modelName"/>
          <xsl:with-param name="instanceID" select="@instance"/>
          <xsl:with-param name="entryPointName" select="@epName"/>
          <xsl:with-param name="argumentID" select="@id"/>
        </xsl:call-template>
      </xsl:variable>

      <!-- This coupling field has one or more inputs (may also have outputs,
      but we create separate PRISM transients for those). -->
      <xsl:if test="current()[contains(@form,'get')]"> 

        <!-- Create a separate transient for each source -->
        <xsl:call-template name="processCorresFields">
          <xsl:with-param name="processType" select="'smiocTransients'"/>
          <xsl:with-param name="direction" select="'get'"/>
          <xsl:with-param name="entryPointArg" select="current()"/>
          <xsl:with-param name="couplingField" select="$couplingField"/>
          <xsl:with-param name="couplingFieldBFGID" select="$couplingFieldBFGID"/>
        </xsl:call-template>

      </xsl:if>

      <!-- This coupling field has one or more outputs (may also have inputs,
      but we create separate PRISM transients for those). -->
      <xsl:if test="current()[contains(@form,'put')]"> 

        <!-- Create a separate transient for each destination -->
        <xsl:call-template name="processCorresFields">
          <xsl:with-param name="processType" select="'smiocTransients'"/>
          <xsl:with-param name="direction" select="'put'"/>
          <xsl:with-param name="entryPointArg" select="current()"/>
          <xsl:with-param name="couplingField" select="$couplingField"/>
          <xsl:with-param name="couplingFieldBFGID" select="$couplingFieldBFGID"/>
        </xsl:call-template>

      </xsl:if>
    </xsl:for-each>

  </component>
</xsl:template>


<!-- Adds all grids associated with this component (SU) to the SMIOC -->
<xsl:template name="smiocConfigFileGrids">
  <xsl:param name="entryPointArgTuples"/>
  <xsl:param name="smiocGridNames" select="' '"/>

  <xsl:if test="$entryPointArgTuples">

    <xsl:variable name="currentTuple" select="$entryPointArgTuples[1]"/>

    <!-- Get the definition of the coupling field in the definition doc. -->
    <xsl:variable name="couplingField" select="document($root/coupled/models/model)/
      definition[name = $currentTuple/@modelName]//
      entryPoint[@name = $currentTuple/@epName]/data[@id = $currentTuple/@id]"/>

    <xsl:variable name="setPosition">
      <xsl:call-template name="getCouplingFieldSetNumber">
        <xsl:with-param name="entryPointArgTuple" select="$currentTuple"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$setPosition = ''">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): smiocConfigFileGrids: coupling field missing set in compose.xml.</xsl:text>
      </xsl:message>
    </xsl:if>
    <!--
    <xsl:message terminate="no">
      <xsl:text>setPosition is: </xsl:text>
      <xsl:value-of select="$setPosition"/>
    </xsl:message>
    -->

    <xsl:variable name="gridType">
      <xsl:call-template name="getCouplingFieldGridType">
        <xsl:with-param name="couplingField" select="$couplingField"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$gridType = ''">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text>
        <xsl:value-of select="$thisFileName"/>
        <xsl:text>): smiocConfigFileGrids: coupling field missing grid.</xsl:text>
      </xsl:message>
    </xsl:if>
    <!--
    <xsl:message terminate="no">
      <xsl:text>gridType is: </xsl:text>
      <xsl:value-of select="$gridType"/>
    </xsl:message>
    -->

    <!-- Make a unique grid name - by adding the set number for gridless grid
    coupling fields if necessary. -->
    <xsl:variable name="gridName">
      <xsl:choose>
        <xsl:when test="$gridType = $gridlessGridType">
          <xsl:value-of select="concat($gridlessGridPrefix,$setPosition)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$gridType"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!--
    <xsl:message terminate="no">
      <xsl:text>gridName is: </xsl:text>
      <xsl:value-of select="$gridName"/>
    </xsl:message>
    -->

    <!-- Check that this grid hasn't already been written to the SMIOC - 
    many coupling fields may of course be passed over the same grid.
    N.B. We must put spaces around the grid name, because we might have
    'gridless_grid_for_set_40' in initGridNames and want to check for
    the existence of 'gridless_grid_for_set_4'. -->
    <xsl:if test="not(contains($smiocGridNames, concat(' ', $gridName, ' ')))">

      <!-- Now choose what kind of grid to write to the SMIOC -->
      <xsl:choose>
        <xsl:when test="$gridType = $gridlessGridType">
          <xsl:call-template name="smiocConfigFileGridlessGrid">
            <xsl:with-param name="gridName" select="$gridName"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="smiocConfigFileGriddedGrid">
            <xsl:with-param name="gridName" select="$gridName"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:if>

    <!-- Go to next coupling field -->
    <xsl:call-template name="smiocConfigFileGrids">
      <xsl:with-param name="entryPointArgTuples" select="$entryPointArgTuples[position() > 1]"/>
      <!-- No harm in adding gridName to smiocGridNames if it's already there
      - string won't get ridiculously long. -->
      <xsl:with-param name="smiocGridNames" select="concat($smiocGridNames, $gridName, ' ')"/>
    </xsl:call-template>
  </xsl:if>

</xsl:template>


<xsl:template name="smiocConfigFileGridlessGrid">
  <xsl:param name="gridName"/>

  <gridfamily local_name="{$gridName}_family">
    <grid local_name="{$gridName}">
      <sampled_space pole_covered="false"
                    grid_type="PRISM_gridless">
  <!-- RF not sure what this is all about -->
        <indexing_dimension local_name="i"/>
        <indexing_dimension local_name="j"/>
        <indexing_dimension local_name="k"/>
        <!-- IRH: Extent element is optional - leaving it out ATM.
        <indexing_dimension local_name="i">
          <extent>2</extent>
        </indexing_dimension>
        -->
      </sampled_space>
    </grid>
  </gridfamily>
</xsl:template>


<xsl:template name="smiocConfigFileGriddedGrid">
  <xsl:param name="gridName"/>

  <xsl:variable name="gridMosaic"
    select="document($root/coupled/grids/grid)/gridspec/mosaic[@id = $gridName]"/>
  <xsl:if test="count($gridMosaic) != 1">
    <xsl:message terminate="yes">
      <xsl:text>Error (</xsl:text>
      <xsl:value-of select="$thisFileName"/>
      <xsl:text>): SMIOC: gridName unrecognised or not unique.</xsl:text>
    </xsl:message>
  </xsl:if>

  <xsl:variable name="prismGridType">
    <xsl:call-template name="getPRISMGridType">
      <xsl:with-param name="gridDescriptor" select="$gridMosaic/grid_descriptor"/>
    </xsl:call-template>
  </xsl:variable>

  <gridfamily local_name="{$gridMosaic/@id}_family">
    <grid local_name="{$gridMosaic/@id}">
      <sampled_space pole_covered="{$gridMosaic/mosaicgrid/pole_covered}"
        grid_type="{$prismGridType}">
        <!-- RF not sure what this is all about -->
        <indexing_dimension local_name="i"/>
        <indexing_dimension local_name="j"/>
        <indexing_dimension local_name="k"/>
        <!-- IRH: Extent element is optional - leaving it out ATM.
        <indexing_dimension local_name="i">
          <extent>64</extent>
        </indexing_dimension>
        -->
      </sampled_space>
    </grid>
  </gridfamily>
</xsl:template>


<!-- Adds a transient (coupling field) element to the SMIOC -->
<xsl:template name="smiocTransient">
  <xsl:param name="direction"/>
  <xsl:param name="transientName"/>
  <xsl:param name="couplingField"/>
  <xsl:param name="couplingFieldBFGID"/>
  <xsl:param name="entryPointOfThisCouplingField"/>
  <xsl:param name="entryPointOfRemoteCouplingField"/>
  <xsl:param name="entryPointOfRemoteCouplingFieldBFGID"/>
  <xsl:param name="gridType"/>
  <xsl:param name="transformName"/>

  <xsl:variable name="couplingFieldLongName">
    <xsl:choose>
      <xsl:when test="string($couplingField/@name)">
        <xsl:value-of select="$couplingField/@name"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>NO_NAME</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <transient local_name="{$transientName}" long_name="{$couplingFieldLongName}">

    <!-- IRH: TODO: CF-compliant name - where will this come from? definition.xml?
    transient_standard_name is mandatory in the SMIOC. -->
    <transient_standard_name>CF_standard_name_for_<xsl:value-of select="$couplingFieldBFGID"/></transient_standard_name>

    <!-- IRH: TODO: Where will information about units come from? definition.xml?
    physics element is optional in the SMIOC - leaving it out for now. -->
    <!--
    <physics transient_type="single">
      <physical_units>J/s</physical_units>
      <valid_min>-1.0</valid_min>
      <valid_max>1100.0</valid_max>
    </physics>
    -->

    <xsl:variable name="xmlSchemaDataType">
      <xsl:call-template name="getXMLSchemaDataType">
        <xsl:with-param name="bfgDataType" select="$couplingField/@dataType"/>
      </xsl:call-template>
    </xsl:variable>
    <numerics datatype="{$xmlSchemaDataType}"/>

    <!-- IRH: TODO: Where does mask info come from?  Gridspec?
    method_type - should come from the same place as CF-compliant name and 
    physical info above.
    associated_gridfamily local_name?
    Can also give the associated point set here.
    Since all this information is optional, I assume it's just there to
    double-check against the information about masks, grids and point-sets
    for the coupling field passed to OASIS by the PSMILE calls. (prism_def_var etc.) -->
    <!--
    <computation mask="false" mask_time_dependency="false" method_type="mean">
      <associated_gridfamily local_name="my_gridless_grid" />
    </computation>
    -->

    <intent>
      <xsl:choose>
        <xsl:when test="$direction = 'get'">
          <xsl:call-template name="smiocInputTransient">
            <xsl:with-param name="transientName" select="$transientName"/>
            <xsl:with-param name="gridType" select="$gridType"/>
            <xsl:with-param name="transformName" select="$transformName"/>
            <xsl:with-param name="entryPointOfThisCouplingField" select="$entryPointOfThisCouplingField"/>
            <xsl:with-param name="entryPointOfThisCouplingFieldBFGID" select="$couplingFieldBFGID"/>
            <xsl:with-param name="entryPointThatPutsToThisCouplingField" select="$entryPointOfRemoteCouplingField"/>
            <xsl:with-param name="entryPointThatPutsToThisCouplingFieldBFGID" select="$entryPointOfRemoteCouplingFieldBFGID"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="$direction = 'put'">
          <xsl:call-template name="smiocOutputTransient">
            <xsl:with-param name="transientName" select="$transientName"/>
            <xsl:with-param name="gridType" select="$gridType"/>
            <xsl:with-param name="entryPointOfThisCouplingField" select="$entryPointOfThisCouplingField"/>
            <xsl:with-param name="entryPointOfThisCouplingFieldBFGID" select="$couplingFieldBFGID"/>
            <xsl:with-param name="entryPointThatGetsFromThisCouplingField" select="$entryPointOfRemoteCouplingField"/>
            <xsl:with-param name="entryPointThatGetsFromThisCouplingFieldBFGID" select="$entryPointOfRemoteCouplingFieldBFGID"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="yes">
            <xsl:text>Error (</xsl:text>
            <xsl:value-of select="$thisFileName"/>
            <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
          </xsl:message>
        </xsl:otherwise>
      </xsl:choose>
    </intent>
  </transient>
</xsl:template>


<!-- Adds an input element to an INPUT transient (coupling field) in the SMIOC -->
<xsl:template name="smiocInputTransient">
  <xsl:param name="transientName"/>
  <xsl:param name="gridType"/>
  <xsl:param name="transformName"/>
  <xsl:param name="entryPointOfThisCouplingField"/>
  <xsl:param name="entryPointOfThisCouplingFieldBFGID"/>
  <xsl:param name="entryPointThatPutsToThisCouplingField"/>
  <xsl:param name="entryPointThatPutsToThisCouplingFieldBFGID"/>

  <input>
    <!-- IRH: TODO: Minimal period and exchange date - where will this information
    come from (not gridspec). For the gridless grid example, it's the same for
    all coupling fields. -->
    <minimal_period>
      <nbr_mins>30</nbr_mins>
    </minimal_period>

    <exchange_date>
      <period>
        <month>1</month>
      </period>
    </exchange_date>

    <origin transi_in_name="{$transientName}_in">

      <!-- Generate the name of the component that the corresponding field is received from. -->
      <xsl:variable name="compCorresName"> 
        <xsl:call-template name="getCompNameForField">
          <xsl:with-param name="couplingField" select="$entryPointThatPutsToThisCouplingField"/>
        </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="correspTransiOutName">
        <xsl:call-template name="getTransientName">
          <xsl:with-param name="myBFGID" select="$entryPointThatPutsToThisCouplingFieldBFGID"/>
          <xsl:with-param name="remoteBFGID" select="$entryPointOfThisCouplingFieldBFGID"/>
          <xsl:with-param name="remoteSU" select="$entryPointOfThisCouplingField/@su"/>
          <xsl:with-param name="direction" select="'put'"/>
          <xsl:with-param name="suffix" select="'_out'"/>
        </xsl:call-template>
      </xsl:variable>
      <corresp_transi_out_name><xsl:value-of select="$correspTransiOutName"/></corresp_transi_out_name>

      <component_name><xsl:value-of select="$compCorresName"/></component_name>

      <middle_transformation>
        <!-- <xsl:if test="$gridType != $gridlessGridType and $transformName != $notIntrinsic"> -->
        <xsl:if test="$gridType != $gridlessGridType">
          <!-- There's a gridspec (a geographical grid) associated with this
          transient. We therefore need an interpolation method. -->
          <interpolation>
            <interp2D>
              <xsl:choose>
                <xsl:when test="contains($transformName, 'bilinear')">
                  <bilinear>
                    <para_search>local</para_search>
                    <if_masked>nneighbour</if_masked>
                  </bilinear>
                </xsl:when>
                <xsl:when test="contains($transformName, 'nneighbour2D')">
                  <nneighbour2D>
                    <para_search>local</para_search>
                    <if_masked>nneighbour</if_masked>
                    <!-- The simplest method - base interpolated value
                    only on the very nearest neighbour. -->
                    <nbr_neighbours>1</nbr_neighbours>
                  </nneighbour2D>
                </xsl:when>
                <xsl:when test="contains($transformName, 'bicubic')">
                  <bicubic>
                    <para_search>local</para_search>
                    <if_masked>nneighbour</if_masked>
                    <!-- This is the default method -->
                    <bicubic_method>sixteen</bicubic_method>
                  </bicubic>
                </xsl:when>
                <xsl:when test="contains($transformName, 'conservativ2D')">
                  <conservativ2D>
                    <!-- Only first-order conservative remapping is
                    currently available -->
                    <order>first</order>
                    <normalisation2D>
                      <!-- TODO: Not sure which method here. -->
                      <methodnorm2D>fracarea</methodnorm2D>
                    </normalisation2D>
                  </conservativ2D>
                </xsl:when>
                <!-- This transformation is between identical grids;
                OASIS4 appears to still want an interpolation method though.
                Use NN2D because it's probably the quickest. -->
                <xsl:when test="contains($transformName, $notIntrinsic)">
                  <nneighbour2D>
                    <para_search>local</para_search>
                    <if_masked>nneighbour</if_masked>
                    <!-- The simplest method - base interpolated value
                    only on the very nearest neighbour. -->
                    <nbr_neighbours>1</nbr_neighbours>
                  </nneighbour2D>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:message terminate="yes">
                    <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
                    <xsl:text>): Unrecognised interpolation method: </xsl:text>
                    <xsl:value-of select="$transformName"/>
                  </xsl:message>
                </xsl:otherwise>
              </xsl:choose>
            </interp2D>
            <!-- IRH: TODO: Just assuming 2D coupling ATM.  Note that
            we must provide an "interp1D" element too, as all grids are
            3D as far as OASIS4 is concerned (2D grids are 3D grids with
            one vertical level). -->
            <interp1D><none/></interp1D>
          </interpolation>
        </xsl:if>
      </middle_transformation>

    </origin>

    <!-- IRH: TODO: No transformations to be carried out by OASIS4
    for this example.  Where will this information come from? -->
    <!--
    <target_transformation>
      <target_local_transformation>
        <add_scalar>0.0</add_scalar>
      </target_local_transformation>
      <statistics>
      </statistics>
    </target_transformation>
    -->

    <!-- NetCDF output for transients -->
    <xsl:choose>
      <!-- IRH: TODO: There is a bug with debug NetCDF output for gridless   
      grids (at least when a component has a gridded grid as well)
      so I am turning off the output for gridless grids for the moment,
      and referring the matter to Sophie/the PRISM team.
      Update: this is a known issue on their TODO list. -->
      <xsl:when test="$gridType = $gridlessGridType">
        <debug_mode>false</debug_mode>
      </xsl:when>
      <xsl:otherwise>
        <!-- Currently there's a limit on the number of NetCDF debug output
        files OASIS can currently handle.  Better to set this manually for just
        the transients we're interested in, rather than for all transients 
        (potentially a very large number - >100 - with GENIE). -->
        <debug_mode>false</debug_mode>
        <!-- <debug_mode>true</debug_mode> -->
      </xsl:otherwise>
    </xsl:choose>  
  </input>
</xsl:template>


<!-- Adds an OUTPUT transient (coupling field) to the SMIOC -->
<xsl:template name="smiocOutputTransient">
  <xsl:param name="transientName"/>
  <xsl:param name="gridType"/>
  <xsl:param name="entryPointOfThisCouplingField"/>
  <xsl:param name="entryPointOfThisCouplingFieldBFGID"/>
  <xsl:param name="entryPointThatGetsFromThisCouplingField"/>
  <xsl:param name="entryPointThatGetsFromThisCouplingFieldBFGID"/>

  <output transi_out_name="{$transientName}_out">
    <minimal_period>
      <nbr_mins>30</nbr_mins>
    </minimal_period>

    <exchange_date>
      <period>
        <month>1</month>
      </period>
    </exchange_date>

    <!-- Generate the name of the component that the current corresponding field is sent to. -->
    <xsl:variable name="compCorresName"> 
      <xsl:call-template name="getCompNameForField">
        <xsl:with-param name="couplingField" select="$entryPointThatGetsFromThisCouplingField"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="correspTransiInName">
      <xsl:call-template name="getTransientName">
        <xsl:with-param name="myBFGID" select="$entryPointThatGetsFromThisCouplingFieldBFGID"/>
        <xsl:with-param name="remoteBFGID" select="$entryPointOfThisCouplingFieldBFGID"/>
        <xsl:with-param name="remoteSU" select="$entryPointOfThisCouplingField/@su"/>
        <xsl:with-param name="direction" select="'get'"/>
        <xsl:with-param name="suffix" select="'_in'"/>
      </xsl:call-template>
    </xsl:variable>
    <corresp_transi_in_name><xsl:value-of select="$correspTransiInName"/></corresp_transi_in_name>

    <component_name><xsl:value-of select="$compCorresName"/></component_name>

    <!-- IRH: TODO: No transformations for this example.
    Where will this information come from? -->
    <source_transformation>
      <statistics>
      </statistics>
    </source_transformation>

    <xsl:choose>
      <!-- IRH: TODO: There is a bug with debug NetCDF output for gridless   
      grids (at least when a component has a gridded grid as well)
      so I am turning off the output for gridless grids for the moment,
      and referring the matter to Sophie/the PRISM team.
      Update: this is a known issue on their TODO list. -->
      <xsl:when test="$gridType = $gridlessGridType">
        <debug_mode>false</debug_mode>
      </xsl:when>
      <xsl:otherwise>
        <!-- Currently there's a limit on the number of NetCDF debug output
        files OASIS can currently handle.  Better to set this manually for just
        the transients we're interested in, rather than for all transients 
        (potentially a very large number - >100 - with GENIE). -->
        <debug_mode>false</debug_mode>
        <!-- <debug_mode>true</debug_mode> -->
      </xsl:otherwise>
    </xsl:choose>  
  </output>
</xsl:template>


<!-- Generate a unique ID string for identifying a coupling field
based on BFG data about it. -->
<xsl:template name="getCouplingFieldBFGID">
 <xsl:param name="modelName"/>
 <xsl:param name="instanceID"/>
 <xsl:param name="entryPointName"/>
 <xsl:param name="argumentID"/>

 <xsl:value-of select="$modelName"/>
 <xsl:if test="number($instanceID)">
  <xsl:text>_inst</xsl:text><xsl:value-of select="$instanceID"/>
 </xsl:if>
 <xsl:text>_</xsl:text><xsl:value-of select="$entryPointName"/>
 <xsl:text>_arg</xsl:text><xsl:value-of select="$argumentID"/>
</xsl:template>


<!-- Get the entry point argument tuples in DS.xml that match (are in the same composition
set as) the given entry point argument tuple, and carry out the action specified by 
"processType" for each. -->
<xsl:template name="processCorresFields">
  <xsl:param name="processType"/>
  <xsl:param name="direction"/>
  <xsl:param name="entryPointArg"/>
  <xsl:param name="couplingField"/>
  <xsl:param name="couplingFieldBFGID"/>

  <!--
  <xsl:message terminate="no">
    <xsl:text>*** START of processCorresFields, processing: </xsl:text>
    <xsl:value-of select="$processType"/>
    <xsl:text> ***</xsl:text>
  </xsl:message>
  
  <xsl:message terminate="no">
    <xsl:text>entryPointArg is: </xsl:text>
    <xsl:value-of select="$entryPointArg/@modelName"/>
    <xsl:value-of select="$entryPointArg/@epName"/>
    <xsl:value-of select="$entryPointArg/@instance"/>
    <xsl:value-of select="$entryPointArg/@id"/>
  </xsl:message>
  -->
  
<!-- check the composition document (rather than DS.xml - DS.xml is only there to save refs,
not specify the composition), find the set to which the current field belongs, and if there
are any other fields in that set that belong to intrinsic transformations, find the set of
the 'other side' of the intrinsic field (i.e. if it's 'in', look for the 'out' counterpart)
and make a connection with the fields in the set of the 'other side'.
For fields in the set that do not belong to intrinsics, use that field itself - 
if two models use the same grid for coupling, there won't be an intrinsic transformation. -->

  <xsl:variable name="entryPointArgField" select="document($root/coupled/composition)/composition//set/
    field[@modelName=$entryPointArg/@modelName
      and @epName=$entryPointArg/@epName
      and (not(boolean(@instance)) or @instance=$entryPointArg/@instance)
      and @id=$entryPointArg/@id]"/>
  <xsl:if test="count($entryPointArgField) != 1">
    <xsl:message terminate="yes">
      <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
      <xsl:text>): Should only be one field in compose.xml for this entry point argument!</xsl:text>
    </xsl:message>
  </xsl:if>

  <!--
  <xsl:message terminate="no">
    <xsl:text>Count of entryPointArgField is: </xsl:text>
    <xsl:value-of select="count($entryPointArgField)"/>
  </xsl:message>

  <xsl:message terminate="no">
    <xsl:text>entryPointArgField is: </xsl:text>
    <xsl:value-of select="$entryPointArgField/@modelName"/>
    <xsl:value-of select="$entryPointArgField/@epName"/>
    <xsl:value-of select="$entryPointArgField/@instance"/>
    <xsl:value-of select="$entryPointArgField/@id"/>
  </xsl:message>
  -->

  <!-- Select all corresponding fields in the set - i.e. all fields apart from
  the one for which we're trying to find the partner (entryPointArgField).
  This includes both fields belonging to intrinsic transformations and other fields.
  TODO: Surely there's a simpler way to select all siblings of entryPointArgField?! -->
  <xsl:variable name="corresFields" select="$entryPointArgField/../
    field[not(@modelName = $entryPointArgField/@modelName
      and @epName = $entryPointArgField/@epName
      and (not(boolean($entryPointArgField/@instance)) or @instance = $entryPointArgField/@instance)
      and @id = $entryPointArgField/@id)]"/>
  <xsl:if test="count($corresFields) = 0">
    <xsl:message terminate="yes">
      <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
      <xsl:text>): No corresponding coupling field found (no put for this get, or vice versa)!</xsl:text>
    </xsl:message>
  </xsl:if>

  <!--
  <xsl:message terminate="no">
    <xsl:text>Count of corresFields is: </xsl:text>
    <xsl:value-of select="count($corresFields)"/>
  </xsl:message>

  <xsl:for-each select="$corresFields">
    <xsl:message terminate="no">
      <xsl:text>corresFields[</xsl:text>
      <xsl:value-of select="position()"/>
      <xsl:text>] is: </xsl:text>
      <xsl:value-of select="@modelName"/>
      <xsl:value-of select="@epName"/>
      <xsl:value-of select="@instance"/>
      <xsl:value-of select="@id"/>
    </xsl:message>
  </xsl:for-each>
  -->

  <!-- Get any intrinsic transformations used by this coupled configuration -->
  <xsl:variable name="intrinsicTransforms" select="document($root/coupled/models/model)/
    definition[language='intrinsic']"/>

  <xsl:for-each select="$corresFields">

    <!-- Get the argument ID of the partner of this field if this field
    is part of an intrinsic transformation - only select the side of the
    intrinsic field that matches the coupling direction we're processing. -->
    <xsl:variable name="partnerID">
      <xsl:choose>
        <xsl:when test="@modelName = $intrinsicTransforms/name and $direction = 'put' and @id = '1'">
          <xsl:value-of select="2"/>
        </xsl:when>
        <xsl:when test="@modelName = $intrinsicTransforms/name and $direction = 'get' and @id = '2'">
          <xsl:value-of select="1"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$invalidArgumentID"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="intrinsicTransformName">
      <xsl:choose>
        <xsl:when test="$partnerID != $invalidArgumentID">
          <xsl:value-of select="@modelName"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$notIntrinsic"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
  
    <!-- OK, so we know the model name, EP name, instance and arg ID of the
    other side of the intrinsic.
    Now we need to find the set that contains the other side of the intrinsic.
    (ASSUMPTION: only one matching set - different instances of an intrinsic
    transform will be used when there are two or more sources/destinations).
    Note that if the current corresField isn't an intrinsic, we get an empty
    partner set below as the partnerID has been set to 'invalidArgumentID'. -->
    <xsl:variable name="partnerSet" select="document($root/coupled/composition)/composition//set/
      field[@modelName = current()/@modelName
        and @epName = current()/@epName
        and (not(boolean(@instance)) or @instance=current()/@instance)
        and @id = $partnerID]/.."/>
    <xsl:if test="count($partnerSet) &gt; 1">
      <xsl:message terminate="yes">
        <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
        <xsl:text>): More than one partner set found in compose.xml for an intrinsic transform instance!</xsl:text>
      </xsl:message>
    </xsl:if>

    <!-- We have the partner set if the corresField was intrinsic.  Now:
    Either: select the field(s) in that set that do not belong to intrinsic
    transformations (N.B. there may be more than one such field);
    Or: select the current corresField itself, if the corresField was not an
    intrinsic (i.e. the partnerID is invalid). -->
    <xsl:variable name="partnerFields" select="
      ($partnerSet/field[@modelName != $intrinsicTransforms/name])[$partnerID != $invalidArgumentID] |
      (current()[$partnerID = $invalidArgumentID])"/> 

    <!--
    <xsl:message terminate="no">
      <xsl:text>Count of partnerFields is: </xsl:text>
      <xsl:value-of select="count($partnerFields)"/>
    </xsl:message>
    -->

    <xsl:for-each select="$partnerFields">
      <xsl:variable name="partnerField" select="current()"/>
      
      <!--
      <xsl:message terminate="no">
        <xsl:text>Current partnerField is: </xsl:text>
        <xsl:value-of select="$partnerField/@modelName"/>
        <xsl:value-of select="$partnerField/@epName"/>
        <xsl:value-of select="$partnerField/@instance"/>
        <xsl:value-of select="$partnerField/@id"/>
      </xsl:message>
      -->

      <!-- We now have the field that the field of the given entry point is coupled with.
      Now get the entry point argument tuple in DS.xml matching our partnerField,
      remembering to check that the tuple has the correct "direction" and that the 
      tuple doesn't belong to the same SU. --> 
      <xsl:variable name="corresDirection">
        <xsl:choose>
          <xsl:when test="$direction = 'get'">
            <xsl:value-of select="'put'"/>
          </xsl:when>
          <xsl:when test="$direction = 'put'">
            <xsl:value-of select="'get'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
              <xsl:text>): Direction can only be 'put' or 'get'.</xsl:text>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="entryPointArgCorres" select="$commFormsRoot/communicationForms/
        tuple[@modelName = $partnerField/@modelName
          and @epName = $partnerField/@epName
          and (not(boolean($partnerField/@instance)) or @instance=$partnerField/@instance)
          and @id = $partnerField/@id
          and contains(@form,$corresDirection)
          and @su != $entryPointArg/@su]"/>
      <xsl:if test="count($entryPointArgCorres) &gt; 1">
        <xsl:message terminate="yes">
          <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
          <xsl:text>): More than one tuple found in DS.xml for the given entry point argument instance!</xsl:text>
        </xsl:message>
      </xsl:if>

      <!--
      <xsl:message terminate="no">
        <xsl:text>Count of entryPointArgCorres is: </xsl:text>
        <xsl:value-of select="count($entryPointArgCorres)"/>
      </xsl:message>

      <xsl:message terminate="no">
        <xsl:text>entryPointArgCorres is: </xsl:text>
        <xsl:value-of select="$entryPointArgCorres/@modelName"/>
        <xsl:value-of select="$entryPointArgCorres/@epName"/>
        <xsl:value-of select="$entryPointArgCorres/@instance"/>
        <xsl:value-of select="$entryPointArgCorres/@id"/>
      </xsl:message>
      -->

      <xsl:variable name="gridType">
        <xsl:call-template name="getCouplingFieldGridType">
          <xsl:with-param name="couplingField" select="$couplingField"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:if test="$gridType = ''">
        <xsl:message terminate="yes">
          <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
          <xsl:text>): processCorresFields: coupling field missing grid.</xsl:text>
        </xsl:message>
      </xsl:if>

      <xsl:if test="count($entryPointArgCorres) = 1">
        <xsl:variable name="entryPointArgCorresBFGID">
          <xsl:call-template name="getCouplingFieldBFGID">
            <xsl:with-param name="modelName" select="$entryPointArgCorres/@modelName"/>
            <xsl:with-param name="instanceID" select="$entryPointArgCorres/@instance"/>
            <xsl:with-param name="entryPointName" select="$entryPointArgCorres/@epName"/>
            <xsl:with-param name="argumentID" select="$entryPointArgCorres/@id"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="transientName">
          <xsl:call-template name="getTransientName">
            <xsl:with-param name="myBFGID" select="$couplingFieldBFGID"/>
            <xsl:with-param name="remoteBFGID" select="$entryPointArgCorresBFGID"/>
            <xsl:with-param name="remoteSU" select="$entryPointArgCorres/@su"/>
            <xsl:with-param name="direction" select="$direction"/>
          </xsl:call-template>
        </xsl:variable>

        <!-- START CALLER DEPENDENT CODE -->
        <xsl:choose>
          <xsl:when test="$processType = 'initTransients'">
            <!-- Create the PSMILe initialisation call for this transient. -->
            <xsl:call-template name="initTransient">
              <xsl:with-param name="direction" select="$direction"/>
              <xsl:with-param name="corresDirection" select="$corresDirection"/>
              <xsl:with-param name="transientName" select="$transientName"/>
              <xsl:with-param name="entryPointOfThisCouplingField" select="$entryPointArg"/>
              <xsl:with-param name="entryPointOfRemoteCouplingField" select="$entryPointArgCorres"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="$processType = 'smiocTransients'">
            <!-- Fill in the SMIOC coupling field xml for this transient. -->
            <xsl:call-template name="smiocTransient">
              <xsl:with-param name="direction" select="$direction"/>
              <xsl:with-param name="transientName" select="$transientName"/>
              <xsl:with-param name="couplingField" select="$couplingField"/>
              <xsl:with-param name="couplingFieldBFGID" select="$couplingFieldBFGID"/>
              <xsl:with-param name="entryPointOfThisCouplingField" select="$entryPointArg"/>
              <xsl:with-param name="entryPointOfRemoteCouplingField" select="$entryPointArgCorres"/>
              <xsl:with-param name="entryPointOfRemoteCouplingFieldBFGID" select="$entryPointArgCorresBFGID"/>
              <xsl:with-param name="gridType" select="$gridType"/>
              <xsl:with-param name="transformName" select="$intrinsicTransformName"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">
              <xsl:text>Error (</xsl:text><xsl:value-of select="$thisFileName"/>
              <xsl:text>): Unrecognised processType for processing corresponding coupling fields</xsl:text>
            </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
        <!-- END CALLER DEPENDENT CODE -->
      </xsl:if>

    </xsl:for-each><!-- partnerFields -->

  </xsl:for-each><!-- corresFields -->

  <!--
  <xsl:message terminate="no">
    <xsl:text>*** END of processCorresFields, processing: </xsl:text>
    <xsl:value-of select="$processType"/>
    <xsl:text> ***</xsl:text>
  </xsl:message>
  -->

</xsl:template>


<xsl:template name="writePointsToKML">
  <xsl:param name="pointsName"/>
  <xsl:param name="longitudesCount"/>
  <xsl:param name="latitudesCount"/>

  <!-- IRH: WKML
     ! File handle, filename, unit number (-1 = library will assign), replace existing, doc name
     call kmlBeginFile(kf, trim(points%name)//".kml", -1, .true., trim(points%name))
     do lat_loop=1,72
       do lon_loop=1,72
         call kmlCreatePoints(kf, &
                points%coords%longitudes(lon_loop), &
                points%coords%latitudes(lat_loop) )
       end do
     end do
     call kmlFinishFile(kf)
  -->
 <!-- Get application and component names -->
 <xsl:variable name="applName">
  <xsl:call-template name="createAppName">
   <xsl:with-param name="deploymentUnit" select="ancestor::deploymentUnit"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:variable name="compName">
  <xsl:call-template name="createCompName">
   <xsl:with-param name="sequenceUnit" select="."/>
  </xsl:call-template>
 </xsl:variable>

  <comment>File handle, filename, unit (-1 = library will assign), replace, doc name</comment>
  <call name="kmlBeginFile">
    <arg name="kml_file_handle"/>
    <arg value="{$pointsName}.kml" type="string-literal"/>
    <arg value="-1"/>
    <arg value=".true."/>
    <arg value="{$pointsName}" type="string-literal"/>
  </call>

  <iterate start="1" end="{$longitudesCount}" counter="lon_loop">
    <iterate start="1" end="{$latitudesCount}" counter="lat_loop">
      <call name="kmlCreatePoints">
        <arg name="kml_file_handle"/>
        <arg name="points%coords%longitudes(lon_loop)"/>
        <arg name="points%coords%latitudes(lat_loop)"/>
      </call>
    </iterate>
  </iterate>

  <call name="kmlFinishFile">
    <arg name="kml_file_handle"/>
  </call>

</xsl:template>


</xsl:stylesheet>
