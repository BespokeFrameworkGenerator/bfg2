<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template name="sequentialInitComms">
    <Includes/>
    <DataDecs/>
    <Content>
     <comment>nothing needed for sequential</comment>
    </Content>
</xsl:template>

<xsl:template name="sequentialFinaliseComms">
    <Includes/>
    <DataDecs/>
    <Content>
     <comment>nothing needed for sequential</comment>
    </Content>
</xsl:template>

<xsl:template name="sequentialIncludeTarget">
   <comment>running in sequence so no includes required</comment>
</xsl:template>

<xsl:template name="sequentialDeclareSendRecvVars">
   <comment>running in sequence so no send or receives required</comment>
</xsl:template>

<xsl:template name="sequentialCommsSync">
   <comment>running in sequence so no sync is required</comment>
</xsl:template>

</xsl:stylesheet>
