<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">
-->

<!--
    RF 19th Sept 2005
    Phase 2 of creating code. Input is an xml code template,
    output is a more specific xml code template.

    Phase 2 responsibility is
    1: add in all model arguments that will be involved in argument
    passing. This is a subset of all arguments as some will be
    wrapped to put/get comms.

    In this phase we
    1: (Args.xsl) add in the arguments and their id's

Note, the MatchAll.xsl is used to output any
remaining unchanged template xml

-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="Args.xsl"/>

<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
