<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 28th Sept 2005
    Sorting out argument coupling and initialisation

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.
-->

<!--<xsl:include href="GenVarIDs.xsl"/>-->
<xsl:include href="GenModelEPid.xsl"/>
<!--<xsl:include href="ArgPassingTests.xsl"/>-->
<xsl:include href="../../Utils/Loops.xsl"/>
<xsl:include href="../../Utils/DummyVars.xsl"/>


<xsl:template match="call">
   <!-- add required arguments to the call -->
   <xsl:variable name="modelName" select="@modelName"/>
   <xsl:variable name="epName" select="@epName"/>
   <xsl:variable name="activate" select="@activate"/>
   <xsl:variable name="instance" select="@instance"/>
   <!-- loop over all argument passing vars for this entrypoint -->
   <xsl:for-each select="document($root/coupled/models/model)
                 /definition[name=$modelName]/entryPoints[1]/entryPoint[@name=$epName]/data[@form='argpass']">
    <!-- order as they should be in the argument list as they may not be specified
         in the required order in the xml document -->
    <xsl:sort select="@id" data-type="number"/>
    <xsl:variable name="myID" select="@id"/>
    <xsl:if test="$commFormsRoot/communicationForms
            /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not($instance) or @instance=$instance) and contains(@form,'get')]">
     <xsl:variable name="uniqueTag" select="$commFormsRoot/communicationForms
                   /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not($instance) or @instance=$instance)]/@ref"/>

<!--CWA START added 12/09/07-->

     <!--
     <xsl:message terminate="no">
      <xsl:text> my model: </xsl:text><xsl:value-of select="$modelName"/>
      <xsl:text>, my ep: </xsl:text><xsl:value-of select="$epName"/>
      <xsl:text>, my ID: </xsl:text><xsl:value-of select="$myID"/>
      <xsl:text>, my instance: </xsl:text><xsl:value-of select="$instance"/>
     </xsl:message>
     -->

     <!--find the set in the composition of which this field is a member-->
     <xsl:variable name="mySet" select="document($root/coupled/composition)/composition//set[field
                                        [@modelName=$modelName and @epName=$epName and @id=$myID and (not(@instance) or @instance=$instance)]]"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text>my set: </xsl:text><xsl:value-of select="$mySet/@name"/>
     </xsl:message>
     -->

     <!--find out whether any of the fields in this set have assumed sizes-->
     <xsl:variable name="isAssumedAll">
      <xsl:for-each select="$mySet/field">
       <xsl:variable name="setMod" select="@modelName"/>
       <xsl:variable name="setEP" select="@epName"/>
       <xsl:variable name="setid" select="@id"/>
       <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod and language!='intrinsic']//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
        <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text>is assumed all </xsl:text><xsl:value-of select="$isAssumedAll"/>
      <xsl:text>is assumed </xsl:text><xsl:value-of select="$isAssumed"/>
     </xsl:message>
     -->

     <!--if an assumed size is in the set, then always receive the size of each dimension of the array-->
     <xsl:if test="$isAssumed!=''">
      <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
      <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
      <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text> assumed model: </xsl:text><xsl:value-of select="$assumedMod"/>
      <xsl:text>, assumed ep: </xsl:text><xsl:value-of select="$assumedEP"/>
      <xsl:text>, assumed ID: </xsl:text><xsl:value-of select="$assumedid"/>
     </xsl:message>
     -->

      <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]/entryPoint[@name=$assumedEP]/data[@id=$assumedid]//assumed">
       <call epName="{$epName}" modelName="{$modelName}" name="{concat('get_',$modelName)}" activate="{$activate}" instance="{$instance}">
        <xsl:variable name="dummyRef">
         <xsl:call-template name="getAssumedDummyRef">
          <xsl:with-param name="assumed" select="."/>
         </xsl:call-template>
        </xsl:variable>
        <arg name="i{$dummyRef + $maxTag}"/><!--add maxTag so that we don't clash with put/get tags used for passing arguments-->
        <arg name="-{$dummyRef + $maxTag}"/>
       </call>
      </xsl:for-each>
     </xsl:if>

<!--CWA END added 12/09/07-->

     <call epName="{$epName}" modelName="{$modelName}" name="{concat('get_',$modelName)}" activate="{$activate}"  instance="{$instance}">
      <arg id="{$myID}"/>
      <arg name="-{$uniqueTag}"/>
     </call>
    </xsl:if>
   </xsl:for-each>
   <!-- rewrite the call template -->
   <call epName="{$epName}" modelName="{$modelName}" name="{@name}" activate="{$activate}" instance="{$instance}">
   <xsl:apply-templates/> <!-- match any args for other types of calls -->
    <!-- loop over all argument passing vars for this entrypoint -->
      <xsl:for-each select="document($root/coupled/models/model)
                    /definition/entryPoints[1]/entryPoint/data
                    [ancestor::definition/name=$modelName and 
                     ancestor::entryPoint/@name=$epName and @form='argpass']">
       <!-- order as they should be in the argument list as they may not be specified
            in the required order in the xml document -->
       <xsl:sort select="@id" data-type="number"/>
       <xsl:variable name="myID" select="@id"/>
       <arg id="{$myID}"/>
      </xsl:for-each>
   </call>

   <!-- loop over all argument passing vars for this entrypoint -->
   <xsl:for-each select="document($root/coupled/models/model)
                 /definition/entryPoints[1]/entryPoint/data
                 [ancestor::definition/name=$modelName and 
                  ancestor::entryPoint/@name=$epName and @form='argpass']">
    <!-- order as they should be in the argument list as they may not be specified
         in the required order in the xml document -->
    <xsl:sort select="@id" data-type="number"/>
    <xsl:variable name="myID" select="@id"/>
    <xsl:if test="$commFormsRoot/communicationForms
            /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not($instance) or @instance=$instance) and contains(@form,'put')]">
     <xsl:variable name="uniqueTag" select="$commFormsRoot/communicationForms
                   /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not($instance) or @instance=$instance)]/@ref"/>
<!--CWA START added 12/09/07-->

     <!--
     <xsl:message terminate="no">
      <xsl:text> my model: </xsl:text><xsl:value-of select="$modelName"/>
      <xsl:text>, my ep: </xsl:text><xsl:value-of select="$epName"/>
      <xsl:text>, my ID: </xsl:text><xsl:value-of select="$myID"/>
      <xsl:text>, my instance: </xsl:text><xsl:value-of select="$instance"/>
     </xsl:message>
     -->

     <!--find the set in the composition of which this field is a member-->
     <xsl:variable name="mySet" select="document($root/coupled/composition)/composition//set[field
                                        [@modelName=$modelName and @epName=$epName and @id=$myID and (not(@instance) or @instance=$instance)]]"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text>my set: </xsl:text><xsl:value-of select="$mySet/@name"/>
     </xsl:message>
     -->

     <!--find out whether any of the fields in this set have assumed sizes-->
     <xsl:variable name="isAssumedAll">
      <xsl:for-each select="$mySet/field">
       <xsl:variable name="setMod" select="@modelName"/>
       <xsl:variable name="setEP" select="@epName"/>
       <xsl:variable name="setid" select="@id"/>
       <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod and language!='intrinsic']//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
        <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text>is assumed all </xsl:text><xsl:value-of select="$isAssumedAll"/>
      <xsl:text>is assumed </xsl:text><xsl:value-of select="$isAssumed"/>
     </xsl:message>
     -->

     <!--if an assumed size is in the set, then always send the size of each dimension of the array-->
     <xsl:if test="$isAssumed!=''">
      <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
      <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
      <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>

     <!--
     <xsl:message terminate="no">
      <xsl:text> assumed model: </xsl:text><xsl:value-of select="$assumedMod"/>
      <xsl:text>, assumed ep: </xsl:text><xsl:value-of select="$assumedEP"/>
      <xsl:text>, assumed ID: </xsl:text><xsl:value-of select="$assumedid"/>
     </xsl:message>
     -->

      <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]/entryPoint[@name=$assumedEP]/data[@id=$assumedid]//assumed">
       <xsl:variable name="index">
        <xsl:call-template name="getIndexPos">
         <xsl:with-param name="assumed" select="."/>
         <xsl:with-param name="data" select="ancestor::data"/>
        </xsl:call-template>
       </xsl:variable>
       <xsl:variable name="dummyRef">
        <xsl:call-template name="getAssumedDummyRef">
         <xsl:with-param name="assumed" select="."/>
        </xsl:call-template>
       </xsl:variable>
       <xsl:if test="$activate = 'true'">
        <assign lhs="i{$dummyRef + $maxTag}" rhs="size(r{$uniqueTag},{$index})"/>
       </xsl:if>
       <call epName="{$epName}" modelName="{$modelName}" name="{concat('put_',$modelName)}" activate="{$activate}" instance="{$instance}">
        <arg name="i{$dummyRef + $maxTag}"/><!--add maxTag so that we don't clash with put/get tags used for passing arguments-->
        <arg name="-{$dummyRef + $maxTag}"/>
       </call>
      </xsl:for-each>
     </xsl:if>

<!--CWA END added 12/09/07-->

     <call epName="{$epName}" modelName="{$modelName}" name="{concat('put_',$modelName)}" activate="{$activate}" instance="{$instance}">
      <arg id="{$myID}"/>
      <arg name="-{$uniqueTag}"/>
     </call>
    </xsl:if>
   </xsl:for-each>
 <!--
  </xsl:otherwise>
 </xsl:choose>
 -->
</xsl:template>

<!--CWA START added 12/09/07-->


<xsl:variable name="maxTag">
 <xsl:for-each select="$commFormsRoot/communicationForms/*">
  <xsl:sort select="@ref" order="descending"/>
  <xsl:if test="position()=1">
   <xsl:value-of select="@ref"/>
  </xsl:if>
 </xsl:for-each>
</xsl:variable>

<xsl:template name="getIndexPos">
 <xsl:param name="assumed"/>
 <xsl:param name="data"/>
 <xsl:for-each select="$data/dim[upper or size]">
  <xsl:sort select="@value" order="ascending"/>
  <xsl:if test="generate-id(.//assumed) = generate-id($assumed)">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<!--CWA END added 12/09/07-->

</xsl:stylesheet>
