<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template name="getModelEPid">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:for-each select="document($root/coupled/deployment)
               /deployment/schedule//model">
  <xsl:if test="@name=$modelName and @ep=$epName">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
