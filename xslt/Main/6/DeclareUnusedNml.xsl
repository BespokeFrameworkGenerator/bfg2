<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template match="decnamelist">
 <xsl:if test="@name!='time' and @fileref!=''">
  <xsl:for-each select="nmldata">
   <xsl:variable name="argref" select="@ref"/>
    <xsl:if test="not(../../declare[@ref=$argref])">
     <xsl:variable name="myname" select="@fileref"/>
     <xsl:choose>
      <xsl:when test="document($root/coupled/composition)
                /composition/connections/priming//file[@dataRef=$myname]">
       <xsl:variable name="modelName" select="document($root/coupled/composition)
                     /composition/connections/priming//file[@dataRef=$myname]/../../../@name"/>
       <xsl:variable name="epName" select="document($root/coupled/composition)
                     /composition/connections/priming//file[@dataRef=$myname]/../../@name"/>
       <xsl:variable name="id" select="document($root/coupled/composition)
                     /composition/connections/priming//file[@dataRef=$myname]/../@id"/>
       <xsl:variable name="mytype" select="document($root/coupled/models/model)/definition[normalize-space(./name)=$modelName]
                     /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dataType"/>
       <xsl:variable name="mydim" select="document($root/coupled/models/model)/definition[normalize-space(name)=$modelName]
                     /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dimension"/>
       <declare ref="{$argref}" datatype="{$mytype}" dimension="{$mydim}">
        <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]
                           /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/dim">
         <xsl:for-each select="lower">
          <dim order="{2 * number(../@value) - 1}">
           <lower>
            <xsl:apply-templates select="*">
             <xsl:with-param name="modelName" select="$modelName"/>
             <xsl:with-param name="epName" select="$epName"/>
             <xsl:with-param name="id" select="$id"/>
            </xsl:apply-templates>
           </lower>
          </dim>
         </xsl:for-each>
         <xsl:for-each select="upper">
          <dim order="{2 * number(../@value)}">
           <upper>
            <xsl:apply-templates select="*">
             <xsl:with-param name="modelName" select="$modelName"/>
             <xsl:with-param name="epName" select="$epName"/>
             <xsl:with-param name="id" select="$id"/>
            </xsl:apply-templates>
           </upper>
          </dim>
         </xsl:for-each>
        </xsl:for-each>
       </declare>
      </xsl:when>
      <xsl:otherwise>
       <xsl:variable name="modelName" select="document($root/coupled/composition)
                     /composition/connections/timestepping/set/primed/file[@dataRef=$myname]/../../field[1]/@modelName"/>
       <xsl:variable name="epName" select="document($root/coupled/composition)
                     /composition/connections/timestepping/set/primed/file[@dataRef=$myname]/../../field[1]/@epName"/>
       <xsl:variable name="id" select="document($root/coupled/composition)
                     /composition/connections/timestepping/set/primed//file[@dataRef=$myname]/../../field[1]/@id"/>
       <xsl:variable name="mytype" select="document($root/coupled/models/model)/definition[normalize-space(./name)=$modelName]
                     /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dataType"/>
       <xsl:variable name="mydim" select="document($root/coupled/models/model)/definition[normalize-space(name)=$modelName]
                     /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@dimension"/>
       <declare ref="{$argref}" datatype="{$mytype}" dimension="{$mydim}">
        <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]
                           /entryPoints/entryPoint[@name=$epName]/data[@id=$id]/dim">
         <xsl:for-each select="lower">
          <dim order="{2 * number(../@value) - 1}">
           <lower>
            <xsl:apply-templates select="*">
             <xsl:with-param name="modelName" select="$modelName"/>
             <xsl:with-param name="epName" select="$epName"/>
             <xsl:with-param name="id" select="$id"/>
            </xsl:apply-templates>
           </lower>
          </dim>
         </xsl:for-each>
         <xsl:for-each select="upper">
          <dim order="{2 * number(../@value)}">
           <upper>
            <xsl:apply-templates select="*">
             <xsl:with-param name="modelName" select="$modelName"/>
             <xsl:with-param name="epName" select="$epName"/>
             <xsl:with-param name="id" select="$id"/>
            </xsl:apply-templates>
           </upper>
          </dim>
         </xsl:for-each>
        </xsl:for-each>
       </declare>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:if>
  </xsl:for-each>
 </xsl:if>
 <xsl:if test="nmldata">
  <xsl:copy>
   <xsl:copy-of select="@*" />
   <xsl:copy-of select="normalize-space(text())" />
   <xsl:apply-templates select="*"/>
  </xsl:copy>
 </xsl:if>
</xsl:template>

<xsl:template match="file">
 <xsl:if test="namelist/data or namelist/@name='time'">
  <xsl:for-each select="namelist/data[@used='true']">
   <xsl:if test="ancestor::code//declare[./@ref=current()/@ref]//variable">
    <if>
     <condition>
      <not>
       <value>allocated(r<xsl:value-of select="@ref"/>)</value>
      </not>
     </condition>
     <action>
      <allocate name="r{@ref}">
       <xsl:for-each select="ancestor::code//declare[./@ref=current()/@ref]/dim">
        <xsl:copy>
         <xsl:copy-of select="@*" />
         <xsl:copy-of select="normalize-space(text())" />
         <xsl:apply-templates select="*"/>
        </xsl:copy>
       </xsl:for-each>
      </allocate>
     </action>
    </if>
   </xsl:if>
  </xsl:for-each>
  <xsl:copy>
   <xsl:copy-of select="@*" />
   <xsl:copy-of select="normalize-space(text())" />
   <xsl:apply-templates select="*"/>
  </xsl:copy>
 </xsl:if>
</xsl:template>

</xsl:stylesheet>
