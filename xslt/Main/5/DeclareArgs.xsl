<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 30th Sept 2005
    Sorting out argument coupling and initialisation

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.
-->

<xsl:param name="debug" select="'false'"/>

          <xsl:key name="nml-files" match="primed/file" use="@name"/>
          <xsl:key name="nml-files" match="primed/file" use="@name"/>
          <xsl:key name="namelists" match="primed/file" use="@nlName"/>
          <xsl:key name="dataRefs" match="primed/file" use="@dataRef"/>

<xsl:include href="FindFirstRef.xsl"/>

<xsl:template match="Includes">
 <Includes>
  <!-- only include netcdf if we read at least one netcdf file -->
  <xsl:if test="count(document($root/coupled/composition)
          /composition/connections/priming/model/
          entryPoint/primed/file[@type='netcdf'])>0">
   <include name="netcdf"/>
  </xsl:if>
  <xsl:apply-templates select="*"/>
 </Includes>
</xsl:template>

<xsl:key name="extraVars" match="call/arg" use="@name"/>

<xsl:template match="DataDecs">
 <xsl:variable name="MainProgID" select="ancestor::code/@id"/>
 <DataDecs>
  <xsl:apply-templates select="*"/>
  <comment>Declare size ref variables (for assumed arrays)</comment>
  <xsl:for-each select="//Content//arg[starts-with(@name,'i')]">
   <xsl:if test="generate-id(key('extraVars',@name)[1])=generate-id(.)">
    <declare datatype="integer" ref="{@name}"/>
   </xsl:if>
  </xsl:for-each>
  <comment>Begin declaration of arguments</comment>
  <comment>Point to Point and Uncoupled Vars</comment>
  <!--Below won't match p2p, but why do they need declaring in main code? Leave as is.-->
  <xsl:for-each select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data[@form='argpass']">
   <xsl:variable name="modelName" select="ancestor::definition/name"/>
   <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
   <xsl:variable name="id" select="@id"/>
   <xsl:variable name="dimension" select="@dimension"/>
   <xsl:variable name="vdim" select="dim"/>

   <!-- RF added string support -->
   <xsl:variable name="stringSize" select="stringSize"/>

   <!--CWA: Here, we're assuming that each instance of the model has arguments of the same type (i.e. no polymorphism)-->
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@name=$modelName and @ep=$epName]">
    <xsl:variable name="instance" select="@instance"/>
    <!-- only consider the variable if it's parent model is on my deployment unit-->
    <xsl:variable name="ModelDUID">
     <xsl:for-each select="document($root/coupled/deployment)//deploymentUnit">
       <xsl:if test="sequenceUnit/model[@name=$modelName and (not($instance) or @instance=$instance)]">
         <xsl:value-of select="position()"/>
       </xsl:if>
     </xsl:for-each>
    </xsl:variable>
    <xsl:if test="$ModelDUID=$MainProgID">
     <!-- if this variable is not used in set notation then use this logic -->
     <xsl:if test="not(document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$modelName and @epName=$epName
                   and @id=$id and (not(@instance) or @instance=$instance)])">
      <debug author="rupert" info="The following arg passing variable uses point to point notation"/>
      <xsl:variable name="datatype">
       <xsl:call-template name="datatype">
        <xsl:with-param name="modelName" select="$modelName"/>
        <xsl:with-param name="epName" select="$epName"/>
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="root" select="$root"/>
       </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="ref" select="$commFormsRoot/communicationForms
                  /*[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/@ref"/>
      <xsl:variable name="varname" select="$commFormsRoot/communicationForms
                  /*[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/@name"/>
      <xsl:variable name="kind" select="document($root/coupled/models/model)/definition[name=$modelName]/entryPoints/entryPoint[@name=$epName]/data[@id=$id]/@kind"/>
      <HELLO1/>
      <declare datatype="{$datatype}" ref="{$ref}" dimension="{$dimension}" name="{$varname}" modelName="{$modelName}" epName="{$epName}" id="{$id}" instance="{$instance}" kind="{$kind}">

       <!-- RF adding support for strings -->
        <!-- manipulate stringsize declaration so that it looks like a
        dimension (i.e. add upper element). This allows us to use the
        dimension template at the codegen phase. -->
        <xsl:if test="$stringSize">
          <stringSize>
            <upper>
              <xsl:apply-templates select="$stringSize/*">
                <xsl:with-param name="modelName" select="$modelName"/>
                <xsl:with-param name="epName" select="$epName"/>
                <xsl:with-param name="id" select="$id"/>
                <xsl:with-param name="instance" select="$instance"/>
              </xsl:apply-templates>
            </upper>
          </stringSize>
        </xsl:if>
       <!-- RF end addition of support for strings -->

       <xsl:if test="$vdim//argument">
        <firstRef model="{$modelName}" entryPoint="{$epName}" instance="{$instance}"/>
       </xsl:if>
       <xsl:for-each select="$vdim">
        <xsl:for-each select="lower">
         <dim order="{2 * number(../@value) - 1}">
          <lower>
           <xsl:apply-templates select="*">
            <xsl:with-param name="modelName" select="$modelName"/>
            <xsl:with-param name="epName" select="$epName"/>
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="instance" select="$instance"/>
           </xsl:apply-templates>
          </lower>
         </dim>
        </xsl:for-each>
        <xsl:for-each select="upper">
         <dim order="{2 * number(../@value)}">
          <upper>
           <xsl:apply-templates select="*">
            <xsl:with-param name="modelName" select="$modelName"/>
            <xsl:with-param name="epName" select="$epName"/>
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="instance" select="$instance"/>
           </xsl:apply-templates>
          </upper>
         </dim>
        </xsl:for-each>
       </xsl:for-each>
      </declare>
     </xsl:if>
    </xsl:if>
   </xsl:for-each>
  </xsl:for-each>
  <comment>Set Notation Vars</comment>
  <!-- declare a variable for each set using a name derived from the first field declared in that set -->
  <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/set">
   <xsl:variable name="thisSet" select="."/>
   <xsl:variable name="declareSetVar">
    <xsl:for-each select="field">
     <xsl:variable name="instance" select="@instance"/>
     <xsl:variable name="modelName" select="@modelName"/>
     <xsl:variable name="ModelDUID">
      <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
       <xsl:if test="sequenceUnit/model[@name=$modelName and (not($instance) or @instance=$instance)]">
        <xsl:value-of select="position()"/>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="srcOnMyDU" select="boolean($ModelDUID=$MainProgID)"/>
     <xsl:if test="$srcOnMyDU">
       <xsl:text>Y</xsl:text>
     </xsl:if>
    </xsl:for-each>
   </xsl:variable>
   <xsl:if test="$declareSetVar!=''">
    <xsl:variable name="fieldsWithoutAssumed">
     <xsl:for-each select="field">
      <xsl:if test="document($root/coupled/models/model)/definition[name=current()/@modelName]/entryPoints/entryPoint[@name=current()/@epName]/data[@id=current()/@id and not(.//assumed)]">
       <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
      </xsl:if>
     </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="fieldWithoutAssumed">
     <xsl:value-of select="substring-before($fieldsWithoutAssumed,':')"/>
    </xsl:variable>


    <xsl:variable name="modelName" select="field[number($fieldWithoutAssumed)]/@modelName"/>
    <xsl:variable name="epName" select="field[number($fieldWithoutAssumed)]/@epName"/>
    <xsl:variable name="id" select="field[number($fieldWithoutAssumed)]/@id"/>
    <xsl:variable name="instance" select="field[number($fieldWithoutAssumed)]/@instance"/>

    <xsl:variable name="ref" select="$commFormsRoot/communicationForms
                /*[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/@ref"/>
    <xsl:variable name="varname" select="$commFormsRoot/communicationForms
                /*[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/@name"/>


    <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modelName]/entryPoints/entryPoint[@name=$epName]/data[@form='argpass' and @id=$id]">
     <DEBUG position="HELLO2" modelName="{$modelName}" epName="{$epName}" id="{$id}" />
     <declare datatype="{@dataType}" ref="{$ref}" dimension="{@dimension}" name="{$varname}" kind="{@kind}">
      <xsl:if test="dim//argument">
       <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
        <xsl:variable name="SUpos" select="position()"/>
        <xsl:call-template name="findFirstRef">
         <xsl:with-param name="set" select="$thisSet"/>
         <xsl:with-param name="SU" select="."/>
         <xsl:with-param name="SUpos" select="$SUpos"/>
        </xsl:call-template>
       </xsl:for-each>
      </xsl:if>

      <!-- 
      CWA: Removed the following because regridding should be handled as 
      per other transformations.

      IRH: No longer declaring the dimensions explicitly here - dimensions
      for the field may be of different sizes for models running in different
      sequence units, for instance where a field is regridded by OASIS4 between
      models.  We assume the field name, data type, and number of dimensions is
      the same across models (so we arbitrarily use field[1] above) but we
      cannot assume that for the size of the dimensions because of the
      possibility of regridding. The SU-dependent sizes are therefore now set
      in the findFirstRef template above, where they are placed inside the
      <firstRef> elements which represent the first use of the field *within*
      each sequence unit.  The dimensions are then copied from the <firstRef>
      elements to the point in the code where the array is allocated (for each
      sequence unit) by the "call" template in Main/7/Allocate.xsl.-->
      <xsl:for-each select="dim">
       <xsl:for-each select="lower">
        <dim order="{2 * number(../@value)-1}">
         <lower>
           <xsl:apply-templates select="*">
            <xsl:with-param name="modelName" select="$modelName"/>
            <xsl:with-param name="epName" select="$epName"/>
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="instance" select="$instance"/>
           </xsl:apply-templates>
         </lower>
        </dim>
       </xsl:for-each>
       <xsl:for-each select="upper">
        <dim order="{2 * number(../@value)}">
         <upper>
           <xsl:apply-templates select="*">
            <xsl:with-param name="modelName" select="$modelName"/>
            <xsl:with-param name="epName" select="$epName"/>
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="instance" select="$instance"/>
           </xsl:apply-templates>
         </upper>
        </dim>
       </xsl:for-each>
      </xsl:for-each>
     </declare>
     <xsl:value-of select="$newline"/>
    </xsl:for-each>
   </xsl:if>
  </xsl:for-each>

  <!--CWA: Multi-instance support has not been added here-->
  <comment>p2p between set notation vars</comment>
  <!-- There is a special case of p2p connections between data within set notation. In this case we need a temporary variable -->

  <!-- search all point to point connections -->
  <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection">

    <!-- is the source within a set? (also implies it must be argpassing so no need to explicitly check) -->
    <xsl:variable name="modelName" select="ancestor::modelChannel/@outModel"/>
    <xsl:variable name="epName" select="ancestor::epChannel/@outEP"/>
    <xsl:variable name="id" select="@outID"/>
    <xsl:variable name="srcInSet" select="boolean(document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$modelName and @epName=$epName and @id=$id])"/>

    <!-- is the sink within a set? (also implies it must be argpassing so no need to explicitly check) -->
    <xsl:variable name="modelName2" select="ancestor::modelChannel/@inModel"/>
    <xsl:variable name="epName2" select="ancestor::epChannel/@inEP"/>
    <xsl:variable name="id2" select="@inID"/>
    <xsl:variable name="sinkInSet" select="boolean(document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$modelName2 and @epName=$epName2 and @id=$id2])"/>

    <!-- is source on my DU? -->
    <xsl:variable name="ModelDUID">
      <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
        <xsl:if test="sequenceUnit/model/@name=$modelName">
          <xsl:value-of select="position()"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="srcOnMyDU" select="boolean($ModelDUID=$MainProgID)"/>

    <!-- is sink on my DU? -->
    <xsl:variable name="ModelDUID2">
      <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
        <xsl:if test="sequenceUnit/model/@name=$modelName2">
          <xsl:value-of select="position()"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="sinkOnMyDU" select="boolean($ModelDUID2=$MainProgID)"/>

    <!-- if both source and sink are in a set and both source and sink are on my DU then declare a temporary var -->
    <xsl:if test="$srcInSet and $sinkInSet and $srcOnMyDU and $sinkOnMyDU">
      <xsl:message terminate="yes"><xsl:text>Congratulations, you are the first to hit the temporary variable problem. Unfortunately for you, I've not bothered to add the temporary variable codegen yet so your code will not run - oops.</xsl:text></xsl:message>
    </xsl:if>
  </xsl:for-each>

  <comment>End declaration of arguments</comment>

  <comment>Begin declaration of namelist input</comment>
  <xsl:variable name="myDU" select="ancestor::code/@id"/>

          <xsl:for-each select="document($root/coupled/composition)
				   /composition//primed/file[@type='namelist']">
           <!--if this is the first namelist with this name-->
           <xsl:if test="generate-id(.)=generate-id(key('namelists',@nlName)[1])">
            <xsl:variable name="mynml" select="@nlName"/>
            <xsl:variable name="used">
             <xsl:apply-templates select="document($root/coupled/composition)
				   /composition//primed/file[@type='namelist' and @nlName=$mynml]" mode="used">
              <xsl:with-param name="myDU" select="$myDU"/>
             </xsl:apply-templates>
            </xsl:variable>
            <!--if this namelist contains a var that is referenced in the current DU-->
            <xsl:if test="contains($used,'true')">
                <decnamelist name="{@nlName}">
                 <xsl:for-each select="document($root/coupled/composition)
				      /composition//primed/file[@type='namelist' and @nlName=$mynml]">
                  <xsl:if test="generate-id(.)=generate-id(key('dataRefs',@dataRef)[1])">
                   <xsl:choose>
                    <xsl:when test="ancestor::model">
                     <xsl:variable name="modelName" select="ancestor::model/@name"/>
                     <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
                     <xsl:variable name="id" select="../@id"/>
                     <xsl:variable name="instance" select="../../@instance"/>
                     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                        /*[@modelName=$modelName and @epName=$epName and @id=$id and
                                                        (not($instance) or @instance=$instance)]/@ref"/>
                     <nmldata ref="{$argref}" fileref="{@dataRef}"/>
                    </xsl:when>
                    <xsl:when test="ancestor::set">
                     <xsl:variable name="modelName" select="../../field[1]/@modelName"/>
                     <xsl:variable name="epName" select="../../field[1]/@epName"/>
                     <xsl:variable name="id" select="../../field[1]/@id"/>
                     <xsl:variable name="instance" select="../../field[1]/@instance"/>
                     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                        /*[@modelName=$modelName and @epName=$epName and @id=$id and 
                                                        (not($instance) or @instance=$instance)]/@ref"/>
                     <nmldata ref="{$argref}" fileref="{@dataRef}"/>
                    </xsl:when>
                   </xsl:choose>
                  </xsl:if>
                 </xsl:for-each>
                </decnamelist>
            </xsl:if>
           </xsl:if>
          </xsl:for-each>
  <comment>End declaration of namelist input</comment>
  <xsl:if test="$stateDump = 'true'">
   <comment>Begin declaration of namelist output</comment>
   <xsl:for-each select="document($root/coupled/deployment)/deployment//schedule//model">
    <xsl:variable name="modName" select="@name"/>
    <xsl:variable name="epName" select="@ep"/>
    <xsl:variable name="instance" select="@instance"/>
    <decnamelist name="{$epName}{$instance}nml">
     <xsl:for-each select="document($root/coupled/models/model)/definition[name=$modName]//entryPoint[@name=$epName]/data">
      <xsl:variable name="argref" select="$commFormsRoot/communicationForms/*[@modelName=$modName and @epName=$epName and 
                                  @id=current()/@id and (not($instance) or @instance=$instance)]/@ref"/>
      <nmldata name="r{$argref}"/>
     </xsl:for-each>
    </decnamelist>
   </xsl:for-each>
   <comment>End declaration of namelist output</comment>
  </xsl:if>
 </DataDecs>
</xsl:template>

<xsl:template match="InitValues">
 <InitValues>
  <xsl:apply-templates select="*"/>
  <comment>Begin initial values data</comment>

  <xsl:variable name="MainProgID" select="ancestor::code/@id"/>

	  <!--Multi-instance support not added here-->
	  <comment>Begin P2P notation priming</comment>
	  <xsl:for-each select="document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed">
	   <xsl:if test="data">
	    <xsl:variable name="modelName" select="ancestor::model/@name"/>
	    <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
	    <xsl:variable name="id" select="@id"/>

	    <!-- is this data on my deployment unit? -->
	    <xsl:variable name="ModelDUID">
	      <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
		<xsl:if test="sequenceUnit/model/@name=$modelName">
		  <xsl:value-of select="position()"/>
		</xsl:if>
	      </xsl:for-each>
	    </xsl:variable>
	    <xsl:variable name="srcOnMyDU" select="boolean($ModelDUID=$MainProgID)"/>

	    <!-- is this model argpassing? -->
	    <xsl:variable name="isArgPassing" select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data[@form='argpass' and ancestor::definition/name=$modelName and ancestor::entryPoint/@name=$epName and @id=$id]"/>

	    <xsl:if test="$srcOnMyDU and $isArgPassing">

	    <xsl:variable name="argref" select="$commFormsRoot/communicationForms
			  /*[@modelName=$modelName and @epName=$epName and @id=$id]/@ref"/>

	    <var ref="{$argref}" value="{data/@value}"/>
	    </xsl:if>
	   </xsl:if>
	  </xsl:for-each>
	  <comment>End P2P notation priming</comment>

	  <!--set notation priming-->
	  <comment>Begin set notation priming</comment>
	  <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/set/primed">
	   <xsl:if test="data">
	    <xsl:variable name="srcOnMyDU">
	     <xsl:for-each select="../field">
	      <xsl:variable name="modelName" select="@modelName"/>
	      <xsl:variable name="epName" select="@epName"/>
	      <xsl:variable name="id" select="@id"/>
	      <xsl:variable name="instance" select="@instance"/>
	      <!-- is this data on my deployment unit? -->
              <xsl:if test="document($root/coupled/models/model)/definition[name=$modelName]/language!='intrinsic'">
	       <xsl:variable name="ModelDUID">
	        <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
                 <xsl:if test="sequenceUnit/model[@name=$modelName and (not($instance) or @instance=$instance)]">
	 	  <xsl:value-of select="position()"/>
                 </xsl:if>
	        </xsl:for-each>
	       </xsl:variable>
	       <xsl:if test="boolean($ModelDUID=$MainProgID)">
	        <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
	       </xsl:if>
              </xsl:if>
	     </xsl:for-each>
	    </xsl:variable>
	    <xsl:if test="boolean($srcOnMyDU!='')">
	     <xsl:variable name="pos" select="substring-before($srcOnMyDU,':')"/>
	     <xsl:variable name="modelName" select="../field[position()=$pos]/@modelName"/>
	     <xsl:variable name="epName" select="../field[position()=$pos]/@epName"/>
	     <xsl:variable name="id" select="../field[position()=$pos]/@id"/>
             <xsl:variable name="instance" select="../field[position()=$pos]/@instance"/>
	     <!--for some reason, processor was returning wrong ref attribute when normalize-space and number functions weren't used-->
	     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
			  /*[normalize-space(@modelName)=normalize-space($modelName) and normalize-space(@epName)=normalize-space($epName) and 
			    number(@id)=number($id) and (not($instance) or number(@instance)=number($instance))]/@ref"/>
	     <var ref="{$argref}" value="{data/@value}"/>
	    </xsl:if>
	   </xsl:if>
	  </xsl:for-each>
	  <comment>End set notation priming</comment>
	  <comment>End initial values data</comment>
	  <comment>Begin initial values file read</comment>
	  <comment>namelist files</comment>
	  <!-- for each unique namelist filename -->
          <xsl:variable name="myDU" select="ancestor::code/@id"/>
          <xsl:for-each select="document($root/coupled/composition)
				   /composition//primed/file[@type='namelist']">
           <xsl:if test="generate-id(.)=generate-id(key('nml-files',@name)[1])">
            <xsl:variable name="filename" select="@name"/>
            <xsl:variable name="usedInFile">
             <xsl:apply-templates select="document($root/coupled/composition)
                                         /composition//primed/file[@type='namelist' and @name=$filename]" mode="used">
              <xsl:with-param name="myDU" select="$myDU"/>
             </xsl:apply-templates>
            </xsl:variable>
            <xsl:if test="contains($usedInFile,'true')">
             <file type="{@type}" name="{@name}">
              <xsl:call-template name="modelsUsingFile">
               <xsl:with-param name="filename" select="@name"/>
               <xsl:with-param name="DU" select="$myDU"/>
              </xsl:call-template>
              <xsl:for-each select="document($root/coupled/composition)
                                   /composition//primed/file[@type='namelist' and @name=$filename]">
               <xsl:if test="generate-id(.)=generate-id(key('namelists',@nlName)[1])">
                <xsl:variable name="nmlname" select="@nlName"/>
                <xsl:variable name="usedInNamelist">
                 <xsl:apply-templates select="document($root/coupled/composition)
                                             /composition//primed/file[@type='namelist' and @name=$filename and @nlName=$nmlname]" mode="used">
                  <xsl:with-param name="myDU" select="$myDU"/>
                 </xsl:apply-templates>
                </xsl:variable>
                <xsl:if test="contains($usedInNamelist,'true')">
                 <namelist name="{@nlName}">
                  <xsl:for-each select="document($root/coupled/composition)
                                       /composition//primed/file[@type='namelist' and @name=$filename and @nlName=$nmlname]">
                   <xsl:if test="generate-id(.)=generate-id(key('dataRefs',@dataRef)[1])">
                    <xsl:variable name="dataRef" select="@dataRef"/>
                    <xsl:variable name="used">
                     <xsl:apply-templates select="document($root/coupled/composition)
                                     /composition//primed/file[@type='namelist' and @name=$filename and @nlName=$nmlname and @dataRef=$dataRef]" mode="used">
                      <xsl:with-param name="myDU" select="$myDU"/>
                     </xsl:apply-templates>
                    </xsl:variable>
                    <xsl:variable name="usedData">
                     <xsl:choose>
                      <xsl:when test="contains($used,'true')">
                       <xsl:text>true</xsl:text>
                      </xsl:when>
                      <xsl:otherwise>
                       <xsl:text>false</xsl:text>
                      </xsl:otherwise>
                     </xsl:choose>
                    </xsl:variable>
                    <xsl:choose>
                     <xsl:when test="ancestor::model">
                      <xsl:variable name="modelName" select="ancestor::model/@name"/>
                      <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
                      <xsl:variable name="id" select="../@id"/>
                      <xsl:variable name="instance" select="../../@instance"/>
                      <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                         /*[@modelName=$modelName and @epName=$epName and @id=$id and
                                                         (not($instance) or @instance=$instance)]/@ref"/>
                      <data ref="{$argref}" fileref="{@dataRef}" used="{$usedData}"/>
                     </xsl:when>
                     <xsl:when test="ancestor::set">
                      <xsl:variable name="modelName" select="../../field[1]/@modelName"/>
                      <xsl:variable name="epName" select="../../field[1]/@epName"/>
                      <xsl:variable name="id" select="../../field[1]/@id"/>
                      <xsl:variable name="instance" select="../../field[1]/@instance"/>
                      <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                         /*[@modelName=$modelName and @epName=$epName and @id=$id and 
                                                         (not($instance) or @instance=$instance)]/@ref"/>
                      <data ref="{$argref}" fileref="{@dataRef}" used="{$usedData}"/>
                     </xsl:when>
                    </xsl:choose>
                   </xsl:if>
                  </xsl:for-each>
                 </namelist>
                </xsl:if>
               </xsl:if>
              </xsl:for-each>
             </file>
            </xsl:if>
           </xsl:if>
          </xsl:for-each>

  <!-- now for any netcdf files -->
  <comment>netcdf files</comment>
  <!-- for each unique netcdf filename -->
  <!-- should really use a key here or exslt set support but below works
       I've no idea how though!!!!! -->
  <xsl:variable name="ids3" select="document($root/coupled/composition)
                            /composition//primed/file[@type='netcdf']"/>
  <xsl:for-each select="$ids3">
   <xsl:if test="generate-id(.)=generate-id($ids3[@name=current()/@name])">
    <xsl:variable name="myname" select="@name"/>
    <file type="{@type}" name="{@name}">
      <!-- now for each var for this file -->
      <xsl:for-each select="document($root/coupled/composition)
                    /composition//primed">
       <xsl:choose>
	  <!--Multi-instance support not added here-->
        <xsl:when test="file/@type='netcdf' and file/@name=$myname and ancestor::model">
         <xsl:variable name="modelName" select="ancestor::model/@name"/>
         <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
         <xsl:variable name="id" select="@id"/>
         <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                      /*[@modelName=$modelName and @epName=$epName and @id=$id]/@ref"/>
         <!--
         <xsl:variable name="argref">
          <xsl:call-template name="dataidgen">
           <xsl:with-param name="modelName" select="$modelName"/>
           <xsl:with-param name="epName" select="$epName"/>
           <xsl:with-param name="id" select="$id"/>
          </xsl:call-template>
         </xsl:variable>
         -->
         <data ref="{$argref}" fileref="{file/@dataRef}"/>
        </xsl:when>
        <xsl:otherwise>
         <xsl:variable name="modelName" select="../field[1]/@modelName"/>
         <xsl:variable name="epName" select="../field[1]/@modelName"/>
         <xsl:variable name="id" select="../field[1]/@id"/>
         <xsl:variable name="instance" select="../field[1]/@instance"/>
         <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                      /*[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/@ref"/>
         <!--
         <xsl:variable name="argref">
          <xsl:call-template name="dataidgen">
           <xsl:with-param name="modelName" select="$modelName"/>
           <xsl:with-param name="epName" select="$epName"/>
           <xsl:with-param name="id" select="$id"/>
          </xsl:call-template>
         </xsl:variable>
         -->
         <data ref="{$argref}" fileref="{file/@dataRef}"/>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:for-each>
    </file>
   </xsl:if>
  </xsl:for-each>
  <comment>End initial values file read</comment>
 </InitValues>
</xsl:template>

<xsl:template match="call">
 <xsl:variable name="modelName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="activate" select="@activate"/>
 <xsl:variable name="instance" select="@instance"/>
 <call epName="{$epName}" modelName="{$modelName}" name="{@name}" activate="{@activate}" instance="{@instance}">
  <xsl:for-each select="arg">
   <xsl:choose>
    <xsl:when test="@id">
     <xsl:variable name="myID" select="@id"/>
     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                   /*[@modelName=$modelName and @epName=$epName and @id=$myID and (not(@instance) or @instance=$instance)]/@ref"/>
     <!--
     <xsl:variable name="argref">
      <xsl:call-template name="dataidgen">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="epName" select="$epName"/>
       <xsl:with-param name="id" select="@id"/>
      </xsl:call-template>
     </xsl:variable>
     -->
     <xsl:choose>
      <xsl:when test="number($argref)">
       <!-- var does not have a name so a unique id has been generated -->
       <arg name="r{$argref}"/>
      </xsl:when>
      <xsl:otherwise>
       <!-- var is named so just use this -->
       <arg name="{$argref}"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="@name">
     <arg name="{@name}"/>
    </xsl:when>
    <xsl:when test="@value">
     <arg name="{@value}"/>
    </xsl:when>
   </xsl:choose>
  </xsl:for-each>
 </call>

	  <!--Multi-instance support not added here-->
<!-- ********** ADD logic to check if p2p for sink var and if connection is arg passing ????? ******** -->

  <!-- now copy data for p2p coupled argpassing data to respect dependencies (if model is activated) -->
  <xsl:if test="$activate='true'">

    <xsl:for-each select="arg">
      <xsl:variable name="id" select="@id"/>

      <!-- priming connections -->
      <xsl:for-each select="document($root/coupled/composition)/composition/connections/priming/model/entryPoint/primed/model[@name=$modelName and @entryPoint=$epName and @id=$id]">

        <xsl:variable name="remoteModelName" select="ancestor::model/@name"/>
        <xsl:variable name="remoteEPName" select="ancestor::entryPoint/@name"/>
        <xsl:variable name="remoteID" select="ancestor::primed/@id"/>

        <debug author="rupert" info="model {$modelName} ep {$epName} arg {$id} primes model {$remoteModelName} ep {$remoteEPName} id {$remoteID} in p2p notation"/>

        <!-- only copy data if priming is via arg passing -->
        <!-- we already know that local is arg passing so ...
             if on same su and if remote is argpassing -->

        <!-- are the models on the same su as eachother? -->
        <xsl:variable name="sameSU">
          <xsl:call-template name="inSequence">
            <xsl:with-param name="modelName1" select="$modelName"/>
            <xsl:with-param name="epName1" select="$epName"/>
            <xsl:with-param name="modelName2" select="$remoteModelName"/>
            <xsl:with-param name="epName2" select="$remoteEPName"/>
            <xsl:with-param name="root" select="$root"/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="remoteIsArgPassing" select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data[@form='argpass' and ancestor::definition/name=$remoteModelName and ancestor::entryPoint/@name=$remoteEPName and @id=$remoteID]"/>

        <xsl:if test="contains($sameSU,'TRUE') and $remoteIsArgPassing">

          <xsl:variable name="lhsArgRef" select="$commFormsRoot/communicationForms/*[@modelName=$remoteModelName and @epName=$remoteEPName and @id=$remoteID]/@ref"/>
  <!--
  <xsl:variable name="lhsArgRef">
    <xsl:call-template name="dataidgen">
    <xsl:with-param name="modelName" select="$remoteModelName"/>
    <xsl:with-param name="epName" select="$remoteEPName"/>
    <xsl:with-param name="id" select="$remoteID"/>
    </xsl:call-template>
  </xsl:variable>
  -->

          <xsl:variable name="rhsArgRef" select="$commFormsRoot/communicationForms/*[@modelName=$modelName and @epName=$epName and @id=$id]/@ref"/>
  <!--
  <xsl:variable name="rhsArgRef">
    <xsl:call-template name="dataidgen">
    <xsl:with-param name="modelName" select="$modelName"/>
    <xsl:with-param name="epName" select="$epName"/>
    <xsl:with-param name="id" select="$id"/>
    </xsl:call-template>
  </xsl:variable>
  -->

          <xsl:value-of select="$newline"/>
          <assign lhs="r{$lhsArgRef}" rhs="r{$rhsArgRef}"/>

        </xsl:if>

      </xsl:for-each>

  <!-- coupling connections -->
  <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel/epChannel/connection[ancestor::modelChannel/@outModel=$modelName and ancestor::epChannel/@outEP=$epName and @outID=$id]">

  <xsl:variable name="remoteModelName" select="ancestor::modelChannel/@inModel"/>
  <xsl:variable name="remoteEPName" select="ancestor::epChannel/@inEP"/>
  <xsl:variable name="remoteID" select="@inID"/>

<!-- only copy data if we couple via arg passing -->
<!-- we already know that local is arg passing so ...
  if on same su and if remote is argpassing -->

  <!-- are the models on the same su as eachother? -->
  <xsl:variable name="sameSU">
  <xsl:call-template name="inSequence">
  <xsl:with-param name="modelName1" select="$modelName"/>
  <xsl:with-param name="epName1" select="$epName"/>
  <xsl:with-param name="modelName2" select="$remoteModelName"/>
  <xsl:with-param name="epName2" select="$remoteEPName"/>
  <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="remoteIsArgPassing" select="document($root/coupled/models/model)/definition/entryPoints/entryPoint/data[@form='argpass' and ancestor::definition/name=$remoteModelName and ancestor::entryPoint/@name=$remoteEPName and @id=$remoteID]"/>

  <xsl:if test="contains($sameSU,'TRUE') and $remoteIsArgPassing">

  <xsl:variable name="lhsArgRef" select="$commFormsRoot/communicationForms
                   /*[@modelName=$remoteModelName and @epName=$remoteEPName and @id=$remoteID]/@ref"/>
  <!--
  <xsl:variable name="lhsArgRef">
    <xsl:call-template name="dataidgen">
    <xsl:with-param name="modelName" select="$remoteModelName"/>
    <xsl:with-param name="epName" select="$remoteEPName"/>
    <xsl:with-param name="id" select="$remoteID"/>
    </xsl:call-template>
  </xsl:variable>
  -->

  <xsl:variable name="rhsArgRef" select="$commFormsRoot/communicationForms
                   /*[@modelName=$modelName and @epName=$epName and @id=$id]/@ref"/>
  <!--
  <xsl:variable name="rhsArgRef">
    <xsl:call-template name="dataidgen">
    <xsl:with-param name="modelName" select="$modelName"/>
    <xsl:with-param name="epName" select="$epName"/>
    <xsl:with-param name="id" select="$id"/>
    </xsl:call-template>
  </xsl:variable>
  -->

<xsl:text>
</xsl:text>
    <assign lhs="r{$lhsArgRef}" rhs="r{$rhsArgRef}"/>

  </xsl:if>

  </xsl:for-each>

</xsl:for-each>

</xsl:if>

</xsl:template>

<xsl:template name="datatype">
  <xsl:param name="modelName"/>
  <xsl:param name="epName"/>
  <xsl:param name="id"/>
  <xsl:param name="root"/>
  <xsl:value-of select="document($root/coupled/models/model)
                /definition/entryPoints/entryPoint/data/@dataType
                [ancestor::data/@id=$id and ancestor::entryPoint/@name=$epName and 
                 ancestor::definition/name=$modelName]"/>
</xsl:template>


<!-- Decide whether a file is used within a given deployment unit-->
<xsl:template match="file" mode="used">
 <xsl:param name="myDU"/>
                   <xsl:choose>
                    <xsl:when test="ancestor::model">
                     <xsl:variable name="modelName" select="ancestor::model/@name"/>
                     <xsl:variable name="epName" select="ancestor::entryPoint/@name"/>
                     <xsl:variable name="id" select="../@id"/>
                     <xsl:variable name="instance" select="../../@instance"/>
                     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                        /*[@modelName=$modelName and @epName=$epName and @id=$id and
                                                        (not($instance) or @instance=$instance)]/@ref"/>
                     <xsl:variable name="modDU">
                      <xsl:for-each select="document($root/coupled/deployment)//deploymentUnit">
                       <xsl:if test="sequenceUnit/model[@name=$modelName]">
                        <xsl:value-of select="position()"/>
                       </xsl:if>
                      </xsl:for-each>
                     </xsl:variable>
                      <xsl:choose>
                       <xsl:when test="$modDU=$myDU">
                        <xsl:text>true</xsl:text>
                       </xsl:when>
                       <xsl:otherwise>
                        <xsl:text>false</xsl:text>
                       </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:when test="ancestor::set">
                     <xsl:variable name="modelName" select="../../field[1]/@modelName"/>
                     <xsl:variable name="epName" select="../../field[1]/@epName"/>
                     <xsl:variable name="id" select="../../field[1]/@id"/>
                     <xsl:variable name="instance" select="../../field[1]/@instance"/>
                     <xsl:variable name="argref" select="$commFormsRoot/communicationForms
                                                        /*[@modelName=$modelName and @epName=$epName and @id=$id and 
                                                        (not($instance) or @instance=$instance)]/@ref"/>
                     <xsl:variable name="modDU">
                      <xsl:for-each select="document($root/coupled/deployment)//deploymentUnit">
                       <xsl:if test="sequenceUnit/model[@name=$modelName and (not(@instance) or @instance=$instance)]">
                        <xsl:value-of select="position()"/>
                       </xsl:if>
                      </xsl:for-each>
                     </xsl:variable>
                      <xsl:choose>
                       <xsl:when test="$modDU=$myDU">
                        <xsl:text>true</xsl:text>
                       </xsl:when>
                       <xsl:otherwise>
                        <xsl:text>false</xsl:text>
                       </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                   </xsl:choose>
</xsl:template>



<!--
This template determines which sequence units should call namelist read subroutines.
An element is output for the first model that requires a read to the filename passed in on each SU.
Then we can perform a test in main code
e.g.
  if(atmosThread())then
    call nmlinit11(a,b,c)
  end if
  if(oceanThread())then
    call nmlinit12(a,d,e)
  end if
In this example, the variable a is primed from different files, and so the tests are necessary.
-->
<xsl:template name="modelsUsingFile">
 <xsl:param name="filename"/>
 <xsl:param name="DU"/>
 <xsl:choose>
  <xsl:when test="document($root/coupled/composition)/composition//file[@name=$filename]/ancestor::model">

 <xsl:variable name="set" select="document($root/coupled/composition)/composition//file[@name=$filename]/ancestor::model"/>
 <!--need to find the model with the first reference to the field in each SU in current DU-->
 <xsl:for-each select="document($root/coupled/deployment)/deployment//deploymentUnit">
 <xsl:if test="position()=$DU">
 <xsl:for-each select="sequenceUnit">
  <xsl:variable name="SU" select="."/>
  <xsl:variable name="list">
   <xsl:for-each select="$set">
    <xsl:variable name="modName" select="@name"/>
    <xsl:variable name="epName" select="entryPoint/@name"/>
    <xsl:variable name="instance" select="entryPoint/@instance"/>
    <!-- Determine whether the current field's associated model runs as part
    of the given sequence unit -->
    <xsl:variable name="inSU">
     <xsl:for-each select="$SU/model">
      <xsl:if test="@name=$modName">
       <xsl:value-of select="'T'"/>
      </xsl:if>
     </xsl:for-each>
    </xsl:variable>
    <xsl:if test="$inSU!=''">
     <!-- Build a list of the model entryPoint positions in the schedule for
     the current field. -->
     <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
      <xsl:if test="@name=$modName and @ep=$epName and (not(@instance) or @instance=$instance)">
       <xsl:value-of select="position()"/>
       <xsl:value-of select="','"/>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <!-- Sort the entryPoint position list - it will need sorting, as fields
  within a set need not be listed in the same order as their corresponding
  entry points within the schedule. -->
  <xsl:variable name="sortedlist">
   <xsl:call-template name="sort">
    <xsl:with-param name="list" select="$list"/>
   </xsl:call-template>
  </xsl:variable>
  <!-- Find the first model entryPoint in the schedule that gets called for the
  current set (remember, not necessarily = to first field listed for the set in
  compose.xml) -->
  <!-- IRH MOD: I've modified the "sort" template in such a way that trailing
  commas will be removed, so one-element lists will be e.g. '10' instead of '10,'.
  We must therefore check that the list contains commas before doing "substring-before".
  See Utils/sorting.xsl for more information. -->
  <xsl:variable name="firstRefPos">
   <xsl:choose>
    <xsl:when test="contains($sortedlist,',')">
     <xsl:value-of select="number(substring-before($sortedlist,','))"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="number($sortedlist)"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <!-- <xsl:variable name="firstRefPos" select="number(substring-before($sortedlist,','))"/> -->
  <xsl:variable name="targetMod">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
    <xsl:if test="position()=$firstRefPos">
     <xsl:value-of select="@name"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <xsl:variable name="instance">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
    <xsl:if test="position()=$firstRefPos">
     <xsl:value-of select="@instance"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <xsl:if test="$targetMod!=''">
   <model name="{$targetMod}" instance="{$instance}"/>
  </xsl:if>
 </xsl:for-each>
 </xsl:if>
 </xsl:for-each>

  </xsl:when>


  <xsl:when test="document($root/coupled/composition)/composition//file[@name=$filename]/ancestor::set">

 <xsl:variable name="set" select="document($root/coupled/composition)/composition//file[@name=$filename]/ancestor::set"/>
 <!--need to find the model with the first reference to the field in each SU in current DU-->
 <xsl:for-each select="document($root/coupled/deployment)/deployment//deploymentUnit">
 <xsl:if test="position()=$DU">
 <xsl:for-each select="sequenceUnit">
  <xsl:variable name="SU" select="."/>
  <xsl:variable name="list">
   <xsl:for-each select="$set/field">
    <xsl:variable name="modName" select="@modelName"/>
    <xsl:variable name="epName" select="@epName"/>
    <xsl:variable name="instance" select="@instance"/>
    <!-- Determine whether the current field's associated model runs as part
    of the given sequence unit -->
    <xsl:variable name="inSU">
     <xsl:for-each select="$SU/model">
      <xsl:if test="@name=$modName">
       <xsl:value-of select="'T'"/>
      </xsl:if>
     </xsl:for-each>
    </xsl:variable>
    <xsl:if test="$inSU!=''">
     <!-- Build a list of the model entryPoint positions in the schedule for
     the current field. -->
     <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
      <xsl:if test="@name=$modName and @ep=$epName and (not(@instance) or @instance=$instance)">
       <xsl:value-of select="position()"/>
       <xsl:value-of select="','"/>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <!-- Sort the entryPoint position list - it will need sorting, as fields
  within a set need not be listed in the same order as their corresponding
  entry points within the schedule. -->
  <xsl:variable name="sortedlist">
   <xsl:call-template name="sort">
    <xsl:with-param name="list" select="$list"/>
   </xsl:call-template>
  </xsl:variable>
  <!-- Find the first model entryPoint in the schedule that gets called for the
  current set (remember, not necessarily = to first field listed for the set in
  compose.xml) -->
  <!-- IRH MOD: I've modified the "sort" template in such a way that trailing
  commas will be removed, so one-element lists will be e.g. '10' instead of '10,'.
  We must therefore check that the list contains commas before doing "substring-before".
  See Utils/sorting.xsl for more information. -->
  <xsl:variable name="firstRefPos">
   <xsl:choose>
    <xsl:when test="contains($sortedlist,',')">
     <xsl:value-of select="number(substring-before($sortedlist,','))"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="number($sortedlist)"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <!-- <xsl:variable name="firstRefPos" select="number(substring-before($sortedlist,','))"/> -->
  <xsl:variable name="targetMod">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
    <xsl:if test="position()=$firstRefPos">
     <xsl:value-of select="@name"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <xsl:variable name="instance">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
    <xsl:if test="position()=$firstRefPos">
     <xsl:value-of select="@instance"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:variable>
  <xsl:if test="$targetMod!=''">
   <model name="{$targetMod}" instance="{$instance}"/>
  </xsl:if>
 </xsl:for-each>
 </xsl:if>
 </xsl:for-each>

  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error in modelsUningFile template in Main/5/DeclareArgs.xsl</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="inSequence">
  <xsl:param name="modelName1"/>
  <xsl:param name="epName1"/>
  <xsl:param name="modelName2"/>
  <xsl:param name="epName2"/>
  <xsl:param name="root"/>

  <xsl:choose>
    <xsl:when test="document($root//deployment)//sequenceUnit[model/@name=$modelName1 and model/@name=$modelName2]">
      <xsl:text>TRUE</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>FALSE</xsl:text>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>



</xsl:stylesheet>
