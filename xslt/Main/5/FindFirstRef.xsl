<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:include href="../../Utils/sorting.xsl"/>


<xsl:template name="findFirstRef">
 <xsl:param name="set"/>
 <xsl:param name="SU"/>
 <xsl:param name="SUpos"/>

 <!-- Build a list of entryPoint positions in the schedule for
 entry points for all fields in the given set associated with models
 running under the given sequence unit. -->
 <xsl:variable name="list">
  <xsl:for-each select="$set/field">
   <xsl:variable name="modName" select="@modelName"/>
   <xsl:variable name="epName" select="@epName"/>
   <xsl:variable name="instance" select="@instance"/>
   <xsl:if test="not(document($root/coupled/models/model)/definition[name=$modName]/language='intrinsic')">

   <!-- Determine whether the current field's associated model runs as part
   of the given sequence unit -->
   <xsl:variable name="inSU">
    <xsl:for-each select="$SU/model">
     <xsl:if test="@name=$modName">
      <xsl:value-of select="'T'"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:variable>

   <xsl:if test="$inSU!=''">
    <!-- Build a list of the model entryPoint positions in the schedule for
    the current field. -->
    <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
     <xsl:if test="@name=$modName and @ep=$epName and (not(@instance) or @instance=$instance)">
      <xsl:value-of select="position()"/>
      <xsl:value-of select="','"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:if>

   </xsl:if>
  </xsl:for-each>
 </xsl:variable>

 <!--
 <xsl:message terminate="no">
  <xsl:text>unsorted list: </xsl:text><xsl:value-of select="$list"/>
 </xsl:message>
 -->

 <!-- Sort the entryPoint position list - it will need sorting, as fields
 within a set need not be listed in the same order as their corresponding
 entry points within the schedule. -->
 <xsl:variable name="sortedlist">
  <xsl:call-template name="sort">
   <xsl:with-param name="list" select="$list"/>
  </xsl:call-template>
 </xsl:variable>

 <!--
 <xsl:message terminate="no">
  <xsl:text>sorted list: </xsl:text><xsl:value-of select="$sortedlist"/>
 </xsl:message>
 -->

  <!-- Find the first model entryPoint in the schedule that gets called for the
  current set (remember, not necessarily = to first field listed for the set in
  compose.xml) -->
  <!-- IRH MOD: I've modified the "sort" template in such a way that trailing
  commas will be removed, so one-element lists will be e.g. '10' instead of '10,'.
  We must therefore check that the list contains commas before doing "substring-before".
  See Utils/sorting.xsl for more information. -->
  <xsl:variable name="firstRefPos">
   <xsl:choose>
    <xsl:when test="contains($sortedlist,',')">
     <xsl:value-of select="number(substring-before($sortedlist,','))"/>
    </xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="number($sortedlist)"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <!-- <xsl:variable name="firstRefPos" select="number(substring-before($sortedlist,','))"/> -->

  <!--
  <xsl:message terminate="no">
   <xsl:text>first ref pos: </xsl:text><xsl:value-of select="$firstRefPos"/>
  </xsl:message>
  -->

  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model">
   <xsl:if test="position()=$firstRefPos">
    <xsl:variable name="targetMod" select="@name"/>
    <xsl:variable name="targetEP" select="@ep"/>
    <xsl:variable name="targetInstance" select="@instance"/>
    <firstRef model="{$targetMod}" entryPoint="{$targetEP}" suid="{$SUpos}" instance="{$targetInstance}">

      <!--
      CWA: Removed the following because regridding should be handled as 
      per other transformations.

      IRH: Get and declare the dimensions of the argument/field for which
      this entryPoint is the first caller in the current sequence unit.
      Most of the following code was moved from DeclareArgs.xsl and modified to
      use targetMod, targetEP etc. variables. 
      <xsl:variable name="targetModDef" select="document($root/coupled/models/model)/definition[name=$targetMod]"/>
      <xsl:variable name="targetEPDef" select="$targetModDef/entryPoints/entryPoint[@name=$targetEP]"/>
      <xsl:variable name="targetEPArgID" select="$set/field[@modelName=$targetMod and @epName=$targetEP]/@id"/>
      <xsl:variable name="targetEPArgData" select="$targetEPDef/data[@id=$targetEPArgID]"/>
      <xsl:for-each select="$targetEPArgData/dim">
        <xsl:for-each select="lower">
          <dim order="{2 * number(../@value)-1}">
            <lower>
              <xsl:apply-templates select="*">
                <xsl:with-param name="modelName" select="$targetMod"/>
                <xsl:with-param name="epName" select="$targetEP"/>
                <xsl:with-param name="id" select="$targetEPArgID"/>
                <xsl:with-param name="instance" select="$targetInstance"/>
              </xsl:apply-templates>
            </lower>
          </dim>
        </xsl:for-each>
        <xsl:for-each select="upper">
          <dim order="{2 * number(../@value)}">
            <upper>
              <xsl:apply-templates select="*">
                <xsl:with-param name="modelName" select="$targetMod"/>
                <xsl:with-param name="epName" select="$targetEP"/>
                <xsl:with-param name="id" select="$targetEPArgID"/>
                <xsl:with-param name="instance" select="$targetInstance"/>
              </xsl:apply-templates>
            </upper>
          </dim>
        </xsl:for-each>
      </xsl:for-each>
-->
     </firstRef>
   </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
