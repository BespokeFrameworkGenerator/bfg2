<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
    Main Phase 1 

    Input template:
    <framework>
     <code>
      <EP>
       <Includes/>
       <DataDecs/>
       <Content/>
      </EP>
     </code>
    </framework>

    This phase expands the template to include a <code> element 
    for each deployment unit and add detail to <Content/>
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="DUIndex" select="'false'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<!--
    Include a <code> element for each deployment unit.
-->
<xsl:template match="code">
 <xsl:variable name="children" select="*"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
  <xsl:if test="$DUIndex=position() or $DUIndex='false'">
    <code id="{position()}">
     <xsl:apply-templates select="$children"/>
    </code>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<!--
    Add detail to Content, the executable part of the code. This is made up of initialising values and 
    then controlling execution of models. Either side of these activities, communication is initialised
    and shut down.
-->
<xsl:template match="Content">
 <Content>
  <!-- IRH: Moved InitValues to before Comms - OASIS4 requires dimensions of
  coupling fields to be initialised via <InitValues> *before* initComms() is
  called as these dimensions are required for initialising gridless grids.
  NO - solution isn't so simple - <InitValues> currently includes the call
  to initModelInfo().  initModelInfo() must be called AFTER initComms(), 
  as initComms() initialises the b2mmap() array, which initModelInfo
  requires for setting info(epNo)%du.  So either way, we have a problem.
  Need to disentangle this initialisation stuff, and find a way to make
  the r1, r2, r3 etc. coupling field dimensions in BFG2Main "visible" to
  the gridless grid initialisation code in BFG2Target. -->
  <!-- <InitValues/> -->
  <Comms>
   <InitValues/>
   <Control/>
  </Comms>
 </Content>
 <xsl:apply-templates select="*"/>
</xsl:template>

<!--
    Catch-all template that outputs untouched elements.
-->
<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
