<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<!--
    Main Phase 2.
    Responsibility is:
    1: name of each "main" entry point
    2: control and model invocation
    3: interface to framework specific layer

    In this phase we
    1: (MainName.xsl) Create a main program/class start and end
       point and name it. This name has a default (BFG2Main)
       which can be changed by using the parameter
       ProgramName - see MainName.xsl for more details.
    2: (Control.xsl) Create the required model calls
       and control structure.
    3: (CommsInterface.xsl) Add the start and end calls to the
       FSL and any required includes, such as the include for
       the communication specific layer - FSL (which is generated
       separately). This name has a default (BFG2Target) which
       can be changed by using the parameter UseTarget. Note,
       this would also need to be changed in the FSL.
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="MainName.xsl"/>
<xsl:include href="Control.xsl"/>
<xsl:include href="CommsInterface.xsl"/>
<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
