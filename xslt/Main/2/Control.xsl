<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<!--
    RF 16th Sept 2005
    Adds fortran control structure to BFG2 code
    template. Assumes <DataDecsBegin>, <DataDecsEnd>,
    <InitValuesBegin>, <InitValuesEnd> and <Control>
    exist in the code template.

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.

    Control code reads timestep information from a file
    so that these values can be changed without rerunning
    BFG2. By default the filename is BFGControl.nam
    however the argument -PARAM FileName <MyFile>
    will change this to <MyFile>.

-->

<xsl:param name="FileName" select="'BFG2Control.nam'"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>

<xsl:include href="../../Utils/ThreadName.xsl"/>
<xsl:include href="../../Utils/RenamedEP.xsl"/>
<xsl:include href="../../Utils/Loops.xsl"/>
<xsl:include href="../../Utils/SetNotationUtils.xsl"/>


<xsl:template match="DataDecs">
 <DataDecs>
  <comment>Begin declaration of Control</comment>
  <!--Declare loop bounds-->
  <xsl:for-each select="document($root/coupled/deployment)//loop">
   <!--<declare name="its{position()}" datatype="integer"/> declared in Target because it's used in getNext/getLast functions-->
   <declare name="nts{position()}" datatype="integer"/>
  </xsl:for-each>
  <!--Declare model timestepping frequencies-->
  <xsl:for-each select="document($root/coupled/models/model)/definition/timestep">
   <xsl:variable name="modelName" select="../name"/>
   <declare name="{$modelName}__freq" datatype="integer"/>
  </xsl:for-each>
  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@rate]">
   <xsl:variable name="modelName" select="@name"/>
   <declare name="{$modelName}_{@instance}_freq" datatype="integer"/>
  </xsl:for-each>
  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@offset]">
   <xsl:variable name="modelName" select="@name"/>
   <declare name="{$modelName}_{@instance}_offset" datatype="integer"/>
  </xsl:for-each>
  <!--Declare namelist for loop bounds and TS frequencies-->
  <decnamelist name="time">
  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule/bfgiterate//loop">
   <xsl:variable name="tsvar">
    <xsl:text>nts</xsl:text>
    <xsl:call-template name="get_loop_pos">
     <xsl:with-param name="myloopid" select="generate-id(.)"/>
    </xsl:call-template>
   </xsl:variable>
   <nmldata name="{$tsvar}"/>
  </xsl:for-each>
  <xsl:for-each select="document($root/coupled/models/model)/definition/timestep">
   <xsl:variable name="modelName" select="../name"/>
   <nmldata name="{$modelName}__freq"/>
  </xsl:for-each>
  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@rate]">
   <xsl:variable name="modelName" select="@name"/>
   <nmldata name="{$modelName}_{@instance}_freq"/>
  </xsl:for-each>
  <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@offset]">
   <xsl:variable name="modelName" select="@name"/>
   <nmldata name="{$modelName}_{@instance}_offset"/>
  </xsl:for-each>
  </decnamelist>
  <comment>****End declaration of Control****</comment>
 </DataDecs>
</xsl:template>


<xsl:template match="InitValues">
 <InitValues>
  <comment>Begin control values file read</comment>
  <file name="{$FileName}" type="namelist">
   <namelist name="time"/>
  </file>
  <comment>End control values file read</comment>
  <!-- Only call initModelInfo if we are using set notation and at least one of the connections requires concurrent execution -->
  <xsl:variable name="RetValue">
    <xsl:call-template name="SetNotationWithConcurrentExecution"/>
  </xsl:variable>
  <xsl:if test="$RetValue='true'">
    <comment>Init model data structures</comment>
    <comment>(for concurrent models coupled using set notation)</comment>
    <call name="initModelInfo"/>
  </xsl:if>
 </InitValues>
</xsl:template>


<xsl:template match="Content">
 <Content>
  <xsl:apply-templates select="*"/>
 </Content>
</xsl:template>


<xsl:template match="Control">
 <xsl:variable name="ControlType">
  <xsl:choose>
   <xsl:when test="document($root/coupled/deployment)/deployment/schedule/bfgiterate">
    <xsl:text>bfgiterate</xsl:text>
   </xsl:when>
   <xsl:when test="document($root/coupled/deployment)/deployment/schedule/bfgevent">
    <xsl:text>bfgevent</xsl:text>
   </xsl:when>
   <xsl:otherwise>
    <xsl:message terminate="yes">
     <xsl:text>Schedule type not allowed</xsl:text>
     <xsl:value-of select="$newline"/>
    </xsl:message>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$ControlType='bfgiterate'">
   <xsl:call-template name="bfgiterateControl">
    <xsl:with-param name="mycode" select="../../../.."/>
   </xsl:call-template>
  </xsl:when>
  <xsl:when test="$ControlType='bfgevent'">
   <xsl:message terminate="yes">
    <xsl:text>Schema support for bfgevent control is not yet written.</xsl:text>
    <xsl:text>Fatal error, aborting code generation.</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:message>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Unrecognised control type *</xsl:text>
    <xsl:value-of select="$ControlType"/><xsl:text>*</xsl:text>
    <xsl:text>Known types are bfgts, bfgiterate and bfgevent.</xsl:text>
    <xsl:text>Fatal error, aborting code generation.</xsl:text>
    <xsl:value-of select="$newline"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="bfgiterateControl">
 <xsl:param name="mycode"/>
 <xsl:apply-templates select="document($root/coupled/deployment)/deployment/schedule/bfgiterate/init/*" mode="bfgiterate">
  <xsl:with-param name="mycode" select="$mycode"/>
 </xsl:apply-templates>
 <xsl:apply-templates select="document($root/coupled/deployment)/deployment/schedule/bfgiterate/iterate/*" mode="bfgiterate">
  <xsl:with-param name="mycode" select="$mycode"/>
 </xsl:apply-templates>
 <xsl:apply-templates select="document($root/coupled/deployment)/deployment/schedule/bfgiterate/final/*" mode="bfgiterate">
  <xsl:with-param name="mycode" select="$mycode"/>
 </xsl:apply-templates>
</xsl:template>


<xsl:template match="model" mode="bfgiterate">
 <xsl:param name="mycode"/><!--mycode is the <code> node-set we're currently processing-->
 <xsl:variable name="modName" select="@name"/>
 <xsl:variable name="epName" select="@ep"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="duPos">
  <!--find the position of the deployment unit for this model-->
  <xsl:for-each select="document($root/coupled/deployment)//deploymentUnit">
   <xsl:if test="sequenceUnit/model[@name=$modName and (not(@instance) or @instance=$instance)]">
    <xsl:value-of select="position()"/>
   </xsl:if>
  </xsl:for-each>
 </xsl:variable>
 <xsl:variable name="activate">
  <!--check the deployment unit currently being written is the same as the model's-->
  <xsl:choose>
   <xsl:when test="document($root/coupled/models/model)/definition[name=$modName]/language='intrinsic'">
    <xsl:text>false</xsl:text>
   </xsl:when>
   <xsl:when test="$mycode/@id=$duPos">
    <xsl:text>true</xsl:text>
   </xsl:when>
   <xsl:otherwise>
    <xsl:text>false</xsl:text>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>
 <xsl:variable name="condition">
  <xsl:call-template name="ThreadName">
   <xsl:with-param name="modelName" select="$modName"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:call-template>
  <xsl:text>()</xsl:text>
 </xsl:variable>
 <!-- what language is this model written in? -->
 <xsl:variable name="lang">
   <xsl:for-each select="$root/coupled/models/model">
    <xsl:value-of select="document(.)/definition/language[../name=$modName]"/>
   </xsl:for-each>
 </xsl:variable>
 <!-- determine what name to use when calling this entry point -->
 <xsl:variable name="callName">
   <xsl:choose>
    <!-- f90 so we will rename the entry point -->
    <xsl:when test="$lang='f90'">
<!--
 and document($root/coupled/models/model)/definition[name=$modName]/languageSpecific/fortran/moduleName and count(document($root/coupled/models/model)/definition[name=$modName]/entryPoints)=1"
-->
     <xsl:call-template name="RenamedEP">
      <xsl:with-param name="modelName" select="@name"/>
      <xsl:with-param name="EP" select="@ep"/>
     </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
     <xsl:value-of select="@ep"/>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <xsl:variable name="myEp" select="@ep"/>
  <xsl:choose>
   <xsl:when test="$activate='true'">
    <if>
     <condition>
      <value><xsl:value-of select="$condition"/></value>
     </condition>
     <action>
      <xsl:variable name="timestep" select="(document($root/coupled/models/model)
                                    /definition[entryPoints/entryPoint[@name=$myEp]
                                    /@type='iteration']/name[. = $modName]/../timestep or
                                    document($root/coupled/deployment)/deployment/schedule
                                    //iterate//model[@name=$modName and @ep=$epName and (not($instance) or @instance=$instance)]/@rate) and
                                    current()/ancestor::iterate"/>
      <xsl:choose>
       <xsl:when test="$timestep">
        <xsl:variable name="offset">
         <xsl:choose>
          <xsl:when test="boolean(document($root/coupled/deployment)/deployment/schedule
                        //iterate//model[@name=$modName and @ep=$myEp and (not($instance) or @instance=$instance)]/@offset)">
           <xsl:value-of select="$modName"/><xsl:text>_</xsl:text><xsl:value-of select="@instance"/><xsl:text>_offset</xsl:text>
          </xsl:when>
          <xsl:otherwise/>
         </xsl:choose>
        </xsl:variable>
        <xsl:choose>
         <xsl:when test="document($root/coupled/deployment)/deployment/schedule
                         //iterate//model[@name=$modName and @ep=$myEp and (not($instance) or @instance=$instance) and @rate]">
          <timestep variable="{$modName}_{@instance}_freq" offset="{$offset}">
           <call name="{$callName}" modelName="{@name}" epName="{@ep}" activate="true" instance="{$instance}"/>
          </timestep>
         </xsl:when>
         <xsl:otherwise>
          <timestep variable="{$modName}__freq" offset="{$offset}">
           <call name="{$callName}" modelName="{@name}" epName="{@ep}" activate="true" instance="{$instance}"/>
          </timestep>
         </xsl:otherwise>
        </xsl:choose>
       </xsl:when>
       <xsl:otherwise>
        <call name="{$callName}" modelName="{@name}" epName="{@ep}" activate="true" instance="{$instance}"/>
       </xsl:otherwise>
      </xsl:choose>
     </action>
    </if>
   </xsl:when>
   <xsl:otherwise>
    <call name="{$callName}" modelName="{@name}" epName="{@ep}" activate="false" instance="{$instance}"/>
   </xsl:otherwise>
  </xsl:choose>
 <!--</xsl:if>-->
</xsl:template>


<!-- IRH: TODO: commsSync call added below.  This is at the right place for the 
OASIS4 gridless grid examples, but will not necessarily be right for other 
models.  Can we calculate in the XSL where a sync should occur, or should we 
add a <sync.../<block... element to the timestep loop in deploy.xml? -->
<xsl:template match="loop" mode="bfgiterate">
 <xsl:param name="mycode"/>
 <xsl:variable name="loop_pos">
  <xsl:call-template name="get_loop_pos">
   <xsl:with-param name="myloopid" select="generate-id(.)"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:variable name="ntsname">
  <xsl:text>nts</xsl:text><xsl:value-of select="$loop_pos"/>
 </xsl:variable>
 <iterate start="1" end="{$ntsname}">
  <xsl:apply-templates select="*" mode="bfgiterate">
   <xsl:with-param name="mycode" select="$mycode"/>
  </xsl:apply-templates>
  <xsl:if test="$loop_pos = 1">
   <call name="commsSync"/>
  </xsl:if>
 </iterate>
</xsl:template>

</xsl:stylesheet>
