<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 16th Sept 2005
    Adds fortran comms calls to  BFG2 control code
    template. Assumes <Comms> exists in
    the code template.

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.

    By default the target specific f90 module is called
    BFG2Target (i.e. use BFG2Target), however the argument
    -PARAM UseTarget <MyName> will call the module <MyName>.
-->

<xsl:param name="UseTarget" select="'BFG2Target'"/>
<xsl:param name="UseInPlace" select="'BFG2InPlace'"/>

<xsl:template match="Comms">
 <call name="initComms"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment//loop">
  <assign lhs="its{position()}" rhs="0"/>
 </xsl:for-each>
 <xsl:apply-templates select="*"/>
 <call name="finaliseComms"/>
</xsl:template>

<xsl:template match="Includes">
 <Includes>
  <xsl:variable name="duid" select="../../@id"/>
  <include name="{concat($UseTarget,../../@id)}"/>
  <!-- RF This include is no longer required
  <xsl:if test="count(document($root/coupled/deployment)/deployment//sequenceUnit)>1">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
    <xsl:if test="position()=$duid">
     <xsl:for-each select=".//model[not(@instance) or @instance=1]">
      <xsl:variable name="modName" select="@name"/>
      <xsl:if test="document($root/coupled/models/model)/definition[name=$modName]/language='f90'">
         <xsl:if test="$commFormsRoot/communicationForms/*[@modelName=$modName and (contains(@form,'get') or contains(@form,'put'))]">
          <include name="{concat($UseInPlace,'_',$modName)}">
           <rename to="{concat('put_',$modName)}" from="put"/>
           <rename to="{concat('get_',$modName)}" from="get"/>
          </include>
         </xsl:if>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
   </xsl:for-each>
  </xsl:if>
  -->
 </Includes>
</xsl:template>
</xsl:stylesheet>
