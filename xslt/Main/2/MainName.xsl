<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
>

<!--
    RF 16th Sept 2005
    Adds fortran main program structure to BFG2 code
    template. Assumes <EP/> exists in
    the code template.

    By default the program is called BFG2Main
    however the argument -PARAM ProgramName <MyName>
    will call the program <MyName>
-->

<xsl:param name="ProgramName" select="'BFG2Main'"/>

<xsl:template match="EP">
 <xsl:variable name="mypos" select="../code/@id"/>
 <EP type="program" name="{$ProgramName}{$mypos}">
  <xsl:apply-templates select="*"/>
 </EP>
</xsl:template>

</xsl:stylesheet>
