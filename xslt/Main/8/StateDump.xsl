<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template match="if[action//call/@modelName]">

<xsl:choose>
<xsl:when test="count(action//call[not(@name = 'setActiveModel' or starts-with(@name,$inRoutine) or starts-with(@name,$outRoutine))]/arg) &gt; 0">

<xsl:variable name="modelCallPos">
 <xsl:choose>
  <xsl:when test="action/timestep">
   <xsl:variable name="modelCallID" select="generate-id(action/timestep/call[not(@name = 'setActiveModel' or starts-with(@name,$inRoutine) or starts-with(@name,$outRoutine))])"/>
    <xsl:for-each select="action/timestep/*">
     <xsl:if test="generate-id(.) = $modelCallID">
      <xsl:value-of select="position()"/>
     </xsl:if>
    </xsl:for-each>
  </xsl:when>
  <xsl:otherwise>
   <xsl:variable name="modelCallID" select="generate-id(action/call[not(@name = 'setActiveModel' or starts-with(@name,$inRoutine) or starts-with(@name,$outRoutine))])"/>
    <xsl:for-each select="action/*">
     <xsl:if test="generate-id(.) = $modelCallID">
      <xsl:value-of select="position()"/>
     </xsl:if>
    </xsl:for-each>
  </xsl:otherwise>
 </xsl:choose>
</xsl:variable>

 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <xsl:choose>
    <xsl:when test="action/timestep">
     <xsl:for-each select="action/timestep/*">
      <xsl:if test="position() &lt; number($modelCallPos)">
       <timestep offset="{../@offset}" variable="{../@variable}">
        <xsl:apply-templates select="."/>
       </timestep>
      </xsl:if>
     </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
     <xsl:for-each select="action/*">
      <xsl:if test="position() &lt; number($modelCallPos)">
       <xsl:apply-templates select="."/>
      </xsl:if>
     </xsl:for-each>
    </xsl:otherwise>
   </xsl:choose>
  </action>
 </if>


 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <xsl:choose>
    <xsl:when test="action/timestep">
     <timestep offset="{action/timestep/@offset}" variable="{action/timestep/@variable}">
      <open><unit>101</unit><file>filename</file></open>
      <write><unit>101</unit><nml><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/><xsl:text>nml</xsl:text></nml></write>
      <close><unit>101</unit></close>
      <call name="flush">
       <arg name="101"/>
      </call>
     </timestep>
    </xsl:when>
    <xsl:otherwise>
     <open><unit>101</unit><file>filename</file></open>
     <write><unit>101</unit><nml><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/><xsl:text>nml</xsl:text></nml></write>
     <close><unit>101</unit></close>
     <call name="flush">
      <arg name="101"/>
     </call>
    </xsl:otherwise>
   </xsl:choose>
  </action>
 </if>

 <xsl:choose>
  <xsl:when test="action/timestep">
    <timestep offset="{action/timestep/@offset}" variable="{action/timestep/@variable}">
    <print><string><xsl:text>before </xsl:text><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/></string><var name="its1"/></print>
    <call name="commsSync"/>
    <call name="commsSync"/>
   </timestep>
  </xsl:when>
  <xsl:otherwise>
   <print><string><xsl:text>before </xsl:text><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/></string><var name="its1"/></print>
   <call name="commsSync"/>
   <call name="commsSync"/>
  </xsl:otherwise>
 </xsl:choose>


 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <xsl:choose>
    <xsl:when test="action/timestep">
     <xsl:for-each select="action/timestep/*">
      <xsl:if test="position() = number($modelCallPos)">
       <timestep offset="{../@offset}" variable="{../@variable}">
        <xsl:apply-templates select="."/>
       </timestep>
      </xsl:if>
     </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
     <xsl:for-each select="action/*">
      <xsl:if test="position() = number($modelCallPos)">
       <xsl:apply-templates select="."/>
      </xsl:if>
     </xsl:for-each>
    </xsl:otherwise>
   </xsl:choose>
  </action>
 </if>



 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <xsl:choose>
    <xsl:when test="action/timestep">
     <timestep offset="{action/timestep/@offset}" variable="{action/timestep/@variable}">
      <open><unit>101</unit><file>filename</file></open>
      <write><unit>101</unit><nml><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/><xsl:text>nml</xsl:text></nml></write>
      <close><unit>101</unit></close>
      <call name="flush">
       <arg name="101"/>
      </call>
     </timestep>
    </xsl:when>
    <xsl:otherwise>
     <open><unit>101</unit><file>filename</file></open>
     <write><unit>101</unit><nml><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/><xsl:text>nml</xsl:text></nml></write>
     <close><unit>101</unit></close>
     <call name="flush">
      <arg name="101"/>
     </call>
    </xsl:otherwise>
   </xsl:choose>
  </action>
 </if>

 <xsl:choose>
  <xsl:when test="action/timestep">
    <timestep offset="{action/timestep/@offset}" variable="{action/timestep/@variable}">
    <print><string><xsl:text>after </xsl:text><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/></string><var name="its1"/></print>
    <call name="commsSync"/>
    <call name="commsSync"/>
   </timestep>
  </xsl:when>
  <xsl:otherwise>
   <print><string><xsl:text>after </xsl:text><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/></string><var name="its1"/></print>
   <call name="commsSync"/>
   <call name="commsSync"/>
  </xsl:otherwise>
 </xsl:choose>



<!--
 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <open><unit>101</unit><file>filename</file></open>
   <write><unit>101</unit><nml><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/><xsl:text>nml</xsl:text></nml></write>
   <close><unit>101</unit></close>
   <call name="flush">
    <arg name="101"/>
   </call>
  </action>
 </if>
 <print><string><xsl:text>after </xsl:text><xsl:value-of select="action//call[@name!='setActiveModel']/@epName"/><xsl:value-of select="action//call[@name!='setActiveModel']/@instance"/></string><var name="its1"/></print>
 <call name="commsSync"/>
 <call name="commsSync"/>
-->

 <if>
  <condition>
   <value><xsl:value-of select="condition/value"/></value>
  </condition>
  <action>
   <xsl:choose>
    <xsl:when test="action/timestep">
     <xsl:for-each select="action/timestep/*">
      <xsl:if test="position() &gt; number($modelCallPos)">
       <timestep offset="{../@offset}" variable="{../@variable}">
        <xsl:apply-templates select="."/>
       </timestep>
      </xsl:if>
     </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
     <xsl:for-each select="action/*">
      <xsl:if test="position() &gt; number($modelCallPos)">
       <xsl:apply-templates select="."/>
      </xsl:if>
     </xsl:for-each>
    </xsl:otherwise>
   </xsl:choose>
  </action>
 </if>


</xsl:when>
<xsl:otherwise>
  <xsl:copy>
    <xsl:copy-of select="@*" />
    <xsl:copy-of select="normalize-space(text())" />
    <xsl:apply-templates select="*"/>
  </xsl:copy>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>
