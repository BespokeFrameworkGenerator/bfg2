<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
    RF est 10th July 2006
    Phase 1.5 (it had to happen!)
    Here we take the control structure generated in the previous phase
    and add an id which specifies which model is active. This is required
    to determine from which model particular puts or gets are coming from
    when more than one model is run in sequence (in a sequence unit).

    We also add in the appropriate model includes

Note, the MatchAll.xsl is used to output any
remaining unchanged template xml

-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="InSequenceIDs.xsl"/>
<xsl:include href="ModelIncludes.xsl"/>
<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
