<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 10th July 2006
    
    add id's to control code so we know which model is active in a
    sequence unit. This is required to determine which puts and gets
    are being called.

-->
<xsl:include href="../../Utils/InSequenceContextID.xsl"/>

<xsl:template match="call[@activate='true']">

    <!-- this call is a model call as only model calls have an activate attribute and this
         model is also active in the current deployment unit -->
    <xsl:variable name="callName" select="@name"/>
    <xsl:variable name="modelName" select="@modelName"/>
    <xsl:variable name="epName" select="@epName"/>
    <xsl:variable name="instance" select="@instance"/>
    <!-- create a unique context id for this entry point -->
    <xsl:variable name="context">
     <xsl:call-template name="InSequenceContextID">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="instance" select="$instance"/>
     </xsl:call-template>
    </xsl:variable>
    <!-- assign the context variable to the current models context -->
    <call name="setActiveModel"><arg value="{$context}"/></call>
    <!-- copy the matched call back in its original form -->
    <call name="{$callName}" modelName="{$modelName}" epName="{$epName}" activate="true" instance="{@instance}"/>
</xsl:template>

</xsl:stylesheet>
