<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 16th Sept 2005
    Adds any required includes (f90 USE) to BFG2 code
    template. Assumes <Includes> exists in the code template.

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.

-->

<xsl:include href="../../Utils/RenamedEP.xsl"/>

<!-- add appropriate "use model" for all f90 models inside Include container -->
<xsl:template match="Includes">
 <xsl:variable name="currentduid" select="ancestor::code/@id"/>
 <Includes>
  <!-- copy any existing includes -->
  <xsl:apply-templates select="*"/>
  <!-- now add in our model -->

  <!-- loop over each f90 model -->
  <xsl:for-each select="document($root/coupled/models/model)/definition[language='f90']">
 
   <!-- what is the current model name -->
   <xsl:variable name="myName" select="name"/>
   <!-- is this model on the current deployment unit? -->
   <xsl:variable name="oncurrentdu" select="boolean(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit[number($currentduid)]//model/@name=$myName)"/>
   <!-- if yes then add the appropriate "use module" statement -->
   <xsl:if test="$oncurrentdu">
    <!-- determine the f90 models module name -->
    <xsl:variable name="moduleName">
      <xsl:choose>
        <!-- module name is explicitly specified -->
        <xsl:when test="languageSpecific/fortran/moduleName">
          <xsl:value-of select="languageSpecific/fortran/moduleName"/>
        </xsl:when>
        <!-- default module name to model name -->
        <xsl:otherwise>
          <xsl:value-of select="$myName"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!-- add appropriate include as output -->
    <include name="{$moduleName}">
    <xsl:variable name="numEPSets" select="count(document($root/coupled/models/model)/definition[name=$myName]/entryPoints)"/>
    <xsl:choose>
    <xsl:when test="$numEPSets=1">
     <!-- rename all my entry points according to our RenamedEP template -->
     <!-- we do this to make each ep name unique for all models -->
     <xsl:for-each select="entryPoints/entryPoint">
      <xsl:variable name="to">
       <xsl:call-template name="RenamedEP">
        <xsl:with-param name="modelName" select="$myName"/>
        <xsl:with-param name="EP" select="@name"/>
       </xsl:call-template>
      </xsl:variable>
      <rename from="{@name}" to="{$to}"/>
     </xsl:for-each>
    </xsl:when>
    <xsl:when test="$numEPSets>1">
      <xsl:message terminate="yes">
        <xsl:text>Oh dear, use association renaming of generic models not yet supported
</xsl:text>
      </xsl:message>
    </xsl:when>
    <xsl:otherwise>
      <!-- $numEPSets=0 so no entrypoints found -->
    </xsl:otherwise>
    </xsl:choose>
   </include>
   </xsl:if>
  </xsl:for-each>
 </Includes>
</xsl:template>

</xsl:stylesheet>
