<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--

-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:param name="outRoutine" select="'put'"/>
<xsl:param name="inRoutine" select="'get'"/>
<xsl:param name="mpivis" select="'false'"/>
<!-- IRH MOD: For OASIS comms profiling - works as with mpivis -->
<xsl:param name="oasisvis" select="'false'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="Allocate.xsl"/>

<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
