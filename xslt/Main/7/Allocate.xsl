<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
 Templates that position allocation and assignment statements based on where variables are used first.
-->
<xsl:include href="../../Utils/DummyVars.xsl"/>
<xsl:include href="../../Utils/SequenceUnitID.xsl"/>

<xsl:template match="call">
 <xsl:variable name="modName" select="@modelName"/>
 <xsl:variable name="epName" select="@epName"/>
 <xsl:variable name="myName" select="@name"/>
 <xsl:variable name="instance" select="@instance"/>
 <xsl:variable name="call" select="."/>
 <xsl:variable name="callName" select="@name"/>

 <!--
 <xsl:message terminate="no">
  <xsl:text> Model name is:  </xsl:text>
  <xsl:value-of select="$modName"/><xsl:text> </xsl:text>
  <xsl:text> Call name is:  </xsl:text>
  <xsl:value-of select="$callName"/><xsl:text> </xsl:text>
 </xsl:message>
 -->

 <!--<xsl:if test="not(@modelName='') and not(ancestor::iterate) and @name!=$outRoutine">-->
 <xsl:if test="not(@modelName='') and substring-before(@name,'_')!=$outRoutine">
  <xsl:for-each select="ancestor::EP/DataDecs/declare/firstRef[@model=$modName and
                @entryPoint=$epName and (not($instance) or @instance=$instance)]">
   <xsl:variable name="myvar">
    <xsl:if test="number(../@ref)">
     <xsl:text>r</xsl:text>
    </xsl:if>
    <xsl:value-of select="../@ref"/>
   </xsl:variable>

   <!--
   <xsl:message terminate="no">
    <xsl:text> Var is:  </xsl:text>
    <xsl:value-of select="$myvar"/><xsl:text> </xsl:text>
   </xsl:message>
   -->

   <xsl:variable name="myref" select="../@ref"/>
   <xsl:if test="$call/arg/@name=$myvar and not($call/preceding-sibling::call[@modelName=$modName and 
         @epName=$epName and (not($instance) or @instance=$instance) and substring-before(@name,'_')=$inRoutine]/arg/@name=$myvar)">
   <xsl:variable name="myduid">
    <xsl:for-each select="document($root/coupled/deployment)//deploymentUnit">
     <xsl:variable name="indu">
      <xsl:for-each select=".//model">
       <xsl:if test="@name=$modName and (not(@instance) or @instance=$instance)">
        <xsl:value-of select="position()"/>
       </xsl:if>
      </xsl:for-each>
     </xsl:variable>
     <xsl:if test="number($indu)">
      <xsl:value-of select="position()"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:variable>
   <xsl:if test="ancestor::code/@id=$myduid">
    <if>
     <condition>
      <not>
       <value>allocated(<xsl:value-of select="$myvar"/>)</value>
      </not>
     </condition>
     <action>
        <allocate name="{$myvar}">
          <!--<xsl:variable name="order" select="@order"/>-->

          <!--CWA We now need to know whether the field being allocated is in a set that contains an 'assumed' field.
              If so, we need to allocate based on the vaules of the integers received especially.-->

          <!--first, choose a model.ep.id.instance that references this variable-->
          <xsl:variable name="AmodelName" select="$commFormsRoot/communicationForms/*[@ref=$myref][1]/@modelName"/>
          <xsl:variable name="AepName"    select="$commFormsRoot/communicationForms/*[@ref=$myref][1]/@epName"/>
          <xsl:variable name="Aid"        select="$commFormsRoot/communicationForms/*[@ref=$myref][1]/@id"/>
          <xsl:variable name="Ainstance"  select="$commFormsRoot/communicationForms/*[@ref=$myref][1]/@instance"/>

          <!--
          <xsl:message terminate="no">
           <xsl:text> A model.ep.id.instance that refs this var: </xsl:text>
           <xsl:value-of select="$AmodelName"/><xsl:text> </xsl:text>
           <xsl:value-of select="$AepName"/><xsl:text> </xsl:text>
           <xsl:value-of select="$Aid"/><xsl:text> </xsl:text>
           <xsl:value-of select="$Ainstance"/>
          </xsl:message>
          -->

          <!--second, find the set to which this belongs-->
          <xsl:variable name="mySet" select="document($root/coupled/composition)//set[field[@modelName=$AmodelName and @epName=$AepName and @id=$Aid]]"/>

          <!--
          <xsl:message terminate="no">
           <xsl:text> set name: </xsl:text><xsl:value-of select="$mySet/@name"/>
          </xsl:message>
          -->

          <!-- third, find out whether any of the fields in this set have assumed sizes -->
          <xsl:variable name="isAssumedAll">
           <xsl:for-each select="$mySet/field">
            <xsl:variable name="setMod" select="@modelName"/>
            <xsl:variable name="setEP" select="@epName"/>
            <xsl:variable name="setid" select="@id"/>
            <!-- IRH: Test was malformed? It didn't exclude intrinsic transforms
            as it was meant to. -->
            <!--
            <xsl:if test="document($root/coupled/models/model)/definition[name=$setMod]//entryPoint[@name=$setEP and language!='intrinsic']/data[@id=$setid]//assumed">
            -->
            <xsl:variable name="fieldDef" select="document($root/coupled/models/model)/definition[name=$setMod]"/>
            <xsl:if test="$fieldDef/language != 'intrinsic'
              and $fieldDef//entryPoint[@name=$setEP]/data[@id=$setid]//assumed">
             <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
            </xsl:if>
           </xsl:for-each>
          </xsl:variable>
          <xsl:variable name="isAssumed" select="substring-before($isAssumedAll,':')"/>

          <!--
          <xsl:message terminate="no">
           <xsl:text> is assumed: </xsl:text><xsl:value-of select="$isAssumed"/>
          </xsl:message>
          -->

          <xsl:choose>
          <!--if at least one field in the set is declared as having an 'assumed' dimension AND 
              this is a receive call then allocate using one
              of the especially received variables beginning with 'i'-->
          <xsl:when test="$isAssumed!='' and contains($callName,$inRoutine)">
           <xsl:variable name="assumedMod" select="$mySet/field[number($isAssumed)]/@modelName"/>
           <xsl:variable name="assumedEP" select="$mySet/field[number($isAssumed)]/@epName"/>
           <xsl:variable name="assumedid" select="$mySet/field[number($isAssumed)]/@id"/>

          <!--
          <xsl:message terminate="no">
           <xsl:text> assumed model.ep.id: </xsl:text>
           <xsl:value-of select="$assumedMod"/><xsl:text> </xsl:text>
           <xsl:value-of select="$assumedEP"/><xsl:text> </xsl:text>
           <xsl:value-of select="$assumedid"/>
          </xsl:message>
          -->

           <xsl:for-each select="document($root/coupled/models/model)/definition[name=$assumedMod]//entryPoints[1]
                                /entryPoint[@name=$assumedEP]/data[@id=$assumedid]/dim">
            <xsl:choose>
             <xsl:when test=".//assumed">
              <xsl:variable name="dummyRef">
               <xsl:call-template name="getAssumedDummyRef">
                <xsl:with-param name="assumed" select=".//assumed"/>
               </xsl:call-template>
              </xsl:variable>
              <dim order="{@value}">
               <size>
                <variable name="i{$dummyRef + $maxTag}"/>
               </size>
              </dim>
             </xsl:when>
             <xsl:otherwise>
              <dim order="{@value}">
               <xsl:apply-templates select="*"/>
              </dim>
             </xsl:otherwise>
            </xsl:choose>

           </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
           <xsl:for-each select="ancestor::EP/DataDecs/declare[@ref=$myref]/
                       firstRef[@model=$modName and @entryPoint=$epName and (not($instance) or @instance=$instance)]/../dim">
            <xsl:apply-templates select="."/>
           </xsl:for-each>
          </xsl:otherwise>
          </xsl:choose>
        </allocate>
       <xsl:if test="ancestor::EP/Content/InitValues/var/@ref=$myref">
        <assign lhs="{$myvar}" rhs="{ancestor::EP/Content/InitValues/var[@ref=$myref]/@value}"/>
       </xsl:if>
     </action>
    </if>
   </xsl:if>
   </xsl:if>
  </xsl:for-each>
 </xsl:if>
 <xsl:if test="not(@activate) or @activate='' or @activate='true'">
  <!-- only keep if activate is set for this model or I am another type of call -->
  <!--<call name="{@name}" modelName="{@modelName}" epName="{@epName}"/>-->
  <xsl:if test="not(@modelName='') and substring-before(@name,'_')!=$outRoutine and substring-before(@name,'_')!=$inRoutine">
   <xsl:if test="$mpivis!='false'">
    <call name="start_mpivis_log"><arg name="'{$modName} {$epName} {$instance}'"/></call>
   </xsl:if>
   <!-- IRH MOD: Similar profiling functionality for OASIS targets. -->
   <xsl:if test="$oasisvis!='false'">
    <xsl:variable name="bfgSUID">
      <xsl:call-template name="getSequenceUnitID">
        <xsl:with-param name="modelName" select="$modName"/>
        <xsl:with-param name="modelInstance" select="$instance"/>
      </xsl:call-template>
    </xsl:variable>
    <call name="start_oasisvis_log">
      <arg value="{$modName} {$epName} {$instance}" type="string-literal"/>
      <arg value="{$bfgSUID}"/>
    </call>
   </xsl:if>
  </xsl:if>
  <xsl:copy>
    <xsl:copy-of select="@*" />
    <xsl:copy-of select="normalize-space(text())" />
    <xsl:apply-templates select="*"/>
  </xsl:copy>
  <xsl:if test="not(@modelName='') and substring-before(@name,'_')!=$outRoutine and substring-before(@name,'_')!=$inRoutine">
   <xsl:if test="$mpivis!='false'">
    <call name="end_mpivis_log"><arg name="'{$modName} {$epName} {$instance}'"/></call>
   </xsl:if>
   <!-- IRH MOD: Similar profiling functionality for OASIS targets. -->
   <xsl:if test="$oasisvis!='false'">
    <xsl:variable name="bfgSUID">
      <xsl:call-template name="getSequenceUnitID">
        <xsl:with-param name="modelName" select="$modName"/>
        <xsl:with-param name="modelInstance" select="$instance"/>
      </xsl:call-template>
    </xsl:variable>
    <call name="end_oasisvis_log">
      <arg value="{$modName} {$epName} {$instance}" type="string-literal"/>
      <arg value="{$bfgSUID}"/>
    </call>
   </xsl:if>
  </xsl:if>
 </xsl:if>
</xsl:template>

<xsl:template match="assumed">
</xsl:template>

<xsl:variable name="maxTag">
 <xsl:for-each select="$commFormsRoot/communicationForms/*">
  <xsl:sort select="@ref" order="descending"/>
  <xsl:if test="position()=1">
   <xsl:value-of select="@ref"/>
  </xsl:if>
 </xsl:for-each>
</xsl:variable>

</xsl:stylesheet>
