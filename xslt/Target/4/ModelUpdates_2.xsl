<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:param name="EPName" select="'BFG2Target'"/>
<xsl:include href="../../Utils/InSequenceContextID.xsl"/>
<xsl:include href="../../Utils/DeploymentUnitID.xsl"/>
<xsl:include href="DeploymentUnits.xsl"/>
<xsl:include href="../../Utils/Loops.xsl"/>
<xsl:include href="../../Utils/SetNotationUtils.xsl"/>

<!-- start simpler selection to avoid cygwin python libxslt errors
     my version of cygwin fails here with
     "Failed to compile predicate"
<xsl:template match="DataDecs[contains(../@name,$EPName)]">
-->
<xsl:template match="DataDecs">
<xsl:choose>
<xsl:when test="contains(../@name,$EPName)">
<!-- end simpler selection to avoid cygwin python libxslt errors -->
<DataDecs>
<!-- first copy across any existing declarations -->
<xsl:apply-templates/>

<xsl:variable name="RetValue">
  <xsl:call-template name="SetNotationWithConcurrentExecution"/>
</xsl:variable>
<xsl:choose>
 <xsl:when test="$RetValue='true'">
  <xsl:choose>
   <xsl:when test="contains(../../EP/@name,$EPName)">
<!--    <DataDecs> -->
     <xsl:for-each select="document($root/coupled/deployment)/deployment//loop">
      <declare datatype="integer" name="its{position()}" target="true"/>
      <!--<declare datatype="integer, target" name="its{position()}"/>-->
     </xsl:for-each>
     <!-- IRH: Using new XML pseudocode format for defining and declaring
     derived types (mainly so that they can contain arrays and other derived
     types).  Also added attribute for pointers and pointer targets 
     (applied throughout this file). -->
     <!-- IRH: Added su and bfg_du to this structure. 
     TODO: "du" is badly named - it should be "su_rank". -->
     <derive name="modelInfo">
      <declare datatype="integer" name="bfg_du"/>
      <declare datatype="integer" name="du"/>
      <declare datatype="integer" name="su"/>
      <declare datatype="integer" name="period"/>
      <declare datatype="integer" name="nesting"/>
      <declare datatype="integer" name="bound"/>
      <declare datatype="integer" name="offset"/>
      <declare datatype="integer" name="its" pointer="true"/>
     </derive>
     <!--<declare datatype="derived" name="modelInfo">
      <part datatype="integer" name="du"/>
      <part datatype="integer" name="period"/>
      <part datatype="integer" name="nesting"/>
      <part datatype="integer" name="bound"/>
      <part datatype="integer" name="offset"/>
      <part datatype="integer, pointer" name="its"/>
     </declare>-->
     <declare datatype="modelInfo" dimension="1" name="info" derived="true">
     <!--<declare datatype="type(modelInfo)" dimension="1" name="info">-->
      <dim order="1">
       <lower><value>1</value></lower>
      </dim>
      <dim order="2">
       <upper><value><xsl:value-of select="count(document($root/coupled/deployment)//schedule//model)"/></value></upper>
      </dim>
     </declare>
     <declare datatype="integer" parameter="true" value="32767" name="inf"/>
<!--    </DataDecs> -->
 <!-- share these variables by use association instead of common -->
 <!--
    <Common name="iterations">
     <xsl:for-each select="document($root/coupled/deployment)/deployment//loop">
      <value name="its{position()}"/>
     </xsl:for-each>
    </Common>
 -->
   </xsl:when>
   <xsl:otherwise>
<!--    <DataDecs/> -->
   </xsl:otherwise>
  </xsl:choose>
 </xsl:when>
 <xsl:otherwise>
<!--   <DataDecs> -->
    <xsl:for-each select="document($root/coupled/deployment)/deployment//loop">
     <declare datatype="integer" name="its{position()}" target="true"/>
     <!--<declare datatype="integer, target" name="its{position()}"/>-->
    </xsl:for-each>
<!--   </DataDecs> -->
 </xsl:otherwise>
</xsl:choose>
<!-- start simpler selection to avoid cygwin python libxslt errors -->
</DataDecs>
</xsl:when>
<xsl:otherwise>
<!-- simply copy what we already have -->
<DataDecs>
<xsl:apply-templates/>
</DataDecs>
</xsl:otherwise>
</xsl:choose>
<!-- end simpler selection for cygwin python libxslt errors -->
</xsl:template>

<xsl:template match="Content[../@name='initModelInfo']">
 <Content>
  <!--
  <xsl:for-each select="document($root/coupled/models/model)
                /definition//entryPoint">
  -->
  <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
   <xsl:variable name="modelName" select="@name"/>
   <xsl:variable name="epName" select="@ep"/>
   <xsl:variable name="instance" select="@instance"/>
   <xsl:variable name="timestep" select="@rate"/>
   <xsl:variable name="offset" select="@offset"/>
   <xsl:variable name="myID">
    <xsl:call-template name="InSequenceContextID">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="epName" select="$epName"/>
     <xsl:with-param name="instance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>
   <!-- IRH: Added to get the BFG deployment unit number. -->
   <xsl:variable name="myDU">
    <xsl:call-template name="getDeploymentUnitID">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="modelInstance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>
   <!-- IRH: Changed name from "myDU" to "mySU" -->
   <xsl:variable name="mySU"><!--actually uses sequenceUnit position-->
    <!--
    <xsl:call-template name="get_du_pos">
     <xsl:with-param name="myduid" select="generate-id(document($root/coupled/deployment)
                                   /deployment/deploymentUnits/deploymentUnit[.//model/@name=$modelName])"/>
    </xsl:call-template>
    -->
    <xsl:call-template name="get_su_pos">
     <xsl:with-param name="mysuid" select="generate-id(document($root/coupled/deployment)
                                   /deployment/deploymentUnits/deploymentUnit/sequenceUnit[model[@name=$modelName and (not($instance) or @instance=$instance)]])"/>
    </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="myRate">
    <xsl:choose>
     <xsl:when test="boolean($timestep)">
      <xsl:value-of select="$timestep"/>
     </xsl:when>
     <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]/timestep">
      <xsl:value-of select="document($root/coupled/models/model)/definition[name=$modelName]/timestep"/>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>1</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
   <xsl:variable name="myOffset">
    <xsl:choose>
     <xsl:when test="boolean($offset)">
      <xsl:value-of select="$offset"/>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>0</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
   <xsl:variable name="nesting" select="count(document($root/coupled/deployment)
                                /deployment/schedule//model[@name=$modelName and @ep=$epName and (not($instance) or @instance=$instance)]
                                /ancestor::loop)"/>
   <xsl:variable name="bound">
    <xsl:choose>
     <xsl:when test="document($root/coupled/deployment)
               /deployment/schedule//model[@name=$modelName and @ep=$epName and (not($instance) or @instance=$instance)]/../@niters">
      <xsl:value-of select="document($root/coupled/deployment)
                    /deployment/schedule//model[@name=$modelName and @ep=$epName and (not($instance) or @instance=$instance)]/../@niters"/>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>1</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
   <xsl:variable name="counter">
    <xsl:text>its</xsl:text>
    <xsl:choose>
     <xsl:when test="document($root/coupled/deployment)
               /deployment/schedule//loop[./model[@name=$modelName and @ep=$epName and (not($instance) or @instance=$instance)]]">
      <xsl:call-template name="get_loop_pos">
       <xsl:with-param name="myloopid" select="generate-id(document($root/coupled/deployment)
                                       /deployment/schedule//loop[./model[@name=$modelName and @ep=$epName and (not($instance) or @instance=$instance)]])"/>
      </xsl:call-template>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>0</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
   <!--handling of arrays needs to be more general throughout-->
   <!--<assign lhs="modelInfo(1,{$myID})" rhs="0"/>-->
   <comment>model.ep=<xsl:value-of select="$modelName"/>.<xsl:value-of select="$epName"/>.<xsl:value-of select="$instance"/></comment>
   <assign lhs="info({$myID})%bfg_du" rhs="{$myDU}"/>
   <assign lhs="info({$myID})%du" rhs="b2mmap({$mySU})"/>
   <assign lhs="info({$myID})%su" rhs="{$mySU}"/>
   <xsl:choose>
    <xsl:when test="@rate">
     <assign lhs="info({$myID})%period" rhs="{@rate}"/>
    </xsl:when>
    <xsl:when test="ancestor::iterate and document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/@type='iteration'">
     <assign lhs="info({$myID})%period" rhs="{$myRate}"/>
    </xsl:when>
    <xsl:otherwise>
     <assign lhs="info({$myID})%period" rhs="1"/>
    </xsl:otherwise>
   </xsl:choose>
   <assign lhs="info({$myID})%nesting" rhs="{$nesting}"/>
   <assign lhs="info({$myID})%bound" rhs="{$bound}"/>
   <assign lhs="info({$myID})%offset" rhs="{$myOffset}"/>
   <xsl:choose>
    <xsl:when test="$counter!='its0'">
     <point lhs="info({$myID})%its" rhs="{$counter}"/>
    </xsl:when>
    <xsl:otherwise>
     <call name="nullify" type="function">
      <arg name="info({$myID})%its"/>
     </call>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:for-each>
 </Content>
</xsl:template>

<xsl:template match="DataDecs[../@name='getNext']">
 <DataDecs>
  <declare datatype="integer" allocatable="true" name="newlist" dimension="1">
   <dim order="1">
    <lower><value>1</value></lower>
   </dim>
   <dim order="2">
    <upper><variable ref="newlsize"/></upper>
   </dim>
  </declare>
  <declare datatype="integer" name="its" pointer="true"/>
  <!--<declare datatype="integer, pointer" name="its"/>-->
  <declare datatype="integer" name="i"/>
  <declare datatype="integer" name="newlsize"/>
  <declare datatype="integer" name="currentNesting"/>
  <declare datatype="integer" name="startPoint"/>
  <declare datatype="integer" name="endPoint"/>
  <declare datatype="integer" name="pos"/>
  <declare datatype="integer" name="targetpos"/>
 </DataDecs>
</xsl:template>


<xsl:template match="Content[../@name='getNext']">
 <assign lhs="getNext" rhs="-1"/>
 <iterate start="1" end="lsize" counter="i">
  <if>
   <condition>
    <equals>
     <value>list(i)</value>
     <value>point</value>
    </equals>
   </condition>
   <action>
    <assign lhs="pos" rhs="i"/>
   </action>
  </if>
 </iterate>
 <point lhs="its" rhs="info(list(pos))%its"/>
 <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
 <while>
  <condition>
   <equals>
    <value>getNext</value>
    <value>-1</value>
   </equals>
  </condition>
  <action>
   <call name="findStartPoint" type="function" return="startPoint">
    <arg name="list"/>
    <arg name="lsize"/>
    <arg name="pos"/>
    <arg name="its"/>
    <arg name="currentNesting"/>
   </call>
   <call name="findEndPoint" type="function" return="endPoint">
    <arg name="list"/>
    <arg name="lsize"/>
    <arg name="pos"/>
    <arg name="its"/>
    <arg name="currentNesting"/>
   </call>
   <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
   <allocate name="newlist">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
    <upper><variable ref="newlsize"/></upper>
    </dim>
   </allocate>
   <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
   <assign lhs="targetpos" rhs="1"/>
   <iterate start="1" end="newlsize" counter="i">
    <if>
     <condition>
      <equals>
       <value>point</value>
       <value>newlist(i)</value>
      </equals>
     </condition>
     <action>
      <assign lhs="targetpos" rhs="i"/>
     </action>
    </if>
   </iterate>
   <call name="findNext" type="function" return="getNext">
    <arg name="newlist"/>
    <arg name="newlsize"/>
    <arg name="point"/>
    <arg name="targetpos"/>
   </call>
   <deallocate name="newlist"/>
   <if>
    <condition>
     <equals>
      <value>getNext</value>
      <value>-1</value>
     </equals>
    </condition>
    <action>
     <call name="getNextPos" type="function" return="pos">
      <arg name="list"/>
      <arg name="lsize"/>
      <arg name="pos"/>
     </call>
     <if>
      <condition>
       <equals>
        <value>pos</value>
        <value>-1</value>
       </equals>
      </condition>
      <action>
       <assign lhs="getNext" rhs="-1"/>
       <return/>
      </action>
     </if>
     <point lhs="its" rhs="info(list(pos))%its"/>
     <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
    </action>
   </if>
  </action>
 </while>
</xsl:template>


<xsl:template match="DataDecs[../@name='getLast']">
 <DataDecs>
  <declare datatype="integer" allocatable="true" name="newlist" dimension="1">
   <dim order="1">
    <lower><value>1</value></lower>
   </dim>
   <dim order="2">
    <upper><variable ref="newlsize"/></upper>
   </dim>
  </declare>
  <declare datatype="integer" name="its" pointer="true"/>
  <!--<declare datatype="integer, pointer" name="its"/>-->
  <declare datatype="integer" name="i"/>
  <declare datatype="integer" name="newlsize"/>
  <declare datatype="integer" name="startPoint"/>
  <declare datatype="integer" name="endPoint"/>
  <declare datatype="integer" name="pos"/>
  <declare datatype="integer" name="currentNesting"/>
  <declare datatype="integer" name="targetpos"/>
  <declare datatype="integer" name="currentMin"/>
 </DataDecs>
</xsl:template>

<xsl:template match="Content[../@name='getLast']">
 <Content>
  <assign lhs="getLast" rhs="-1"/>
  <assign lhs="currentMin" rhs="inf"/>
  <iterate start="1" end="lsize" counter="i">
   <if>
    <condition>
     <equals>
      <value>list(i)</value>
      <value>point</value>
     </equals>
    </condition>
    <action>
     <assign lhs="pos" rhs="i"/>
    </action>
   </if>
  </iterate>
  <point lhs="its" rhs="info(list(pos))%its"/>
  <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
  <while>
   <condition>
    <equals>
     <value>getLast</value>
     <value>-1</value>
    </equals>
   </condition>
   <action>
    <call name="findStartPoint" type="function" return="startPoint">
     <arg name="list"/>
     <arg name="lsize"/>
     <arg name="pos"/>
     <arg name="its"/>
     <arg name="currentNesting"/>
    </call>
    <call name="findEndPoint" type="function" return="endPoint">
     <arg name="list"/>
     <arg name="lsize"/>
     <arg name="pos"/>
     <arg name="its"/>
     <arg name="currentNesting"/>
    </call>
    <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
    <allocate name="newlist">
     <dim order="1">
      <lower><value>1</value></lower>
     </dim>
     <dim order="2">
    <upper><variable ref="newlsize"/></upper>
     </dim>
    </allocate>
    <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
    <assign lhs="targetpos" rhs="1"/>
    <iterate start="1" end="newlsize" counter="i">
     <if>
      <condition>
       <equals>
        <value>point</value>
        <value>newlist(i)</value>
       </equals>
      </condition>
      <action>
       <assign lhs="targetpos" rhs="i"/>
      </action>
     </if>
    </iterate>
    <call name="findLast" type="function" return="getLast">
     <arg name="newlist"/>
     <arg name="newlsize"/>
     <arg name="point"/>
     <arg name="targetpos"/>
    </call>
    <deallocate name="newlist"/>
    <if>
     <condition>
      <equals>
       <value>getLast</value>
       <value>-1</value>
      </equals>
     </condition>
     <action>
      <call name="getNextPos" type="function" return="pos">
       <arg name="list"/>
       <arg name="lsize"/>
       <arg name="pos"/>
      </call>
      <if>
       <condition>
        <equals>
         <value>pos</value>
         <value>-1</value>
        </equals>
       </condition>
       <action>
        <assign lhs="getLast" rhs="-1"/>
        <return/>
       </action>
      </if>
      <point lhs="its" rhs="info(list(pos))%its"/>
      <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
     </action>
    </if>
   </action>
  </while>
 </Content>
</xsl:template>



<xsl:template match="DataDecs[../@name='getNextPos']">
 <DataDecs>
  <declare datatype="integer" name="i"/>
 </DataDecs>
</xsl:template>

<xsl:template match="Content[../@name='getNextPos']">
 <Content>
  <iterate start="pos-1" end="1" increment="-1" counter="i">
   <if>
    <condition>
     <and>
      <lessThan>
       <value>info(list(i))%nesting</value>
       <value>info(list(pos))%nesting</value>
      </lessThan>
      <not>
       <value>associated(info(list(i))%its,info(list(pos))%its)</value>
      </not>
     </and>
    </condition>
    <action>
     <assign lhs="getNextPos" rhs="i"/>
     <return/>
    </action>
   </if>
  </iterate>
  <iterate start="pos+1" end="lsize" counter="i">
   <if>
    <condition>
     <and>
      <lessThan>
       <value>info(list(i))%nesting</value>
       <value>info(list(pos))%nesting</value>
      </lessThan>
      <not>
       <value>associated(info(list(i))%its,info(list(pos))%its)</value>
      </not>
     </and>
    </condition>
    <action>
     <assign lhs="getNextPos" rhs="i"/>
     <return/>
    </action>
   </if>
  </iterate>
  <assign lhs="getNextPos" rhs="-1"/>
 </Content>
</xsl:template>

<xsl:template match="DataDecs[../@name='findNext']">
 <DataDecs>
  <declare datatype="integer" name="i"/>
  <declare datatype="integer" name="j"/>
  <declare datatype="integer" name="currentNesting"/>
  <declare datatype="integer" name="previousIts" pointer="true"/>
  <!--<declare datatype="integer, pointer" name="previousIts"/>-->
  <declare datatype="integer" name="startPoint"/>
  <declare datatype="integer" name="endPoint"/>
  <declare datatype="integer" name="newlsize"/>
  <declare datatype="integer" name="nestNext"/>
  <declare datatype="integer" name="currentMin"/>
  <declare datatype="integer" name="remainIters"/>
  <declare datatype="integer" name="waitIters"/>
  <declare datatype="integer" name="its"/>
  <declare datatype="integer" name="newpos"/>
  <declare datatype="integer" name="saveits"/>
  <declare datatype="integer" allocatable="true" name="newlist" dimension="1">
   <dim order="1">
    <lower><value>1</value></lower>
   </dim>
   <dim order="2">
    <upper><variable ref="newlsize"/></upper>
   </dim>
  </declare>
 </DataDecs>
</xsl:template>


<xsl:template match="Content[../@name='findNext']">
 <Content>
  <assign lhs="findNext" rhs="-1"/>
  <assign lhs="currentMin" rhs="inf"/>
  <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
  <if>
   <condition>
    <value>associated(info(list(pos))%its)</value>
   </condition>
   <action>
    <point lhs="previousIts" rhs="info(list(pos))%its"/>
   </action>
  </if>
  <if>
   <condition>
    <notEquals>
     <value>list(pos)</value>
     <value>point</value>
    </notEquals>
   </condition>
   <action>
    <assign lhs="pos" rhs="pos-1"/>
   </action>
  </if>
  <iterate start="1" end="lsize" counter="i">
   <assign lhs="pos" rhs="mod(pos+1,lsize)"/>
   <if>
    <condition>
     <equals>
      <value>pos</value>
      <value>0</value>
     </equals>
    </condition>
    <action>
     <assign lhs="pos" rhs="lsize"/>
    </action>
   </if>
   <if>
    <condition>
     <value>associated(info(list(pos))%its)</value>
    </condition>
    <action>
     <assign lhs="its" rhs="info(list(pos))%its"/>
    </action>
    <else>
     <action>
      <assign lhs="its" rhs="1"/>
     </action>
    </else>
   </if>
   <if>
    <condition>
     <or>
      <equals>
       <value>its</value>
       <value>info(list(pos))%bound + 1</value>
      </equals>
      <equals>
       <value>its</value>
       <value>0</value>
      </equals>
     </or>
    </condition>
    <action>
     <assign lhs="its" rhs="1"/>
    </action>
   </if>
   <if>
    <condition>
     <greaterThan>
      <value>list(pos)</value>
      <value>point</value>
     </greaterThan>
    </condition>
    <action>
     <assign lhs="its" rhs="its - 1"/>
    </action>
   </if>
   <if>
    <condition>
     <greaterThan>
      <value>info(list(pos))%nesting</value>
      <value>currentNesting</value>
     </greaterThan>
    </condition>
    <action>
     <call name="findStartPoint" type="function" return="startPoint">
      <arg name="list"/>
      <arg name="lsize"/>
      <arg name="pos"/>
      <arg name="info(list(pos))%its"/>
      <arg name="info(list(pos))%nesting"/>
     </call>
     <call name="findEndPoint" type="function" return="endPoint">
      <arg name="list"/>
      <arg name="lsize"/>
      <arg name="pos"/>
      <arg name="info(list(pos))%its"/>
      <arg name="info(list(pos))%nesting"/>
     </call>
     <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
     <allocate name="newlist">
      <dim order="1">
       <lower><value>1</value></lower>
      </dim>
      <dim order="2">
    <upper><variable ref="newlsize"/></upper>
      </dim>
     </allocate>
     <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
     <assign lhs="newpos" rhs="1"/>
     <iterate start="1" end="newlsize" counter="j">
      <if>
       <condition>
        <equals>
         <value>list(pos)</value>
         <value>newlist(j)</value>
        </equals>
       </condition>
       <action>
        <assign lhs="newpos" rhs="j"/>
       </action>
      </if>
     </iterate>
     <assign lhs="saveits" rhs="info(list(pos))%its"/>
     <if>
      <condition>
       <greaterThan>
        <value>info(list(pos))%its</value>
        <value>0</value>
       </greaterThan>
      </condition>
      <action>
       <assign lhs="info(list(pos))%its" rhs="info(list(pos))%bound+1"/>
      </action>
     </if>
     <call name="findNext" type="function" return="nestNext">
      <arg name="newlist"/>
      <arg name="newlsize"/>
      <arg name="point"/>
      <arg name="newpos"/>
     </call>
     <assign lhs="info(list(pos))%its" rhs="saveits"/>
     <if>
      <condition>
       <notEquals>
        <value>nestNext</value>
        <value>-1</value>
       </notEquals>
      </condition>
      <action>
       <assign lhs="findNext" rhs="nestNext"/>
       <return/>
      </action>
     </if>
     <deallocate name="newlist"/>
    </action>
    <elseif>
     <condition>
      <and>
       <value>associated(info(list(pos))%its)</value>
       <not>
        <value>associated(previousIts,info(list(pos))%its)</value>
       </not>
      </and>
     </condition>
     <action>
      <if>
       <condition>
        <notEquals>
         <value>findNext</value>
         <value>-1</value>
        </notEquals>
       </condition>
       <action>
        <return/>
       </action>
      </if>
      <call name="findStartPoint" type="function" return="startPoint">
       <arg name="list"/>
       <arg name="lsize"/>
       <arg name="pos"/>
       <arg name="info(list(pos))%its"/>
       <arg name="info(list(pos))%nesting"/>
      </call>
      <call name="findEndPoint" type="function" return="endPoint">
       <arg name="list"/>
       <arg name="lsize"/>
       <arg name="pos"/>
       <arg name="info(list(pos))%its"/>
       <arg name="info(list(pos))%nesting"/>
      </call>
      <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
      <allocate name="newlist">
       <dim order="1">
        <lower><value>1</value></lower>
       </dim>
       <dim order="2">
    <upper><variable ref="newlsize"/></upper>
       </dim>
      </allocate>
      <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
      <assign lhs="newpos" rhs="1"/>
      <iterate start="1" end="newlsize" counter="j">
       <if>
        <condition>
         <equals>
          <value>list(pos)</value>
          <value>newlist(j)</value>
         </equals>
        </condition>
        <action>
         <assign lhs="newpos" rhs="j"/>
        </action>
       </if>
      </iterate>
      <call name="findNext" type="function" return="nestNext">
       <arg name="newlist"/>
       <arg name="newlsize"/>
       <arg name="point"/>
       <arg name="newpos"/>
      </call>
      <if>
       <condition>
        <notEquals>
         <value>nestNext</value>
         <value>-1</value>
        </notEquals>
       </condition>
       <action>
        <assign lhs="findNext" rhs="nestNext"/>
        <return/>
       </action>
      </if>
      <deallocate name="newlist"/>
     </action>
    </elseif>
    <else>
     <action>
      <assign lhs="remainIters" rhs="info(list(pos))%bound - its"/>
      <if>
       <condition>
        <greaterThan>
         <value>remainIters</value>
         <value>0</value>
        </greaterThan>
       </condition>
       <action>
        <!--<assign lhs="waitIters" rhs="info(list(pos))%period - mod(its,info(list(pos))%period)"/>-->
        <if>
         <condition>
          <lessThan>
           <plus>
            <value>its</value>
            <value>1</value>
           </plus>
            <value>info(list(pos))%offset</value>
          </lessThan>
         </condition>
         <action>
          <assign lhs="waitIters" rhs="info(list(pos))%offset - its"/>
         </action>
         <elseif>
          <condition>
           <equals>
            <plus>
             <value>its</value>
             <value>1</value>
            </plus>
            <value>info(list(pos))%offset</value>
           </equals>
          </condition>
          <action>
           <assign lhs="waitIters" rhs="1"/>
          </action>
         </elseif>
         <else>
          <action>
           <assign lhs="waitIters" rhs="info(list(pos))%period - mod(its - info(list(pos))%offset,info(list(pos))%period)"/>
          </action>
         </else>
        </if>
        <if>
         <condition>
          <equals>
           <value>waitIters</value>
           <value>1</value>
          </equals>
         </condition>
         <action>
          <assign lhs="findNext" rhs="list(pos)"/>
          <return/>
         </action>
         <elseif>
          <condition>
           <and>
            <lessThan>
             <value>waitIters</value>
             <value>currentMin</value>
            </lessThan>
            <lessThanOrEqual>
             <value>waitIters</value>
             <value>remainIters</value>
            </lessThanOrEqual>
           </and>
          </condition>
          <action>
           <assign lhs="findNext" rhs="list(pos)"/>
           <assign lhs="currentMin" rhs="waitIters"/>
          </action>
         </elseif>
        </if>
       </action>
      </if>
     </action>
    </else>
   </if>
   <!--<point lhs="previousIts" rhs="info(list(pos))%its"/>-->
  </iterate>
 </Content>
</xsl:template>

<xsl:template match="DataDecs[../@name='findLast']">
 <DataDecs>
  <declare datatype="integer" name="i"/>
  <declare datatype="integer" name="j"/>
  <declare datatype="integer" name="currentNesting"/>
  <declare datatype="integer" name="previousIts" pointer="true"/>
  <!--<declare datatype="integer, pointer" name="previousIts"/>-->
  <declare datatype="integer" name="startPoint"/>
  <declare datatype="integer" name="endPoint"/>
  <declare datatype="integer" name="newlsize"/>
  <declare datatype="integer" name="nestLast"/>
  <declare datatype="integer" name="currentMin"/>
  <declare datatype="integer" name="elapsedIters"/>
  <declare datatype="integer" name="its"/>
  <declare datatype="integer" name="newpos"/>
  <declare datatype="integer" name="saveits"/>
  <declare datatype="integer" allocatable="true" name="newlist" dimension="1">
   <dim order="1">
    <lower><value>1</value></lower>
   </dim>
   <dim order="2">
    <upper><variable ref="newlsize"/></upper>
   </dim>
  </declare>
 </DataDecs>
</xsl:template>

<xsl:template match="Content[../@name='findLast']">
 <Content>
  <assign lhs="findLast" rhs="-1"/>
  <assign lhs="currentMin" rhs="inf"/>
  <assign lhs="currentNesting" rhs="info(list(pos))%nesting"/>
  <if>
   <condition>
    <value>associated(info(list(pos))%its)</value>
   </condition>
   <action>
    <point lhs="previousIts" rhs="info(list(pos))%its"/>
   </action>
  </if>
  <if>
   <condition>
    <notEquals>
     <value>list(pos)</value>
     <value>point</value>
    </notEquals>
   </condition>
   <action>
    <assign lhs="pos" rhs="pos+1"/>
   </action>
  </if>
  <iterate start="1" end="lsize" counter="i">
   <assign lhs="pos" rhs="mod(pos-1,lsize)"/>
   <if>
    <condition>
     <equals>
      <value>pos</value>
      <value>0</value>
     </equals>
    </condition>
    <action>
     <assign lhs="pos" rhs="lsize"/>
    </action>
   </if>
   <if>
    <condition>
     <value>associated(info(list(pos))%its)</value>
    </condition>
    <action>
     <assign lhs="its" rhs="info(list(pos))%its"/>
    </action>
    <else>
     <action>
      <assign lhs="its" rhs="1"/>
     </action>
    </else>
   </if>
   <if>
    <condition>
     <equals>
      <value>its</value>
      <value>info(list(pos))%bound + 1</value>
     </equals>
    </condition>
    <action>
     <assign lhs="its" rhs="info(list(pos))%bound"/>
    </action>
   </if>
   <if>
    <condition>
     <greaterThanOrEqual>
      <value>list(pos)</value>
      <value>point</value>
     </greaterThanOrEqual>
    </condition>
    <action>
     <assign lhs="its" rhs="its - 1"/>
    </action>
   </if>
   <if>
    <condition>
     <and>
      <not>
       <value>associated(info(list(pos))%its)</value>
      </not>
      <bracket>
       <or>
        <bracket>
         <and>
          <equals>
           <value>info(point)%nesting</value>
           <value>1</value>
          </equals>
          <greaterThan>
           <value>its1</value>
           <value>info(point)%period</value>
          </greaterThan>
         </and>
        </bracket>
        <bracket>
         <and>
          <greaterThan>
           <value>info(point)%nesting</value>
           <value>1</value>
          </greaterThan>
          <greaterThan>
           <value>its1</value>
           <value>1</value>
          </greaterThan>
         </and>
        </bracket>
       </or>
      </bracket>
     </and>
    </condition>
    <action>
     <continue/>
    </action>
    <elseif>
     <condition>
      <greaterThan>
       <value>info(list(pos))%nesting</value>
       <value>currentNesting</value>
      </greaterThan>
     </condition>
     <action>
      <call name="findStartPoint" type="function" return="startPoint">
       <arg name="list"/>
       <arg name="lsize"/>
       <arg name="pos"/>
       <arg name="info(list(pos))%its"/>
       <arg name="info(list(pos))%nesting"/>
      </call>
      <call name="findEndPoint" type="function" return="endPoint">
       <arg name="list"/>
       <arg name="lsize"/>
       <arg name="pos"/>
       <arg name="info(list(pos))%its"/>
       <arg name="info(list(pos))%nesting"/>
      </call>
      <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
      <allocate name="newlist">
       <dim order="1">
        <lower><value>1</value></lower>
       </dim>
       <dim order="2">
    <upper><variable ref="newlsize"/></upper>
       </dim>
      </allocate>
      <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
      <assign lhs="newpos" rhs="1"/>
      <iterate start="1" end="newlsize" counter="j">
       <if>
        <condition>
         <equals>
          <value>list(pos)</value>
          <value>newlist(j)</value>
         </equals>
        </condition>
        <action>
         <assign lhs="newpos" rhs="j"/>
        </action>
       </if>
      </iterate>
      <assign lhs="saveits" rhs="info(list(pos))%its"/>
      <if>
       <condition>
        <greaterThan>
         <value>info(list(pos))%its</value>
         <value>0</value>
        </greaterThan>
       </condition>
       <action>
        <assign lhs="info(list(pos))%its" rhs="info(list(pos))%bound+1"/>
       </action>
      </if>
      <call name="findLast" type="function" return="nestLast">
       <arg name="newlist"/>
       <arg name="newlsize"/>
       <arg name="point"/>
       <arg name="newpos"/>
      </call>
      <assign lhs="info(list(pos))%its" rhs="saveits"/>
      <if>
       <condition>
        <notEquals>
         <value>nestLast</value>
         <value>-1</value>
        </notEquals>
       </condition>
       <action>
        <assign lhs="findLast" rhs="nestLast"/>
        <return/>
       </action>
      </if>
      <deallocate name="newlist"/>
     </action>
     <elseif>
      <condition>
       <and>
        <value>associated(info(list(pos))%its)</value>
        <not>
         <value>associated(previousIts,info(list(pos))%its)</value>
        </not>
       </and>
      </condition>
      <action>
       <if>
        <condition>
         <notEquals>
          <value>findLast</value>
          <value>-1</value>
         </notEquals>
        </condition>
        <action>
         <return/>
        </action>
       </if>
       <call name="findStartPoint" type="function" return="startPoint">
        <arg name="list"/>
        <arg name="lsize"/>
        <arg name="pos"/>
        <arg name="info(list(pos))%its"/>
        <arg name="info(list(pos))%nesting"/>
       </call>
       <call name="findEndPoint" type="function" return="endPoint">
        <arg name="list"/>
        <arg name="lsize"/>
        <arg name="pos"/>
        <arg name="info(list(pos))%its"/>
        <arg name="info(list(pos))%nesting"/>
       </call>
       <assign lhs="newlsize" rhs="endPoint - startPoint + 1"/>
       <allocate name="newlist">
        <dim order="1">
         <lower><value>1</value></lower>
        </dim>
        <dim order="2">
    <upper><variable ref="newlsize"/></upper>
        </dim>
       </allocate>
       <assign lhs="newlist(1:newlsize)" rhs="list(startPoint:endPoint)"/>
       <assign lhs="newpos" rhs="1"/>
       <iterate start="1" end="newlsize" counter="j">
        <if>
         <condition>
          <equals>
           <value>list(pos)</value>
           <value>newlist(j)</value>
          </equals>
         </condition>
         <action>
          <assign lhs="newpos" rhs="j"/>
         </action>
        </if>
       </iterate>
       <call name="findLast" type="function" return="nestLast">
        <arg name="newlist"/>
        <arg name="newlsize"/>
        <arg name="point"/>
        <arg name="newpos"/>
       </call>
       <if>
        <condition>
         <notEquals>
          <value>nestLast</value>
          <value>-1</value>
         </notEquals>
        </condition>
        <action>
         <assign lhs="findLast" rhs="nestLast"/>
         <return/>
        </action>
       </if>
       <deallocate name="newlist"/>
      </action>
     </elseif>
     <else>
      <action>
       <if>
        <condition>
         <and>
          <greaterThan>
           <value>its</value>
           <value>0</value>
          </greaterThan>
          <greaterThanOrEqual>
           <value>its</value>
           <value>info(list(pos))%offset</value>
          </greaterThanOrEqual>
         </and>
        </condition>
        <action>
         <assign lhs="elapsedIters" rhs="mod(its - info(list(pos))%offset,info(list(pos))%period)"/>
         <if>
          <condition>
           <equals>
            <value>elapsedIters</value>
            <value>0</value>
           </equals>
          </condition>
          <action>
           <assign lhs="findLast" rhs="list(pos)"/>
           <return/>
          </action>
          <elseif>
           <condition>
            <and>
             <lessThan>
              <value>elapsedIters</value>
              <value>currentMin</value>
             </lessThan>
             <lessThan>
              <value>elapsedIters</value>
              <value>its</value>
             </lessThan>
            </and>
           </condition>
           <action>
            <assign lhs="findLast" rhs="list(pos)"/>
            <assign lhs="currentMin" rhs="elapsedIters"/>
           </action>
          </elseif>
         </if>
        </action>
       </if>
      </action>
     </else>
    </elseif>
   </if>
   <!--<point lhs="previousIts" rhs="info(list(pos))%its"/>-->
  </iterate>
 </Content>
</xsl:template>

<xsl:template match="DataDecs[../@name='findStartPoint']">
 <DataDecs>
  <declare datatype="integer" name="i"/>
 </DataDecs>
</xsl:template>

<xsl:template match="Content[../@name='findStartPoint']">
 <Content>
  <if>
  <condition>
   <not>
    <value>associated(its)</value>
   </not>
  </condition>
  <action>
   <assign lhs="findStartPoint" rhs="1"/>
   <return/>
  </action>
  </if>
  <iterate start="pos-1" end="1" increment="-1" counter="i">
   <if>
    <condition>
     <and>
      <lessThanOrEqual>
       <value>info(list(i))%nesting</value>
       <value>nesting</value>
      </lessThanOrEqual>
      <not>
       <value>associated(info(list(i))%its,its)</value>
      </not>
     </and>
    </condition>
    <action>
     <assign lhs="findStartPoint" rhs="i + 1"/>
     <return/>
    </action>
   </if>
  </iterate>
  <assign lhs="findStartPoint" rhs="1"/>
 </Content>
</xsl:template>

<xsl:template match="DataDecs[../@name='findEndPoint']">
 <DataDecs>
  <declare datatype="integer" name="i"/>
 </DataDecs>
</xsl:template>

<xsl:template match="Content[../@name='findEndPoint']">
 <Content>
  <if>
  <condition>
   <not>
    <value>associated(its)</value>
   </not>
  </condition>
  <action>
   <assign lhs="findEndPoint" rhs="lsize"/>
   <return/>
  </action>
  </if>
  <iterate start="pos+1" end="lsize" counter="i">
   <if>
    <condition>
     <and>
      <lessThanOrEqual>
       <value>info(list(i))%nesting</value>
       <value>nesting</value>
      </lessThanOrEqual>
      <not>
       <value>associated(info(list(i))%its,its)</value>
      </not>
     </and>
    </condition>
    <action>
     <assign lhs="findEndPoint" rhs="i - 1"/>
     <return/>
    </action>
   </if>
  </iterate>
  <assign lhs="findEndPoint" rhs="lsize"/>
 </Content>
</xsl:template>

</xsl:stylesheet>
