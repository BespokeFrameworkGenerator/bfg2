<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template name="get_du_pos">
 <xsl:param name="myduid"/>
 <xsl:variable name="root" select="document($CoupledDocument)"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
  <xsl:if test="generate-id(.)=$myduid">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template name="get_su_pos">
 <xsl:param name="mysuid"/>
 <xsl:variable name="root" select="document($CoupledDocument)"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits//sequenceUnit">
  <xsl:if test="generate-id(.)=$mysuid">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
