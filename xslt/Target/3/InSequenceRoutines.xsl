<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 13th July 2006
    
    add functions/subroutines to store and provide id's which determine
    which model is active in a particular sequence unit. This is required
    to determine which puts and gets are being called.

-->

<!-- make sure we only match the required DataDecs as there are potentially others within nested EP's -->
<xsl:template match="code/EP/DataDecs">
  <!-- add required model context variable -->
  <DataDecs>
  <xsl:apply-templates/>
    <!-- HACK, just put in 10 until we add a template to cound the number of loops and convergences in the schedule -->
    <xsl:variable name="NLoops" select="'10'"/>
    <xsl:variable name="NConverge" select="'10'"/>
    <declare name="activeModelID" datatype="integer"/>
    <declare name="firstFixedIteration" datatype="logical">
      <dim order="1"><size><value><xsl:value-of select="$NLoops"/></value></size></dim>
    </declare>
    <declare name="lastFixedIteration" datatype="logical">
      <dim order="1"><size><value><xsl:value-of select="$NLoops"/></value></size></dim>
    </declare>
    <declare name="firstConvergeIteration" datatype="logical">
      <dim order="1"><size><value><xsl:value-of select="$NConverge"/></value></size></dim>
    </declare>
    <declare name="firstConvergeControl" datatype="logical">
      <dim order="1"><size><value><xsl:value-of select="$NConverge"/></value></size></dim>
    </declare>
    <declare name="convergeCount" datatype="integer">
      <dim order="1"><size><value><xsl:value-of select="$NConverge"/></value></size></dim>
    </declare>
  </DataDecs>

</xsl:template>

<!-- match separator and add my functions just after it.
     Should really just add into something like <eps></eps>
     but its not implemented like that - should be tidied
     up one day -->

<xsl:template match="Separator">

  <Separator/>
  <comment>in sequence support routines start</comment>
  <EP type="procedure" name="setActiveModel">
    <arg name="idIN" type="integer" rank="0" intent="in"/>
    <assign lhs="activeModelID" rhs="idIN"/>
  </EP>
  <EP type="function" return="integer" name="getActiveModel">
    <assign lhs="getActiveModel" rhs="activeModelID"/>
  </EP>
  <EP type="procedure" name="setLoopInfo">
    <arg name="id" type="integer" rank="0" intent="in"/>
    <arg name="start" type="integer" rank="0" intent="in"/>
    <arg name="end" type="integer" rank="0" intent="in"/>
    <arg name="current" type="integer" rank="0" intent="in"/>
    <assign lhs="firstFixedIteration(id)" rhs=".false."/>
    <if>
      <condition>
        <equals>
          <value>current</value>
          <value>start</value>
        </equals>
      </condition>
      <action>
        <assign lhs="firstFixedIteration(id)" rhs=".true."/>
      </action>
    </if>
    <assign lhs="lastFixedIteration(id)" rhs=".false."/>
    <if>
      <condition>
        <equals>
          <value>current</value>
          <value>end</value>
        </equals>
      </condition>
      <action>
        <assign lhs="lastFixedIteration(id)" rhs=".true."/>
      </action>
    </if>
  </EP>
  <EP type="procedure" name="setConvergeInfo">
    <arg name="id" type="integer" rank="0" intent="in"/>
    <assign lhs="firstConvergeIteration(id)" rhs=".false."/>
    <if>
      <condition>
        <equals>
          <value>convergeCount(id)</value>
          <value>0</value>
        </equals>
      </condition>
      <action>
        <assign lhs="firstConvergeIteration(id)" rhs=".true."/>
        <assign lhs="convergeCount(id)" rhs="convergeCount(id)+1"/>
      </action>
    </if>
  </EP>
  <EP type="procedure" name="setfirstConvergeControl">
    <arg name="id" type="integer" rank="0" intent="in"/>
    <assign lhs="firstConvergeControl(id)" rhs=".true."/>
  </EP>
  <EP type="procedure" name="unsetfirstConvergeControl">
    <arg name="id" type="integer" rank="0" intent="in"/>
    <assign lhs="firstConvergeControl(id)" rhs=".false."/>
  </EP>
  <comment>in sequence support routines end</comment>
  <xsl:apply-templates/>

</xsl:template>

</xsl:stylesheet>
