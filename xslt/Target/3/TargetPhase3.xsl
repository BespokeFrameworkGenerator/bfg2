<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 13th July 2006
    Phase 1.5 of applying xsl templates to target code template
    Adds in the routines which store and provide information on
    which model is currently running (needed for in-sequence models)
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:include href="InSequenceRoutines.xsl"/>
<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
