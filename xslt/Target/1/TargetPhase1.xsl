<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
    RF est 8th Dec 2005
    Phase 0 of creating code. Input is an xml code template,
    output is a more specific xml code template.

    Phase 0 responsibility is
    1: create appropriate number of code elements (one for each du)
    2: add structure to the content of the template

Note, the MatchAll.xsl is used to output any
remaining unchanged template xml

-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="mpivis" select="'false'"/>
<!-- IRH MOD: Similar profiling functionality for OASIS. -->
<xsl:param name="oasisvis" select="'false'"/>
<xsl:param name="DUIndex" select="'false'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<xsl:template match="code">
 <xsl:variable name="children" select="*"/>
 <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
  <xsl:if test="$DUIndex=position() or $DUIndex='false'">
    <code id="{position()}">
     <xsl:apply-templates select="$children"/>
    </code>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template match="Content">
 <Content>
  <Separator/>
  <Threading/>
  <!-- IRH MOD: Similar profiling functionality for OASIS. -->
  <xsl:if test="$mpivis!='false' or $oasisvis!='false'">
   <Logging/>
  </xsl:if>
  <Comms>
   <InitComms/>
   <FinaliseComms/>
  </Comms>
  <Updates/>
 </Content>
</xsl:template>

<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
