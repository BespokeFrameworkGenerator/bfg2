<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 20th Sept 2005

-->
<xsl:include href="../../Utils/targets.xsl"/>

<xsl:template match="code/EP/DataDecs">
  <!-- IRH: Add the target include at module level (required for OASIS4) -->
  <xsl:call-template name="targetIncludes"/>
  <!-- add required module variable -->
  <DataDecs>
  <xsl:apply-templates/>
  <declare name="bfgSUID" datatype="integer"/>
  <!--
  <xsl:if test="document($root/coupled/deployment)/deployment/target='mpi'">
  -->
    <xsl:variable name="TotNumSUs">
      <xsl:value-of select="count(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit)"/>
    </xsl:variable>
    <declare name="b2mmap({$TotNumSUs})" datatype="integer"/>
  <!--
  </xsl:if>
  -->
  <!-- IRH: Add any target-comms-type specific (e.g. OASIS4, MPI) 
  module level variable/parameter declarations -->
  <xsl:call-template name="CreateCommsGlobalData"/>
  </DataDecs>
</xsl:template>

<xsl:template match="Comms">
 <EP type="procedure" name="initComms">
 <xsl:call-template name="InitComms"/>
 </EP>
 <EP type="procedure" name="finaliseComms">
 <xsl:call-template name="FinaliseComms"/>
 </EP>
</xsl:template>

<!--
<xsl:template name="InitComms">
 <EP type="procedure" name="initComms">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:apply-templates select="." mode="$targetType"/>
 </EP>
</xsl:template>

<xsl:template name="FinaliseComms">
 <EP type="procedure" name="finaliseComms">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:variable name="templateName" select="concat($targetType,FinaliseComms)"/>
 <xsl:call-template name="$templateName"/>
 </EP>
</xsl:template>
-->

</xsl:stylesheet>
