<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 20th Sept 2005
    Phase 1 of applying xsl templates to target code template
-->

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:param name="OutDir"/>
<!-- IRH MOD: Similar profiling functionality for OASIS. -->
<xsl:param name="mpivis" select="'false'"/>
<xsl:param name="oasisvis" select="'false'"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>

<!--
<xsl:include href="../../Utils/EPName.xsl"/>
-->
<xsl:template match="EP">
 <EP type="entryPoint" name="{concat('BFG2Target',../@id)}">
  <xsl:apply-templates select="*"/>
 </EP>
</xsl:template>
<xsl:include href="Concurrency.xsl"/>
<xsl:include href="TargetComms.xsl"/>

<!-- <xsl:include href="TargetIncludes.xsl"/> -->

<xsl:include href="ModelUpdates.xsl"/>
<!-- IRH MOD: Similar profiling functionality for OASIS
now in non-target-specific include. -->
<xsl:include href="Vis.xsl"/>
<!-- <xsl:include href="MpiVis.xsl"/> -->
<xsl:include href="../../Utils/MatchAll.xsl"/>

</xsl:stylesheet>
