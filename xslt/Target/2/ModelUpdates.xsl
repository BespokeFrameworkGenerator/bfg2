<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:include href="../../Utils/SetNotationUtils.xsl"/>

<xsl:template match="Updates">
<xsl:variable name="RetValue">
  <xsl:call-template name="SetNotationWithConcurrentExecution"/>
</xsl:variable>
<xsl:if test="$RetValue='true'">
 <Updates>
  <EP type="procedure" name="initModelInfo">
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="getNext">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="point" type="integer" rank="0" intent="in"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="findNext"  recursive="true">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="point" type="integer" rank="0" intent="in"/>
   <arg name="pos" type="integer" rank="0" intent="inout"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="getLast">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="point" type="integer" rank="0" intent="in"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="findLast" recursive="true">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="point" type="integer" rank="0" intent="in"/>
   <arg name="pos" type="integer" rank="0" intent="inout"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="getNextPos">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="pos" type="integer" rank="0" intent="in"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="findStartPoint">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="pos" type="integer" rank="0" intent="in"/>
   <arg name="its" type="integer, pointer" rank="0"/>
   <arg name="nesting" type="integer" rank="0" intent="in"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
  <EP type="function" return="integer" name="findEndPoint">
   <arg name="list" type="integer" rank="1" intent="in">
    <dim order="1">
     <lower><value>1</value></lower>
    </dim>
    <dim order="2">
     <upper><variable ref="lsize"/></upper>
    </dim>
   </arg>
   <arg name="lsize" type="integer" rank="0" intent="in"/>
   <arg name="pos" type="integer" rank="0" intent="in"/>
   <arg name="its" type="integer, pointer" rank="0"/>
   <arg name="nesting" type="integer" rank="0" intent="in"/>
   <Includes/>
   <DataDecs/>
   <Content/>
  </EP>
 </Updates>
</xsl:if>
</xsl:template>

</xsl:stylesheet>
