<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template match="Logging"> 
  <xsl:if test="$oasisvis = 'true'">
    <EP type="procedure" name="start_oasisvis_log">
      <arg intent="in" type="character" name="logid" rank="1"/>
      <arg intent="in" type="integer" name="bfg_suid" rank="0"/>
      <Includes/>
      <DataDecs/>
      <Content>
        <call name="oasisvis_comp">
          <arg value="begin" type="string-literal"/>
          <arg name="logid"/>
          <arg name="bfg_suid"/>
        </call>
      </Content>
    </EP>
    <EP type="procedure" name="end_oasisvis_log">
      <arg intent="in" type="character" name="logid" rank="1"/>
      <arg intent="in" type="integer" name="bfg_suid" rank="0"/>
      <Includes/>
      <DataDecs/>
      <Content>
        <call name="oasisvis_comp">
          <arg value="end" type="string-literal"/>
          <arg name="logid"/>
          <arg name="bfg_suid"/>
        </call>
      </Content>
    </EP>
  </xsl:if>
  <xsl:if test="$mpivis = 'true'">
    <EP type="procedure" name="start_mpivis_log">
      <arg intent="in" type="string" name="logid" rank="0">
        <stringSize order="1">
	  <size> <value>*</value></size>
	</stringSize>
      </arg>
      <Includes/>
      <DataDecs>
        <declare name="globalrank" datatype="integer"/>
        <declare name="ierr" datatype="integer"/>
      </DataDecs>
      <Content>
        <call name="mpi_comm_rank"><arg name="mpi_comm_world"/><arg name="globalrank"/><arg name="ierr"/></call>
        <call name="mpivis_comp"><arg name="'begin'"/><arg name="logid"/><arg name="globalrank"/></call>
      </Content>
    </EP>
    <EP type="procedure" name="end_mpivis_log">
      <arg intent="in" type="string" name="logid" rank="0">
        <stringSize order="1">
	  <size> <value>*</value></size>
	</stringSize>
      </arg>
      <Includes/>
      <DataDecs>
        <declare name="globalrank" datatype="integer"/>
        <declare name="ierr" datatype="integer"/>
      </DataDecs>
      <Content>
        <call name="mpi_comm_rank"><arg name="mpi_comm_world"/><arg name="globalrank"/><arg name="ierr"/></call>
        <call name="mpivis_comp"><arg name="'end'"/><arg name="logid"/><arg name="globalrank"/></call>
      </Content>
    </EP>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
