<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 20th Sept 2005

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    sam directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.
-->

<xsl:include href="../../Utils/ThreadName.xsl"/>

<xsl:template match="Threading"> 
 <comment><xsl:text>concurrency support routines start</xsl:text></comment>
 <xsl:call-template name="Threads"/>
 <xsl:call-template name="Sync"/>
 <comment><xsl:text>concurrency support routines end</xsl:text></comment>
</xsl:template>

<xsl:template name="Threads">
 <xsl:variable name="currentduid" select="ancestor::code/@id"/>
 <xsl:for-each select="document($root/coupled/deployment)//deploymentUnits//model">
  <xsl:variable name="myModelName" select="@name"/>
  <xsl:variable name="instance" select="@instance"/>
   <xsl:variable name="myduid">
     <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit">
       <xsl:if test="sequenceUnit/model[@name=$myModelName and (not($instance) or @instance=$instance)]">
         <xsl:value-of select="position()"/>
       </xsl:if>
     </xsl:for-each>
   </xsl:variable>
  <xsl:if test="$myduid=$currentduid">
  <xsl:variable name="tmpName">
   <xsl:call-template name="ThreadName">
    <xsl:with-param name="modelName" select="$myModelName"/>
    <xsl:with-param name="instance" select="$instance"/>
   </xsl:call-template>
  </xsl:variable>
  <EP type="function" return="logical" name="{$tmpName}">
  <xsl:choose>
<!--   <xsl:when test="document($root/coupled/deployment)/deployment/target='mpi'">-->
   <!--Test whether this is a concurrent deployment-->
   <xsl:when test="count(document($root/coupled/deployment)/deployment//sequenceUnit) > 1">
        <xsl:variable name="bfgSUID">
          <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit">
            <xsl:if test="model[@name=$myModelName and (not($instance) or @instance=$instance)]">
              <xsl:value-of select="position()"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
   <if><condition><equals><value>bfgSUID</value><value><xsl:value-of select="$bfgSUID"/></value></equals></condition>
   <action>
   <assign lhs="{$tmpName}" rhs=".true."/>
   </action>
   <else>
   <action>
   <assign lhs="{$tmpName}" rhs=".false."/>
   </action>
   </else>
   </if>
   </xsl:when>
   <xsl:otherwise>
   <comment>only one thread so always true</comment>
   <assign lhs="{$tmpName}" rhs=".true."/>
   </xsl:otherwise>
  </xsl:choose>
  </EP>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

<xsl:template name="Sync">
 <EP type="procedure" name="commsSync">
  <xsl:call-template name="CommsSyncBody"/>
 </EP>
</xsl:template>

</xsl:stylesheet>
