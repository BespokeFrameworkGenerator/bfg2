<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--rf change variable name algorithm again as there is a
    problem with the previous one. The problem is when
    two sets are primed from the same data. This version
    uses the same name for all members of a set and chooses
    the first declared in the set as the name. Point to
    point and primings all get given their own names. This
    is not optimal but should work in general -->

<xsl:template name="dataidgen">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>

<!--
<xsl:message terminate="no">
     <xsl:text>Looking for: </xsl:text>
     <xsl:value-of select="$modelName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$epName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$id"/>
     <xsl:text>:</xsl:text>
     <xsl:value-of select="$instance"/>
     <xsl:text>:</xsl:text>
</xsl:message>
-->

  <xsl:choose>
   <!--look at set first-->
   <xsl:when test="document($root/coupled/composition)
             /composition/connections/timestepping/set/field
             [./@modelName=$modelName and ./@epName=$epName and ./@id=$id and (not($instance) or ./@instance=$instance)]">

    <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/set/field[@modelName=$modelName and
                  @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/..">

     <xsl:variable name="firstModelName" select="field[1]/@modelName"/>
     <xsl:variable name="firstEpName"    select="field[1]/@epName"/>
     <xsl:variable name="firstId"        select="field[1]/@id"/>
     <xsl:variable name="firstInst"      select="field[1]/@instance"/>

<!--
     <xsl:if test="$modelName = 'goldstein_seaice'">
      <xsl:message terminate="no">
       <xsl:text>%%%%i am goldstein_seaice </xsl:text><xsl:value-of select="$epName"/><xsl:text>.</xsl:text><xsl:value-of select="$id"/>
       <xsl:text> -> first in set: </xsl:text><xsl:value-of select="$firstModelName"/><xsl:text>.</xsl:text><xsl:value-of select="$firstEpName"/><xsl:text>.</xsl:text><xsl:value-of select="$firstId"/><xsl:text>.</xsl:text><xsl:value-of select="$firstInst"/>
      </xsl:message>
     </xsl:if>
-->

     <xsl:variable name="tmp">
      <xsl:call-template name="dataidgenvalue">
       <xsl:with-param name="modelName" select="$firstModelName"/>
       <xsl:with-param name="epName" select="$firstEpName"/>
       <xsl:with-param name="id" select="$firstId"/>
       <xsl:with-param name="instance" select="$firstInst"/>
       <xsl:with-param name="name" select="@name"/>
      </xsl:call-template>
     </xsl:variable>

     <xsl:value-of select="substring-before($tmp,':')"/>



    </xsl:for-each>

   </xsl:when>
   <!--look at p2p connections-->
   <xsl:otherwise>

    <xsl:variable name="tmp">
     <xsl:call-template name="dataidgenvalue">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="epName" select="$epName"/>
       <xsl:with-param name="id" select="$id"/>
     </xsl:call-template>
    </xsl:variable>

    <xsl:value-of select="substring-before($tmp,':')"/>

   </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="dataidgenvalue">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <xsl:param name="name"/>

 <xsl:variable name="max" select="count(document($root/coupled/models/model)
               /definition//entryPoint/data)"/>
 <xsl:variable name="targetref">
 <xsl:for-each select="document($root/coupled/models/model)
               /definition//entryPoint/data">
<!--
<xsl:message terminate="no">
     <xsl:text>checking:</xsl:text>
     <xsl:value-of select="ancestor::definition/name"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="ancestor::entryPoint/@name"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="@id"/>
     <xsl:text>:</xsl:text>
</xsl:message>
-->
  <xsl:if test="@id=$id and normalize-space(ancestor::entryPoint/@name)=$epName and 
          normalize-space(ancestor::definition/name)=$modelName">

<!--
<xsl:message terminate="no">
     <xsl:text>matches</xsl:text>
</xsl:message>
-->

  <xsl:choose>
   <xsl:when test="$instance">
    <xsl:value-of select="$max*($instance - 1)+position()"/><xsl:text>:</xsl:text>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="position()"/><xsl:text>:</xsl:text>
   </xsl:otherwise>
  </xsl:choose>
  </xsl:if>
 </xsl:for-each>
 </xsl:variable>
 <xsl:value-of select="$targetref"/>

<!--
<xsl:message terminate="no">
     <xsl:text>looking for:</xsl:text>
     <xsl:value-of select="$modelName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$epName"/>
     <xsl:text>.</xsl:text>
     <xsl:value-of select="$id"/>
     <xsl:text>:</xsl:text>
     <xsl:value-of select="$instance"/>
     <xsl:text>:</xsl:text>
     <xsl:value-of select="$name"/>
     <xsl:text>:</xsl:text>
     <xsl:value-of select="$targetref"/>
</xsl:message>
-->

</xsl:template>

<xsl:template name="getvarname">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <xsl:variable name="myname">
  <xsl:value-of select="document($root/coupled/composition)
                /composition/connections/timestepping/set[./field
                [./@modelName=$modelName and ./@epName=$epName and ./@id=$id and (not(@instance) or @instance=$instance)]]/@name"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$myname!=''">
   <xsl:value-of select="$myname"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="document($root/coupled/models/model)
             /definition[normalize-space(name)=$modelName]/entryPoints/
             entryPoint[@name=$epName]/data[@id=$id]/@name"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
