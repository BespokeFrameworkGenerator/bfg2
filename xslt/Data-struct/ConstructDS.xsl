<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:variable name="debug" select="'false'"/>
<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:include href="GenVarIDs.xsl"/>
<xsl:include href="../Utils/SequenceUnitID.xsl"/>
<xsl:include href="../Utils/DeploymentUnitID.xsl"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<!--
 CWA 8/8/06
 Preprocessing step that creates an XML representation of 
 what form of communication is required for each model.ep.id tuple.

 ptuple elements indicate the form of communication required in a priming connection;
 tuple elements indicate the form of communication required in a timestepping connection.

 The form attribute of tuple and ptuple elements may be either:
 inplace, argpass, get:argpass, argpass:put or get:argpass:put 
 
 * inplace indicates that an inplace put or get is used in model code.
 * argpass indicates that argument passing communication is used.
 * get:argpass indicates that argument passing communication is used, but 
   the tuple has at least one potential receive-connection with a remote tuple, and therefore
   get must be called before argument passing takes place.
 * argpass:put indicates that argument passing communication is used, but 
   the tuple has at least one potential send-connection with a remote tuple, and therefore
   put must be called after argument passing takes place.
 * get:argpass:put indicates that argument passing communication is used, but 
   the tuple has at least one send-connection and at least one receive-connection 
   with a remote tuple(s), and therefore get must be called before, and put after,
   argument passing takes place.

 Note: works with set notation only at the moment...
-->


<xsl:template match="/">
 <communicationForms>
  <xsl:for-each select="document($root/coupled/models/model)">
  <xsl:if test="/definition/language!='intrinsic'">
   <xsl:variable name="modelName" select="/definition/name"/>
   <xsl:variable name="multi-instance">
    <xsl:choose>
     <xsl:when test="/definition/multiInstanceSupport">
      <xsl:value-of select="'true1'"/>
     </xsl:when>
     <xsl:when test="count(document($root/coupled/deployment)//deploymentUnit//model[@name=$modelName]) > 1">
      <xsl:value-of select="'true2'"/>
     </xsl:when>
    </xsl:choose>
   </xsl:variable>
   <xsl:for-each select="/definition/entryPoints/entryPoint">
    <xsl:variable name="epName" select="@name"/>
    <xsl:for-each select="data">
     <xsl:variable name="id" select="@id"/>

     <DEBUG author="rupert" position="top" modelName="{$modelName}" epName="{$epName}" id="{$id}" multi-instance="{$multi-instance}"/>

     <xsl:call-template name="CoupledArgPass">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="id" select="$id"/>
      <xsl:with-param name="multi-instance" select="$multi-instance"/>
     </xsl:call-template>
     <xsl:call-template name="PrimingArgPass">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="id" select="$id"/>
     </xsl:call-template>
     <xsl:call-template name="UnCoupledArgPass">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="id" select="$id"/>
      <xsl:with-param name="multi-instance" select="$multi-instance"/>
     </xsl:call-template>
    </xsl:for-each>
   </xsl:for-each>
  </xsl:if>
  </xsl:for-each>
 </communicationForms>
</xsl:template>



<xsl:template name="PrimingArgPass">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:variable name="ref">
  <xsl:call-template name="dataidgen">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="document($root/coupled/composition)/composition
            /connections/priming/model[@name=$modelName]
            /entryPoint[@name=$epName]/primed[@id=$id] and 
            document($root/coupled/composition)/composition
            /connections/priming/model/entryPoint/primed
            /model[@name=$modelName and @entryPoint=$epName and @id=$id]">
   <!--tuple is primed and is a primer (inout)-->
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='argpass']">
     <!--if tuple is argpassing, need to do further tests-->
     <xsl:variable name="primers">
      <!--for each primer of current tuple-->
      <xsl:for-each select="document($root/coupled/composition)/composition
                    /connections/priming/model[@name=$modelName]
                    /entryPoint[@name=$epName]/primed[@id=$id]/model">
       <xsl:variable name="cmodelName" select="@name"/>
       <xsl:variable name="cepName" select="@entryPoint"/>
       <xsl:variable name="cid" select="@id"/>
       <xsl:choose>
        <xsl:when test="document($root/coupled/models/model)
                  /definition[name=$cmodelName]/entryPoints
                  /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                  document($root/coupled/deployment)/deployment//sequenceUnit
                  /model[@name=$modelName]/../model[@name=$cmodelName]">
         <xsl:text>argpass</xsl:text>
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>get</xsl:text>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:for-each>
     </xsl:variable>
     <xsl:variable name="primees">
      <!--for each primee of current tuple-->
      <xsl:for-each select="document($root/coupled/composition)
                    /composition/connections/priming/model/entryPoint/primed
                    /model[@name=$modelName and @entryPoint=$epName and @id=$id]">
       <xsl:variable name="cmodelName" select="../../../@name"/>
       <xsl:variable name="cepName" select="../../@name"/>
       <xsl:variable name="cid" select="../@id"/>
       <xsl:choose>
        <xsl:when test="document($root/coupled/models/model)
                  /definition[name=$cmodelName]/entryPoints
                  /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                  document($root/coupled/deployment)/deployment//sequenceUnit
                  /model[@name=$modelName]/../model[@name=$cmodelName]">
         <xsl:text>argpass</xsl:text>
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>put</xsl:text>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:for-each>
     </xsl:variable>
     <!--
     <xsl:message terminate="no">
      <xsl:text> +++++++++inout priming+++++++++++</xsl:text>
      <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$id"/><xsl:text>.</xsl:text>
      <xsl:text>primers=</xsl:text><xsl:value-of select="$primers"/><xsl:text>=</xsl:text>
      <xsl:text>primees=</xsl:text><xsl:value-of select="$primees"/><xsl:text>=</xsl:text>
     </xsl:message>
     -->
     <xsl:choose>
      <xsl:when test="contains($primers,'get') and contains($primees,'put')">
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass:put" ref="{$ref}"/>
      </xsl:when>
      <xsl:when test="contains($primers,'get')">
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass" ref="{$ref}"/>
      </xsl:when>
      <xsl:when test="contains($primees,'put')">
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass:put" ref="{$ref}"/>
      </xsl:when>
      <xsl:otherwise>
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']">
     <!--if tuple is inplace-->
     <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="inplace" ref="{$ref}"/>
    </xsl:when>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="document($root/coupled/composition)/composition
            /connections/priming/model[@name=$modelName]
            /entryPoint[@name=$epName]/primed[@id=$id]">
   <!--tuple is primed only (in)-->
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='argpass']">
     <!--if tuple is argpassing, need to do further tests-->
     <xsl:variable name="primers">
      <!--for each primer of current tuple-->
      <xsl:for-each select="document($root/coupled/composition)/composition
                    /connections/priming/model[@name=$modelName]
                    /entryPoint[@name=$epName]/primed[@id=$id]/model">
       <xsl:variable name="cmodelName" select="@name"/>
       <xsl:variable name="cepName" select="@entryPoint"/>
       <xsl:variable name="cid" select="@id"/>
       <xsl:choose>
        <xsl:when test="document($root/coupled/models/model)
                  /definition[name=$cmodelName]/entryPoints
                  /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                  document($root/coupled/deployment)/deployment//sequenceUnit
                  /model[@name=$modelName]/../model[@name=$cmodelName]">
         <xsl:text>argpass</xsl:text>
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>get</xsl:text>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:for-each>
     </xsl:variable>
     <!--
     <xsl:message terminate="no">
      <xsl:text> +++++++++in priming+++++++++++</xsl:text>
      <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$id"/><xsl:text>.</xsl:text>
      <xsl:text>primers=</xsl:text><xsl:value-of select="$primers"/><xsl:text>=</xsl:text>
     </xsl:message>
     -->
     <xsl:choose>
      <xsl:when test="contains($primers,'get')">
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass" ref="{$ref}"/>
      </xsl:when>
      <xsl:otherwise>
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']">
     <!--if tuple is inplace-->
     <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="inplace" ref="{$ref}"/>
    </xsl:when>
   </xsl:choose>
  </xsl:when>
  <xsl:when test="document($root/coupled/composition)/composition
            /connections/priming/model/entryPoint/primed
            /model[@name=$modelName and @entryPoint=$epName and @id=$id]">
   <!--tuple is a primer only (out)-->
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='argpass']">
     <!--if tuple is argpassing, need to do further tests-->
     <xsl:variable name="primees">
      <!--for each primee of current tuple-->
      <xsl:for-each select="document($root/coupled/composition)
                    /composition/connections/priming/model/entryPoint/primed
                    /model[@name=$modelName and @entryPoint=$epName and @id=$id]">
       <xsl:variable name="cmodelName" select="../../../@name"/>
       <xsl:variable name="cepName" select="../../@name"/>
       <xsl:variable name="cid" select="../@id"/>
       <xsl:choose>
        <xsl:when test="document($root/coupled/models/model)
                  /definition[name=$cmodelName]/entryPoints
                  /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                  document($root/coupled/deployment)/deployment//sequenceUnit
                  /model[@name=$modelName]/../model[@name=$cmodelName]">
         <xsl:text>argpass</xsl:text>
        </xsl:when>
        <xsl:otherwise>
         <xsl:text>put</xsl:text>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:for-each>
     </xsl:variable>
     <!--
     <xsl:message terminate="no">
      <xsl:text> +++++++++out priming+++++++++++</xsl:text>
      <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
      <xsl:value-of select="$id"/><xsl:text>.</xsl:text>
      <xsl:text>primees=</xsl:text><xsl:value-of select="$primees"/><xsl:text>=</xsl:text>
     </xsl:message>
     -->
     <xsl:choose>
      <xsl:when test="contains($primees,'put')">
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass:put" ref="{$ref}"/>
      </xsl:when>
      <xsl:otherwise>
       <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']">
     <!--if tuple is inplace-->
     <ptuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="inplace" ref="{$ref}"/>
    </xsl:when>
   </xsl:choose>
  </xsl:when>
 </xsl:choose>
</xsl:template>


<xsl:template name="UnCoupledArgPass">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="multi-instance"/>
 <xsl:choose>
  <xsl:when test="contains($multi-instance,'true')">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@name=$modelName and @ep=$epName]">
    <xsl:variable name="instance" select="@instance"/>
<!--
    <DEBUG position="HELLO1" modelName="{$modelName}" epName="{$epName}" id="{$id}" />
-->
    <xsl:if test="not(document($root/coupled/composition)
              /composition/connections/timestepping/set
              /field[@modelName=$modelName and @epName=$epName and @id=$id and @instance=$instance])">
     <xsl:variable name="ref">
      <xsl:variable name="tmp">
       <xsl:call-template name="dataidgenvalue">
        <xsl:with-param name="modelName" select="$modelName"/>
        <xsl:with-param name="epName" select="$epName"/>
        <xsl:with-param name="id" select="$id"/>
        <xsl:with-param name="instance" select="$instance"/>
       </xsl:call-template>
      </xsl:variable>
     <xsl:value-of select="substring-before($tmp,':')"/>
     </xsl:variable>
     <xsl:variable name="varname">
      <xsl:call-template name="getvarname">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="epName" select="$epName"/>
       <xsl:with-param name="id" select="$id"/>
       <xsl:with-param name="instance" select="$instance"/>
      </xsl:call-template>
     </xsl:variable>
     <!-- IRH: Added SU# to tuple, so we don't have to recalculate it 
     every time we need it.  Also added DU#. -->
     <xsl:variable name="su">
      <xsl:call-template name="getSequenceUnitID">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="modelInstance" select="$instance"/>
      </xsl:call-template>
     </xsl:variable>
     <xsl:variable name="du">
      <xsl:call-template name="getDeploymentUnitID">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="modelInstance" select="$instance"/>
      </xsl:call-template>
     </xsl:variable>
     <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
    </xsl:if>
   </xsl:for-each>
  </xsl:when>
  <xsl:otherwise>
   <xsl:if test="not(document($root/coupled/composition)
             /composition/connections/timestepping/set
             /field[@modelName=$modelName and @epName=$epName and @id=$id] or
             document($root/coupled/composition)/composition/connections/timestepping/modelChannel
             [@outModel=$modelName]/epChannel[@outEP=$epName]/connection[@outID=$id] or
             document($root/coupled/composition)/composition/connections/timestepping/modelChannel
             [@inModel=$modelName]/epChannel[@inEP=$epName]/connection[@inID=$id])">
    <xsl:variable name="ref">
     <xsl:variable name="tmp">
      <xsl:call-template name="dataidgenvalue">
       <xsl:with-param name="modelName" select="$modelName"/>
       <xsl:with-param name="epName" select="$epName"/>
       <xsl:with-param name="id" select="$id"/>
      </xsl:call-template>
     </xsl:variable>
     <xsl:value-of select="substring-before($tmp,':')"/>
    </xsl:variable>
    <xsl:variable name="varname">
     <xsl:call-template name="getvarname">
      <xsl:with-param name="modelName" select="$modelName"/>
      <xsl:with-param name="epName" select="$epName"/>
      <xsl:with-param name="id" select="$id"/>
     </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="su">
     <xsl:call-template name="getSequenceUnitID">
      <xsl:with-param name="modelName" select="$modelName"/>
     </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="du">
     <xsl:call-template name="getDeploymentUnitID">
      <xsl:with-param name="modelName" select="$modelName"/>
     </xsl:call-template>
    </xsl:variable>
    <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" su="{$su}" du="{$du}"/>
   </xsl:if>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template name="CoupledArgPass">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="multi-instance"/>

 <DEBUG author="rupert" position="CoupledArgPass" modelName="{$modelName}" epName="{$epName}" id="{$id}" multi-instance="{$multi-instance}"/>

 <xsl:choose>
  <xsl:when test="contains($multi-instance,'true')">
   <xsl:for-each select="document($root/coupled/deployment)/deployment/schedule//model[@name=$modelName and @ep=$epName]">
    <!--
    <xsl:message terminate="no">
     <xsl:text>model name=</xsl:text><xsl:value-of select="@name"/><xsl:text>%</xsl:text>
     <xsl:text>modelName=</xsl:text><xsl:value-of select="$modelName"/><xsl:text>%</xsl:text>
     <xsl:text>instance=</xsl:text><xsl:value-of select="@instance"/>
    </xsl:message>
    -->
    <xsl:call-template name="processCoupledArgPass">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="epName" select="$epName"/>
     <xsl:with-param name="id" select="$id"/>
     <xsl:with-param name="instance" select="@instance"/>
    </xsl:call-template>
   </xsl:for-each>
  </xsl:when>
  <xsl:otherwise>
    <xsl:call-template name="processCoupledArgPass">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="epName" select="$epName"/>
     <xsl:with-param name="id" select="$id"/>
    </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="processCoupledArgPass">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <!--below should be a when, with an alternative when that handles p2p-->
   <xsl:variable name="ref">
    <xsl:call-template name="dataidgen">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="epName" select="$epName"/>
     <xsl:with-param name="id" select="$id"/>
     <xsl:with-param name="instance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="varname">
    <xsl:call-template name="getvarname">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="epName" select="$epName"/>
     <xsl:with-param name="id" select="$id"/>
     <xsl:with-param name="instance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="su">
    <xsl:call-template name="getSequenceUnitID">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="modelInstance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>
   <xsl:variable name="du">
    <xsl:call-template name="getDeploymentUnitID">
     <xsl:with-param name="modelName" select="$modelName"/>
     <xsl:with-param name="modelInstance" select="$instance"/>
    </xsl:call-template>
   </xsl:variable>

 <xsl:choose>
  <!-- is this variable part of a set notation coupling? -->
  <xsl:when test="document($root/coupled/composition)
                /composition/connections/timestepping/set
                /field[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]">
   <xsl:choose>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='argpass']">
     <!--if tuple is argpassing, need to do further tests-->
     <xsl:variable name="direction" select="document($root/coupled/models/model)
                                    /definition[name=$modelName]/entryPoints/
                                    entryPoint[@name=$epName]/data[@id=$id]/@direction"/>
     <xsl:choose>
      <xsl:when test="$direction='in'">
       <!--
       <xsl:message terminate="no">
        <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$id"/><xsl:text>.</xsl:text><xsl:value-of select="$newline"/>
        <xsl:text>direction is in</xsl:text><xsl:value-of select="$newline"/>
       </xsl:message>
       -->
       <xsl:variable name="form">
        <xsl:for-each select="document($root/coupled/composition)
                      /composition/connections/timestepping/set
                      /field[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/..
                      /field[@modelName!=$modelName or @epName!=$epName or @id!=$id or (@instance and @instance!=$instance)]">
         <!--connected tuple-->
         <xsl:variable name="cmodelName" select="@modelName"/>
         <xsl:variable name="cepName" select="@epName"/>
         <xsl:variable name="cid" select="@id"/>
         <xsl:variable name="cinstance" select="@instance"/>
         <xsl:variable name="cdirection" select="document($root/coupled/models/model)
                                         /definition[name=$cmodelName]/entryPoints
                                         /entryPoint[@name=$cepName]/data[@id=$cid]/@direction"/>
         <xsl:choose>
          <xsl:when test="not(document($root/coupled/models/model)/definition[name=$cmodelName]/language='intrinsic')">
           <xsl:if test="$cdirection='out' or $cdirection='inout'">
            <xsl:choose>
             <xsl:when test="document($root/coupled/models/model)
                     /definition[name=$cmodelName]/entryPoints
                     /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                     document($root/coupled/deployment)/deployment//sequenceUnit
                     /model[@name=$modelName and (not($instance) or @instance=$instance)]/../
                     model[@name=$cmodelName and (not($cinstance) or @instance=$cinstance)]">
              <xsl:text>argpass</xsl:text>
             </xsl:when>
             <xsl:otherwise>
              <xsl:text>get</xsl:text>
             </xsl:otherwise>
            </xsl:choose>
           </xsl:if>
          </xsl:when>
          <!--When we find an intrinsic field...-->
          <xsl:otherwise>
           <!--...if it's direction is out...-->
           <xsl:if test="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/entryPoint[@name=$cepName]/
                        data[@id=$cid]/@direction='out'">
            <xsl:variable name="intrinsicInID" select="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/
                        entryPoint[@name=$cepName]/data[@direction='in']/@id"/>
            <!--...search the set of the matching 'in' field for connections...-->
            <xsl:for-each select="document($root/coupled/composition)
                /composition//set/field[@modelName=$cmodelName and 
                @epName=$cepName and @id=$intrinsicInID and (not(@instance) or @instance=$cinstance)]/../
                field[@modelName!=$cmodelName or @epName!=$cepName or @id!=$intrinsicInID or @instance!=$cinstance]">

             <xsl:variable name="c2modelName" select="@modelName"/>
             <xsl:variable name="c2epName" select="@epName"/>
             <xsl:variable name="c2id" select="@id"/>
             <xsl:variable name="c2instance" select="@instance"/>
             <xsl:variable name="c2direction" select="document($root/coupled/models/model)
                                         /definition[name=$c2modelName]/entryPoints
                                         /entryPoint[@name=$c2epName]/data[@id=$c2id]/@direction"/>
             <xsl:if test="$c2direction='out' or $c2direction='inout'">
              <xsl:choose>
               <xsl:when test="document($root/coupled/models/model)
                       /definition[name=$c2modelName]/entryPoints
                       /entryPoint[@name=$c2epName]/data[@id=$c2id and @form='argpass'] and 
                       document($root/coupled/deployment)/deployment//sequenceUnit
                       /model[@name=$modelName and (not($instance) or @instance=$instance)]/../
                       model[@name=$c2modelName and (not($c2instance) or @instance=$c2instance)]">
                <xsl:text>argpass</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                <xsl:text>get</xsl:text>
               </xsl:otherwise>
              </xsl:choose>
             </xsl:if>

            </xsl:for-each>
           </xsl:if>
          </xsl:otherwise>
         </xsl:choose>
        </xsl:for-each>
       </xsl:variable>
       <xsl:choose>
        <xsl:when test="contains($form,'get')">
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:when>
        <xsl:otherwise>
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:when>
      <xsl:when test="$direction='out'">
       <!--
       <xsl:message terminate="no">
        <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$id"/><xsl:text>.</xsl:text><xsl:value-of select="$newline"/>
        <xsl:text>direction is out</xsl:text><xsl:value-of select="$newline"/>
       </xsl:message>
       -->
       <xsl:variable name="form">
        <xsl:for-each select="document($root/coupled/composition)
                      /composition/connections/timestepping/set
                      /field[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/..
                      /field[@modelName!=$modelName or @epName!=$epName or @id!=$id or (@instance and @instance!=$instance)]">
 <!--
                      /field[@modelName=$modelName and @epName=$epName and @id=$id]/..
                      /field[@modelName!=$modelName or @epName!=$epName or @id!=$id]">
 -->
         <!--connected tuple-->
         <xsl:variable name="cmodelName" select="@modelName"/>
         <xsl:variable name="cepName" select="@epName"/>
         <xsl:variable name="cid" select="@id"/>
         <xsl:variable name="cinstance" select="@instance"/>
         <xsl:variable name="cdirection" select="document($root/coupled/models/model)
                                         /definition[name=$cmodelName]/entryPoints
                                         /entryPoint[@name=$cepName]/data[@id=$cid]/@direction"/>

         <xsl:choose>
          <xsl:when test="not(document($root/coupled/models/model)/definition[name=$cmodelName]/language='intrinsic')">

           <xsl:if test="$cdirection='in' or $cdirection='inout'">
            <xsl:choose>
             <xsl:when test="document($root/coupled/models/model)
                       /definition[name=$cmodelName]/entryPoints
                       /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                       document($root/coupled/deployment)/deployment//sequenceUnit
                       /model[@name=$modelName and (not(@instance) or @instance=$instance)]/../
                       model[@name=$cmodelName and (not(@instance) or @instance=$cinstance)]">
              <xsl:text>argpass</xsl:text>
             </xsl:when>
             <xsl:otherwise>
              <xsl:text>put</xsl:text>
             </xsl:otherwise>
            </xsl:choose>
           </xsl:if>

          </xsl:when>
          <!--When we find an intrinsic field...-->
          <xsl:otherwise>
           <!--...if it's direction is in ...-->
           <xsl:if test="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/entryPoint[@name=$cepName]/
                        data[@id=$cid]/@direction='in'">
            <xsl:variable name="intrinsicOutID" select="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/
                        entryPoint[@name=$cepName]/data[@direction='out']/@id"/>
            <!--...search the set of the matching 'out' field for connections...-->
            <xsl:for-each select="document($root/coupled/composition)
                /composition//set/field[@modelName=$cmodelName and 
                @epName=$cepName and @id=$intrinsicOutID and (not(@instance) or @instance=$cinstance)]/../
                field[@modelName!=$cmodelName or @epName!=$cepName or @id!=$intrinsicOutID or @instance!=$cinstance]">

             <xsl:variable name="c2modelName" select="@modelName"/>
             <xsl:variable name="c2epName" select="@epName"/>
             <xsl:variable name="c2id" select="@id"/>
             <xsl:variable name="c2instance" select="@instance"/>
             <xsl:variable name="c2direction" select="document($root/coupled/models/model)
                                         /definition[name=$c2modelName]/entryPoints
                                         /entryPoint[@name=$c2epName]/data[@id=$c2id]/@direction"/>
             <xsl:if test="$c2direction='in' or $c2direction='inout'">
              <xsl:choose>
               <xsl:when test="document($root/coupled/models/model)
                       /definition[name=$c2modelName]/entryPoints
                       /entryPoint[@name=$c2epName]/data[@id=$c2id and @form='argpass'] and 
                       document($root/coupled/deployment)/deployment//sequenceUnit
                       /model[@name=$modelName and (not($instance) or @instance=$instance)]/../
                       model[@name=$c2modelName and (not($c2instance) or @instance=$c2instance)]">
                <xsl:text>argpass</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                <xsl:text>put</xsl:text>
               </xsl:otherwise>
              </xsl:choose>
             </xsl:if>

            </xsl:for-each>
           </xsl:if>
          </xsl:otherwise>
         </xsl:choose>

        </xsl:for-each>
       </xsl:variable>
       <xsl:choose>
        <xsl:when test="contains($form,'put')">
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass:put" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:when>
        <xsl:otherwise>
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:when>
      <xsl:when test="$direction='inout'">
      <!--
       <xsl:message terminate="no">
        <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
        <xsl:value-of select="$id"/><xsl:text>.</xsl:text><xsl:value-of select="$newline"/>
        <xsl:text>direction is inout</xsl:text><xsl:value-of select="$newline"/>
       </xsl:message>
       -->
       <xsl:variable name="form">
        <!--
        <xsl:message terminate="no">
         <comment><xsl:text>target=</xsl:text>
           <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$epName"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$id"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$instance"/>
         </comment>
        </xsl:message>
        -->
        <xsl:for-each select="document($root/coupled/composition)
                      /composition/connections/timestepping/set
                      /field[@modelName=$modelName and @epName=$epName and @id=$id and (not(@instance) or @instance=$instance)]/..
                      /field[@modelName!=$modelName or @epName!=$epName or @id!=$id or (@instance and @instance!=$instance)]">
 <!--
                      /field[@modelName=$modelName and @epName=$epName and @id=$id]/..
                      /field[@modelName!=$modelName or @epName!=$epName or @id!=$id]">
 -->
         <!--connected tuple-->
         <xsl:variable name="cmodelName" select="@modelName"/>
         <xsl:variable name="cepName" select="@epName"/>
         <xsl:variable name="cid" select="@id"/>
         <xsl:variable name="cinstance" select="@instance"/>
         <xsl:variable name="cdirection" select="document($root/coupled/models/model)
                                         /definition[name=$cmodelName]/entryPoints
                                         /entryPoint[@name=$cepName]/data[@id=$cid]/@direction"/>
  <!--
         <xsl:message terminate="no">
         <comment><xsl:text>   test=</xsl:text>
           <xsl:value-of select="$cmodelName"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$cepName"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$cid"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$cinstance"/><xsl:text>.</xsl:text>
           <xsl:value-of select="$cdirection"/>
         </comment>
       </xsl:message>
       -->
         <xsl:choose>
          <xsl:when test="not(document($root/coupled/models/model)/definition[name=$cmodelName]/language='intrinsic')">

           <xsl:choose>
            <xsl:when test="document($root/coupled/models/model)
                      /definition[name=$cmodelName]/entryPoints
                      /entryPoint[@name=$cepName]/data[@id=$cid and @form='argpass'] and 
                      document($root/coupled/deployment)/deployment//sequenceUnit
                      /model[@name=$modelName and (not(@instance) or @instance=$instance)]/../
                      model[@name=$cmodelName and (not(@instance) or @instance=$cinstance)]">
             <xsl:text>argpass</xsl:text>
            </xsl:when>
            <xsl:otherwise>
             <xsl:choose>
              <xsl:when test="$cdirection='in'">
               <xsl:text>put</xsl:text>
              </xsl:when>
              <xsl:when test="$cdirection='out'">
               <xsl:text>get</xsl:text>
              </xsl:when>
              <xsl:when test="$cdirection='inout'">
               <xsl:text>get:put</xsl:text>
              </xsl:when>
             </xsl:choose>
            </xsl:otherwise>
           </xsl:choose>

          </xsl:when>
          <!--When we find an intrinsic field...-->
          <xsl:otherwise>
<!--
           <xsl:message terminate="no">
            <xsl:value-of select="$modelName"/><xsl:value-of select="$epName"/><xsl:value-of select="$id"/><xsl:value-of select="$instance"/><xsl:text>
</xsl:text> <xsl:value-of select="$cmodelName"/><xsl:value-of select="$cepName"/><xsl:value-of select="$cid"/><xsl:value-of select="$cinstance"/>
           </xsl:message>
-->
           <!--...if it's direction is in ...
           <xsl:if test="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/entryPoint[@name=$cepName]/
                        data[@id=$cid]/@direction='in'">-->
            <xsl:variable name="oppCdir">
             <xsl:choose>
              <xsl:when test="$cdirection='in'">
               <xsl:text>out</xsl:text>
              </xsl:when>
              <xsl:when test="$cdirection='out'">
               <xsl:text>in</xsl:text>
              </xsl:when>
              <xsl:otherwise>
               <xsl:message terminate="yes">
                <xsl:text>Error: intrinsic model may not have inout field.</xsl:text>
               </xsl:message>
              </xsl:otherwise>
             </xsl:choose>
            </xsl:variable>
            <xsl:variable name="intrinsicOppID" select="document($root/coupled/models/model)/definition[name=$cmodelName]/entryPoints[1]/
                        entryPoint[@name=$cepName]/data[@direction=$oppCdir]/@id"/>
            <!--...search the set of the matching opposite field for connections...-->
            <xsl:for-each select="document($root/coupled/composition)
                /composition//set/field[@modelName=$cmodelName and 
                @epName=$cepName and @id=$intrinsicOppID and (not(@instance) or @instance=$cinstance)]/../
                field[@modelName!=$cmodelName or @epName!=$cepName or @id!=$intrinsicOppID or @instance!=$cinstance]">

             <xsl:variable name="c2modelName" select="@modelName"/>
             <xsl:variable name="c2epName" select="@epName"/>
             <xsl:variable name="c2id" select="@id"/>
             <xsl:variable name="c2instance" select="@instance"/>
             <xsl:variable name="c2direction" select="document($root/coupled/models/model)
                                         /definition[name=$c2modelName]/entryPoints
                                         /entryPoint[@name=$c2epName]/data[@id=$c2id]/@direction"/>
             <xsl:if test="contains($c2direction,$cdirection)">
              <xsl:choose>
               <xsl:when test="document($root/coupled/models/model)
                       /definition[name=$c2modelName]/entryPoints
                       /entryPoint[@name=$c2epName]/data[@id=$c2id and @form='argpass'] and 
                       document($root/coupled/deployment)/deployment//sequenceUnit
                       /model[@name=$modelName and (not($instance) or @instance=$instance)]/../
                       model[@name=$c2modelName and (not($c2instance) or @instance=$c2instance)]">
                <xsl:text>argpass</xsl:text>
               </xsl:when>
               <xsl:otherwise>

                <xsl:choose>
                 <xsl:when test="$c2direction='in'">
                  <xsl:text>put</xsl:text>
                 </xsl:when>
                 <xsl:when test="$c2direction='out'">
                  <xsl:text>get</xsl:text>
                 </xsl:when>
                 <xsl:when test="$c2direction='inout'">
                  <xsl:text>get:put</xsl:text>
                 </xsl:when>
                </xsl:choose>

               </xsl:otherwise>
              </xsl:choose>
             </xsl:if>

            </xsl:for-each>
           <!--</xsl:if>-->
          </xsl:otherwise>
         </xsl:choose>

        </xsl:for-each>
       </xsl:variable>
       <xsl:choose>
        <!-- CWA/IRH: Bugfix for argpass inouts. -->
        <!-- <xsl:when test="contains($form,'get:put')"> -->
        <xsl:when test="contains($form,'get') and contains($form,'put')">
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass:put" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:when>
        <xsl:when test="contains($form,'get')">
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get:argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:when>
        <xsl:when test="contains($form,'put')">
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass:put" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:when>
        <xsl:otherwise>
         <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
        </xsl:otherwise>
       </xsl:choose>
      </xsl:when>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']">
     <!--if tuple is inplace with set notation-->
     <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="inplace" ref="{$ref}" name="{$varname}" instance="{$instance}" su="{$su}" du="{$du}"/>
    </xsl:when>
   </xsl:choose>
  </xsl:when>
  <!-- is this variable part of a p2p coupling -->
  <xsl:when test="document($root/coupled/composition)/composition/connections/timestepping/modelChannel
             [@outModel=$modelName]/epChannel[@outEP=$epName]/connection[@outID=$id] or
             document($root/coupled/composition)/composition/connections/timestepping/modelChannel
             [@inModel=$modelName]/epChannel[@inEP=$epName]/connection[@inID=$id]">

   <xsl:variable name="info1">
     <xsl:text>CoupledArgPass</xsl:text>
     <xsl:text> model </xsl:text><xsl:value-of select="$modelName"/>
     <xsl:text> ep </xsl:text><xsl:value-of select="$epName"/>
     <xsl:text> id </xsl:text><xsl:value-of select="$id"/>
     <xsl:text> is part of a p2p coupling</xsl:text>
   </xsl:variable>
   <debug author="rupert" info="{$info1}"/>

   <xsl:choose>
    <!-- is this data in-place? -->
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']">
     <xsl:variable name="dir" select="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='inplace']/@direction"/>
     <xsl:choose>
      <xsl:when test="$dir='out'">
       <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="put" ref="{$ref}" name="{$varname}" su="{$su}" du="{$du}"/>
      </xsl:when>
      <xsl:when test="$dir='in'">
       <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="get" ref="{$ref}" name="{$varname}" su="{$su}" du="{$du}"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
         <xsl:text>Error: inplace fields must have direction 'in' or 'out'.</xsl:text><xsl:value-of select="$modelName"/><xsl:text>.</xsl:text>
         <xsl:value-of select="$modelName"/><xsl:text>.</xsl:text><xsl:value-of select="$id"/><xsl:text> has direction </xsl:text>
         <xsl:value-of select="$dir"/>
        </xsl:message>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>
    <xsl:when test="document($root/coupled/models/model)
              /definition[name=$modelName]/entryPoints/
              entryPoint[@name=$epName]/data[@id=$id and @form='argpass']">
     <!-- p2p notation with argpassing values -->
     <xsl:variable name="info2">
       <xsl:text>CoupledArgPass</xsl:text>
       <xsl:text> model </xsl:text><xsl:value-of select="$modelName"/>
       <xsl:text> ep </xsl:text><xsl:value-of select="$epName"/>
       <xsl:text> id </xsl:text><xsl:value-of select="$id"/>
       <xsl:text> is an argpassing variable</xsl:text>
     </xsl:variable>
     <debug author="rupert" info="{$info2}"/>
     <tuple modelName="{$modelName}" epName="{$epName}" id="{$id}" form="argpass" ref="{$ref}" name="{$varname}" su="{$su}"/>
    </xsl:when>
    <!-- RF: Catch any falling off the end of this case statement -->
    <xsl:otherwise>
      <xsl:message terminate="yes">
        <xsl:text>CoupledArgPass: Error </xsl:text>
        <xsl:text> model </xsl:text><xsl:value-of select="$modelName"/>
        <xsl:text> ep </xsl:text><xsl:value-of select="$epName"/>
        <xsl:text> id </xsl:text><xsl:value-of select="$id"/>
        <xsl:text> data is not inplace or argpassing.</xsl:text>
      </xsl:message>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <!-- RF: Catch any falling off the end of this case statement -->
  <!-- this variable is not part of any timestepping coupling -->
  <xsl:otherwise>
    <xsl:variable name="info">
      <xsl:text>CoupledArgPass </xsl:text>
      <xsl:text> model </xsl:text><xsl:value-of select="$modelName"/>
      <xsl:text> ep </xsl:text><xsl:value-of select="$epName"/>
      <xsl:text> id </xsl:text><xsl:value-of select="$id"/>
      <xsl:text> is not part of any set or p2p notation coupling</xsl:text>
    </xsl:variable>
    <debug author="rupert" info="{$info}"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


</xsl:stylesheet>
