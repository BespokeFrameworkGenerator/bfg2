<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" indent="yes"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>
<xsl:param name="CommForms" select="'CommForms.xml'"/>
<xsl:param name="OutDir"/>
<xsl:param name="DUIndex" select="'false'"/>
<xsl:variable name="commFormsRoot" select="document($CommForms)"/>
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>
<xsl:variable name="debug" select="0"/>
<xsl:include href="../../Utils/targets.xsl"/>

<xsl:template match="/">
 <xsl:call-template name="CreateConfigFiles">
   <xsl:with-param name="DUIndex" select="$DUIndex"/>
 </xsl:call-template>
</xsl:template>

<!--
<xsl:include href="../../Utils/MatchAll.xsl"/>
-->

</xsl:stylesheet>
