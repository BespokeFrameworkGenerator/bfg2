<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    IRH 2nd January 2008
    
    Generate a unique deployment unit ID 
    from a model name and instance (if available).

-->


<xsl:template name="getDeploymentUnitID">
  <xsl:param name="modelName"/>
  <xsl:param name="modelInstance" select="''"/>

  <xsl:for-each select="document($root/coupled/deployment)/deployment//deploymentUnit">
    <xsl:if test="sequenceUnit/model[@name=$modelName and (not(boolean(@instance)) or @instance=$modelInstance)]">
      <xsl:value-of select="position()"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

