<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    IRH 13th December 2007
    
    Generate a unique sequence unit ID across deployment units
    from a model name and instance (if available).

-->


<xsl:template name="getSequenceUnitID">
  <xsl:param name="modelName"/>
  <xsl:param name="modelInstance" select="''"/>

  <xsl:for-each select="document($root/coupled/deployment)/deployment/deploymentUnits//sequenceUnit">
    <xsl:if test="model[@name=$modelName and (not(boolean(@instance)) or @instance=$modelInstance)]">
      <xsl:value-of select="position()"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Determine the sequence unit ID for a given model instance WITHIN
a deployment unit. -->
<!-- IRH: NOTE: not currently used. -->
<!--
<xsl:template name="getSequenceUnitID">
  <xsl:param name="deploymentUnitID"/>
  <xsl:param name="modelName"/>
  <xsl:param name="instanceID"/>

  <xsl:variable name="deploymentUnit" select="document($root/coupled/deployment)
    /deployment/deploymentUnits/deploymentUnit[$deploymentUnitID]"/>
  <xsl:for-each select="$deploymentUnit/sequenceUnit">
    <xsl:if test="model[@name=$modelName and (not(number($instanceID)) or @instance=$instanceID)]">
      <xsl:value-of select="position()"/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
-->

</xsl:stylesheet>

