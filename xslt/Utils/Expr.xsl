<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="integer">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <value><xsl:value-of select="."/></value>
</xsl:template>

<xsl:template match="argument">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <xsl:variable name="targetid" select="@id"/>
 <!--
 <xsl:message terminate="no">
  <xsl:text>argument:</xsl:text><xsl:value-of select="$newline"/>
  <xsl:text>model=</xsl:text><xsl:value-of select="$modelName"/><xsl:value-of select="$newline"/>
  <xsl:text>ep=</xsl:text><xsl:value-of select="$epName"/><xsl:value-of select="$newline"/>
  <xsl:text>id=</xsl:text><xsl:value-of select="$id"/><xsl:value-of select="$newline"/>
  <xsl:text>instance=</xsl:text><xsl:value-of select="$instance"/><xsl:value-of select="$newline"/>
  <xsl:text>targetid=</xsl:text><xsl:value-of select="$targetid"/><xsl:value-of select="$newline"/>
  <xsl:text>ref=</xsl:text><xsl:value-of select="$commFormsRoot/communicationForms
                 /*[@modelName=$modelName and @epName=$epName and @id=$id and (not($instance) or @instance=$instance)]/@ref"/><xsl:value-of select="$newline"/>
 </xsl:message>
 -->
 <variable ref="{$commFormsRoot/communicationForms
           /*[@modelName=$modelName and @epName=$epName and @id=$targetid and (not($instance) or @instance=$instance)]/@ref}"/>
</xsl:template>

<xsl:template match="plus">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <plus>
  <xsl:apply-templates select="*">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:apply-templates>
 </plus>
</xsl:template>

<xsl:template match="minus">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <minus>
  <xsl:apply-templates select="*">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:apply-templates>
 </minus>
</xsl:template>

<xsl:template match="times">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <times>
  <xsl:apply-templates select="*">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:apply-templates>
 </times>
</xsl:template>

<xsl:template match="divide">
 <xsl:param name="modelName"/>
 <xsl:param name="epName"/>
 <xsl:param name="id"/>
 <xsl:param name="instance"/>
 <divide>
  <xsl:apply-templates select="*">
   <xsl:with-param name="modelName" select="$modelName"/>
   <xsl:with-param name="epName" select="$epName"/>
   <xsl:with-param name="id" select="$id"/>
   <xsl:with-param name="instance" select="$instance"/>
  </xsl:apply-templates>
 </divide>
</xsl:template>


</xsl:stylesheet>
