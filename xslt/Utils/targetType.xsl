<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:template name="getTargetType">
 <xsl:choose>
  <xsl:when test="document($root/coupled/deployment)/deployment/target">
   <xsl:value-of select="document($root/coupled/deployment)/deployment/target"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="'sequential'"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
