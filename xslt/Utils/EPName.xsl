<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 8th Dec 2005
    Adds entrypoint structure to BFG2 code
    template. Assumes <EPStart> and <EPEnd> exist in
    the code template.

    By default the name is BFG2Target
    however the argument -PARAM EPName <MyName>
    will call the entryPoint <MyName>
-->

<xsl:param name="EPName" select="'BFG2Target'"/>

<xsl:template match="EP">
 <EP type="entryPoint" name="{$EPName}">
  <xsl:apply-templates select="*"/>
 </EP>
</xsl:template>

</xsl:stylesheet>
