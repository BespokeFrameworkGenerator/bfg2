<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:template name="ModuleName">
  <xsl:param name="modelRoot"/>

  <!-- module name if explicitly specified -->
  <xsl:variable name="moduleSpecified">
    <xsl:value-of select="$modelRoot/definition/languageSpecific/fortran/moduleName"/>
  </xsl:variable>

  <!-- use module name if it exists otherwise use model name -->
  <xsl:choose>
    <xsl:when test="$moduleSpecified!=''">
      <xsl:value-of select="$moduleSpecified"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$modelRoot/definition/name"/>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>
