<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:output method="text"/>

<xsl:template match="/">
 <xsl:for-each select="coupled/models/model">
  <xsl:value-of select="."/><xsl:text> </xsl:text>
 </xsl:for-each>
 <xsl:value-of select="coupled/deployment"/><xsl:text> </xsl:text>
 <xsl:value-of select="coupled/composition"/>
</xsl:template>

</xsl:stylesheet>
