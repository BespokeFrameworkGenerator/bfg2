<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan"
>

<!--
    RF 20th Sept 2005

    By default this stylesheet expects the BFG2 coupled
    document to be called coupled.xml and be placed in the
    same directory as this stylesheet. This can be changed
    by the argument -PARAM CoupledDocument <MyFile> which
    will change this to <MyFile>.

-->

<xsl:template name="RenamedEP">
 <xsl:param name="modelName"/>
 <xsl:param name="EP"/>
 <xsl:choose>
 <!-- if the module name is explicitly provided then use that -->
 <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]/languageSpecific/fortran/moduleName">
 <xsl:value-of select="document($root/coupled/models/model)/definition[name=$modelName]/languageSpecific/fortran/moduleName"/>
 </xsl:when>
 <!-- otherwise the module name is the model name -->
 <xsl:otherwise>
   <xsl:value-of select="$modelName"/>
 </xsl:otherwise>
 </xsl:choose>
 <xsl:text>_</xsl:text>
 <xsl:value-of select="$EP"/>
 <xsl:text>_</xsl:text>
 <xsl:value-of select="document($root/coupled/models/model)
               /definition/entryPoints/entryPoint/@type
               [../@name=$EP and ../../../name=$modelName]"/>
</xsl:template>

</xsl:stylesheet>
