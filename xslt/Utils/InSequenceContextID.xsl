<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 13th July 2006
    
    generate a unique id across models and entrypoints.

-->

<xsl:template name="InSequenceContextID">
<xsl:param name="modelName"/>
<xsl:param name="epName"/>
<xsl:param name="instance"/>

      <xsl:for-each select="document($root/coupled/deployment)//schedule//model">
        <xsl:if test="@name=$modelName and @ep=$epName and (not(@instance) or @instance=$instance)">
          <xsl:value-of select="position()"/>
        </xsl:if>
      </xsl:for-each>
    <!--
    <xsl:for-each select="document($root/coupled/models/model)/definition/entryPoints/entryPoint">
      <xsl:if test="@name=$epName and ancestor::definition/name=$modelName">
        <xsl:value-of select="position()"/>
      </xsl:if>
    </xsl:for-each>
    -->

</xsl:template>

</xsl:stylesheet>
