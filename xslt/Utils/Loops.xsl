<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>

<!--cwa:Added below template to return the same loop-bound variable for
a particular loop in the schedule; loop-bound variables are named ntsx where x
is the number returned by the below template.-->
<xsl:template name="get_loop_pos">
 <xsl:param name="myloopid"/>
 <xsl:for-each select="document($root/coupled/deployment)//loop">
  <xsl:if test="generate-id(.)=$myloopid">
   <xsl:value-of select="position()"/>
  </xsl:if>
 </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
