<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan"
>

<!--
    RF 20th Sept 2005
-->

<xsl:template name="ThreadName">
 <xsl:param name="modelName"/>
 <xsl:param name="instance"/>
 <xsl:value-of select="$modelName"/>
 <xsl:if test="$instance">
  <xsl:value-of select="concat('inst',string($instance))"/>
 </xsl:if>
 <xsl:text>Thread</xsl:text>
</xsl:template>

</xsl:stylesheet>
