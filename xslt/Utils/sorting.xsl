<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
<xsl:template match="/home">
 <xsl:call-template name="sort">
  <xsl:with-param name="list" select="'2,3,4'"/>
 </xsl:call-template>
</xsl:template>
-->

<xsl:template name="sort">
 <xsl:param name="list"/>
 <xsl:variable name="return">
  <xsl:call-template name="bubbleSort">
   <xsl:with-param name="list" select="$list"/>
  </xsl:call-template>
 </xsl:variable>
 <!-- IRH MOD: HACK! This removes excess commas from the sorted list.
 E.g. with input: '7,42,43,52,7,42,43,52,43,43,47' I was getting output:
 '7,42,,43,27,52'.  This only happens when a field is coupled to >1
 argument in an entry point function.  This situation is much more likely
 with OASIS configurations - e.g. a GENIE field is output to write_averages() 
 directly (on the IGCM grid) as argument 45, and via a transform
 (which regrids the data to the GOLDSTEIN grid) as argument 48.  In the MPI
 configuration, the interpolation transformer for the latter is "opaque" - 
 since it is a real piece of code, the transformer is the output for BFG's
 purposes, not write_averages().  In an OASIS configuration, the transformer
 is intrinsic - it does not represent real code - and the output for BFG's
 purposes is write_averages().  The bubble sort is designed to deal with this
 situation, but occasionally an extra comma is left in the output string,
 which will cause a Fortran compiler error.  I can't see the problem at 
 a glance though, and don't consider it worth spending much time on: 
 this custom sort code should probably be replaced in future versions of BFG.  
 E.g. the sort could be accomplished by writing the unsorted list as a
 series of elements to the next intermediate XML output file, and then 
 using <xsl:sort> at the next processing step.
 (Alternatively, a template could be added to remove duplicates numbers
 before passing the list to this sort template, but ideally we should
 avoid custom sort code where there is a "standard" alternative.) 
 N.B. This change will also remove leading and trailing commas.  -->
 <xsl:value-of select="translate(normalize-space(translate($return, ',', ' ')), ' ', ',')"/>
 <!-- <xsl:value-of select="substring-after($return,',')"/> -->
</xsl:template>


<!--
  bubble sort:
   1. Compare adjacent elements. If the first is greater than the second, swap them.
   2. Do this for each pair of adjacent elements, starting with the first two and ending with the last two. 
      At this point the last element should be the greatest.
   3. Repeat the steps for all elements except the last one.
   4. Keep repeating for one fewer element each time, until you have no more pairs to compare. (Alternatively, keep repeating until no swaps are
needed.)
-->
<xsl:template name="bubbleSort">
 <xsl:param name="list"/>
 <xsl:param name="rootlist" select="$list"/>
 <!--
 <xsl:text>sorting list </xsl:text><xsl:value-of select="$list"/><xsl:value-of select="$newline"/>
 -->
 <xsl:variable name="var1" select="substring-before($list,',')"/>
 <xsl:variable name="var2">
  <xsl:call-template name="get-var">
   <xsl:with-param name="list" select="substring-after($list,',')"/>
  </xsl:call-template>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="not(contains($list,','))">
    <xsl:variable name="remainder">
     <xsl:call-template name="remove-element">
      <xsl:with-param name="list" select="$rootlist"/>
      <xsl:with-param name="element" select="$list"/>
     </xsl:call-template>
    </xsl:variable>
    <!--
    <xsl:text>using remainder: *</xsl:text><xsl:value-of select="$remainder"/><xsl:text>*</xsl:text><xsl:value-of select="$newline"/>
    -->
    <xsl:if test="$remainder!=''">
     <!--
     <xsl:text>found greatest </xsl:text><xsl:value-of select="$list"/><xsl:value-of select="$newline"/>
     -->
     <xsl:call-template name="bubbleSort">
      <xsl:with-param name="list" select="$remainder"/>
     </xsl:call-template>
    </xsl:if>
   <!--
   <xsl:text>reached end: </xsl:text><xsl:value-of select="$list"/>
   <xsl:value-of select="$newline"/>
   -->
   <!--print the greatest value here-->
   <xsl:text>,</xsl:text>
   <xsl:value-of select="$list"/>
  </xsl:when>
  <xsl:when test="$var1>$var2">
   <!--
   <xsl:text>case 1: </xsl:text><xsl:value-of select="$var1"/><xsl:text> greaterThan </xsl:text><xsl:value-of select="$var2"/><xsl:value-of select="$newline"/>
   -->
   <xsl:variable name="newlist">
    <xsl:choose>
     <xsl:when test="substring-after($list,concat(',',$var2,','))">
      <xsl:value-of select="concat($var1,',',substring-after($list,concat(',',$var2,',')))"/>
     </xsl:when>
     <xsl:otherwise>
      <xsl:value-of select="$var1"/>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
   <xsl:call-template name="bubbleSort">
    <xsl:with-param name="list" select="$newlist"/>
    <xsl:with-param name="rootlist" select="$rootlist"/>
   </xsl:call-template>
  </xsl:when>
  <xsl:otherwise>
   <!--
   <xsl:text>case 2: </xsl:text><xsl:value-of select="$var1"/><xsl:text> lessThanEqual </xsl:text><xsl:value-of select="$var2"/><xsl:value-of select="$newline"/>
   -->
   <xsl:call-template name="bubbleSort">
    <xsl:with-param name="list" select="substring-after($list,concat($var1,','))"/>
    <xsl:with-param name="rootlist" select="$rootlist"/>
   </xsl:call-template>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="remove-element">
 <xsl:param name="list"/>
 <xsl:param name="element"/>
 <xsl:choose>
  <xsl:when test="contains($list,',')">
   <xsl:choose>
    <xsl:when test="substring-before($list,',')=$element">
     <xsl:call-template name="remove-element">
      <xsl:with-param name="list" select="substring-after($list,',')"/>
      <xsl:with-param name="element" select="$element"/>
     </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
     <xsl:choose>
      <xsl:when test="substring-after($list,',')!=$element">
       <xsl:value-of select="concat(substring-before($list,','),',')"/>
      </xsl:when>
      <xsl:otherwise>
       <xsl:value-of select="substring-before($list,',')"/>
      </xsl:otherwise>
     </xsl:choose>
     <xsl:call-template name="remove-element">
      <xsl:with-param name="list" select="substring-after($list,',')"/>
      <xsl:with-param name="element" select="$element"/>
     </xsl:call-template>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:when>
  <xsl:otherwise>
   <xsl:if test="$list!=$element">
    <xsl:value-of select="$list"/>
   </xsl:if>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="get-var">
 <xsl:param name="list"/>
 <xsl:choose>
  <xsl:when test="contains($list,',')">
   <xsl:value-of select="substring-before($list,',')"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="$list"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
