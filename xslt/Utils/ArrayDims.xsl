<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:output method="xml" indent="yes"/>

<xsl:template name="AddDims">
<xsl:param name="dims"/>

<!-- we need to create a separate <dim> element for each <lower>
     and <upper> with an increasing id as this is what the code
     gen expects. At the moment there is only one <dim> element
     per dimension i.e. a dim element may have lower and upper or
     just upper -->
<xsl:for-each select="$dims/lower | $dims/upper">
  <dim order="{position()}">

    <xsl:choose>
      <xsl:when test="name()='lower'">
        <lower>
          <xsl:apply-templates/>
        </lower>
      </xsl:when>
      <xsl:when test="name()='upper'">
        <upper>
          <xsl:apply-templates/>
        </upper>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:value-of select="$newline"/>
          <xsl:text>Error in stub1.xsl:AddDims element is neither "lower" or "upper"></xsl:text>
          <xsl:text>Found </xsl:text>
          <xsl:value-of select="name()"/>
          <xsl:value-of select="$newline"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </dim>
</xsl:for-each>

</xsl:template>

<xsl:template match="integer">
<value>
<xsl:value-of select="."/>
</value>
</xsl:template>

</xsl:stylesheet>