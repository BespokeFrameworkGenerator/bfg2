<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<xsl:include href="targetType.xsl"/>

<xsl:include href="../CodeGen/mpif.xsl"/>
<xsl:include href="../CodeGen/sequential.xsl"/>
<xsl:include href="../CodeGen/oasis4.xsl"/>
<xsl:include href="../CodeGen/esmf.xsl"/>

<!-- IRH: Template for creation of any "global" (not really the right word - 
I mean module-level) data required by the comms target type. -->
<xsl:template name="CreateCommsGlobalData">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <!--<xsl:call-template name="mpiGlobalData"/>-->
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4GlobalData"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfModuleData"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <!--<xsl:call-template name="sequentialGlobalData"/>-->
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl CreateCommsGlobalData template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="InitComms">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiInitComms"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4InitComms"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfInitComms"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <xsl:call-template name="sequentialInitComms"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl initComms template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!-- IRH: Not doing this any more - would have to have a separate
send function for each rank of array we were sending (at least with
MPI - perhaps not OASIS, as rank will be the same for all vars on
the same grid).
<xsl:template name="CommsSend">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4CommsSend"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="CommsReceive">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4CommsReceive"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>
-->

<xsl:template name="FinaliseComms">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiFinaliseComms"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4FinaliseComms"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfFinaliseComms"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <xsl:call-template name="sequentialFinaliseComms"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl FinaliseComms template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="targetIncludes">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiIncludeTarget"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4IncludeTarget"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfIncludeTarget"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <xsl:call-template name="sequentialIncludeTarget"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl targetIncludes template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="targetDecs">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiDeclareSendRecvVars"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4DeclareSendRecvVars"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfDeclareSendRecvVars"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <xsl:call-template name="sequentialDeclareSendRecvVars"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl targetDecs template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="CommsSyncBody">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiCommsSync"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4CommsSync"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfCommsSync"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <xsl:call-template name="sequentialCommsSync"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl CommsSyncBody template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="CreateConfigFiles">
 <xsl:param name="DUIndex"/>
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <!--<xsl:call-template name="mpiConfigFiles"/>-->
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4ConfigFiles">
     <xsl:with-param name="DUIndex" select="$DUIndex"/>
  </xsl:call-template>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
    <!-- no config files required for esmf -->
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <!--<xsl:call-template name="sequentialCommsSync"/>-->
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl CreateConfigFiles template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!-- IRH: 1/5/07: New "send" template that redirects to sends 
appropriate to the target type. -->
<xsl:template match="send">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiSend"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4Send"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfSend"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <!-- IRH: TODO: Is this right? We might be using MPI for
   sequential comms if models are in different deployment units. -->
   <xsl:call-template name="mpiSend"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl send template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!-- IRH: 1/5/07: New "receive" template that redirects to receives 
appropriate to the target type. -->
<xsl:template match="receive">
 <xsl:variable name="targetType">
  <xsl:call-template name="getTargetType"/>
 </xsl:variable>
 <xsl:choose>
  <xsl:when test="$targetType='mpi'">
   <xsl:call-template name="mpiReceive"/>
  </xsl:when>
  <xsl:when test="$targetType='esmf'">
   <xsl:call-template name="esmfReceive"/>
  </xsl:when>
  <xsl:when test="$targetType='oasis4'">
   <xsl:call-template name="oasis4Receive"/>
  </xsl:when>
  <xsl:when test="$targetType='sequential'">
   <!-- IRH: TODO: Is this right? We might be using MPI for
   sequential comms if models are in different deployment units. -->
   <xsl:call-template name="mpiReceive"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:message terminate="yes">
    <xsl:text>Error: unrecognised target type: </xsl:text><xsl:value-of select="$targetType"/><xsl:text> in targets.xsl receive template</xsl:text>
   </xsl:message>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
