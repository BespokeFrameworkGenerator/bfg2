<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
info here
-->

<xsl:output method="xml" indent="yes"/>

<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template name="GetName">
<xsl:param name="data"/>

<xsl:choose>
  <xsl:when test="@name">
    <xsl:value-of select="@name"/>
  </xsl:when>
  <xsl:otherwise>
    <xsl:choose>
      <xsl:when test="@form='argpass'">
        <xsl:text>arg</xsl:text>
      </xsl:when>
      <xsl:when test="@form='inplace'">
        <xsl:text>tag</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">
          <xsl:value-of select="$newline"/>
          <xsl:text>VarName.xsl:GetName Error. </xsl:text>
          <xsl:text>Expecting form attribute value of </xsl:text>
          <xsl:text>argpass or inplace but got '</xsl:text>
          <xsl:value-of select="@form"/>
          <xsl:text>'</xsl:text>
          <xsl:value-of select="$newline"/>
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="$data//scalar/@id"/>
  </xsl:otherwise>
</xsl:choose>

</xsl:template>

</xsl:stylesheet>
