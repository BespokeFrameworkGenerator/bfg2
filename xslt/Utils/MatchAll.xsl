<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
xmlns:xalan="org.apache.xalan.xslt.extensions.Redirect"
  extension-element-prefixes="xalan">

<!--
    RF 16th Sept 2005
-->

<!-- RF
<xsl:include href="Newline.xsl"/>
-->

<xsl:template match="node()">
  <xsl:copy>
    <xsl:copy-of select="@*" />
    <xsl:copy-of select="normalize-space(text())" />
    <xsl:apply-templates select="*"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
