<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:bfg2="http://www.cs.manchester.ac.uk/cnc/schema/bfg2"
exclude-result-prefixes="bfg2"
>

<!-- rf, not sure what the appropriate test is here, so always include set notation comms setup even if it is not required -->
<xsl:template name="SetNotationWithConcurrentExecution">
 <xsl:choose>
  <xsl:when test="document($root/coupled/composition)/composition/connections//set and
                  count(document($root/coupled/deployment)/deployment//sequenceUnit) > 1">
   <xsl:text>true</xsl:text>
  </xsl:when>
  <xsl:otherwise>
   <xsl:text>false</xsl:text>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template name="SetNotationWithConcurrentExecution_old">
  <xsl:choose>
    <xsl:when test="document($root/coupled/composition)/composition/connections/timestepping/set">

    <!-- we have some set notation -->
      <!--
      check all composition set notation
      <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/set">
        check that each model referenced in this set is in the same SU
        <xsl:variable name="firstModel">
          <xsl:value-of select="field[1]/@modelName"/>
        </xsl:variable>
        <xsl:variable name="firstModelSU">
          <xsl:value-of select="position(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[model/@name=$firstModel]))"/>
        </xsl:variable>
        <xsl:for-each select="field">
            <xsl:if test="position()>1)>
              <xsl:variable name="currentModel">
                <xsl:value-of select="@modelName"/>
              </xsl:variable>
              <xsl:variable name="currentModelSU">
                <xsl:value-of select="position(document($root/coupled/deployment)/deployment/deploymentUnits/deploymentUnit/sequenceUnit[model/@name=$currentModel]))"/>
              </xsl:variable>
              <xsl:if test="$currentModelSU!=firstModelSU">
                <xsl:text>Models are on different sequence units</xsl:text>
            </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
      -->

      <xsl:text>true</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>false</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
