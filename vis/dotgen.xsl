<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- RF est 15th February 2013
     Create a dot representation of a bfg coupled model
     Techniques are to 1: only vis nearest neighbour with set notation
     This is clearly not a full picture of connectivity (it
     potentially underconnects), 2: vis all potential set connections
     (this potentially overconnects), see parameter "mode" for selections
-->

<!-- options are min, max, all, none -->
<xsl:param name="mode" select="'max'"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:output method="text"/>

<xsl:template match="/">

<xsl:text>digraph graphname {
</xsl:text>

<xsl:text>
node [shape="ellipse", fixedsize=false, fontsize=10, fontname="Helvetica",
	fillcolor="silver", style="filled", penwidth=1, pencolor="black"];
edge [fixedsize=false, fontsize=10, fontname="Arial", labelfloat=false,
	color="black"];

</xsl:text>

<!-- output each model and transformation with its name -->
<xsl:for-each select="document($root/coupled/models/model)/definition">

<xsl:value-of select="name"/>
<xsl:choose>
<xsl:when test="type='scientific'">
<xsl:text>;
</xsl:text>
</xsl:when>
<xsl:when test="type='transformation'">
<xsl:text>[shape="box", fillcolor="crimson", style="filled"];
</xsl:text>
</xsl:when>
</xsl:choose>
</xsl:for-each>

<xsl:text>
</xsl:text>

<!-- output the connectivity between models -->
<xsl:for-each select="document($root/coupled/models/model)/definition">
  <xsl:variable name="SrcName" select="name"/>
  <xsl:for-each select="document($root/coupled/models/model)/definition">
    <xsl:variable name="SinkName" select="name"/>
    <xsl:if test="$SrcName!=$SinkName">
      <xsl:variable name="nAtRisk">
        <xsl:call-template name="atRisk">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          <xsl:with-param name="root" select="$root"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="numConnections">
        <xsl:choose>
	<xsl:when test="$mode='min'">
          <xsl:call-template name="countConnectionsMin">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          </xsl:call-template>
	</xsl:when>
	<xsl:when test="$mode='max'">
          <xsl:call-template name="countConnectionsMax">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          </xsl:call-template>
	</xsl:when>
	<xsl:when test="$mode='all'">
	  <xsl:text>1</xsl:text>
	</xsl:when>
	<xsl:when test="$mode='none'">
	  <xsl:text>0</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:message terminate="yes">
          <xsl:text>Error: mode </xsl:text>
          <xsl:value-of select="$mode"/>
          <xsl:text> is not one of [min,max,all,none]</xsl:text>
          </xsl:message>
	</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:if test="$numConnections&gt;0">

        <xsl:value-of select="$SrcName"/>
        <xsl:text>-&gt;</xsl:text>
        <xsl:value-of select="$SinkName"/>
        <xsl:text>[label=" </xsl:text>
        <xsl:value-of select="$numConnections"/>
        <xsl:text>"];
</xsl:text>
      </xsl:if>
    </xsl:if>
  </xsl:for-each>
</xsl:for-each>

<xsl:text>}
</xsl:text>

</xsl:template>

<xsl:template name="atRisk">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>
<xsl:param name="root"/>
    <xsl:variable name="atRisk">
      <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection">
        <xsl:variable name="epName" select="ancestor::epChannel/@outEP"/>
        <xsl:variable name="id" select="@outID"/>
        <xsl:if test="document($root/coupled//model)/definition[name=$SrcName]//entryPoint[@name=$epName]/data[@availability='AtRisk']//scalar[@id=$id]">
          <xsl:text>x</xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="string-length($atRisk)"/>
</xsl:template>

<xsl:template name="countConnectionsMax">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>

      <xsl:variable name="numPtoPConnections" select="count(document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection)"/>

      <xsl:variable name="numSetConnections" select="count(document($root/coupled/composition)/composition/connections/timestepping/set[field/@modelName=$SrcName and field/@modelName=$SinkName])"/>

      <xsl:value-of select="$numPtoPConnections+$numSetConnections"/>
</xsl:template>

<xsl:template name="countConnectionsMin">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>
<!-- to be written -->
</xsl:template>

</xsl:stylesheet>



