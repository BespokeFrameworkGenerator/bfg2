<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- RF est 4th April 2006
     Create svg images of models in provided coupling
-->

<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:param name="xsize" select="'640'"/>
<xsl:param name="ysize" select="'480'"/>
<xsl:param name="modelwidth" select="0.8 * $xsize"/>
<xsl:param name="modelheight" select="'40'"/>
<xsl:param name="ModelColour" select="'red'"/>
<xsl:param name="transformwidth" select="0.2 * $xsize"/>
<xsl:param name="TransformColour" select="'orange'"/>
<xsl:param name="modelfontsize" select="0.5 * $modelheight"/>
<xsl:param name="transformfontsize" select="0.8 * $modelheight"/>
<xsl:param name="TextStrokeColour" select="'black'"/>

<!--
<xsl:output method="xml" doctype-system="http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" doctype-public="-//W3C//DTD SVG 1.1//EN"/>
-->
<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">

<svg width="{$xsize}" height="{$ysize}" version="1.1">
<!--
xmlns="http://www.w3.org/2000/svg">
-->
<xsl:for-each select="document($root/coupled/models/model)/definition">

<xsl:variable name="xbase" select="0.1 * $xsize"/>
<xsl:variable name="ybase">
<xsl:value-of select="( ( position() * $ysize ) div ( last() + 1 ) ) - ( $modelheight div 2 ) "/>
</xsl:variable>

<!--
<g id="{name}">
-->
<xsl:choose>
<xsl:when test="type='scientific'">
<rect name="{name}" x="{$xbase}" y="{$ybase}" width="{$modelwidth}" height="{$modelheight}" fill="{$ModelColour}" style="stroke:black;stroke-width:2;opacity:0.5">
</rect>
<text name="{name}" style='text-anchor: middle' x="{$xbase + ($modelwidth div 2)}" y="{$ybase + (($modelheight + $modelfontsize) div 2)}" font-size="{$modelfontsize}" stroke="{$TextStrokeColour}" stroke-width="1">
<xsl:value-of select="class"/>
<xsl:text> model (</xsl:text>
<xsl:value-of select="name"/>
<xsl:text>)</xsl:text>
</text>
</xsl:when>
<xsl:when test="type='transformation'">
<rect name="{name}" x="{$xbase}" y="{$ybase}" width="{$transformwidth}" height="{$modelheight}" fill="{$TransformColour}" style="stroke:black;stroke-width:2;opacity:0.5">
</rect>
<text name="{name}" style='text-anchor: middle' x="{$xbase + ($transformwidth div 2)}" y="{$ybase + (($modelheight + $transformfontsize) div 2)}" font-size="{$transformfontsize}" stroke="{$TextStrokeColour}" stroke-width="1">
<xsl:value-of select="class"/>
<xsl:text> model (</xsl:text>
<xsl:value-of select="name"/>
<xsl:text>)</xsl:text>
</text>
</xsl:when>
</xsl:choose>
<!--
</g>
-->

</xsl:for-each>

</svg>

</xsl:template>

</xsl:stylesheet>



