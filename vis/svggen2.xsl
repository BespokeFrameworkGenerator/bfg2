<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:svg="http://www.w3.org/2000/svg">

<!-- RF est 8th April 2006
     Add model connections to existing svg using provided coupling
-->

<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:template match="svg:composition">

<!--
<xsl:text>Hello all
</xsl:text>
-->

<xsl:for-each select="/svg:svg/svg:rect">
<xsl:variable name="SrcName" select="@name"/>
<xsl:variable name="SrcX" select="@x"/>
<xsl:variable name="SrcY" select="@y"/>
<xsl:variable name="SrcWidth" select="@width"/>
<xsl:variable name="SrcHeight" select="@height"/>
  <xsl:for-each select="/svg:svg/svg:rect">
    <xsl:variable name="SinkName" select="@name"/>
    <xsl:variable name="SinkX" select="@x"/>
    <xsl:variable name="SinkY" select="@y"/>
    <xsl:variable name="SinkWidth" select="@width"/>
    <xsl:variable name="SinkHeight" select="@height"/>
    <xsl:if test="$SrcName!=$SinkName">
<!--
      <xsl:text>Connection between </xsl:text>
      <xsl:value-of select="$SrcName"/>
      <xsl:text> and </xsl:text>
      <xsl:value-of select="$SinkName"/>
-->
      <!-- create vertical lines from the middle of the most narrow model -->
      
      <xsl:variable name="LineX">
        <xsl:choose>
          <xsl:when test="$SrcWidth&lt;$SinkWidth">
            <xsl:value-of select="$SrcX + ($SrcWidth div 2)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$SinkX + ($SinkWidth div 2)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="LineSrcY">
      <xsl:choose>
        <xsl:when test="$SrcY&lt;$SinkY">
          <xsl:value-of select="$SrcY + $SrcHeight"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$SrcY"/>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:variable>

      <xsl:variable name="LineSinkY">
      <xsl:choose>
        <xsl:when test="$SrcY&lt;$SinkY">
          <xsl:value-of select="$SinkY"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$SinkY + $SinkHeight"/>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:variable>

      <xsl:variable name="XOffset">
      <xsl:choose>
        <xsl:when test="$SrcY&lt;$SinkY">
          <xsl:value-of select="'10'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'-10'"/>
        </xsl:otherwise>
      </xsl:choose>
      </xsl:variable>

      <xsl:variable name="numConnections" select="count(document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection)"/>

      <line x1="{$LineX+$XOffset}" y1="{$LineSrcY}" x2="{$LineX+$XOffset}" y2="{$LineSinkY}" style="stroke:rgb(99,99,99);stroke-width:{$numConnections}" />
      <xsl:text>
</xsl:text>

    </xsl:if>
  </xsl:for-each>
</xsl:for-each>

<!--
<xsl:text>Bye all
</xsl:text>
-->
</xsl:template>

<xsl:template match="node()">
  <xsl:copy>
    <xsl:copy-of select="@*" />
    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>



