<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:y="http://www.yworks.com/xml/graphml" xmlns="http://graphml.graphdrawing.org/xmlns">

<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">

<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <!--Created by yFiles for Java 2.8-->
  <key for="graphml" id="d0" yfiles.type="resources"/>
  <key for="port" id="d1" yfiles.type="portgraphics"/>
  <key for="port" id="d2" yfiles.type="portgeometry"/>
  <key for="port" id="d3" yfiles.type="portuserdata"/>
  <key attr.name="url" attr.type="string" for="node" id="d4"/>
  <key attr.name="description" attr.type="string" for="node" id="d5"/>
  <key for="node" id="d6" yfiles.type="nodegraphics"/>
  <key attr.name="Description" attr.type="string" for="graph" id="d7"/>
  <key attr.name="url" attr.type="string" for="edge" id="d8"/>
  <key attr.name="description" attr.type="string" for="edge" id="d9"/>
  <key for="edge" id="d10" yfiles.type="edgegraphics"/>
  <graph edgedefault="directed" id="G">
    <data key="d7"/>

    <xsl:variable name="startNodeName">
      <xsl:call-template name="getStartNodeName"/>
    </xsl:variable>

    <xsl:call-template name="startNode">
      <xsl:with-param name="name" select="$startNodeName"/>
    </xsl:call-template>

    <xsl:apply-templates select="document($root//deployment)//schedule/bfgiterate"/>

    <xsl:variable name="endNodeName">
      <xsl:call-template name="getEndNodeName"/>
    </xsl:variable>

    <xsl:call-template name="endNode">
      <xsl:with-param name="name" select="$endNodeName"/>
    </xsl:call-template>

  </graph>
  <data key="d0">
    <y:Resources/>
  </data>
</graphml>

</xsl:template>

<xsl:template match="converge">

  <xsl:variable name="startConvergeName">
    <xsl:call-template name="getStartConvergeName">
      <xsl:with-param name="convergeNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$startConvergeName"/>
  </xsl:call-template>

  <xsl:apply-templates/>

  <xsl:variable name="endConvergeName">
    <xsl:call-template name="getEndConvergeName">
      <xsl:with-param name="convergeNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$endConvergeName"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$startConvergeName"/>
    <xsl:with-param name="myNodeType" select="'startConverge'"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$endConvergeName"/>
    <xsl:with-param name="myNodeType" select="'endConverge'"/>
  </xsl:call-template>

  <xsl:call-template name="connector">
    <xsl:with-param name="fromNodeName" select="$endConvergeName"/>
    <xsl:with-param name="toNodeName" select="$startConvergeName"/>
  </xsl:call-template>

</xsl:template>

<xsl:template match="loop">

  <xsl:variable name="startLoopName">
    <xsl:call-template name="getStartLoopName">
      <xsl:with-param name="loopNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$startLoopName"/>
  </xsl:call-template>

  <xsl:apply-templates/>

  <xsl:variable name="endLoopName">
    <xsl:call-template name="getEndLoopName">
      <xsl:with-param name="loopNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$endLoopName"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$startLoopName"/>
    <xsl:with-param name="myNodeType" select="'startLoop'"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$endLoopName"/>
    <xsl:with-param name="myNodeType" select="'endLoop'"/>
  </xsl:call-template>

  <xsl:call-template name="connector">
    <xsl:with-param name="fromNodeName" select="$endLoopName"/>
    <xsl:with-param name="toNodeName" select="$startLoopName"/>
  </xsl:call-template>

</xsl:template>

<xsl:template match="model">

  <xsl:variable name="myNodeName">
    <xsl:call-template name="getModelName">
      <xsl:with-param name="modelNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="modelName">
    <xsl:value-of select="@name"/>
  </xsl:variable>

  <xsl:variable name="epName">
    <xsl:value-of select="@ep"/>
  </xsl:variable>

  <xsl:variable name="modelType">
    <xsl:value-of select="document($root//model)/definition[name=$modelName and //entryPoint/@name=$epName]/type"/>
  </xsl:variable>


  <xsl:call-template name="createModelNode">
    <xsl:with-param name="name" select="$myNodeName"/>
    <xsl:with-param name="modelType" select="$modelType"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$myNodeName"/>
    <xsl:with-param name="myNodeType" select="'model'"/>
  </xsl:call-template>

</xsl:template>

<xsl:template name="getStartNodeName">
  <xsl:text>startNode</xsl:text>
</xsl:template>

<xsl:template name="getEndNodeName">
  <xsl:text>endNode</xsl:text>
</xsl:template>

<xsl:template name="getStartConvergeName">
  <xsl:param name="convergeNode"/>
  <xsl:text>startConverge</xsl:text>
  <xsl:call-template name="getConvergeID">
    <xsl:with-param name="convergeNode" select="$convergeNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getEndConvergeName">
  <xsl:param name="convergeNode"/>
  <xsl:text>endConverge</xsl:text>
  <xsl:call-template name="getConvergeID">
    <xsl:with-param name="convergeNode" select="$convergeNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getStartLoopName">
  <xsl:param name="loopNode"/>
  <xsl:text>startLoop</xsl:text>
  <xsl:call-template name="getLoopID">
    <xsl:with-param name="loopNode" select="$loopNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getEndLoopName">
  <xsl:param name="loopNode"/>
  <xsl:text>endLoop</xsl:text>
  <xsl:call-template name="getLoopID">
    <xsl:with-param name="loopNode" select="$loopNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getConvergeID">
  <xsl:param name="convergeNode"/>
      <xsl:for-each select="document($root//deployment)//schedule//converge">
         <xsl:if test="generate-id(.)=generate-id($convergeNode)">
           <xsl:value-of select="position()"/>
         </xsl:if>
      </xsl:for-each>
</xsl:template>

<xsl:template name="getLoopID">
  <xsl:param name="loopNode"/>
      <xsl:for-each select="document($root//deployment)//schedule//loop">
         <xsl:if test="generate-id(.)=generate-id($loopNode)">
           <xsl:value-of select="position()"/>
         </xsl:if>
      </xsl:for-each>
</xsl:template>

<xsl:template name="getModelName">
  <xsl:param name="modelNode"/>
  <xsl:value-of select="$modelNode/@name"/>
  <xsl:text>_</xsl:text>
  <xsl:value-of select="$modelNode/@ep"/>
</xsl:template>


<xsl:template name="createEdges">
  <xsl:param name="myNodeName"/>
  <xsl:param name="myNodeType"/>

  <xsl:variable name="children" select="count(child::*)"/>

  <!--
  <xsl:message terminate="no">
    <xsl:text>Found </xsl:text>
    <xsl:value-of select="$children"/>
    <xsl:text> children for nodeName </xsl:text>
    <xsl:value-of select="$myNodeName"/>
    <xsl:text> and nodeType </xsl:text>
    <xsl:value-of select="$myNodeType"/>
  </xsl:message>
  -->
  <xsl:choose>

  <xsl:when test="$children>0 and local-name(child::*[$children])='model' and ($myNodeType='endLoop' or $myNodeType='endConverge')">

    <!-- I am the end of an iteration or convergence loop with a child model that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="$children>0 and local-name(child::*[$children])='loop' and $myNodeType='endLoop'">

    <!-- I am the end of an iteration loop with a child iteration loop that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndLoopName">
        <xsl:with-param name="loopNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="$children>0 and local-name(child::*[$children])='converge' and $myNodeType='endConverge'">

    <!-- I am the end of a convergence loop with a child convergence loop that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndConvergeName">
        <xsl:with-param name="convergeNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='model'">

    <!-- a model connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding-sibling::*) and parent::iterate and ancestor::bfgiterate/init/model">

  <!-- I am the first iterate element and at least one model exists within init, the last of which connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="ancestor::bfgiterate/init/model[last()]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding-sibling::*) and parent::final">

  <!-- I am the first 'final' element, 'iterate' elements must exist for valid BFG metadata, the last of which connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:choose>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='converge'">
          <xsl:call-template name="getEndConvergeName">
            <xsl:with-param name="convergeNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='loop'">
          <xsl:call-template name="getEndLoopName">
            <xsl:with-param name="loopNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='model'">
          <xsl:call-template name="getModelName">
            <xsl:with-param name="modelNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
    </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='loop'">

    <!-- An iteration loop connects to me and I am its sibling (so I am connected to the end of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndLoopName">
        <xsl:with-param name="loopNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='converge'">

    <!-- A convergence loop connects to me and I am its sibling (so I am connected to the end of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndConvergeName">
        <xsl:with-param name="convergeNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="parent::loop">

    <!-- An iteration loop connects to me and I am its child (so I am connected to the start of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartLoopName">
        <xsl:with-param name="loopNode" select="parent::loop[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="parent::converge">

    <!-- A convergence loop connects to me and I am its child (so I am connected to the start of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartConvergeName">
        <xsl:with-param name="convergeNode" select="parent::converge[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding::model/@ep or preceding::loop or preceding::converge)">

    <!-- I am the first object in the schedule so am connected to the start node -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartNodeName"/>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

</xsl:choose>

<xsl:if test="not(following::model/@ep or following::loop or parent::loop or $myNodeType='startLoop' or following::converge or parent::converge or $myNodeType='startConverge')">

  <!-- I am the last object in the schedule so am connected to the end node -->

    <xsl:variable name="toNodeName">
      <xsl:call-template name="getEndNodeName"/>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$myNodeName"/>
      <xsl:with-param name="toNodeName" select="$toNodeName"/>
    </xsl:call-template>

</xsl:if>

</xsl:template>


<xsl:template name="createModelNode">
  <xsl:param name="name"/>
  <xsl:param name="modelType"/>
    <node id="{$name}">
      <data key="d5"/>
      <data key="d6">
        <y:GenericNode configuration="com.yworks.bpmn.Activity.withShadow">
          <xsl:choose>
            <xsl:when test="$modelType='transformation'">
          <y:Geometry height="23.0" width="43.0" x="60.0" y="-60.0"/>
            </xsl:when>
            <xsl:otherwise>
          <y:Geometry height="55.0" width="85.0" x="60.0" y="-60.0"/>
            </xsl:otherwise>
          </xsl:choose>
          <y:Fill color="#FFFFFFE6" color2="#D4D4D4CC" transparent="false"/>
          <y:BorderStyle color="#123EA2" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="4.0" modelName="internal" modelPosition="c" textColor="#000000" visible="true" width="4.0" x="40.5" y="25.5"><xsl:value-of select="$name"/></y:NodeLabel>
          <y:StyleProperties>
            <y:Property class="com.yworks.yfiles.bpmn.view.BPMNTypeEnum" name="com.yworks.bpmn.type" value="ACTIVITY_TYPE_STATELESS_TASK_PLAIN"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.line.color" value="#000000"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill" value="#ffffff"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill2" value="#d4d4d4"/>
          </y:StyleProperties>
        </y:GenericNode>
      </data>
    </node>

</xsl:template>

<xsl:template name="endNode">
  <xsl:param name="name"/>
    <node id="{$name}">
      <data key="d5"/>
      <data key="d6">
        <y:GenericNode configuration="com.yworks.bpmn.Event.withShadow">
          <y:Geometry height="30.0" width="30.0" x="456.0" y="-47.5"/>
          <y:Fill color="#FFFFFFE6" color2="#D4D4D4CC" transparent="false"/>
          <y:BorderStyle color="#B11F1F" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" hasText="false" height="4.0" modelName="eight_pos" modelPosition="s" textColor="#000000" visible="true" width="4.0" x="13.0" y="34.0"/>
          <y:StyleProperties>
            <y:Property class="com.yworks.yfiles.bpmn.view.BPMNTypeEnum" name="com.yworks.bpmn.type" value="EVENT_TYPE_PLAIN"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.line.color" value="#000000"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill" value="#ffffff"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill2" value="#d4d4d4"/>
            <y:Property class="com.yworks.yfiles.bpmn.view.EventCharEnum" name="com.yworks.bpmn.characteristic" value="EVENT_CHARACTERISTIC_END"/>
          </y:StyleProperties>
        </y:GenericNode>
      </data>
    </node>
</xsl:template>


<xsl:template name="startNode">
  <xsl:param name="name"/>
    <node id="{$name}">
      <data key="d5"/>
      <data key="d6">
        <y:GenericNode configuration="com.yworks.bpmn.Event.withShadow">
          <y:Geometry height="30.0" width="30.0" x="0.0" y="-47.5"/>
          <y:Fill color="#FFFFFFE6" color2="#D4D4D4CC" transparent="false"/>
          <y:BorderStyle color="#27AE27" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" hasText="false" height="4.0" modelName="eight_pos" modelPosition="s" textColor="#000000" visible="true" width="4.0" x="13.0" y="34.0"/>
          <y:StyleProperties>
            <y:Property class="com.yworks.yfiles.bpmn.view.BPMNTypeEnum" name="com.yworks.bpmn.type" value="EVENT_TYPE_PLAIN"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.line.color" value="#000000"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill" value="#ffffff"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill2" value="#d4d4d4"/>
            <y:Property class="com.yworks.yfiles.bpmn.view.EventCharEnum" name="com.yworks.bpmn.characteristic" value="EVENT_CHARACTERISTIC_START"/>
          </y:StyleProperties>
        </y:GenericNode>
      </data>
    </node>
</xsl:template>

<xsl:template name="loopNode">
  <xsl:param name="name"/>
    <node id="{$name}">
      <data key="d5"/>
      <data key="d6">
        <y:GenericNode configuration="com.yworks.bpmn.Gateway.withShadow">
          <y:Geometry height="45.0" width="45.0" x="175.0" y="-55.0"/>
          <y:Fill color="#FFFFFFE6" color2="#D4D4D4CC" transparent="false"/>
          <y:BorderStyle color="#E38B00" type="line" width="1.0"/>
          <y:NodeLabel alignment="center" autoSizePolicy="content" fontFamily="Dialog" fontSize="12" fontStyle="plain" hasBackgroundColor="false" hasLineColor="false" height="4.0" modelName="eight_pos" modelPosition="ne" textColor="#000000" visible="false" width="4.0" x="49.0" y="-4.0"><xsl:value-of select="$name"/></y:NodeLabel>
          <y:StyleProperties>
            <y:Property class="com.yworks.yfiles.bpmn.view.BPMNTypeEnum" name="com.yworks.bpmn.type" value="GATEWAY_TYPE_PLAIN"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.line.color" value="#000000"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill" value="#ffffff"/>
            <y:Property class="java.awt.Color" name="com.yworks.bpmn.icon.fill2" value="#d4d4d4"/>
          </y:StyleProperties>
        </y:GenericNode>
      </data>
    </node>
</xsl:template>

<xsl:template name="connector">
  <xsl:param name="fromNodeName"/>
  <xsl:param name="toNodeName"/>
    <edge id="{$fromNodeName}_{$toNodeName}" source="{$fromNodeName}" target="{$toNodeName}">
      <data key="d10">
        <y:GenericEdge configuration="com.yworks.bpmn.Connection">
          <y:Path sx="0.0" sy="0.0" tx="-42.5" ty="-0.0"/>
          <y:LineStyle color="#000000" type="line" width="1.0"/>
          <y:Arrows source="none" target="delta"/>
          <y:StyleProperties>
            <y:Property class="com.yworks.yfiles.bpmn.view.BPMNTypeEnum" name="com.yworks.bpmn.type" value="CONNECTION_TYPE_SEQUENCE_FLOW"/>
          </y:StyleProperties>
        </y:GenericEdge>
      </data>
    </edge>
</xsl:template>

</xsl:stylesheet>



