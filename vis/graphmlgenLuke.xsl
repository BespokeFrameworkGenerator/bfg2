<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- RF est 10th January 2013
     Create a graphml representation of a bfg coupled model
     Takes account of additional property information that states
     whether property is mandatory, removable or possible to add
     in and colours accordingly
-->

<!-- options are min, max, all, none -->
<xsl:param name="mode" select="'max'"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:variable name="modelShape">rectangle</xsl:variable>
<xsl:variable name="modelColour">ccffff</xsl:variable>
<xsl:variable name="modelWidth">160.0</xsl:variable>
<xsl:variable name="modelHeight">30.0</xsl:variable>
<!---->
<xsl:variable name="transShape">ellipse</xsl:variable>
<xsl:variable name="transColour">ffcc00</xsl:variable>
<xsl:variable name="transWidth">30.0</xsl:variable>
<xsl:variable name="transHeight">30.0</xsl:variable>
<!---->
<xsl:variable name="lineColour">000000</xsl:variable>
<xsl:variable name="lineWidth">1.0</xsl:variable>
<xsl:variable name="lineBendSmoothed">false</xsl:variable>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">

<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <key id="d0" for="node" yfiles.type="nodegraphics"/>
  <key id="d1" for="edge" yfiles.type="edgegraphics"/>
  <graph id="BFG2Graph" edgedefault="directed">

<!-- output each model and transformation with its name -->
<xsl:for-each select="document($root/coupled/models/model)/definition">

<xsl:choose>
<xsl:when test="type='scientific'">
    <node id="{name}">
      <data key="d0">
        <y:ShapeNode>
          <y:Geometry  x="0.0" y="0.0" width="{$modelWidth}" height="{$modelHeight}"/>
          <y:Fill color="#{$modelColour}"  transparent="false"/>
          <y:NodeLabel><xsl:value-of select="name"/></y:NodeLabel>
          <y:Shape type="{$modelShape}"/>
        </y:ShapeNode>
      </data>
    </node>
</xsl:when>
<xsl:when test="type='transformation'">
    <node id="{name}">
      <data key="d0">
        <y:ShapeNode>
          <y:Geometry  x="0.0" y="0.0" width="{$transWidth}" height="{$transHeight}"/>
          <y:Fill color="#{$transColour}"  transparent="false"/>
          <y:NodeLabel><xsl:value-of select="name"/></y:NodeLabel>
          <y:Shape type="{$transShape}"/>
        </y:ShapeNode>
      </data>
    </node>
</xsl:when>
</xsl:choose>

</xsl:for-each>

<!-- output the connectivity between models -->
<xsl:for-each select="document($root/coupled/composition)//connections/timestepping//connection">
  <xsl:variable name="srcName" select="ancestor::modelChannel/@outModel"/>
  <xsl:variable name="sinkName" select="ancestor::modelChannel/@inModel"/>
  <xsl:variable name="srcID" select="@outID"/>
  <xsl:variable name="sinkID" select="@inID"/>
  <xsl:variable name="srcEP" select="ancestor::epChannel/@outEP"/>
  <xsl:variable name="edgeName">
    <xsl:value-of select="document($root//model)/definition[name=$srcName]//entryPoint[@name=$srcEP]//scalar[@id=$srcID and @direction='out']/@name"/>
  </xsl:variable>
  <xsl:variable name="futureStatus">
    <xsl:value-of select="document($root//model)/definition[name=$srcName]//entryPoint[@name=$srcEP]//data[scalar/@id=$srcID and sclar/@direction='out']/@futureStatus"/>
  </xsl:variable>

  <edge source="{$srcName}" target="{$sinkName}">
    <data key="d1">
      <y:PolyLineEdge >
        <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
        <xsl:choose>
          <xsl:when test="$futureStatus='risk'">
            <y:LineStyle type="line" width="{$lineWidth}" color="#FF0000"/>
          </xsl:when>
          <xsl:otherwise>
            <y:LineStyle type="line" width="{$lineWidth}" color="#{$lineColour}"/>
          </xsl:otherwise>
        </xsl:choose>
        <y:Arrows source="none" target="standard"/>
        <y:EdgeLabel><xsl:value-of select="$edgeName"/></y:EdgeLabel>
        <y:BendStyle smoothed="{$lineBendSmoothed}"/>
      </y:PolyLineEdge>
    </data>
  </edge>
</xsl:for-each>

  </graph>
</graphml>

</xsl:template>

</xsl:stylesheet>



