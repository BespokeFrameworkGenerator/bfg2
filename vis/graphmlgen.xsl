<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- RF est 4th April 2006
     Create a graphml representation of a bfg coupled model
     Update 10th April 2006
     Made to work with latest set notation
     Techniques are to 1: only vis nearest neighbour with set notation
     This is clearly not a full picture of connectivity (it
     potentially underconnects), 2: vis all potential set connections
     (this potentially overconnects), see parameter "mode" for selections
-->

<!-- options are min, max, all, none -->
<xsl:param name="mode" select="'max'"/>
<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:param name="Names" select="'False'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">

<!--
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"
    xmlns:y="http://yworks.com/xml/graphml"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
-->
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:y="http://www.yworks.com/xml/graphml" xmlns:yed="http://www.yworks.com/xml/yed/3" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd">
  <key id="d0" for="node" yfiles.type="nodegraphics"/>
  <key id="d1" for="edge" yfiles.type="edgegraphics"/>
  <graph id="BFG2Graph" edgedefault="directed">

<!-- output each model and transformation with its name -->
<xsl:for-each select="document($root/coupled/models/model)/definition">

<xsl:choose>
<xsl:when test="type='scientific'">
    <node id="{name}">
      <data key="d0">
        <y:ShapeNode>
          <y:Geometry  x="0.0" y="0.0" width="160.0" height="30.0"/>
          <y:Fill color="#ccffff"  transparent="false"/>
          <y:NodeLabel fontSize="12" alignment="center" modelName="internal" modelPosition="c"><xsl:value-of select="name"/></y:NodeLabel>
          <y:Shape type="rectangle"/>
        </y:ShapeNode>
      </data>
    </node>
</xsl:when>
<xsl:when test="type='transformation'">
    <node id="{name}">
      <data key="d0">
        <y:ShapeNode>
          <y:Geometry  x="0.0" y="0.0" width="30.0" height="30.0"/>
          <y:Fill color="#ffcc00"  transparent="false"/>
          <y:NodeLabel fontSize="12" alignment="center" modelName="internal" modelPosition="c"><xsl:value-of select="name"/></y:NodeLabel>
          <y:Shape type="ellipse"/>
        </y:ShapeNode>
      </data>
    </node>
</xsl:when>
</xsl:choose>

</xsl:for-each>

<!-- output the connectivity between models -->
<xsl:for-each select="document($root/coupled/models/model)/definition">
  <xsl:variable name="SrcName" select="name"/>
  <xsl:for-each select="document($root/coupled/models/model)/definition">
    <xsl:variable name="SinkName" select="name"/>
    <xsl:if test="$SrcName!=$SinkName">
      <xsl:variable name="nAtRisk">
        <xsl:call-template name="atRisk">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          <xsl:with-param name="root" select="$root"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="numConnections">
        <xsl:choose>
	<xsl:when test="$mode='min'">
          <xsl:call-template name="countConnectionsMin">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          </xsl:call-template>
	</xsl:when>
	<xsl:when test="$mode='max'">
          <xsl:call-template name="countConnectionsMax">
          <xsl:with-param name="SrcName" select="$SrcName"/>
          <xsl:with-param name="SinkName" select="$SinkName"/>
          </xsl:call-template>
	</xsl:when>
	<xsl:when test="$mode='all'">
	  <xsl:text>1</xsl:text>
	</xsl:when>
	<xsl:when test="$mode='none'">
	  <xsl:text>0</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:message terminate="yes">
          <xsl:text>Error: mode </xsl:text>
          <xsl:value-of select="$mode"/>
          <xsl:text> is not one of [min,max,all,none]</xsl:text>
          </xsl:message>
	</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:if test="$numConnections&gt;0">
<edge source="{$SrcName}" target="{$SinkName}">
      <data key="d1">
        <y:PolyLineEdge >
<!--  yFiles only supports about 4 line sizes
          <y:LineStyle type="line" width="{$numConnections}" color="#000000"/>
-->
          <y:Path sx="0.0" sy="0.0" tx="0.0" ty="0.0"/>
          <xsl:choose>
          <xsl:when test="$nAtRisk&gt;0">
          <y:LineStyle type="line" width="1.0" color="#FF0000"/>
          </xsl:when>
          <xsl:otherwise>
          <y:LineStyle type="line" width="1.0" color="#000000"/>
          </xsl:otherwise>
          </xsl:choose>
          <y:Arrows source="none" target="standard"/>
          <xsl:choose>
            <xsl:when test="$Names='True'">
              <xsl:variable name="labels">
                <xsl:call-template name="getLabels">
                  <xsl:with-param name="SrcName" select="$SrcName"/>
                  <xsl:with-param name="SinkName" select="$SinkName"/>
                </xsl:call-template>
              </xsl:variable>
              <y:EdgeLabel fontSize="12"><xsl:value-of select="$labels"/><xsl:if test="$nAtRisk&gt;0"><xsl:text> (</xsl:text><xsl:value-of select="$nAtRisk"/><xsl:text>)</xsl:text></xsl:if></y:EdgeLabel>
            </xsl:when>
            <xsl:otherwise>
              <y:EdgeLabel fontSize="12"><xsl:value-of select="$numConnections"/><xsl:if test="$nAtRisk&gt;0"><xsl:text> (</xsl:text><xsl:value-of select="$nAtRisk"/><xsl:text>)</xsl:text></xsl:if></y:EdgeLabel>
            </xsl:otherwise>
          </xsl:choose>
          <y:BendStyle smoothed="false"/>
        </y:PolyLineEdge>
      </data>
    </edge>
      </xsl:if>
    </xsl:if>
  </xsl:for-each>
</xsl:for-each>

  </graph>
</graphml>

</xsl:template>

<xsl:template name="atRisk">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>
<xsl:param name="root"/>
    <xsl:variable name="atRisk">
      <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection">
        <xsl:variable name="epName" select="ancestor::epChannel/@outEP"/>
        <xsl:variable name="id" select="@outID"/>
        <xsl:if test="document($root/coupled//model)/definition[name=$SrcName]//entryPoint[@name=$epName]/data[@availability='AtRisk']//scalar[@id=$id]">
          <xsl:text>x</xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="string-length($atRisk)"/>
</xsl:template>

<xsl:template name="getLabels">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>
                <!--
                 source type = 
                 if source is a model print var name
                 elif source is transformation and sink is a model print sink var name
                 else leave blank
                 -->
  <xsl:for-each select="document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection">
    <xsl:variable name="inID" select="@inID"/>
    <xsl:variable name="outID" select="@outID"/>
    <xsl:variable name="inEP" select="ancestor::epChannel/@inEP"/>
    <xsl:variable name="outEP" select="ancestor::epChannel/@outEP"/>
    <xsl:variable name="inModelType" select="document($root/coupled/models/model)/definition[name=$SinkName]/type"/>
    <xsl:variable name="outModelType" select="document($root/coupled/models/model)/definition[name=$SrcName]/type"/>
    <xsl:choose>
    <xsl:when test="$outModelType='scientific'">
      <xsl:value-of select="document($root/coupled/models/model)/definition[name=$SrcName]/entryPoints/entryPoint[@name=$outEP]//scalar[@id=$outID and @direction='out']/@name"/>
    </xsl:when>
    <xsl:when test="$inModelType='scientific'">
      <xsl:value-of select="document($root/coupled/models/model)/definition[name=$SinkName]/entryPoints/entryPoint[@name=$inEP]//scalar[@id=$inID and @direction='in']/@name"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="document($root/coupled/models/model)/definition[name=$SrcName]/entryPoints/entryPoint[@name=$outEP]//scalar[@id=$outID and @direction='out']/@name"/>
    </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#10;</xsl:text>
  </xsl:for-each>

</xsl:template>

<xsl:template name="countConnectionsMax">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>

      <xsl:variable name="numPtoPConnections" select="count(document($root/coupled/composition)/composition/connections/timestepping/modelChannel[@outModel=$SrcName and @inModel=$SinkName]/epChannel/connection)"/>

      <xsl:variable name="numSetConnections" select="count(document($root/coupled/composition)/composition/connections/timestepping/set[field/@modelName=$SrcName and field/@modelName=$SinkName])"/>

      <xsl:value-of select="$numPtoPConnections+$numSetConnections"/>
</xsl:template>

<xsl:template name="countConnectionsMin">
<xsl:param name="SrcName"/>
<xsl:param name="SinkName"/>
<!-- to be written -->
</xsl:template>

</xsl:stylesheet>



