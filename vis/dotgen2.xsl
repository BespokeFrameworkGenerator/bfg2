<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:y="http://www.yworks.com/xml/graphml" xmlns="http://graphml.graphdrawing.org/xmlns">

<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument,/code)"/>

<xsl:output method="text"/>

<xsl:template match="/">

<xsl:text>digraph graphname {
</xsl:text>

    <xsl:variable name="startNodeName">
      <xsl:call-template name="getStartNodeName"/>
    </xsl:variable>

    <xsl:call-template name="startNode">
      <xsl:with-param name="name" select="$startNodeName"/>
    </xsl:call-template>

    <xsl:apply-templates select="document($root//deployment)//schedule/bfgiterate"/>

    <xsl:variable name="endNodeName">
      <xsl:call-template name="getEndNodeName"/>
    </xsl:variable>

    <xsl:call-template name="endNode">
      <xsl:with-param name="name" select="$endNodeName"/>
    </xsl:call-template>

<xsl:text>}
</xsl:text>

</xsl:template>

<!-- remove all white space -->
<xsl:template match="node()">
  <xsl:copy>
    <xsl:copy-of select="@*" />
    <xsl:copy-of select="normalize-space(text())" />
    <xsl:apply-templates select="*"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="converge">

  <xsl:variable name="startConvergeName">
    <xsl:call-template name="getStartConvergeName">
      <xsl:with-param name="convergeNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$startConvergeName"/>
  </xsl:call-template>

  <xsl:apply-templates/>

  <xsl:variable name="endConvergeName">
    <xsl:call-template name="getEndConvergeName">
      <xsl:with-param name="convergeNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$endConvergeName"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$startConvergeName"/>
    <xsl:with-param name="myNodeType" select="'startConverge'"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$endConvergeName"/>
    <xsl:with-param name="myNodeType" select="'endConverge'"/>
  </xsl:call-template>

  <xsl:call-template name="connector">
    <xsl:with-param name="fromNodeName" select="$endConvergeName"/>
    <xsl:with-param name="toNodeName" select="$startConvergeName"/>
  </xsl:call-template>

</xsl:template>

<xsl:template match="loop">

  <xsl:variable name="startLoopName">
    <xsl:call-template name="getStartLoopName">
      <xsl:with-param name="loopNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$startLoopName"/>
  </xsl:call-template>

  <xsl:apply-templates/>

  <xsl:variable name="endLoopName">
    <xsl:call-template name="getEndLoopName">
      <xsl:with-param name="loopNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:call-template name="loopNode">
    <xsl:with-param name="name" select="$endLoopName"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$startLoopName"/>
    <xsl:with-param name="myNodeType" select="'startLoop'"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$endLoopName"/>
    <xsl:with-param name="myNodeType" select="'endLoop'"/>
  </xsl:call-template>

  <xsl:call-template name="connector">
    <xsl:with-param name="fromNodeName" select="$endLoopName"/>
    <xsl:with-param name="toNodeName" select="$startLoopName"/>
  </xsl:call-template>

</xsl:template>

<xsl:template match="model">

  <xsl:variable name="myNodeName">
    <xsl:call-template name="getModelName">
      <xsl:with-param name="modelNode" select="."/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="modelName">
    <xsl:value-of select="@name"/>
  </xsl:variable>

  <xsl:variable name="epName">
    <xsl:value-of select="@ep"/>
  </xsl:variable>

  <xsl:variable name="modelType">
    <xsl:value-of select="document($root//model)/definition[name=$modelName and //entryPoint/@name=$epName]/type"/>
  </xsl:variable>

  <xsl:call-template name="createModelNode">
    <xsl:with-param name="name" select="$myNodeName"/>
    <xsl:with-param name="modelType" select="$modelType"/>
  </xsl:call-template>

  <xsl:call-template name="createEdges">
    <xsl:with-param name="myNodeName" select="$myNodeName"/>
    <xsl:with-param name="myNodeType" select="'model'"/>
  </xsl:call-template>

</xsl:template>

<xsl:template name="getStartNodeName">
  <xsl:text>startNode</xsl:text>
</xsl:template>

<xsl:template name="getEndNodeName">
  <xsl:text>endNode</xsl:text>
</xsl:template>

<xsl:template name="getStartConvergeName">
  <xsl:param name="convergeNode"/>
  <xsl:text>startConverge</xsl:text>
  <xsl:call-template name="getConvergeID">
    <xsl:with-param name="convergeNode" select="$convergeNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getEndConvergeName">
  <xsl:param name="convergeNode"/>
  <xsl:text>endConverge</xsl:text>
  <xsl:call-template name="getConvergeID">
    <xsl:with-param name="convergeNode" select="$convergeNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getStartLoopName">
  <xsl:param name="loopNode"/>
  <xsl:text>startLoop</xsl:text>
  <xsl:call-template name="getLoopID">
    <xsl:with-param name="loopNode" select="$loopNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getEndLoopName">
  <xsl:param name="loopNode"/>
  <xsl:text>endLoop</xsl:text>
  <xsl:call-template name="getLoopID">
    <xsl:with-param name="loopNode" select="$loopNode"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="getConvergeID">
  <xsl:param name="convergeNode"/>
      <xsl:for-each select="document($root//deployment)//schedule//converge">
         <xsl:if test="generate-id(.)=generate-id($convergeNode)">
           <xsl:value-of select="position()"/>
         </xsl:if>
      </xsl:for-each>
</xsl:template>

<xsl:template name="getLoopID">
  <xsl:param name="loopNode"/>
      <xsl:for-each select="document($root//deployment)//schedule//loop">
         <xsl:if test="generate-id(.)=generate-id($loopNode)">
           <xsl:value-of select="position()"/>
         </xsl:if>
      </xsl:for-each>
</xsl:template>

<xsl:template name="getModelName">
  <xsl:param name="modelNode"/>
  <xsl:value-of select="$modelNode/@name"/>
  <xsl:text>_</xsl:text>
  <xsl:value-of select="$modelNode/@ep"/>
</xsl:template>


<xsl:template name="createEdges">
  <xsl:param name="myNodeName"/>
  <xsl:param name="myNodeType"/>

  <xsl:variable name="children" select="count(child::*)"/>

  <!--
  <xsl:message terminate="no">
    <xsl:text>Found </xsl:text>
    <xsl:value-of select="$children"/>
    <xsl:text> children for nodeName </xsl:text>
    <xsl:value-of select="$myNodeName"/>
    <xsl:text> and nodeType </xsl:text>
    <xsl:value-of select="$myNodeType"/>
  </xsl:message>
  -->
  <xsl:choose>

  <xsl:when test="$children>0 and local-name(child::*[$children])='model' and ($myNodeType='endLoop' or $myNodeType='endConverge')">

    <!-- I am the end of an iteration or convergence loop with a child model that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="$children>0 and local-name(child::*[$children])='loop' and $myNodeType='endLoop'">

    <!-- I am the end of an iteration loop with a child iteration loop that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndLoopName">
        <xsl:with-param name="loopNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="$children>0 and local-name(child::*[$children])='converge' and $myNodeType='endConverge'">

    <!-- I am the end of a convergence loop with a child convergence loop that connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndConvergeName">
        <xsl:with-param name="convergeNode" select="child::*[$children]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='model'">

    <!-- a model connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding-sibling::*) and parent::iterate and ancestor::bfgiterate/init/model">

  <!-- I am the first iterate element and at least one model exists within init, the last of which connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getModelName">
        <xsl:with-param name="modelNode" select="ancestor::bfgiterate/init/model[last()]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding-sibling::*) and parent::final">

  <!-- I am the first 'final' element, 'iterate' elements must exist for valid BFG metadata, the last of which connects to me -->

    <xsl:variable name="fromNodeName">
      <xsl:choose>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='converge'">
          <xsl:call-template name="getEndConvergeName">
            <xsl:with-param name="convergeNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='loop'">
          <xsl:call-template name="getEndLoopName">
            <xsl:with-param name="loopNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="local-name(ancestor::bfgiterate/iterate/*[last()])='model'">
          <xsl:call-template name="getModelName">
            <xsl:with-param name="modelNode" select="ancestor::bfgiterate/iterate/*[last()]"/>
          </xsl:call-template>
        </xsl:when>
    </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='loop'">

    <!-- An iteration loop connects to me and I am its sibling (so I am connected to the end of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndLoopName">
        <xsl:with-param name="loopNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="local-name(preceding-sibling::*[1])='converge'">

    <!-- A convergence loop connects to me and I am its sibling (so I am connected to the end of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getEndConvergeName">
        <xsl:with-param name="convergeNode" select="preceding-sibling::*[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="parent::loop">

    <!-- An iteration loop connects to me and I am its child (so I am connected to the start of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartLoopName">
        <xsl:with-param name="loopNode" select="parent::loop[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="parent::converge">

    <!-- A convergence loop connects to me and I am its child (so I am connected to the start of the loop) -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartConvergeName">
        <xsl:with-param name="convergeNode" select="parent::converge[1]"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

  <xsl:when test="not(preceding::model/@ep or preceding::loop or preceding::converge)">

    <!-- I am the first object in the schedule so am connected to the start node -->

    <xsl:variable name="fromNodeName">
      <xsl:call-template name="getStartNodeName"/>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$fromNodeName"/>
      <xsl:with-param name="toNodeName" select="$myNodeName"/>
    </xsl:call-template>

  </xsl:when>

</xsl:choose>

<xsl:if test="not(following::model/@ep or following::loop or parent::loop or $myNodeType='startLoop' or following::converge or parent::converge or $myNodeType='startConverge')">

  <!-- I am the last object in the schedule so am connected to the end node -->

    <xsl:variable name="toNodeName">
      <xsl:call-template name="getEndNodeName"/>
    </xsl:variable>

    <xsl:call-template name="connector">
      <xsl:with-param name="fromNodeName" select="$myNodeName"/>
      <xsl:with-param name="toNodeName" select="$toNodeName"/>
    </xsl:call-template>

</xsl:if>

</xsl:template>


<xsl:template name="createModelNode">
  <xsl:param name="name"/>
  <xsl:param name="modelType"/>
    <xsl:value-of select="$name"/>
    <xsl:choose>
      <xsl:when test="$modelType='transformation'">
        <xsl:text>[color="black",style="filled",fillcolor="beige",shape="box"];
</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>[color="black", style="filled", fillcolor="khaki", shape="box"];
</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

</xsl:template>

<xsl:template name="endNode">
  <xsl:param name="name"/>
    <xsl:value-of select="$name"/>
    <xsl:text>[label="",color="black",style="filled",fillcolor="indianred",shape="circle"];
</xsl:text>
</xsl:template>


<xsl:template name="startNode">
  <xsl:param name="name"/>
    <xsl:value-of select="$name"/>
    <xsl:text>[label="",color="black",style="filled",fillcolor="palegreen",shape="circle"];
</xsl:text>
</xsl:template>

<xsl:template name="loopNode">
  <xsl:param name="name"/>
    <xsl:value-of select="$name"/>
    <xsl:text>[color="black",style="filled",fillcolor="azure",shape="Mdiamond"];
</xsl:text>
</xsl:template>

<xsl:template name="connector">
  <xsl:param name="fromNodeName"/>
  <xsl:param name="toNodeName"/>
        <xsl:value-of select="$fromNodeName"/>
        <xsl:text>-&gt;</xsl:text>
        <xsl:value-of select="$toNodeName"/>
        <xsl:text>[label=""];
</xsl:text>
</xsl:template>

</xsl:stylesheet>

