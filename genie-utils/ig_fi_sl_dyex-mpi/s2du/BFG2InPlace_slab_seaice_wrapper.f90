       ! f77 to f90 put/get wrappers start
       subroutine put_slab_seaice(data,tag)
       use BFG2Target2
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-105) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-107) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-276) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==21) then
       if (tag==-105) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-367) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-371) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-373) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-375) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-325) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-329) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-108) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-109) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-377) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-379) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-276) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-107) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine put_slab_seaice
       subroutine get_slab_seaice(data,tag)
       use BFG2Target2
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-105) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-107) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-276) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==21) then
       if (tag==-105) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-367) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-371) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-373) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-375) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-325) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-329) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-108) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-109) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-377) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-379) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-276) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-107) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine get_slab_seaice
       subroutine getreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_slab_seaice
       subroutine putreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_slab_seaice
       ! f77 to f90 put/get wrappers end
