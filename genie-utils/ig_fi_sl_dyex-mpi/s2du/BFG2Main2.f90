       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !fixed_ocean__freq
       integer :: fixed_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,fixed_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_ocn
       real, dimension(1:36) :: r7
       !alat1_ocn
       real, dimension(1:36) :: r8
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r12
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_ocn
       real, dimension(1:36,1:36) :: r15
       !sensible_ocn
       real, dimension(1:36,1:36) :: r18
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_ocn
       real, dimension(1:36,1:36) :: r21
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_ocn
       real, dimension(1:36,1:36) :: r24
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_ocn
       real, dimension(1:36,1:36) :: r27
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !evap_ocn
       real, dimension(1:36,1:36) :: r33
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_ocn
       real, dimension(1:36,1:36) :: r36
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_ocn
       real, dimension(1:36,1:36) :: r39
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r42
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_ocn
       real, dimension(1:36,1:36) :: r45
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_ocn
       real, dimension(1:36,1:36) :: r48
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_ocn
       real, dimension(1:36,1:36) :: r51
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !energycarry_ocn_ice
       real, dimension(1:64,1:32) :: r86
       !dtcarry_ocn_ice
       real, dimension(1:64,1:32) :: r87
       ! Set Notation Vars
       !conductflux_atm
       real, dimension(1:64,1:32) :: r277
       !ksic_loop
       integer :: r281
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r107
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r375
       !istep_sic
       integer :: r739
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r405
       !netsolar_atm
       real, dimension(1:64,1:32) :: r108
       !alat1_atm
       real, dimension(1:32) :: r93
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r390
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r394
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r30
       !iconv_che
       integer :: r175
       !alon1_atm
       real, dimension(1:64) :: r92
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r325
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r399
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r329
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !dt_write
       integer :: r59
       !netlong_atm
       real, dimension(1:64,1:32) :: r109
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !istep_lic
       integer :: r2050
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r377
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r409
       !write_flag_sic
       logical :: r3
       !write_flag_atm
       logical :: r1
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r367
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !fname_restart_main
       character(len=200) :: r58
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r411
       !istep_che
       integer :: r1613
       !outputdir_name
       character(len=200) :: r60
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r373
       !test_energy_seaice
       real :: r279
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r379
       !istep_ocn
       integer :: r1176
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r276
       !test_water_seaice
       real :: r280
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r371
       !iconv_ice
       integer :: r151
       !temptop_atm
       real, dimension(1:64,1:32) :: r295
       !kocn_loop
       integer :: r383
       !tstar_atm
       real, dimension(1:64,1:32) :: r105
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r386
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r91
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r401
       !genie_timestep
       real :: r57
       !ilon1_atm
       integer :: r90
       !write_flag_ocn
       logical :: r2
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r407
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r403
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r277=0.0
           r281=6
           r75=0
           r107=0.0
           r375=0.0
           r739=0
           r405=0.0
           r108=0.0
           r93=0.0
           r390=0.0
           r381=0.0
           r63=0.0
           r394=0.0
           r31=0.0
           r30=0.0
           r175=0
           r92=0.0
           r413=0.0
           r325=0.0
           r399=0.0
           r329=0.0
           r62=0.0
           r59=720
           r109=0.0
           r76=0.0
           r77=0.0
           r2050=0
           r377=0.0
           r409=0.0
           r3=.false.
           r1=.true.
           r367=0.0
           r41=0.0
           r61=0.0
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r16=0.0
           r411=0.0
           r1613=0
           r60='/home/armstroc/genie_output/genie_ig_fi_sl_dyex/main'
           r373=0.0
           r279=0.0
           r379=0.0
           r1176=0
           r276=0.0
           r280=0.0
           r371=0.0
           r151=0
           r295=0.0
           r383=48
           r105=0.0
           r386=0.0
           r78=0.0
           r91=32
           r401=0.0
           r57=3600.0
           r90=64
           r2=.true.
           r407=0.0
           r403=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call bfg_averages_ini_averages_init(r1,r2,r3)
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r75,r76,r77,r78)
       call put_initialise_fixedicesheet_mod(r75,-75)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call initialise_fixedchem(r61,r62,r63)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r107,-107)
       call get_slab_seaice(r276,-276)
       call initialise_slabseaice(r105,r107,r276,r277,r75,r279,r280,r281)
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r107,-107)
       call put_slab_seaice(r276,-276)
       end if
       if (fixed_oceanThread()) then
       call setActiveModel(7)
       call get_fixed_ocean(r105,-105)
       call get_fixed_ocean(r107,-107)
       call get_fixed_ocean(r276,-276)
       call initialise_fixedocean(r105,r107,r276,r75)
       call put_fixed_ocean(r105,-105)
       call put_fixed_ocean(r107,-107)
       call put_fixed_ocean(r276,-276)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call counter(r739)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r367,-367)
       call get_slab_seaice(r371,-371)
       call get_slab_seaice(r373,-373)
       call get_slab_seaice(r375,-375)
       call get_slab_seaice(r325,-325)
       call get_slab_seaice(r329,-329)
       call get_slab_seaice(r108,-108)
       call get_slab_seaice(r109,-109)
       call get_slab_seaice(r377,-377)
       call get_slab_seaice(r379,-379)
       call get_slab_seaice(r276,-276)
       call get_slab_seaice(r107,-107)
       call slabseaice(r739,r105,r367,r371,r373,r375,r325,r329,r108,r109,r377,r379,r276,r295,r277,r107,r75,r279,r280,r281)
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r367,-367)
       call put_slab_seaice(r371,-371)
       call put_slab_seaice(r373,-373)
       call put_slab_seaice(r375,-375)
       call put_slab_seaice(r325,-325)
       call put_slab_seaice(r329,-329)
       call put_slab_seaice(r108,-108)
       call put_slab_seaice(r109,-109)
       call put_slab_seaice(r377,-377)
       call put_slab_seaice(r379,-379)
       call put_slab_seaice(r276,-276)
       call put_slab_seaice(r107,-107)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call get_transformer4(r276,-276)
       call new_transformer_4(r90,r91,r386,r276,r281,r383,r390,r277)
       call put_transformer4(r276,-276)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(24)
       call counter(r1176)
       end if
       end if
       if (fixed_oceanThread()) then
       if(mod(its1,fixed_ocean__freq).eq.0)then
       call setActiveModel(25)
       call get_fixed_ocean(r105,-105)
       call get_fixed_ocean(r276,-276)
       call get_fixed_ocean(r107,-107)
       call fixedocean(r1176,r105,r276,r86,r87,r107,r75)
       call put_fixed_ocean(r105,-105)
       call put_fixed_ocean(r276,-276)
       call put_fixed_ocean(r107,-107)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(26)
       call counter(r1613)
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(27)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r175,-175)
       call fixedchem(r1613,r61,r62,r63,r175)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r175,-175)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(28)
       call counter(r2050)
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(29)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r151,-151)
       call fixedicesheet(r2050,r75,r76,r77,r78,r151)
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r151,-151)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(30)
       call get_bfg_averages(r92,-92)
       call get_bfg_averages(r93,-93)
       call get_bfg_averages(r401,-401)
       call get_bfg_averages(r403,-403)
       call get_bfg_averages(r399,-399)
       call get_bfg_averages(r394,-394)
       call get_bfg_averages(r405,-405)
       call get_bfg_averages(r407,-407)
       call get_bfg_averages(r411,-411)
       call get_bfg_averages(r409,-409)
       call get_bfg_averages(r413,-413)
       call get_bfg_averages(r105,-105)
       call get_bfg_averages(r107,-107)
       call bfg_averages_write_averages_iteration(r1176,r92,r93,r7,r8,r9,r10,r401,r12,r13,r403,r15,r16,r399,r18,r19,r394,r21,r22,r405,r24,r25,r407,r27,r28,r390,r30,r31,r411,r33,r34,r409,r36,r37,r413,r39,r40,r41,r42,r43,r386,r45,r46,r105,r48,r49,r107,r51,r52,r1,r2,r3,r383,r57,r58,r59,r60)
       call put_bfg_averages(r92,-92)
       call put_bfg_averages(r93,-93)
       call put_bfg_averages(r401,-401)
       call put_bfg_averages(r403,-403)
       call put_bfg_averages(r399,-399)
       call put_bfg_averages(r394,-394)
       call put_bfg_averages(r405,-405)
       call put_bfg_averages(r407,-407)
       call put_bfg_averages(r411,-411)
       call put_bfg_averages(r409,-409)
       call put_bfg_averages(r413,-413)
       call put_bfg_averages(r105,-105)
       call put_bfg_averages(r107,-107)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call get_transformer6(r367,-367)
       call get_transformer6(r371,-371)
       call get_transformer6(r373,-373)
       call get_transformer6(r375,-375)
       call get_transformer6(r377,-377)
       call get_transformer6(r379,-379)
       call get_transformer6(r381,-381)
       call new_transformer_6(r90,r91,r367,r371,r373,r375,r377,r379,r381)
       call put_transformer6(r367,-367)
       call put_transformer6(r371,-371)
       call put_transformer6(r373,-373)
       call put_transformer6(r375,-375)
       call put_transformer6(r377,-377)
       call put_transformer6(r379,-379)
       call put_transformer6(r381,-381)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(32)
       call get_transformer7(r394,-394)
       call get_transformer7(r399,-399)
       call get_transformer7(r401,-401)
       call get_transformer7(r403,-403)
       call get_transformer7(r405,-405)
       call get_transformer7(r407,-407)
       call get_transformer7(r409,-409)
       call get_transformer7(r411,-411)
       call get_transformer7(r413,-413)
       call new_transformer_7(r90,r91,r394,r399,r401,r403,r405,r407,r409,r411,r413,r386,r390,r41)
       call put_transformer7(r394,-394)
       call put_transformer7(r399,-399)
       call put_transformer7(r401,-401)
       call put_transformer7(r403,-403)
       call put_transformer7(r405,-405)
       call put_transformer7(r407,-407)
       call put_transformer7(r409,-409)
       call put_transformer7(r411,-411)
       call put_transformer7(r413,-413)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main

