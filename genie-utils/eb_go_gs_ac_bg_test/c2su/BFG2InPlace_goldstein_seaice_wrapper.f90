       ! f77 to f90 put/get wrappers start
       subroutine put_goldstein_seaice(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==3) then
       if (tag==-256) then
       call putreal__2_goldstein_seaice(data,tag)
       end if
       if (tag==-257) then
       call putreal__2_goldstein_seaice(data,tag)
       end if
       end if
       if (currentModel==14) then
       if (tag==-256) then
       call putreal__2_goldstein_seaice(data,tag)
       end if
       if (tag==-257) then
       call putreal__2_goldstein_seaice(data,tag)
       end if
       end if
       end subroutine put_goldstein_seaice
       subroutine putreal__2_goldstein_seaice(data,tag)
       use BFG2InPlace_goldstein_seaice, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_goldstein_seaice
       ! f77 to f90 put/get wrappers end
