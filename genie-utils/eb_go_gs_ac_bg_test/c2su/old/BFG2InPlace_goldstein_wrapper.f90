       ! f77 to f90put/get wrappers start
       subroutine put_goldstein(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==1) then
       if (tag==-187) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-188) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-189) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-191) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-194) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-195) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-196) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-197) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-198) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-199) then
       call putreal__0_goldstein(data,tag)
       end if
       if (tag==-200) then
       call putreal__0_goldstein(data,tag)
       end if
       end if
       if (currentModel==1) then
       if (tag==-190) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-202) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-203) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-207) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-208) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-209) then
       call putreal__1_goldstein(data,tag)
       end if
       if (tag==-210) then
       call putreal__1_goldstein(data,tag)
       end if
       end if
       if (currentModel==1) then
       if (tag==-192) then
       call putinteger__1_goldstein(data,tag)
       end if
       if (tag==-193) then
       call putinteger__1_goldstein(data,tag)
       end if
       if (tag==-204) then
       call putinteger__1_goldstein(data,tag)
       end if
       if (tag==-205) then
       call putinteger__1_goldstein(data,tag)
       end if
       end if
       if (currentModel==1) then
       if (tag==-201) then
       call putinteger__2_goldstein(data,tag)
       end if
       end if
       if (currentModel==1) then
       if (tag==-206) then
       call putinteger__0_goldstein(data,tag)
       end if
       end if
       if (currentModel==1) then
       if (tag==-211) then
       call putreal__4_goldstein(data,tag)
       end if
       if (tag==-212) then
       call putreal__4_goldstein(data,tag)
       end if
       end if
       if (currentModel==15) then
       if (tag==-211) then
       call putreal__4_goldstein(data,tag)
       end if
       if (tag==-212) then
       call putreal__4_goldstein(data,tag)
       end if
       if (tag==-238) then
       call putreal__4_goldstein(data,tag)
       end if
       end if
       if (currentModel==15) then
       if (tag==-237) then
       call putreal__2_goldstein(data,tag)
       end if
       end if
       if (currentModel==15) then
       if (tag==-239) then
       call putreal__3_goldstein(data,tag)
       end if
       end if
       end subroutine put_goldstein
       subroutine get_goldstein(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==1) then
       if (tag==-211) then
       call getreal__4_goldstein(data,tag)
       end if
       if (tag==-212) then
       call getreal__4_goldstein(data,tag)
       end if
       end if
       if (currentModel==15) then
       if (tag==-211) then
       call getreal__4_goldstein(data,tag)
       end if
       if (tag==-212) then
       call getreal__4_goldstein(data,tag)
       end if
       end if
       end subroutine get_goldstein
       subroutine getreal__4_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : get=>getreal__4
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__4_goldstein
       subroutine putreal__0_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__0
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__0_goldstein
       subroutine putreal__1_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__1
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__1_goldstein
       subroutine putinteger__1_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putinteger__1
       implicit none
       integer , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__1_goldstein
       subroutine putinteger__2_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putinteger__2
       implicit none
       integer , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__2_goldstein
       subroutine putinteger__0_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putinteger__0
       implicit none
       integer , intent(out) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__0_goldstein
       subroutine putreal__4_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__4
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__4_goldstein
       subroutine putreal__2_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_goldstein
       subroutine putreal__3_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__3
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__3_goldstein
       ! f77 to f90 put/get wrappers end
