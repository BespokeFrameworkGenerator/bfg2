       module BFG2InPlace_biogem
       use BFG2Target1
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__0,getinteger__1,getinteger__0,getinteger__2,getreal__1,getreal__4,getreal__3,getreal__2
       end interface
       interface put
       module procedure putreal__4,putreal__3
       end interface
       contains
       subroutine getreal__0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(13) :: pp2p=(/0,0,0,0,0,0,0,0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogemand ep=initialise_biogem and instance=
       if (arg2==-187) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,187,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-188) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remotesource
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,188,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-189) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,189,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-190) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,190,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-191) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,191,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-194) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remotesource
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,194,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-195) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,195,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-196) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,196,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-197) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,197,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-198) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(10).eq.1) then
       pp2p(10)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,198,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-199) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(11).eq.1) then
       pp2p(11)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,199,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-79) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(12).eq.1) then
       pp2p(12)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,79,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-200) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(13).eq.1) then
       pp2p(13)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_real,du,200,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I ammodel=biogem and ep=biogem and instance=
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I ammodel=biogem and ep=end_biogem and instance=
       end if
       end subroutine getreal__0
       subroutine getinteger__1(arg1,arg2)
       implicit none
       integer , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(4) :: pp2p=(/0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-192) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=36
       !call mpi_recv(arg1,mysize,mpi_integer,du,192,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-193) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=36
       !call mpi_recv(arg1,mysize,mpi_integer,du,193,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-204) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=36
       !call mpi_recv(arg1,mysize,mpi_integer,du,204,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-205) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=36
       !call mpi_recv(arg1,mysize,mpi_integer,du,205,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem and instance=
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem andep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getinteger__1
       subroutine getinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-206) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       !call mpi_recv(arg1,mysize,mpi_integer,du,206,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem and instance=
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getinteger__0
       subroutine getinteger__2(arg1,arg2)
       implicit none
       integer , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-201) then
       ! Iam biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receivefrom remote source
       du=remoteDU
       mysize=1296
       !call mpi_recv(arg1,mysize,mpi_integer,du,201,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem andinstance=
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogemand instance=
       end if
       end subroutine getinteger__2
       subroutine getreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(7) :: pp2p=(/0,0,0,0,0,0,1/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-202) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=8
       !call mpi_recv(arg1,mysize,mpi_real,du,202,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-203) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=8
       !call mpi_recv(arg1,mysize,mpi_real,du,203,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-207) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=37
       !call mpi_recv(arg1,mysize,mpi_real,du,207,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-208) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive fromremote source
       du=remoteDU
       mysize=37
       !call mpi_recv(arg1,mysize,mpi_real,du,208,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-209) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=37
       !call mpi_recv(arg1,mysize,mpi_real,du,209,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-210) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=37
       !call mpi_recv(arg1,mysize,mpi_real,du,210,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem and instance=
       if (arg2==-46) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/10,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=36
       !call mpi_recv(arg1,mysize,mpi_real,du,46,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==17) then
       ! I ammodel=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getreal__1
       subroutine getreal__4(arg1,arg2)
       implicit none
       real , dimension(:,:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(5) :: pp2p=(/0,0,1,1,1/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-211) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/1,4,15,16/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=145152
       !call mpi_recv(arg1,mysize,mpi_real,du,211,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-212) then
       ! I am biogem.initialise_biogem..4
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/1,4,15,16/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=145152
       !call mpi_recv(arg1,mysize,mpi_real,du,212,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem and instance=
       if (arg2==-211) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/1,4,15,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=145152
       !call mpi_recv(arg1,mysize,mpi_real,du,211,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-212) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/1,4,15,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=145152
       !call mpi_recv(arg1,mysize,mpi_real,du,212,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-238) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/15,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remotesource
       du=remoteDU
       mysize=31104
       !call mpi_recv(arg1,mysize,mpi_real,du,238,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getreal__4
       subroutine getreal__3(arg1,arg2)
       implicit none
       real , dimension(:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(5) :: pp2p=(/0,1,1,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-121) then
       ! Iam biogem.initialise_biogem..4
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/4,6,10,16,21/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receivefrom remote source
       du=remoteDU
       mysize=24624
       !call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem andinstance=
       if (arg2==-239) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/15,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2592
       !call mpi_recv(arg1,mysize,mpi_real,du,239,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-121) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/4,6,10,16,21/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=24624
       !call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       if (arg2==-121) then
       ! I am biogem.diag_biogem_timeslice..17
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/4,6,10,16,17,21/)
       myID=17
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=24624
       !call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       if (arg2==-121) then
       ! I am biogem.diag_biogem_timeseries..18
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/4,6,10,16,18,21/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=24624
       !call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getreal__3
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(4) :: pp2p=(/1,1,1,1/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       end if
       if (currentModel==16) then
       ! I am model=biogemand ep=biogem and instance=
       if (arg2==-256) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,10,14,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1296
       !call mpi_recv(arg1,mysize,mpi_real,du,256,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-257) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,10,14,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1296
       !call mpi_recv(arg1,mysize,mpi_real,du,257,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-237) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/15,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1296
       !call mpi_recv(arg1,mysize,mpi_real,du,237,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-47) then
       ! I am biogem.biogem..16
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/10,16/)
       myID=16
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1296
       !call mpi_recv(arg1,mysize,mpi_real,du,47,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine getreal__2
       subroutine putreal__4(arg1,arg2)
       implicit none
       real , dimension(:,:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem and instance=
       if (arg2==-211) then
       ! I am biogem.initialise_biogem..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,4,15,16/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       print*,"getNext=",remoteID
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,211,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! sendto remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,211,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-212) then
       ! I am biogem.initialise_biogem..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,4,15,16/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,212,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,212,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==16) then
       ! Iam model=biogem and ep=biogem and instance=
       if (arg2==-211) then
       ! I am biogem.biogem..16
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,4,15,16/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=16
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,211,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,211,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-212) then
       ! I am biogem.biogem..16
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,4,15,16/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=16
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,212,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=145152
       call mpi_bsend(arg1,mysize,mpi_real,du,212,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine putreal__4
       subroutine putreal__3(arg1,arg2)
       implicit none
       real , dimension(:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=biogem and ep=initialise_biogem andinstance=
       if (arg2==-121) then
       ! I am biogem.initialise_biogem..4
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/4,6,10,16,17,18,21/)
       ins=(/0,0,0,0,1,1,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send toremote source
       du=remoteDU
       mysize=24624
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=24624
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==16) then
       ! I am model=biogem and ep=biogem and instance=
       if (arg2==-121) then
       ! I am biogem.biogem..16
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/4,6,10,16,17,18,21/)
       ins=(/0,0,0,0,1,1,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=16
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send toremote source
       du=remoteDU
       mysize=24624
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=24624
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==17) then
       ! I am model=biogem and ep=diag_biogem_timeslice and instance=
       end if
       if (currentModel==18) then
       ! I am model=biogem and ep=diag_biogem_timeseries and instance=
       end if
       if (currentModel==22) then
       ! I am model=biogem and ep=rest_biogem and instance=
       end if
       if (currentModel==28) then
       ! I am model=biogem and ep=end_biogem and instance=
       end if
       end subroutine putreal__3
       end module BFG2InPlace_biogem
