       module BFG2InPlace_atchem
       use BFG2Target1
       use mpi
       private 
       public get,put
       interface get
       module procedure getinteger__0,getreal__3
       end interface
       interface put
       module procedure putreal__3
       end interface
       contains
       subroutine getinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(3) :: pp2p=(/0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==5) then
       ! I am model=atchem and ep=initialise_atchem and instance=1
       end if
       if (currentModel==6) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=1
       if (arg2==-48) then
       ! I am atchem.cpl_comp_atmocn.1.6
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/6,10/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,48,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==19) then
       ! I am model=atchem and ep=cpl_flux_ocnatm and instance=1
       if (arg2==-48) then
       ! I am atchem.cpl_flux_ocnatm.1.19
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/10,19/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,48,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==20) then
       ! I am model=atchem and ep=atchem and instance=1
       end if
       if (currentModel==21) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=2
       if (arg2==-48) then
       ! I am atchem.cpl_comp_atmocn.2.21
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/10,21/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,48,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==23) then
       ! I am model=atchem and ep=atchem and instance=2
       end if
       if (currentModel==24) then
       ! I am model=atchem and ep=rest_atchem and instance=1
       end if
       if (currentModel==29) then
       ! I am model=atchem and ep=end_atchem and instance=1
       end if
       end subroutine getinteger__0
       subroutine getreal__3(arg1,arg2)
       implicit none
       real , dimension(:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(2) :: pp2p=(/0,0/)
       currentModel=getActiveModel()
       if (currentModel==5) then
       ! I am model=atchem and ep=initialise_atchem and instance=1
       end if
       if (currentModel==6) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=1
       if (arg2==-121) then
       ! I am atchem.cpl_comp_atmocn.1.6
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/4,6,10,16,21/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==19) then
       ! I am model=atchem andep=cpl_flux_ocnatm and instance=1
       end if
       if (currentModel==20) then
       ! I ammodel=atchem and ep=atchem and instance=1
       end if
       if (currentModel==21) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=2
       if (arg2==-121) then
       ! I am atchem.cpl_comp_atmocn.2.21
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/4,6,10,16,21/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,121,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==23) then
       ! I am model=atchem and ep=atchem and instance=2
       end if
       if (currentModel==24) then
       ! I am model=atchem and ep=rest_atchem and instance=1
       end if
       if (currentModel==29) then
       ! I am model=atchem and ep=end_atchem and instance=1
       end if
       end subroutine getreal__3
       subroutine putreal__3(arg1,arg2)
       implicit none
       real , dimension(:,:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==5) then
       ! I am model=atchem and ep=initialise_atchem and instance=1
       end if
       if (currentModel==6) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=1
       if (arg2==-121) then
       ! I am atchem.cpl_comp_atmocn.1.6
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/4,6,10,16,17,18,21/)
       ins=(/0,0,0,0,1,1,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==19) then
       ! I am model=atchem and ep=cpl_flux_ocnatm and instance=1
       end if
       if (currentModel==20) then
       ! I am model=atchem and ep=atchem and instance=1
       end if
       if (currentModel==21) then
       ! I am model=atchem and ep=cpl_comp_atmocn and instance=2
       if (arg2==-121) then
       ! I am atchem.cpl_comp_atmocn.2.21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/4,6,10,16,17,18,21/)
       ins=(/0,0,0,0,1,1,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,121,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==23) then
       ! I am model=atchem and ep=atchem and instance=2
       end if
       if (currentModel==24) then
       ! I am model=atchem and ep=rest_atchem and instance=1
       end if
       if (currentModel==29) then
       ! I am model=atchem andep=end_atchem and instance=1
       end if
       end subroutine putreal__3
       end module BFG2InPlace_atchem
