       ! f77 to f90 put/get wrappers start
       subroutine put_counter(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==9) then
       if (tag==-3) then
       call putinteger__0_counter(data,tag)
       end if
       end if
       if (currentModel==11) then
       if (tag==-297) then
       call putinteger__0_counter(data,tag)
       end if
       end if
       end subroutine put_counter
       subroutine putinteger__0_counter(data,tag)
       use BFG2InPlace_counter, only : put=>putinteger__0
       implicit none
       integer , intent(out) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__0_counter
       ! f77 tof90 put/get wrappers end
