       module BFG2Target1
       use mpi
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(2)
       integer :: b2mmap(2)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       type modelInfo
       !du
       integer :: du
       !su
       integer :: su
       !period
       integer :: period
       !nesting
       integer :: nesting
       !bound
       integer :: bound
       !offset
       integer :: offset
       !its
       integer, pointer :: its
       end type modelInfo
       !info
       type(modelInfo), dimension(1:29) :: info
       !inf
       integer, parameter :: inf=32767
       integer :: buffsize = 5000000*8
       real, dimension(2*200000) :: buff
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function goldstein_seaiceThread()
       implicit none
       if (bfgSUID==1) then
       goldstein_seaiceThread=.true.
       else
       goldstein_seaiceThread=.false.
       end if
       end function goldstein_seaiceThread
       logical function embmThread()
       implicit none
       if (bfgSUID==1) then
       embmThread=.true.
       else
       embmThread=.false.
       end if
       end function embmThread
       logical function surfluxThread()
       implicit none
       if (bfgSUID==1) then
       surfluxThread=.true.
       else
       surfluxThread=.false.
       end if
       end function surfluxThread
       logical function weight_checkThread()
       implicit none
       if (bfgSUID==1) then
       weight_checkThread=.true.
       else
       weight_checkThread=.false.
       end if
       end function weight_checkThread
       logical function counterinst1Thread()
       implicit none
       if (bfgSUID==1) then
       counterinst1Thread=.true.
       else
       counterinst1Thread=.false.
       end if
       end function counterinst1Thread
       logical function counterinst2Thread()
       implicit none
       if (bfgSUID==1) then
       counterinst2Thread=.true.
       else
       counterinst2Thread=.false.
       end if
       end function counterinst2Thread
       logical function counterinst3Thread()
       implicit none
       if (bfgSUID==1) then
       counterinst3Thread=.true.
       else
       counterinst3Thread=.false.
       end if
       end function counterinst3Thread
       logical function goldsteinThread()
       implicit none
       if (bfgSUID==1) then
       goldsteinThread=.true.
       else
       goldsteinThread=.false.
       end if
       end function goldsteinThread
       logical function bfg_increment_genie_clockThread()
       implicit none
       if (bfgSUID==2) then
       bfg_increment_genie_clockThread=.true.
       else
       bfg_increment_genie_clockThread=.false.
       end if
       end function bfg_increment_genie_clockThread
       logical function biogemThread()
       implicit none
       if (bfgSUID==2) then
       biogemThread=.true.
       else
       biogemThread=.false.
       end if
       end function biogemThread
       logical function atcheminst1Thread()
       implicit none
       if (bfgSUID==2) then
       atcheminst1Thread=.true.
       else
       atcheminst1Thread=.false.
       end if
       end function atcheminst1Thread
       logical function atcheminst2Thread()
       implicit none
       if (bfgSUID==2) then
       atcheminst2Thread=.true.
       else
       atcheminst2Thread=.false.
       end if
       end function atcheminst2Thread
       subroutine commsSync()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       call mpi_barrier(mpi_comm_world,ierr)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine start_mpivis_log(logid)
       implicit none
       character (len=*), intent(in) :: logid
       !globalrank
       integer :: globalrank
       !ierr
       integer :: ierr
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       call mpivis_comp('begin',logid,globalrank)
       end subroutine start_mpivis_log
       subroutine end_mpivis_log(logid)
       implicit none
       character (len=*), intent(in) :: logid
       !globalrank
       integer :: globalrank
       !ierr
       integer :: ierr
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       call mpivis_comp('end',logid,globalrank)
       end subroutine end_mpivis_log
       subroutine initComms()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       !rc
       integer :: rc
       !globalsize
       integer :: globalsize
       !globalrank
       integer :: globalrank
       !colour
       integer :: colour
       !key
       integer :: key
       !localsize
       integer :: localsize
       !localrank
       integer :: localrank
       !b2mtemp(2)
       integer :: b2mtemp(2)
       !mpi_comm_local
       integer :: mpi_comm_local
       call mpi_init(ierr)
       call mpi_comm_size(mpi_comm_world,globalsize,ierr)
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       call mpivis_init(globalrank)
       ! arbitrarily decide on a unique colour for this deployment unit
       colour=1
       if (globalsize.ne.2) then
       print *,"Error: (du",colour,"):","2 threads should be requested"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       key=0
       call mpi_comm_split(mpi_comm_world,colour,key,mpi_comm_local,ierr)
       call mpi_comm_size(mpi_comm_local,localsize,ierr)
       call mpi_comm_rank(mpi_comm_local,localrank,ierr)
       if (localsize.ne.2) then
       print *,"Error: 2 threads expected in du",colour
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! arbitrarilybind model threads to a local rank (and therefore a global rank)
       b2mtemp=0
       if (localrank.ge.0.and.localrank.le.0) then
       ! model name is 'goldstein_seaice'
       ! model name is 'embm'
       ! modelname is 'surflux'
       ! model name is'weight_check'
       ! model name is 'counter'
       ! model name is 'counter'
       ! model name is 'counter'
       ! model name is 'goldstein'
       bfgSUID=1
       if (localrank==0) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.ge.1.and.localrank.le.1) then
       ! model name is 'bfg_increment_genie_clock'
       ! model name is 'biogem'
       ! model name is 'atchem'
       ! model name is 'atchem'
       bfgSUID=2
       if (localrank==1) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.lt.0.or.localrank.gt.1) then
       print *,"'Error: (du",colour,",0): localrank has unexpected value"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! distribute id's to all su's
       call mpi_allreduce(b2mtemp,b2mmap,2,mpi_integer,mpi_sum,mpi_comm_world,ierr)
       if (localrank==0) then
       print *,"du",colour,"bfg to mpi id map is",b2mmap
       end if
       end if
       end if
       end if
       call mpi_buffer_attach(buff,buffsize,ierr)
       end subroutine initComms
       subroutine finaliseComms()
       use mpi
       implicit none
       !globalrank
       integer :: globalrank
       !ierr
       integer :: ierr
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       call mpivis_final(globalrank)
       call mpi_finalize(ierr)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=goldstein.initialise_goldstein.
       info(1)%du=b2mmap(1)
       info(1)%su=1
       info(1)%period=1
       info(1)%nesting=0
       info(1)%bound=1
       info(1)%offset=0
       nullify(info(1)%its)
       ! model.ep=embm.initialise_embm.
       info(2)%du=b2mmap(1)
       info(2)%su=1
       info(2)%period=1
       info(2)%nesting=0
       info(2)%bound=1
       info(2)%offset=0
       nullify(info(2)%its)
       ! model.ep=goldstein_seaice.initialise_seaice.
       info(3)%du=b2mmap(1)
       info(3)%su=1
       info(3)%period=1
       info(3)%nesting=0
       info(3)%bound=1
       info(3)%offset=0
       nullify(info(3)%its)
       ! model.ep=biogem.initialise_biogem.
       info(4)%du=b2mmap(2)
       info(4)%su=2
       info(4)%period=1
       info(4)%nesting=0
       info(4)%bound=1
       info(4)%offset=0
       nullify(info(4)%its)
       ! model.ep=atchem.initialise_atchem.1
       info(5)%du=b2mmap(2)
       info(5)%su=2
       info(5)%period=1
       info(5)%nesting=0
       info(5)%bound=1
       info(5)%offset=0
       nullify(info(5)%its)
       ! model.ep=atchem.cpl_comp_atmocn.1
       info(6)%du=b2mmap(2)
       info(6)%su=2
       info(6)%period=1
       info(6)%nesting=0
       info(6)%bound=1
       info(6)%offset=0
       nullify(info(6)%its)
       ! model.ep=weight_check.weightcheck.
       info(7)%du=b2mmap(1)
       info(7)%su=1
       info(7)%period=1
       info(7)%nesting=0
       info(7)%bound=1
       info(7)%offset=0
       nullify(info(7)%its)
       ! model.ep=bfg_increment_genie_clock.increment_genie_clock.
       info(8)%du=b2mmap(2)
       info(8)%su=2
       info(8)%period=1
       info(8)%nesting=1
       info(8)%bound=500
       info(8)%offset=0
       info(8)%its=>its1
       ! model.ep=counter.counter.1
       info(9)%du=b2mmap(1)
       info(9)%su=1
       info(9)%period=5
       info(9)%nesting=1
       info(9)%bound=500
       info(9)%offset=1
       info(9)%its=>its1
       ! model.ep=surflux.surflux.
       info(10)%du=b2mmap(1)
       info(10)%su=1
       info(10)%period=5
       info(10)%nesting=1
       info(10)%bound=500
       info(10)%offset=1
       info(10)%its=>its1
       ! model.ep=counter.counter.2
       info(11)%du=b2mmap(1)
       info(11)%su=1
       info(11)%period=1
       info(11)%nesting=1
       info(11)%bound=500
       info(11)%offset=0
       info(11)%its=>its1
       ! model.ep=embm.embm.
       info(12)%du=b2mmap(1)
       info(12)%su=1
       info(12)%period=1
       info(12)%nesting=1
       info(12)%bound=500
       info(12)%offset=0
       info(12)%its=>its1
       ! model.ep=counter.counter.3
       info(13)%du=b2mmap(1)
       info(13)%su=1
       info(13)%period=5
       info(13)%nesting=1
       info(13)%bound=500
       info(13)%offset=0
       info(13)%its=>its1
       ! model.ep=goldstein_seaice.gold_seaice.
       info(14)%du=b2mmap(1)
       info(14)%su=1
       info(14)%period=5
       info(14)%nesting=1
       info(14)%bound=500
       info(14)%offset=0
       info(14)%its=>its1
       ! model.ep=goldstein.goldstein.
       info(15)%du=b2mmap(1)
       info(15)%su=1
       info(15)%period=5
       info(15)%nesting=1
       info(15)%bound=500
       info(15)%offset=0
       info(15)%its=>its1
       ! model.ep=biogem.biogem.
       info(16)%du=b2mmap(2)
       info(16)%su=2
       info(16)%period=25
       info(16)%nesting=1
       info(16)%bound=500
       info(16)%offset=0
       info(16)%its=>its1
       ! model.ep=biogem.diag_biogem_timeslice.
       info(17)%du=b2mmap(2)
       info(17)%su=2
       info(17)%period=25
       info(17)%nesting=1
       info(17)%bound=500
       info(17)%offset=0
       info(17)%its=>its1
       ! model.ep=biogem.diag_biogem_timeseries.
       info(18)%du=b2mmap(2)
       info(18)%su=2
       info(18)%period=25
       info(18)%nesting=1
       info(18)%bound=500
       info(18)%offset=0
       info(18)%its=>its1
       ! model.ep=atchem.cpl_flux_ocnatm.1
       info(19)%du=b2mmap(2)
       info(19)%su=2
       info(19)%period=25
       info(19)%nesting=1
       info(19)%bound=500
       info(19)%offset=0
       info(19)%its=>its1
       ! model.ep=atchem.atchem.1
       info(20)%du=b2mmap(2)
       info(20)%su=2
       info(20)%period=25
       info(20)%nesting=1
       info(20)%bound=500
       info(20)%offset=0
       info(20)%its=>its1
       ! model.ep=atchem.cpl_comp_atmocn.2
       info(21)%du=b2mmap(2)
       info(21)%su=2
       info(21)%period=25
       info(21)%nesting=1
       info(21)%bound=500
       info(21)%offset=0
       info(21)%its=>its1
       ! model.ep=biogem.rest_biogem.
       info(22)%du=b2mmap(2)
       info(22)%su=2
       info(22)%period=1
       info(22)%nesting=0
       info(22)%bound=1
       info(22)%offset=0
       nullify(info(22)%its)
       ! model.ep=atchem.atchem.2
       info(23)%du=b2mmap(2)
       info(23)%su=2
       info(23)%period=1
       info(23)%nesting=0
       info(23)%bound=1
       info(23)%offset=0
       nullify(info(23)%its)
       ! model.ep=atchem.rest_atchem.1
       info(24)%du=b2mmap(2)
       info(24)%su=2
       info(24)%period=1
       info(24)%nesting=0
       info(24)%bound=1
       info(24)%offset=0
       nullify(info(24)%its)
       ! model.ep=goldstein.end_goldstein.
       info(25)%du=b2mmap(1)
       info(25)%su=1
       info(25)%period=1
       info(25)%nesting=0
       info(25)%bound=1
       info(25)%offset=0
       nullify(info(25)%its)
       ! model.ep=embm.end_embm.
       info(26)%du=b2mmap(1)
       info(26)%su=1
       info(26)%period=1
       info(26)%nesting=0
       info(26)%bound=1
       info(26)%offset=0
       nullify(info(26)%its)
       ! model.ep=goldstein_seaice.end_seaice.
       info(27)%du=b2mmap(1)
       info(27)%su=1
       info(27)%period=1
       info(27)%nesting=0
       info(27)%bound=1
       info(27)%offset=0
       nullify(info(27)%its)
       ! model.ep=biogem.end_biogem.
       info(28)%du=b2mmap(2)
       info(28)%su=2
       info(28)%period=1
       info(28)%nesting=0
       info(28)%bound=1
       info(28)%offset=0
       nullify(info(28)%its)
       ! model.ep=atchem.end_atchem.1
       info(29)%du=b2mmap(2)
       info(29)%su=2
       info(29)%period=1
       info(29)%nesting=0
       info(29)%bound=1
       info(29)%offset=0
       nullify(info(29)%its)
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !currentNesting
       integer :: currentNesting
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !targetpos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getNext=findNext(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestNext
       integer :: nestNext
       !currentMin
       integer :: currentMin
       !remainIters
       integer :: remainIters
       !waitIters
       integer :: waitIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1.or.its==0) then
       its=1
       end if
       if (list(pos).gt.point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestNext=findNext(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters.gt.0) then
       if (its+1.lt.info(list(pos))%offset) then
       waitIters=info(list(pos))%offset - its
       else if (its+1==info(list(pos))%offset) then
       waitIters=1
       else
       waitIters=info(list(pos))%period - mod(its - info(list(pos))%offset,info(list(pos))%period)
       end if
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters.lt.currentMin.and.waitIters.le.remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
       end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !currentNesting
       integer :: currentNesting
       !targetpos
       integer :: targetpos
       !currentMin
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getLast=findLast(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestLast
       integer :: nestLast
       !currentMin
       integer :: currentMin
       !elapsedIters
       integer :: elapsedIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos).ge.point) then
       its=its - 1
       end if
       if (.not.(associated(info(list(pos))%its)).and.((info(point)%nesting==1.and.its1.gt.info(point)%period).or.(info(point)%nesting.gt.1.and.its1.gt.1))) then
       continue
       else if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestLast=findLast(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       if (its.gt.0.and.its.ge.info(list(pos))%offset) then
       elapsedIters=mod(its - info(list(pos))%offset,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters.lt.currentMin.and.elapsedIters.lt.its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
       end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       !i
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       do i=pos+1,lsize
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findStartPoint=1
       return
       end if
       do i=pos-1,1,-1
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
       findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findEndPoint=lsize
       return
       end if
       do i=pos+1,lsize
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
       findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target1
