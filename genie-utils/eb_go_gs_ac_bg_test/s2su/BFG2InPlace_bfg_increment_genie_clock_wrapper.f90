       ! f77 to f90 put/get wrappers start
       subroutine put_bfg_increment_genie_clock(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==8) then
       if (tag==-1) then
       call putinteger8__0_bfg_increment_genie_clock(data,tag)
       end if
       end if
       end subroutine put_bfg_increment_genie_clock
       subroutine putinteger8__0_bfg_increment_genie_clock(data,tag)
       use BFG2InPlace_bfg_increment_genie_clock, only : put=>putinteger8__0
       implicit none
       integer*8 , intent(out) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger8__0_bfg_increment_genie_clock
       ! f77 to f90 put/get wrappers end
