       module BFG2InPlace_fixed_icesheet
       use BFG2Target3
       ! oasis4IncludeTarget
       use prism
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(3) :: pp2p=(/0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==29) then
       ! I am model=fixed_icesheet and ep=fixedicesheet and instance=
       if (arg2==-76) then
       ! I am fixed_icesheet.fixedicesheet..29
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,9,29/)
       myID=29
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg3
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-77) then
       ! I am fixed_icesheet.fixedicesheet..29
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,12,29/)
       myID=29
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg4
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-78) then
       ! I am fixed_icesheet.fixedicesheet..29
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,12,29/)
       myID=29
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg5
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine getinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==29) then
       ! I am model=fixed_icesheet and ep=fixedicesheet and instance=
       if (arg2==-151) then
       ! I am fixed_icesheet.fixedicesheet..29
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/9,29/)
       myID=29
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg6
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       end subroutine getinteger__0
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==29) then
       ! I am model=fixed_icesheet and ep=fixedicesheet and instance=
       if (arg2==-76) then
       ! I am fixed_icesheet.fixedicesheet..29
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,9,29/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=29
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg3
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg3
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-77) then
       ! I am fixed_icesheet.fixedicesheet..29
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,12,29/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=29
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg4
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg4
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-78) then
       ! I am fixed_icesheet.fixedicesheet..29
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,12,29/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=29
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg5
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg5
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       end subroutine putreal__2
       subroutine putinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==29) then
       ! I am model=fixed_icesheet and ep=fixedicesheet and instance=
       if (arg2==-151) then
       ! I am fixed_icesheet.fixedicesheet..29
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/9,29/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=29
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg6
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: fixed_icesheet_fixedicesheet_arg6
       coupling_field=>component%coupling_fields(fixed_icesheet_fixedicesheet_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       end subroutine putinteger__0
       end module BFG2InPlace_fixed_icesheet
