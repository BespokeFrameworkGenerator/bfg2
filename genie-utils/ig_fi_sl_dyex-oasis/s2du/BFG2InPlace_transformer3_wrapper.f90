       ! f77 to f90 put/get wrappers start
       subroutine put_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==19) then
       if (tag==-367) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-325) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-371) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-329) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-373) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-108) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-375) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-109) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-377) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-379) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-381) then
       call putreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine put_transformer3
       subroutine get_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==19) then
       if (tag==-367) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-325) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-371) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-329) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-373) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-108) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-375) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-109) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-377) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-379) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-381) then
       call getreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine get_transformer3
       subroutine getreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_transformer3
       subroutine putreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_transformer3
       ! f77 to f90 put/get wrappers end
