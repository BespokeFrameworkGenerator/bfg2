       module BFG2InPlace_slab_seaice
       use BFG2Target2
       ! oasis4IncludeTarget
       use prism
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(16) :: pp2p=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==6) then
       ! I am model=slab_seaice and ep=initialise_slabseaice and instance=
       if (arg2==-105) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg1
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg1%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-107) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-276) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/6,7,21,22,23,25/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       if (currentModel==21) then
       ! I am model=slab_seaice and ep=slabseaice and instance=
       if (arg2==-105) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-367) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-371) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg4
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-373) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg5
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-375) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg6
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-325) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/17,18,19,21,23/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg7
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-329) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/17,18,19,21,23/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(10).eq.1) then
       pp2p(10)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg8
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-108) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(11).eq.1) then
       pp2p(11)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg9
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-109) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(12).eq.1) then
       pp2p(12)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg10
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-377) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(13).eq.1) then
       pp2p(13)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg11
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-379) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(14).eq.1) then
       pp2p(14)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg12
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-276) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/6,7,21,22,23,25/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(15).eq.1) then
       pp2p(15)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg13
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-107) then
       ! I am slab_seaice.slabseaice..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(16).eq.1) then
       pp2p(16)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg16
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg16%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==6) then
       ! I am model=slab_seaice and ep=initialise_slabseaice and instance=
       if (arg2==-105) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg1
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg1%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg1
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg1%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-107) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-276) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=6
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,7,21,22,23,25/)
       ins=(/0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==21) then
       ! I am model=slab_seaice and ep=slabseaice and instance=
       if (arg2==-105) then
       ! I am slab_seaice.slabseaice..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg2
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg2%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-367) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg3
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-371) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg4
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg4
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-373) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg5
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg5
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg5%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-375) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg6
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg6
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg6%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-325) then
       ! I am slab_seaice.slabseaice..21
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/17,18,19,21,23/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg7
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg7
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-329) then
       ! I am slab_seaice.slabseaice..21
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/17,18,19,21,23/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg8
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg8
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-108) then
       ! I am slab_seaice.slabseaice..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg9
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg9
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-109) then
       ! I am slab_seaice.slabseaice..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg10
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg10
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-377) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg11
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg11
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-379) then
       ! I am slab_seaice.slabseaice..21
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg12
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg12
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-276) then
       ! I am slab_seaice.slabseaice..21
       setSize=6
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,7,21,22,23,25/)
       ins=(/0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg13
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg13
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-107) then
       ! I am slab_seaice.slabseaice..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg16
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg16%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg16
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg16%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_slab_seaice
