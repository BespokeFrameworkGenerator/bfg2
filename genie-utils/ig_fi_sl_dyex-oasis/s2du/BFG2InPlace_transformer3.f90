       module BFG2InPlace_transformer3
       use BFG2Target1
       ! oasis4IncludeTarget
       use prism
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(11) :: pp2p=(/0,0,0,0,0,0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==19) then
       ! I am model=transformer3 and ep=new_transformer_3 and instance=
       if (arg2==-367) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg3
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-325) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/17,18,19,21,23/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg4
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-371) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg7
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-329) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/17,18,19,21,23/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg8
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-373) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg9
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-108) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg10
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-375) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg11
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-109) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg12
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-377) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg13
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-379) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(10).eq.1) then
       pp2p(10)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg15
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg15%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       if (arg2==-381) then
       ! I am transformer3.new_transformer_3..19
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/19,31/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(11).eq.1) then
       pp2p(11)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg17
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg17%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%get_instance
       ! Receive data from sending entry point argument
       call oasisvis_prism_get(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==19) then
       ! I am model=transformer3 and ep=new_transformer_3 and instance=
       if (arg2==-367) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg3
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg3
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg3%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-325) then
       ! I am transformer3.new_transformer_3..19
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/17,18,19,21,23/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg4
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg4
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg4%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-371) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg7
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg7
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg7%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-329) then
       ! I am transformer3.new_transformer_3..19
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/17,18,19,21,23/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg8
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg8
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg8%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-373) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg9
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg9
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg9%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-108) then
       ! I am transformer3.new_transformer_3..19
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg10
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg10
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg10%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-375) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg11
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg11
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg11%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-109) then
       ! I am transformer3.new_transformer_3..19
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,12,15,18,19,21,23/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg12
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg12
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg12%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-377) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg13
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg13
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg13%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-379) then
       ! I am transformer3.new_transformer_3..19
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg15
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg15%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg15
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg15%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-381) then
       ! I am transformer3.new_transformer_3..19
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,31/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=19
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg17
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg17%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: transformer3_new_transformer_3_arg17
       coupling_field=>component%coupling_fields(transformer3_new_transformer_3_arg17%coupling_field_no)
       coupling_field_ep_inst=>coupling_field%ep_instances(remoteID)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       ! Send data to receiving entry point argument(s)
       do while(associated(coupling_field_arg_inst))
       call oasisvis_prism_put(info(myID)%su,info(remoteID)%su,trim(coupling_field_arg_inst%msg_tag),coupling_field_arg_inst%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       coupling_field_arg_inst=>coupling_field_arg_inst%next_instance
       end do
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_transformer3
