       module BFG2Target2
       ! oasis4IncludeTarget
       use prism
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(3)
       integer :: b2mmap(3)
       ! Constant declarations
       !name_len_max
       integer, parameter :: name_len_max=256
       !ndim
       integer, parameter :: ndim=3
       !ep_instance_count
       integer, parameter :: ep_instance_count=56
       ! Derived type definitions
       type shape_type
       !bounds
       integer, dimension(2,ndim) :: bounds
       end type shape_type
       type coords_type
       !longitudes
       real, allocatable, dimension(:) :: longitudes
       !latitudes
       real, allocatable, dimension(:) :: latitudes
       !verticals
       real, allocatable, dimension(:) :: verticals
       end type coords_type
       type corners_type
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type corners_type
       type points_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type points_type
       type mask_type
       !id
       integer :: id
       !actual_shape
       type(shape_type) :: actual_shape
       !array
       logical, allocatable, dimension(:,:) :: array
       end type mask_type
       type grid_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !type_id
       integer :: type_id
       !valid_shape
       type(shape_type) :: valid_shape
       !corners
       type(corners_type) :: corners
       !point_sets
       type(points_type), dimension(1) :: point_sets
       !landmask
       type(mask_type) :: landmask
       end type grid_type
       type coupling_field_ep_instance_type
       !get_instance
       type(coupling_field_arg_instance_type), pointer :: get_instance
       !put_instances
       type(coupling_field_arg_instance_type), pointer :: put_instances
       end type coupling_field_ep_instance_type
       type coupling_field_arg_instance_type
       !prism_id
       integer :: prism_id
       !bfg_ref
       integer :: bfg_ref
       !msg_tag
       character(len=name_len_max) :: msg_tag
       !next_instance
       type(coupling_field_arg_instance_type), pointer :: next_instance
       end type coupling_field_arg_instance_type
       type coupling_field_type
       !ep_instances
       type(coupling_field_ep_instance_type), dimension(ep_instance_count) :: ep_instances
       !bfg_id
       character(len=name_len_max) :: bfg_id
       !name
       character(len=name_len_max) :: name
       !nodims
       integer, dimension(2) :: nodims
       !actual_shape
       type(shape_type) :: actual_shape
       !type_id
       integer :: type_id
       end type coupling_field_type
       type component_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !local_comm
       integer :: local_comm
       !gridded_grids
       type(grid_type), allocatable, dimension(:) :: gridded_grids
       !gridless_grids
       type(grid_type), allocatable, dimension(:) :: gridless_grids
       !coupling_fields
       type(coupling_field_type), allocatable, dimension(:) :: coupling_fields
       !coupling_fields_count
       integer :: coupling_fields_count
       end type component_type
       type coupling_field_key_type
       !coupling_field_no
       integer :: coupling_field_no
       end type coupling_field_key_type
       ! Variable declarations
       ! Coupling field keys for this deployment unit
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg1
       !slab_seaice_initialise_slabseaice_arg1
       type(coupling_field_key_type) :: slab_seaice_initialise_slabseaice_arg1
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg2
       !slab_seaice_initialise_slabseaice_arg2
       type(coupling_field_key_type) :: slab_seaice_initialise_slabseaice_arg2
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg3
       !slab_seaice_initialise_slabseaice_arg3
       type(coupling_field_key_type) :: slab_seaice_initialise_slabseaice_arg3
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg5
       !slab_seaice_initialise_slabseaice_arg5
       type(coupling_field_key_type) :: slab_seaice_initialise_slabseaice_arg5
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg2
       !slab_seaice_slabseaice_arg2
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg2
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg3
       !slab_seaice_slabseaice_arg3
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg3
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg4
       !slab_seaice_slabseaice_arg4
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg4
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg5
       !slab_seaice_slabseaice_arg5
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg5
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg6
       !slab_seaice_slabseaice_arg6
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg6
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg7
       !slab_seaice_slabseaice_arg7
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg7
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg8
       !slab_seaice_slabseaice_arg8
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg8
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg9
       !slab_seaice_slabseaice_arg9
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg9
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg10
       !slab_seaice_slabseaice_arg10
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg10
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg11
       !slab_seaice_slabseaice_arg11
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg11
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg12
       !slab_seaice_slabseaice_arg12
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg12
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg13
       !slab_seaice_slabseaice_arg13
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg13
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg16
       !slab_seaice_slabseaice_arg16
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg16
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg17
       !slab_seaice_slabseaice_arg17
       type(coupling_field_key_type) :: slab_seaice_slabseaice_arg17
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg3
       !transformer4_new_transformer_4_arg3
       type(coupling_field_key_type) :: transformer4_new_transformer_4_arg3
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg4
       !transformer4_new_transformer_4_arg4
       type(coupling_field_key_type) :: transformer4_new_transformer_4_arg4
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg7
       !transformer4_new_transformer_4_arg7
       type(coupling_field_key_type) :: transformer4_new_transformer_4_arg7
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg3
       !transformer6_new_transformer_6_arg3
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg3
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg4
       !transformer6_new_transformer_6_arg4
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg4
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg5
       !transformer6_new_transformer_6_arg5
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg5
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg6
       !transformer6_new_transformer_6_arg6
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg6
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg7
       !transformer6_new_transformer_6_arg7
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg7
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg8
       !transformer6_new_transformer_6_arg8
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg8
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg9
       !transformer6_new_transformer_6_arg9
       type(coupling_field_key_type) :: transformer6_new_transformer_6_arg9
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg3
       !copy_tstar_inst1_copytstar_arg3
       type(coupling_field_key_type) :: copy_tstar_inst1_copytstar_arg3
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg3
       !copy_tstar_inst2_copytstar_arg3
       type(coupling_field_key_type) :: copy_tstar_inst2_copytstar_arg3
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg4
       !copy_tstar_inst1_copytstar_arg4
       type(coupling_field_key_type) :: copy_tstar_inst1_copytstar_arg4
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg4
       !copy_tstar_inst2_copytstar_arg4
       type(coupling_field_key_type) :: copy_tstar_inst2_copytstar_arg4
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg5
       !copy_tstar_inst1_copytstar_arg5
       type(coupling_field_key_type) :: copy_tstar_inst1_copytstar_arg5
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg5
       !copy_tstar_inst2_copytstar_arg5
       type(coupling_field_key_type) :: copy_tstar_inst2_copytstar_arg5
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg6
       !copy_tstar_inst1_copytstar_arg6
       type(coupling_field_key_type) :: copy_tstar_inst1_copytstar_arg6
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg6
       !copy_tstar_inst2_copytstar_arg6
       type(coupling_field_key_type) :: copy_tstar_inst2_copytstar_arg6
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg3
       !copy_albedo_inst1_copyalbedo_arg3
       type(coupling_field_key_type) :: copy_albedo_inst1_copyalbedo_arg3
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg3
       !copy_albedo_inst2_copyalbedo_arg3
       type(coupling_field_key_type) :: copy_albedo_inst2_copyalbedo_arg3
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg4
       !copy_albedo_inst1_copyalbedo_arg4
       type(coupling_field_key_type) :: copy_albedo_inst1_copyalbedo_arg4
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg4
       !copy_albedo_inst2_copyalbedo_arg4
       type(coupling_field_key_type) :: copy_albedo_inst2_copyalbedo_arg4
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg5
       !copy_albedo_inst1_copyalbedo_arg5
       type(coupling_field_key_type) :: copy_albedo_inst1_copyalbedo_arg5
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg5
       !copy_albedo_inst2_copyalbedo_arg5
       type(coupling_field_key_type) :: copy_albedo_inst2_copyalbedo_arg5
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg6
       !copy_albedo_inst1_copyalbedo_arg6
       type(coupling_field_key_type) :: copy_albedo_inst1_copyalbedo_arg6
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg6
       !copy_albedo_inst2_copyalbedo_arg6
       type(coupling_field_key_type) :: copy_albedo_inst2_copyalbedo_arg6
       !component
       type(component_type), target :: component
       !model_time
       type(PRISM_Time_Struct) :: model_time
       !model_time_bounds
       type(PRISM_Time_Struct), dimension(2) :: model_time_bounds
       ! Current rank, and all ranks for this deployment unit
       !my_local_rank
       integer :: my_local_rank
       !SU2_rank
       integer :: SU2_rank
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       !its2
       integer, target :: its2
       !its3
       integer, target :: its3
       type modelInfo
       !bfg_du
       integer :: bfg_du
       !du
       integer :: du
       !su
       integer :: su
       !period
       integer :: period
       !nesting
       integer :: nesting
       !bound
       integer :: bound
       !offset
       integer :: offset
       !its
       integer, pointer :: its
       end type modelInfo
       !info
       type(modelInfo), dimension(1:56) :: info
       !inf
       integer, parameter :: inf=32767
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function transformer4Thread()
       implicit none
       if (bfgSUID==2) then
       transformer4Thread=.true.
       else
       transformer4Thread=.false.
       end if
       end function transformer4Thread
       logical function transformer6Thread()
       implicit none
       if (bfgSUID==2) then
       transformer6Thread=.true.
       else
       transformer6Thread=.false.
       end if
       end function transformer6Thread
       logical function counterinst2Thread()
       implicit none
       if (bfgSUID==2) then
       counterinst2Thread=.true.
       else
       counterinst2Thread=.false.
       end if
       end function counterinst2Thread
       logical function slab_seaiceThread()
       implicit none
       if (bfgSUID==2) then
       slab_seaiceThread=.true.
       else
       slab_seaiceThread=.false.
       end if
       end function slab_seaiceThread
       logical function copy_tstarinst1Thread()
       implicit none
       if (bfgSUID==2) then
       copy_tstarinst1Thread=.true.
       else
       copy_tstarinst1Thread=.false.
       end if
       end function copy_tstarinst1Thread
       logical function copy_tstarinst2Thread()
       implicit none
       if (bfgSUID==2) then
       copy_tstarinst2Thread=.true.
       else
       copy_tstarinst2Thread=.false.
       end if
       end function copy_tstarinst2Thread
       logical function copy_albedoinst1Thread()
       implicit none
       if (bfgSUID==2) then
       copy_albedoinst1Thread=.true.
       else
       copy_albedoinst1Thread=.false.
       end if
       end function copy_albedoinst1Thread
       logical function copy_albedoinst2Thread()
       implicit none
       if (bfgSUID==2) then
       copy_albedoinst2Thread=.true.
       else
       copy_albedoinst2Thread=.false.
       end if
       end function copy_albedoinst2Thread
       subroutine commsSync()
       implicit none
       ! oasis4CommsSync
       !ierror
       integer :: ierror
       call mpi_barrier(component%local_comm,ierror)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine start_oasisvis_log(logid,bfg_suid)
       implicit none
       character (len=*), intent(in) :: logid
       integer , intent(in) :: bfg_suid
       call oasisvis_comp('begin',logid,bfg_suid)
       end subroutine start_oasisvis_log
       subroutine end_oasisvis_log(logid,bfg_suid)
       implicit none
       character (len=*), intent(in) :: logid
       integer , intent(in) :: bfg_suid
       call oasisvis_comp('end',logid,bfg_suid)
       end subroutine end_oasisvis_log
       subroutine initComms()
       use prism
       use FoX_wkml
       implicit none
       !kml_file_handle
       type(xmlf_t) :: kml_file_handle
       !RANK_UNKNOWN
       integer, parameter :: RANK_UNKNOWN=-1
       !ierror
       integer :: ierror
       !comp_loop
       integer :: comp_loop
       !points_loop
       integer :: points_loop
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !grid
       type(grid_type), pointer :: grid
       !points
       type(points_type), pointer :: points
       !corners
       type(corners_type), pointer :: corners
       !mask
       type(mask_type), pointer :: mask
       !coord_array
       real, dimension(:), pointer :: coord_array
       !corners_longitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_longitudes_OASIS4
       !corners_latitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_latitudes_OASIS4
       !corners_verticals_OASIS4
       real, allocatable, dimension(:,:) :: corners_verticals_OASIS4
       !all_coords_array
       real, allocatable, dimension(:) :: all_coords_array
       !index_loop
       integer :: index_loop
       !coord_loop
       integer :: coord_loop
       !lon_loop
       integer :: lon_loop
       !lat_loop
       integer :: lat_loop
       !appl_name
       character(len=name_len_max) :: appl_name
       !local_comm
       integer :: local_comm
       !rank_lists
       integer, dimension(1,3) :: rank_lists
       ! Set a PRISM application name for this deployment unit
       appl_name='DU2'
       ! Initialise the coupling environment (must be called by each process)
       call prism_init(trim(appl_name),ierror)
       ! Get the rank for the current process
       ! This rank is relative to the current deployment unit - it is not global
       call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       call MPI_Comm_rank(local_comm,my_local_rank,ierror)
       ! Reset all sequence unit ranks
       b2mmap=RANK_UNKNOWN
       ! Get the ranks for all component processes
       call prism_get_ranklists('SU2',1,rank_lists,ierror)
       SU2_rank=rank_lists(1,1)
       call oasisvis_init(2)
       ! Store mapping from sequence unit number to local rank
       b2mmap(2)=SU2_rank
       ! Initialise the PRISM component
       if (my_local_rank==SU2_rank) then
       ! Assign sequence unit number (unique across deployment units)
       bfgSUID=2
       component%name='SU2'
       call prism_init_comp(component%id,trim(component%name),ierror)
       call prism_get_localcomm(component%id,component%local_comm,ierror)
       ! Initialise grids and coupling data for component: SU2
       allocate(component%gridded_grids(1:2))
       allocate(component%gridless_grids(1:178))
       allocate(component%coupling_fields(1:44))
       component%coupling_fields_count=44
       ! Initialising grid: IGCM_T21
       grid=>component%gridded_grids(1)
       grid%name='IGCM_T21'
       grid%type_id=PRISM_reglonlatvrt
       grid%valid_shape%bounds(1,1)=1
       grid%valid_shape%bounds(2,1)=64
       grid%valid_shape%bounds(1,2)=1
       grid%valid_shape%bounds(2,2)=32
       grid%valid_shape%bounds(1,3)=1
       grid%valid_shape%bounds(2,3)=1
       call prism_def_grid(grid%id,trim(grid%name),component%id,grid%valid_shape%bounds,grid%type_id,ierror)
       points=>grid%point_sets(1)
       corners=>grid%corners
       points%actual_shape=grid%valid_shape
       corners%actual_shape=grid%valid_shape
       allocate(points%coords%longitudes(1:64))
       allocate(corners%coords%longitudes(1:65))
       allocate(points%coords%latitudes(1:32))
       allocate(corners%coords%latitudes(1:33))
       ! Coordinates specified by start and increment values in gridspec
       ! Initialise longitudes for grid points
       coord_array=>points%coords%longitudes
       coord_array(1)=5.625
       coord_array(2)=11.25
       coord_array(3)=16.875
       coord_array(4)=22.5
       coord_array(5)=28.125
       coord_array(6)=33.75
       coord_array(7)=39.375
       coord_array(8)=45
       coord_array(9)=50.625
       coord_array(10)=56.25
       coord_array(11)=61.875
       coord_array(12)=67.5
       coord_array(13)=73.125
       coord_array(14)=78.75
       coord_array(15)=84.375
       coord_array(16)=90
       coord_array(17)=95.625
       coord_array(18)=101.25
       coord_array(19)=106.875
       coord_array(20)=112.5
       coord_array(21)=118.125
       coord_array(22)=123.75
       coord_array(23)=129.375
       coord_array(24)=135
       coord_array(25)=140.625
       coord_array(26)=146.25
       coord_array(27)=151.875
       coord_array(28)=157.5
       coord_array(29)=163.125
       coord_array(30)=168.75
       coord_array(31)=174.375
       coord_array(32)=180
       coord_array(33)=185.625
       coord_array(34)=191.25
       coord_array(35)=196.875
       coord_array(36)=202.5
       coord_array(37)=208.125
       coord_array(38)=213.75
       coord_array(39)=219.375
       coord_array(40)=225
       coord_array(41)=230.625
       coord_array(42)=236.25
       coord_array(43)=241.875
       coord_array(44)=247.5
       coord_array(45)=253.125
       coord_array(46)=258.75
       coord_array(47)=264.375
       coord_array(48)=270
       coord_array(49)=275.625
       coord_array(50)=281.25
       coord_array(51)=286.875
       coord_array(52)=292.5
       coord_array(53)=298.125
       coord_array(54)=303.75
       coord_array(55)=309.375
       coord_array(56)=315
       coord_array(57)=320.625
       coord_array(58)=326.25
       coord_array(59)=331.875
       coord_array(60)=337.5
       coord_array(61)=343.125
       coord_array(62)=348.75
       coord_array(63)=354.375
       coord_array(64)=360
       ! Initialise longitudes for grid corners
       coord_array=>corners%coords%longitudes
       coord_array(1)=2.8125
       coord_array(2)=8.4375
       coord_array(3)=14.0625
       coord_array(4)=19.6875
       coord_array(5)=25.3125
       coord_array(6)=30.9375
       coord_array(7)=36.5625
       coord_array(8)=42.1875
       coord_array(9)=47.8125
       coord_array(10)=53.4375
       coord_array(11)=59.0625
       coord_array(12)=64.6875
       coord_array(13)=70.3125
       coord_array(14)=75.9375
       coord_array(15)=81.5625
       coord_array(16)=87.1875
       coord_array(17)=92.8125
       coord_array(18)=98.4375
       coord_array(19)=104.0625
       coord_array(20)=109.6875
       coord_array(21)=115.3125
       coord_array(22)=120.9375
       coord_array(23)=126.5625
       coord_array(24)=132.1875
       coord_array(25)=137.8125
       coord_array(26)=143.4375
       coord_array(27)=149.0625
       coord_array(28)=154.6875
       coord_array(29)=160.3125
       coord_array(30)=165.9375
       coord_array(31)=171.5625
       coord_array(32)=177.1875
       coord_array(33)=182.8125
       coord_array(34)=188.4375
       coord_array(35)=194.0625
       coord_array(36)=199.6875
       coord_array(37)=205.3125
       coord_array(38)=210.9375
       coord_array(39)=216.5625
       coord_array(40)=222.1875
       coord_array(41)=227.8125
       coord_array(42)=233.4375
       coord_array(43)=239.0625
       coord_array(44)=244.6875
       coord_array(45)=250.3125
       coord_array(46)=255.9375
       coord_array(47)=261.5625
       coord_array(48)=267.1875
       coord_array(49)=272.8125
       coord_array(50)=278.4375
       coord_array(51)=284.0625
       coord_array(52)=289.6875
       coord_array(53)=295.3125
       coord_array(54)=300.9375
       coord_array(55)=306.5625
       coord_array(56)=312.1875
       coord_array(57)=317.8125
       coord_array(58)=323.4375
       coord_array(59)=329.0625
       coord_array(60)=334.6875
       coord_array(61)=340.3125
       coord_array(62)=345.9375
       coord_array(63)=351.5625
       coord_array(64)=357.1875
       coord_array(65)=362.8125
       ! Coordinates listed explicitly in gridspec
       ! Initialise latitudes for grid points
       coord_array=>points%coords%latitudes
       coord_array(1)=85.7605743408203125
       coord_array(2)=80.268768310546875
       coord_array(3)=74.74454498291015625
       coord_array(4)=69.21297454833984375
       coord_array(5)=63.6786346435546875
       coord_array(6)=58.142955780029296875
       coord_array(7)=52.6065216064453125
       coord_array(8)=47.06964111328125
       coord_array(9)=41.532459259033203125
       coord_array(10)=35.995075225830078125
       coord_array(11)=30.457550048828125
       coord_array(12)=24.91992950439453125
       coord_array(13)=19.3822307586669921875
       coord_array(14)=13.84448337554931640625
       coord_array(15)=8.30670261383056640625
       coord_array(16)=2.76890277862548828125
       coord_array(17)=-2.76890277862548828125
       coord_array(18)=-8.30670261383056640625
       coord_array(19)=-13.84448337554931640625
       coord_array(20)=-19.3822307586669921875
       coord_array(21)=-24.91992950439453125
       coord_array(22)=-30.457550048828125
       coord_array(23)=-35.995075225830078125
       coord_array(24)=-41.532459259033203125
       coord_array(25)=-47.06964111328125
       coord_array(26)=-52.6065216064453125
       coord_array(27)=-58.142955780029296875
       coord_array(28)=-63.6786346435546875
       coord_array(29)=-69.21297454833984375
       coord_array(30)=-74.74454498291015625
       coord_array(31)=-80.268768310546875
       coord_array(32)=-85.7605743408203125
       ! Initialise latitudes for grid corners
       coord_array=>corners%coords%latitudes
       coord_array(1)=90
       coord_array(2)=83.2076873779296875
       coord_array(3)=77.6092681884765625
       coord_array(4)=72.0479736328125
       coord_array(5)=66.4972686767578125
       coord_array(6)=60.951045989990234375
       coord_array(7)=55.407138824462890625
       coord_array(8)=49.864574432373046875
       coord_array(9)=44.3228607177734375
       coord_array(10)=38.781707763671875
       coord_array(11)=33.240947723388671875
       coord_array(12)=27.7004604339599609375
       coord_array(13)=22.1601715087890625
       coord_array(14)=16.6200199127197265625
       coord_array(15)=11.07996463775634765625
       coord_array(16)=5.539968013763427734375
       coord_array(17)=2.668042498044087551534175872802734375E-7
       coord_array(18)=-5.539967060089111328125
       coord_array(19)=-11.07996368408203125
       coord_array(20)=-16.6200199127197265625
       coord_array(21)=-22.1601715087890625
       coord_array(22)=-27.7004604339599609375
       coord_array(23)=-33.240947723388671875
       coord_array(24)=-38.781707763671875
       coord_array(25)=-44.3228607177734375
       coord_array(26)=-49.864574432373046875
       coord_array(27)=-55.407135009765625
       coord_array(28)=-60.95104217529296875
       coord_array(29)=-66.4972686767578125
       coord_array(30)=-72.0479736328125
       coord_array(31)=-77.60926055908203125
       coord_array(32)=-83.20767974853515625
       coord_array(33)=-89.99217987060546875
       allocate(points%coords%verticals(1:1))
       allocate(corners%coords%verticals(1:1))
       points%coords%verticals=0.0
       corners%coords%verticals=0.0
       points%name='DU2_SU2_IGCM_T21_point_set_1'
       call prism_set_points(points%id,trim(points%name),grid%id,points%actual_shape%bounds,points%coords%longitudes,points%coords%latitudes,points%coords%verticals,.true.,ierror)
       ! Write point set to KML file for viewing in GoogleEarth
       ! File handle, filename, unit (-1 = library will assign), replace, doc name
       call kmlBeginFile(kml_file_handle,'DU2_SU2_IGCM_T21_point_set_1.kml',-1,.true.,'DU2_SU2_IGCM_T21_point_set_1')
       do lon_loop=1,64
       do lat_loop=1,32
       call kmlCreatePoints(kml_file_handle,points%coords%longitudes(lon_loop),points%coords%latitudes(lat_loop))
       end do
       end do
       call kmlFinishFile(kml_file_handle)
       ! Copy corners to OASIS4 format 2D array where
       ! leading/trailing corners are stored separately
       allocate(corners_longitudes_OASIS4(1:64,1:2))
       corners_longitudes_OASIS4( 1:64, 1)=corners%coords%longitudes( 1:64 )
       corners_longitudes_OASIS4( 1:64, 2)=corners%coords%longitudes( 2:( 64 + 1 ) )
       allocate(corners_latitudes_OASIS4(1:32,1:2))
       corners_latitudes_OASIS4( 1:32, 1)=corners%coords%latitudes( 1:32 )
       corners_latitudes_OASIS4( 1:32, 2)=corners%coords%latitudes( 2:( 32 + 1 ) )
       allocate(corners_verticals_OASIS4(1:1,1:2))
       corners_verticals_OASIS4=0.0
       call prism_set_corners(grid%id,8,corners%actual_shape%bounds,corners_longitudes_OASIS4,corners_latitudes_OASIS4,corners_verticals_OASIS4,ierror)
       if(allocated(corners_longitudes_OASIS4))deallocate(corners_longitudes_OASIS4)
       if(allocated(corners_latitudes_OASIS4))deallocate(corners_latitudes_OASIS4)
       if(allocated(corners_verticals_OASIS4))deallocate(corners_verticals_OASIS4)
       mask=>grid%landmask
       ! Initialise the land mask for this grid
       mask%actual_shape=grid%valid_shape
       allocate(mask%array(1:64,1:32))
       mask%array=reshape( (/ .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .false., .false., .false., .false., .false., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .true., .true., .true., .false., .false., .false., .false., .true., .true., &
       &.true., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .false., .false., .false., .false., .true., .true., &
       &.true., .false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .true., .true., .true., .false., .false., &
       &.false., .false., .true., .false., .false., .false., .false., .false., .true., .false., &
       &.false., .false., .true., .false., .false., .true., .true., .false., .false., .false., &
       &.false., .false., .true., .true., .true., .true., .true., .true., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .true., .true., .true., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.false., .false., .true., .true., .false., .false., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .false., .false., .true., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .false., .true., .true., .true., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .true., .true., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.true., .false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .false., .true., &
       &.true., .true., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .true., .true., .true., .true., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .true., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .true., .false., .false., .false., .true., .true., .false., .false., .false., &
       &.true., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .true., .true., .true., .true., &
       &.true., .false., .true., .true., .true., .false., .false., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .false., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .true., .true., .false., .false., .false., .false., .false., .false., .false., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.true., .false., .false., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .false., .false., .false., .false., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .false., .false., &
       &.false., .false., .false., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .true., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .false., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.false., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .false., .false., .false., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .false., .false., .false., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .false., .false., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .false., .false., .false., .false., .false., .true., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .false., .true., &
       &.true., .true., .true., .true., .true., .true., .true., .true., .true., .true., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .true., .true., .true., .true., .true., .true., .true., .true., &
       &.true., .true., .false., .false., .false., .false., .false., .true., .false., .false., &
       &.false., .false., .false., .false., .false., .true., .true., .true., .true., .true., &
       &.true., .true., .true., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .true., .true., &
       &.true., .true., .true., .true., .true., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.true., .true., .true., .true., .false., .false., .true., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .true., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false., .false., .false., .false., .false., .false., .false., &
       &.false., .false., .false. /), (/ 64, 32 /) )
       call prism_set_mask(mask%id,grid%id,mask%actual_shape%bounds,mask%array,.true.,ierror)
       ! Finished initialising grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg1
       slab_seaice_initialise_slabseaice_arg1%coupling_field_no=1
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg1%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_initialise_slabseaice_arg1'
       coupling_field%name='tstar'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_getFromSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_getFromSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_putToSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg1_putToSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg2
       slab_seaice_initialise_slabseaice_arg2%coupling_field_no=2
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg2%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_initialise_slabseaice_arg2'
       coupling_field%name='albedo'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_getFromSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_getFromSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_putToSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg2_putToSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg3
       slab_seaice_initialise_slabseaice_arg3%coupling_field_no=3
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg3%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_initialise_slabseaice_arg3'
       coupling_field%name='seaice_frac'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg3_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg3_putToSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising grid: gridless_grid_for_set_5
       grid=>component%gridless_grids(5)
       grid%name='gridless_grid_for_set_5'
       grid%type_id=PRISM_Gridless
       ! Bounds for each dimension default to 1, if not given below
       grid%valid_shape%bounds(:,:)=1
       grid%valid_shape%bounds(1,1)=1
       grid%valid_shape%bounds(2,1)=64
       grid%valid_shape%bounds(1,2)=1
       grid%valid_shape%bounds(2,2)=32
       ! No land mask for gridless grids
       grid%landmask%id=PRISM_UNDEFINED
       call prism_def_grid(grid%id,trim(grid%name),component%id,grid%valid_shape%bounds,grid%type_id,ierror)
       points=>grid%point_sets(1)
       points%name='gridless_grid_for_set_5_point_set_1'
       call prism_set_points_Gridless(points%id,trim(points%name),grid%id,.true.,ierror)
       ! Finished initialising grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: slab_seaice_initialise_slabseaice_arg5
       slab_seaice_initialise_slabseaice_arg5%coupling_field_no=4
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(slab_seaice_initialise_slabseaice_arg5%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_initialise_slabseaice_arg5'
       coupling_field%name='iland'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_initialise_slabseaice_arg5_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg2
       slab_seaice_slabseaice_arg2%coupling_field_no=5
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg2%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg2'
       coupling_field%name='tstar_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_getFromSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_getFromSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_putToSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg2_putToSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg3
       slab_seaice_slabseaice_arg3%coupling_field_no=6
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg3%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg3'
       coupling_field%name='latent_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg3_getFromSU1_transformer3_new_transformer_3_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=442
       coupling_field_arg_inst%msg_tag='put442_get442'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg3_putToSU1_transformer3_new_transformer_3_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=442
       coupling_field_arg_inst%msg_tag='put442_get442'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg4
       slab_seaice_slabseaice_arg4%coupling_field_no=7
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg4%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg4'
       coupling_field%name='sensible_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg4_getFromSU1_transformer3_new_transformer_3_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=446
       coupling_field_arg_inst%msg_tag='put446_get446'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg4_putToSU1_transformer3_new_transformer_3_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=446
       coupling_field_arg_inst%msg_tag='put446_get446'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg5
       slab_seaice_slabseaice_arg5%coupling_field_no=8
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg5%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg5'
       coupling_field%name='netsolar_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg5_getFromSU1_transformer3_new_transformer_3_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=448
       coupling_field_arg_inst%msg_tag='put448_get448'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg5_putToSU1_transformer3_new_transformer_3_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=448
       coupling_field_arg_inst%msg_tag='put448_get448'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg6
       slab_seaice_slabseaice_arg6%coupling_field_no=9
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg6%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg6'
       coupling_field%name='netlong_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg6_getFromSU1_transformer3_new_transformer_3_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=450
       coupling_field_arg_inst%msg_tag='put450_get450'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg6_putToSU1_transformer3_new_transformer_3_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=450
       coupling_field_arg_inst%msg_tag='put450_get450'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg7
       slab_seaice_slabseaice_arg7%coupling_field_no=10
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg7%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg7'
       coupling_field%name='latent_inst'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_getFromSU1_transformer2_new_transformer_2_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(21)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_getFromSU1_igcm_atmosphere_igcm3_diab_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_getFromSU1_transformer3_new_transformer_3_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_getFromSU1_transformer5_new_transformer_5_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_putToSU1_transformer2_new_transformer_2_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(21)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_putToSU1_igcm_atmosphere_igcm3_diab_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_putToSU1_transformer3_new_transformer_3_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg7_putToSU1_transformer5_new_transformer_5_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=400
       coupling_field_arg_inst%msg_tag='put400_get400'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg8
       slab_seaice_slabseaice_arg8%coupling_field_no=11
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg8%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg8'
       coupling_field%name='sensible_inst'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_getFromSU1_transformer2_new_transformer_2_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(21)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_getFromSU1_igcm_atmosphere_igcm3_diab_arg10',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_getFromSU1_transformer3_new_transformer_3_arg8',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_getFromSU1_transformer5_new_transformer_5_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_putToSU1_transformer2_new_transformer_2_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(21)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_putToSU1_igcm_atmosphere_igcm3_diab_arg10',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_putToSU1_transformer3_new_transformer_3_arg8',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg8_putToSU1_transformer5_new_transformer_5_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=404
       coupling_field_arg_inst%msg_tag='put404_get404'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg9
       slab_seaice_slabseaice_arg9%coupling_field_no=12
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg9%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg9'
       coupling_field%name='netsolar_inst'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_igcm_atmosphere_initialise_atmos_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_igcm_atmosphere_igcm_land_surflux_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(16)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_igcm_atmosphere_igcm3_diab_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_transformer3_new_transformer_3_arg10',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_getFromSU1_transformer5_new_transformer_5_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_igcm_atmosphere_initialise_atmos_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_igcm_atmosphere_igcm_land_surflux_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(16)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_igcm_atmosphere_igcm3_diab_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_transformer3_new_transformer_3_arg10',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg9_putToSU1_transformer5_new_transformer_5_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=179
       coupling_field_arg_inst%msg_tag='put179_get179'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg10
       slab_seaice_slabseaice_arg10%coupling_field_no=13
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg10%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg10'
       coupling_field%name='netlong_inst'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_igcm_atmosphere_initialise_atmos_arg20',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_igcm_atmosphere_igcm_land_surflux_arg8',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(16)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_igcm_atmosphere_igcm3_diab_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_transformer3_new_transformer_3_arg12',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_getFromSU1_transformer5_new_transformer_5_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_igcm_atmosphere_initialise_atmos_arg20',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_igcm_atmosphere_igcm_land_surflux_arg8',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(16)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_igcm_atmosphere_igcm3_diab_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(22)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_transformer3_new_transformer_3_arg12',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg10_putToSU1_transformer5_new_transformer_5_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=180
       coupling_field_arg_inst%msg_tag='put180_get180'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg11
       slab_seaice_slabseaice_arg11%coupling_field_no=14
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg11%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg11'
       coupling_field%name='stressx_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg11_getFromSU1_transformer3_new_transformer_3_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=452
       coupling_field_arg_inst%msg_tag='put452_get452'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg11_putToSU1_transformer3_new_transformer_3_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=452
       coupling_field_arg_inst%msg_tag='put452_get452'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg12
       slab_seaice_slabseaice_arg12%coupling_field_no=15
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg12%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg12'
       coupling_field%name='stressy_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg12_getFromSU1_transformer3_new_transformer_3_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=454
       coupling_field_arg_inst%msg_tag='put454_get454'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg12_putToSU1_transformer3_new_transformer_3_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=454
       coupling_field_arg_inst%msg_tag='put454_get454'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg13
       slab_seaice_slabseaice_arg13%coupling_field_no=16
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg13%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg13'
       coupling_field%name='seaice_frac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg13_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg13_putToSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg16
       slab_seaice_slabseaice_arg16%coupling_field_no=17
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg16%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg16'
       coupling_field%name='albedo_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_getFromSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_getFromSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_putToSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg16_putToSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: slab_seaice_slabseaice_arg17
       slab_seaice_slabseaice_arg17%coupling_field_no=18
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(slab_seaice_slabseaice_arg17%coupling_field_no)
       coupling_field%bfg_id='slab_seaice_slabseaice_arg17'
       coupling_field%name='iland'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'slab_seaice_slabseaice_arg17_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg3
       transformer4_new_transformer_4_arg3%coupling_field_no=19
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer4_new_transformer_4_arg3%coupling_field_no)
       coupling_field%bfg_id='transformer4_new_transformer_4_arg3'
       coupling_field%name='seaicefrac_atm_meanocn'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg3_getFromSU3_bfg_averages_write_averages_arg41',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=461
       coupling_field_arg_inst%msg_tag='put461_get461'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg3_getFromSU3_transformer7_new_transformer_7_arg12',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=461
       coupling_field_arg_inst%msg_tag='put461_get461'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(54)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg3_putToSU3_bfg_averages_write_averages_arg42',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=461
       coupling_field_arg_inst%msg_tag='put461_get1439'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg3_putToSU3_bfg_averages_write_averages_arg41',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=461
       coupling_field_arg_inst%msg_tag='put461_get461'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg3_putToSU3_transformer7_new_transformer_7_arg12',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=461
       coupling_field_arg_inst%msg_tag='put461_get461'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(54)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg4
       transformer4_new_transformer_4_arg4%coupling_field_no=20
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer4_new_transformer_4_arg4%coupling_field_no)
       coupling_field%bfg_id='transformer4_new_transformer_4_arg4'
       coupling_field%name='seaicefrac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg4_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg4_putToSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer4_new_transformer_4_arg7
       transformer4_new_transformer_4_arg7%coupling_field_no=21
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer4_new_transformer_4_arg7%coupling_field_no)
       coupling_field%bfg_id='transformer4_new_transformer_4_arg7'
       coupling_field%name='conductflux_atm_meanocn'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_getFromSU3_bfg_averages_write_averages_arg26',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get465'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_getFromSU3_transformer7_new_transformer_7_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get465'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(54)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_putToSU3_goldstein_goldstein_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get1970'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_putToSU3_bfg_averages_write_averages_arg27',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get1970'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_putToSU3_bfg_averages_write_averages_arg26',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get465'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer4_new_transformer_4_arg7_putToSU3_transformer7_new_transformer_7_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=465
       coupling_field_arg_inst%msg_tag='put465_get465'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(54)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg3
       transformer6_new_transformer_6_arg3%coupling_field_no=22
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg3%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg3'
       coupling_field%name='latent_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg3_getFromSU1_transformer3_new_transformer_3_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=442
       coupling_field_arg_inst%msg_tag='put442_get442'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg3_putToSU1_transformer3_new_transformer_3_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=442
       coupling_field_arg_inst%msg_tag='put442_get442'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg4
       transformer6_new_transformer_6_arg4%coupling_field_no=23
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg4%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg4'
       coupling_field%name='sensible_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg4_getFromSU1_transformer3_new_transformer_3_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=446
       coupling_field_arg_inst%msg_tag='put446_get446'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg4_putToSU1_transformer3_new_transformer_3_arg7',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=446
       coupling_field_arg_inst%msg_tag='put446_get446'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg5
       transformer6_new_transformer_6_arg5%coupling_field_no=24
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg5%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg5'
       coupling_field%name='netsolar_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg5_getFromSU1_transformer3_new_transformer_3_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=448
       coupling_field_arg_inst%msg_tag='put448_get448'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg5_putToSU1_transformer3_new_transformer_3_arg9',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=448
       coupling_field_arg_inst%msg_tag='put448_get448'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg6
       transformer6_new_transformer_6_arg6%coupling_field_no=25
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg6%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg6'
       coupling_field%name='netlong_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg6_getFromSU1_transformer3_new_transformer_3_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=450
       coupling_field_arg_inst%msg_tag='put450_get450'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg6_putToSU1_transformer3_new_transformer_3_arg11',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=450
       coupling_field_arg_inst%msg_tag='put450_get450'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg7
       transformer6_new_transformer_6_arg7%coupling_field_no=26
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg7%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg7'
       coupling_field%name='stressx_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg7_getFromSU1_transformer3_new_transformer_3_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=452
       coupling_field_arg_inst%msg_tag='put452_get452'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg7_putToSU1_transformer3_new_transformer_3_arg13',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=452
       coupling_field_arg_inst%msg_tag='put452_get452'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg8
       transformer6_new_transformer_6_arg8%coupling_field_no=27
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg8%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg8'
       coupling_field%name='stressy_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg8_getFromSU1_transformer3_new_transformer_3_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=454
       coupling_field_arg_inst%msg_tag='put454_get454'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg8_putToSU1_transformer3_new_transformer_3_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=454
       coupling_field_arg_inst%msg_tag='put454_get454'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: transformer6_new_transformer_6_arg9
       transformer6_new_transformer_6_arg9%coupling_field_no=28
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(transformer6_new_transformer_6_arg9%coupling_field_no)
       coupling_field%bfg_id='transformer6_new_transformer_6_arg9'
       coupling_field%name='precip_atm_meansic'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg9_getFromSU1_transformer3_new_transformer_3_arg17',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=456
       coupling_field_arg_inst%msg_tag='put456_get456'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'transformer6_new_transformer_6_arg9_putToSU1_transformer3_new_transformer_3_arg17',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=456
       coupling_field_arg_inst%msg_tag='put456_get456'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(23)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg3
       copy_tstar_inst1_copytstar_arg3%coupling_field_no=29
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(copy_tstar_inst1_copytstar_arg3%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst1_copytstar_arg3'
       coupling_field%name='ilandmask1_atm'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg3_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg3
       copy_tstar_inst2_copytstar_arg3%coupling_field_no=30
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(copy_tstar_inst2_copytstar_arg3%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst2_copytstar_arg3'
       coupling_field%name='ilandmask1_atm'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg3_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg4
       copy_tstar_inst1_copytstar_arg4%coupling_field_no=31
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst1_copytstar_arg4%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst1_copytstar_arg4'
       coupling_field%name='seaicefrac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg4_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg4
       copy_tstar_inst2_copytstar_arg4%coupling_field_no=32
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst2_copytstar_arg4%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst2_copytstar_arg4'
       coupling_field%name='seaicefrac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg4_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg5
       copy_tstar_inst1_copytstar_arg5%coupling_field_no=33
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst1_copytstar_arg5%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst1_copytstar_arg5'
       coupling_field%name='tstar_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_getFromSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_getFromSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_putToSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg5_putToSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg5
       copy_tstar_inst2_copytstar_arg5%coupling_field_no=34
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst2_copytstar_arg5%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst2_copytstar_arg5'
       coupling_field%name='tstar_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_getFromSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_getFromSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_putToSU1_igcm_atmosphere_initialise_atmos_arg16',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg5_putToSU3_bfg_averages_write_averages_arg44',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=517
       coupling_field_arg_inst%msg_tag='put517_get517'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst1_copytstar_arg6
       copy_tstar_inst1_copytstar_arg6%coupling_field_no=35
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst1_copytstar_arg6%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst1_copytstar_arg6'
       coupling_field%name='dummy_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_goldstein_initialise_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_goldstein_goldstein_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_bfg_averages_write_averages_arg45',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_goldstein_initialise_goldstein_arg23',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_goldstein_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_bfg_averages_write_averages_arg48',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put528_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst1_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put530_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_tstar_inst2_copytstar_arg6
       copy_tstar_inst2_copytstar_arg6%coupling_field_no=36
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_tstar_inst2_copytstar_arg6%coupling_field_no)
       coupling_field%bfg_id='copy_tstar_inst2_copytstar_arg6'
       coupling_field%name='dummy_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_goldstein_initialise_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_goldstein_goldstein_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_bfg_averages_write_averages_arg45',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_goldstein_initialise_goldstein_arg23',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_goldstein_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_bfg_averages_write_averages_arg48',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put528_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_tstar_inst2_copytstar_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put530_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg3
       copy_albedo_inst1_copyalbedo_arg3%coupling_field_no=37
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(copy_albedo_inst1_copyalbedo_arg3%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst1_copyalbedo_arg3'
       coupling_field%name='ilandmask1_atm'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg3_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: gridless_grid_for_set_5
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg3
       copy_albedo_inst2_copyalbedo_arg3%coupling_field_no=38
       grid=>component%gridless_grids(5)
       coupling_field=>component%coupling_fields(copy_albedo_inst2_copyalbedo_arg3%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst2_copyalbedo_arg3'
       coupling_field%name='ilandmask1_atm'
       coupling_field%type_id=PRISM_Integer
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg3_getFromSU3_initialise_fixedicesheet_mod_initialise_fixedicesheet_arg1',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=515
       coupling_field_arg_inst%msg_tag='put515_get515'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(2)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: gridless_grid_for_set_5
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg4
       copy_albedo_inst1_copyalbedo_arg4%coupling_field_no=39
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst1_copyalbedo_arg4%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst1_copyalbedo_arg4'
       coupling_field%name='seaicefrac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg4_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg4
       copy_albedo_inst2_copyalbedo_arg4%coupling_field_no=40
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst2_copyalbedo_arg4%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst2_copyalbedo_arg4'
       coupling_field%name='seaicefrac_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg4_getFromSU1_transformer5_new_transformer_5_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=516
       coupling_field_arg_inst%msg_tag='put516_get516'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(27)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg5
       copy_albedo_inst1_copyalbedo_arg5%coupling_field_no=41
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst1_copyalbedo_arg5%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst1_copyalbedo_arg5'
       coupling_field%name='albedo_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_getFromSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_getFromSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_putToSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg5_putToSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg5
       copy_albedo_inst2_copyalbedo_arg5%coupling_field_no=42
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst2_copyalbedo_arg5%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst2_copyalbedo_arg5'
       coupling_field%name='albedo_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_getFromSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_getFromSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_getFromSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_putToSU1_igcm_atmosphere_initialise_atmos_arg18',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(5)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_putToSU1_igcm_atmosphere_igcm_ocean_surflux_arg14',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(19)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg5_putToSU3_bfg_averages_write_averages_arg47',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=524
       coupling_field_arg_inst%msg_tag='put524_get524'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       coupling_field_arg_inst%next_instance=>coupling_field_ep_inst%put_instances
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst1_copyalbedo_arg6
       copy_albedo_inst1_copyalbedo_arg6%coupling_field_no=43
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst1_copyalbedo_arg6%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst1_copyalbedo_arg6'
       coupling_field%name='dummy_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_goldstein_initialise_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_goldstein_goldstein_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_bfg_averages_write_averages_arg45',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_goldstein_initialise_goldstein_arg23',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_goldstein_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_bfg_averages_write_averages_arg48',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put528_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst1_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put530_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       ! Initialising coupling field for grid: IGCM_T21
       ! The BFG ID string for this coupling field is: copy_albedo_inst2_copyalbedo_arg6
       copy_albedo_inst2_copyalbedo_arg6%coupling_field_no=44
       grid=>component%gridded_grids(1)
       coupling_field=>component%coupling_fields(copy_albedo_inst2_copyalbedo_arg6%coupling_field_no)
       coupling_field%bfg_id='copy_albedo_inst2_copyalbedo_arg6'
       coupling_field%name='dummy_atm'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       do index_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(index_loop)
       nullify(coupling_field_ep_inst%get_instance)
       nullify(coupling_field_ep_inst%put_instances)
       end do
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_goldstein_initialise_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_goldstein_goldstein_arg15',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg4',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_bfg_averages_write_averages_arg45',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put97_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_goldstein_initialise_goldstein_arg23',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(7)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_goldstein_goldstein_arg19',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(42)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg6',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_bfg_averages_write_averages_arg48',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put101_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(52)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg3',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put528_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Create a coupling field instance for this entry point argument
       allocate(coupling_field_arg_inst)
       call prism_def_var(coupling_field_arg_inst%prism_id,'copy_albedo_inst2_copyalbedo_arg6_getFromSU3_copy_dummy_goldstein_copy_dummy_arg5',grid%id,grid%point_sets(1)%id,grid%landmask%id,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       coupling_field_arg_inst%bfg_ref=518
       coupling_field_arg_inst%msg_tag='put530_get518'
       ! Add the coupling field instance to the entry point list
       coupling_field_ep_inst=>coupling_field%ep_instances(43)
       nullify(coupling_field_arg_inst%next_instance)
       coupling_field_ep_inst%get_instance=>coupling_field_arg_inst
       ! Finished initialising coupling field for grid: IGCM_T21
       end if
       ! Initialisation phase is complete
       call prism_enddef(ierror)
       ! Set the date/time bounds within which coupling will be valid
       model_time=PRISM_jobstart_date
       model_time_bounds(1)=model_time
       model_time_bounds(2)=model_time
       call prism_calc_newdate(model_time_bounds(1),-3600.0,ierror)
       call prism_calc_newdate(model_time_bounds(2),3600.0,ierror)
       end subroutine initComms
       subroutine finaliseComms()
       use prism
       implicit none
       !ierror
       integer :: ierror
       !coupling_field_loop
       integer :: coupling_field_loop
       !ep_instance_loop
       integer :: ep_instance_loop
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       call oasisvis_final(2)
       call prism_terminate(ierror)
       ! Deallocate lists of coupling field instances
       do coupling_field_loop=1,component%coupling_fields_count
       coupling_field=>component%coupling_fields(coupling_field_loop)
       do ep_instance_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(ep_instance_loop)
       if(associated(coupling_field_ep_inst%get_instance))deallocate(coupling_field_ep_inst%get_instance)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       do while(associated(coupling_field_arg_inst))
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst%next_instance
       if(associated(coupling_field_arg_inst))deallocate(coupling_field_arg_inst)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       end do
       end do
       end do
       ! Deallocate the arrays of coupling fields and grids
       if(allocated(component%coupling_fields))deallocate(component%coupling_fields)
       if(allocated(component%gridless_grids))deallocate(component%gridless_grids)
       if(allocated(component%gridded_grids))deallocate(component%gridded_grids)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=bfg_averages.ini_averages.
       info(1)%bfg_du=3
       info(1)%du=b2mmap(3)
       info(1)%su=3
       info(1)%period=1
       info(1)%nesting=0
       info(1)%bound=1
       info(1)%offset=0
       nullify(info(1)%its)
       ! model.ep=initialise_fixedicesheet_mod.initialise_fixedicesheet.
       info(2)%bfg_du=3
       info(2)%du=b2mmap(3)
       info(2)%su=3
       info(2)%period=1
       info(2)%nesting=0
       info(2)%bound=1
       info(2)%offset=0
       nullify(info(2)%its)
       ! model.ep=fixed_chemistry.initialise_fixedchem.
       info(3)%bfg_du=3
       info(3)%du=b2mmap(3)
       info(3)%su=3
       info(3)%period=1
       info(3)%nesting=0
       info(3)%bound=1
       info(3)%offset=0
       nullify(info(3)%its)
       ! model.ep=igcm_atmosphere.initialise_igcmsurf.
       info(4)%bfg_du=1
       info(4)%du=b2mmap(1)
       info(4)%su=1
       info(4)%period=1
       info(4)%nesting=0
       info(4)%bound=1
       info(4)%offset=0
       nullify(info(4)%its)
       ! model.ep=igcm_atmosphere.initialise_atmos.
       info(5)%bfg_du=1
       info(5)%du=b2mmap(1)
       info(5)%su=1
       info(5)%period=1
       info(5)%nesting=0
       info(5)%bound=1
       info(5)%offset=0
       nullify(info(5)%its)
       ! model.ep=slab_seaice.initialise_slabseaice.
       info(6)%bfg_du=2
       info(6)%du=b2mmap(2)
       info(6)%su=2
       info(6)%period=1
       info(6)%nesting=0
       info(6)%bound=1
       info(6)%offset=0
       nullify(info(6)%its)
       ! model.ep=goldstein.initialise_goldstein.
       info(7)%bfg_du=3
       info(7)%du=b2mmap(3)
       info(7)%su=3
       info(7)%period=1
       info(7)%nesting=0
       info(7)%bound=1
       info(7)%offset=0
       nullify(info(7)%its)
       ! model.ep=interp_bilinear.regrid.1
       info(8)%bfg_du=3
       info(8)%du=b2mmap(3)
       info(8)%su=3
       info(8)%period=1
       info(8)%nesting=0
       info(8)%bound=1
       info(8)%offset=0
       nullify(info(8)%its)
       ! model.ep=copy_tstar.copytstar.1
       info(9)%bfg_du=2
       info(9)%du=b2mmap(2)
       info(9)%su=2
       info(9)%period=1
       info(9)%nesting=0
       info(9)%bound=1
       info(9)%offset=0
       nullify(info(9)%its)
       ! model.ep=interp_bilinear.regrid.2
       info(10)%bfg_du=3
       info(10)%du=b2mmap(3)
       info(10)%su=3
       info(10)%period=1
       info(10)%nesting=0
       info(10)%bound=1
       info(10)%offset=0
       nullify(info(10)%its)
       ! model.ep=copy_albedo.copyalbedo.1
       info(11)%bfg_du=2
       info(11)%du=b2mmap(2)
       info(11)%su=2
       info(11)%period=1
       info(11)%nesting=0
       info(11)%bound=1
       info(11)%offset=0
       nullify(info(11)%its)
       ! model.ep=counter.counter.1
       info(12)%bfg_du=1
       info(12)%du=b2mmap(1)
       info(12)%su=1
       info(12)%period=1
       info(12)%nesting=1
       info(12)%bound=864
       info(12)%offset=0
       info(12)%its=>its1
       ! model.ep=igcm_atmosphere.igcm3_adiab.
       info(13)%bfg_du=1
       info(13)%du=b2mmap(1)
       info(13)%su=1
       info(13)%period=1
       info(13)%nesting=1
       info(13)%bound=864
       info(13)%offset=0
       info(13)%its=>its1
       ! model.ep=transformer1.new_transformer_1.
       info(14)%bfg_du=1
       info(14)%du=b2mmap(1)
       info(14)%su=1
       info(14)%period=1
       info(14)%nesting=1
       info(14)%bound=864
       info(14)%offset=0
       info(14)%its=>its1
       ! model.ep=counter_mod.counter_mod.1
       info(15)%bfg_du=1
       info(15)%du=b2mmap(1)
       info(15)%su=1
       info(15)%period=1
       info(15)%nesting=2
       info(15)%bound=6
       info(15)%offset=0
       info(15)%its=>its2
       ! model.ep=igcm_atmosphere.igcm_land_surflux.
       info(16)%bfg_du=1
       info(16)%du=b2mmap(1)
       info(16)%su=1
       info(16)%period=1
       info(16)%nesting=2
       info(16)%bound=6
       info(16)%offset=0
       info(16)%its=>its2
       ! model.ep=igcm_atmosphere.igcm_land_blayer.
       info(17)%bfg_du=1
       info(17)%du=b2mmap(1)
       info(17)%su=1
       info(17)%period=1
       info(17)%nesting=2
       info(17)%bound=6
       info(17)%offset=0
       info(17)%its=>its2
       ! model.ep=counter_mod.counter_mod.2
       info(18)%bfg_du=1
       info(18)%du=b2mmap(1)
       info(18)%su=1
       info(18)%period=1
       info(18)%nesting=2
       info(18)%bound=6
       info(18)%offset=0
       info(18)%its=>its3
       ! model.ep=igcm_atmosphere.igcm_ocean_surflux.
       info(19)%bfg_du=1
       info(19)%du=b2mmap(1)
       info(19)%su=1
       info(19)%period=1
       info(19)%nesting=2
       info(19)%bound=6
       info(19)%offset=0
       info(19)%its=>its3
       ! model.ep=igcm_atmosphere.igcm_ocean_blayer.
       info(20)%bfg_du=1
       info(20)%du=b2mmap(1)
       info(20)%su=1
       info(20)%period=1
       info(20)%nesting=2
       info(20)%bound=6
       info(20)%offset=0
       info(20)%its=>its3
       ! model.ep=transformer2.new_transformer_2.
       info(21)%bfg_du=1
       info(21)%du=b2mmap(1)
       info(21)%su=1
       info(21)%period=1
       info(21)%nesting=1
       info(21)%bound=864
       info(21)%offset=0
       info(21)%its=>its1
       ! model.ep=igcm_atmosphere.igcm3_diab.
       info(22)%bfg_du=1
       info(22)%du=b2mmap(1)
       info(22)%su=1
       info(22)%period=1
       info(22)%nesting=1
       info(22)%bound=864
       info(22)%offset=0
       info(22)%its=>its1
       ! model.ep=transformer3.new_transformer_3.
       info(23)%bfg_du=1
       info(23)%du=b2mmap(1)
       info(23)%su=1
       info(23)%period=1
       info(23)%nesting=1
       info(23)%bound=864
       info(23)%offset=0
       info(23)%its=>its1
       ! model.ep=counter.counter.2
       info(24)%bfg_du=2
       info(24)%du=b2mmap(2)
       info(24)%su=2
       info(24)%period=6
       info(24)%nesting=1
       info(24)%bound=864
       info(24)%offset=0
       info(24)%its=>its1
       ! model.ep=slab_seaice.slabseaice.
       info(25)%bfg_du=2
       info(25)%du=b2mmap(2)
       info(25)%su=2
       info(25)%period=6
       info(25)%nesting=1
       info(25)%bound=864
       info(25)%offset=0
       info(25)%its=>its1
       ! model.ep=transformer4.new_transformer_4.
       info(26)%bfg_du=2
       info(26)%du=b2mmap(2)
       info(26)%su=2
       info(26)%period=6
       info(26)%nesting=1
       info(26)%bound=864
       info(26)%offset=0
       info(26)%its=>its1
       ! model.ep=transformer5.new_transformer_5.
       info(27)%bfg_du=1
       info(27)%du=b2mmap(1)
       info(27)%su=1
       info(27)%period=1
       info(27)%nesting=1
       info(27)%bound=864
       info(27)%offset=0
       info(27)%its=>its1
       ! model.ep=interp_bilinear.regrid.3
       info(28)%bfg_du=3
       info(28)%du=b2mmap(3)
       info(28)%su=3
       info(28)%period=48
       info(28)%nesting=1
       info(28)%bound=864
       info(28)%offset=0
       info(28)%its=>its1
       ! model.ep=interp_bilinear.regrid.4
       info(29)%bfg_du=3
       info(29)%du=b2mmap(3)
       info(29)%su=3
       info(29)%period=48
       info(29)%nesting=1
       info(29)%bound=864
       info(29)%offset=0
       info(29)%its=>its1
       ! model.ep=interp_bilinear.regrid.5
       info(30)%bfg_du=3
       info(30)%du=b2mmap(3)
       info(30)%su=3
       info(30)%period=48
       info(30)%nesting=1
       info(30)%bound=864
       info(30)%offset=0
       info(30)%its=>its1
       ! model.ep=interp_bilinear.regrid.6
       info(31)%bfg_du=3
       info(31)%du=b2mmap(3)
       info(31)%su=3
       info(31)%period=48
       info(31)%nesting=1
       info(31)%bound=864
       info(31)%offset=0
       info(31)%its=>its1
       ! model.ep=interp_bilinear.regrid.7
       info(32)%bfg_du=3
       info(32)%du=b2mmap(3)
       info(32)%su=3
       info(32)%period=48
       info(32)%nesting=1
       info(32)%bound=864
       info(32)%offset=0
       info(32)%its=>its1
       ! model.ep=interp_bilinear.regrid.8
       info(33)%bfg_du=3
       info(33)%du=b2mmap(3)
       info(33)%su=3
       info(33)%period=48
       info(33)%nesting=1
       info(33)%bound=864
       info(33)%offset=0
       info(33)%its=>its1
       ! model.ep=interp_bilinear.regrid.9
       info(34)%bfg_du=3
       info(34)%du=b2mmap(3)
       info(34)%su=3
       info(34)%period=48
       info(34)%nesting=1
       info(34)%bound=864
       info(34)%offset=0
       info(34)%its=>its1
       ! model.ep=interp_bilinear.regrid.10
       info(35)%bfg_du=3
       info(35)%du=b2mmap(3)
       info(35)%su=3
       info(35)%period=48
       info(35)%nesting=1
       info(35)%bound=864
       info(35)%offset=0
       info(35)%its=>its1
       ! model.ep=interp_bilinear.regrid.11
       info(36)%bfg_du=3
       info(36)%du=b2mmap(3)
       info(36)%su=3
       info(36)%period=48
       info(36)%nesting=1
       info(36)%bound=864
       info(36)%offset=0
       info(36)%its=>its1
       ! model.ep=interp_bilinear.regrid.12
       info(37)%bfg_du=3
       info(37)%du=b2mmap(3)
       info(37)%su=3
       info(37)%period=48
       info(37)%nesting=1
       info(37)%bound=864
       info(37)%offset=0
       info(37)%its=>its1
       ! model.ep=interp_bilinear.regrid.13
       info(38)%bfg_du=3
       info(38)%du=b2mmap(3)
       info(38)%su=3
       info(38)%period=48
       info(38)%nesting=1
       info(38)%bound=864
       info(38)%offset=0
       info(38)%its=>its1
       ! model.ep=interp_bilinear.regrid.14
       info(39)%bfg_du=3
       info(39)%du=b2mmap(3)
       info(39)%su=3
       info(39)%period=48
       info(39)%nesting=1
       info(39)%bound=864
       info(39)%offset=0
       info(39)%its=>its1
       ! model.ep=interp_bilinear.regrid.15
       info(40)%bfg_du=3
       info(40)%du=b2mmap(3)
       info(40)%su=3
       info(40)%period=48
       info(40)%nesting=1
       info(40)%bound=864
       info(40)%offset=0
       info(40)%its=>its1
       ! model.ep=counter.counter.3
       info(41)%bfg_du=3
       info(41)%du=b2mmap(3)
       info(41)%su=3
       info(41)%period=48
       info(41)%nesting=1
       info(41)%bound=864
       info(41)%offset=0
       info(41)%its=>its1
       ! model.ep=goldstein.goldstein.
       info(42)%bfg_du=3
       info(42)%du=b2mmap(3)
       info(42)%su=3
       info(42)%period=48
       info(42)%nesting=1
       info(42)%bound=864
       info(42)%offset=0
       info(42)%its=>its1
       ! model.ep=copy_dummy_goldstein.copy_dummy.
       info(43)%bfg_du=3
       info(43)%du=b2mmap(3)
       info(43)%su=3
       info(43)%period=48
       info(43)%nesting=1
       info(43)%bound=864
       info(43)%offset=0
       info(43)%its=>its1
       ! model.ep=interp_bilinear.regrid.16
       info(44)%bfg_du=3
       info(44)%du=b2mmap(3)
       info(44)%su=3
       info(44)%period=48
       info(44)%nesting=1
       info(44)%bound=864
       info(44)%offset=0
       info(44)%its=>its1
       ! model.ep=copy_tstar.copytstar.2
       info(45)%bfg_du=2
       info(45)%du=b2mmap(2)
       info(45)%su=2
       info(45)%period=48
       info(45)%nesting=1
       info(45)%bound=864
       info(45)%offset=0
       info(45)%its=>its1
       ! model.ep=interp_bilinear.regrid.17
       info(46)%bfg_du=3
       info(46)%du=b2mmap(3)
       info(46)%su=3
       info(46)%period=48
       info(46)%nesting=1
       info(46)%bound=864
       info(46)%offset=0
       info(46)%its=>its1
       ! model.ep=copy_albedo.copyalbedo.2
       info(47)%bfg_du=2
       info(47)%du=b2mmap(2)
       info(47)%su=2
       info(47)%period=48
       info(47)%nesting=1
       info(47)%bound=864
       info(47)%offset=0
       info(47)%its=>its1
       ! model.ep=counter.counter.4
       info(48)%bfg_du=3
       info(48)%du=b2mmap(3)
       info(48)%su=3
       info(48)%period=240
       info(48)%nesting=1
       info(48)%bound=864
       info(48)%offset=0
       info(48)%its=>its1
       ! model.ep=fixed_chemistry.fixedchem.
       info(49)%bfg_du=3
       info(49)%du=b2mmap(3)
       info(49)%su=3
       info(49)%period=240
       info(49)%nesting=1
       info(49)%bound=864
       info(49)%offset=0
       info(49)%its=>its1
       ! model.ep=counter.counter.5
       info(50)%bfg_du=3
       info(50)%du=b2mmap(3)
       info(50)%su=3
       info(50)%period=240
       info(50)%nesting=1
       info(50)%bound=864
       info(50)%offset=0
       info(50)%its=>its1
       ! model.ep=fixed_icesheet.fixedicesheet.
       info(51)%bfg_du=3
       info(51)%du=b2mmap(3)
       info(51)%su=3
       info(51)%period=240
       info(51)%nesting=1
       info(51)%bound=864
       info(51)%offset=0
       info(51)%its=>its1
       ! model.ep=bfg_averages.write_averages.
       info(52)%bfg_du=3
       info(52)%du=b2mmap(3)
       info(52)%su=3
       info(52)%period=48
       info(52)%nesting=1
       info(52)%bound=864
       info(52)%offset=0
       info(52)%its=>its1
       ! model.ep=transformer6.new_transformer_6.
       info(53)%bfg_du=2
       info(53)%du=b2mmap(2)
       info(53)%su=2
       info(53)%period=6
       info(53)%nesting=1
       info(53)%bound=864
       info(53)%offset=0
       info(53)%its=>its1
       ! model.ep=transformer7.new_transformer_7.
       info(54)%bfg_du=3
       info(54)%du=b2mmap(3)
       info(54)%su=3
       info(54)%period=48
       info(54)%nesting=1
       info(54)%bound=864
       info(54)%offset=0
       info(54)%its=>its1
       ! model.ep=igcm_atmosphere.end_atmos.
       info(55)%bfg_du=1
       info(55)%du=b2mmap(1)
       info(55)%su=1
       info(55)%period=1
       info(55)%nesting=0
       info(55)%bound=1
       info(55)%offset=0
       nullify(info(55)%its)
       ! model.ep=goldstein.end_goldstein.
       info(56)%bfg_du=3
       info(56)%du=b2mmap(3)
       info(56)%su=3
       info(56)%period=1
       info(56)%nesting=0
       info(56)%bound=1
       info(56)%offset=0
       nullify(info(56)%its)
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !currentNesting
       integer :: currentNesting
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !targetpos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getNext=findNext(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestNext
       integer :: nestNext
       !currentMin
       integer :: currentMin
       !remainIters
       integer :: remainIters
       !waitIters
       integer :: waitIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1.or.its==0) then
       its=1
       end if
       if (list(pos).gt.point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestNext=findNext(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       deallocate(newlist)
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters.gt.0) then
       if (its+1.lt.info(list(pos))%offset) then
       waitIters=info(list(pos))%offset - its
       else if (its+1==info(list(pos))%offset) then
       waitIters=1
       else
       waitIters=info(list(pos))%period - mod(its - info(list(pos))%offset,info(list(pos))%period)
       end if
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters.lt.currentMin.and.waitIters.le.remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
       end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !currentNesting
       integer :: currentNesting
       !targetpos
       integer :: targetpos
       !currentMin
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getLast=findLast(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestLast
       integer :: nestLast
       !currentMin
       integer :: currentMin
       !elapsedIters
       integer :: elapsedIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos).ge.point) then
       its=its - 1
       end if
       if (.not.(associated(info(list(pos))%its)).and.((info(point)%nesting==1.and.its1.gt.info(point)%period).or.(info(point)%nesting.gt.1.and.its1.gt.1))) then
       continue
       else if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestLast=findLast(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       deallocate(newlist)
       else
       if (its.gt.0.and.its.ge.info(list(pos))%offset) then
       elapsedIters=mod(its - info(list(pos))%offset,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters.lt.currentMin.and.elapsedIters.lt.its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
       end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       !i
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       do i=pos+1,lsize
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findStartPoint=1
       return
       end if
       do i=pos-1,1,-1
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
       findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findEndPoint=lsize
       return
       end if
       do i=pos+1,lsize
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
       findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target2
