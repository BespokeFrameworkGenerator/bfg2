       ! f77 to f90 put/get wrappers start
       subroutine put_copy_dummy_goldstein(data,tag)
       use BFG2Target3
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==43) then
       if (tag==-528) then
       call putreal__2_copy_dummy_goldstein(data,tag)
       end if
       if (tag==-97) then
       call putreal__2_copy_dummy_goldstein(data,tag)
       end if
       if (tag==-530) then
       call putreal__2_copy_dummy_goldstein(data,tag)
       end if
       if (tag==-101) then
       call putreal__2_copy_dummy_goldstein(data,tag)
       end if
       end if
       end subroutine put_copy_dummy_goldstein
       subroutine putreal__2_copy_dummy_goldstein(data,tag)
       use BFG2InPlace_copy_dummy_goldstein, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_copy_dummy_goldstein
       ! f77 to f90 put/get wrappers end
