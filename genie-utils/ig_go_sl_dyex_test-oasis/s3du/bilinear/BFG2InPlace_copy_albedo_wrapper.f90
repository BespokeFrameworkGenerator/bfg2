       ! f77 to f90 put/get wrappers start
       subroutine put_copy_albedo(data,tag)
       use BFG2Target2
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==11) then
       if (tag==-524) then
       call putreal__2_copy_albedo(data,tag)
       end if
       end if
       if (currentModel==47) then
       if (tag==-524) then
       call putreal__2_copy_albedo(data,tag)
       end if
       end if
       end subroutine put_copy_albedo
       subroutine get_copy_albedo(data,tag)
       use BFG2Target2
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==11) then
       if (tag==-515) then
       call getinteger__2_copy_albedo(data,tag)
       end if
       end if
       if (currentModel==47) then
       if (tag==-515) then
       call getinteger__2_copy_albedo(data,tag)
       end if
       end if
       if (currentModel==11) then
       if (tag==-516) then
       call getreal__2_copy_albedo(data,tag)
       end if
       if (tag==-524) then
       call getreal__2_copy_albedo(data,tag)
       end if
       if (tag==-518) then
       call getreal__2_copy_albedo(data,tag)
       end if
       end if
       if (currentModel==47) then
       if (tag==-516) then
       call getreal__2_copy_albedo(data,tag)
       end if
       if (tag==-524) then
       call getreal__2_copy_albedo(data,tag)
       end if
       if (tag==-518) then
       call getreal__2_copy_albedo(data,tag)
       end if
       end if
       end subroutine get_copy_albedo
       subroutine getinteger__2_copy_albedo(data,tag)
       use BFG2InPlace_copy_albedo, only : get=>getinteger__2
       implicit none
       integer , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getinteger__2_copy_albedo
       subroutine getreal__2_copy_albedo(data,tag)
       use BFG2InPlace_copy_albedo, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_copy_albedo
       subroutine putreal__2_copy_albedo(data,tag)
       use BFG2InPlace_copy_albedo, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_copy_albedo
       ! f77 to f90 put/get wrappers end
