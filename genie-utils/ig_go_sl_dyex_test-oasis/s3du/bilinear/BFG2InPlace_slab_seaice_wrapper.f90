       ! f77 to f90 put/get wrappers start
       subroutine put_slab_seaice(data,tag)
       use BFG2Target2
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-517) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-524) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-516) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-517) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-442) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-446) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-448) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-450) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-400) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-404) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-179) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-180) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-452) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-454) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-516) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-524) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine put_slab_seaice
       subroutine get_slab_seaice(data,tag)
       use BFG2Target2
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-517) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-524) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-516) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-517) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-442) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-446) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-448) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-450) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-400) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-404) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-179) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-180) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-452) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-454) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-516) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-524) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==6) then
       if (tag==-515) then
       call getinteger__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-515) then
       call getinteger__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine get_slab_seaice
       subroutine getreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_slab_seaice
       subroutine getinteger__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : get=>getinteger__2
       implicit none
       integer , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getinteger__2_slab_seaice
       subroutine putreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_slab_seaice
       ! f77 to f90 put/get wrappers end
