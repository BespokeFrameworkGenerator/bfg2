       program BFG2Main
       use BFG2Target3
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_nneighbour2D_3_freq
       integer :: interp_nneighbour2D_3_freq
       !interp_nneighbour2D_4_freq
       integer :: interp_nneighbour2D_4_freq
       !interp_nneighbour2D_5_freq
       integer :: interp_nneighbour2D_5_freq
       !interp_nneighbour2D_6_freq
       integer :: interp_nneighbour2D_6_freq
       !interp_nneighbour2D_7_freq
       integer :: interp_nneighbour2D_7_freq
       !interp_nneighbour2D_8_freq
       integer :: interp_nneighbour2D_8_freq
       !interp_nneighbour2D_9_freq
       integer :: interp_nneighbour2D_9_freq
       !interp_nneighbour2D_10_freq
       integer :: interp_nneighbour2D_10_freq
       !interp_nneighbour2D_11_freq
       integer :: interp_nneighbour2D_11_freq
       !interp_nneighbour2D_12_freq
       integer :: interp_nneighbour2D_12_freq
       !interp_nneighbour2D_13_freq
       integer :: interp_nneighbour2D_13_freq
       !interp_nneighbour2D_14_freq
       integer :: interp_nneighbour2D_14_freq
       !interp_nneighbour2D_15_freq
       integer :: interp_nneighbour2D_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy_goldstein__freq
       integer :: copy_dummy_goldstein__freq
       !interp_nneighbour2D_16_freq
       integer :: interp_nneighbour2D_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_nneighbour2D_17_freq
       integer :: interp_nneighbour2D_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,counter_2_freq,interp_nneighbour2D_3_freq,interp_nneighbour2D_4_freq,interp_nneighbour2D_5_freq,interp_nneighbour2D_6_freq,interp_nneighbour2D_7_freq,interp_nneighbour2D_8_freq,interp_nneighbour2D_9_freq,interp_nneighbour2D_10_freq,interp_nneighbour2D_11_freq,interp_nneighbour2D_12_freq,interp_nneighbour2D_13_freq,interp_nneighbour2D_14_freq,interp_nneighbour2D_15_freq,counter_3_freq,goldstein__freq,copy_dummy_goldstein__freq,interp_nneighbour2D_16_freq,copy_tstar_2_freq,interp_nneighbour2D_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !olon2
       real, dimension(1:36) :: r81
       !olat2
       real, dimension(1:36) :: r82
       !olon3
       real, dimension(1:36) :: r83
       !olat3
       real, dimension(1:36) :: r84
       !oboxedge2_lon
       real, dimension(1:37) :: r87
       !oboxedge2_lat
       real, dimension(1:37) :: r88
       !oboxedge3_lon
       real, dimension(1:37) :: r89
       !oboxedge3_lat
       real, dimension(1:37) :: r90
       !depth
       real, dimension(1:8) :: r91
       !depth1
       real, dimension(1:9) :: r92
       !ilandmask2
       integer, dimension(1:36,1:36) :: r94
       !ilandmask3
       integer, dimension(1:36,1:36) :: r95
       !ias_out
       integer, dimension(1:36) :: r102
       !iaf_out
       integer, dimension(1:36) :: r103
       !ips_out
       integer, dimension(1:36) :: r104
       !ipf_out
       integer, dimension(1:36) :: r105
       !jsf_out
       integer :: r106
       !go_rhosc
       real :: r119
       !go_scf
       real :: r121
       !go_k1
       integer, dimension(1:36,1:36) :: r122
       !go_dz
       real, dimension(1:8) :: r123
       !go_dza
       real, dimension(1:8) :: r124
       !go_ias
       integer, dimension(1:36) :: r125
       !go_iaf
       integer, dimension(1:36) :: r126
       !go_c
       real, dimension(0:36) :: r128
       !go_cv
       real, dimension(0:36) :: r129
       !go_s
       real, dimension(0:36) :: r130
       !go_sv
       real, dimension(0:36) :: r131
       !test_energy_ocean
       real :: r153
       !test_water_ocean
       real :: r154
       !koverall
       integer :: r155
       !go_cost
       real, dimension(1:36,1:36) :: r158
       !go_u
       real, dimension(1:3,1:36,1:36,1:8) :: r159
       !go_tau
       real, dimension(1:2,1:36,1:36) :: r160
       ! Set Notation Vars
       !write_flag_atm
       logical :: r1
       !write_flag_ocn
       logical :: r2
       !write_flag_sic
       logical :: r3
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r515
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !alon1_atm
       real, dimension(1:64) :: r163
       !alat1_atm
       real, dimension(1:32) :: r164
       !tstar_atm
       real, dimension(1:64,1:32) :: r517
       !albedo_atm
       real, dimension(1:64,1:32) :: r524
       !alon1_ocn
       real, dimension(1:36) :: r79
       !alat1_ocn
       real, dimension(1:36) :: r80
       !aboxedge1_lon_ocn
       real, dimension(1:37) :: r85
       !aboxedge1_lat_ocn
       real, dimension(1:37) :: r86
       !ilandmask1_ocn
       integer, dimension(1:36,1:36) :: r93
       !tstar_ocn
       real, dimension(1:36,1:36) :: r97
       !sstar_ocn
       real, dimension(1:36,1:36) :: r98
       !ustar_ocn
       real, dimension(1:36,1:36) :: r99
       !vstar_ocn
       real, dimension(1:36,1:36) :: r100
       !albedo_ocn
       real, dimension(1:36,1:36) :: r101
       !go_ts
       real, dimension(1:14,1:36,1:36,1:8) :: r132
       !go_ts1
       real, dimension(1:14,1:36,1:36,1:8) :: r133
       !dummy_atm
       real, dimension(1:64,1:32) :: r518
       !iconv_ice
       integer :: r222
       !iconv_che
       integer :: r246
       !kocn_loop
       integer :: r458
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r461
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r465
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r469
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r474
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r476
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r478
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r480
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r482
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r484
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r486
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r488
       !istep_ocn
       integer :: r1435
       !seaicefrac_ocn
       real, dimension(1:36,1:36) :: r1439
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r1970
       !latent_ocn
       real, dimension(1:36,1:36) :: r2501
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r3032
       !netlong_ocn
       real, dimension(1:36,1:36) :: r3563
       !sensible_ocn
       real, dimension(1:36,1:36) :: r4094
       !ocean_stressx2_ocn
       real, dimension(1:36,1:36) :: r4625
       !ocean_stressx3_ocn
       real, dimension(1:36,1:36) :: r5156
       !ocean_stressy2_ocn
       real, dimension(1:36,1:36) :: r5687
       !ocean_stressy3_ocn
       real, dimension(1:36,1:36) :: r6218
       !precip_ocn
       real, dimension(1:36,1:36) :: r6749
       !evap_ocn
       real, dimension(1:36,1:36) :: r7280
       !runoff_ocn
       real, dimension(1:36,1:36) :: r7811
       !dumtmp_ocn
       real, allocatable, dimension(:,:) :: r528
       !dumalb_ocn
       real, allocatable, dimension(:,:) :: r530
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r143
       !istep_che
       integer :: r1966
       !istep_lic
       integer :: r2497
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !ilon1_atm
       integer :: r514
       !ilat1_atm
       integer :: r513
       !ilon1_ocn
       integer :: r526
       !ilat1_ocn
       integer :: r527
       !outputdir_name
       character(len=200) :: r60
       !fname_restart_main
       character(len=200) :: r58
       !dt_write
       integer :: r59
       !genie_timestep
       real :: r57
       !lrestart_genie
       logical :: r107
       !totsteps
       integer :: r96
       !go_saln0
       real :: r108
       !go_rhoair
       real :: r109
       !go_cd
       real :: r110
       !go_ds
       real, dimension(1:36) :: r111
       !go_dphi
       real :: r112
       !go_ips
       integer, dimension(1:36) :: r113
       !go_ipf
       integer, dimension(1:36) :: r114
       !go_usc
       real :: r115
       !go_dsc
       real :: r116
       !go_fsc
       real :: r117
       !go_rh0sc
       real :: r118
       !go_jsf
       integer :: r127
       !go_cpsc
       real :: r120
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r1=.true.
           r2=.true.
           r3=.false.
           r515=0
           r76=0.0
           r77=0.0
           r78=0.0
           r61=0.0
           r62=0.0
           r63=0.0
           r163=0.0
           r164=0.0
           r517=0.0
           r524=0.0
           r79=0.0
           r80=0.0
           r85=0.0
           r86=0.0
           r93=0
           r97=0.0
           r98=0.0
           r99=0.0
           r100=0.0
           r101=0.0
           r132=0.0
           r133=0.0
           r222=0.0
           r246=0
           r458=48
           r461=0.0
           r465=0.0
           r469=0.0
           r474=0.0
           r476=0.0
           r478=0.0
           r480=0.0
           r482=0.0
           r484=0.0
           r486=0.0
           r488=0.0
           r1435=0
           r1439=0.0
           r1970=0.0
           r2501=0.0
           r3032=0.0
           r3563=0.0
           r4094=0.0
           r4625=0.0
           r5156=0.0
           r5687=0.0
           r6218=0.0
           r6749=0.0
           r7280=0.0
           r7811=0.0
           ! 528is allocatable; cannot be written to yet
           ! 530is allocatable; cannot be written to yet
           r143=0.0
           r1966=0
           r2497=0
           r41=0.0
           r514=64
           r513=32
           r526=36
           r527=36
           r60='/home/armstroc/genie_output/genie_ig_go_sl_dyex_test/main'
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r59=720
           r57=3600.0
           r107=.false.
           r96=86400
           r108=0.0
           r109=1.25
           r110=0.0013
           r111=0.0
           r112=0.0
           r113=0
           r114=0
           r115=0.0
           r116=5000.0
           r117=2*7.2921e-5
           r118=1000.0
           r127=0
           r120=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call start_oasisvis_log('bfg_averages ini_averages ',3)
       call bfg_averages_ini_averages_init(r1,r2,r3)
       call end_oasisvis_log('bfg_averages ini_averages ',3)
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call start_oasisvis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ',3)
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r515,r76,r77,r78)
       call end_oasisvis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ',3)
       call put_initialise_fixedicesheet_mod(r515,-515)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call start_oasisvis_log('fixed_chemistry initialise_fixedchem ',3)
       call initialise_fixedchem(r61,r62,r63)
       call end_oasisvis_log('fixed_chemistry initialise_fixedchem ',3)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (goldsteinThread()) then
       call setActiveModel(7)
       call start_oasisvis_log('goldstein initialise_goldstein ',3)
       call initialise_goldstein(r79,r80,r81,r82,r83,r84,r85,r86,r87,r88,r89,r90,r91,r92,r93,r94,r95,r96,r97,r98,r99,r100,r101,r102,r103,r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r120,r121,r122,r123,r124,r125,r126,r127,r128,r129,r130,r131,r132,r133)
       call end_oasisvis_log('goldstein initialise_goldstein ',3)
       call put_goldstein(r97,-97)
       call put_goldstein(r101,-101)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(41)
       call start_oasisvis_log('counter counter 3',3)
       call counter(r1435)
       call end_oasisvis_log('counter counter 3',3)
       end if
       end if
       if (goldsteinThread()) then
       if(mod(its1,goldstein__freq).eq.0)then
       call setActiveModel(42)
       call get_goldstein(r2501,-2501)
       call get_goldstein(r4094,-4094)
       call get_goldstein(r3032,-3032)
       call get_goldstein(r3563,-3563)
       call get_goldstein(r1970,-1970)
       call get_goldstein(r7280,-7280)
       call get_goldstein(r6749,-6749)
       call get_goldstein(r7811,-7811)
       call get_goldstein(r4625,-4625)
       call get_goldstein(r5687,-5687)
       call get_goldstein(r5156,-5156)
       call get_goldstein(r6218,-6218)
       call start_oasisvis_log('goldstein goldstein ',3)
       call goldstein(r1435,r2501,r4094,r3032,r3563,r1970,r7280,r6749,r7811,r143,r4625,r5687,r5156,r6218,r97,r98,r99,r100,r101,r153,r154,r155,r132,r133,r158,r159,r160)
       call end_oasisvis_log('goldstein goldstein ',3)
       call put_goldstein(r97,-97)
       call put_goldstein(r101,-101)
       end if
       end if
       if (copy_dummy_goldsteinThread()) then
       if(mod(its1,copy_dummy_goldstein__freq).eq.0)then
       call setActiveModel(43)
       if (.not.(allocated(r528))) then
       allocate(r528(1:r526,1:r527))
       r528=0.0
       end if
       if (.not.(allocated(r530))) then
       allocate(r530(1:r526,1:r527))
       r530=0.0
       end if
       call start_oasisvis_log('copy_dummy_goldstein copy_dummy ',3)
       call copy_dummy(r526,r527,r528,r97,r530,r101)
       call end_oasisvis_log('copy_dummy_goldstein copy_dummy ',3)
       call put_copy_dummy_goldstein(r528,-528)
       call put_copy_dummy_goldstein(r97,-97)
       call put_copy_dummy_goldstein(r530,-530)
       call put_copy_dummy_goldstein(r101,-101)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(48)
       call start_oasisvis_log('counter counter 4',3)
       call counter(r1966)
       call end_oasisvis_log('counter counter 4',3)
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(49)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r246,-246)
       call start_oasisvis_log('fixed_chemistry fixedchem ',3)
       call fixedchem(r1966,r61,r62,r63,r246)
       call end_oasisvis_log('fixed_chemistry fixedchem ',3)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r246,-246)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(50)
       call start_oasisvis_log('counter counter 5',3)
       call counter(r2497)
       call end_oasisvis_log('counter counter 5',3)
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(51)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r222,-222)
       call start_oasisvis_log('fixed_icesheet fixedicesheet ',3)
       call fixedicesheet(r2497,r515,r76,r77,r78,r222)
       call end_oasisvis_log('fixed_icesheet fixedicesheet ',3)
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r222,-222)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(52)
       call get_bfg_averages(r163,-163)
       call get_bfg_averages(r164,-164)
       call get_bfg_averages(r476,-476)
       call get_bfg_averages(r3032,-3032)
       call get_bfg_averages(r478,-478)
       call get_bfg_averages(r3563,-3563)
       call get_bfg_averages(r474,-474)
       call get_bfg_averages(r4094,-4094)
       call get_bfg_averages(r469,-469)
       call get_bfg_averages(r2501,-2501)
       call get_bfg_averages(r480,-480)
       call get_bfg_averages(r4625,-4625)
       call get_bfg_averages(r482,-482)
       call get_bfg_averages(r5687,-5687)
       call get_bfg_averages(r465,-465)
       call get_bfg_averages(r1970,-1970)
       call get_bfg_averages(r486,-486)
       call get_bfg_averages(r7280,-7280)
       call get_bfg_averages(r484,-484)
       call get_bfg_averages(r6749,-6749)
       call get_bfg_averages(r488,-488)
       call get_bfg_averages(r7811,-7811)
       call get_bfg_averages(r461,-461)
       call get_bfg_averages(r1439,-1439)
       call get_bfg_averages(r517,-517)
       call get_bfg_averages(r524,-524)
       call start_oasisvis_log('bfg_averages write_averages ',3)
       call bfg_averages_write_averages_iteration(r1435,r163,r164,r79,r80,r9,r10,r476,r3032,r13,r478,r3563,r16,r474,r4094,r19,r469,r2501,r22,r480,r4625,r25,r482,r5687,r28,r465,r1970,r31,r486,r7280,r34,r484,r6749,r37,r488,r7811,r40,r41,r143,r43,r461,r1439,r46,r517,r97,r49,r524,r101,r52,r1,r2,r3,r458,r57,r58,r59,r60)
       call end_oasisvis_log('bfg_averages write_averages ',3)
       call put_bfg_averages(r163,-163)
       call put_bfg_averages(r164,-164)
       call put_bfg_averages(r476,-476)
       call put_bfg_averages(r3032,-3032)
       call put_bfg_averages(r478,-478)
       call put_bfg_averages(r3563,-3563)
       call put_bfg_averages(r474,-474)
       call put_bfg_averages(r4094,-4094)
       call put_bfg_averages(r469,-469)
       call put_bfg_averages(r2501,-2501)
       call put_bfg_averages(r480,-480)
       call put_bfg_averages(r4625,-4625)
       call put_bfg_averages(r482,-482)
       call put_bfg_averages(r5687,-5687)
       call put_bfg_averages(r465,-465)
       call put_bfg_averages(r1970,-1970)
       call put_bfg_averages(r486,-486)
       call put_bfg_averages(r7280,-7280)
       call put_bfg_averages(r484,-484)
       call put_bfg_averages(r6749,-6749)
       call put_bfg_averages(r488,-488)
       call put_bfg_averages(r7811,-7811)
       call put_bfg_averages(r461,-461)
       call put_bfg_averages(r1439,-1439)
       call put_bfg_averages(r517,-517)
       call put_bfg_averages(r97,-97)
       call put_bfg_averages(r524,-524)
       call put_bfg_averages(r101,-101)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(54)
       call get_transformer7(r469,-469)
       call get_transformer7(r474,-474)
       call get_transformer7(r476,-476)
       call get_transformer7(r478,-478)
       call get_transformer7(r480,-480)
       call get_transformer7(r482,-482)
       call get_transformer7(r484,-484)
       call get_transformer7(r486,-486)
       call get_transformer7(r488,-488)
       call get_transformer7(r461,-461)
       call get_transformer7(r465,-465)
       call start_oasisvis_log('transformer7 new_transformer_7 ',3)
       call new_transformer_7(r514,r513,r469,r474,r476,r478,r480,r482,r484,r486,r488,r461,r465,r41)
       call end_oasisvis_log('transformer7 new_transformer_7 ',3)
       call put_transformer7(r469,-469)
       call put_transformer7(r474,-474)
       call put_transformer7(r476,-476)
       call put_transformer7(r478,-478)
       call put_transformer7(r480,-480)
       call put_transformer7(r482,-482)
       call put_transformer7(r484,-484)
       call put_transformer7(r486,-486)
       call put_transformer7(r488,-488)
       call put_transformer7(r461,-461)
       call put_transformer7(r465,-465)
       end if
       end if
       end do
       if (goldsteinThread()) then
       call setActiveModel(56)
       call start_oasisvis_log('goldstein end_goldstein ',3)
       call end_goldstein()
       call end_oasisvis_log('goldstein end_goldstein ',3)
       end if
       call finaliseComms()
       end program BFG2Main

