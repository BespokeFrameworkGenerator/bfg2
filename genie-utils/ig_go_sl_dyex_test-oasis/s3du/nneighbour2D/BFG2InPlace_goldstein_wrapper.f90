       ! f77 to f90 put/get wrappers start
       subroutine put_goldstein(data,tag)
       use BFG2Target3
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==7) then
       if (tag==-97) then
       call putreal__2_goldstein(data,tag)
       end if
       if (tag==-101) then
       call putreal__2_goldstein(data,tag)
       end if
       end if
       if (currentModel==42) then
       if (tag==-97) then
       call putreal__2_goldstein(data,tag)
       end if
       if (tag==-101) then
       call putreal__2_goldstein(data,tag)
       end if
       end if
       end subroutine put_goldstein
       subroutine get_goldstein(data,tag)
       use BFG2Target3
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==42) then
       if (tag==-2501) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-4094) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-3032) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-3563) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-1970) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-7280) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-6749) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-7811) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-4625) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-5687) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-5156) then
       call getreal__2_goldstein(data,tag)
       end if
       if (tag==-6218) then
       call getreal__2_goldstein(data,tag)
       end if
       end if
       end subroutine get_goldstein
       subroutine getreal__2_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_goldstein
       subroutine putreal__2_goldstein(data,tag)
       use BFG2InPlace_goldstein, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_goldstein
       ! f77 to f90 put/get wrappers end
