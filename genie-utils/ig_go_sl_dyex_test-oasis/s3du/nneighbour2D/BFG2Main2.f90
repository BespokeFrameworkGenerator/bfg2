       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_nneighbour2D_3_freq
       integer :: interp_nneighbour2D_3_freq
       !interp_nneighbour2D_4_freq
       integer :: interp_nneighbour2D_4_freq
       !interp_nneighbour2D_5_freq
       integer :: interp_nneighbour2D_5_freq
       !interp_nneighbour2D_6_freq
       integer :: interp_nneighbour2D_6_freq
       !interp_nneighbour2D_7_freq
       integer :: interp_nneighbour2D_7_freq
       !interp_nneighbour2D_8_freq
       integer :: interp_nneighbour2D_8_freq
       !interp_nneighbour2D_9_freq
       integer :: interp_nneighbour2D_9_freq
       !interp_nneighbour2D_10_freq
       integer :: interp_nneighbour2D_10_freq
       !interp_nneighbour2D_11_freq
       integer :: interp_nneighbour2D_11_freq
       !interp_nneighbour2D_12_freq
       integer :: interp_nneighbour2D_12_freq
       !interp_nneighbour2D_13_freq
       integer :: interp_nneighbour2D_13_freq
       !interp_nneighbour2D_14_freq
       integer :: interp_nneighbour2D_14_freq
       !interp_nneighbour2D_15_freq
       integer :: interp_nneighbour2D_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy_goldstein__freq
       integer :: copy_dummy_goldstein__freq
       !interp_nneighbour2D_16_freq
       integer :: interp_nneighbour2D_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_nneighbour2D_17_freq
       integer :: interp_nneighbour2D_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,counter_2_freq,interp_nneighbour2D_3_freq,interp_nneighbour2D_4_freq,interp_nneighbour2D_5_freq,interp_nneighbour2D_6_freq,interp_nneighbour2D_7_freq,interp_nneighbour2D_8_freq,interp_nneighbour2D_9_freq,interp_nneighbour2D_10_freq,interp_nneighbour2D_11_freq,interp_nneighbour2D_12_freq,interp_nneighbour2D_13_freq,interp_nneighbour2D_14_freq,interp_nneighbour2D_15_freq,counter_3_freq,goldstein__freq,copy_dummy_goldstein__freq,interp_nneighbour2D_16_freq,copy_tstar_2_freq,interp_nneighbour2D_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r515
       !tstar_atm
       real, dimension(1:64,1:32) :: r517
       !albedo_atm
       real, dimension(1:64,1:32) :: r524
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r516
       !conductflux_atm
       real, dimension(1:64,1:32) :: r348
       !test_energy_seaice
       real :: r350
       !test_water_seaice
       real :: r351
       !ksic_loop
       integer :: r352
       !dummy_atm
       real, dimension(1:64,1:32) :: r518
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r400
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r404
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r442
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r446
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r448
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r450
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r452
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r454
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r456
       !kocn_loop
       integer :: r458
       !istep_sic
       integer :: r904
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r461
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r465
       !ilon1_atm
       integer :: r514
       !ilat1_atm
       integer :: r513
       !temptop_atm
       real, dimension(1:64,1:32) :: r519
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r515=0
           r517=0.0
           r524=0.0
           r179=0.0
           r180=0.0
           r516=0.0
           r348=0.0
           r350=0.0
           r351=0.0
           r352=6
           r518=0.0
           r400=0.0
           r404=0.0
           r442=0.0
           r446=0.0
           r448=0.0
           r450=0.0
           r452=0.0
           r454=0.0
           r456=0.0
           r458=48
           r904=0
           r461=0.0
           r465=0.0
           r514=64
           r513=32
           r519=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r517,-517)
       call get_slab_seaice(r524,-524)
       call get_slab_seaice(r516,-516)
       call get_slab_seaice(r515,-515)
       call start_oasisvis_log('slab_seaice initialise_slabseaice ',2)
       call initialise_slabseaice(r517,r524,r516,r348,r515,r350,r351,r352)
       call end_oasisvis_log('slab_seaice initialise_slabseaice ',2)
       call put_slab_seaice(r517,-517)
       call put_slab_seaice(r524,-524)
       call put_slab_seaice(r516,-516)
       end if
       if (copy_tstarinst1Thread()) then
       call setActiveModel(9)
       call get_copy_tstar(r515,-515)
       call get_copy_tstar(r516,-516)
       call get_copy_tstar(r517,-517)
       call get_copy_tstar(r518,-518)
       call start_oasisvis_log('copy_tstar copytstar 1',2)
       call copytstar(r513,r514,r515,r516,r517,r518,r519)
       call end_oasisvis_log('copy_tstar copytstar 1',2)
       call put_copy_tstar(r517,-517)
       end if
       if (copy_albedoinst1Thread()) then
       call setActiveModel(11)
       call get_copy_albedo(r515,-515)
       call get_copy_albedo(r516,-516)
       call get_copy_albedo(r524,-524)
       call get_copy_albedo(r518,-518)
       call start_oasisvis_log('copy_albedo copyalbedo 1',2)
       call copyalbedo(r513,r514,r515,r516,r524,r518)
       call end_oasisvis_log('copy_albedo copyalbedo 1',2)
       call put_copy_albedo(r524,-524)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(24)
       call start_oasisvis_log('counter counter 2',2)
       call counter(r904)
       call end_oasisvis_log('counter counter 2',2)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(25)
       call get_slab_seaice(r517,-517)
       call get_slab_seaice(r442,-442)
       call get_slab_seaice(r446,-446)
       call get_slab_seaice(r448,-448)
       call get_slab_seaice(r450,-450)
       call get_slab_seaice(r400,-400)
       call get_slab_seaice(r404,-404)
       call get_slab_seaice(r179,-179)
       call get_slab_seaice(r180,-180)
       call get_slab_seaice(r452,-452)
       call get_slab_seaice(r454,-454)
       call get_slab_seaice(r516,-516)
       call get_slab_seaice(r524,-524)
       call get_slab_seaice(r515,-515)
       call start_oasisvis_log('slab_seaice slabseaice ',2)
       call slabseaice(r904,r517,r442,r446,r448,r450,r400,r404,r179,r180,r452,r454,r516,r519,r348,r524,r515,r350,r351,r352)
       call end_oasisvis_log('slab_seaice slabseaice ',2)
       call put_slab_seaice(r517,-517)
       call put_slab_seaice(r442,-442)
       call put_slab_seaice(r446,-446)
       call put_slab_seaice(r448,-448)
       call put_slab_seaice(r450,-450)
       call put_slab_seaice(r400,-400)
       call put_slab_seaice(r404,-404)
       call put_slab_seaice(r179,-179)
       call put_slab_seaice(r180,-180)
       call put_slab_seaice(r452,-452)
       call put_slab_seaice(r454,-454)
       call put_slab_seaice(r516,-516)
       call put_slab_seaice(r524,-524)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(26)
       call get_transformer4(r461,-461)
       call get_transformer4(r516,-516)
       call get_transformer4(r465,-465)
       call start_oasisvis_log('transformer4 new_transformer_4 ',2)
       call new_transformer_4(r514,r513,r461,r516,r352,r458,r465,r348)
       call end_oasisvis_log('transformer4 new_transformer_4 ',2)
       call put_transformer4(r461,-461)
       call put_transformer4(r516,-516)
       call put_transformer4(r465,-465)
       end if
       end if
       if (copy_tstarinst2Thread()) then
       if(mod(its1,copy_tstar_2_freq).eq.0)then
       call setActiveModel(45)
       call get_copy_tstar(r515,-515)
       call get_copy_tstar(r516,-516)
       call get_copy_tstar(r517,-517)
       call get_copy_tstar(r518,-518)
       call start_oasisvis_log('copy_tstar copytstar 2',2)
       call copytstar(r513,r514,r515,r516,r517,r518,r519)
       call end_oasisvis_log('copy_tstar copytstar 2',2)
       call put_copy_tstar(r517,-517)
       end if
       end if
       if (copy_albedoinst2Thread()) then
       if(mod(its1,copy_albedo_2_freq).eq.0)then
       call setActiveModel(47)
       call get_copy_albedo(r515,-515)
       call get_copy_albedo(r516,-516)
       call get_copy_albedo(r524,-524)
       call get_copy_albedo(r518,-518)
       call start_oasisvis_log('copy_albedo copyalbedo 2',2)
       call copyalbedo(r513,r514,r515,r516,r524,r518)
       call end_oasisvis_log('copy_albedo copyalbedo 2',2)
       call put_copy_albedo(r524,-524)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(53)
       call get_transformer6(r442,-442)
       call get_transformer6(r446,-446)
       call get_transformer6(r448,-448)
       call get_transformer6(r450,-450)
       call get_transformer6(r452,-452)
       call get_transformer6(r454,-454)
       call get_transformer6(r456,-456)
       call start_oasisvis_log('transformer6 new_transformer_6 ',2)
       call new_transformer_6(r514,r513,r442,r446,r448,r450,r452,r454,r456)
       call end_oasisvis_log('transformer6 new_transformer_6 ',2)
       call put_transformer6(r442,-442)
       call put_transformer6(r446,-446)
       call put_transformer6(r448,-448)
       call put_transformer6(r450,-450)
       call put_transformer6(r452,-452)
       call put_transformer6(r454,-454)
       call put_transformer6(r456,-456)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main

