       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_nneighbour2D_3_freq
       integer :: interp_nneighbour2D_3_freq
       !interp_nneighbour2D_4_freq
       integer :: interp_nneighbour2D_4_freq
       !interp_nneighbour2D_5_freq
       integer :: interp_nneighbour2D_5_freq
       !interp_nneighbour2D_6_freq
       integer :: interp_nneighbour2D_6_freq
       !interp_nneighbour2D_7_freq
       integer :: interp_nneighbour2D_7_freq
       !interp_nneighbour2D_8_freq
       integer :: interp_nneighbour2D_8_freq
       !interp_nneighbour2D_9_freq
       integer :: interp_nneighbour2D_9_freq
       !interp_nneighbour2D_10_freq
       integer :: interp_nneighbour2D_10_freq
       !interp_nneighbour2D_11_freq
       integer :: interp_nneighbour2D_11_freq
       !interp_nneighbour2D_12_freq
       integer :: interp_nneighbour2D_12_freq
       !interp_nneighbour2D_13_freq
       integer :: interp_nneighbour2D_13_freq
       !interp_nneighbour2D_14_freq
       integer :: interp_nneighbour2D_14_freq
       !interp_nneighbour2D_15_freq
       integer :: interp_nneighbour2D_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy_goldstein__freq
       integer :: copy_dummy_goldstein__freq
       !interp_nneighbour2D_16_freq
       integer :: interp_nneighbour2D_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_nneighbour2D_17_freq
       integer :: interp_nneighbour2D_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,counter_2_freq,interp_nneighbour2D_3_freq,interp_nneighbour2D_4_freq,interp_nneighbour2D_5_freq,interp_nneighbour2D_6_freq,interp_nneighbour2D_7_freq,interp_nneighbour2D_8_freq,interp_nneighbour2D_9_freq,interp_nneighbour2D_10_freq,interp_nneighbour2D_11_freq,interp_nneighbour2D_12_freq,interp_nneighbour2D_13_freq,interp_nneighbour2D_14_freq,interp_nneighbour2D_15_freq,counter_3_freq,goldstein__freq,copy_dummy_goldstein__freq,interp_nneighbour2D_16_freq,copy_tstar_2_freq,interp_nneighbour2D_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon2_atm
       real, dimension(1:64) :: r165
       !alat2_atm
       real, dimension(1:32) :: r166
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r168
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r170
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r172
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r190
       !hrlons_atm
       real, dimension(1:320) :: r203
       !hrlats_atm
       real, dimension(1:160) :: r204
       !hrlonsedge_atm
       real, dimension(1:321) :: r205
       !hrlatsedge_atm
       real, dimension(1:161) :: r206
       !u10m_atm
       real, dimension(1:64,1:32) :: r213
       !v10m_atm
       real, dimension(1:64,1:32) :: r214
       !q2m_atm
       real, dimension(1:64,1:32) :: r216
       !rh2m_atm
       real, dimension(1:64,1:32) :: r217
       !psigma
       real, dimension(1:7) :: r221
       !massair
       real, dimension(1:64,1:32,1:7) :: r224
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r249
       !test_water_land
       real :: r293
       !glim_snow_model
       logical :: r300
       !glim_coupled
       logical :: r301
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r215
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r515
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !alon1_atm
       real, dimension(1:64) :: r163
       !alat1_atm
       real, dimension(1:32) :: r164
       !aboxedge1_lon_atm
       real, dimension(1:65) :: r167
       !aboxedge1_lat_atm
       real, dimension(1:33) :: r169
       !land_niter_tim
       integer :: r173
       !ocean_niter_tim
       integer :: r174
       !atmos_dt_tim
       real :: r175
       !tstar_atm
       real, dimension(1:64,1:32) :: r517
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r177
       !albedo_atm
       real, dimension(1:64,1:32) :: r524
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !precip_atm
       real, dimension(1:64,1:32) :: r181
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r187
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r188
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r191
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r194
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r195
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r196
       !water_flux_atmos
       real :: r201
       !glim_covmap
       real, dimension(1:64,1:32) :: r202
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r516
       !ksic_loop
       integer :: r352
       !istep_atm
       integer :: r373
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r207
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r208
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r209
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r210
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r211
       !surfsigma
       real :: r219
       !surfdsigma
       real :: r220
       !iconv_ice
       integer :: r222
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r223
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r388
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r389
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r390
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r391
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r392
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r393
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r394
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r395
       !surf_iter_tim_bfg
       integer :: r374
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r279
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r280
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r281
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r282
       !land_latent_atm
       real, dimension(1:64,1:32) :: r283
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r284
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r285
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r286
       !land_evap_atm
       real, dimension(1:64,1:32) :: r287
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r288
       !land_rough_atm
       real, dimension(1:64,1:32) :: r289
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r290
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r332
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r333
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r334
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r335
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r336
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r337
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r338
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r339
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r340
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r341
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r342
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r343
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r344
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r400
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r404
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r407
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r410
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r413
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r416
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r419
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r422
       !iconv_che
       integer :: r246
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r442
       !katm_loop
       integer :: r444
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r446
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r448
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r450
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r452
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r454
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r456
       !kocn_loop
       integer :: r458
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r469
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r474
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r476
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r478
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r480
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r482
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r484
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r486
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r488
       !ilon1_atm
       integer :: r514
       !ilat1_atm
       integer :: r513
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r397
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r396
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r278
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r331
       !flag_goldsteinocean
       logical :: r183
       !flag_goldsteinseaice
       logical :: r184
       !flag_land
       logical :: r185
       !klnd_loop
       integer :: r186
       !lgraphic
       logical :: r182
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r189
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r515=0
           r76=0.0
           r77=0.0
           r78=0.0
           r61=0.0
           r62=0.0
           r63=0.0
           r163=0.0
           r164=0.0
           r167=0.0
           r169=0.0
           r173=0
           r174=0
           r175=0.0
           r517=0.0
           ! 177is allocatable; cannot be written to yet
           r524=0.0
           r179=0.0
           r180=0.0
           r181=0.0
           ! 187is allocatable; cannot be written to yet
           r188=0.0
           ! 191is allocatable; cannot be written to yet
           r194=0.0
           r195=0.0
           r196=0.0
           r201=0.0
           r202=0.0
           r516=0.0
           r352=6
           r373=0
           r207=0.0
           r208=0.0
           r209=0.0
           r210=0.0
           r211=0.0
           r219=0.0
           r220=0.0
           r222=0.0
           r223=0.0
           ! 388is allocatable; cannot be written to yet
           ! 389is allocatable; cannot be written to yet
           ! 390is allocatable; cannot be written to yet
           ! 391is allocatable; cannot be written to yet
           ! 392is allocatable; cannot be written to yet
           ! 393is allocatable; cannot be written to yet
           ! 394is allocatable; cannot be written to yet
           ! 395is allocatable; cannot be written to yet
           r374=0
           r279=0.0
           r280=0.0
           r281=0.0
           r282=0.0
           r283=0.0
           r284=0.0
           r285=0.0
           r286=0.0
           r287=0.0
           r288=0.0
           r289=0.0
           r290=0.0
           r332=0.0
           r333=0.0
           r334=0.0
           r335=0.0
           r336=0.0
           r337=0.0
           r338=0.0
           r339=0.0
           r340=0.0
           r341=0.0
           r342=0.0
           r343=0.0
           r344=0.0
           r400=0.0
           r404=0.0
           r407=0.0
           r410=0.0
           r413=0.0
           ! 416is allocatable; cannot be written to yet
           ! 419is allocatable; cannot be written to yet
           ! 422is allocatable; cannot be written to yet
           r246=0
           r442=0.0
           r444=1
           r446=0.0
           r448=0.0
           r450=0.0
           r452=0.0
           r454=0.0
           r456=0.0
           r458=48
           r469=0.0
           r474=0.0
           r476=0.0
           r478=0.0
           r480=0.0
           r482=0.0
           r484=0.0
           r486=0.0
           r488=0.0
           r514=64
           r513=32
           ! 397is allocatable; cannot be written to yet
           ! 396is allocatable; cannot be written to yet
           r278=0.0
           r331=0.0
           r183=.true.
           r184=.false.
           r185=.false.
           r186=1
           r182=.false.
           ! 189is allocatable; cannot be written to yet
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call start_oasisvis_log('igcm_atmosphere initialise_igcmsurf ',1)
       call initialise_igcmsurf()
       call end_oasisvis_log('igcm_atmosphere initialise_igcmsurf ',1)
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r163,-163)
       call get_igcm_atmosphere(r164,-164)
       call get_igcm_atmosphere(r515,-515)
       call get_igcm_atmosphere(r517,-517)
       call get_igcm_atmosphere(r524,-524)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r190))) then
       allocate(r190(1:r514,1:r513))
       end if
       if (.not.(allocated(r177))) then
       allocate(r177(1:r514,1:r513))
       r177=0.0
       end if
       if (.not.(allocated(r187))) then
       allocate(r187(1:r514,1:r513))
       r187=0.0
       end if
       if (.not.(allocated(r191))) then
       allocate(r191(1:r514,1:r513))
       r191=0.0
       end if
       if (.not.(allocated(r189))) then
       allocate(r189(1:r514,1:r513))
       r189=0.0
       end if
       call start_oasisvis_log('igcm_atmosphere initialise_atmos ',1)
       call initialise_atmos(r514,r513,r163,r164,r165,r166,r167,r168,r169,r170,r515,r172,r173,r174,r175,r517,r177,r524,r179,r180,r181,r182,r183,r184,r185,r186,r187,r188,r189,r190,r191,r76,r78,r194,r195,r196,r77,r61,r62,r63,r201,r202,r203,r204,r205,r206)
       call end_oasisvis_log('igcm_atmosphere initialise_atmos ',1)
       call put_igcm_atmosphere(r163,-163)
       call put_igcm_atmosphere(r164,-164)
       call put_igcm_atmosphere(r517,-517)
       call put_igcm_atmosphere(r524,-524)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(12)
       call start_oasisvis_log('counter counter 1',1)
       call counter(r373)
       call end_oasisvis_log('counter counter 1',1)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(13)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r222,-222)
       call start_oasisvis_log('igcm_atmosphere igcm3_adiab ',1)
       call igcm3_adiab(r207,r208,r209,r210,r211,r187,r213,r214,r215,r216,r217,r76,r219,r220,r221,r222,r223,r224)
       call end_oasisvis_log('igcm_atmosphere igcm3_adiab ',1)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r222,-222)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(14)
       if (.not.(allocated(r388))) then
       allocate(r388(1:r514,1:r513))
       r388=0.0
       end if
       if (.not.(allocated(r389))) then
       allocate(r389(1:r514,1:r513))
       r389=0.0
       end if
       if (.not.(allocated(r390))) then
       allocate(r390(1:r514,1:r513))
       r390=0.0
       end if
       if (.not.(allocated(r391))) then
       allocate(r391(1:r514,1:r513))
       r391=0.0
       end if
       if (.not.(allocated(r392))) then
       allocate(r392(1:r514,1:r513))
       r392=0.0
       end if
       if (.not.(allocated(r393))) then
       allocate(r393(1:r514,1:r513))
       r393=0.0
       end if
       if (.not.(allocated(r394))) then
       allocate(r394(1:r514,1:r513))
       r394=0.0
       end if
       if (.not.(allocated(r395))) then
       allocate(r395(1:r514,1:r513))
       r395=0.0
       end if
       if (.not.(allocated(r397))) then
       allocate(r397(1:r514,1:r513))
       r397=0.0
       end if
       if (.not.(allocated(r396))) then
       allocate(r396(1:r514,1:r513))
       r396=0.0
       end if
       call start_oasisvis_log('transformer1 new_transformer_1 ',1)
       call new_transformer_1(r514,r513,r207,r208,r209,r210,r187,r211,r388,r389,r390,r391,r392,r393,r394,r395,r396,r397)
       call end_oasisvis_log('transformer1 new_transformer_1 ',1)
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(15)
       call start_oasisvis_log('counter_mod counter_mod 1',1)
       call counter_mod(r374,r173)
       call end_oasisvis_log('counter_mod counter_mod 1',1)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(16)
       call get_igcm_atmosphere(r515,-515)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call start_oasisvis_log('igcm_atmosphere igcm_land_surflux ',1)
       call igcm_land_surflux(r373,r374,r173,r175,r219,r515,r179,r180,r181,r388,r389,r390,r391,r211,r278,r279,r280,r281,r282,r283,r284,r285,r286,r287,r288,r289,r290,r191,r188,r293,r78,r194,r195,r196,r77,r202,r300,r301)
       call end_oasisvis_log('igcm_atmosphere igcm_land_surflux ',1)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(17)
       call get_igcm_atmosphere(r515,-515)
       call start_oasisvis_log('igcm_atmosphere igcm_land_blayer ',1)
       call igcm_land_blayer(r374,r173,r175,r220,r515,r279,r280,r281,r282,r388,r389,r390,r391,r211)
       call end_oasisvis_log('igcm_atmosphere igcm_land_blayer ',1)
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(18)
       call start_oasisvis_log('counter_mod counter_mod 2',1)
       call counter_mod(r374,r174)
       call end_oasisvis_log('counter_mod counter_mod 2',1)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(19)
       call get_igcm_atmosphere(r515,-515)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r524,-524)
       call get_igcm_atmosphere(r517,-517)
       call start_oasisvis_log('igcm_atmosphere igcm_ocean_surflux ',1)
       call igcm_ocean_surflux(r373,r374,r174,r219,r515,r179,r180,r181,r392,r393,r394,r395,r211,r524,r517,r331,r332,r333,r334,r335,r336,r337,r338,r339,r340,r341,r342,r343,r344)
       call end_oasisvis_log('igcm_atmosphere igcm_ocean_surflux ',1)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r524,-524)
       call put_igcm_atmosphere(r517,-517)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(20)
       call get_igcm_atmosphere(r515,-515)
       call start_oasisvis_log('igcm_atmosphere igcm_ocean_blayer ',1)
       call igcm_ocean_blayer(r374,r174,r175,r220,r515,r332,r333,r334,r335,r392,r393,r394,r395,r211)
       call end_oasisvis_log('igcm_atmosphere igcm_ocean_blayer ',1)
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(21)
       call get_transformer2(r400,-400)
       call get_transformer2(r515,-515)
       call get_transformer2(r404,-404)
       if (.not.(allocated(r416))) then
       allocate(r416(1:r514,1:r513))
       r416=0.0
       end if
       if (.not.(allocated(r419))) then
       allocate(r419(1:r514,1:r513))
       r419=0.0
       end if
       if (.not.(allocated(r422))) then
       allocate(r422(1:r514,1:r513))
       r422=0.0
       end if
       call start_oasisvis_log('transformer2 new_transformer_2 ',1)
       call new_transformer_2(r514,r513,r400,r515,r283,r336,r404,r284,r337,r407,r285,r338,r410,r286,r339,r413,r287,r340,r416,r288,r341,r419,r289,r342,r422,r290,r343,r177,r191,r344,r207,r388,r392,r208,r389,r393,r209,r390,r394,r210,r391,r395)
       call end_oasisvis_log('transformer2 new_transformer_2 ',1)
       call put_transformer2(r400,-400)
       call put_transformer2(r404,-404)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(22)
       call get_igcm_atmosphere(r400,-400)
       call get_igcm_atmosphere(r404,-404)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r246,-246)
       call start_oasisvis_log('igcm_atmosphere igcm3_diab ',1)
       call igcm3_diab(r207,r208,r209,r210,r177,r422,r416,r419,r400,r404,r407,r410,r179,r180,r181,r196,r215,r287,r61,r62,r63,r246,r201,r223,r249)
       call end_oasisvis_log('igcm_atmosphere igcm3_diab ',1)
       call put_igcm_atmosphere(r400,-400)
       call put_igcm_atmosphere(r404,-404)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r246,-246)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer3(r442,-442)
       call get_transformer3(r400,-400)
       call get_transformer3(r446,-446)
       call get_transformer3(r404,-404)
       call get_transformer3(r448,-448)
       call get_transformer3(r179,-179)
       call get_transformer3(r450,-450)
       call get_transformer3(r180,-180)
       call get_transformer3(r452,-452)
       call get_transformer3(r454,-454)
       call get_transformer3(r456,-456)
       call start_oasisvis_log('transformer3 new_transformer_3 ',1)
       call new_transformer_3(r514,r513,r442,r400,r444,r352,r446,r404,r448,r179,r450,r180,r452,r407,r454,r410,r456,r181,r458)
       call end_oasisvis_log('transformer3 new_transformer_3 ',1)
       call put_transformer3(r442,-442)
       call put_transformer3(r400,-400)
       call put_transformer3(r446,-446)
       call put_transformer3(r404,-404)
       call put_transformer3(r448,-448)
       call put_transformer3(r179,-179)
       call put_transformer3(r450,-450)
       call put_transformer3(r180,-180)
       call put_transformer3(r452,-452)
       call put_transformer3(r454,-454)
       call put_transformer3(r456,-456)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(27)
       call get_transformer5(r469,-469)
       call get_transformer5(r400,-400)
       call get_transformer5(r516,-516)
       call get_transformer5(r474,-474)
       call get_transformer5(r404,-404)
       call get_transformer5(r476,-476)
       call get_transformer5(r179,-179)
       call get_transformer5(r478,-478)
       call get_transformer5(r180,-180)
       call get_transformer5(r480,-480)
       call get_transformer5(r482,-482)
       call get_transformer5(r484,-484)
       call get_transformer5(r486,-486)
       call get_transformer5(r488,-488)
       call start_oasisvis_log('transformer5 new_transformer_5 ',1)
       call new_transformer_5(r514,r513,r469,r400,r516,r444,r458,r474,r404,r476,r179,r478,r180,r480,r407,r482,r410,r484,r181,r486,r413,r488,r188)
       call end_oasisvis_log('transformer5 new_transformer_5 ',1)
       call put_transformer5(r469,-469)
       call put_transformer5(r400,-400)
       call put_transformer5(r516,-516)
       call put_transformer5(r474,-474)
       call put_transformer5(r404,-404)
       call put_transformer5(r476,-476)
       call put_transformer5(r179,-179)
       call put_transformer5(r478,-478)
       call put_transformer5(r180,-180)
       call put_transformer5(r480,-480)
       call put_transformer5(r482,-482)
       call put_transformer5(r484,-484)
       call put_transformer5(r486,-486)
       call put_transformer5(r488,-488)
       end if
       end if
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(55)
       call start_oasisvis_log('igcm_atmosphere end_atmos ',1)
       call end_atmos()
       call end_oasisvis_log('igcm_atmosphere end_atmos ',1)
       end if
       call finaliseComms()
       end program BFG2Main

