       ! f77 to f90 put/get wrappers start
       subroutine put_copy_tstar(data,tag)
       use BFG2Target2
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==9) then
       if (tag==-517) then
       call putreal__2_copy_tstar(data,tag)
       end if
       end if
       if (currentModel==45) then
       if (tag==-517) then
       call putreal__2_copy_tstar(data,tag)
       end if
       end if
       end subroutine put_copy_tstar
       subroutine get_copy_tstar(data,tag)
       use BFG2Target2
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==9) then
       if (tag==-515) then
       call getinteger__2_copy_tstar(data,tag)
       end if
       end if
       if (currentModel==45) then
       if (tag==-515) then
       call getinteger__2_copy_tstar(data,tag)
       end if
       end if
       if (currentModel==9) then
       if (tag==-516) then
       call getreal__2_copy_tstar(data,tag)
       end if
       if (tag==-517) then
       call getreal__2_copy_tstar(data,tag)
       end if
       if (tag==-518) then
       call getreal__2_copy_tstar(data,tag)
       end if
       end if
       if (currentModel==45) then
       if (tag==-516) then
       call getreal__2_copy_tstar(data,tag)
       end if
       if (tag==-517) then
       call getreal__2_copy_tstar(data,tag)
       end if
       if (tag==-518) then
       call getreal__2_copy_tstar(data,tag)
       end if
       end if
       end subroutine get_copy_tstar
       subroutine getinteger__2_copy_tstar(data,tag)
       use BFG2InPlace_copy_tstar, only : get=>getinteger__2
       implicit none
       integer , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getinteger__2_copy_tstar
       subroutine getreal__2_copy_tstar(data,tag)
       use BFG2InPlace_copy_tstar, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_copy_tstar
       subroutine putreal__2_copy_tstar(data,tag)
       use BFG2InPlace_copy_tstar, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_copy_tstar
       ! f77 to f90 put/get wrappers end
