       ! f77 to f90 put/get wrappers start
       subroutine put_fixed_ocean(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==7) then
       if (tag==-105) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-107) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-276) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-105) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-276) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-107) then
       call putreal__2_fixed_ocean(data,tag)
       end if
       end if
       end subroutine put_fixed_ocean
       subroutine get_fixed_ocean(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void*
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==7) then
       if (tag==-105) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-107) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-276) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-105) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-276) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       if (tag==-107) then
       call getreal__2_fixed_ocean(data,tag)
       end if
       end if
       end subroutine get_fixed_ocean
       subroutine getreal__2_fixed_ocean(data,tag)
       use BFG2InPlace_fixed_ocean, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_fixed_ocean
       subroutine putreal__2_fixed_ocean(data,tag)
       use BFG2InPlace_fixed_ocean, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_fixed_ocean
       ! f77 to f90 put/get wrappers end
