       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !fixed_ocean__freq
       integer :: fixed_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,fixed_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_ocn
       real, dimension(1:36) :: r7
       !alat1_ocn
       real, dimension(1:36) :: r8
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r12
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_ocn
       real, dimension(1:36,1:36) :: r15
       !sensible_ocn
       real, dimension(1:36,1:36) :: r18
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_ocn
       real, dimension(1:36,1:36) :: r21
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_ocn
       real, dimension(1:36,1:36) :: r24
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_ocn
       real, dimension(1:36,1:36) :: r27
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !evap_ocn
       real, dimension(1:36,1:36) :: r33
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_ocn
       real, dimension(1:36,1:36) :: r36
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_ocn
       real, dimension(1:36,1:36) :: r39
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r42
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_ocn
       real, dimension(1:36,1:36) :: r45
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_ocn
       real, dimension(1:36,1:36) :: r48
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_ocn
       real, dimension(1:36,1:36) :: r51
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !energycarry_ocn_ice
       real, dimension(1:64,1:32) :: r86
       !dtcarry_ocn_ice
       real, dimension(1:64,1:32) :: r87
       !alon2_atm
       real, dimension(1:64) :: r94
       !alat2_atm
       real, dimension(1:32) :: r95
       !aboxedge1_lon_atm
       real, dimension(1:65) :: r96
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r97
       !aboxedge1_lat_atm
       real, dimension(1:33) :: r98
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r99
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r101
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r118
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r119
       !hrlons_atm
       real, dimension(1:320) :: r132
       !hrlats_atm
       real, dimension(1:160) :: r133
       !hrlonsedge_atm
       real, dimension(1:321) :: r134
       !hrlatsedge_atm
       real, dimension(1:161) :: r135
       !u10m_atm
       real, dimension(1:64,1:32) :: r142
       !v10m_atm
       real, dimension(1:64,1:32) :: r143
       !q2m_atm
       real, dimension(1:64,1:32) :: r145
       !rh2m_atm
       real, dimension(1:64,1:32) :: r146
       !psigma
       real, dimension(1:7) :: r150
       !massair
       real, dimension(1:64,1:32,1:7) :: r153
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r178
       !test_water_land
       real :: r222
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r144
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r270
       !surfdsigma
       real :: r149
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r211
       !conductflux_atm
       real, dimension(1:64,1:32) :: r277
       !ksic_loop
       integer :: r281
       !precip_atm
       real, dimension(1:64,1:32) :: r110
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r125
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r107
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r375
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r264
       !istep_sic
       integer :: r739
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r405
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r341
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r117
       !netsolar_atm
       real, dimension(1:64,1:32) :: r108
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r318
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r316
       !alat1_atm
       real, dimension(1:32) :: r93
       !klnd_loop
       integer :: r115
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r215
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r338
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r390
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r213
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r152
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r106
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r322
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r140
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r267
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r394
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r262
       !atmos_dt_tim
       real :: r104
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r266
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r30
       !iconv_che
       integer :: r175
       !alon1_atm
       real, dimension(1:64) :: r92
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r273
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r124
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r261
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r325
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r399
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r329
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r269
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r137
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r207
       !dt_write
       integer :: r59
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r263
       !netlong_atm
       real, dimension(1:64,1:32) :: r109
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r319
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !istep_lic
       integer :: r2050
       !water_flux_atmos
       real :: r130
       !land_niter_tim
       integer :: r102
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r139
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r377
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r138
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r409
       !katm_loop
       integer :: r369
       !write_flag_sic
       logical :: r3
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r321
       !write_flag_atm
       logical :: r1
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r210
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r367
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r315
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r214
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r317
       !fname_restart_main
       character(len=200) :: r58
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r332
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !istep_atm
       integer :: r302
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r411
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r347
       !land_rough_atm
       real, dimension(1:64,1:32) :: r218
       !istep_che
       integer :: r1613
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r123
       !outputdir_name
       character(len=200) :: r60
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r373
       !test_energy_seaice
       real :: r279
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r379
       !istep_ocn
       integer :: r1176
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r276
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r320
       !test_water_seaice
       real :: r280
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r371
       !iconv_ice
       integer :: r151
       !surfsigma
       real :: r148
       !temptop_atm
       real, dimension(1:64,1:32) :: r295
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r314
       !kocn_loop
       integer :: r383
       !glim_covmap
       real, dimension(1:64,1:32) :: r131
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r120
       !tstar_atm
       real, dimension(1:64,1:32) :: r105
       !land_latent_atm
       real, dimension(1:64,1:32) :: r212
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r265
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r209
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r386
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r272
       !surf_iter_tim_bfg_land
       integer :: r303
       !surf_iter_tim_bfg_ocean
       integer :: r740
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r260
       !ocean_niter_tim
       integer :: r103
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r335
       !land_evap_atm
       real, dimension(1:64,1:32) :: r216
       !lgraphics
       logical :: r111
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r91
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r271
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r401
       !genie_timestep
       real :: r57
       !ilon1_atm
       integer :: r90
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r116
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r217
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r344
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r219
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r136
       !write_flag_ocn
       logical :: r2
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r208
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r407
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r403
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r313
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r268
       !flag_land
       logical :: r114
       !flag_goldsic
       logical :: r113
       !flag_goldocn
       logical :: r112
       !glim_coupled
       logical :: r230
       !glim_snow_model
       logical :: r229
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r270=0.0
           r149=0.0
           r211=0.0
           r277=0.0
           r281=6
           r110=0.0
           r125=0.0
           r75=0
           r107=0.0
           r375=0.0
           r264=0.0
           r739=0
           r405=0.0
           ! 341is allocatable; cannot be written to yet
           r117=0.0
           r108=0.0
           ! 318is allocatable; cannot be written to yet
           ! 316is allocatable; cannot be written to yet
           r93=0.0
           r115=1
           r215=0.0
           r338=0.0
           r390=0.0
           r381=0.0
           r213=0.0
           r152=0.0
           ! 106is allocatable; cannot be written to yet
           r63=0.0
           ! 322is allocatable; cannot be written to yet
           r140=0.0
           r267=0.0
           r394=0.0
           r262=0.0
           r104=0.0
           r31=0.0
           r266=0.0
           r30=0.0
           r175=0
           r92=0.0
           r273=0.0
           r124=0.0
           r261=0.0
           r413=0.0
           r325=0.0
           r399=0.0
           r329=0.0
           r269=0.0
           r62=0.0
           r137=0.0
           r207=0.0
           r59=720
           r263=0.0
           r109=0.0
           ! 319is allocatable; cannot be written to yet
           r76=0.0
           r77=0.0
           r2050=0
           r130=0.0
           r102=0
           r139=0.0
           r377=0.0
           r138=0.0
           r409=0.0
           r369=1
           r3=.false.
           ! 321is allocatable; cannot be written to yet
           r1=.true.
           r210=0.0
           r367=0.0
           ! 315is allocatable; cannot be written to yet
           r41=0.0
           r214=0.0
           r61=0.0
           ! 317is allocatable; cannot be written to yet
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r332=0.0
           r16=0.0
           r302=0
           r411=0.0
           ! 347is allocatable; cannot be written to yet
           r218=0.0
           r1613=0
           r123=0.0
           r60='/home/armstroc/genie_output/genie_ig_fi_sl_dyex/main'
           r373=0.0
           r279=0.0
           r379=0.0
           r1176=0
           r276=0.0
           ! 320is allocatable; cannot be written to yet
           r280=0.0
           r371=0.0
           r151=0
           r148=0.0
           r295=0.0
           ! 314is allocatable; cannot be written to yet
           r383=48
           r131=0.0
           ! 120is allocatable; cannot be written to yet
           r105=0.0
           r212=0.0
           r265=0.0
           r209=0.0
           r386=0.0
           r272=0.0
           r303=0
           r740=0
           r260=0.0
           r103=0
           r335=0.0
           r216=0.0
           r111=.false.
           r78=0.0
           r91=32
           r271=0.0
           r401=0.0
           r57=3600.0
           r90=64
           ! 116is allocatable; cannot be written to yet
           r217=0.0
           ! 344is allocatable; cannot be written to yet
           r219=0.0
           r136=0.0
           r2=.true.
           r208=0.0
           r407=0.0
           r403=0.0
           ! 313is allocatable; cannot be written to yet
           r268=0.0
           r114=.false.
           r113=.false.
           r112=.false.
           r230=.true.
           r229=.false.
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call start_mpivis_log('bfg_averages ini_averages ')
       call bfg_averages_ini_averages_init(r1,r2,r3)
       call end_mpivis_log('bfg_averages ini_averages ')
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call start_mpivis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ')
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r75,r76,r77,r78)
       call end_mpivis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ')
       call put_initialise_fixedicesheet_mod(r75,-75)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call start_mpivis_log('fixed_chemistry initialise_fixedchem ')
       call initialise_fixedchem(r61,r62,r63)
       call end_mpivis_log('fixed_chemistry initialise_fixedchem ')
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call start_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       call initialise_igcmsurf()
       call end_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r92,-92)
       call get_igcm_atmosphere(r93,-93)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r105,-105)
       call get_igcm_atmosphere(r107,-107)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r118))) then
       allocate(r118(1:r90,1:r91))
       end if
       if (.not.(allocated(r119))) then
       allocate(r119(1:r90,1:r91))
       end if
       if (.not.(allocated(r106))) then
       allocate(r106(1:r90,1:r91))
       r106=0.0
       end if
       if (.not.(allocated(r120))) then
       allocate(r120(1:r90,1:r91))
       r120=0.0
       end if
       if (.not.(allocated(r116))) then
       allocate(r116(1:r90,1:r91))
       r116=0.0
       end if
       call start_mpivis_log('igcm_atmosphere initialise_atmos ')
       call initialise_atmos(r90,r91,r92,r93,r94,r95,r96,r97,r98,r99,r75,r101,r102,r103,r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r120,r76,r78,r123,r124,r125,r77,r61,r62,r63,r130,r131,r132,r133,r134,r135)
       call end_mpivis_log('igcm_atmosphere initialise_atmos ')
       call put_igcm_atmosphere(r92,-92)
       call put_igcm_atmosphere(r93,-93)
       call put_igcm_atmosphere(r105,-105)
       call put_igcm_atmosphere(r107,-107)
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r107,-107)
       call get_slab_seaice(r276,-276)
       call start_mpivis_log('slab_seaice initialise_slabseaice ')
       call initialise_slabseaice(r105,r107,r276,r277,r75,r279,r280,r281)
       call end_mpivis_log('slab_seaice initialise_slabseaice ')
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r107,-107)
       call put_slab_seaice(r276,-276)
       end if
       if (fixed_oceanThread()) then
       call setActiveModel(7)
       call get_fixed_ocean(r105,-105)
       call get_fixed_ocean(r107,-107)
       call get_fixed_ocean(r276,-276)
       call start_mpivis_log('fixed_ocean initialise_fixedocean ')
       call initialise_fixedocean(r105,r107,r276,r75)
       call end_mpivis_log('fixed_ocean initialise_fixedocean ')
       call put_fixed_ocean(r105,-105)
       call put_fixed_ocean(r107,-107)
       call put_fixed_ocean(r276,-276)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(8)
       call start_mpivis_log('counter counter 1')
       call counter(r302)
       call end_mpivis_log('counter counter 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(9)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r151,-151)
       call start_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call igcm3_adiab(r136,r137,r138,r139,r140,r116,r142,r143,r144,r145,r146,r76,r148,r149,r150,r151,r152,r153)
       call end_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r151,-151)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(10)
       if (.not.(allocated(r318))) then
       allocate(r318(1:r90,1:r91))
       r318=0.0
       end if
       if (.not.(allocated(r316))) then
       allocate(r316(1:r90,1:r91))
       r316=0.0
       end if
       if (.not.(allocated(r322))) then
       allocate(r322(1:r90,1:r91))
       r322=0.0
       end if
       if (.not.(allocated(r319))) then
       allocate(r319(1:r90,1:r91))
       r319=0.0
       end if
       if (.not.(allocated(r321))) then
       allocate(r321(1:r90,1:r91))
       r321=0.0
       end if
       if (.not.(allocated(r315))) then
       allocate(r315(1:r90,1:r91))
       r315=0.0
       end if
       if (.not.(allocated(r317))) then
       allocate(r317(1:r90,1:r91))
       r317=0.0
       end if
       if (.not.(allocated(r320))) then
       allocate(r320(1:r90,1:r91))
       r320=0.0
       end if
       if (.not.(allocated(r314))) then
       allocate(r314(1:r90,1:r91))
       r314=0.0
       end if
       if (.not.(allocated(r313))) then
       allocate(r313(1:r90,1:r91))
       r313=0.0
       end if
       call start_mpivis_log('transformer1 new_transformer_1 ')
       call new_transformer_1(r90,r91,r136,r137,r138,r139,r116,r140,r313,r314,r315,r316,r317,r318,r319,r320,r321,r322)
       call end_mpivis_log('transformer1 new_transformer_1 ')
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(11)
       call start_mpivis_log('counter_mod counter_mod 1')
       call counter_mod(r303,r102)
       call end_mpivis_log('counter_mod counter_mod 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(12)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call start_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call igcm_land_surflux(r302,r303,r102,r104,r148,r75,r108,r109,r110,r313,r314,r315,r316,r140,r207,r208,r209,r210,r211,r212,r213,r214,r215,r216,r217,r218,r219,r120,r117,r222,r78,r123,r124,r125,r77,r131,r229,r230)
       call end_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(13)
       call get_igcm_atmosphere(r75,-75)
       call start_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       call igcm_land_blayer(r303,r102,r104,r149,r75,r208,r209,r210,r211,r313,r314,r315,r316,r140)
       call end_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(14)
       call start_mpivis_log('counter_mod counter_mod 2')
       call counter_mod(r740,r103)
       call end_mpivis_log('counter_mod counter_mod 2')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r107,-107)
       call get_igcm_atmosphere(r105,-105)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call igcm_ocean_surflux(r302,r740,r103,r148,r75,r108,r109,r110,r317,r318,r319,r320,r140,r107,r105,r260,r261,r262,r263,r264,r265,r266,r267,r268,r269,r270,r271,r272,r273)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r107,-107)
       call put_igcm_atmosphere(r105,-105)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(16)
       call get_igcm_atmosphere(r75,-75)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       call igcm_ocean_blayer(r740,r103,r104,r149,r75,r261,r262,r263,r264,r317,r318,r319,r320,r140)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(17)
       call get_transformer2(r325,-325)
       call get_transformer2(r75,-75)
       call get_transformer2(r329,-329)
       if (.not.(allocated(r341))) then
       allocate(r341(1:r90,1:r91))
       r341=0.0
       end if
       if (.not.(allocated(r347))) then
       allocate(r347(1:r90,1:r91))
       r347=0.0
       end if
       if (.not.(allocated(r344))) then
       allocate(r344(1:r90,1:r91))
       r344=0.0
       end if
       call start_mpivis_log('transformer2 new_transformer_2 ')
       call new_transformer_2(r90,r91,r325,r75,r212,r265,r329,r213,r266,r332,r214,r267,r335,r215,r268,r338,r216,r269,r341,r217,r270,r344,r218,r271,r347,r219,r272,r106,r120,r273,r136,r313,r317,r137,r314,r318,r138,r315,r319,r139,r316,r320)
       call end_mpivis_log('transformer2 new_transformer_2 ')
       call put_transformer2(r325,-325)
       call put_transformer2(r329,-329)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call get_igcm_atmosphere(r325,-325)
       call get_igcm_atmosphere(r329,-329)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r175,-175)
       call start_mpivis_log('igcm_atmosphere igcm3_diab ')
       call igcm3_diab(r136,r137,r138,r139,r106,r347,r341,r344,r325,r329,r332,r335,r108,r109,r110,r125,r144,r216,r61,r62,r63,r175,r130,r152,r178)
       call end_mpivis_log('igcm_atmosphere igcm3_diab ')
       call put_igcm_atmosphere(r325,-325)
       call put_igcm_atmosphere(r329,-329)
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r175,-175)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(19)
       call get_transformer3(r367,-367)
       call get_transformer3(r325,-325)
       call get_transformer3(r371,-371)
       call get_transformer3(r329,-329)
       call get_transformer3(r373,-373)
       call get_transformer3(r108,-108)
       call get_transformer3(r375,-375)
       call get_transformer3(r109,-109)
       call get_transformer3(r377,-377)
       call get_transformer3(r379,-379)
       call get_transformer3(r381,-381)
       call start_mpivis_log('transformer3 new_transformer_3 ')
       call new_transformer_3(r90,r91,r367,r325,r369,r281,r371,r329,r373,r108,r375,r109,r377,r332,r379,r335,r381,r110,r383)
       call end_mpivis_log('transformer3 new_transformer_3 ')
       call put_transformer3(r367,-367)
       call put_transformer3(r325,-325)
       call put_transformer3(r371,-371)
       call put_transformer3(r329,-329)
       call put_transformer3(r373,-373)
       call put_transformer3(r108,-108)
       call put_transformer3(r375,-375)
       call put_transformer3(r109,-109)
       call put_transformer3(r377,-377)
       call put_transformer3(r379,-379)
       call put_transformer3(r381,-381)
       end if
       end if
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call start_mpivis_log('counter counter 2')
       call counter(r739)
       call end_mpivis_log('counter counter 2')
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r367,-367)
       call get_slab_seaice(r371,-371)
       call get_slab_seaice(r373,-373)
       call get_slab_seaice(r375,-375)
       call get_slab_seaice(r325,-325)
       call get_slab_seaice(r329,-329)
       call get_slab_seaice(r108,-108)
       call get_slab_seaice(r109,-109)
       call get_slab_seaice(r377,-377)
       call get_slab_seaice(r379,-379)
       call get_slab_seaice(r276,-276)
       call get_slab_seaice(r107,-107)
       call start_mpivis_log('slab_seaice slabseaice ')
       call slabseaice(r739,r105,r367,r371,r373,r375,r325,r329,r108,r109,r377,r379,r276,r295,r277,r107,r75,r279,r280,r281)
       call end_mpivis_log('slab_seaice slabseaice ')
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r367,-367)
       call put_slab_seaice(r371,-371)
       call put_slab_seaice(r373,-373)
       call put_slab_seaice(r375,-375)
       call put_slab_seaice(r325,-325)
       call put_slab_seaice(r329,-329)
       call put_slab_seaice(r108,-108)
       call put_slab_seaice(r109,-109)
       call put_slab_seaice(r377,-377)
       call put_slab_seaice(r379,-379)
       call put_slab_seaice(r276,-276)
       call put_slab_seaice(r107,-107)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call get_transformer4(r276,-276)
       call start_mpivis_log('transformer4 new_transformer_4 ')
       call new_transformer_4(r90,r91,r386,r276,r281,r383,r390,r277)
       call end_mpivis_log('transformer4 new_transformer_4 ')
       call put_transformer4(r276,-276)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer5(r394,-394)
       call get_transformer5(r325,-325)
       call get_transformer5(r276,-276)
       call get_transformer5(r399,-399)
       call get_transformer5(r329,-329)
       call get_transformer5(r401,-401)
       call get_transformer5(r108,-108)
       call get_transformer5(r403,-403)
       call get_transformer5(r109,-109)
       call get_transformer5(r405,-405)
       call get_transformer5(r407,-407)
       call get_transformer5(r409,-409)
       call get_transformer5(r411,-411)
       call get_transformer5(r413,-413)
       call start_mpivis_log('transformer5 new_transformer_5 ')
       call new_transformer_5(r90,r91,r394,r325,r276,r369,r383,r399,r329,r401,r108,r403,r109,r405,r332,r407,r335,r409,r110,r411,r338,r413,r117)
       call end_mpivis_log('transformer5 new_transformer_5 ')
       call put_transformer5(r394,-394)
       call put_transformer5(r325,-325)
       call put_transformer5(r276,-276)
       call put_transformer5(r399,-399)
       call put_transformer5(r329,-329)
       call put_transformer5(r401,-401)
       call put_transformer5(r108,-108)
       call put_transformer5(r403,-403)
       call put_transformer5(r109,-109)
       call put_transformer5(r405,-405)
       call put_transformer5(r407,-407)
       call put_transformer5(r409,-409)
       call put_transformer5(r411,-411)
       call put_transformer5(r413,-413)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(24)
       call start_mpivis_log('counter counter 3')
       call counter(r1176)
       call end_mpivis_log('counter counter 3')
       end if
       end if
       if (fixed_oceanThread()) then
       if(mod(its1,fixed_ocean__freq).eq.0)then
       call setActiveModel(25)
       call get_fixed_ocean(r105,-105)
       call get_fixed_ocean(r276,-276)
       call get_fixed_ocean(r107,-107)
       call start_mpivis_log('fixed_ocean fixedocean ')
       call fixedocean(r1176,r105,r276,r86,r87,r107,r75)
       call end_mpivis_log('fixed_ocean fixedocean ')
       call put_fixed_ocean(r105,-105)
       call put_fixed_ocean(r276,-276)
       call put_fixed_ocean(r107,-107)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(26)
       call start_mpivis_log('counter counter 4')
       call counter(r1613)
       call end_mpivis_log('counter counter 4')
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(27)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r175,-175)
       call start_mpivis_log('fixed_chemistry fixedchem ')
       call fixedchem(r1613,r61,r62,r63,r175)
       call end_mpivis_log('fixed_chemistry fixedchem ')
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r175,-175)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(28)
       call start_mpivis_log('counter counter 5')
       call counter(r2050)
       call end_mpivis_log('counter counter 5')
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(29)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r151,-151)
       call start_mpivis_log('fixed_icesheet fixedicesheet ')
       call fixedicesheet(r2050,r75,r76,r77,r78,r151)
       call end_mpivis_log('fixed_icesheet fixedicesheet ')
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r151,-151)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(30)
       call get_bfg_averages(r92,-92)
       call get_bfg_averages(r93,-93)
       call get_bfg_averages(r401,-401)
       call get_bfg_averages(r403,-403)
       call get_bfg_averages(r399,-399)
       call get_bfg_averages(r394,-394)
       call get_bfg_averages(r405,-405)
       call get_bfg_averages(r407,-407)
       call get_bfg_averages(r411,-411)
       call get_bfg_averages(r409,-409)
       call get_bfg_averages(r413,-413)
       call get_bfg_averages(r105,-105)
       call get_bfg_averages(r107,-107)
       call start_mpivis_log('bfg_averages write_averages ')
       call bfg_averages_write_averages_iteration(r1176,r92,r93,r7,r8,r9,r10,r401,r12,r13,r403,r15,r16,r399,r18,r19,r394,r21,r22,r405,r24,r25,r407,r27,r28,r390,r30,r31,r411,r33,r34,r409,r36,r37,r413,r39,r40,r41,r42,r43,r386,r45,r46,r105,r48,r49,r107,r51,r52,r1,r2,r3,r383,r57,r58,r59,r60)
       call end_mpivis_log('bfg_averages write_averages ')
       call put_bfg_averages(r92,-92)
       call put_bfg_averages(r93,-93)
       call put_bfg_averages(r401,-401)
       call put_bfg_averages(r403,-403)
       call put_bfg_averages(r399,-399)
       call put_bfg_averages(r394,-394)
       call put_bfg_averages(r405,-405)
       call put_bfg_averages(r407,-407)
       call put_bfg_averages(r411,-411)
       call put_bfg_averages(r409,-409)
       call put_bfg_averages(r413,-413)
       call put_bfg_averages(r105,-105)
       call put_bfg_averages(r107,-107)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call get_transformer6(r367,-367)
       call get_transformer6(r371,-371)
       call get_transformer6(r373,-373)
       call get_transformer6(r375,-375)
       call get_transformer6(r377,-377)
       call get_transformer6(r379,-379)
       call get_transformer6(r381,-381)
       call start_mpivis_log('transformer6 new_transformer_6 ')
       call new_transformer_6(r90,r91,r367,r371,r373,r375,r377,r379,r381)
       call end_mpivis_log('transformer6 new_transformer_6 ')
       call put_transformer6(r367,-367)
       call put_transformer6(r371,-371)
       call put_transformer6(r373,-373)
       call put_transformer6(r375,-375)
       call put_transformer6(r377,-377)
       call put_transformer6(r379,-379)
       call put_transformer6(r381,-381)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(32)
       call get_transformer7(r394,-394)
       call get_transformer7(r399,-399)
       call get_transformer7(r401,-401)
       call get_transformer7(r403,-403)
       call get_transformer7(r405,-405)
       call get_transformer7(r407,-407)
       call get_transformer7(r409,-409)
       call get_transformer7(r411,-411)
       call get_transformer7(r413,-413)
       call start_mpivis_log('transformer7 new_transformer_7 ')
       call new_transformer_7(r90,r91,r394,r399,r401,r403,r405,r407,r409,r411,r413,r386,r390,r41)
       call end_mpivis_log('transformer7 new_transformer_7 ')
       call put_transformer7(r394,-394)
       call put_transformer7(r399,-399)
       call put_transformer7(r401,-401)
       call put_transformer7(r403,-403)
       call put_transformer7(r405,-405)
       call put_transformer7(r407,-407)
       call put_transformer7(r409,-409)
       call put_transformer7(r411,-411)
       call put_transformer7(r413,-413)
       end if
       end if
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(33)
       call start_mpivis_log('igcm_atmosphere end_atmos ')
       call end_atmos()
       call end_mpivis_log('igcm_atmosphere end_atmos ')
       end if
       call finaliseComms()
       end program BFG2Main

