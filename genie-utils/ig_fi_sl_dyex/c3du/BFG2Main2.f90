       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !fixed_ocean__freq
       integer :: fixed_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,fixed_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !conductflux_atm
       real, dimension(1:64,1:32) :: r277
       !ksic_loop
       integer :: r281
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r107
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r375
       !istep_sic
       integer :: r739
       !netsolar_atm
       real, dimension(1:64,1:32) :: r108
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r390
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r325
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r329
       !netlong_atm
       real, dimension(1:64,1:32) :: r109
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r377
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r367
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r373
       !test_energy_seaice
       real :: r279
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r379
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r276
       !test_water_seaice
       real :: r280
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r371
       !temptop_atm
       real, dimension(1:64,1:32) :: r295
       !kocn_loop
       integer :: r383
       !tstar_atm
       real, dimension(1:64,1:32) :: r105
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r386
       !ilat1_atm
       integer :: r91
       !ilon1_atm
       integer :: r90
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101113,file='BFG2Control.nam')
       read(101113,time)
       close(101113)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent modelscoupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
           r276=0.0
       ! End P2P notation priming
       ! Begin set notation priming
           r277=0.0
           r281=6
           r75=0
           r739=0
           r390=0.0
           r279=0.0
           r280=0.0
           r295=0.0
           r383=48
           r386=0.0
           r91=32
           r90=64
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(slab_seaiceThread())then
       call nmlinit1214(r105,r367,r371,r373,r375,r325,r329,r108,r109,r377,r379,r276,r107,r381)
       end if
       ! netcdf files
       ! End initial values file read
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r107,-107)
       call get_slab_seaice(r276,-276)
       call get_slab_seaice(r75,-75)
       call start_mpivis_log('slab_seaice initialise_slabseaice ')
       call initialise_slabseaice(r105,r107,r276,r277,r75,r279,r280,r281)
       call end_mpivis_log('slab_seaice initialise_slabseaice ')
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r107,-107)
       call put_slab_seaice(r276,-276)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call start_mpivis_log('counter counter 2')
       call counter(r739)
       call end_mpivis_log('counter counter 2')
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call get_slab_seaice(r105,-105)
       call get_slab_seaice(r367,-367)
       call get_slab_seaice(r371,-371)
       call get_slab_seaice(r373,-373)
       call get_slab_seaice(r375,-375)
       call get_slab_seaice(r325,-325)
       call get_slab_seaice(r329,-329)
       call get_slab_seaice(r108,-108)
       call get_slab_seaice(r109,-109)
       call get_slab_seaice(r377,-377)
       call get_slab_seaice(r379,-379)
       call get_slab_seaice(r276,-276)
       call get_slab_seaice(r107,-107)
       call get_slab_seaice(r75,-75)
       call start_mpivis_log('slab_seaice slabseaice ')
       call slabseaice(r739,r105,r367,r371,r373,r375,r325,r329,r108,r109,r377,r379,r276,r295,r277,r107,r75,r279,r280,r281)
       call spin(0.3)
       call end_mpivis_log('slab_seaice slabseaice ')
       call put_slab_seaice(r105,-105)
       call put_slab_seaice(r367,-367)
       call put_slab_seaice(r371,-371)
       call put_slab_seaice(r373,-373)
       call put_slab_seaice(r375,-375)
       call put_slab_seaice(r325,-325)
       call put_slab_seaice(r329,-329)
       call put_slab_seaice(r108,-108)
       call put_slab_seaice(r109,-109)
       call put_slab_seaice(r377,-377)
       call put_slab_seaice(r379,-379)
       call put_slab_seaice(r276,-276)
       call put_slab_seaice(r107,-107)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call get_transformer4(r386,-386)
       call get_transformer4(r276,-276)
       call get_transformer4(r390,-390)
       call start_mpivis_log('transformer4 new_transformer_4 ')
       call new_transformer_4(r90,r91,r386,r276,r281,r383,r390,r277)
       call end_mpivis_log('transformer4 new_transformer_4 ')
       call put_transformer4(r386,-386)
       call put_transformer4(r276,-276)
       call put_transformer4(r390,-390)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call get_transformer6(r367,-367)
       call get_transformer6(r371,-371)
       call get_transformer6(r373,-373)
       call get_transformer6(r375,-375)
       call get_transformer6(r377,-377)
       call get_transformer6(r379,-379)
       call get_transformer6(r381,-381)
       call start_mpivis_log('transformer6 new_transformer_6 ')
       call new_transformer_6(r90,r91,r367,r371,r373,r375,r377,r379,r381)
       call end_mpivis_log('transformer6 new_transformer_6 ')
       call put_transformer6(r367,-367)
       call put_transformer6(r371,-371)
       call put_transformer6(r373,-373)
       call put_transformer6(r375,-375)
       call put_transformer6(r377,-377)
       call put_transformer6(r379,-379)
       call put_transformer6(r381,-381)
       end if
       end if
       !call commsSync()
       end do
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit1214(R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379)
        implicit none
       real, dimension(1:64,1:32) :: R105
       real, dimension(1:64,1:32) :: R365
       real, dimension(1:64,1:32) :: R369
       real, dimension(1:64,1:32) :: R371
       real, dimension(1:64,1:32) :: R373
       real, dimension(1:64,1:32) :: R323
       real, dimension(1:64,1:32) :: R327
       real, dimension(1:64,1:32) :: R108
       real, dimension(1:64,1:32) :: R109
       real, dimension(1:64,1:32) :: R375
       real, dimension(1:64,1:32) :: R377
       real, dimension(1:64,1:32) :: R274
       real, dimension(1:64,1:32) :: R107
       real, dimension(1:64,1:32) :: R379
       namelist /conc_pri/ R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379
       open(unit=1214,file='conc_pri.nam')
       read(1214,conc_pri)
       close(1214)
       end subroutine nmlinit1214
