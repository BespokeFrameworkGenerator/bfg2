       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !fixed_ocean__freq
       integer :: fixed_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,fixed_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Pointand Uncoupled Vars
       !alon2_atm
       real, dimension(1:64) :: r94
       !alat2_atm
       real, dimension(1:32) :: r95
       !aboxedge1_lon_atm
       real, dimension(1:65) :: r96
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r97
       !aboxedge1_lat_atm
       real, dimension(1:33) :: r98
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r99
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r101
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r118
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r119
       !hrlons_atm
       real, dimension(1:320) :: r132
       !hrlats_atm
       real, dimension(1:160) :: r133
       !hrlonsedge_atm
       real, dimension(1:321) :: r134
       !hrlatsedge_atm
       real, dimension(1:161) :: r135
       !u10m_atm
       real, dimension(1:64,1:32) :: r142
       !v10m_atm
       real, dimension(1:64,1:32) :: r143
       !q2m_atm
       real, dimension(1:64,1:32) :: r145
       !rh2m_atm
       real, dimension(1:64,1:32) :: r146
       !psigma
       real, dimension(1:7) :: r150
       !massair
       real, dimension(1:64,1:32,1:7) :: r153
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r178
       !test_water_land
       real :: r222
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r144
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r270
       !surfdsigma
       real :: r149
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r211
       !ksic_loop
       integer :: r281
       !precip_atm
       real, dimension(1:64,1:32) :: r110
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r125
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r107
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r375
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r264
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r405
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r341
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r117
       !netsolar_atm
       real, dimension(1:64,1:32) :: r108
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r318
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r316
       !alat1_atm
       real, dimension(1:32) :: r93
       !klnd_loop
       integer :: r115
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r215
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r338
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r213
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r152
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r106
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r322
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r140
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r267
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r394
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r262
       !atmos_dt_tim
       real :: r104
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r266
       !iconv_che
       integer :: r175
       !alon1_atm
       real, dimension(1:64) :: r92
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r273
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r124
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r261
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r325
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r399
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r329
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r269
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r137
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r207
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r263
       !netlong_atm
       real, dimension(1:64,1:32) :: r109
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r319
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !water_flux_atmos
       real :: r130
       !land_niter_tim
       integer :: r102
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r139
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r377
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r138
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r409
       !katm_loop
       integer :: r369
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r321
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r210
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r367
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r315
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r214
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r317
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r332
       !istep_atm
       integer :: r302
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r411
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r347
       !land_rough_atm
       real, dimension(1:64,1:32) :: r218
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r123
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r373
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r379
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r276
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r320
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r371
       !iconv_ice
       integer :: r151
       !surfsigma
       real :: r148
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r314
       !kocn_loop
       integer :: r383
       !glim_covmap
       real, dimension(1:64,1:32) :: r131
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r120
       !tstar_atm
       real, dimension(1:64,1:32) :: r105
       !land_latent_atm
       real, dimension(1:64,1:32) :: r212
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r265
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r209
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r272
       !surf_iter_tim_bfg_land
       integer :: r303
       !surf_iter_tim_bfg_ocean
       integer :: r740
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r260
       !ocean_niter_tim
       integer :: r103
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r335
       !land_evap_atm
       real, dimension(1:64,1:32) :: r216
       !lgraphics
       logical :: r111
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r91
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r271
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r401
       !ilon1_atm
       integer :: r90
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r116
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r217
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r344
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r219
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r136
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r208
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r407
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r403
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r313
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r268
       !flag_land
       logical :: r114
       !flag_goldsic
       logical :: r113
       !flag_goldocn
       logical :: r112
       !glim_coupled
       logical :: r230
       !glim_snow_model
       logical :: r229
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101113,file='BFG2Control.nam')
       read(101113,time)
       close(101113)
       ! End control values file read
       ! Init model datastructures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
           r105=0.0
           r367=0.0
           r371=0.0
           r373=0.0
           r375=0.0
           r325=0.0
           r329=0.0
           r108=0.0
           r109=0.0
           r377=0.0
           r379=0.0
           r107=0.0
           r381=0.0
       ! End P2P notation priming
       ! Beginset notation priming
           r270=0.0
           r149=0.0
           r211=0.0
           r281=6
           r110=0.0
           r125=0.0
           r75=0
           r264=0.0
           r405=0.0
           ! 341is allocatable; cannot be written to yet
           r117=0.0
           ! 318is allocatable; cannot be written to yet
           ! 316is allocatable; cannot be written to yet
           r93=0.0
           r115=1
           r215=0.0
           r338=0.0
           r213=0.0
           r152=0.0
           ! 106is allocatable; cannot be written to yet
           r63=0.0
           ! 322is allocatable; cannot be written to yet
           r140=0.0
           r267=0.0
           r394=0.0
           r262=0.0
           r104=0.0
           r266=0.0
           r175=0
           r92=0.0
           r273=0.0
           r124=0.0
           r261=0.0
           r413=0.0
           r399=0.0
           r269=0.0
           r62=0.0
           r137=0.0
           r207=0.0
           r263=0.0
           ! 319is allocatable; cannot be written to yet
           r76=0.0
           r77=0.0
           r130=0.0
           r102=0
           r139=0.0
           r138=0.0
           r409=0.0
           r369=1
           ! 321is allocatable; cannot be written to yet
           r210=0.0
           ! 315is allocatable; cannot be written to yet
           r214=0.0
           r61=0.0
           ! 317is allocatable; cannot be written to yet
           r332=0.0
           r302=0
           r411=0.0
           ! 347is allocatable; cannot be written to yet
           r218=0.0
           r123=0.0
           ! 320is allocatable; cannot be written to yet
           r151=0
           r148=0.0
           ! 314is allocatable; cannot be written to yet
           r383=48
           r131=0.0
           ! 120is allocatable; cannot be written to yet
           r212=0.0
           r265=0.0
           r209=0.0
           r272=0.0
           r303=0
           r740=0
           r260=0.0
           r103=0
           r335=0.0
           r216=0.0
           r111=.false.
           r78=0.0
           r91=32
           r271=0.0
           r401=0.0
           r90=64
           ! 116is allocatable; cannot be written to yet
           r217=0.0
           ! 344is allocatable; cannot be written to yet
           r219=0.0
           r136=0.0
           r208=0.0
           r407=0.0
           r403=0.0
           ! 313is allocatable; cannot be written to yet
           r268=0.0
           r114=.false.
           r113=.false.
           r112=.false.
           r230=.true.
           r229=.false.
       ! End setnotation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call start_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       call initialise_igcmsurf()
       call end_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r92,-92)
       call get_igcm_atmosphere(r93,-93)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r105,-105)
       call get_igcm_atmosphere(r107,-107)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r118))) then
       allocate(r118(1:r90,1:r91))
       end if
       if (.not.(allocated(r119))) then
       allocate(r119(1:r90,1:r91))
       end if
       if (.not.(allocated(r106))) then
       allocate(r106(1:r90,1:r91))
       r106=0.0
       end if
       if (.not.(allocated(r120))) then
       allocate(r120(1:r90,1:r91))
       r120=0.0
       end if
       if (.not.(allocated(r116))) then
       allocate(r116(1:r90,1:r91))
       r116=0.0
       end if
       call start_mpivis_log('igcm_atmosphere initialise_atmos ')
       call initialise_atmos(r90,r91,r92,r93,r94,r95,r96,r97,r98,r99,r75,r101,r102,r103,r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r120,r76,r78,r123,r124,r125,r77,r61,r62,r63,r130,r131,r132,r133,r134,r135)
       call end_mpivis_log('igcm_atmosphere initialise_atmos ')
       call put_igcm_atmosphere(r92,-92)
       call put_igcm_atmosphere(r93,-93)
       call put_igcm_atmosphere(r105,-105)
       call put_igcm_atmosphere(r107,-107)
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(8)
       call start_mpivis_log('counter counter 1')
       call counter(r302)
       call end_mpivis_log('counter counter 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(9)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r151,-151)
       call start_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call igcm3_adiab(r136,r137,r138,r139,r140,r116,r142,r143,r144,r145,r146,r76,r148,r149,r150,r151,r152,r153)
       call end_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r151,-151)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(10)
       if (.not.(allocated(r318))) then
       allocate(r318(1:r90,1:r91))
       r318=0.0
       end if
       if (.not.(allocated(r316))) then
       allocate(r316(1:r90,1:r91))
       r316=0.0
       end if
       if (.not.(allocated(r322))) then
       allocate(r322(1:r90,1:r91))
       r322=0.0
       end if
       if (.not.(allocated(r319))) then
       allocate(r319(1:r90,1:r91))
       r319=0.0
       end if
       if (.not.(allocated(r321))) then
       allocate(r321(1:r90,1:r91))
       r321=0.0
       end if
       if (.not.(allocated(r315))) then
       allocate(r315(1:r90,1:r91))
       r315=0.0
       end if
       if (.not.(allocated(r317))) then
       allocate(r317(1:r90,1:r91))
       r317=0.0
       end if
       if (.not.(allocated(r320))) then
       allocate(r320(1:r90,1:r91))
       r320=0.0
       end if
       if (.not.(allocated(r314))) then
       allocate(r314(1:r90,1:r91))
       r314=0.0
       end if
       if (.not.(allocated(r313))) then
       allocate(r313(1:r90,1:r91))
       r313=0.0
       end if
       call start_mpivis_log('transformer1 new_transformer_1 ')
       call new_transformer_1(r90,r91,r136,r137,r138,r139,r116,r140,r313,r314,r315,r316,r317,r318,r319,r320,r321,r322)
       call end_mpivis_log('transformer1 new_transformer_1 ')
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(11)
       call start_mpivis_log('counter_mod counter_mod 1')
       call counter_mod(r303,r102)
       call end_mpivis_log('counter_mod counter_mod 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(12)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call start_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call igcm_land_surflux(r302,r303,r102,r104,r148,r75,r108,r109,r110,r313,r314,r315,r316,r140,r207,r208,r209,r210,r211,r212,r213,r214,r215,r216,r217,r218,r219,r120,r117,r222,r78,r123,r124,r125,r77,r131,r229,r230)
       call end_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(13)
       call get_igcm_atmosphere(r75,-75)
       call start_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       call igcm_land_blayer(r303,r102,r104,r149,r75,r208,r209,r210,r211,r313,r314,r315,r316,r140)
       call end_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(14)
       call start_mpivis_log('counter_mod counter_mod 2')
       call counter_mod(r740,r103)
       call end_mpivis_log('counter_mod counter_mod 2')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r107,-107)
       call get_igcm_atmosphere(r105,-105)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call igcm_ocean_surflux(r302,r740,r103,r148,r75,r108,r109,r110,r317,r318,r319,r320,r140,r107,r105,r260,r261,r262,r263,r264,r265,r266,r267,r268,r269,r270,r271,r272,r273)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r107,-107)
       call put_igcm_atmosphere(r105,-105)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(16)
       call get_igcm_atmosphere(r75,-75)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       call igcm_ocean_blayer(r740,r103,r104,r149,r75,r261,r262,r263,r264,r317,r318,r319,r320,r140)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(17)
       call get_transformer2(r325,-325)
       call get_transformer2(r75,-75)
       call get_transformer2(r329,-329)
       if (.not.(allocated(r341))) then
       allocate(r341(1:r90,1:r91))
       r341=0.0
       end if
       if (.not.(allocated(r347))) then
       allocate(r347(1:r90,1:r91))
       r347=0.0
       end if
       if (.not.(allocated(r344))) then
       allocate(r344(1:r90,1:r91))
       r344=0.0
       end if
       call start_mpivis_log('transformer2 new_transformer_2 ')
       call new_transformer_2(r90,r91,r325,r75,r212,r265,r329,r213,r266,r332,r214,r267,r335,r215,r268,r338,r216,r269,r341,r217,r270,r344,r218,r271,r347,r219,r272,r106,r120,r273,r136,r313,r317,r137,r314,r318,r138,r315,r319,r139,r316,r320)
       call end_mpivis_log('transformer2 new_transformer_2 ')
       call put_transformer2(r325,-325)
       call put_transformer2(r329,-329)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call get_igcm_atmosphere(r325,-325)
       call get_igcm_atmosphere(r329,-329)
       call get_igcm_atmosphere(r108,-108)
       call get_igcm_atmosphere(r109,-109)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r175,-175)
       call start_mpivis_log('igcm_atmosphere igcm3_diab ')
       call igcm3_diab(r136,r137,r138,r139,r106,r347,r341,r344,r325,r329,r332,r335,r108,r109,r110,r125,r144,r216,r61,r62,r63,r175,r130,r152,r178)
       call end_mpivis_log('igcm_atmosphere igcm3_diab ')
       call put_igcm_atmosphere(r325,-325)
       call put_igcm_atmosphere(r329,-329)
       call put_igcm_atmosphere(r108,-108)
       call put_igcm_atmosphere(r109,-109)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r175,-175)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(19)
       call get_transformer3(r367,-367)
       call get_transformer3(r325,-325)
       call get_transformer3(r371,-371)
       call get_transformer3(r329,-329)
       call get_transformer3(r373,-373)
       call get_transformer3(r108,-108)
       call get_transformer3(r375,-375)
       call get_transformer3(r109,-109)
       call get_transformer3(r377,-377)
       call get_transformer3(r379,-379)
       call get_transformer3(r381,-381)
       call start_mpivis_log('transformer3 new_transformer_3 ')
       call new_transformer_3(r90,r91,r367,r325,r369,r281,r371,r329,r373,r108,r375,r109,r377,r332,r379,r335,r381,r110,r383)
       call end_mpivis_log('transformer3 new_transformer_3 ')
       call put_transformer3(r367,-367)
       call put_transformer3(r325,-325)
       call put_transformer3(r371,-371)
       call put_transformer3(r329,-329)
       call put_transformer3(r373,-373)
       call put_transformer3(r108,-108)
       call put_transformer3(r375,-375)
       call put_transformer3(r109,-109)
       call put_transformer3(r377,-377)
       call put_transformer3(r379,-379)
       call put_transformer3(r381,-381)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer5(r394,-394)
       call get_transformer5(r325,-325)
       call get_transformer5(r276,-276)
       call get_transformer5(r399,-399)
       call get_transformer5(r329,-329)
       call get_transformer5(r401,-401)
       call get_transformer5(r108,-108)
       call get_transformer5(r403,-403)
       call get_transformer5(r109,-109)
       call get_transformer5(r405,-405)
       call get_transformer5(r407,-407)
       call get_transformer5(r409,-409)
       call get_transformer5(r411,-411)
       call get_transformer5(r413,-413)
       call start_mpivis_log('transformer5 new_transformer_5 ')
       call new_transformer_5(r90,r91,r394,r325,r276,r369,r383,r399,r329,r401,r108,r403,r109,r405,r332,r407,r335,r409,r110,r411,r338,r413,r117)
       call end_mpivis_log('transformer5 new_transformer_5 ')
       call put_transformer5(r394,-394)
       call put_transformer5(r325,-325)
       call put_transformer5(r276,-276)
       call put_transformer5(r399,-399)
       call put_transformer5(r329,-329)
       call put_transformer5(r401,-401)
       call put_transformer5(r108,-108)
       call put_transformer5(r403,-403)
       call put_transformer5(r109,-109)
       call put_transformer5(r405,-405)
       call put_transformer5(r407,-407)
       call put_transformer5(r409,-409)
       call put_transformer5(r411,-411)
       call put_transformer5(r413,-413)
       end if
       end if
       !call commsSync()
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(33)
       call start_mpivis_log('igcm_atmosphere end_atmos ')
       call end_atmos()
       call end_mpivis_log('igcm_atmosphere end_atmos ')
       end if
       call finaliseComms()
       end program BFG2Main

