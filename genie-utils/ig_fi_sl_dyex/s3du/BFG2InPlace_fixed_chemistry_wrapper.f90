       ! f77 to f90 put/get wrappers start
       subroutine put_fixed_chemistry(data,tag)
       use BFG2Target3
       implicit none
       real  , intent(in) :: data
       integer  , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==3) then
       if (tag==-61) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-62) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-63) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       end if
       if (currentModel==27) then
       if (tag==-61) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-62) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-63) then
       call putreal__2_fixed_chemistry(data,tag)
       end if
       end if
       if (currentModel==27) then
       if (tag==-175) then
       call putinteger__0_fixed_chemistry(data,tag)
       end if
       end if
       end subroutine put_fixed_chemistry
       subroutine get_fixed_chemistry(data,tag)
       use BFG2Target3
       implicit none
       real  , intent(out) :: data
       integer  , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==3) then
       if (tag==-61) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-62) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-63) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       end if
       if (currentModel==27) then
       if (tag==-61) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-62) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       if (tag==-63) then
       call getreal__2_fixed_chemistry(data,tag)
       end if
       end if
       if (currentModel==27) then
       if (tag==-175) then
       call getinteger__0_fixed_chemistry(data,tag)
       end if
       end if
       end subroutine get_fixed_chemistry
       subroutine getreal__2_fixed_chemistry(data,tag)
       use BFG2InPlace_fixed_chemistry, only : get=>getreal__2
       implicit none
       real  , intent(in), dimension() :: data
       integer  , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_fixed_chemistry
       subroutine getinteger__0_fixed_chemistry(data,tag)
       use BFG2InPlace_fixed_chemistry, only : get=>getinteger__0
       implicit none
       integer  , intent(in) :: data
       integer  , intent(in) :: tag
       call get(data,tag)
       end subroutine getinteger__0_fixed_chemistry
       subroutine putreal__2_fixed_chemistry(data,tag)
       use BFG2InPlace_fixed_chemistry, only : put=>putreal__2
       implicit none
       real  , intent(out), dimension() :: data
       integer  , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_fixed_chemistry
       subroutine putinteger__0_fixed_chemistry(data,tag)
       use BFG2InPlace_fixed_chemistry, only : put=>putinteger__0
       implicit none
       integer  , intent(out) :: data
       integer  , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__0_fixed_chemistry
       ! f77 to f90 put/get wrappers end
