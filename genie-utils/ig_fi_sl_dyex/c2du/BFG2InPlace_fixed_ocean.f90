       module BFG2InPlace_fixed_ocean
       use BFG2Target2
       use mpi
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(6) :: pp2p=(/0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==7) then
       ! I am model=fixed_ocean andep=initialise_fixedocean and instance=
       if (arg2==-105) then
       ! I am fixed_ocean.initialise_fixedocean..7
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=7
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,105,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-107) then
       ! I amfixed_ocean.initialise_fixedocean..7
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=7
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,107,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-276) then
       ! I am fixed_ocean.initialise_fixedocean..7
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/6,7,21,22,23,25/)
       myID=7
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,276,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==25) then
       ! I am model=fixed_ocean and ep=fixedocean and instance=
       if (arg2==-105) then
       ! I am fixed_ocean.fixedocean..25
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=25
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,105,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-276) then
       ! I am fixed_ocean.fixedocean..25
       remoteID=-1
       setSize=6
       allocate(set(1:setSize))
       set=(/6,7,21,22,23,25/)
       myID=25
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,276,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-107) then
       ! I amfixed_ocean.fixedocean..25
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       myID=25
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,107,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==7) then
       ! I am model=fixed_ocean and ep=initialise_fixedocean and instance=
       if (arg2==-105) then
       ! I am fixed_ocean.initialise_fixedocean..7
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=7
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,105,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,105,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-107) then
       ! I am fixed_ocean.initialise_fixedocean..7
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=7
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,107,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,107,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-276) then
       ! I am fixed_ocean.initialise_fixedocean..7
       setSize=6
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,7,21,22,23,25/)
       ins=(/0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0/)
       myID=7
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,276,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,276,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==25) then
       ! I am model=fixed_ocean and ep=fixedocean and instance=
       if (arg2==-105) then
       ! I am fixed_ocean.fixedocean..25
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=25
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,105,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,105,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-276) then
       ! I am fixed_ocean.fixedocean..25
       setSize=6
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,7,21,22,23,25/)
       ins=(/0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0/)
       myID=25
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,276,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,276,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-107) then
       ! I am fixed_ocean.fixedocean..25
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,7,15,21,25,30/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=25
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,107,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,107,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_fixed_ocean
