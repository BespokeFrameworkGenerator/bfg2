       module BFG2Target1
       use bfg_averages, only : bfg_averages_ini_averages_init=>ini_averages,&
bfg_averages_write_averages_iteration=>write_averages
       use initialise_fixedicesheet_mod, only : initialise_fixedicesheet_mod_initialise_fixedicesheet_init=>initialise_fixedicesheet
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       !its2
       integer, target :: its2
       !its3
       integer, target :: its3
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function bfg_averagesThread()
       implicit none
       ! only one thread so always true
       bfg_averagesThread=.true.
       end function bfg_averagesThread
       logical function fixed_chemistryThread()
       implicit none
       ! only one thread so always true
       fixed_chemistryThread=.true.
       end function fixed_chemistryThread
       logical function fixed_oceanThread()
       implicit none
       ! only one thread so always true
       fixed_oceanThread=.true.
       end function fixed_oceanThread
       logical function fixed_icesheetThread()
       implicit none
       ! only one thread so always true
       fixed_icesheetThread=.true.
       end function fixed_icesheetThread
       logical function initialise_fixedicesheet_modThread()
       implicit none
       ! only one thread so always true
       initialise_fixedicesheet_modThread=.true.
       end function initialise_fixedicesheet_modThread
       logical function slab_seaiceThread()
       implicit none
       ! only one thread so always true
       slab_seaiceThread=.true.
       end function slab_seaiceThread
       logical function counterinst1Thread()
       implicit none
       ! only one thread so always true
       counterinst1Thread=.true.
       end function counterinst1Thread
       logical function counterinst2Thread()
       implicit none
       ! only one thread so always true
       counterinst2Thread=.true.
       end function counterinst2Thread
       logical function counterinst3Thread()
       implicit none
       ! only one thread so always true
       counterinst3Thread=.true.
       end function counterinst3Thread
       logical function counterinst4Thread()
       implicit none
       ! only one thread so always true
       counterinst4Thread=.true.
       end function counterinst4Thread
       logical function counterinst5Thread()
       implicit none
       ! only one thread so always true
       counterinst5Thread=.true.
       end function counterinst5Thread
       logical function transformer1Thread()
       implicit none
       ! only one thread so always true
       transformer1Thread=.true.
       end function transformer1Thread
       logical function transformer2Thread()
       implicit none
       ! only one thread so always true
       transformer2Thread=.true.
       end function transformer2Thread
       logical function transformer3Thread()
       implicit none
       ! only one thread so always true
       transformer3Thread=.true.
       end function transformer3Thread
       logical function transformer4Thread()
       implicit none
       ! only one thread so always true
       transformer4Thread=.true.
       end function transformer4Thread
       logical function transformer5Thread()
       implicit none
       ! only one thread so always true
       transformer5Thread=.true.
       end function transformer5Thread
       logical function transformer6Thread()
       implicit none
       ! only one thread so always true
       transformer6Thread=.true.
       end function transformer6Thread
       logical function transformer7Thread()
       implicit none
       ! only one thread so always true
       transformer7Thread=.true.
       end function transformer7Thread
       logical function igcm_atmosphereThread()
       implicit none
       ! only one thread so always true
       igcm_atmosphereThread=.true.
       end function igcm_atmosphereThread
       logical function counter_modinst1Thread()
       implicit none
       ! only one thread so always true
       counter_modinst1Thread=.true.
       end function counter_modinst1Thread
       logical function counter_modinst2Thread()
       implicit none
       ! only one thread so always true
       counter_modinst2Thread=.true.
       end function counter_modinst2Thread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
