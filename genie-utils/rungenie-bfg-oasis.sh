#!/bin/bash

# #####################################################################
#                                                                     #
# Script runs the BFG examples of genie, tested agains revision 3351. #
#                                                                     #
# Author : CWA                                                        #
# Last update: 23/11/07,CWA                                           #
#                                                                     #
#######################################################################

trap "cleanall; restore; exit" 1 2 3 6 15

# configs correspond to those in genie-main/configs
# IRH MOD
#CONFIGS="ig_go_sl_dyex_test-oasis"
CONFIGS="ig_fi_sl_dyex-oasis"
#CONFIGS="ig_fi_sl_dyex ig_sl_sl_noflux ig_go_sl_dyex_test" # ig_fi_sl_dyex ig_sl_sl_noflux ig_go_sl_dyex_test eb_go_gs_ac_bg_test

# see README file for the meaning of these deployment types
# IRH MOD
#DEPLOYMENT="s3du"
DEPLOYMENT="s2du"
#DEPLOYMENT="s2su"
#DEPLOYMENT="s1su s2su s2du s3su s3du c2su c2du c3su c3du" #"s1su s3su s3du c3su c3du" #"s1su s2su s2du s3su s3du c2su c2du c3su c3du" 

# number of runs of each config/deployment to perform
RUNS="1" #1 2 3...

CODEDIR=$HOME # location of genie
OUTDIR=$HOME/genie_output # output of genie
# true = replace makefile.arc at end of run; false = don't... 
# (replacing makefile.arc initiates a total recompilation on next run)
TOTAL_RESTORE='false' 
# the location of this script
BFG2RUN=$PWD
BFG2ROOT=$HOME/bfg2
TONULL=">& /dev/null"

# IRH MOD: I am using MPI1, Chris was using MPI2.  Therefore I use
# mpirun, not mpiexec.  Here it is:
MPIRUN=/home/ian/software/mpich-1.2.7/bin/mpirun

# IRH MOD: OASIS4 variables
# Location of OASIS4 exe in the PRISM SCE
PRISM_HOME=$HOME/prism/OASIS4/prism
oa4_dir=$PRISM_HOME/Linux/bin
oa4_exe=oasis4.MPI1.x

# ------------------ functions --------------------------------

printresults (){
 echo 'Wall clock time: config, deployment, run, time'
 grep 'real' $BFG2RUN/performance/$DATE/*/*/time.* | sed 's/real *//' | sed 's/.*\/\(.*\)\/\([cs][123][sd]u\)\/.*time\.\(.*\):\(.*\)/\1 \2 \3 \4/'
 echo 'CPU time: config, deployment, run, [proc id], time'
 grep 'cpu time' $BFG2RUN/performance/$DATE/*/*/out.* | sed 's/.*\/\(.*\)\/\([cs][123][sd]u\)\/out.\(.*\):\(.\).*cpu time= \(.*\)/\1 \2 \3 \4 \5/'
}

cleanrun () {
  cd $CODEDIR/genie/genie-main
  eval rm -f BFG2* bfg2* genie*.exe
# IRH MOD: DEBUG: Leave everything in the output dir so we can find out what went wrong
#  eval rm -fr $OUTDIR/*
  echo 'make clean'
  eval make clean >& /dev/null
}

cleanall () {
 cleanrun
 eval rm -f $CODEDIR/genie/genie-main/bfg_averages.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/counter.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/counter_mod.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/new_transformer* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/bfg_increment_genie_clock.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/copy_albedo.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/copy_dummy.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/copy_tstar.* $TONULL
 # IRH MOD: Remove goldstein_latitudes function if present
 eval rm -f $CODEDIR/genie/genie-main/goldstein_latitudes.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/ini_weights-bfg.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/interp_ocn_atm-bfg.* $TONULL
 eval rm -f $CODEDIR/genie/genie-main/weight_check.* $TONULL
}

restore(){
 eval mv $CODEDIR/genie/genie-igcm3/src/fortran/initialise_atmos.F.orig $CODEDIR/genie/genie-igcm3/src/fortran/initialise_atmos.F $TONULL
 eval mv $CODEDIR/genie/genie-main/genie_example.job.orig $CODEDIR/genie/genie-main/genie_example.job $TONULL
 eval mv $CODEDIR/genie/genie-main/configs/genie_eb_go_gs_ac_bg_test.config.orig $CODEDIR/genie/genie-main/configs/genie_eb_go_gs_ac_bg_test.config
 eval mv $CODEDIR/genie/genie-main/user.mak.orig $CODEDIR/genie/genie-main/user.mak $TONULL
 if [ $TOTAL_RESTORE = 'true' ] ; then
  eval mv $CODEDIR/genie/genie-main/makefile.arc.orig $CODEDIR/genie/genie-main/makefile.arc $TONULL
 else
  echo 'Warning: Not replacing original makefile.arc in order to avoid total recompilation'
 fi
}


# ------------------------------------------ START HERE ------------------------------------------

# Get command line options
#while getopts "pgof" flag; do
while getopts "pgo" flag; do
 case $flag in
  p ) PERF="true";; # gather performance results
  g ) GEN="true";;  # generate BFG code anew
  o ) OASISVIS="-o";; # turn on OASIS visualisation 
#  f ) PROFILE="true";; # this is a gprof profiling build
 esac
done

# IRH MOD: Added, for profiling with GPROF
if [ $PROFILE ] ; then
 export GMON_OUT_PREFIX=genie
fi

if [ $PERF ] ; then
 DATE=$(date '+%H.%M.%S-%d%b%y')
 echo "Results will be put in dir performance/$DATE"
 eval mkdir -p $BFG2RUN/performance/$DATE
fi

# prevent genie_example.job from running the executable(s) - this script will do so
echo 'patching no run option into genie_example.job'
patch -b $CODEDIR/genie/genie-main/genie_example.job $BFG2RUN/patch/genie_example.job.patch

# flag to keep track of when changes required to igcm have been made
ig_changes="false"

# loop over GENIE configurations to run
for config in $CONFIGS
do

 # IRH MOD: OASIS4 only - TODO: should do for all oasis configs
 if [ $config = 'ig_fi_sl_dyex-oasis' ] ; then
  if [ ! -e configs/genie_ig_fi_sl_dyex-oasis.config ] ; then
   # this is to maintain config filename consistency
   cp $CODEDIR/genie/genie-main/configs/genie_ig_fi_sl_dyex.config $CODEDIR/genie/genie-main/configs/genie_ig_fi_sl_dyex-oasis.config
   sed -i 's/genie_ig_fi_sl_dyex/genie_ig_fi_sl_dyex-oasis/g' $CODEDIR/genie/genie-main/configs/genie_ig_fi_sl_dyex-oasis.config
  fi
 fi

 if [ $config = 'ig_go_sl_dyex_test-oasis' ] ; then
  if [ ! -e configs/genie_ig_go_sl_dyex_test-oasis.config ] ; then
   # this is to maintain config filename consistency
   cp $CODEDIR/genie/genie-main/configs/genie_ig_go_sl.config $CODEDIR/genie/genie-main/configs/genie_ig_go_sl_dyex_test-oasis.config
   sed -i 's/genie_ig_go_sl_dyex_test/genie_ig_go_sl_dyex_test-oasis/g' $CODEDIR/genie/genie-main/configs/genie_ig_go_sl_dyex_test-oasis.config
  fi
 fi
 #if [ $config = 'ig_go_sl_dyex_test' ] ; then
 # if [ ! -e configs/genie_ig_go_sl_dyex_test.config ] ; then
 #  # this is to maintain config filename consistency
 #  cp $CODEDIR/genie/genie-main/configs/genie_ig_go_sl.config $CODEDIR/genie/genie-main/configs/genie_ig_go_sl_dyex_test.config
 # fi
 #fi

 # changes required to run igcm atmosphere configs
 if [ ${config:0:2} = 'ig' -a $ig_changes  = "false" ] ; then
  echo 'patching initialise_atmos.F - commenting out call to genie_restarts (which uses global data)'
  patch -b $CODEDIR/genie/genie-igcm3/src/fortran/initialise_atmos.F $BFG2RUN/patch/initialise_atmos.F.patch
  eval cp $BFG2RUN/code/bfg_averages.f90 $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/counter.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/counter_mod.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/new_transformer*.F $CODEDIR/genie/genie-main
  ig_changes="true"
 fi

 # further specific changes required to run ig_go_sl_dyex_test config
 # IRH MOD: ignore any suffix, e.g. '-oasis'
 # I've also added goldstein_latitudes.  TODO: Note that for BFG-OASIS,
 # ini_weights, weight_check etc. aren't required. I should really stop
 # copying them if it's an OASIS build (they're not in the makefile
 # so nothing happens to them).
 #if [ $config = 'ig_go_sl_dyex_test' ] ; then
 if [ ${config:0:18} = 'ig_go_sl_dyex_test' ] ; then
  eval cp $BFG2RUN/code/copy_albedo.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/copy_dummy.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/copy_tstar.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/goldstein_latitudes.f90 $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/ini_weights-bfg.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/interp_ocn_atm-bfg.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/weight_check.F $CODEDIR/genie/genie-main
 fi

 # specific changes required to run eb_go_gs_ac_bg_test config
 if [ $config = 'eb_go_gs_ac_bg_test' ] ; then
  eval cp $BFG2RUN/code/counter.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/weight_check.F $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/code/bfg_increment_genie_clock.F $CODEDIR/genie/genie-main
  patch -b $CODEDIR/genie/genie-main/configs/genie_eb_go_gs_ac_bg_test.config $BFG2RUN/patch/genie_eb_go_gs_ac_bg_test.config.patch
 fi

 if [ $PERF ] ; then
  eval mkdir -p $BFG2RUN/performance/$DATE/$config
 fi

 # deployment choices
 for dep in $DEPLOYMENT
 do

  # IRH COMMENT: I think this applies to OASIS configs too - they should be using mpif90
  if [ ${dep:1:1} -gt 1 ]; then
   # IRH MOD: use OASIS-specific makefile.arc and user.mak patches 
   # if the makefile has no mpich f90 compile script option, add it
   grep -q mpif90 $CODEDIR/genie/genie-main/makefile.arc
   if [ $? = "1" ] ; then
    echo 'patching mpif90 option and oasisvis libs into makefile.arc'
    patch -b $CODEDIR/genie/genie-main/makefile.arc $BFG2RUN/patch/makefile.arc.oasis.patch
   fi
   grep -q mpif90 $CODEDIR/genie/genie-main/user.mak
   if [ $? = "1" ] ; then
    echo 'patching mpif90 option into user.mak'
    patch -b $CODEDIR/genie/genie-main/user.mak $BFG2RUN/patch/user.mak.oasis.patch
   fi
  fi

  # IRH TODO: Needs modification for code generating OASIS deployments
  if [ $GEN ] ; then
   if [ -e $BFG2ROOT/examples/genie/inputs/coupled_$config-$dep.xml ] ; then
    # generate the BFG wrapper code anew
    rm -f $BFG2RUN/$config/$dep/*
    eval $BFG2ROOT/bin/runbfg2.sh $OASISVIS -d $BFG2RUN/$config/$dep -f $BFG2ROOT/examples/genie/inputs/coupled_$config-$dep.xml
   else
    echo "Cannot generate BFG2 code for $config $dep : Metadata does not exist"
    exit
   fi
  fi

  echo 'Copying BFG wrapper code'
  eval cp $BFG2RUN/$config/$dep/BFG2* $CODEDIR/genie/genie-main
  # IRH TODO: OASIS4 only - copy scc and smiocs.  We can't prefix
  # the smioc's with "BFG2", as the smioc file name format is 
  # constrained by the PRISM developers
  # (must be [application_name]_[component_name]_smioc.xml)
  eval cp $BFG2RUN/$config/$dep/scc.xml $CODEDIR/genie/genie-main
  eval cp $BFG2RUN/$config/$dep/*smioc.xml $CODEDIR/genie/genie-main

  echo 'cd to genie-main'
  eval cd $CODEDIR/genie/genie-main

  # make sure a new build is done
  touch BFG2Main*.f90

  #Set specific options for genie_example.job
  if [ $config = "eb_go_gs_ac_bg_test" ]; then
    scriptopt="-tk"
    ./scripts/configure_biogem_test.sh $CODEDIR/genie
  fi

  echo 'Running genie_example.job'
  echo ./genie_example.job $scriptopt -n -f configs/genie_$config.config -m "-f $BFG2RUN/makefiles/makefile.$config.${dep:1}"
  ./genie_example.job $scriptopt -n -f configs/genie_$config.config -m "-f $BFG2RUN/makefiles/makefile.$config.${dep:1}"

  SUCCESS=$?

   if [ $SUCCESS = "0" ] ; then

    echo "Compilation and staging successful"
    eval cp BFG2Control.nam $OUTDIR/genie_$config
    eval cp genie*.exe $OUTDIR/genie_$config
    # IRH TODO: OASIS4 only
    eval cp scc.xml $OUTDIR/genie_$config
    eval cp *smioc.xml $OUTDIR/genie_$config
    eval cp $oa4_dir/$oa4_exe $OUTDIR/genie_$config

    eval cd $OUTDIR/genie_$config
    if [ $PERF ] ; then
     eval mkdir -p $BFG2RUN/performance/$DATE/$config/$dep
    fi

    if [ ${dep:0:1} = 'c' ] ; then
     echo cp $BFG2RUN/concurrent-namelists/$config/*.nam .
     cp $BFG2RUN/concurrent-namelists/$config/*.nam .
    fi
  
    for run in $RUNS
    do

      if [ $PERF ] ; then
       OUTFILE="> $BFG2RUN/performance/$DATE/$config/$dep/out.$run"
       PERFILE="2> $BFG2RUN/performance/$DATE/$config/$dep/time.$run"
      else
       OUTFILE=""
       PERFILE=""
      fi

      echo running $config $dep $run

      if [ $dep = "s1su" ] ; then
       echo genie.exe # sequential execution
       eval "{ eval time ./genie.exe $OUTFILE ; } $PERFILE"
      elif [ ${dep:2} = "su" ] ; then
        # IRH MOD: Use mpirun, not mpiexec, as I'm using MPICH1.2.7 (MPI1)
        # not an MPI2 implementation.
        RUN_DIR=`pwd`
        cat <<EOF> $RUN_DIR/appl-linux.conf
$HOST 0 $RUN_DIR/$oa4_exe
EOF
        count=0
        while [[ $count -lt ${dep:1:1} ]];do
          (( count += 1 ))
          cat <<EOF>> $RUN_DIR/appl-linux.conf
$HOST 1 $RUN_DIR/genie.exe
EOF
        done

        echo 'Executing the model using '$MPIRUN
        if [ "$1" = "--debug" ]; then
          echo 'Starting model in IDB'
          $MPIRUN -p4pg appl-linux.conf -dbg=idb $oa4_exe
        # IRH MOD: Added, for profiling with GPROF
        elif [ $PROFILE ] ; then
          echo $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe
          eval "{ time $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        else
          echo $MPIRUN -p4pg appl-linux.conf $oa4_exe
          eval "{ time $MPIRUN -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        fi

       #echo mpiexec -l -n ${dep:1:1} genie.exe # multiple sequence units concurrent execution (one deployment unit)
       #eval "{ eval time mpiexec -l -n ${dep:1:1} genie.exe $OUTFILE ; } $PERFILE"
      elif [ ${dep:1} = "2du" ] ; then
        # IRH MOD: Use mpirun, not mpiexec, as I'm using MPICH1.2.7 (MPI1)
        # not an MPI2 implementation.
        RUN_DIR=`pwd`
        cat <<EOF> $RUN_DIR/appl-linux.conf
$HOST 0 $RUN_DIR/$oa4_exe
EOF
        count=0
        while [[ $count -lt 2 ]];do
          (( count += 1 ))
          cat <<EOF>> $RUN_DIR/appl-linux.conf
$HOST 1 $RUN_DIR/genie$count.exe
EOF
        done

        echo 'Executing the model using '$MPIRUN
        if [ "$1" = "--debug" ]; then
          echo 'Starting model in IDB'
          $MPIRUN -p4pg appl-linux.conf -dbg=idb $oa4_exe
        # IRH MOD: Added, for profiling with GPROF
        elif [ $PROFILE ] ; then
          echo $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe # 2 deployment units
          eval "{ time $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        else
          echo $MPIRUN -p4pg appl-linux.conf $oa4_exe # 2 deployment units
          eval "{ time $MPIRUN -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        fi
       
       #echo mpiexec -l -n 1 genie1.exe : -n 1 genie2.exe # 2 deployment units
       #eval "{ time mpiexec -l -n 1 genie1.exe : -n 1 genie2.exe $OUTFILE ; } $PERFILE"
      elif [ ${dep:1} = "3du" ] ; then
        # IRH MOD: Use mpirun, not mpiexec, as I'm using MPICH1.2.7 (MPI1)
        # not an MPI2 implementation.
        RUN_DIR=`pwd`
        cat <<EOF> $RUN_DIR/appl-linux.conf
$HOST 0 $RUN_DIR/$oa4_exe
EOF
        count=0
        while [[ $count -lt 3 ]];do
          (( count += 1 ))
          cat <<EOF>> $RUN_DIR/appl-linux.conf
$HOST 1 $RUN_DIR/genie$count.exe
EOF
        done

        echo 'Executing the model using '$MPIRUN
        if [ "$1" = "--debug" ]; then
          echo 'Starting model in IDB'
          $MPIRUN -p4pg appl-linux.conf -dbg=idb $oa4_exe
        # IRH MOD: Added, for profiling with GPROF
        elif [ $PROFILE ] ; then
          echo $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe # 2 deployment units
          eval "{ time $MPIRUN -x $GMON_OUT_PREFIX -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        else
          echo $MPIRUN -p4pg appl-linux.conf $oa4_exe # 2 deployment units
          eval "{ time $MPIRUN -p4pg appl-linux.conf $oa4_exe $OUTFILE ; } $PERFILE"
        fi
       
       #echo mpiexec -l -n 1 genie1.exe : -n 1 genie2.exe : -n 1 genie3.exe # 3 deployment units
       #eval "{ time mpiexec -l -n 1 genie1.exe : -n 1 genie2.exe : -n 1 genie3.exe $OUTFILE ; } $PERFILE"
      fi

    done # done looping over runs

    if [ $OASISVIS ] ; then
     echo 'concatenating log files'
     echo '<oasisvis>' > $BFG2RUN/performance/$DATE/$config/$dep/oasisvis.xml
     cat $OUTDIR/genie_$config/oasisvis* >> $BFG2RUN/performance/$DATE/$config/$dep/oasisvis.xml
     echo '</oasisvis>' >> $BFG2RUN/performance/$DATE/$config/$dep/oasisvis.xml
    fi

   else # else if running genie_example.job not success
    echo failed to compile $config $dep
    cleanall
    restore
    exit
   fi
 
  echo 'Cleaning up run'
  cleanrun $config
  cd $BFG2RUN

 done # done looping over deployments 

 echo 'Cleaning up all'
 cleanall
 cd $BFG2RUN

done # done looping over configs

# leave genie unchanged
restore

if [ $PERF ] ; then
 printresults
fi

echo 'Finished experiments successfully'

