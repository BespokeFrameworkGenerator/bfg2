       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !slab_ocean__freq
       integer :: slab_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,slab_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !conductflux_atm
       real, dimension(1:64,1:32) :: r291
       !ksic_loop
       integer :: r295
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r121
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r389
       !istep_sic
       integer :: r767
       !netsolar_atm
       real, dimension(1:64,1:32) :: r122
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r404
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r395
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r339
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r343
       !netlong_atm
       real, dimension(1:64,1:32) :: r123
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r391
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r387
       !test_energy_seaice
       real :: r293
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r393
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r290
       !test_water_seaice
       real :: r294
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r385
       !temptop_atm
       real, dimension(1:64,1:32) :: r82
       !kocn_loop
       integer :: r397
       !tstar_atm
       real, dimension(1:64,1:32) :: r119
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r400
       !ilat1_atm
       integer :: r105
       !ilon1_atm
       integer :: r104
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101113,file='BFG2Control.nam')
       read(101113,time)
       close(101113)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent modelscoupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
           r290=0.0
           r404=0.0
       ! End P2P notation priming
       ! Begin set notationpriming
           r291=0.0
           r295=6
           r75=0
           r767=0
           r387=0.0
           r293=0.0
           r294=0.0
           r397=48
           r400=0.0
           r105=32
           r104=64
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(slab_seaiceThread())then
       call nmlinit12(r119,r381,r385,r387,r389,r339,r343,r122,r123,r391,r393,r290,r121,r395)
       end if
       ! netcdf files
       ! End initial values file read
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r119,-119)
       call get_slab_seaice(r121,-121)
       call get_slab_seaice(r290,-290)
       call get_slab_seaice(r75,-75)
       call initialise_slabseaice(r119,r121,r290,r291,r75,r293,r294,r295)
       call put_slab_seaice(r119,-119)
       call put_slab_seaice(r121,-121)
       call put_slab_seaice(r290,-290)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call counter(r767)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call get_slab_seaice(r119,-119)
       call get_slab_seaice(r381,-381)
       call get_slab_seaice(r385,-385)
       call get_slab_seaice(r387,-387)
       call get_slab_seaice(r389,-389)
       call get_slab_seaice(r339,-339)
       call get_slab_seaice(r343,-343)
       call get_slab_seaice(r122,-122)
       call get_slab_seaice(r123,-123)
       call get_slab_seaice(r391,-391)
       call get_slab_seaice(r393,-393)
       call get_slab_seaice(r290,-290)
       call get_slab_seaice(r82,-82)
       call get_slab_seaice(r121,-121)
       call get_slab_seaice(r75,-75)
       call slabseaice(r767,r119,r381,r385,r387,r389,r339,r343,r122,r123,r391,r393,r290,r82,r291,r121,r75,r293,r294,r295)
       call put_slab_seaice(r119,-119)
       call put_slab_seaice(r381,-381)
       call put_slab_seaice(r385,-385)
       call put_slab_seaice(r387,-387)
       call put_slab_seaice(r389,-389)
       call put_slab_seaice(r339,-339)
       call put_slab_seaice(r343,-343)
       call put_slab_seaice(r122,-122)
       call put_slab_seaice(r123,-123)
       call put_slab_seaice(r391,-391)
       call put_slab_seaice(r393,-393)
       call put_slab_seaice(r290,-290)
       call put_slab_seaice(r82,-82)
       call put_slab_seaice(r121,-121)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call get_transformer4(r400,-400)
       call get_transformer4(r290,-290)
       call get_transformer4(r404,-404)
       call new_transformer_4(r104,r105,r400,r290,r295,r397,r404,r291)
       call put_transformer4(r400,-400)
       call put_transformer4(r290,-290)
       call put_transformer4(r404,-404)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call get_transformer6(r381,-381)
       call get_transformer6(r385,-385)
       call get_transformer6(r387,-387)
       call get_transformer6(r389,-389)
       call get_transformer6(r391,-391)
       call get_transformer6(r393,-393)
       call get_transformer6(r395,-395)
       call new_transformer_6(r104,r105,r381,r385,r387,r389,r391,r393,r395)
       call put_transformer6(r381,-381)
       call put_transformer6(r385,-385)
       call put_transformer6(r387,-387)
       call put_transformer6(r389,-389)
       call put_transformer6(r391,-391)
       call put_transformer6(r393,-393)
       call put_transformer6(r395,-395)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit12(R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379)
        implicit none
       real, dimension(1:64,1:32) :: R105
       real, dimension(1:64,1:32) :: R365
       real, dimension(1:64,1:32) :: R369
       real, dimension(1:64,1:32) :: R371
       real, dimension(1:64,1:32) :: R373
       real, dimension(1:64,1:32) :: R323
       real, dimension(1:64,1:32) :: R327
       real, dimension(1:64,1:32) :: R108
       real, dimension(1:64,1:32) :: R109
       real, dimension(1:64,1:32) :: R375
       real, dimension(1:64,1:32) :: R377
       real, dimension(1:64,1:32) :: R274
       real, dimension(1:64,1:32) :: R107
       real, dimension(1:64,1:32) :: R379
       namelist /conc_pri/ R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379
       open(unit=12,file='conc_pri.nam')
       read(12,conc_pri)
       close(12)
       end subroutine nmlinit12
