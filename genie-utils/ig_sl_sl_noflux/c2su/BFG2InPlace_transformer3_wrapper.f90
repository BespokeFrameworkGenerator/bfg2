       ! f77 to f90 put/get wrappers start
       subroutine put_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==19) then
       if (tag==-381) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-339) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-385) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-343) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-387) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-122) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-389) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-123) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-391) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-393) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-395) then
       call putreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine put_transformer3
       subroutine get_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==19) then
       if (tag==-381) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-339) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-385) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-343) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-387) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-122) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-389) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-123) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-391) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-393) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-395) then
       call getreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine get_transformer3
       subroutine getreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_transformer3
       subroutine putreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_transformer3
       ! f77 to f90 put/get wrappers end
