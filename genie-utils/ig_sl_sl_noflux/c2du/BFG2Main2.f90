       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !slab_ocean__freq
       integer :: slab_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,slab_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_ocn
       real, dimension(1:36) :: r7
       !alat1_ocn
       real, dimension(1:36) :: r8
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r12
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_ocn
       real, dimension(1:36,1:36) :: r15
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !sensible_ocn
       real, dimension(1:36,1:36) :: r18
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_ocn
       real, dimension(1:36,1:36) :: r21
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_ocn
       real, dimension(1:36,1:36) :: r24
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_ocn
       real, dimension(1:36,1:36) :: r27
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r30
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !evap_ocn
       real, dimension(1:36,1:36) :: r33
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_ocn
       real, dimension(1:36,1:36) :: r36
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_ocn
       real, dimension(1:36,1:36) :: r39
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r42
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_ocn
       real, dimension(1:36,1:36) :: r45
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_ocn
       real, dimension(1:36,1:36) :: r48
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_ocn
       real, dimension(1:36,1:36) :: r51
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       ! Set Notation Vars
       !conductflux_atm
       real, dimension(1:64,1:32) :: r291
       !ksic_loop
       integer :: r295
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r121
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r389
       !istep_sic
       integer :: r767
       !test_energy_ocean
       real :: r84
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r419
       !netsolar_atm
       real, dimension(1:64,1:32) :: r122
       !alat1_atm
       real, dimension(1:32) :: r107
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r404
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r395
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r408
       !iconv_che
       integer :: r189
       !alon1_atm
       real, dimension(1:64) :: r106
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r427
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r339
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r343
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !dt_write
       integer :: r59
       !netlong_atm
       real, dimension(1:64,1:32) :: r123
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !istep_lic
       integer :: r2120
       !test_water_ocean
       real :: r85
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r391
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r423
       !write_flag_sic
       logical :: r3
       !write_flag_atm
       logical :: r1
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !fname_restart_main
       character(len=200) :: r58
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r425
       !istep_che
       integer :: r1669
       !outputdir_name
       character(len=200) :: r60
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r387
       !test_energy_seaice
       real :: r293
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r393
       !istep_ocn
       integer :: r1218
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r290
       !test_water_seaice
       real :: r294
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r385
       !iconv_ice
       integer :: r165
       !temptop_atm
       real, dimension(1:64,1:32) :: r82
       !kocn_loop
       integer :: r397
       !tstar_atm
       real, dimension(1:64,1:32) :: r119
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r400
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r105
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r415
       !genie_timestep
       real :: r57
       !ilon1_atm
       integer :: r104
       !write_flag_ocn
       logical :: r2
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r421
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r417
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
           r290=0.0
       ! End P2P notation priming
       ! Begin set notation priming
           r291=0.0
           r295=6
           r75=0
           r121=0.0
           r389=0.0
           r767=0
           r84=0.0
           r419=0.0
           r122=0.0
           r107=0.0
           r404=0.0
           r395=0.0
           r63=0.0
           r408=0.0
           r189=0
           r106=0.0
           r427=0.0
           r339=0.0
           r413=0.0
           r343=0.0
           r62=0.0
           r59=720
           r123=0.0
           r76=0.0
           r77=0.0
           r2120=0
           r85=0.0
           r391=0.0
           r423=0.0
           r3=.false.
           r1=.true.
           r381=0.0
           r41=0.0
           r61=0.0
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r425=0.0
           r1669=0
           r60='/home/armstroc/genie_output/genie_ig_sl_sl_noflux/main'
           r387=0.0
           r293=0.0
           r393=0.0
           r1218=0
           r290=0.0
           r294=0.0
           r385=0.0
           r165=0
           r82=0.0
           r397=48
           r119=0.0
           r400=0.0
           r78=0.0
           r105=32
           r415=0.0
           r57=3600.0
           r104=64
           r2=.true.
           r421=0.0
           r417=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(slab_seaiceThread())then
       call nmlinit12(r119,r381,r385,r387,r389,r339,r343,r122,r123,r391,r393,r290,r121,r395)
       end if
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call bfg_averages_ini_averages_init(r1,r2,r3)
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r75,r76,r77,r78)
       call put_initialise_fixedicesheet_mod(r75,-75)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call initialise_fixedchem(r61,r62,r63)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r119,-119)
       call get_slab_seaice(r121,-121)
       call get_slab_seaice(r290,-290)
       call initialise_slabseaice(r119,r121,r290,r291,r75,r293,r294,r295)
       call put_slab_seaice(r119,-119)
       call put_slab_seaice(r121,-121)
       call put_slab_seaice(r290,-290)
       end if
       if (slab_oceanThread()) then
       call setActiveModel(7)
       call get_slab_ocean(r119,-119)
       call get_slab_ocean(r121,-121)
       call get_slab_ocean(r290,-290)
       call initialise_slabocean(r119,r121,r290,r82,r75,r84,r85)
       call put_slab_ocean(r119,-119)
       call put_slab_ocean(r121,-121)
       call put_slab_ocean(r290,-290)
       call put_slab_ocean(r75,-75)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call counter(r767)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call get_slab_seaice(r119,-119)
       call get_slab_seaice(r381,-381)
       call get_slab_seaice(r385,-385)
       call get_slab_seaice(r387,-387)
       call get_slab_seaice(r389,-389)
       call get_slab_seaice(r339,-339)
       call get_slab_seaice(r343,-343)
       call get_slab_seaice(r122,-122)
       call get_slab_seaice(r123,-123)
       call get_slab_seaice(r391,-391)
       call get_slab_seaice(r393,-393)
       call get_slab_seaice(r290,-290)
       call get_slab_seaice(r121,-121)
       call slabseaice(r767,r119,r381,r385,r387,r389,r339,r343,r122,r123,r391,r393,r290,r82,r291,r121,r75,r293,r294,r295)
       call put_slab_seaice(r119,-119)
       call put_slab_seaice(r381,-381)
       call put_slab_seaice(r385,-385)
       call put_slab_seaice(r387,-387)
       call put_slab_seaice(r389,-389)
       call put_slab_seaice(r339,-339)
       call put_slab_seaice(r343,-343)
       call put_slab_seaice(r122,-122)
       call put_slab_seaice(r123,-123)
       call put_slab_seaice(r391,-391)
       call put_slab_seaice(r393,-393)
       call put_slab_seaice(r290,-290)
       call put_slab_seaice(r121,-121)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call get_transformer4(r290,-290)
       call new_transformer_4(r104,r105,r400,r290,r295,r397,r404,r291)
       call put_transformer4(r290,-290)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(24)
       call counter(r1218)
       end if
       end if
       if (slab_oceanThread()) then
       if(mod(its1,slab_ocean__freq).eq.0)then
       call setActiveModel(25)
       call get_slab_ocean(r119,-119)
       call get_slab_ocean(r408,-408)
       call get_slab_ocean(r413,-413)
       call get_slab_ocean(r415,-415)
       call get_slab_ocean(r417,-417)
       call get_slab_ocean(r419,-419)
       call get_slab_ocean(r421,-421)
       call get_slab_ocean(r423,-423)
       call get_slab_ocean(r425,-425)
       call get_slab_ocean(r427,-427)
       call get_slab_ocean(r290,-290)
       call get_slab_ocean(r121,-121)
       call slabocean(r1218,r119,r408,r413,r415,r417,r419,r421,r423,r425,r427,r290,r82,r404,r121,r75,r84,r85)
       call put_slab_ocean(r119,-119)
       call put_slab_ocean(r408,-408)
       call put_slab_ocean(r413,-413)
       call put_slab_ocean(r415,-415)
       call put_slab_ocean(r417,-417)
       call put_slab_ocean(r419,-419)
       call put_slab_ocean(r421,-421)
       call put_slab_ocean(r423,-423)
       call put_slab_ocean(r425,-425)
       call put_slab_ocean(r427,-427)
       call put_slab_ocean(r290,-290)
       call put_slab_ocean(r121,-121)
       call put_slab_ocean(r75,-75)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(26)
       call counter(r1669)
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(27)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r189,-189)
       call fixedchem(r1669,r61,r62,r63,r189)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r189,-189)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(28)
       call counter(r2120)
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(29)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r165,-165)
       call fixedicesheet(r2120,r75,r76,r77,r78,r165)
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r165,-165)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(30)
       call get_bfg_averages(r106,-106)
       call get_bfg_averages(r107,-107)
       call get_bfg_averages(r415,-415)
       call get_bfg_averages(r417,-417)
       call get_bfg_averages(r413,-413)
       call get_bfg_averages(r408,-408)
       call get_bfg_averages(r419,-419)
       call get_bfg_averages(r421,-421)
       call get_bfg_averages(r425,-425)
       call get_bfg_averages(r423,-423)
       call get_bfg_averages(r427,-427)
       call get_bfg_averages(r119,-119)
       call get_bfg_averages(r121,-121)
       call bfg_averages_write_averages_iteration(r1218,r106,r107,r7,r8,r9,r10,r415,r12,r13,r417,r15,r16,r413,r18,r19,r408,r21,r22,r419,r24,r25,r421,r27,r28,r404,r30,r31,r425,r33,r34,r423,r36,r37,r427,r39,r40,r41,r42,r43,r400,r45,r46,r119,r48,r49,r121,r51,r52,r1,r2,r3,r397,r57,r58,r59,r60)
       call put_bfg_averages(r106,-106)
       call put_bfg_averages(r107,-107)
       call put_bfg_averages(r415,-415)
       call put_bfg_averages(r417,-417)
       call put_bfg_averages(r413,-413)
       call put_bfg_averages(r408,-408)
       call put_bfg_averages(r419,-419)
       call put_bfg_averages(r421,-421)
       call put_bfg_averages(r425,-425)
       call put_bfg_averages(r423,-423)
       call put_bfg_averages(r427,-427)
       call put_bfg_averages(r119,-119)
       call put_bfg_averages(r121,-121)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call get_transformer6(r381,-381)
       call get_transformer6(r385,-385)
       call get_transformer6(r387,-387)
       call get_transformer6(r389,-389)
       call get_transformer6(r391,-391)
       call get_transformer6(r393,-393)
       call get_transformer6(r395,-395)
       call new_transformer_6(r104,r105,r381,r385,r387,r389,r391,r393,r395)
       call put_transformer6(r381,-381)
       call put_transformer6(r385,-385)
       call put_transformer6(r387,-387)
       call put_transformer6(r389,-389)
       call put_transformer6(r391,-391)
       call put_transformer6(r393,-393)
       call put_transformer6(r395,-395)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(32)
       call get_transformer7(r408,-408)
       call get_transformer7(r413,-413)
       call get_transformer7(r415,-415)
       call get_transformer7(r417,-417)
       call get_transformer7(r419,-419)
       call get_transformer7(r421,-421)
       call get_transformer7(r423,-423)
       call get_transformer7(r425,-425)
       call get_transformer7(r427,-427)
       call new_transformer_7(r104,r105,r408,r413,r415,r417,r419,r421,r423,r425,r427,r400,r404,r41)
       call put_transformer7(r408,-408)
       call put_transformer7(r413,-413)
       call put_transformer7(r415,-415)
       call put_transformer7(r417,-417)
       call put_transformer7(r419,-419)
       call put_transformer7(r421,-421)
       call put_transformer7(r423,-423)
       call put_transformer7(r425,-425)
       call put_transformer7(r427,-427)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit12(R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379)
        implicit none
       real, dimension(1:64,1:32) :: R105
       real, dimension(1:64,1:32) :: R365
       real, dimension(1:64,1:32) :: R369
       real, dimension(1:64,1:32) :: R371
       real, dimension(1:64,1:32) :: R373
       real, dimension(1:64,1:32) :: R323
       real, dimension(1:64,1:32) :: R327
       real, dimension(1:64,1:32) :: R108
       real, dimension(1:64,1:32) :: R109
       real, dimension(1:64,1:32) :: R375
       real, dimension(1:64,1:32) :: R377
       real, dimension(1:64,1:32) :: R274
       real, dimension(1:64,1:32) :: R107
       real, dimension(1:64,1:32) :: R379
       namelist /conc_pri/ R105,R365,R369,R371,R373,R323,R327,R108,R109,R375,R377,R274,R107,R379
       open(unit=12,file='conc_pri.nam')
       read(12,conc_pri)
       close(12)
       end subroutine nmlinit12
