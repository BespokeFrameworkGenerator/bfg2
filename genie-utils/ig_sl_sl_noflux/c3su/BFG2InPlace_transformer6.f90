       module BFG2InPlace_transformer6
       use BFG2Target1
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__2
       end interface
       interface put
       module procedure putreal__2
       end interface
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(7) :: pp2p=(/0,0,0,0,0,0,1/)
       currentModel=getActiveModel()
       if (currentModel==31) then
       ! I am model=transformer6 and ep=new_transformer_6 and instance=
       if (arg2==-381) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receivefrom remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,381,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-385) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,385,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-387) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,387,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-389) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,389,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-391) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,391,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-393) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/19,21,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive fromremote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,393,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-395) then
       ! I am transformer6.new_transformer_6..31
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/19,31/)
       myID=31
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,395,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==31) then
       ! I am model=transformer6 and ep=new_transformer_6 and instance=
       if (arg2==-381) then
       ! Iam transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,381,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,381,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-385) then
       ! I am transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,385,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send toremote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,385,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-387) then
       ! I am transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,387,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,387,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-389) then
       ! I am transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,389,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,389,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-391) then
       ! I am transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,391,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,391,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-393) then
       ! I am transformer6.new_transformer_6..31
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,21,31/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,393,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,393,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-395) then
       ! I am transformer6.new_transformer_6..31
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/19,31/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=31
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,395,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,395,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_transformer6
