       ! f77 to f90 put/get wrappers start
       subroutine put_slab_seaice(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void*
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-119) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-121) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-290) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==21) then
       if (tag==-119) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-381) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-385) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-387) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-389) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-339) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-343) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-122) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-123) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-391) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-393) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-290) then
       call putreal__2_slab_seaice(data,tag)
       end if
       if (tag==-121) then
       call putreal__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine put_slab_seaice
       subroutine get_slab_seaice(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==6) then
       if (tag==-119) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-121) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-290) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       if (currentModel==21) then
       if (tag==-119) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-381) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-385) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-387) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-389) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-339) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-343) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-122) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-123) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-391) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-393) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-290) then
       call getreal__2_slab_seaice(data,tag)
       end if
       if (tag==-121) then
       call getreal__2_slab_seaice(data,tag)
       end if
       end if
       end subroutine get_slab_seaice
       subroutine getreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_slab_seaice
       subroutine putreal__2_slab_seaice(data,tag)
       use BFG2InPlace_slab_seaice, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_slab_seaice
       ! f77 to f90 put/get wrappers end
