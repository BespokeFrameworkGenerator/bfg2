       ! f77 to f90 put/get wrappers start
       subroutine put_slab_ocean(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==7) then
       if (tag==-119) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-121) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-290) then
       call putreal__2_slab_ocean(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-119) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-408) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-413) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-415) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-417) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-419) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-421) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-423) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-425) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-427) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-290) then
       call putreal__2_slab_ocean(data,tag)
       end if
       if (tag==-121) then
       call putreal__2_slab_ocean(data,tag)
       end if
       end if
       if (currentModel==7) then
       if (tag==-75) then
       call putinteger__2_slab_ocean(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-75) then
       call putinteger__2_slab_ocean(data,tag)
       end if
       end if
       end subroutine put_slab_ocean
       subroutine get_slab_ocean(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==7) then
       if (tag==-119) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-121) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-290) then
       call getreal__2_slab_ocean(data,tag)
       end if
       end if
       if (currentModel==25) then
       if (tag==-119) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-408) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-413) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-415) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-417) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-419) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-421) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-423) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-425) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-427) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-290) then
       call getreal__2_slab_ocean(data,tag)
       end if
       if (tag==-121) then
       call getreal__2_slab_ocean(data,tag)
       end if
       end if
       end subroutine get_slab_ocean
       subroutine getreal__2_slab_ocean(data,tag)
       use BFG2InPlace_slab_ocean, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_slab_ocean
       subroutine putreal__2_slab_ocean(data,tag)
       use BFG2InPlace_slab_ocean, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_slab_ocean
       subroutine putinteger__2_slab_ocean(data,tag)
       use BFG2InPlace_slab_ocean, only : put=>putinteger__2
       implicit none
       integer , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__2_slab_ocean
       ! f77 to f90 put/getwrappers end
