       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !slab_ocean__freq
       integer :: slab_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,slab_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Pointand Uncoupled Vars
       !alon2_atm
       real, dimension(1:64) :: r108
       !alat2_atm
       real, dimension(1:32) :: r109
       !aboxedge1_lon_atm
       real, dimension(1:65) :: r110
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r111
       !aboxedge1_lat_atm
       real, dimension(1:33) :: r112
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r113
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r115
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r132
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r133
       !hrlons_atm
       real, dimension(1:320) :: r146
       !hrlats_atm
       real, dimension(1:160) :: r147
       !hrlonsedge_atm
       real, dimension(1:321) :: r148
       !hrlatsedge_atm
       real, dimension(1:161) :: r149
       !u10m_atm
       real, dimension(1:64,1:32) :: r156
       !v10m_atm
       real, dimension(1:64,1:32) :: r157
       !q2m_atm
       real, dimension(1:64,1:32) :: r159
       !rh2m_atm
       real, dimension(1:64,1:32) :: r160
       !psigma
       real, dimension(1:7) :: r164
       !massair
       real, dimension(1:64,1:32,1:7) :: r167
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r192
       !test_water_land
       real :: r236
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r158
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r284
       !surfdsigma
       real :: r163
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r225
       !ksic_loop
       integer :: r295
       !precip_atm
       real, dimension(1:64,1:32) :: r124
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r139
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r121
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r389
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r278
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r419
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r355
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r131
       !netsolar_atm
       real, dimension(1:64,1:32) :: r122
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r332
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r330
       !alat1_atm
       real, dimension(1:32) :: r107
       !klnd_loop
       integer :: r129
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r229
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r352
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r395
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r227
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r166
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r120
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r336
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r154
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r281
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r408
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r276
       !atmos_dt_tim
       real :: r118
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r280
       !iconv_che
       integer :: r189
       !alon1_atm
       real, dimension(1:64) :: r106
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r287
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r138
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r275
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r427
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r339
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r343
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r283
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r151
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r221
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r277
       !netlong_atm
       real, dimension(1:64,1:32) :: r123
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r333
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !water_flux_atmos
       real :: r144
       !land_niter_tim
       integer :: r116
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r153
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r391
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r152
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r423
       !katm_loop
       integer :: r383
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r335
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r224
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r329
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r228
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r331
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r346
       !istep_atm
       integer :: r316
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r425
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r361
       !land_rough_atm
       real, dimension(1:64,1:32) :: r232
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r137
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r387
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r393
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r290
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r334
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r385
       !iconv_ice
       integer :: r165
       !surfsigma
       real :: r162
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r328
       !kocn_loop
       integer :: r397
       !glim_covmap
       real, dimension(1:64,1:32) :: r145
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r134
       !tstar_atm
       real, dimension(1:64,1:32) :: r119
       !land_latent_atm
       real, dimension(1:64,1:32) :: r226
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r279
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r223
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r286
       !surf_iter_tim_bfg_land
       integer :: r317
       !surf_iter_tim_bfg_ocean
       integer :: r768
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r274
       !ocean_niter_tim
       integer :: r117
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r349
       !land_evap_atm
       real, dimension(1:64,1:32) :: r230
       !lgraphics
       logical :: r125
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r105
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r285
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r415
       !ilon1_atm
       integer :: r104
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r130
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r231
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r358
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r233
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r150
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r222
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r421
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r417
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r327
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r282
       !flag_land
       logical :: r128
       !flag_goldsic
       logical :: r127
       !flag_goldocn
       logical :: r126
       !glim_coupled
       logical :: r244
       !glim_snow_model
       logical :: r243
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       ! Init model datastructures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notationpriming
           r284=0.0
           r163=0.0
           r225=0.0
           r295=6
           r124=0.0
           r139=0.0
           r75=0
           r121=0.0
           r389=0.0
           r278=0.0
           r419=0.0
           ! 355is allocatable; cannot be written to yet
           r131=0.0
           r122=0.0
           ! 332is allocatable; cannot be written to yet
           ! 330is allocatable; cannot be written to yet
           r107=0.0
           r129=1
           r229=0.0
           r352=0.0
           r395=0.0
           r227=0.0
           r166=0.0
           ! 120is allocatable; cannot be written to yet
           r63=0.0
           ! 336is allocatable; cannot be written to yet
           r154=0.0
           r281=0.0
           r408=0.0
           r276=0.0
           r118=0.0
           r280=0.0
           r189=0
           r106=0.0
           r287=0.0
           r138=0.0
           r275=0.0
           r427=0.0
           r339=0.0
           r413=0.0
           r343=0.0
           r283=0.0
           r62=0.0
           r151=0.0
           r221=0.0
           r277=0.0
           r123=0.0
           ! 333is allocatable; cannot be written to yet
           r76=0.0
           r77=0.0
           r144=0.0
           r116=0
           r153=0.0
           r391=0.0
           r152=0.0
           r423=0.0
           r383=1
           ! 335is allocatable; cannot be written to yet
           r224=0.0
           r381=0.0
           ! 329is allocatable; cannot be written to yet
           r228=0.0
           r61=0.0
           ! 331is allocatable; cannot be written to yet
           r346=0.0
           r316=0
           r425=0.0
           ! 361is allocatable; cannot be written to yet
           r232=0.0
           r137=0.0
           r387=0.0
           r393=0.0
           r290=0.0
           ! 334is allocatable; cannot be written to yet
           r385=0.0
           r165=0
           r162=0.0
           ! 328is allocatable; cannot be written to yet
           r397=48
           r145=0.0
           ! 134is allocatable; cannot be written to yet
           r119=0.0
           r226=0.0
           r279=0.0
           r223=0.0
           r286=0.0
           r317=0
           r768=0
           r274=0.0
           r117=0
           r349=0.0
           r230=0.0
           r125=.false.
           r78=0.0
           r105=32
           r285=0.0
           r415=0.0
           r104=64
           ! 130is allocatable; cannot be written to yet
           r231=0.0
           ! 358is allocatable; cannot be written to yet
           r233=0.0
           r150=0.0
           r222=0.0
           r421=0.0
           r417=0.0
           ! 327is allocatable; cannot be written to yet
           r282=0.0
           r128=.false.
           r127=.false.
           r126=.false.
           r244=.true.
           r243=.false.
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call initialise_igcmsurf()
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r106,-106)
       call get_igcm_atmosphere(r107,-107)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r119,-119)
       call get_igcm_atmosphere(r121,-121)
       call get_igcm_atmosphere(r122,-122)
       call get_igcm_atmosphere(r123,-123)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r132))) then
       allocate(r132(1:r104,1:r105))
       end if
       if (.not.(allocated(r133))) then
       allocate(r133(1:r104,1:r105))
       end if
       if (.not.(allocated(r120))) then
       allocate(r120(1:r104,1:r105))
       r120=0.0
       end if
       if (.not.(allocated(r134))) then
       allocate(r134(1:r104,1:r105))
       r134=0.0
       end if
       if (.not.(allocated(r130))) then
       allocate(r130(1:r104,1:r105))
       r130=0.0
       end if
       call initialise_atmos(r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r75,r115,r116,r117,r118,r119,r120,r121,r122,r123,r124,r125,r126,r127,r128,r129,r130,r131,r132,r133,r134,r76,r78,r137,r138,r139,r77,r61,r62,r63,r144,r145,r146,r147,r148,r149)
       call put_igcm_atmosphere(r106,-106)
       call put_igcm_atmosphere(r107,-107)
       call put_igcm_atmosphere(r119,-119)
       call put_igcm_atmosphere(r121,-121)
       call put_igcm_atmosphere(r122,-122)
       call put_igcm_atmosphere(r123,-123)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(8)
       call counter(r316)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(9)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r165,-165)
       call igcm3_adiab(r150,r151,r152,r153,r154,r130,r156,r157,r158,r159,r160,r76,r162,r163,r164,r165,r166,r167)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r165,-165)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(10)
       if (.not.(allocated(r332))) then
       allocate(r332(1:r104,1:r105))
       r332=0.0
       end if
       if (.not.(allocated(r330))) then
       allocate(r330(1:r104,1:r105))
       r330=0.0
       end if
       if (.not.(allocated(r336))) then
       allocate(r336(1:r104,1:r105))
       r336=0.0
       end if
       if (.not.(allocated(r333))) then
       allocate(r333(1:r104,1:r105))
       r333=0.0
       end if
       if (.not.(allocated(r335))) then
       allocate(r335(1:r104,1:r105))
       r335=0.0
       end if
       if (.not.(allocated(r329))) then
       allocate(r329(1:r104,1:r105))
       r329=0.0
       end if
       if (.not.(allocated(r331))) then
       allocate(r331(1:r104,1:r105))
       r331=0.0
       end if
       if (.not.(allocated(r334))) then
       allocate(r334(1:r104,1:r105))
       r334=0.0
       end if
       if (.not.(allocated(r328))) then
       allocate(r328(1:r104,1:r105))
       r328=0.0
       end if
       if (.not.(allocated(r327))) then
       allocate(r327(1:r104,1:r105))
       r327=0.0
       end if
       call new_transformer_1(r104,r105,r150,r151,r152,r153,r130,r154,r327,r328,r329,r330,r331,r332,r333,r334,r335,r336)
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(11)
       call counter_mod(r317,r116)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(12)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r122,-122)
       call get_igcm_atmosphere(r123,-123)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call igcm_land_surflux(r316,r317,r116,r118,r162,r75,r122,r123,r124,r327,r328,r329,r330,r154,r221,r222,r223,r224,r225,r226,r227,r228,r229,r230,r231,r232,r233,r134,r131,r236,r78,r137,r138,r139,r77,r145,r243,r244)
       call put_igcm_atmosphere(r122,-122)
       call put_igcm_atmosphere(r123,-123)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(13)
       call get_igcm_atmosphere(r75,-75)
       call igcm_land_blayer(r317,r116,r118,r163,r75,r222,r223,r224,r225,r327,r328,r329,r330,r154)
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(14)
       call counter_mod(r768,r117)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call get_igcm_atmosphere(r75,-75)
       call get_igcm_atmosphere(r122,-122)
       call get_igcm_atmosphere(r123,-123)
       call get_igcm_atmosphere(r121,-121)
       call get_igcm_atmosphere(r119,-119)
       call igcm_ocean_surflux(r316,r768,r117,r162,r75,r122,r123,r124,r331,r332,r333,r334,r154,r121,r119,r274,r275,r276,r277,r278,r279,r280,r281,r282,r283,r284,r285,r286,r287)
       call put_igcm_atmosphere(r122,-122)
       call put_igcm_atmosphere(r123,-123)
       call put_igcm_atmosphere(r121,-121)
       call put_igcm_atmosphere(r119,-119)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(16)
       call get_igcm_atmosphere(r75,-75)
       call igcm_ocean_blayer(r768,r117,r118,r163,r75,r275,r276,r277,r278,r331,r332,r333,r334,r154)
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(17)
       call get_transformer2(r339,-339)
       call get_transformer2(r75,-75)
       call get_transformer2(r343,-343)
       if (.not.(allocated(r355))) then
       allocate(r355(1:r104,1:r105))
       r355=0.0
       end if
       if (.not.(allocated(r361))) then
       allocate(r361(1:r104,1:r105))
       r361=0.0
       end if
       if (.not.(allocated(r358))) then
       allocate(r358(1:r104,1:r105))
       r358=0.0
       end if
       call new_transformer_2(r104,r105,r339,r75,r226,r279,r343,r227,r280,r346,r228,r281,r349,r229,r282,r352,r230,r283,r355,r231,r284,r358,r232,r285,r361,r233,r286,r120,r134,r287,r150,r327,r331,r151,r328,r332,r152,r329,r333,r153,r330,r334)
       call put_transformer2(r339,-339)
       call put_transformer2(r343,-343)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call get_igcm_atmosphere(r339,-339)
       call get_igcm_atmosphere(r343,-343)
       call get_igcm_atmosphere(r122,-122)
       call get_igcm_atmosphere(r123,-123)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r189,-189)
       call igcm3_diab(r150,r151,r152,r153,r120,r361,r355,r358,r339,r343,r346,r349,r122,r123,r124,r139,r158,r230,r61,r62,r63,r189,r144,r166,r192)
       call put_igcm_atmosphere(r339,-339)
       call put_igcm_atmosphere(r343,-343)
       call put_igcm_atmosphere(r122,-122)
       call put_igcm_atmosphere(r123,-123)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r189,-189)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(19)
       call get_transformer3(r381,-381)
       call get_transformer3(r339,-339)
       call get_transformer3(r385,-385)
       call get_transformer3(r343,-343)
       call get_transformer3(r387,-387)
       call get_transformer3(r122,-122)
       call get_transformer3(r389,-389)
       call get_transformer3(r123,-123)
       call get_transformer3(r391,-391)
       call get_transformer3(r393,-393)
       call get_transformer3(r395,-395)
       call new_transformer_3(r104,r105,r381,r339,r383,r295,r385,r343,r387,r122,r389,r123,r391,r346,r393,r349,r395,r124,r397)
       call put_transformer3(r381,-381)
       call put_transformer3(r339,-339)
       call put_transformer3(r385,-385)
       call put_transformer3(r343,-343)
       call put_transformer3(r387,-387)
       call put_transformer3(r122,-122)
       call put_transformer3(r389,-389)
       call put_transformer3(r123,-123)
       call put_transformer3(r391,-391)
       call put_transformer3(r393,-393)
       call put_transformer3(r395,-395)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer5(r408,-408)
       call get_transformer5(r339,-339)
       call get_transformer5(r290,-290)
       call get_transformer5(r413,-413)
       call get_transformer5(r343,-343)
       call get_transformer5(r415,-415)
       call get_transformer5(r122,-122)
       call get_transformer5(r417,-417)
       call get_transformer5(r123,-123)
       call get_transformer5(r419,-419)
       call get_transformer5(r421,-421)
       call get_transformer5(r423,-423)
       call get_transformer5(r425,-425)
       call get_transformer5(r427,-427)
       call new_transformer_5(r104,r105,r408,r339,r290,r383,r397,r413,r343,r415,r122,r417,r123,r419,r346,r421,r349,r423,r124,r425,r352,r427,r131)
       call put_transformer5(r408,-408)
       call put_transformer5(r339,-339)
       call put_transformer5(r290,-290)
       call put_transformer5(r413,-413)
       call put_transformer5(r343,-343)
       call put_transformer5(r415,-415)
       call put_transformer5(r122,-122)
       call put_transformer5(r417,-417)
       call put_transformer5(r123,-123)
       call put_transformer5(r419,-419)
       call put_transformer5(r421,-421)
       call put_transformer5(r423,-423)
       call put_transformer5(r425,-425)
       call put_transformer5(r427,-427)
       end if
       end if
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(33)
       call end_atmos()
       end if
       call finaliseComms()
       end program BFG2Main

