#!/bin/sed -nf
#cwa 10/7/07
#This file adds mpe_log_event calls around calls to models
#in the global loop of BFG2Main source code.

#exchange pattern space with hold space
x
#prime the first line
/^$/ s/^.*$/1/
#exchange pattern space with hold space
x
#Concatenate \n and hold to pattern
G
#print mpe logging calls with model calls. Identify model calls as consisting of all r[0-9]+ type args.
/do its1/,/finaliseComms/ s/call \(.*\)[(]\(\(r[0-9,]\+\)\+\)[)]\n\([0-9]\+\)/call mpe_log_event(b\4,its1,"\1",ierr)\n       call \1(\2)\n       call mpe_log_event(e\4,its1,"\1",ierr)/
s/\(.*\)\n[0-9]\+/\1/
p
#exchange pattern space with hold space
x


#    increment line no

#line no is in pattern space
#for any string of digits that start with 9 prepend a zero
/^9*$/ s/^/0/
#prepend an x before the string of .9* i.e. the changing part
s/.9*$/x&/
#
#hold the changing part
h
#delete the stuff before the changing part
s/^.*x//
#increment the changing part
y/0123456789/1234567890/
x
#keep the unchanged digits in pattern space
s/x.*$//
#Compose the new number
G
#remove \n inserted by G
s/\n//
#hold the number
h
