#!/bin/sed -nf

#This file outputs a list of calls to mpe_log_get_event_number
s/call mpe_log_event[(]\(.[0-9]\+\),its1,\(.*\),ierr[)]/\1=mpe_log_get_event_number()/p
