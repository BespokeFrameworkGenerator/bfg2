#!/bin/sed -nf

#This file outputs a list of calls to mpe_describe_state 
s/call mpe_log_event[(]b\([0-9]\+\),its1,\(.*\),ierr[)]/call mpe_describe_state(b\1,e\1,\2,"red")/p
