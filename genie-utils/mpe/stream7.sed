#!/bin/sed -nf
#This file constructs a call to get events. Need something similar to construct subroutine get_events
/^ *program BFG2Main/ s/.*/       subroutine get_events(/
s/ *call mpe_log_event[(]b\([0-9]\+\),its1,\(.*\),ierr[)]/     + b\1,e\1,/
/b[0-9]\+,e[0-9]\+\|end program BFG2Main\|get_events(/! d
H
x
#s/\n//
/end program BFG2Main/ s/,\n *end program BFG2Main/)\n       include 'mpe_logf.h'/p
x
