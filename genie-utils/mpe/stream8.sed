#!/bin/sed -nf
#This file constructs a call to get events. Need something similar to construct subroutine get_events
/^ *program BFG2Main/ s/.*/       INTEGER :: /
s/ *call mpe_log_event[(]b\([0-9]\+\),its1,\(.*\),ierr[)]/     + b\1,e\1,/
/b[0-9]\+,e[0-9]\+\|end program BFG2Main\|INTEGER :: /! d
H
x
#s/\n//
#p
/end program BFG2Main$/ s/,\n *end program BFG2Main//p
x
