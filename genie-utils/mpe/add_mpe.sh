#!/bin/bash
FILE=$1
DESTINATION=$2
GET_EVENTS=$3
add_mpe_log_event.sed $FILE > tmp.F
#Need to rename test*.sed scripts according to their job
./stream1.sed tmp.F > BFG2Main_new.f90
./stream2.sed tmp.F >> BFG2Main_new.f90
./stream3.sed tmp.F >> BFG2Main_new.f90
echo '       call mpe_init_log()' >> BFG2Main_new.f90
./stream4.sed tmp.F >> BFG2Main_new.f90
./stream5.sed tmp.F >> BFG2Main_new.f90
echo '       call mpe_start_log()' >> BFG2Main_new.f90
./stream6.sed tmp.F >> BFG2Main_new.f90
#Problem - get_events.F is f77 and needs newlines for declarations
./stream7.sed tmp.F > get_events.F
./stream8.sed tmp.F >> get_events.F
./stream9.sed tmp.F >> get_events.F
echo '       end' >> get_events.F
#cat BFG2Main_new.f90
if [ $DESTINATION ] ; then
 mv BFG2Main_new.f90 $DESTINATION
fi
if [ $GET_EVENTS ] ; then
 mv get_events.F $GET_EVENTS
fi
rm tmp.F
