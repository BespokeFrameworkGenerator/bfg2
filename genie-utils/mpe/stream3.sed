#!/bin/sed -nf

#This file places a call to get_events, the calls to mpe_describe_event and the call to mpe_start_log before do its1=
/do its1=/ x
#print only if the pattern space contains newlines, which it will if the above exchange has taken place
s/\n//
/.*\n.*/ p
/! Begin declaration of Control/,/do its1=/ H
