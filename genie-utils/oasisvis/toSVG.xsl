<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/2000/svg">

<xsl:output method="xml" indent="yes"/>
<xsl:variable name="minStart">
<xsl:for-each select="//start">
 <xsl:sort select="normalize-space(@time)" order="ascending"/>
 <xsl:if test="position() = 1">
  <xsl:value-of select="normalize-space(@time)"/>
 </xsl:if>
</xsl:for-each>
</xsl:variable>

<xsl:variable name="maxEnd">
<xsl:for-each select="//end">
 <xsl:sort select="normalize-space(@time)" order="descending"/>
 <xsl:if test="position() = 1">
  <xsl:value-of select="number(normalize-space(@time)) - number($minStart)"/>
 </xsl:if>
</xsl:for-each>
</xsl:variable>

<!--an accuracy of 1000 gives 1px = 1 millisecond-->
<xsl:param name="accuracy" select="number('100000')"/>
<xsl:param name="view" select="floor(number($maxEnd*$accuracy))"/>
<xsl:param name="window" select="number('1000')"/>

<xsl:template match="/">
<svg version="1.1"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns="http://www.w3.org/2000/svg"
 height="{$window*9 div 16}" width="{$window}"
 viewBox="0 0 {$view} {$view*9 div 16}"
 onclick="zoom(evt)"
 onload="init()">
 <!--viewBox="0 0 {floor($accuracy*number($maxEnd))} {floor($accuracy*number($maxEnd))}"-->
<script type="text/ecmascript" xlink:href="./mapApp.js"/>
<script type="text/ecmascript" xlink:href="./helper_functions.js"/>
<script type="text/ecmascript"><xsl:text disable-output-escaping="yes">&lt;![CDATA[

  var myMapApp = new mapApp(false,undefined);

  function init()
  {
   evtText = document.getElementById("component");
   var node = document.firstChild;
   var ids = new Array(100);
   for(var i=0, child; child = node.childNodes[i]; i++)
   {
    if(child.nodeType == 1)
    {
     if(child.hasAttribute("id") &amp;&amp; child.getAttributeNode("id").value=="canvas")
     {;}
     else if(child.localName == "rect")
     {
      var seen=undefined;
      for(var j=0; ids[j]; j++)
      {
       if(ids[j].name == child.getAttribute("id"))
       {
        seen=j;
       }
      }
      if(seen == undefined)
      {
       ids[j] = {name:child.getAttribute("id"),randR:Math.floor(Math.random()*255),randG:Math.floor(Math.random()*255),randB:Math.floor(Math.random()*255)};
       seen=j;
      }
      child.setAttribute("style","fill:rgb("+ids[seen].randR+","+ids[seen].randG+","+ids[seen].randB+")");
      //alert("fill:rgb("+randR+","+randG+","+randB+")");
     }
    }
   }
  }

  function zoom(evt)
  {
   var coords = myMapApp.calcCoord(evt);
   var myVB = evt.target.getAttribute("viewBox");
   var node = evt.target;
   var parent = evt.target.parentNode;
   while(!myVB)
   {
    myVB = parent.getAttribute("viewBox");
    node = parent;
    parent = parent.parentNode;
   }
   newWidth = myVB.split(" ")[2]*0.5;
   newHeight = myVB.split(" ")[3]*0.5;
   newX = coords.x - 0.5*newWidth
   newY = coords.y - 0.5*newHeight
   //alert("canvas coors are "+evt.clientX+" "+evt.clientY+"\ncoordinates are "+coords.x+" "+coords.y+"\nold viewBox was "+myVB+"\nnew viewBox is "+newX+" "+newY+" "+newWidth+" "+newHeight);
   node.setAttribute("viewBox",newX+" "+newY+" "+newWidth+" "+newHeight);
   for(var i=0, child; child = node.childNodes[i]; i++)
   {
    if(child.nodeType == 1)
    {
     if(child.hasAttribute("id") &amp;&amp; child.getAttributeNode("id").value=="canvas")
     {
      child.setAttribute("x",newX);
      child.setAttribute("y",newY);
      child.setAttribute("width",newWidth);
      child.setAttribute("height",newHeight);
      var myStyles = child.getAttribute("style").split(";");
      var myWidth = newHeight/100;
      child.setAttribute("style",myStyles[0]+";"+"stroke-width:"+myWidth+";"+myStyles[2]);
     }
     else if(child.localName == "rect")
     {
      var myY = child.getAttribute("heightfrac")*newHeight + newY;
      child.setAttribute("y",myY)
      child.setAttribute("height",newHeight/20);
      var myX = Number(child.getAttribute("x"));
      var myWidth = Number(child.getAttribute("width"));
      var tmp = myX + myWidth;
      //alert("myX="+myX+"\nmyWidth="+myWidth+"\nmyX+myWidth="+tmp+"\nnewX="+newX);
      if(myX &lt; newX)
      {
       var myNewX = newX;
       var myNewWidth = myWidth - (newX - myX);
       if(myNewWidth &gt; newWidth)
       {
        //alert("in if stmnt \nmyNewWidth="+myNewWidth+"\nnewWidth="+newWidth);
        myNewWidth = newWidth;
       }
       //alert("myNewWidth="+myNewWidth+"\nnewWidth="+newWidth);
       child.setAttribute("x",myNewX);
       child.setAttribute("width",myNewWidth);
      }
      if(tmp &gt; newX+newWidth)
      {
       var myNewWidth = newX+newWidth - myX;
       if(myNewWidth &gt; newWidth)
       {
        //alert("in if stmnt \nmyNewWidth="+myNewWidth+"\nnewWidth="+newWidth);
        myNewWidth = newWidth;
       }
       child.setAttribute("width",myNewWidth);
      }
      if(tmp &lt; newX)
      {
       //alert("removing rect; before box");
       node.removeChild(child);
      }
      else if(myX &gt; newX+newWidth)
      {
       //alert("removing rect; after box");
       node.removeChild(child);
      }
     }
     else if(child.localName == "line")
     {
      var myY1 = child.getAttribute("heightfrac1")*newHeight + newY;
      child.setAttribute("y1",myY1)
      var myY2 = child.getAttribute("heightfrac2")*newHeight + newY;
      child.setAttribute("y2",myY2)
      var myStyles = child.getAttribute("style").split(";");
      if(child.getAttribute("y1") == child.getAttribute("y2"))
      {
       var myWidth = newHeight/100;
       child.setAttribute("x1",newX);
       child.setAttribute("x2",newX+newWidth);
      }
      else
      {
       var myWidth = newHeight/1000;
       var myX1 = Number(child.getAttribute("x1"));
       var myX2 = Number(child.getAttribute("x2"));
       if(myX1 &lt; newX &amp;&amp; myX2 &lt; newX)
       {
        //alert("removing line; one or more points before box");
        node.removeChild(child);
       }
       else if(myX1 &gt; newX+newWidth &amp;&amp; myX2 &gt; newX+newWidth)
       {
        //alert("removing line; one or more points after box");
        node.removeChild(child);
       }
       if(myX1 &lt; newX)
       {
        var myNewY1 = ((newX - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
        child.setAttribute("y1",myNewY1);
        child.setAttribute("x1",newX);
       }
       if(myX2 &gt; newX+newWidth)
       {
        var myNewY2 = ((newX+newWidth - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
        child.setAttribute("y2",myNewY2);
        child.setAttribute("x2",newX+newWidth);
       }
//I think the two if statements above do the same job as the commented section below
//       else if(myX1 &lt; newX &amp;&amp; myX2 &gt; newX+newWidth)
//       {
//        var myNewY1 = ((newX - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
//        child.setAttribute("y1",myNewY1);
//        child.setAttribute("x1",newX);
//        var myNewY2 = ((newX+newWidth - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
//        child.setAttribute("y2",myNewY2);
//        child.setAttribute("x2",newX+newWidth);
//        alert("removing left and right sides of line");
//       }
//       else if(myX1 &lt; newX &amp;&amp; myX2 &gt; newX)
//       {
//        var myNewY1 = ((newX - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
//        child.setAttribute("y1",myNewY1);
//        child.setAttribute("x1",newX);
//        alert("removing left side of line\n y1 was:"+myY1+"\n y1 is:"+myNewY1);
//       }
//       else if(myX1 &lt; newX+newWidth &amp;&amp; myX2 &gt; newX+newWidth)
//       {
//        var myNewY2 = ((newX+newWidth - myX1)/(myX2 - myX1))*(myY2 - myY1) + myY1;
//        child.setAttribute("y2",myNewY2);
//        child.setAttribute("x2",newX+newWidth);
//        alert("removing right side of line\n y2 was:"+myY2+"\n y2 is:"+myNewY2);
//       }
      }
      child.setAttribute("style",myStyles[0]+";"+"stroke-width:"+myWidth);
     }
     else if(child.localName == "text")
     {
//this will get done twice because there are two text elements - need to identify them separately
      var numSecs = document.getElementById("numSecs");
      numSecs.setAttribute("x",newX + newWidth/5);
      numSecs.setAttribute("y",newY + newHeight*0.1);
      numSecs.firstChild.nodeValue = numSecs.firstChild.nodeValue / 2;
      var mySize = newHeight/40;
      numSecs.setAttribute("font-size",mySize);

      var component = document.getElementById("component");
      component.setAttribute("x",newX + newWidth/2);
      component.setAttribute("y",newY + newHeight*0.1);
      component.setAttribute("font-size",mySize);

      //var secs = document.getElementById("secs");
      //secs.setAttribute("x",newX + newWidth/2);
      //secs.setAttribute("y",newY + newHeight*0.125);
      //component.setAttribute("font-size",mySize);
     }
    }
   }
  }

  function echo(evt)
  {
   var coords = myMapApp.calcCoord(evt);
   //need to divide by the accuracy below
   //alert(evt.target.getAttributeNode("id").value+" at "+coords.x/100000+" seconds");
  }
  
  var evtText;
  function display_component(evt)
  {
   var coords = myMapApp.calcCoord(evt);
   evtText.firstChild.nodeValue = evt.target.getAttributeNode("id").value+" at "+coords.x/100000+" secs";
  }

  //function display_secs(evt)
  //{
  // var coords = myMapApp.calcCoord(evt);
  // evtText.firstChild.nodeValue = coords.x/100000+" secs";
  //}
]]&gt;</xsl:text>
 </script>
<!--
 <defs>
  <symbol id="arrowHead">
  </symbol>
 </defs>
-->
 <rect x="0" y="0" width="{$view}" height="{$view*9 div 16}" style="fill:white;stroke-width:{$view*9 div 1600};stroke:black" id="canvas" onclick="echo(evt)" onmousemove="display_component(evt)"/>
 <text><tspan x="{$view div 5}" y="{$view*9 div 160}" font-size="{$view*9 div 640}" id="numSecs" onmousemove="display_component(evt)"><xsl:value-of select="$maxEnd"/></tspan></text>
 <text><tspan x="{$view div 2}" y="{$view*9 div 160}" font-size="{$view*9 div 640}" id="component" onmousemove="display_component(evt)">info</tspan></text>
 <!--<text><tspan x="{$view div 2}" y="{$view*45 div 640}" font-size="{$view*9 div 640}" id="secs" onmousemove="display_secs(evt)">secs</tspan></text>-->
 <xsl:apply-templates select="/oasisvis/proc/end"/>
 <xsl:apply-templates select="/oasisvis/event[normalize-space(@type)='begin']"/>
 <xsl:apply-templates select="/oasisvis/event[normalize-space(@type)='send']"/>
 <!--
 -->
</svg>
</xsl:template>

<xsl:template match="end">
 <!--heightfrac below should really be in a separate namespace-->
 <line x1="{$accuracy*(number(normalize-space(../../proc[@id=current()/../@id]/start/@time)) - number($minStart))}" 
       x2="{$accuracy*(number(normalize-space(@time)) - number(normalize-space(../../proc[@id=current()/../@id]/start/@time)))}" 
       y1="{($view*9 div 16)*((number(../@id) + 1) div (count(//end) + 1))}" heightfrac1="{((number(../@id) + 1) div (count(//end) + 1))}"
       y2="{($view*9 div 16)*((number(../@id) + 1) div (count(//end) + 1))}" heightfrac2="{((number(../@id) + 1) div (count(//end) + 1))}"
       style="stroke:black;stroke-width:{$view*9 div 1600}" id="proc{normalize-space(../@id)}" onclick="echo(evt)" onmousemove="display_component(evt)"/>
</xsl:template>

<xsl:key name="sends" match="event[normalize-space(@type)='send']" use="@tag"/>
<xsl:key name="receives" match="event[normalize-space(@type)='receive']" use="@tag"/>
<xsl:key name="begins" match="event[normalize-space(@type)='begin']" use="@name"/>
<xsl:key name="ends" match="event[normalize-space(@type)='end']" use="@name"/>

<!-- 
Need to use percentage sizes, but a percentage must be for the whole canvas, so will
need to change all shapes to percentage sizes at once to notice the difference, otherwise
absolutely-sized shapes will distort the canvas size. Therefore, can't really use percentages 
for the y-direction alone. Need to calculate absoute y values dynamically in javascript.
-->

<xsl:template match="event">
 <xsl:variable name="myID" select="generate-id(.)"/>

 <xsl:if test="normalize-space(@type)='send'">
  <xsl:variable name="myTag" select="@tag"/>
  <xsl:variable name="src_proc" select="normalize-space(@proc)"/>
  <xsl:variable name="dest_proc" select="normalize-space(@dest)"/>
  <xsl:for-each select="key('sends',$myTag)[normalize-space(@dest)=$dest_proc and normalize-space(@proc)=$src_proc]">
   <xsl:sort select="@time" order="ascending"/>
   <xsl:variable name="sendPos" select="position()"/>
   <xsl:if test="generate-id(.)=$myID">
    <xsl:variable name="sendEvent" select="."/>
    <xsl:for-each select="key('receives',$myTag)[normalize-space(@src)=$src_proc and normalize-space(@proc)=$dest_proc]">
     <xsl:sort select="@time" order="ascending"/>
     <xsl:if test="position()=$sendPos">
      <xsl:variable name="receiveEvent" select="."/>
   <line x1="{$accuracy*(number(normalize-space($sendEvent/@time)) - number($minStart))}" 
         x2="{$accuracy*(number(normalize-space($receiveEvent/@time)) - number($minStart))}" 
         y1="{($view*9 div 16)*((number($sendEvent/@proc) + 1) div (count(//end) + 1))}" heightfrac1="{((number($sendEvent/@proc) + 1) div (count(//end) + 1))}"
         y2="{($view*9 div 16)*((number($receiveEvent/@proc)+1) div (count(//end) + 1))}" heightfrac2="{((number($receiveEvent/@proc)+1) div (count(//end) + 1))}"
         style="stroke:green;stroke-width:{$view*9 div 16000}" id="Comm tag {normalize-space(@tag)} ({$sendEvent/@proc}->{$receiveEvent/@proc})" 
         onclick="echo(evt)" onmousemove="display_component(evt)"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:if>
  </xsl:for-each>
 </xsl:if>

 <xsl:if test="normalize-space(@type)='begin'">
  <xsl:variable name="myName" select="@name"/>
  <xsl:for-each select="key('begins',$myName)">
   <xsl:sort select="@time" order="ascending"/>
   <xsl:variable name="beginPos" select="position()"/>
   <xsl:if test="generate-id(.)=$myID">
    <xsl:variable name="beginEvent" select="."/>
    <xsl:for-each select="key('ends',$myName)">
     <xsl:sort select="@time" order="ascending"/>
     <xsl:if test="position()=$beginPos">
      <xsl:variable name="endEvent" select="."/>
   <rect x="{$accuracy*(number(normalize-space($beginEvent/@time)) - number($minStart))}" 
         y="{($view*9 div 16)*((number($beginEvent/@proc) + 1) div (count(//end) + 1))}" heightfrac="{((number($beginEvent/@proc) + 1) div (count(//end) + 1))}"
         width="{$accuracy*(number(normalize-space($endEvent/@time)) - number(normalize-space($beginEvent/@time)))}" 
         height="{$view*9 div 320}" style="fill:red" id="{normalize-space(@name)}" onclick="echo(evt)" onmousemove="display_component(evt)"/>
     </xsl:if>
    </xsl:for-each>
   </xsl:if>
  </xsl:for-each>
 </xsl:if>
</xsl:template>

</xsl:stylesheet>
