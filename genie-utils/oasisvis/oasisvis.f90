      subroutine oasisvis_prism_put ( my_su, dest_su, msg_tag, field_id, date, date_bounds, data_array, info, ierror )
       use mpi
       use PRISM

       implicit none

       Integer, Intent (In)                 :: my_su
       Integer, Intent (In)                 :: dest_su
       !Integer, Intent (In)                 :: msg_tag
       CHARACTER(LEN=*), INTENT(IN) :: msg_tag
       Integer, Intent (In)                 :: field_id
       Type(PRISM_Time_Struct), Intent (In) :: date
       Type(PRISM_Time_Struct), Intent (In) :: date_bounds(2)
       Double Precision, Intent (In)        :: data_array(*)
       integer, Intent (Out)               :: info
       integer, Intent (Out)               :: ierror

       ! My vars
       INTEGER :: UNT!,MYRANK,local_comm,process_id
       DOUBLE PRECISION :: T

       call prism_put( field_id, date, date_bounds, data_array, info, ierror )
       ! Get the current time
       T = MPI_WTIME()
       ! Get my rank (local to this deployment unit)
       !call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       !CALL MPI_COMM_RANK(local_comm,MYRANK,ierror)
       ! Calculate a process id from DU# and local rank
       !process_id = my_du * 1000 + MYRANK
       ! Calculate which file to write to
       UNT=100+my_su
       ! Write the XML element
       !WRITE(UNIT=UNT,FMT="(A24,I3,A8,I5,A8,I2,A8,F20.6,A3)") &
       ! The SU numbers (or rank numbers if MPI) should be zero-based
       ! for the toSVG.xsl templates
       WRITE(UNIT=UNT,FMT="(A,A,A,I2,A,I2,A,F20.6,A)") &
         '<event type="send" tag="',trim(msg_tag),'" proc="',my_su-1,'" dest="',dest_su-1,'" time="',T,'"/>'
      end subroutine oasisvis_prism_put

      subroutine oasisvis_prism_get ( my_su, src_su, msg_tag, field_id, date, date_bounds, data_array, info, ierror )
       use mpi
       use PRISM

       implicit none

       Integer, Intent (In)                 :: my_su
       Integer, Intent (In)                 :: src_su
       !Integer, Intent (In)                 :: msg_tag
       CHARACTER(LEN=*), INTENT(IN) :: msg_tag
       Integer, Intent (In)                 :: field_id
       Type(PRISM_Time_Struct), Intent (In) :: date
       Type(PRISM_Time_Struct), Intent (In) :: date_bounds(2)
       Double Precision, Intent (InOut)     :: data_array(*)
       Integer, Intent (Out)                :: info
       Integer, Intent (Out)                :: ierror

       ! My vars
       INTEGER :: UNT!,MYRANK,local_comm,process_id
       DOUBLE PRECISION :: T

       call prism_get( field_id, date, date_bounds, data_array, info, ierror )
       ! Get the current time
       T = MPI_WTIME()
       ! Get my rank (local to this deployment unit)
       !call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       !CALL MPI_COMM_RANK(local_comm,MYRANK,ierror)
       ! Calculate a process id from DU# and local rank
       !process_id = my_du * 1000 + MYRANK
       ! Calculate which file to write to
       UNT=100+my_su
       ! Write the XML element
       !WRITE(UNIT=UNT,FMT="(A27,I3,A8,I5,A7,I2,A8,F20.6,A3)") &
       WRITE(UNIT=UNT,FMT="(A,A,A,I2,A,I2,A,F20.6,A)") &
         '<event type="receive" tag="',trim(msg_tag),'" proc="',my_su-1,'" src="',src_su-1,'" time="',T,'"/>'
      end subroutine oasisvis_prism_get

      ! Subroutine outputs an XML tag for computation - marks begin and end times
      SUBROUTINE OASISVIS_COMP(MYTYPE,MYNAME,my_su)
       USE MPI
       !use prism
       IMPLICIT NONE
       CHARACTER(LEN=*), INTENT(IN) :: MYTYPE
       CHARACTER(LEN=*), INTENT(IN) :: MYNAME
       INTEGER, INTENT(IN) :: my_su
       INTEGER :: UNT!,ierror,MYRANK,local_comm,process_id
       DOUBLE PRECISION :: T
       ! Get the current time
       T = MPI_WTIME()
       ! Get my rank (local to this deployment unit)
       !call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       !CALL MPI_COMM_RANK(local_comm,MYRANK,ierror)
       ! Calculate a process id from DU# and local rank
       !process_id = my_du * 1000 + MYRANK
       ! Calculate which file to write to
       UNT=100+my_su
       ! Write the XML element
       !WRITE(UNIT=UNT,FMT="(A13,A20,A8,A200,A8,I5,A8,F20.6,A3)") &
       WRITE(UNIT=UNT,FMT="(A,A,A,A,A,I2,A,F20.6,A)") &
         '<event type="',MYTYPE,'" name="',MYNAME,'" proc="',my_su-1,'" time="',T,'"/>'
      END SUBROUTINE OASISVIS_COMP

      SUBROUTINE OASISVIS_INIT(my_su)
       USE MPI
       !use prism
       IMPLICIT NONE
       INTEGER, INTENT(IN) :: my_su
       INTEGER :: UNT!,MYRANK,local_comm,ierror,process_id
       DOUBLE PRECISION :: T
       CHARACTER DIRNAME*20
       CHARACTER DIRNUM*20
       ! Get my rank (local to this deployment unit)
       !call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       !CALL MPI_COMM_RANK(local_comm,MYRANK,ierror)
       ! Calculate a process id from DU# and local rank
       !process_id = my_du * 1000 + MYRANK
       UNT=100+my_su
       WRITE(UNIT=DIRNUM,FMT='(I2)')my_su
       DIRNAME='oasisvisSU'//trim(adjustl(DIRNUM))//'.xml' 
       OPEN(UNIT=UNT,FILE=DIRNAME)
       T = MPI_WTIME()
       !WRITE(UNIT=UNT,FMT="(A10,I5,A15,F20.6,A10)") &
       WRITE(UNIT=UNT,FMT="(A,I2,A,F20.6,A)") &
         '<proc id="',my_su-1,'"><start time="',T,'"/></proc>'
      END SUBROUTINE OASISVIS_INIT

      SUBROUTINE OASISVIS_FINAL(my_su)
       USE MPI
       !use prism
       IMPLICIT NONE
       INTEGER, INTENT(IN) :: my_su
       INTEGER :: UNT!MYRANK,local_comm,ierror,process_id
       DOUBLE PRECISION :: T
       ! Get my rank (local to this deployment unit)
       !call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       !CALL MPI_COMM_RANK(local_comm,MYRANK,ierror)
       ! Calculate a process id from DU# and local rank
       !process_id = my_du * 1000 + MYRANK
       UNT=100+my_su
       T = MPI_WTIME()
       !WRITE(UNIT=UNT,FMT="(A10,I5,A13,F20.6,A10)") &
       WRITE(UNIT=UNT,FMT="(A,I2,A,F20.6,A)") &
         '<proc id="',my_su-1,'"><end time="',T,'"/></proc>'
       CLOSE(UNT)
      END SUBROUTINE OASISVIS_FINAL
