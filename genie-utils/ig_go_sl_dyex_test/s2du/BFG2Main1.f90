       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !interp_ocn_atm__freq
       integer :: interp_ocn_atm__freq
       !ini_weights__freq
       integer :: ini_weights__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !weight_check__freq
       integer :: weight_check__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_ocn_atm_3_freq
       integer :: interp_ocn_atm_3_freq
       !interp_ocn_atm_4_freq
       integer :: interp_ocn_atm_4_freq
       !interp_ocn_atm_5_freq
       integer :: interp_ocn_atm_5_freq
       !interp_ocn_atm_6_freq
       integer :: interp_ocn_atm_6_freq
       !interp_ocn_atm_7_freq
       integer :: interp_ocn_atm_7_freq
       !interp_ocn_atm_8_freq
       integer :: interp_ocn_atm_8_freq
       !interp_ocn_atm_9_freq
       integer :: interp_ocn_atm_9_freq
       !interp_ocn_atm_10_freq
       integer :: interp_ocn_atm_10_freq
       !interp_ocn_atm_11_freq
       integer :: interp_ocn_atm_11_freq
       !interp_ocn_atm_12_freq
       integer :: interp_ocn_atm_12_freq
       !interp_ocn_atm_13_freq
       integer :: interp_ocn_atm_13_freq
       !interp_ocn_atm_14_freq
       integer :: interp_ocn_atm_14_freq
       !interp_ocn_atm_15_freq
       integer :: interp_ocn_atm_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy__freq
       integer :: copy_dummy__freq
       !interp_ocn_atm_16_freq
       integer :: interp_ocn_atm_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_ocn_atm_17_freq
       integer :: interp_ocn_atm_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,interp_ocn_atm__freq,ini_weights__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,weight_check__freq,counter_2_freq,interp_ocn_atm_3_freq,interp_ocn_atm_4_freq,interp_ocn_atm_5_freq,interp_ocn_atm_6_freq,interp_ocn_atm_7_freq,interp_ocn_atm_8_freq,interp_ocn_atm_9_freq,interp_ocn_atm_10_freq,interp_ocn_atm_11_freq,interp_ocn_atm_12_freq,interp_ocn_atm_13_freq,interp_ocn_atm_14_freq,interp_ocn_atm_15_freq,counter_3_freq,goldstein__freq,copy_dummy__freq,interp_ocn_atm_16_freq,copy_tstar_2_freq,interp_ocn_atm_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon2_atm
       real, dimension(1:64) :: r165
       !alat2_atm
       real, dimension(1:32) :: r166
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r168
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r170
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r172
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r190
       !hrlons_atm
       real, dimension(1:320) :: r203
       !hrlats_atm
       real, dimension(1:160) :: r204
       !hrlonsedge_atm
       real, dimension(1:321) :: r205
       !hrlatsedge_atm
       real, dimension(1:161) :: r206
       !u10m_atm
       real, dimension(1:64,1:32) :: r213
       !v10m_atm
       real, dimension(1:64,1:32) :: r214
       !q2m_atm
       real, dimension(1:64,1:32) :: r216
       !rh2m_atm
       real, dimension(1:64,1:32) :: r217
       !psigma
       real, dimension(1:7) :: r221
       !massair
       real, dimension(1:64,1:32,1:7) :: r224
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r249
       !test_water_land
       real :: r293
       !glim_snow_model
       logical :: r300
       !glim_coupled
       logical :: r301
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r215
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r548
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !alon1_atm
       real, dimension(1:64) :: r163
       !alat1_atm
       real, dimension(1:32) :: r164
       !aboxedge1_lon_atm
       real, allocatable, dimension(:) :: r565
       !aboxedge1_lat_atm
       real, allocatable, dimension(:) :: r564
       !land_niter_tim
       integer :: r173
       !ocean_niter_tim
       integer :: r174
       !atmos_dt_tim
       real :: r175
       !tstar_atm
       real, dimension(1:64,1:32) :: r550
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r177
       !albedo_atm
       real, dimension(1:64,1:32) :: r557
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !precip_atm
       real, dimension(1:64,1:32) :: r181
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r187
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r188
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r191
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r194
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r195
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r196
       !water_flux_atmos
       real :: r201
       !glim_covmap
       real, dimension(1:64,1:32) :: r202
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r549
       !ksic_loop
       integer :: r352
       !istep_atm
       integer :: r373
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r207
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r208
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r209
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r210
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r211
       !surfsigma
       real :: r219
       !surfdsigma
       real :: r220
       !iconv_ice
       integer :: r222
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r223
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r421
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r422
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r423
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r424
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r425
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r426
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r427
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r428
       !surf_iter_tim_bfg
       integer :: r374
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r279
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r280
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r281
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r282
       !land_latent_atm
       real, dimension(1:64,1:32) :: r283
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r284
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r285
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r286
       !land_evap_atm
       real, dimension(1:64,1:32) :: r287
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r288
       !land_rough_atm
       real, dimension(1:64,1:32) :: r289
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r290
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r332
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r333
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r334
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r335
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r336
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r337
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r338
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r339
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r340
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r341
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r342
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r343
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r344
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r433
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r437
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r440
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r443
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r446
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r449
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r452
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r455
       !iconv_che
       integer :: r246
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r475
       !katm_loop
       integer :: r477
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r479
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r481
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r483
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r485
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r487
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r489
       !kocn_loop
       integer :: r491
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r502
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r507
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r509
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r511
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r513
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r515
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r517
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r519
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r521
       !ilon1_atm
       integer :: r547
       !ilat1_atm
       integer :: r546
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r430
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r429
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r278
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r331
       !flag_goldsteinocean
       logical :: r183
       !flag_goldsteinseaice
       logical :: r184
       !flag_land
       logical :: r185
       !klnd_loop
       integer :: r186
       !lgraphic
       logical :: r182
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r189
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r548=0
           r76=0.0
           r77=0.0
           r78=0.0
           r61=0.0
           r62=0.0
           r63=0.0
           r163=0.0
           r164=0.0
           ! 565is allocatable; cannot be written to yet
           ! 564is allocatable; cannot be written to yet
           r173=0
           r174=0
           r175=0.0
           r550=0.0
           ! 177is allocatable; cannot be written to yet
           r557=0.0
           r179=0.0
           r180=0.0
           r181=0.0
           ! 187is allocatable; cannot be written to yet
           r188=0.0
           ! 191is allocatable; cannot be written to yet
           r194=0.0
           r195=0.0
           r196=0.0
           r201=0.0
           r202=0.0
           r549=0.0
           r352=6
           r373=0
           r207=0.0
           r208=0.0
           r209=0.0
           r210=0.0
           r211=0.0
           r219=0.0
           r220=0.0
           r222=0.0
           r223=0.0
           ! 421is allocatable; cannot be written to yet
           ! 422is allocatable; cannot be written to yet
           ! 423is allocatable; cannot be written to yet
           ! 424is allocatable; cannot be written to yet
           ! 425is allocatable; cannot be written to yet
           ! 426is allocatable; cannot be written to yet
           ! 427is allocatable; cannot be written to yet
           ! 428is allocatable; cannot be written to yet
           r374=0
           r279=0.0
           r280=0.0
           r281=0.0
           r282=0.0
           r283=0.0
           r284=0.0
           r285=0.0
           r286=0.0
           r287=0.0
           r288=0.0
           r289=0.0
           r290=0.0
           r332=0.0
           r333=0.0
           r334=0.0
           r335=0.0
           r336=0.0
           r337=0.0
           r338=0.0
           r339=0.0
           r340=0.0
           r341=0.0
           r342=0.0
           r343=0.0
           r344=0.0
           r433=0.0
           r437=0.0
           r440=0.0
           r443=0.0
           r446=0.0
           ! 449is allocatable; cannot be written to yet
           ! 452is allocatable; cannot be written to yet
           ! 455is allocatable; cannot be written to yet
           r246=0
           r475=0.0
           r477=1
           r479=0.0
           r481=0.0
           r483=0.0
           r485=0.0
           r487=0.0
           r489=0.0
           r491=48
           r502=0.0
           r507=0.0
           r509=0.0
           r511=0.0
           r513=0.0
           r515=0.0
           r517=0.0
           r519=0.0
           r521=0.0
           r547=64
           r546=32
           ! 430is allocatable; cannot be written to yet
           ! 429is allocatable; cannot be written to yet
           r278=0.0
           r331=0.0
           r183=.true.
           r184=.false.
           r185=.false.
           r186=1
           r182=.false.
           ! 189is allocatable; cannot be written to yet
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call start_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       call initialise_igcmsurf()
       call end_mpivis_log('igcm_atmosphere initialise_igcmsurf ')
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r163,-163)
       call get_igcm_atmosphere(r164,-164)
       if (.not.(allocated(r565))) then
       allocate(r565(1:r547+1))
       r565=0.0
       end if
       call get_igcm_atmosphere(r565,-565)
       if (.not.(allocated(r564))) then
       allocate(r564(1:r546+1))
       r564=0.0
       end if
       call get_igcm_atmosphere(r564,-564)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r550,-550)
       call get_igcm_atmosphere(r557,-557)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r190))) then
       allocate(r190(1:r547,1:r546))
       end if
       if (.not.(allocated(r177))) then
       allocate(r177(1:r547,1:r546))
       r177=0.0
       end if
       if (.not.(allocated(r187))) then
       allocate(r187(1:r547,1:r546))
       r187=0.0
       end if
       if (.not.(allocated(r191))) then
       allocate(r191(1:r547,1:r546))
       r191=0.0
       end if
       if (.not.(allocated(r189))) then
       allocate(r189(1:r547,1:r546))
       r189=0.0
       end if
       call start_mpivis_log('igcm_atmosphere initialise_atmos ')
       call initialise_atmos(r547,r546,r163,r164,r165,r166,r565,r168,r564,r170,r548,r172,r173,r174,r175,r550,r177,r557,r179,r180,r181,r182,r183,r184,r185,r186,r187,r188,r189,r190,r191,r76,r78,r194,r195,r196,r77,r61,r62,r63,r201,r202,r203,r204,r205,r206)
       call end_mpivis_log('igcm_atmosphere initialise_atmos ')
       call put_igcm_atmosphere(r163,-163)
       call put_igcm_atmosphere(r164,-164)
       call put_igcm_atmosphere(r565,-565)
       call put_igcm_atmosphere(r564,-564)
       call put_igcm_atmosphere(r550,-550)
       call put_igcm_atmosphere(r557,-557)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(14)
       call start_mpivis_log('counter counter 1')
       call counter(r373)
       call end_mpivis_log('counter counter 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r222,-222)
       call start_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call igcm3_adiab(r207,r208,r209,r210,r211,r187,r213,r214,r215,r216,r217,r76,r219,r220,r221,r222,r223,r224)
       call end_mpivis_log('igcm_atmosphere igcm3_adiab ')
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r222,-222)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(16)
       if (.not.(allocated(r421))) then
       allocate(r421(1:r547,1:r546))
       r421=0.0
       end if
       if (.not.(allocated(r422))) then
       allocate(r422(1:r547,1:r546))
       r422=0.0
       end if
       if (.not.(allocated(r423))) then
       allocate(r423(1:r547,1:r546))
       r423=0.0
       end if
       if (.not.(allocated(r424))) then
       allocate(r424(1:r547,1:r546))
       r424=0.0
       end if
       if (.not.(allocated(r425))) then
       allocate(r425(1:r547,1:r546))
       r425=0.0
       end if
       if (.not.(allocated(r426))) then
       allocate(r426(1:r547,1:r546))
       r426=0.0
       end if
       if (.not.(allocated(r427))) then
       allocate(r427(1:r547,1:r546))
       r427=0.0
       end if
       if (.not.(allocated(r428))) then
       allocate(r428(1:r547,1:r546))
       r428=0.0
       end if
       if (.not.(allocated(r430))) then
       allocate(r430(1:r547,1:r546))
       r430=0.0
       end if
       if (.not.(allocated(r429))) then
       allocate(r429(1:r547,1:r546))
       r429=0.0
       end if
       call start_mpivis_log('transformer1 new_transformer_1 ')
       call new_transformer_1(r547,r546,r207,r208,r209,r210,r187,r211,r421,r422,r423,r424,r425,r426,r427,r428,r429,r430)
       call end_mpivis_log('transformer1 new_transformer_1 ')
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(17)
       call start_mpivis_log('counter_mod counter_mod 1')
       call counter_mod(r374,r173)
       call end_mpivis_log('counter_mod counter_mod 1')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call start_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call igcm_land_surflux(r373,r374,r173,r175,r219,r548,r179,r180,r181,r421,r422,r423,r424,r211,r278,r279,r280,r281,r282,r283,r284,r285,r286,r287,r288,r289,r290,r191,r188,r293,r78,r194,r195,r196,r77,r202,r300,r301)
       call end_mpivis_log('igcm_atmosphere igcm_land_surflux ')
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(19)
       call get_igcm_atmosphere(r548,-548)
       call start_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       call igcm_land_blayer(r374,r173,r175,r220,r548,r279,r280,r281,r282,r421,r422,r423,r424,r211)
       call end_mpivis_log('igcm_atmosphere igcm_land_blayer ')
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(20)
       call start_mpivis_log('counter_mod counter_mod 2')
       call counter_mod(r374,r174)
       call end_mpivis_log('counter_mod counter_mod 2')
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(21)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r557,-557)
       call get_igcm_atmosphere(r550,-550)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call igcm_ocean_surflux(r373,r374,r174,r219,r548,r179,r180,r181,r425,r426,r427,r428,r211,r557,r550,r331,r332,r333,r334,r335,r336,r337,r338,r339,r340,r341,r342,r343,r344)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_surflux ')
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r557,-557)
       call put_igcm_atmosphere(r550,-550)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(22)
       call get_igcm_atmosphere(r548,-548)
       call start_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       call igcm_ocean_blayer(r374,r174,r175,r220,r548,r332,r333,r334,r335,r425,r426,r427,r428,r211)
       call end_mpivis_log('igcm_atmosphere igcm_ocean_blayer ')
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer2(r433,-433)
       call get_transformer2(r548,-548)
       call get_transformer2(r437,-437)
       if (.not.(allocated(r449))) then
       allocate(r449(1:r547,1:r546))
       r449=0.0
       end if
       if (.not.(allocated(r452))) then
       allocate(r452(1:r547,1:r546))
       r452=0.0
       end if
       if (.not.(allocated(r455))) then
       allocate(r455(1:r547,1:r546))
       r455=0.0
       end if
       call start_mpivis_log('transformer2 new_transformer_2 ')
       call new_transformer_2(r547,r546,r433,r548,r283,r336,r437,r284,r337,r440,r285,r338,r443,r286,r339,r446,r287,r340,r449,r288,r341,r452,r289,r342,r455,r290,r343,r177,r191,r344,r207,r421,r425,r208,r422,r426,r209,r423,r427,r210,r424,r428)
       call end_mpivis_log('transformer2 new_transformer_2 ')
       call put_transformer2(r433,-433)
       call put_transformer2(r437,-437)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(24)
       call get_igcm_atmosphere(r433,-433)
       call get_igcm_atmosphere(r437,-437)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r246,-246)
       call start_mpivis_log('igcm_atmosphere igcm3_diab ')
       call igcm3_diab(r207,r208,r209,r210,r177,r455,r449,r452,r433,r437,r440,r443,r179,r180,r181,r196,r215,r287,r61,r62,r63,r246,r201,r223,r249)
       call end_mpivis_log('igcm_atmosphere igcm3_diab ')
       call put_igcm_atmosphere(r433,-433)
       call put_igcm_atmosphere(r437,-437)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r246,-246)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(25)
       call get_transformer3(r475,-475)
       call get_transformer3(r433,-433)
       call get_transformer3(r479,-479)
       call get_transformer3(r437,-437)
       call get_transformer3(r481,-481)
       call get_transformer3(r179,-179)
       call get_transformer3(r483,-483)
       call get_transformer3(r180,-180)
       call get_transformer3(r485,-485)
       call get_transformer3(r487,-487)
       call get_transformer3(r489,-489)
       call start_mpivis_log('transformer3 new_transformer_3 ')
       call new_transformer_3(r547,r546,r475,r433,r477,r352,r479,r437,r481,r179,r483,r180,r485,r440,r487,r443,r489,r181,r491)
       call end_mpivis_log('transformer3 new_transformer_3 ')
       call put_transformer3(r475,-475)
       call put_transformer3(r433,-433)
       call put_transformer3(r479,-479)
       call put_transformer3(r437,-437)
       call put_transformer3(r481,-481)
       call put_transformer3(r179,-179)
       call put_transformer3(r483,-483)
       call put_transformer3(r180,-180)
       call put_transformer3(r485,-485)
       call put_transformer3(r487,-487)
       call put_transformer3(r489,-489)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(29)
       call get_transformer5(r502,-502)
       call get_transformer5(r433,-433)
       call get_transformer5(r549,-549)
       call get_transformer5(r507,-507)
       call get_transformer5(r437,-437)
       call get_transformer5(r509,-509)
       call get_transformer5(r179,-179)
       call get_transformer5(r511,-511)
       call get_transformer5(r180,-180)
       call get_transformer5(r513,-513)
       call get_transformer5(r515,-515)
       call get_transformer5(r517,-517)
       call get_transformer5(r519,-519)
       call get_transformer5(r521,-521)
       call start_mpivis_log('transformer5 new_transformer_5 ')
       call new_transformer_5(r547,r546,r502,r433,r549,r477,r491,r507,r437,r509,r179,r511,r180,r513,r440,r515,r443,r517,r181,r519,r446,r521,r188)
       call end_mpivis_log('transformer5 new_transformer_5 ')
       call put_transformer5(r502,-502)
       call put_transformer5(r433,-433)
       call put_transformer5(r549,-549)
       call put_transformer5(r507,-507)
       call put_transformer5(r437,-437)
       call put_transformer5(r509,-509)
       call put_transformer5(r179,-179)
       call put_transformer5(r511,-511)
       call put_transformer5(r180,-180)
       call put_transformer5(r513,-513)
       call put_transformer5(r515,-515)
       call put_transformer5(r517,-517)
       call put_transformer5(r519,-519)
       call put_transformer5(r521,-521)
       end if
       end if
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(57)
       call start_mpivis_log('igcm_atmosphere end_atmos ')
       call end_atmos()
       call end_mpivis_log('igcm_atmosphere end_atmos ')
       end if
       call finaliseComms()
       end program BFG2Main

