       ! f77 to f90 put/get wrappers start
       subroutine put_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==25) then
       if (tag==-475) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-433) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-479) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-437) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-481) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-179) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-483) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-180) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-485) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-487) then
       call putreal__2_transformer3(data,tag)
       end if
       if (tag==-489) then
       call putreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine put_transformer3
       subroutine get_transformer3(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==25) then
       if (tag==-475) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-433) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-479) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-437) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-481) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-179) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-483) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-180) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-485) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-487) then
       call getreal__2_transformer3(data,tag)
       end if
       if (tag==-489) then
       call getreal__2_transformer3(data,tag)
       end if
       end if
       end subroutine get_transformer3
       subroutine getreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_transformer3
       subroutine putreal__2_transformer3(data,tag)
       use BFG2InPlace_transformer3, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_transformer3
       ! f77 to f90 put/get wrappers end
