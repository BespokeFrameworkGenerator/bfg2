       module BFG2InPlace_ini_weights
       use BFG2Target2
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__1
       end interface
       interface put
       module procedure putreal__1
       end interface
       contains
       subroutine getreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(2) :: pp2p=(/0,0/)
       currentModel=getActiveModel()
       if (currentModel==12) then
       ! I am model=ini_weights and ep=ini_weights and instance=
       if (arg2==-565) then
       ! I am ini_weights.ini_weights..12
       remoteID=-1
       setSize=19
       allocate(set(1:setSize))
       set=(/5,8,10,12,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=12
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,565,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-564) then
       ! I am ini_weights.ini_weights..12
       remoteID=-1
       setSize=19
       allocate(set(1:setSize))
       set=(/5,8,10,12,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=12
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,564,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__1
       subroutine putreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==12) then
       ! I am model=ini_weights and ep=ini_weights and instance=
       if (arg2==-565) then
       ! I am ini_weights.ini_weights..12
       setSize=20
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       ins=(/0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       myID=12
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,565,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,565,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-564) then
       ! I am ini_weights.ini_weights..12
       setSize=20
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       ins=(/0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       myID=12
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,564,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,564,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       end subroutine putreal__1
       end module BFG2InPlace_ini_weights
