       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_interp_ocn_atm, only : put_interp_ocn_atm=>put,&
get_interp_ocn_atm=>get
       use BFG2InPlace_ini_weights, only : put_ini_weights=>put,&
get_ini_weights=>get
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       use BFG2InPlace_weight_check, only : put_weight_check=>put,&
get_weight_check=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !interp_ocn_atm__freq
       integer :: interp_ocn_atm__freq
       !ini_weights__freq
       integer :: ini_weights__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !weight_check__freq
       integer :: weight_check__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_ocn_atm_3_freq
       integer :: interp_ocn_atm_3_freq
       !interp_ocn_atm_4_freq
       integer :: interp_ocn_atm_4_freq
       !interp_ocn_atm_5_freq
       integer :: interp_ocn_atm_5_freq
       !interp_ocn_atm_6_freq
       integer :: interp_ocn_atm_6_freq
       !interp_ocn_atm_7_freq
       integer :: interp_ocn_atm_7_freq
       !interp_ocn_atm_8_freq
       integer :: interp_ocn_atm_8_freq
       !interp_ocn_atm_9_freq
       integer :: interp_ocn_atm_9_freq
       !interp_ocn_atm_10_freq
       integer :: interp_ocn_atm_10_freq
       !interp_ocn_atm_11_freq
       integer :: interp_ocn_atm_11_freq
       !interp_ocn_atm_12_freq
       integer :: interp_ocn_atm_12_freq
       !interp_ocn_atm_13_freq
       integer :: interp_ocn_atm_13_freq
       !interp_ocn_atm_14_freq
       integer :: interp_ocn_atm_14_freq
       !interp_ocn_atm_15_freq
       integer :: interp_ocn_atm_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy__freq
       integer :: copy_dummy__freq
       !interp_ocn_atm_16_freq
       integer :: interp_ocn_atm_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_ocn_atm_17_freq
       integer :: interp_ocn_atm_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,interp_ocn_atm__freq,ini_weights__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,weight_check__freq,counter_2_freq,interp_ocn_atm_3_freq,interp_ocn_atm_4_freq,interp_ocn_atm_5_freq,interp_ocn_atm_6_freq,interp_ocn_atm_7_freq,interp_ocn_atm_8_freq,interp_ocn_atm_9_freq,interp_ocn_atm_10_freq,interp_ocn_atm_11_freq,interp_ocn_atm_12_freq,interp_ocn_atm_13_freq,interp_ocn_atm_14_freq,interp_ocn_atm_15_freq,counter_3_freq,goldstein__freq,copy_dummy__freq,interp_ocn_atm_16_freq,copy_tstar_2_freq,interp_ocn_atm_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !olon2
       real, dimension(1:36) :: r81
       !olat2
       real, dimension(1:36) :: r82
       !olon3
       real, dimension(1:36) :: r83
       !olat3
       real, dimension(1:36) :: r84
       !oboxedge2_lon
       real, dimension(1:37) :: r87
       !oboxedge2_lat
       real, dimension(1:37) :: r88
       !oboxedge3_lon
       real, dimension(1:37) :: r89
       !oboxedge3_lat
       real, dimension(1:37) :: r90
       !depth
       real, dimension(1:8) :: r91
       !depth1
       real, dimension(1:9) :: r92
       !ilandmask2
       integer, dimension(1:36,1:36) :: r94
       !ilandmask3
       integer, dimension(1:36,1:36) :: r95
       !ias_out
       integer, dimension(1:36) :: r102
       !iaf_out
       integer, dimension(1:36) :: r103
       !ips_out
       integer, dimension(1:36) :: r104
       !ipf_out
       integer, dimension(1:36) :: r105
       !jsf_out
       integer :: r106
       !go_rhosc
       real :: r119
       !go_scf
       real :: r121
       !go_k1
       integer, dimension(1:36,1:36) :: r122
       !go_dz
       real, dimension(1:8) :: r123
       !go_dza
       real, dimension(1:8) :: r124
       !go_ias
       integer, dimension(1:36) :: r125
       !go_iaf
       integer, dimension(1:36) :: r126
       !go_c
       real, dimension(0:36) :: r128
       !go_cv
       real, dimension(0:36) :: r129
       !go_s
       real, dimension(0:36) :: r130
       !go_sv
       real, dimension(0:36) :: r131
       !test_energy_ocean
       real :: r153
       !test_water_ocean
       real :: r154
       !koverall
       integer :: r155
       !go_cost
       real, dimension(1:36,1:36) :: r158
       !go_u
       real, dimension(1:3,1:36,1:36,1:8) :: r159
       !go_tau
       real, dimension(1:2,1:36,1:36) :: r160
       ! Set Notation Vars
       !write_flag_atm
       logical :: r1
       !write_flag_ocn
       logical :: r2
       !write_flag_sic
       logical :: r3
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r548
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !alon1_atm
       real, dimension(1:64) :: r163
       !alat1_atm
       real, dimension(1:32) :: r164
       !aboxedge1_lon_atm
       real, allocatable, dimension(:) :: r565
       !aboxedge1_lat_atm
       real, allocatable, dimension(:) :: r564
       !tstar_atm
       real, dimension(1:64,1:32) :: r550
       !albedo_atm
       real, dimension(1:64,1:32) :: r557
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r549
       !conductflux_atm
       real, dimension(1:64,1:32) :: r348
       !test_energy_seaice
       real :: r350
       !test_water_seaice
       real :: r351
       !ksic_loop
       integer :: r352
       !alon1_ocn
       real, dimension(1:36) :: r79
       !alat1_ocn
       real, dimension(1:36) :: r80
       !aboxedge1_lon_ocn
       real, allocatable, dimension(:) :: r567
       !aboxedge1_lat_ocn
       real, allocatable, dimension(:) :: r566
       !ilandmask1_ocn
       integer, dimension(1:36,1:36) :: r93
       !tstar_ocn
       real, dimension(1:36,1:36) :: r97
       !sstar_ocn
       real, dimension(1:36,1:36) :: r98
       !ustar_ocn
       real, dimension(1:36,1:36) :: r99
       !vstar_ocn
       real, dimension(1:36,1:36) :: r100
       !albedo_ocn
       real, dimension(1:36,1:36) :: r101
       !go_ts
       real, dimension(1:14,1:36,1:36,1:8) :: r132
       !go_ts1
       real, dimension(1:14,1:36,1:36,1:8) :: r133
       !iwork1_atm
       integer, allocatable, dimension(:,:) :: r382
       !iwork1_ocn
       integer, allocatable, dimension(:,:) :: r383
       !interpmask_atm
       real, allocatable, dimension(:,:) :: r384
       !weighttot_atm
       real :: r385
       !interpmask_ocn
       real, allocatable, dimension(:,:) :: r386
       !weighttot_ocn
       real :: r387
       !dummy_atm
       real, dimension(1:64,1:32) :: r551
       !weight_atm_weights
       real, allocatable, dimension(:,:) :: r391
       !weight_atm_global
       real, allocatable, dimension(:,:) :: r568
       !weight_ocn_global
       real, allocatable, dimension(:,:) :: r569
       !weight_ocn_weights
       real, allocatable, dimension(:,:) :: r392
       !iconv_ice
       integer :: r222
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r433
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r437
       !iconv_che
       integer :: r246
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r475
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r479
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r481
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r483
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r485
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r487
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r489
       !kocn_loop
       integer :: r491
       !istep_sic
       integer :: r948
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r494
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r498
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r502
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r507
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r509
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r511
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r513
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r515
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r517
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r519
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r521
       !istep_ocn
       integer :: r1523
       !seaicefrac_ocn
       real, allocatable, dimension(:,:) :: r1539
       !conductflux_ocn
       real, allocatable, dimension(:,:) :: r2114
       !latent_ocn
       real, allocatable, dimension(:,:) :: r2689
       !netsolar_ocn
       real, allocatable, dimension(:,:) :: r3264
       !netlong_ocn
       real, allocatable, dimension(:,:) :: r3839
       !sensible_ocn
       real, allocatable, dimension(:,:) :: r4414
       !ocean_stressx2_ocn
       real, allocatable, dimension(:,:) :: r4989
       !ocean_stressx3_ocn
       real, allocatable, dimension(:,:) :: r5564
       !ocean_stressy2_ocn
       real, allocatable, dimension(:,:) :: r6139
       !ocean_stressy3_ocn
       real, allocatable, dimension(:,:) :: r6714
       !precip_ocn
       real, allocatable, dimension(:,:) :: r7289
       !evap_ocn
       real, allocatable, dimension(:,:) :: r7864
       !runoff_ocn
       real, allocatable, dimension(:,:) :: r8439
       !dumtmp_ocn
       real, allocatable, dimension(:,:) :: r572
       !dumalb_ocn
       real, allocatable, dimension(:,:) :: r574
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r143
       !istep_che
       integer :: r2098
       !istep_lic
       integer :: r2673
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !ilon1_atm
       integer :: r547
       !ilat1_atm
       integer :: r546
       !ilon1_ocn
       integer :: r561
       !ilat1_ocn
       integer :: r562
       !itype-2
       integer :: r390
       !itype2
       integer :: r1540
       !itype4
       integer :: r2115
       !outputdir_name
       character(len=200) :: r60
       !fname_restart_main
       character(len=200) :: r58
       !dt_write
       integer :: r59
       !genie_timestep
       real :: r57
       !lrestart_genie
       logical :: r107
       !totsteps
       integer :: r96
       !go_saln0
       real :: r108
       !go_rhoair
       real :: r109
       !go_cd
       real :: r110
       !go_ds
       real, dimension(1:36) :: r111
       !go_dphi
       real :: r112
       !go_ips
       integer, dimension(1:36) :: r113
       !go_ipf
       integer, dimension(1:36) :: r114
       !go_usc
       real :: r115
       !go_dsc
       real :: r116
       !go_fsc
       real :: r117
       !go_rh0sc
       real :: r118
       !temptop_atm
       real, dimension(1:64,1:32) :: r552
       !plumin
       real :: r563
       !go_jsf
       integer :: r127
       !go_cpsc
       real :: r120
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r1=.true.
           r2=.true.
           r3=.false.
           r548=0
           r76=0.0
           r77=0.0
           r78=0.0
           r61=0.0
           r62=0.0
           r63=0.0
           r163=0.0
           r164=0.0
           ! 565is allocatable; cannot be written to yet
           ! 564is allocatable; cannot be written to yet
           r550=0.0
           r557=0.0
           r179=0.0
           r180=0.0
           r549=0.0
           r348=0.0
           r350=0.0
           r351=0.0
           r352=6
           r79=0.0
           r80=0.0
           ! 567is allocatable; cannot be written to yet
           ! 566is allocatable; cannot be written to yet
           r93=0
           r97=0.0
           r98=0.0
           r99=0.0
           r100=0.0
           r101=0.0
           r132=0.0
           r133=0.0
           ! 382is allocatable; cannot be written to yet
           ! 383is allocatable; cannot be written to yet
           ! 384is allocatable; cannot be written to yet
           r385=0.0
           ! 386is allocatable; cannot be written to yet
           r387=0.0
           r551=0.0
           ! 391is allocatable; cannot be written to yet
           ! 568is allocatable; cannot be written to yet
           ! 569is allocatable; cannot be written to yet
           ! 392is allocatable; cannot be written to yet
           r222=0.0
           r433=0.0
           r437=0.0
           r246=0
           r475=0.0
           r479=0.0
           r481=0.0
           r483=0.0
           r485=0.0
           r487=0.0
           r489=0.0
           r491=48
           r948=0
           r494=0.0
           r498=0.0
           r502=0.0
           r507=0.0
           r509=0.0
           r511=0.0
           r513=0.0
           r515=0.0
           r517=0.0
           r519=0.0
           r521=0.0
           r1523=0
           ! 1539is allocatable; cannot be written to yet
           ! 2114is allocatable; cannot be written to yet
           ! 2689is allocatable; cannot be written to yet
           ! 3264is allocatable; cannot be written to yet
           ! 3839is allocatable; cannot be written to yet
           ! 4414is allocatable; cannot be written to yet
           ! 4989is allocatable; cannot be written to yet
           ! 5564is allocatable; cannot be written to yet
           ! 6139is allocatable; cannot be written to yet
           ! 6714is allocatable; cannot be written to yet
           ! 7289is allocatable; cannot be written to yet
           ! 7864is allocatable; cannot be written to yet
           ! 8439is allocatable; cannot be written to yet
           ! 572is allocatable; cannot be written to yet
           ! 574is allocatable; cannot be written to yet
           r143=0.0
           r2098=0
           r2673=0
           r41=0.0
           r547=64
           r546=32
           r561=36
           r562=36
           r390=-2
           r1540=2
           r2115=4
           r60='/home/armstroc/genie_output/genie_ig_go_sl_dyex_test/main'
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r59=720
           r57=3600.0
           r107=.false.
           r96=86400
           r108=0.0
           r109=1.25
           r110=0.0013
           r111=0.0
           r112=0.0
           r113=0
           r114=0
           r115=0.0
           r116=5000.0
           r117=2*7.2921e-5
           r118=1000.0
           r552=0.0
           r563=0.0
           r127=0
           r120=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call start_mpivis_log('bfg_averages ini_averages ')
       call bfg_averages_ini_averages_init(r1,r2,r3)
       call end_mpivis_log('bfg_averages ini_averages ')
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call start_mpivis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ')
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r548,r76,r77,r78)
       call end_mpivis_log('initialise_fixedicesheet_mod initialise_fixedicesheet ')
       call put_initialise_fixedicesheet_mod(r548,-548)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call start_mpivis_log('fixed_chemistry initialise_fixedchem ')
       call initialise_fixedchem(r61,r62,r63)
       call end_mpivis_log('fixed_chemistry initialise_fixedchem ')
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r557,-557)
       call get_slab_seaice(r549,-549)
       call start_mpivis_log('slab_seaice initialise_slabseaice ')
       call initialise_slabseaice(r550,r557,r549,r348,r548,r350,r351,r352)
       call end_mpivis_log('slab_seaice initialise_slabseaice ')
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r557,-557)
       call put_slab_seaice(r549,-549)
       end if
       if (goldsteinThread()) then
       call setActiveModel(7)
       if (.not.(allocated(r567))) then
       allocate(r567(1:r561+1))
       r567=0.0
       end if
       if (.not.(allocated(r566))) then
       allocate(r566(1:r562+1))
       r566=0.0
       end if
       call start_mpivis_log('goldstein initialise_goldstein ')
       call initialise_goldstein(r79,r80,r81,r82,r83,r84,r567,r566,r87,r88,r89,r90,r91,r92,r93,r94,r95,r96,r97,r98,r99,r100,r101,r102,r103,r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r120,r121,r122,r123,r124,r125,r126,r127,r128,r129,r130,r131,r132,r133)
       call end_mpivis_log('goldstein initialise_goldstein ')
       end if
       if (interp_ocn_atminst1Thread()) then
       call setActiveModel(8)
       if (.not.(allocated(r565))) then
       allocate(r565(1:r547+1))
       r565=0.0
       end if
       call get_interp_ocn_atm(r565,-565)
       if (.not.(allocated(r564))) then
       allocate(r564(1:r546+1))
       r564=0.0
       end if
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r382))) then
       allocate(r382(1:r547,1:r546))
       r382=0
       end if
       if (.not.(allocated(r383))) then
       allocate(r383(1:r561,1:r562))
       r383=0
       end if
       if (.not.(allocated(r384))) then
       allocate(r384(1:r547,1:r546))
       r384=0.0
       end if
       if (.not.(allocated(r386))) then
       allocate(r386(1:r561,1:r562))
       r386=0.0
       end if
       if (.not.(allocated(r391))) then
       allocate(r391(1:r547,1:r546))
       r391=0.0
       end if
       if (.not.(allocated(r392))) then
       allocate(r392(1:r561,1:r562))
       r392=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 1')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r97,r390,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 1')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       if (copy_tstarinst1Thread()) then
       call setActiveModel(9)
       call get_copy_tstar(r549,-549)
       call get_copy_tstar(r550,-550)
       call start_mpivis_log('copy_tstar copytstar 1')
       call copytstar(r546,r547,r548,r549,r550,r551,r552)
       call end_mpivis_log('copy_tstar copytstar 1')
       call put_copy_tstar(r550,-550)
       end if
       if (interp_ocn_atminst2Thread()) then
       call setActiveModel(10)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 2')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r101,r390,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 2')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       if (copy_albedoinst1Thread()) then
       call setActiveModel(11)
       call get_copy_albedo(r549,-549)
       call get_copy_albedo(r557,-557)
       call start_mpivis_log('copy_albedo copyalbedo 1')
       call copyalbedo(r546,r547,r548,r549,r557,r551)
       call end_mpivis_log('copy_albedo copyalbedo 1')
       call put_copy_albedo(r557,-557)
       end if
       if (ini_weightsThread()) then
       call setActiveModel(12)
       call get_ini_weights(r565,-565)
       call get_ini_weights(r564,-564)
       call start_mpivis_log('ini_weights ini_weights ')
       call ini_weights(r565,r564,r567,r566,r548,r93,r384,r385,r386,r387,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('ini_weights ini_weights ')
       call put_ini_weights(r565,-565)
       call put_ini_weights(r564,-564)
       end if
       if (weight_checkThread()) then
       call setActiveModel(13)
       call get_weight_check(r564,-564)
       call get_weight_check(r565,-565)
       if (.not.(allocated(r568))) then
       allocate(r568(1:r547,1:r546))
       r568=0.0
       end if
       if (.not.(allocated(r569))) then
       allocate(r569(1:r561,1:r562))
       r569=0.0
       end if
       call start_mpivis_log('weight_check weightcheck ')
       call weightcheck(r547,r546,r561,r562,r563,r564,r565,r566,r567,r568,r569)
       call end_mpivis_log('weight_check weightcheck ')
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(26)
       call start_mpivis_log('counter counter 2')
       call counter(r948)
       call end_mpivis_log('counter counter 2')
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(27)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r475,-475)
       call get_slab_seaice(r479,-479)
       call get_slab_seaice(r481,-481)
       call get_slab_seaice(r483,-483)
       call get_slab_seaice(r433,-433)
       call get_slab_seaice(r437,-437)
       call get_slab_seaice(r179,-179)
       call get_slab_seaice(r180,-180)
       call get_slab_seaice(r485,-485)
       call get_slab_seaice(r487,-487)
       call get_slab_seaice(r549,-549)
       call get_slab_seaice(r557,-557)
       call start_mpivis_log('slab_seaice slabseaice ')
       call slabseaice(r948,r550,r475,r479,r481,r483,r433,r437,r179,r180,r485,r487,r549,r552,r348,r557,r548,r350,r351,r352)
       call end_mpivis_log('slab_seaice slabseaice ')
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r475,-475)
       call put_slab_seaice(r479,-479)
       call put_slab_seaice(r481,-481)
       call put_slab_seaice(r483,-483)
       call put_slab_seaice(r433,-433)
       call put_slab_seaice(r437,-437)
       call put_slab_seaice(r179,-179)
       call put_slab_seaice(r180,-180)
       call put_slab_seaice(r485,-485)
       call put_slab_seaice(r487,-487)
       call put_slab_seaice(r549,-549)
       call put_slab_seaice(r557,-557)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(28)
       call get_transformer4(r549,-549)
       call start_mpivis_log('transformer4 new_transformer_4 ')
       call new_transformer_4(r547,r546,r494,r549,r352,r491,r498,r348)
       call end_mpivis_log('transformer4 new_transformer_4 ')
       call put_transformer4(r549,-549)
       end if
       end if
       if (interp_ocn_atminst3Thread()) then
       if(mod(its1,interp_ocn_atm_3_freq).eq.0)then
       call setActiveModel(30)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r1539))) then
       allocate(r1539(1:r561,1:r562))
       r1539=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 3')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r494,r1539,r1540,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 3')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (interp_ocn_atminst4Thread()) then
       if(mod(its1,interp_ocn_atm_4_freq).eq.0)then
       call setActiveModel(31)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r2114))) then
       allocate(r2114(1:r561,1:r562))
       r2114=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 4')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r498,r2114,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 4')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (interp_ocn_atminst5Thread()) then
       if(mod(its1,interp_ocn_atm_5_freq).eq.0)then
       call setActiveModel(32)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r502,-502)
       if (.not.(allocated(r2689))) then
       allocate(r2689(1:r561,1:r562))
       r2689=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 5')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r502,r2689,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 5')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r502,-502)
       end if
       end if
       if (interp_ocn_atminst6Thread()) then
       if(mod(its1,interp_ocn_atm_6_freq).eq.0)then
       call setActiveModel(33)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r509,-509)
       if (.not.(allocated(r3264))) then
       allocate(r3264(1:r561,1:r562))
       r3264=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 6')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r509,r3264,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 6')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r509,-509)
       end if
       end if
       if (interp_ocn_atminst7Thread()) then
       if(mod(its1,interp_ocn_atm_7_freq).eq.0)then
       call setActiveModel(34)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r511,-511)
       if (.not.(allocated(r3839))) then
       allocate(r3839(1:r561,1:r562))
       r3839=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 7')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r511,r3839,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 7')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r511,-511)
       end if
       end if
       if (interp_ocn_atminst8Thread()) then
       if(mod(its1,interp_ocn_atm_8_freq).eq.0)then
       call setActiveModel(35)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r507,-507)
       if (.not.(allocated(r4414))) then
       allocate(r4414(1:r561,1:r562))
       r4414=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 8')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r507,r4414,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 8')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r507,-507)
       end if
       end if
       if (interp_ocn_atminst9Thread()) then
       if(mod(its1,interp_ocn_atm_9_freq).eq.0)then
       call setActiveModel(36)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r513,-513)
       if (.not.(allocated(r4989))) then
       allocate(r4989(1:r561,1:r562))
       r4989=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 9')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r513,r4989,r1540,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 9')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r513,-513)
       end if
       end if
       if (interp_ocn_atminst10Thread()) then
       if(mod(its1,interp_ocn_atm_10_freq).eq.0)then
       call setActiveModel(37)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r513,-513)
       if (.not.(allocated(r5564))) then
       allocate(r5564(1:r561,1:r562))
       r5564=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 10')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r513,r5564,r1540,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 10')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r513,-513)
       end if
       end if
       if (interp_ocn_atminst11Thread()) then
       if(mod(its1,interp_ocn_atm_11_freq).eq.0)then
       call setActiveModel(38)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r515,-515)
       if (.not.(allocated(r6139))) then
       allocate(r6139(1:r561,1:r562))
       r6139=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 11')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r515,r6139,r1540,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 11')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r515,-515)
       end if
       end if
       if (interp_ocn_atminst12Thread()) then
       if(mod(its1,interp_ocn_atm_12_freq).eq.0)then
       call setActiveModel(39)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r515,-515)
       if (.not.(allocated(r6714))) then
       allocate(r6714(1:r561,1:r562))
       r6714=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 12')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r515,r6714,r1540,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 12')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r515,-515)
       end if
       end if
       if (interp_ocn_atminst13Thread()) then
       if(mod(its1,interp_ocn_atm_13_freq).eq.0)then
       call setActiveModel(40)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r517,-517)
       if (.not.(allocated(r7289))) then
       allocate(r7289(1:r561,1:r562))
       r7289=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 13')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r517,r7289,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 13')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r517,-517)
       end if
       end if
       if (interp_ocn_atminst14Thread()) then
       if(mod(its1,interp_ocn_atm_14_freq).eq.0)then
       call setActiveModel(41)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r519,-519)
       if (.not.(allocated(r7864))) then
       allocate(r7864(1:r561,1:r562))
       r7864=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 14')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r519,r7864,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 14')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r519,-519)
       end if
       end if
       if (interp_ocn_atminst15Thread()) then
       if(mod(its1,interp_ocn_atm_15_freq).eq.0)then
       call setActiveModel(42)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r521,-521)
       if (.not.(allocated(r8439))) then
       allocate(r8439(1:r561,1:r562))
       r8439=0.0
       end if
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 15')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r521,r8439,r2115,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 15')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r521,-521)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(43)
       call start_mpivis_log('counter counter 3')
       call counter(r1523)
       call end_mpivis_log('counter counter 3')
       end if
       end if
       if (goldsteinThread()) then
       if(mod(its1,goldstein__freq).eq.0)then
       call setActiveModel(44)
       call start_mpivis_log('goldstein goldstein ')
       call goldstein(r1523,r2689,r4414,r3264,r3839,r2114,r7864,r7289,r8439,r143,r4989,r6139,r5564,r6714,r97,r98,r99,r100,r101,r153,r154,r155,r132,r133,r158,r159,r160)
       call end_mpivis_log('goldstein goldstein ')
       end if
       end if
       if (copy_dummyThread()) then
       if(mod(its1,copy_dummy__freq).eq.0)then
       call setActiveModel(45)
       if (.not.(allocated(r572))) then
       allocate(r572(1:r561,1:r562))
       r572=0.0
       end if
       if (.not.(allocated(r574))) then
       allocate(r574(1:r561,1:r562))
       r574=0.0
       end if
       call start_mpivis_log('copy_dummy copy_dummy ')
       call copy_dummy(r561,r562,r572,r97,r574,r101)
       call end_mpivis_log('copy_dummy copy_dummy ')
       end if
       end if
       if (interp_ocn_atminst16Thread()) then
       if(mod(its1,interp_ocn_atm_16_freq).eq.0)then
       call setActiveModel(46)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 16')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r572,r390,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 16')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (copy_tstarinst2Thread()) then
       if(mod(its1,copy_tstar_2_freq).eq.0)then
       call setActiveModel(47)
       call get_copy_tstar(r549,-549)
       call get_copy_tstar(r550,-550)
       call start_mpivis_log('copy_tstar copytstar 2')
       call copytstar(r546,r547,r548,r549,r550,r551,r552)
       call end_mpivis_log('copy_tstar copytstar 2')
       call put_copy_tstar(r550,-550)
       end if
       end if
       if (interp_ocn_atminst17Thread()) then
       if(mod(its1,interp_ocn_atm_17_freq).eq.0)then
       call setActiveModel(48)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call start_mpivis_log('interp_ocn_atm interp_ocn_atm 17')
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r574,r390,r391,r392,r547,r546,r561,r562)
       call end_mpivis_log('interp_ocn_atm interp_ocn_atm 17')
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (copy_albedoinst2Thread()) then
       if(mod(its1,copy_albedo_2_freq).eq.0)then
       call setActiveModel(49)
       call get_copy_albedo(r549,-549)
       call get_copy_albedo(r557,-557)
       call start_mpivis_log('copy_albedo copyalbedo 2')
       call copyalbedo(r546,r547,r548,r549,r557,r551)
       call end_mpivis_log('copy_albedo copyalbedo 2')
       call put_copy_albedo(r557,-557)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(50)
       call start_mpivis_log('counter counter 4')
       call counter(r2098)
       call end_mpivis_log('counter counter 4')
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(51)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r246,-246)
       call start_mpivis_log('fixed_chemistry fixedchem ')
       call fixedchem(r2098,r61,r62,r63,r246)
       call end_mpivis_log('fixed_chemistry fixedchem ')
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r246,-246)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(52)
       call start_mpivis_log('counter counter 5')
       call counter(r2673)
       call end_mpivis_log('counter counter 5')
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(53)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r222,-222)
       call start_mpivis_log('fixed_icesheet fixedicesheet ')
       call fixedicesheet(r2673,r548,r76,r77,r78,r222)
       call end_mpivis_log('fixed_icesheet fixedicesheet ')
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r222,-222)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(54)
       call get_bfg_averages(r163,-163)
       call get_bfg_averages(r164,-164)
       call get_bfg_averages(r509,-509)
       call get_bfg_averages(r511,-511)
       call get_bfg_averages(r507,-507)
       call get_bfg_averages(r502,-502)
       call get_bfg_averages(r513,-513)
       call get_bfg_averages(r515,-515)
       call get_bfg_averages(r519,-519)
       call get_bfg_averages(r517,-517)
       call get_bfg_averages(r521,-521)
       call get_bfg_averages(r550,-550)
       call get_bfg_averages(r557,-557)
       call start_mpivis_log('bfg_averages write_averages ')
       call bfg_averages_write_averages_iteration(r1523,r163,r164,r79,r80,r9,r10,r509,r3264,r13,r511,r3839,r16,r507,r4414,r19,r502,r2689,r22,r513,r4989,r25,r515,r6139,r28,r498,r2114,r31,r519,r7864,r34,r517,r7289,r37,r521,r8439,r40,r41,r143,r43,r494,r1539,r46,r550,r97,r49,r557,r101,r52,r1,r2,r3,r491,r57,r58,r59,r60)
       call end_mpivis_log('bfg_averages write_averages ')
       call put_bfg_averages(r163,-163)
       call put_bfg_averages(r164,-164)
       call put_bfg_averages(r509,-509)
       call put_bfg_averages(r511,-511)
       call put_bfg_averages(r507,-507)
       call put_bfg_averages(r502,-502)
       call put_bfg_averages(r513,-513)
       call put_bfg_averages(r515,-515)
       call put_bfg_averages(r519,-519)
       call put_bfg_averages(r517,-517)
       call put_bfg_averages(r521,-521)
       call put_bfg_averages(r550,-550)
       call put_bfg_averages(r557,-557)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(55)
       call get_transformer6(r475,-475)
       call get_transformer6(r479,-479)
       call get_transformer6(r481,-481)
       call get_transformer6(r483,-483)
       call get_transformer6(r485,-485)
       call get_transformer6(r487,-487)
       call get_transformer6(r489,-489)
       call start_mpivis_log('transformer6 new_transformer_6 ')
       call new_transformer_6(r547,r546,r475,r479,r481,r483,r485,r487,r489)
       call end_mpivis_log('transformer6 new_transformer_6 ')
       call put_transformer6(r475,-475)
       call put_transformer6(r479,-479)
       call put_transformer6(r481,-481)
       call put_transformer6(r483,-483)
       call put_transformer6(r485,-485)
       call put_transformer6(r487,-487)
       call put_transformer6(r489,-489)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(56)
       call get_transformer7(r502,-502)
       call get_transformer7(r507,-507)
       call get_transformer7(r509,-509)
       call get_transformer7(r511,-511)
       call get_transformer7(r513,-513)
       call get_transformer7(r515,-515)
       call get_transformer7(r517,-517)
       call get_transformer7(r519,-519)
       call get_transformer7(r521,-521)
       call start_mpivis_log('transformer7 new_transformer_7 ')
       call new_transformer_7(r547,r546,r502,r507,r509,r511,r513,r515,r517,r519,r521,r494,r498,r41)
       call end_mpivis_log('transformer7 new_transformer_7 ')
       call put_transformer7(r502,-502)
       call put_transformer7(r507,-507)
       call put_transformer7(r509,-509)
       call put_transformer7(r511,-511)
       call put_transformer7(r513,-513)
       call put_transformer7(r515,-515)
       call put_transformer7(r517,-517)
       call put_transformer7(r519,-519)
       call put_transformer7(r521,-521)
       end if
       end if
       end do
       if (goldsteinThread()) then
       call setActiveModel(58)
       call start_mpivis_log('goldstein end_goldstein ')
       call end_goldstein()
       call end_mpivis_log('goldstein end_goldstein ')
       end if
       call finaliseComms()
       end program BFG2Main

