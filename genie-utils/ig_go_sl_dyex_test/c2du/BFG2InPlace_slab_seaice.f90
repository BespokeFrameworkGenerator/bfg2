       module BFG2InPlace_slab_seaice
       use BFG2Target2
       use mpi
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(16) :: pp2p=(/0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1/)
       currentModel=getActiveModel()
       if (currentModel==6) then
       ! I am model=slab_seaice and ep=initialise_slabseaice and instance=
       if (arg2==-550) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,550,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-557) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,557,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-549) then
       ! I am slab_seaice.initialise_slabseaice..6
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/6,27,28,29/)
       myID=6
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,549,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==27) then
       ! I am model=slab_seaiceand ep=slabseaice and instance=
       if (arg2==-550) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remotesource
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,550,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-475) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,475,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-479) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,479,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-481) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,481,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-483) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,483,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-433) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/23,24,25,27,29/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,433,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-437) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/23,24,25,27,29/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(10).eq.1) then
       pp2p(10)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,437,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-179) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(11).eq.1) then
       pp2p(11)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,179,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-180) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(12).eq.1) then
       pp2p(12)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,180,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-485) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(13).eq.1) then
       pp2p(13)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,485,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-487) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=3
       allocate(set(1:setSize))
       set=(/25,27,55/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(14).eq.1) then
       pp2p(14)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,487,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-549) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/6,27,28,29/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(15).eq.1) then
       pp2p(15)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,549,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-557) then
       ! I am slab_seaice.slabseaice..27
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       myID=27
       myDU=info(myID)%du
       if (pp2p(16).eq.1) then
       pp2p(16)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,557,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==6) then
       ! I am model=slab_seaice and ep=initialise_slabseaice and instance=
       if (arg2==-550) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-557) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-549) then
       ! I am slab_seaice.initialise_slabseaice..6
       setSize=8
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,9,11,27,28,29,47,49/)
       ins=(/0,1,1,0,0,0,1,1/)
       outs=(/0,0,0,0,0,0,0,0/)
       myID=6
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,549,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,549,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       if (currentModel==27) then
       ! I am model=slab_seaice and ep=slabseaice and instance=
       if (arg2==-550) then
       ! I am slab_seaice.slabseaice..27
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-475) then
       ! I am slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,475,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,475,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-479) then
       ! I am slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,479,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,479,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-481) then
       ! I am slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,481,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send toremote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,481,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-483) then
       ! I am slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,483,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,483,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-433) then
       ! I am slab_seaice.slabseaice..27
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/23,24,25,27,29/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,433,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,433,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-437) then
       ! I am slab_seaice.slabseaice..27
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/23,24,25,27,29/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,437,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,437,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-179) then
       ! I am slab_seaice.slabseaice..27
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-180) then
       ! I am slab_seaice.slabseaice..27
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-485) then
       ! I am slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,485,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,485,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-487) then
       ! Iam slab_seaice.slabseaice..27
       setSize=3
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/25,27,55/)
       ins=(/0,0,0/)
       outs=(/0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send toremote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,487,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,487,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-549) then
       ! I am slab_seaice.slabseaice..27
       setSize=8
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/6,9,11,27,28,29,47,49/)
       ins=(/0,1,1,0,0,0,1,1/)
       outs=(/0,0,0,0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,549,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,549,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-557) then
       ! I am slab_seaice.slabseaice..27
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=27
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_slab_seaice
