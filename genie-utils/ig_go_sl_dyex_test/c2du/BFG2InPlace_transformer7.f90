       module BFG2InPlace_transformer7
       use BFG2Target2
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__2
       end interface
       interface put
       module procedure putreal__2
       end interface
       contains
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(9) :: pp2p=(/0,0,0,0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==56) then
       ! I am model=transformer7 and ep=new_transformer_7 and instance=
       if (arg2==-502) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,32,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,502,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-507) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,35,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,507,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-509) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,33,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,509,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-511) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,34,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,511,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-513) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/29,36,37,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remotesource
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,513,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-515) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/29,38,39,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,515,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-517) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,40,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,517,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-519) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,41,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,519,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-521) then
       ! I am transformer7.new_transformer_7..56
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/29,42,54,56/)
       myID=56
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,521,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__2
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==56) then
       ! I am model=transformer7 and ep=new_transformer_7 and instance=
       if (arg2==-502) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,32,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,502,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,502,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-507) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,35,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,507,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,507,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-509) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,33,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,509,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,509,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-511) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,34,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,511,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,511,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-513) then
       ! I am transformer7.new_transformer_7..56
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,36,37,54,56/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,513,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,513,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-515) then
       ! I am transformer7.new_transformer_7..56
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,38,39,54,56/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,515,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,515,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-517) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,40,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,517,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,517,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-519) then
       ! I amtransformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,41,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,519,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,519,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       if (arg2==-521) then
       ! I am transformer7.new_transformer_7..56
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/29,42,54,56/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=56
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,521,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,521,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine putreal__2
       end module BFG2InPlace_transformer7
