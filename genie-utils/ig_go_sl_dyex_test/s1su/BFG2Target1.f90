       module BFG2Target1
       use bfg_averages, only : bfg_averages_ini_averages_init=>ini_averages,&
bfg_averages_write_averages_iteration=>write_averages
       use initialise_fixedicesheet_mod, only : initialise_fixedicesheet_mod_initialise_fixedicesheet_init=>initialise_fixedicesheet
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       !its2
       integer, target :: its2
       !its3
       integer, target :: its3
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency supportroutines start
       logical function bfg_averagesThread()
       implicit none
       ! only one threadso always true
       bfg_averagesThread=.true.
       end function bfg_averagesThread
       logical function fixed_chemistryThread()
       implicit none
       ! only one thread so always true
       fixed_chemistryThread=.true.
       end function fixed_chemistryThread
       logical function goldsteinThread()
       implicit none
       ! only one thread so always true
       goldsteinThread=.true.
       end function goldsteinThread
       logical function fixed_icesheetThread()
       implicit none
       ! only one thread so always true
       fixed_icesheetThread=.true.
       end function fixed_icesheetThread
       logical function initialise_fixedicesheet_modThread()
       implicit none
       ! only one thread so always true
       initialise_fixedicesheet_modThread=.true.
       end function initialise_fixedicesheet_modThread
       logical function interp_ocn_atminst1Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst1Thread=.true.
       end function interp_ocn_atminst1Thread
       logical function interp_ocn_atminst2Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst2Thread=.true.
       end function interp_ocn_atminst2Thread
       logical function interp_ocn_atminst3Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst3Thread=.true.
       end function interp_ocn_atminst3Thread
       logical function interp_ocn_atminst4Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst4Thread=.true.
       end function interp_ocn_atminst4Thread
       logical function interp_ocn_atminst5Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst5Thread=.true.
       end function interp_ocn_atminst5Thread
       logical function interp_ocn_atminst6Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst6Thread=.true.
       end function interp_ocn_atminst6Thread
       logical function interp_ocn_atminst7Thread()
       implicit none
       ! only one thread so alwaystrue
       interp_ocn_atminst7Thread=.true.
       end function interp_ocn_atminst7Thread
       logical function interp_ocn_atminst8Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst8Thread=.true.
       end function interp_ocn_atminst8Thread
       logical function interp_ocn_atminst9Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst9Thread=.true.
       end function interp_ocn_atminst9Thread
       logical function interp_ocn_atminst10Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst10Thread=.true.
       end function interp_ocn_atminst10Thread
       logical function interp_ocn_atminst11Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst11Thread=.true.
       end function interp_ocn_atminst11Thread
       logical function interp_ocn_atminst12Thread()
       implicit none
       ! only one threadso always true
       interp_ocn_atminst12Thread=.true.
       end function interp_ocn_atminst12Thread
       logical function interp_ocn_atminst13Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst13Thread=.true.
       end function interp_ocn_atminst13Thread
       logical function interp_ocn_atminst14Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst14Thread=.true.
       end function interp_ocn_atminst14Thread
       logical function interp_ocn_atminst15Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst15Thread=.true.
       end function interp_ocn_atminst15Thread
       logical function interp_ocn_atminst16Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst16Thread=.true.
       end function interp_ocn_atminst16Thread
       logical function interp_ocn_atminst17Thread()
       implicit none
       ! only one thread so always true
       interp_ocn_atminst17Thread=.true.
       end function interp_ocn_atminst17Thread
       logical function ini_weightsThread()
       implicit none
       ! only one thread so always true
       ini_weightsThread=.true.
       end function ini_weightsThread
       logical function transformer1Thread()
       implicit none
       ! only one thread so always true
       transformer1Thread=.true.
       end function transformer1Thread
       logical function transformer2Thread()
       implicit none
       ! only one thread so always true
       transformer2Thread=.true.
       end function transformer2Thread
       logical function transformer3Thread()
       implicit none
       ! only one thread so always true
       transformer3Thread=.true.
       end function transformer3Thread
       logical function transformer4Thread()
       implicit none
       ! only one thread so always true
       transformer4Thread=.true.
       end function transformer4Thread
       logical function transformer5Thread()
       implicit none
       ! only one thread so alwaystrue
       transformer5Thread=.true.
       end function transformer5Thread
       logical function transformer6Thread()
       implicit none
       ! only one thread so always true
       transformer6Thread=.true.
       end function transformer6Thread
       logical function transformer7Thread()
       implicit none
       ! only one thread so always true
       transformer7Thread=.true.
       end function transformer7Thread
       logical function weight_checkThread()
       implicit none
       ! only one thread so always true
       weight_checkThread=.true.
       end function weight_checkThread
       logical function copy_tstarinst1Thread()
       implicit none
       ! only one thread so always true
       copy_tstarinst1Thread=.true.
       end function copy_tstarinst1Thread
       logical function copy_tstarinst2Thread()
       implicit none
       ! only one threadso always true
       copy_tstarinst2Thread=.true.
       end function copy_tstarinst2Thread
       logical function copy_albedoinst1Thread()
       implicit none
       ! only one thread so always true
       copy_albedoinst1Thread=.true.
       end function copy_albedoinst1Thread
       logical function copy_albedoinst2Thread()
       implicit none
       ! only one thread so always true
       copy_albedoinst2Thread=.true.
       end function copy_albedoinst2Thread
       logical function counter_modinst1Thread()
       implicit none
       ! only one thread so always true
       counter_modinst1Thread=.true.
       end function counter_modinst1Thread
       logical function counter_modinst2Thread()
       implicit none
       ! only one thread so always true
       counter_modinst2Thread=.true.
       end function counter_modinst2Thread
       logical function counterinst1Thread()
       implicit none
       ! only one thread so always true
       counterinst1Thread=.true.
       end function counterinst1Thread
       logical function counterinst2Thread()
       implicit none
       ! only one thread so always true
       counterinst2Thread=.true.
       end function counterinst2Thread
       logical function counterinst3Thread()
       implicit none
       ! only one thread so always true
       counterinst3Thread=.true.
       end function counterinst3Thread
       logical function counterinst4Thread()
       implicit none
       ! only one thread so always true
       counterinst4Thread=.true.
       end function counterinst4Thread
       logical function counterinst5Thread()
       implicit none
       ! only one thread so always true
       counterinst5Thread=.true.
       end function counterinst5Thread
       logical function slab_seaiceThread()
       implicit none
       ! only one thread so always true
       slab_seaiceThread=.true.
       end function slab_seaiceThread
       logical function copy_dummyThread()
       implicit none
       ! only one thread so alwaystrue
       copy_dummyThread=.true.
       end function copy_dummyThread
       logical function igcm_atmosphereThread()
       implicit none
       ! only one thread so always true
       igcm_atmosphereThread=.true.
       end function igcm_atmosphereThread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
