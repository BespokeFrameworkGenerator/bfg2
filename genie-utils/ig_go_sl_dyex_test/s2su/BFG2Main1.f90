       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_igcm_atmosphere, only : put_igcm_atmosphere=>put,&
get_igcm_atmosphere=>get
       use BFG2InPlace_transformer2, only : put_transformer2=>put,&
get_transformer2=>get
       use BFG2InPlace_transformer5, only : put_transformer5=>put,&
get_transformer5=>get
       use BFG2InPlace_bfg_averages, only : put_bfg_averages=>put,&
get_bfg_averages=>get
       use BFG2InPlace_initialise_fixedicesheet_mod, only : put_initialise_fixedicesheet_mod=>put,&
get_initialise_fixedicesheet_mod=>get
       use BFG2InPlace_interp_ocn_atm, only : put_interp_ocn_atm=>put,&
get_interp_ocn_atm=>get
       use BFG2InPlace_ini_weights, only : put_ini_weights=>put,&
get_ini_weights=>get
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       use BFG2InPlace_transformer7, only : put_transformer7=>put,&
get_transformer7=>get
       use BFG2InPlace_weight_check, only : put_weight_check=>put,&
get_weight_check=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !interp_ocn_atm__freq
       integer :: interp_ocn_atm__freq
       !ini_weights__freq
       integer :: ini_weights__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !weight_check__freq
       integer :: weight_check__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_ocn_atm_3_freq
       integer :: interp_ocn_atm_3_freq
       !interp_ocn_atm_4_freq
       integer :: interp_ocn_atm_4_freq
       !interp_ocn_atm_5_freq
       integer :: interp_ocn_atm_5_freq
       !interp_ocn_atm_6_freq
       integer :: interp_ocn_atm_6_freq
       !interp_ocn_atm_7_freq
       integer :: interp_ocn_atm_7_freq
       !interp_ocn_atm_8_freq
       integer :: interp_ocn_atm_8_freq
       !interp_ocn_atm_9_freq
       integer :: interp_ocn_atm_9_freq
       !interp_ocn_atm_10_freq
       integer :: interp_ocn_atm_10_freq
       !interp_ocn_atm_11_freq
       integer :: interp_ocn_atm_11_freq
       !interp_ocn_atm_12_freq
       integer :: interp_ocn_atm_12_freq
       !interp_ocn_atm_13_freq
       integer :: interp_ocn_atm_13_freq
       !interp_ocn_atm_14_freq
       integer :: interp_ocn_atm_14_freq
       !interp_ocn_atm_15_freq
       integer :: interp_ocn_atm_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy__freq
       integer :: copy_dummy__freq
       !interp_ocn_atm_16_freq
       integer :: interp_ocn_atm_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_ocn_atm_17_freq
       integer :: interp_ocn_atm_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,interp_ocn_atm__freq,ini_weights__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,weight_check__freq,counter_2_freq,interp_ocn_atm_3_freq,interp_ocn_atm_4_freq,interp_ocn_atm_5_freq,interp_ocn_atm_6_freq,interp_ocn_atm_7_freq,interp_ocn_atm_8_freq,interp_ocn_atm_9_freq,interp_ocn_atm_10_freq,interp_ocn_atm_11_freq,interp_ocn_atm_12_freq,interp_ocn_atm_13_freq,interp_ocn_atm_14_freq,interp_ocn_atm_15_freq,counter_3_freq,goldstein__freq,copy_dummy__freq,interp_ocn_atm_16_freq,copy_tstar_2_freq,interp_ocn_atm_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !olon2
       real, dimension(1:36) :: r81
       !olat2
       real, dimension(1:36) :: r82
       !olon3
       real, dimension(1:36) :: r83
       !olat3
       real, dimension(1:36) :: r84
       !oboxedge2_lon
       real, dimension(1:37) :: r87
       !oboxedge2_lat
       real, dimension(1:37) :: r88
       !oboxedge3_lon
       real, dimension(1:37) :: r89
       !oboxedge3_lat
       real, dimension(1:37) :: r90
       !depth
       real, dimension(1:8) :: r91
       !depth1
       real, dimension(1:9) :: r92
       !ilandmask2
       integer, dimension(1:36,1:36) :: r94
       !ilandmask3
       integer, dimension(1:36,1:36) :: r95
       !ias_out
       integer, dimension(1:36) :: r102
       !iaf_out
       integer, dimension(1:36) :: r103
       !ips_out
       integer, dimension(1:36) :: r104
       !ipf_out
       integer, dimension(1:36) :: r105
       !jsf_out
       integer :: r106
       !go_rhosc
       real :: r119
       !go_scf
       real :: r121
       !go_k1
       integer, dimension(1:36,1:36) :: r122
       !go_dz
       real, dimension(1:8) :: r123
       !go_dza
       real, dimension(1:8) :: r124
       !go_ias
       integer, dimension(1:36) :: r125
       !go_iaf
       integer, dimension(1:36) :: r126
       !go_c
       real, dimension(0:36) :: r128
       !go_cv
       real, dimension(0:36) :: r129
       !go_s
       real, dimension(0:36) :: r130
       !go_sv
       real, dimension(0:36) :: r131
       !test_energy_ocean
       real :: r153
       !test_water_ocean
       real :: r154
       !koverall
       integer :: r155
       !go_cost
       real, dimension(1:36,1:36) :: r158
       !go_u
       real, dimension(1:3,1:36,1:36,1:8) :: r159
       !go_tau
       real, dimension(1:2,1:36,1:36) :: r160
       !alon2_atm
       real, dimension(1:64) :: r165
       !alat2_atm
       real, dimension(1:32) :: r166
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r168
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r170
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r172
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r190
       !hrlons_atm
       real, dimension(1:320) :: r203
       !hrlats_atm
       real, dimension(1:160) :: r204
       !hrlonsedge_atm
       real, dimension(1:321) :: r205
       !hrlatsedge_atm
       real, dimension(1:161) :: r206
       !u10m_atm
       real, dimension(1:64,1:32) :: r213
       !v10m_atm
       real, dimension(1:64,1:32) :: r214
       !q2m_atm
       real, dimension(1:64,1:32) :: r216
       !rh2m_atm
       real, dimension(1:64,1:32) :: r217
       !psigma
       real, dimension(1:7) :: r221
       !massair
       real, dimension(1:64,1:32,1:7) :: r224
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r249
       !test_water_land
       real :: r293
       !glim_snow_model
       logical :: r300
       !glim_coupled
       logical :: r301
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r215
       !write_flag_atm
       logical :: r1
       !write_flag_ocn
       logical :: r2
       !write_flag_sic
       logical :: r3
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r548
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !alon1_atm
       real, dimension(1:64) :: r163
       !alat1_atm
       real, dimension(1:32) :: r164
       !aboxedge1_lon_atm
       real, allocatable, dimension(:) :: r565
       !aboxedge1_lat_atm
       real, allocatable, dimension(:) :: r564
       !land_niter_tim
       integer :: r173
       !ocean_niter_tim
       integer :: r174
       !atmos_dt_tim
       real :: r175
       !tstar_atm
       real, dimension(1:64,1:32) :: r550
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r177
       !albedo_atm
       real, dimension(1:64,1:32) :: r557
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !precip_atm
       real, dimension(1:64,1:32) :: r181
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r187
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r188
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r191
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r194
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r195
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r196
       !water_flux_atmos
       real :: r201
       !glim_covmap
       real, dimension(1:64,1:32) :: r202
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r549
       !conductflux_atm
       real, dimension(1:64,1:32) :: r348
       !test_energy_seaice
       real :: r350
       !test_water_seaice
       real :: r351
       !ksic_loop
       integer :: r352
       !alon1_ocn
       real, dimension(1:36) :: r79
       !alat1_ocn
       real, dimension(1:36) :: r80
       !aboxedge1_lon_ocn
       real, allocatable, dimension(:) :: r567
       !aboxedge1_lat_ocn
       real, allocatable, dimension(:) :: r566
       !ilandmask1_ocn
       integer, dimension(1:36,1:36) :: r93
       !tstar_ocn
       real, dimension(1:36,1:36) :: r97
       !sstar_ocn
       real, dimension(1:36,1:36) :: r98
       !ustar_ocn
       real, dimension(1:36,1:36) :: r99
       !vstar_ocn
       real, dimension(1:36,1:36) :: r100
       !albedo_ocn
       real, dimension(1:36,1:36) :: r101
       !go_ts
       real, dimension(1:14,1:36,1:36,1:8) :: r132
       !go_ts1
       real, dimension(1:14,1:36,1:36,1:8) :: r133
       !iwork1_atm
       integer, allocatable, dimension(:,:) :: r382
       !iwork1_ocn
       integer, allocatable, dimension(:,:) :: r383
       !interpmask_atm
       real, allocatable, dimension(:,:) :: r384
       !weighttot_atm
       real :: r385
       !interpmask_ocn
       real, allocatable, dimension(:,:) :: r386
       !weighttot_ocn
       real :: r387
       !dummy_atm
       real, dimension(1:64,1:32) :: r551
       !weight_atm_weights
       real, allocatable, dimension(:,:) :: r391
       !weight_atm_global
       real, allocatable, dimension(:,:) :: r568
       !weight_ocn_global
       real, allocatable, dimension(:,:) :: r569
       !weight_ocn_weights
       real, allocatable, dimension(:,:) :: r392
       !istep_atm
       integer :: r373
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r207
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r208
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r209
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r210
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r211
       !surfsigma
       real :: r219
       !surfdsigma
       real :: r220
       !iconv_ice
       integer :: r222
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r223
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r421
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r422
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r423
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r424
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r425
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r426
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r427
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r428
       !surf_iter_tim_bfg
       integer :: r374
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r279
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r280
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r281
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r282
       !land_latent_atm
       real, dimension(1:64,1:32) :: r283
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r284
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r285
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r286
       !land_evap_atm
       real, dimension(1:64,1:32) :: r287
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r288
       !land_rough_atm
       real, dimension(1:64,1:32) :: r289
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r290
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r332
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r333
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r334
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r335
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r336
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r337
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r338
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r339
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r340
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r341
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r342
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r343
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r344
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r433
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r437
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r440
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r443
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r446
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r449
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r452
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r455
       !iconv_che
       integer :: r246
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r475
       !katm_loop
       integer :: r477
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r479
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r481
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r483
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r485
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r487
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r489
       !kocn_loop
       integer :: r491
       !istep_sic
       integer :: r948
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r494
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r498
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r502
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r507
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r509
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r511
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r513
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r515
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r517
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r519
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r521
       !istep_ocn
       integer :: r1523
       !seaicefrac_ocn
       real, allocatable, dimension(:,:) :: r1539
       !conductflux_ocn
       real, allocatable, dimension(:,:) :: r2114
       !latent_ocn
       real, allocatable, dimension(:,:) :: r2689
       !netsolar_ocn
       real, allocatable, dimension(:,:) :: r3264
       !netlong_ocn
       real, allocatable, dimension(:,:) :: r3839
       !sensible_ocn
       real, allocatable, dimension(:,:) :: r4414
       !ocean_stressx2_ocn
       real, allocatable, dimension(:,:) :: r4989
       !ocean_stressx3_ocn
       real, allocatable, dimension(:,:) :: r5564
       !ocean_stressy2_ocn
       real, allocatable, dimension(:,:) :: r6139
       !ocean_stressy3_ocn
       real, allocatable, dimension(:,:) :: r6714
       !precip_ocn
       real, allocatable, dimension(:,:) :: r7289
       !evap_ocn
       real, allocatable, dimension(:,:) :: r7864
       !runoff_ocn
       real, allocatable, dimension(:,:) :: r8439
       !dumtmp_ocn
       real, allocatable, dimension(:,:) :: r572
       !dumalb_ocn
       real, allocatable, dimension(:,:) :: r574
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r143
       !istep_che
       integer :: r2098
       !istep_lic
       integer :: r2673
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !ilon1_atm
       integer :: r547
       !ilat1_atm
       integer :: r546
       !ilon1_ocn
       integer :: r561
       !ilat1_ocn
       integer :: r562
       !itype-2
       integer :: r390
       !itype2
       integer :: r1540
       !itype4
       integer :: r2115
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r430
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r429
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r278
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r331
       !outputdir_name
       character(len=200) :: r60
       !fname_restart_main
       character(len=200) :: r58
       !dt_write
       integer :: r59
       !genie_timestep
       real :: r57
       !flag_goldsteinocean
       logical :: r183
       !flag_goldsteinseaice
       logical :: r184
       !flag_land
       logical :: r185
       !lrestart_genie
       logical :: r107
       !klnd_loop
       integer :: r186
       !totsteps
       integer :: r96
       !go_saln0
       real :: r108
       !go_rhoair
       real :: r109
       !go_cd
       real :: r110
       !go_ds
       real, dimension(1:36) :: r111
       !go_dphi
       real :: r112
       !go_ips
       integer, dimension(1:36) :: r113
       !go_ipf
       integer, dimension(1:36) :: r114
       !go_usc
       real :: r115
       !go_dsc
       real :: r116
       !go_fsc
       real :: r117
       !go_rh0sc
       real :: r118
       !temptop_atm
       real, dimension(1:64,1:32) :: r552
       !lgraphic
       logical :: r182
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r189
       !plumin
       real :: r563
       !go_jsf
       integer :: r127
       !go_cpsc
       real :: r120
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begindeclaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupledusing set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r1=.true.
           r2=.true.
           r3=.false.
           r548=0
           r76=0.0
           r77=0.0
           r78=0.0
           r61=0.0
           r62=0.0
           r63=0.0
           r163=0.0
           r164=0.0
           ! 565is allocatable; cannot be written to yet
           ! 564is allocatable; cannot be written to yet
           r173=0
           r174=0
           r175=0.0
           r550=0.0
           ! 177is allocatable; cannot be written to yet
           r557=0.0
           r179=0.0
           r180=0.0
           r181=0.0
           ! 187is allocatable; cannot be written to yet
           r188=0.0
           ! 191is allocatable; cannot be written to yet
           r194=0.0
           r195=0.0
           r196=0.0
           r201=0.0
           r202=0.0
           r549=0.0
           r348=0.0
           r350=0.0
           r351=0.0
           r352=6
           r79=0.0
           r80=0.0
           ! 567is allocatable; cannot be written to yet
           ! 566is allocatable; cannot be written to yet
           r93=0
           r97=0.0
           r98=0.0
           r99=0.0
           r100=0.0
           r101=0.0
           r132=0.0
           r133=0.0
           ! 382is allocatable; cannot be written to yet
           ! 383is allocatable; cannot be written to yet
           ! 384is allocatable; cannot be written to yet
           r385=0.0
           ! 386is allocatable; cannot be written to yet
           r387=0.0
           r551=0.0
           ! 391is allocatable; cannot be written to yet
           ! 568is allocatable; cannot be written to yet
           ! 569is allocatable; cannot be written to yet
           ! 392is allocatable; cannot be written to yet
           r373=0
           r207=0.0
           r208=0.0
           r209=0.0
           r210=0.0
           r211=0.0
           r219=0.0
           r220=0.0
           r222=0.0
           r223=0.0
           ! 421is allocatable; cannot be written to yet
           ! 422is allocatable; cannot be written to yet
           ! 423is allocatable; cannot be written to yet
           ! 424is allocatable; cannot be written to yet
           ! 425is allocatable; cannot be written to yet
           ! 426is allocatable; cannot be written to yet
           ! 427is allocatable; cannot be written to yet
           ! 428is allocatable; cannot be written to yet
           r374=0
           r279=0.0
           r280=0.0
           r281=0.0
           r282=0.0
           r283=0.0
           r284=0.0
           r285=0.0
           r286=0.0
           r287=0.0
           r288=0.0
           r289=0.0
           r290=0.0
           r332=0.0
           r333=0.0
           r334=0.0
           r335=0.0
           r336=0.0
           r337=0.0
           r338=0.0
           r339=0.0
           r340=0.0
           r341=0.0
           r342=0.0
           r343=0.0
           r344=0.0
           r433=0.0
           r437=0.0
           r440=0.0
           r443=0.0
           r446=0.0
           ! 449is allocatable; cannot be written to yet
           ! 452is allocatable; cannot be written to yet
           ! 455is allocatable; cannot be written to yet
           r246=0
           r475=0.0
           r477=1
           r479=0.0
           r481=0.0
           r483=0.0
           r485=0.0
           r487=0.0
           r489=0.0
           r491=48
           r948=0
           r494=0.0
           r498=0.0
           r502=0.0
           r507=0.0
           r509=0.0
           r511=0.0
           r513=0.0
           r515=0.0
           r517=0.0
           r519=0.0
           r521=0.0
           r1523=0
           ! 1539is allocatable; cannot be written to yet
           ! 2114is allocatable; cannot be written to yet
           ! 2689is allocatable; cannot be written to yet
           ! 3264is allocatable; cannot be written to yet
           ! 3839is allocatable; cannot be written to yet
           ! 4414is allocatable; cannot be written to yet
           ! 4989is allocatable; cannot be written to yet
           ! 5564is allocatable; cannot be written to yet
           ! 6139is allocatable; cannot be written to yet
           ! 6714is allocatable; cannot be written to yet
           ! 7289is allocatable; cannot be written to yet
           ! 7864is allocatable; cannot be written to yet
           ! 8439is allocatable; cannot be written to yet
           ! 572is allocatable; cannot be written to yet
           ! 574is allocatable; cannot be written to yet
           r143=0.0
           r2098=0
           r2673=0
           r41=0.0
           r547=64
           r546=32
           r561=36
           r562=36
           r390=-2
           r1540=2
           r2115=4
           ! 430is allocatable; cannot be written to yet
           ! 429is allocatable; cannot be written to yet
           r278=0.0
           r331=0.0
           r60='/home/armstroc/genie_output/genie_ig_go_sl_dyex_test/main'
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r59=720
           r57=3600.0
           r183=.true.
           r184=.false.
           r185=.false.
           r107=.false.
           r186=1
           r96=86400
           r108=0.0
           r109=1.25
           r110=0.0013
           r111=0.0
           r112=0.0
           r113=0
           r114=0
           r115=0.0
           r116=5000.0
           r117=2*7.2921e-5
           r118=1000.0
           r552=0.0
           r182=.false.
           ! 189is allocatable; cannot be written to yet
           r563=0.0
           r127=0
           r120=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call bfg_averages_ini_averages_init(r1,r2,r3)
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call get_initialise_fixedicesheet_mod(r76,-76)
       call get_initialise_fixedicesheet_mod(r77,-77)
       call get_initialise_fixedicesheet_mod(r78,-78)
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r548,r76,r77,r78)
       call put_initialise_fixedicesheet_mod(r548,-548)
       call put_initialise_fixedicesheet_mod(r76,-76)
       call put_initialise_fixedicesheet_mod(r77,-77)
       call put_initialise_fixedicesheet_mod(r78,-78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call initialise_fixedchem(r61,r62,r63)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call initialise_igcmsurf()
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       call get_igcm_atmosphere(r163,-163)
       call get_igcm_atmosphere(r164,-164)
       if (.not.(allocated(r565))) then
       allocate(r565(1:r547+1))
       r565=0.0
       end if
       call get_igcm_atmosphere(r565,-565)
       if (.not.(allocated(r564))) then
       allocate(r564(1:r546+1))
       r564=0.0
       end if
       call get_igcm_atmosphere(r564,-564)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r550,-550)
       call get_igcm_atmosphere(r557,-557)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       if (.not.(allocated(r190))) then
       allocate(r190(1:r547,1:r546))
       end if
       if (.not.(allocated(r177))) then
       allocate(r177(1:r547,1:r546))
       r177=0.0
       end if
       if (.not.(allocated(r187))) then
       allocate(r187(1:r547,1:r546))
       r187=0.0
       end if
       if (.not.(allocated(r191))) then
       allocate(r191(1:r547,1:r546))
       r191=0.0
       end if
       if (.not.(allocated(r189))) then
       allocate(r189(1:r547,1:r546))
       r189=0.0
       end if
       call initialise_atmos(r547,r546,r163,r164,r165,r166,r565,r168,r564,r170,r548,r172,r173,r174,r175,r550,r177,r557,r179,r180,r181,r182,r183,r184,r185,r186,r187,r188,r189,r190,r191,r76,r78,r194,r195,r196,r77,r61,r62,r63,r201,r202,r203,r204,r205,r206)
       call put_igcm_atmosphere(r163,-163)
       call put_igcm_atmosphere(r164,-164)
       call put_igcm_atmosphere(r565,-565)
       call put_igcm_atmosphere(r564,-564)
       call put_igcm_atmosphere(r550,-550)
       call put_igcm_atmosphere(r557,-557)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r557,-557)
       call get_slab_seaice(r549,-549)
       call initialise_slabseaice(r550,r557,r549,r348,r548,r350,r351,r352)
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r557,-557)
       call put_slab_seaice(r549,-549)
       end if
       if (goldsteinThread()) then
       call setActiveModel(7)
       if (.not.(allocated(r567))) then
       allocate(r567(1:r561+1))
       r567=0.0
       end if
       if (.not.(allocated(r566))) then
       allocate(r566(1:r562+1))
       r566=0.0
       end if
       call initialise_goldstein(r79,r80,r81,r82,r83,r84,r567,r566,r87,r88,r89,r90,r91,r92,r93,r94,r95,r96,r97,r98,r99,r100,r101,r102,r103,r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r120,r121,r122,r123,r124,r125,r126,r127,r128,r129,r130,r131,r132,r133)
       end if
       if (interp_ocn_atminst1Thread()) then
       call setActiveModel(8)
       if (.not.(allocated(r565))) then
       allocate(r565(1:r547+1))
       r565=0.0
       end if
       call get_interp_ocn_atm(r565,-565)
       if (.not.(allocated(r564))) then
       allocate(r564(1:r546+1))
       r564=0.0
       end if
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r382))) then
       allocate(r382(1:r547,1:r546))
       r382=0
       end if
       if (.not.(allocated(r383))) then
       allocate(r383(1:r561,1:r562))
       r383=0
       end if
       if (.not.(allocated(r384))) then
       allocate(r384(1:r547,1:r546))
       r384=0.0
       end if
       if (.not.(allocated(r386))) then
       allocate(r386(1:r561,1:r562))
       r386=0.0
       end if
       if (.not.(allocated(r391))) then
       allocate(r391(1:r547,1:r546))
       r391=0.0
       end if
       if (.not.(allocated(r392))) then
       allocate(r392(1:r561,1:r562))
       r392=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r97,r390,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       if (copy_tstarinst1Thread()) then
       call setActiveModel(9)
       call get_copy_tstar(r549,-549)
       call get_copy_tstar(r550,-550)
       call copytstar(r546,r547,r548,r549,r550,r551,r552)
       call put_copy_tstar(r550,-550)
       end if
       if (interp_ocn_atminst2Thread()) then
       call setActiveModel(10)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r101,r390,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       if (copy_albedoinst1Thread()) then
       call setActiveModel(11)
       call get_copy_albedo(r549,-549)
       call get_copy_albedo(r557,-557)
       call copyalbedo(r546,r547,r548,r549,r557,r551)
       call put_copy_albedo(r557,-557)
       end if
       if (ini_weightsThread()) then
       call setActiveModel(12)
       call get_ini_weights(r565,-565)
       call get_ini_weights(r564,-564)
       call ini_weights(r565,r564,r567,r566,r548,r93,r384,r385,r386,r387,r391,r392,r547,r546,r561,r562)
       call put_ini_weights(r565,-565)
       call put_ini_weights(r564,-564)
       end if
       if (weight_checkThread()) then
       call setActiveModel(13)
       call get_weight_check(r564,-564)
       call get_weight_check(r565,-565)
       if (.not.(allocated(r568))) then
       allocate(r568(1:r547,1:r546))
       r568=0.0
       end if
       if (.not.(allocated(r569))) then
       allocate(r569(1:r561,1:r562))
       r569=0.0
       end if
       call weightcheck(r547,r546,r561,r562,r563,r564,r565,r566,r567,r568,r569)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(14)
       call counter(r373)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call get_igcm_atmosphere(r76,-76)
       call get_igcm_atmosphere(r222,-222)
       call igcm3_adiab(r207,r208,r209,r210,r211,r187,r213,r214,r215,r216,r217,r76,r219,r220,r221,r222,r223,r224)
       call put_igcm_atmosphere(r76,-76)
       call put_igcm_atmosphere(r222,-222)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(16)
       if (.not.(allocated(r421))) then
       allocate(r421(1:r547,1:r546))
       r421=0.0
       end if
       if (.not.(allocated(r422))) then
       allocate(r422(1:r547,1:r546))
       r422=0.0
       end if
       if (.not.(allocated(r423))) then
       allocate(r423(1:r547,1:r546))
       r423=0.0
       end if
       if (.not.(allocated(r424))) then
       allocate(r424(1:r547,1:r546))
       r424=0.0
       end if
       if (.not.(allocated(r425))) then
       allocate(r425(1:r547,1:r546))
       r425=0.0
       end if
       if (.not.(allocated(r426))) then
       allocate(r426(1:r547,1:r546))
       r426=0.0
       end if
       if (.not.(allocated(r427))) then
       allocate(r427(1:r547,1:r546))
       r427=0.0
       end if
       if (.not.(allocated(r428))) then
       allocate(r428(1:r547,1:r546))
       r428=0.0
       end if
       if (.not.(allocated(r430))) then
       allocate(r430(1:r547,1:r546))
       r430=0.0
       end if
       if (.not.(allocated(r429))) then
       allocate(r429(1:r547,1:r546))
       r429=0.0
       end if
       call new_transformer_1(r547,r546,r207,r208,r209,r210,r187,r211,r421,r422,r423,r424,r425,r426,r427,r428,r429,r430)
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(17)
       call counter_mod(r374,r173)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r78,-78)
       call get_igcm_atmosphere(r77,-77)
       call igcm_land_surflux(r373,r374,r173,r175,r219,r548,r179,r180,r181,r421,r422,r423,r424,r211,r278,r279,r280,r281,r282,r283,r284,r285,r286,r287,r288,r289,r290,r191,r188,r293,r78,r194,r195,r196,r77,r202,r300,r301)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r78,-78)
       call put_igcm_atmosphere(r77,-77)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(19)
       call get_igcm_atmosphere(r548,-548)
       call igcm_land_blayer(r374,r173,r175,r220,r548,r279,r280,r281,r282,r421,r422,r423,r424,r211)
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(20)
       call counter_mod(r374,r174)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(21)
       call get_igcm_atmosphere(r548,-548)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r557,-557)
       call get_igcm_atmosphere(r550,-550)
       call igcm_ocean_surflux(r373,r374,r174,r219,r548,r179,r180,r181,r425,r426,r427,r428,r211,r557,r550,r331,r332,r333,r334,r335,r336,r337,r338,r339,r340,r341,r342,r343,r344)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r557,-557)
       call put_igcm_atmosphere(r550,-550)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(22)
       call get_igcm_atmosphere(r548,-548)
       call igcm_ocean_blayer(r374,r174,r175,r220,r548,r332,r333,r334,r335,r425,r426,r427,r428,r211)
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(23)
       call get_transformer2(r433,-433)
       call get_transformer2(r548,-548)
       call get_transformer2(r437,-437)
       if (.not.(allocated(r449))) then
       allocate(r449(1:r547,1:r546))
       r449=0.0
       end if
       if (.not.(allocated(r452))) then
       allocate(r452(1:r547,1:r546))
       r452=0.0
       end if
       if (.not.(allocated(r455))) then
       allocate(r455(1:r547,1:r546))
       r455=0.0
       end if
       call new_transformer_2(r547,r546,r433,r548,r283,r336,r437,r284,r337,r440,r285,r338,r443,r286,r339,r446,r287,r340,r449,r288,r341,r452,r289,r342,r455,r290,r343,r177,r191,r344,r207,r421,r425,r208,r422,r426,r209,r423,r427,r210,r424,r428)
       call put_transformer2(r433,-433)
       call put_transformer2(r437,-437)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(24)
       call get_igcm_atmosphere(r433,-433)
       call get_igcm_atmosphere(r437,-437)
       call get_igcm_atmosphere(r179,-179)
       call get_igcm_atmosphere(r180,-180)
       call get_igcm_atmosphere(r61,-61)
       call get_igcm_atmosphere(r62,-62)
       call get_igcm_atmosphere(r63,-63)
       call get_igcm_atmosphere(r246,-246)
       call igcm3_diab(r207,r208,r209,r210,r177,r455,r449,r452,r433,r437,r440,r443,r179,r180,r181,r196,r215,r287,r61,r62,r63,r246,r201,r223,r249)
       call put_igcm_atmosphere(r433,-433)
       call put_igcm_atmosphere(r437,-437)
       call put_igcm_atmosphere(r179,-179)
       call put_igcm_atmosphere(r180,-180)
       call put_igcm_atmosphere(r61,-61)
       call put_igcm_atmosphere(r62,-62)
       call put_igcm_atmosphere(r63,-63)
       call put_igcm_atmosphere(r246,-246)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(25)
       call get_transformer3(r475,-475)
       call get_transformer3(r433,-433)
       call get_transformer3(r479,-479)
       call get_transformer3(r437,-437)
       call get_transformer3(r481,-481)
       call get_transformer3(r179,-179)
       call get_transformer3(r483,-483)
       call get_transformer3(r180,-180)
       call get_transformer3(r485,-485)
       call get_transformer3(r487,-487)
       call get_transformer3(r489,-489)
       call new_transformer_3(r547,r546,r475,r433,r477,r352,r479,r437,r481,r179,r483,r180,r485,r440,r487,r443,r489,r181,r491)
       call put_transformer3(r475,-475)
       call put_transformer3(r433,-433)
       call put_transformer3(r479,-479)
       call put_transformer3(r437,-437)
       call put_transformer3(r481,-481)
       call put_transformer3(r179,-179)
       call put_transformer3(r483,-483)
       call put_transformer3(r180,-180)
       call put_transformer3(r485,-485)
       call put_transformer3(r487,-487)
       call put_transformer3(r489,-489)
       end if
       end if
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(26)
       call counter(r948)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(27)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r475,-475)
       call get_slab_seaice(r479,-479)
       call get_slab_seaice(r481,-481)
       call get_slab_seaice(r483,-483)
       call get_slab_seaice(r433,-433)
       call get_slab_seaice(r437,-437)
       call get_slab_seaice(r179,-179)
       call get_slab_seaice(r180,-180)
       call get_slab_seaice(r485,-485)
       call get_slab_seaice(r487,-487)
       call get_slab_seaice(r549,-549)
       call get_slab_seaice(r557,-557)
       call slabseaice(r948,r550,r475,r479,r481,r483,r433,r437,r179,r180,r485,r487,r549,r552,r348,r557,r548,r350,r351,r352)
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r475,-475)
       call put_slab_seaice(r479,-479)
       call put_slab_seaice(r481,-481)
       call put_slab_seaice(r483,-483)
       call put_slab_seaice(r433,-433)
       call put_slab_seaice(r437,-437)
       call put_slab_seaice(r179,-179)
       call put_slab_seaice(r180,-180)
       call put_slab_seaice(r485,-485)
       call put_slab_seaice(r487,-487)
       call put_slab_seaice(r549,-549)
       call put_slab_seaice(r557,-557)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(28)
       call get_transformer4(r549,-549)
       call new_transformer_4(r547,r546,r494,r549,r352,r491,r498,r348)
       call put_transformer4(r549,-549)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(29)
       call get_transformer5(r502,-502)
       call get_transformer5(r433,-433)
       call get_transformer5(r549,-549)
       call get_transformer5(r507,-507)
       call get_transformer5(r437,-437)
       call get_transformer5(r509,-509)
       call get_transformer5(r179,-179)
       call get_transformer5(r511,-511)
       call get_transformer5(r180,-180)
       call get_transformer5(r513,-513)
       call get_transformer5(r515,-515)
       call get_transformer5(r517,-517)
       call get_transformer5(r519,-519)
       call get_transformer5(r521,-521)
       call new_transformer_5(r547,r546,r502,r433,r549,r477,r491,r507,r437,r509,r179,r511,r180,r513,r440,r515,r443,r517,r181,r519,r446,r521,r188)
       call put_transformer5(r502,-502)
       call put_transformer5(r433,-433)
       call put_transformer5(r549,-549)
       call put_transformer5(r507,-507)
       call put_transformer5(r437,-437)
       call put_transformer5(r509,-509)
       call put_transformer5(r179,-179)
       call put_transformer5(r511,-511)
       call put_transformer5(r180,-180)
       call put_transformer5(r513,-513)
       call put_transformer5(r515,-515)
       call put_transformer5(r517,-517)
       call put_transformer5(r519,-519)
       call put_transformer5(r521,-521)
       end if
       end if
       if (interp_ocn_atminst3Thread()) then
       if(mod(its1,interp_ocn_atm_3_freq).eq.0)then
       call setActiveModel(30)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r1539))) then
       allocate(r1539(1:r561,1:r562))
       r1539=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r494,r1539,r1540,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (interp_ocn_atminst4Thread()) then
       if(mod(its1,interp_ocn_atm_4_freq).eq.0)then
       call setActiveModel(31)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       if (.not.(allocated(r2114))) then
       allocate(r2114(1:r561,1:r562))
       r2114=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r498,r2114,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (interp_ocn_atminst5Thread()) then
       if(mod(its1,interp_ocn_atm_5_freq).eq.0)then
       call setActiveModel(32)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r502,-502)
       if (.not.(allocated(r2689))) then
       allocate(r2689(1:r561,1:r562))
       r2689=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r502,r2689,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r502,-502)
       end if
       end if
       if (interp_ocn_atminst6Thread()) then
       if(mod(its1,interp_ocn_atm_6_freq).eq.0)then
       call setActiveModel(33)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r509,-509)
       if (.not.(allocated(r3264))) then
       allocate(r3264(1:r561,1:r562))
       r3264=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r509,r3264,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r509,-509)
       end if
       end if
       if (interp_ocn_atminst7Thread()) then
       if(mod(its1,interp_ocn_atm_7_freq).eq.0)then
       call setActiveModel(34)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r511,-511)
       if (.not.(allocated(r3839))) then
       allocate(r3839(1:r561,1:r562))
       r3839=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r511,r3839,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r511,-511)
       end if
       end if
       if (interp_ocn_atminst8Thread()) then
       if(mod(its1,interp_ocn_atm_8_freq).eq.0)then
       call setActiveModel(35)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r507,-507)
       if (.not.(allocated(r4414))) then
       allocate(r4414(1:r561,1:r562))
       r4414=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r507,r4414,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r507,-507)
       end if
       end if
       if (interp_ocn_atminst9Thread()) then
       if(mod(its1,interp_ocn_atm_9_freq).eq.0)then
       call setActiveModel(36)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r513,-513)
       if (.not.(allocated(r4989))) then
       allocate(r4989(1:r561,1:r562))
       r4989=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r513,r4989,r1540,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r513,-513)
       end if
       end if
       if (interp_ocn_atminst10Thread()) then
       if(mod(its1,interp_ocn_atm_10_freq).eq.0)then
       call setActiveModel(37)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r513,-513)
       if (.not.(allocated(r5564))) then
       allocate(r5564(1:r561,1:r562))
       r5564=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r513,r5564,r1540,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r513,-513)
       end if
       end if
       if (interp_ocn_atminst11Thread()) then
       if(mod(its1,interp_ocn_atm_11_freq).eq.0)then
       call setActiveModel(38)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r515,-515)
       if (.not.(allocated(r6139))) then
       allocate(r6139(1:r561,1:r562))
       r6139=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r515,r6139,r1540,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r515,-515)
       end if
       end if
       if (interp_ocn_atminst12Thread()) then
       if(mod(its1,interp_ocn_atm_12_freq).eq.0)then
       call setActiveModel(39)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r515,-515)
       if (.not.(allocated(r6714))) then
       allocate(r6714(1:r561,1:r562))
       r6714=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r515,r6714,r1540,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r515,-515)
       end if
       end if
       if (interp_ocn_atminst13Thread()) then
       if(mod(its1,interp_ocn_atm_13_freq).eq.0)then
       call setActiveModel(40)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r517,-517)
       if (.not.(allocated(r7289))) then
       allocate(r7289(1:r561,1:r562))
       r7289=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r517,r7289,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r517,-517)
       end if
       end if
       if (interp_ocn_atminst14Thread()) then
       if(mod(its1,interp_ocn_atm_14_freq).eq.0)then
       call setActiveModel(41)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r519,-519)
       if (.not.(allocated(r7864))) then
       allocate(r7864(1:r561,1:r562))
       r7864=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r519,r7864,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r519,-519)
       end if
       end if
       if (interp_ocn_atminst15Thread()) then
       if(mod(its1,interp_ocn_atm_15_freq).eq.0)then
       call setActiveModel(42)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call get_interp_ocn_atm(r521,-521)
       if (.not.(allocated(r8439))) then
       allocate(r8439(1:r561,1:r562))
       r8439=0.0
       end if
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r521,r8439,r2115,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       call put_interp_ocn_atm(r521,-521)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(43)
       call counter(r1523)
       end if
       end if
       if (goldsteinThread()) then
       if(mod(its1,goldstein__freq).eq.0)then
       call setActiveModel(44)
       call goldstein(r1523,r2689,r4414,r3264,r3839,r2114,r7864,r7289,r8439,r143,r4989,r6139,r5564,r6714,r97,r98,r99,r100,r101,r153,r154,r155,r132,r133,r158,r159,r160)
       end if
       end if
       if (copy_dummyThread()) then
       if(mod(its1,copy_dummy__freq).eq.0)then
       call setActiveModel(45)
       if (.not.(allocated(r572))) then
       allocate(r572(1:r561,1:r562))
       r572=0.0
       end if
       if (.not.(allocated(r574))) then
       allocate(r574(1:r561,1:r562))
       r574=0.0
       end if
       call copy_dummy(r561,r562,r572,r97,r574,r101)
       end if
       end if
       if (interp_ocn_atminst16Thread()) then
       if(mod(its1,interp_ocn_atm_16_freq).eq.0)then
       call setActiveModel(46)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r572,r390,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (copy_tstarinst2Thread()) then
       if(mod(its1,copy_tstar_2_freq).eq.0)then
       call setActiveModel(47)
       call get_copy_tstar(r549,-549)
       call get_copy_tstar(r550,-550)
       call copytstar(r546,r547,r548,r549,r550,r551,r552)
       call put_copy_tstar(r550,-550)
       end if
       end if
       if (interp_ocn_atminst17Thread()) then
       if(mod(its1,interp_ocn_atm_17_freq).eq.0)then
       call setActiveModel(48)
       call get_interp_ocn_atm(r565,-565)
       call get_interp_ocn_atm(r564,-564)
       call interp_ocn_atm(r565,r564,r567,r566,r548,r93,r382,r383,r384,r385,r386,r387,r551,r574,r390,r391,r392,r547,r546,r561,r562)
       call put_interp_ocn_atm(r565,-565)
       call put_interp_ocn_atm(r564,-564)
       end if
       end if
       if (copy_albedoinst2Thread()) then
       if(mod(its1,copy_albedo_2_freq).eq.0)then
       call setActiveModel(49)
       call get_copy_albedo(r549,-549)
       call get_copy_albedo(r557,-557)
       call copyalbedo(r546,r547,r548,r549,r557,r551)
       call put_copy_albedo(r557,-557)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(50)
       call counter(r2098)
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(51)
       call get_fixed_chemistry(r61,-61)
       call get_fixed_chemistry(r62,-62)
       call get_fixed_chemistry(r63,-63)
       call get_fixed_chemistry(r246,-246)
       call fixedchem(r2098,r61,r62,r63,r246)
       call put_fixed_chemistry(r61,-61)
       call put_fixed_chemistry(r62,-62)
       call put_fixed_chemistry(r63,-63)
       call put_fixed_chemistry(r246,-246)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(52)
       call counter(r2673)
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(53)
       call get_fixed_icesheet(r76,-76)
       call get_fixed_icesheet(r77,-77)
       call get_fixed_icesheet(r78,-78)
       call get_fixed_icesheet(r222,-222)
       call fixedicesheet(r2673,r548,r76,r77,r78,r222)
       call put_fixed_icesheet(r76,-76)
       call put_fixed_icesheet(r77,-77)
       call put_fixed_icesheet(r78,-78)
       call put_fixed_icesheet(r222,-222)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(54)
       call get_bfg_averages(r163,-163)
       call get_bfg_averages(r164,-164)
       call get_bfg_averages(r509,-509)
       call get_bfg_averages(r511,-511)
       call get_bfg_averages(r507,-507)
       call get_bfg_averages(r502,-502)
       call get_bfg_averages(r513,-513)
       call get_bfg_averages(r515,-515)
       call get_bfg_averages(r519,-519)
       call get_bfg_averages(r517,-517)
       call get_bfg_averages(r521,-521)
       call get_bfg_averages(r550,-550)
       call get_bfg_averages(r557,-557)
       call bfg_averages_write_averages_iteration(r1523,r163,r164,r79,r80,r9,r10,r509,r3264,r13,r511,r3839,r16,r507,r4414,r19,r502,r2689,r22,r513,r4989,r25,r515,r6139,r28,r498,r2114,r31,r519,r7864,r34,r517,r7289,r37,r521,r8439,r40,r41,r143,r43,r494,r1539,r46,r550,r97,r49,r557,r101,r52,r1,r2,r3,r491,r57,r58,r59,r60)
       call put_bfg_averages(r163,-163)
       call put_bfg_averages(r164,-164)
       call put_bfg_averages(r509,-509)
       call put_bfg_averages(r511,-511)
       call put_bfg_averages(r507,-507)
       call put_bfg_averages(r502,-502)
       call put_bfg_averages(r513,-513)
       call put_bfg_averages(r515,-515)
       call put_bfg_averages(r519,-519)
       call put_bfg_averages(r517,-517)
       call put_bfg_averages(r521,-521)
       call put_bfg_averages(r550,-550)
       call put_bfg_averages(r557,-557)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(55)
       call get_transformer6(r475,-475)
       call get_transformer6(r479,-479)
       call get_transformer6(r481,-481)
       call get_transformer6(r483,-483)
       call get_transformer6(r485,-485)
       call get_transformer6(r487,-487)
       call get_transformer6(r489,-489)
       call new_transformer_6(r547,r546,r475,r479,r481,r483,r485,r487,r489)
       call put_transformer6(r475,-475)
       call put_transformer6(r479,-479)
       call put_transformer6(r481,-481)
       call put_transformer6(r483,-483)
       call put_transformer6(r485,-485)
       call put_transformer6(r487,-487)
       call put_transformer6(r489,-489)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(56)
       call get_transformer7(r502,-502)
       call get_transformer7(r507,-507)
       call get_transformer7(r509,-509)
       call get_transformer7(r511,-511)
       call get_transformer7(r513,-513)
       call get_transformer7(r515,-515)
       call get_transformer7(r517,-517)
       call get_transformer7(r519,-519)
       call get_transformer7(r521,-521)
       call new_transformer_7(r547,r546,r502,r507,r509,r511,r513,r515,r517,r519,r521,r494,r498,r41)
       call put_transformer7(r502,-502)
       call put_transformer7(r507,-507)
       call put_transformer7(r509,-509)
       call put_transformer7(r511,-511)
       call put_transformer7(r513,-513)
       call put_transformer7(r515,-515)
       call put_transformer7(r517,-517)
       call put_transformer7(r519,-519)
       call put_transformer7(r521,-521)
       end if
       end if
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(57)
       call end_atmos()
       end if
       if (goldsteinThread()) then
       call setActiveModel(58)
       call end_goldstein()
       end if
       call finaliseComms()
       end program BFG2Main

