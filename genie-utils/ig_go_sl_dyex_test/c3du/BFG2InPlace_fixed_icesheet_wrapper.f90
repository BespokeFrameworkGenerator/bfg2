       ! f77 to f90 put/get wrappers start
       subroutine put_fixed_icesheet(data,tag)
       use BFG2Target3
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==53) then
       if (tag==-76) then
       call putreal__2_fixed_icesheet(data,tag)
       end if
       if (tag==-77) then
       call putreal__2_fixed_icesheet(data,tag)
       end if
       if (tag==-78) then
       call putreal__2_fixed_icesheet(data,tag)
       end if
       end if
       if (currentModel==53) then
       if (tag==-222) then
       call putinteger__0_fixed_icesheet(data,tag)
       end if
       end if
       end subroutine put_fixed_icesheet
       subroutine get_fixed_icesheet(data,tag)
       use BFG2Target3
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==53) then
       if (tag==-76) then
       call getreal__2_fixed_icesheet(data,tag)
       end if
       if (tag==-77) then
       call getreal__2_fixed_icesheet(data,tag)
       end if
       if (tag==-78) then
       call getreal__2_fixed_icesheet(data,tag)
       end if
       end if
       if (currentModel==53) then
       if (tag==-222) then
       call getinteger__0_fixed_icesheet(data,tag)
       end if
       end if
       end subroutine get_fixed_icesheet
       subroutine getreal__2_fixed_icesheet(data,tag)
       use BFG2InPlace_fixed_icesheet, only : get=>getreal__2
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal__2_fixed_icesheet
       subroutine getinteger__0_fixed_icesheet(data,tag)
       use BFG2InPlace_fixed_icesheet, only : get=>getinteger__0
       implicit none
       integer , intent(in) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getinteger__0_fixed_icesheet
       subroutine putreal__2_fixed_icesheet(data,tag)
       use BFG2InPlace_fixed_icesheet, only : put=>putreal__2
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal__2_fixed_icesheet
       subroutine putinteger__0_fixed_icesheet(data,tag)
       use BFG2InPlace_fixed_icesheet, only : put=>putinteger__0
       implicit none
       integer , intent(out) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putinteger__0_fixed_icesheet
       ! f77 to f90 put/getwrappers end
