       module BFG2InPlace_igcm_atmosphere
       use BFG2Target1
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__1,getinteger__2,getreal__2,getinteger__0
       end interface
       interface put
       module procedure putreal__1,putreal__2,putinteger__0
       end interface
       contains
       subroutine getreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(4) :: pp2p=(/0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       if (arg2==-163) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/5,54/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=64
       call mpi_recv(arg1,mysize,mpi_real,du,163,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-164) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/5,54/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=32
       call mpi_recv(arg1,mysize,mpi_real,du,164,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-565) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=19
       allocate(set(1:setSize))
       set=(/5,8,10,12,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=65
       call mpi_recv(arg1,mysize,mpi_real,du,565,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-564) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=19
       allocate(set(1:setSize))
       set=(/5,8,10,12,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=33
       call mpi_recv(arg1,mysize,mpi_real,du,564,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine getreal__1
       subroutine getinteger__2(arg1,arg2)
       implicit none
       integer , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(5) :: pp2p=(/0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       if (arg2==-548) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,5/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_integer,du,548,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       if (arg2==-548) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,18/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_integer,du,548,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       if (arg2==-548) then
       ! I am igcm_atmosphere.igcm_land_blayer..19
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,19/)
       myID=19
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_integer,du,548,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       if (arg2==-548) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,21/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_integer,du,548,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       if (arg2==-548) then
       ! I am igcm_atmosphere.igcm_ocean_blayer..22
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/2,22/)
       myID=22
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_integer,du,548,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine getinteger__2
       subroutine getreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(26) :: pp2p=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       if (arg2==-550) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,550,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-557) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,557,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-179) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(3).eq.1) then
       pp2p(3)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,179,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(4).eq.1) then
       pp2p(4)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,180,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-76) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,15,53/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(5).eq.1) then
       pp2p(5)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,76,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-78) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,18,53/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(6).eq.1) then
       pp2p(6)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,78,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-77) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,18,53/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(7).eq.1) then
       pp2p(7)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,77,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-61) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(8).eq.1) then
       pp2p(8)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,61,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-62) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(9).eq.1) then
       pp2p(9)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,62,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-63) then
       ! I am igcm_atmosphere.initialise_atmos..5
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=5
       myDU=info(myID)%du
       if (pp2p(10).eq.1) then
       pp2p(10)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,63,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       if (arg2==-76) then
       ! I am igcm_atmosphere.igcm3_adiab..15
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,15,53/)
       myID=15
       myDU=info(myID)%du
       if (pp2p(11).eq.1) then
       pp2p(11)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,76,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(12).eq.1) then
       pp2p(12)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,179,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(13).eq.1) then
       pp2p(13)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,180,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-78) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,18,53/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(14).eq.1) then
       pp2p(14)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,78,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-77) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/2,5,18,53/)
       myID=18
       myDU=info(myID)%du
       if (pp2p(15).eq.1) then
       pp2p(15)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,77,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(16).eq.1) then
       pp2p(16)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,179,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(17).eq.1) then
       pp2p(17)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,180,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-557) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(18).eq.1) then
       pp2p(18)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,557,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-550) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       myID=21
       myDU=info(myID)%du
       if (pp2p(19).eq.1) then
       pp2p(19)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,550,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       if (arg2==-433) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/23,24,25,27,29/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(20).eq.1) then
       pp2p(20)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,433,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-437) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=5
       allocate(set(1:setSize))
       set=(/23,24,25,27,29/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(21).eq.1) then
       pp2p(21)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,437,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(22).eq.1) then
       pp2p(22)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,179,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=7
       allocate(set(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(23).eq.1) then
       pp2p(23)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,180,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-61) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(24).eq.1) then
       pp2p(24)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,61,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-62) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(25).eq.1) then
       pp2p(25)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,62,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-63) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=4
       allocate(set(1:setSize))
       set=(/3,5,24,51/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(26).eq.1) then
       pp2p(26)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=2048
       call mpi_recv(arg1,mysize,mpi_real,du,63,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine getreal__2
       subroutine getinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(2) :: pp2p=(/0,0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       if (arg2==-222) then
       ! I am igcm_atmosphere.igcm3_adiab..15
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/15,53/)
       myID=15
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,222,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       if (arg2==-246) then
       ! I am igcm_atmosphere.igcm3_diab..24
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/24,51/)
       myID=24
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,246,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine getinteger__0
       subroutine putreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       if (arg2==-163) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,54/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=64
       call mpi_send(arg1,mysize,mpi_real,du,163,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=64
       call mpi_send(arg1,mysize,mpi_real,du,163,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-164) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,54/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=32
       call mpi_send(arg1,mysize,mpi_real,du,164,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=32
       call mpi_send(arg1,mysize,mpi_real,du,164,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-565) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=20
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       ins=(/0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=65
       call mpi_send(arg1,mysize,mpi_real,du,565,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=65
       call mpi_send(arg1,mysize,mpi_real,du,565,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-564) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=20
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       ins=(/0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=33
       call mpi_send(arg1,mysize,mpi_real,du,564,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=33
       call mpi_send(arg1,mysize,mpi_real,du,564,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine putreal__1
       subroutine putreal__2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       if (arg2==-550) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-557) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-179) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-76) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,15,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,76,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,76,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-78) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,18,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,78,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,78,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-77) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,18,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,77,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,77,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-61) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,61,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,61,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-62) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,62,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,62,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-63) then
       ! I am igcm_atmosphere.initialise_atmos..5
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=5
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,63,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_send(arg1,mysize,mpi_real,du,63,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       if (arg2==-76) then
       ! I am igcm_atmosphere.igcm3_adiab..15
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,15,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=15
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,76,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,76,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=18
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=18
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-78) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,18,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=18
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,78,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,78,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-77) then
       ! I am igcm_atmosphere.igcm_land_surflux..18
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/2,5,18,53/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=18
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,77,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,77,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-557) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,11,21,27,49,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,557,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-550) then
       ! I am igcm_atmosphere.igcm_ocean_surflux..21
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,6,9,21,27,47,54/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=21
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,550,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       if (arg2==-433) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/23,24,25,27,29/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,433,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,433,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-437) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=5
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/23,24,25,27,29/)
       ins=(/0,0,0,0,0/)
       outs=(/0,0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,437,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,437,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-179) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,179,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-180) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=7
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/5,18,21,24,25,27,29/)
       ins=(/0,0,0,0,0,0,0/)
       outs=(/0,0,0,0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,180,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-61) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,61,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,61,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-62) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,62,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,62,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       if (arg2==-63) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/3,5,24,51/)
       ins=(/0,0,0,0/)
       outs=(/0,0,0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,63,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=2048
       call mpi_send(arg1,mysize,mpi_real,du,63,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine putreal__2
       subroutine putinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=igcm_atmosphere and ep=initialise_igcmsurf and instance=
       end if
       if (currentModel==5) then
       ! I am model=igcm_atmosphere and ep=initialise_atmos and instance=
       end if
       if (currentModel==15) then
       ! I am model=igcm_atmosphere and ep=igcm3_adiab and instance=
       if (arg2==-222) then
       ! I am igcm_atmosphere.igcm3_adiab..15
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/15,53/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=15
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,222,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,222,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==18) then
       ! I am model=igcm_atmosphere and ep=igcm_land_surflux and instance=
       end if
       if (currentModel==19) then
       ! I am model=igcm_atmosphere and ep=igcm_land_blayer and instance=
       end if
       if (currentModel==21) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_surflux and instance=
       end if
       if (currentModel==22) then
       ! I am model=igcm_atmosphere and ep=igcm_ocean_blayer and instance=
       end if
       if (currentModel==24) then
       ! I am model=igcm_atmosphere and ep=igcm3_diab and instance=
       if (arg2==-246) then
       ! I am igcm_atmosphere.igcm3_diab..24
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/24,51/)
       ins=(/0,0/)
       outs=(/0,0/)
       myID=24
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       current=-1
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       ! Only send data if a recipient (other than this process itself) has been found
       if (current.ne.-1) then
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,246,mpi_comm_world,ierr)
       end if
       end if
       newSize=setSize
       newSet=set
       ! Only continue sending data while more recipients
       ! (other than this process itself) can be found
       do while(current.ne.-1)
       if (newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID) then
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,246,mpi_comm_world,ierr)
       end if
       current=-1
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       else
       ! Found all recipients, so quit the loop
       current=-1
       end if
       end do
       end if
       end if
       if (currentModel==57) then
       ! I am model=igcm_atmosphere and ep=end_atmos and instance=
       end if
       end subroutine putinteger__0
       end module BFG2InPlace_igcm_atmosphere
