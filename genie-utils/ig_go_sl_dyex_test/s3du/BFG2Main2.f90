       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_transformer4, only : put_transformer4=>put,&
get_transformer4=>get
       use BFG2InPlace_transformer6, only : put_transformer6=>put,&
get_transformer6=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !interp_ocn_atm__freq
       integer :: interp_ocn_atm__freq
       !ini_weights__freq
       integer :: ini_weights__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !copy_tstar__freq
       integer :: copy_tstar__freq
       !copy_albedo__freq
       integer :: copy_albedo__freq
       !weight_check__freq
       integer :: weight_check__freq
       !counter_2_freq
       integer :: counter_2_freq
       !interp_ocn_atm_3_freq
       integer :: interp_ocn_atm_3_freq
       !interp_ocn_atm_4_freq
       integer :: interp_ocn_atm_4_freq
       !interp_ocn_atm_5_freq
       integer :: interp_ocn_atm_5_freq
       !interp_ocn_atm_6_freq
       integer :: interp_ocn_atm_6_freq
       !interp_ocn_atm_7_freq
       integer :: interp_ocn_atm_7_freq
       !interp_ocn_atm_8_freq
       integer :: interp_ocn_atm_8_freq
       !interp_ocn_atm_9_freq
       integer :: interp_ocn_atm_9_freq
       !interp_ocn_atm_10_freq
       integer :: interp_ocn_atm_10_freq
       !interp_ocn_atm_11_freq
       integer :: interp_ocn_atm_11_freq
       !interp_ocn_atm_12_freq
       integer :: interp_ocn_atm_12_freq
       !interp_ocn_atm_13_freq
       integer :: interp_ocn_atm_13_freq
       !interp_ocn_atm_14_freq
       integer :: interp_ocn_atm_14_freq
       !interp_ocn_atm_15_freq
       integer :: interp_ocn_atm_15_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !copy_dummy__freq
       integer :: copy_dummy__freq
       !interp_ocn_atm_16_freq
       integer :: interp_ocn_atm_16_freq
       !copy_tstar_2_freq
       integer :: copy_tstar_2_freq
       !interp_ocn_atm_17_freq
       integer :: interp_ocn_atm_17_freq
       !copy_albedo_2_freq
       integer :: copy_albedo_2_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,interp_ocn_atm__freq,ini_weights__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,copy_tstar__freq,copy_albedo__freq,weight_check__freq,counter_2_freq,interp_ocn_atm_3_freq,interp_ocn_atm_4_freq,interp_ocn_atm_5_freq,interp_ocn_atm_6_freq,interp_ocn_atm_7_freq,interp_ocn_atm_8_freq,interp_ocn_atm_9_freq,interp_ocn_atm_10_freq,interp_ocn_atm_11_freq,interp_ocn_atm_12_freq,interp_ocn_atm_13_freq,interp_ocn_atm_14_freq,interp_ocn_atm_15_freq,counter_3_freq,goldstein__freq,copy_dummy__freq,interp_ocn_atm_16_freq,copy_tstar_2_freq,interp_ocn_atm_17_freq,copy_albedo_2_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r548
       !tstar_atm
       real, dimension(1:64,1:32) :: r550
       !albedo_atm
       real, dimension(1:64,1:32) :: r557
       !netsolar_atm
       real, dimension(1:64,1:32) :: r179
       !netlong_atm
       real, dimension(1:64,1:32) :: r180
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r549
       !conductflux_atm
       real, dimension(1:64,1:32) :: r348
       !test_energy_seaice
       real :: r350
       !test_water_seaice
       real :: r351
       !ksic_loop
       integer :: r352
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r433
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r437
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r475
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r479
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r481
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r483
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r485
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r487
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r489
       !kocn_loop
       integer :: r491
       !istep_sic
       integer :: r948
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r494
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r498
       !ilon1_atm
       integer :: r547
       !ilat1_atm
       integer :: r546
       !temptop_atm
       real, dimension(1:64,1:32) :: r552
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r548=0
           r550=0.0
           r557=0.0
           r179=0.0
           r180=0.0
           r549=0.0
           r348=0.0
           r350=0.0
           r351=0.0
           r352=6
           r433=0.0
           r437=0.0
           r475=0.0
           r479=0.0
           r481=0.0
           r483=0.0
           r485=0.0
           r487=0.0
           r489=0.0
           r491=48
           r948=0
           r494=0.0
           r498=0.0
           r547=64
           r546=32
           r552=0.0
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r557,-557)
       call get_slab_seaice(r549,-549)
       call get_slab_seaice(r548,-548)
       call start_mpivis_log('slab_seaice initialise_slabseaice ')
       call initialise_slabseaice(r550,r557,r549,r348,r548,r350,r351,r352)
       call end_mpivis_log('slab_seaice initialise_slabseaice ')
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r557,-557)
       call put_slab_seaice(r549,-549)
       end if
       do its1=1,nts1
       do its2=1,nts2
       end do
       do its3=1,nts3
       end do
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(26)
       call start_mpivis_log('counter counter 2')
       call counter(r948)
       call end_mpivis_log('counter counter 2')
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(27)
       call get_slab_seaice(r550,-550)
       call get_slab_seaice(r475,-475)
       call get_slab_seaice(r479,-479)
       call get_slab_seaice(r481,-481)
       call get_slab_seaice(r483,-483)
       call get_slab_seaice(r433,-433)
       call get_slab_seaice(r437,-437)
       call get_slab_seaice(r179,-179)
       call get_slab_seaice(r180,-180)
       call get_slab_seaice(r485,-485)
       call get_slab_seaice(r487,-487)
       call get_slab_seaice(r549,-549)
       call get_slab_seaice(r552,-552)
       call get_slab_seaice(r557,-557)
       call get_slab_seaice(r548,-548)
       call start_mpivis_log('slab_seaice slabseaice ')
       call slabseaice(r948,r550,r475,r479,r481,r483,r433,r437,r179,r180,r485,r487,r549,r552,r348,r557,r548,r350,r351,r352)
       call end_mpivis_log('slab_seaice slabseaice ')
       call put_slab_seaice(r550,-550)
       call put_slab_seaice(r475,-475)
       call put_slab_seaice(r479,-479)
       call put_slab_seaice(r481,-481)
       call put_slab_seaice(r483,-483)
       call put_slab_seaice(r433,-433)
       call put_slab_seaice(r437,-437)
       call put_slab_seaice(r179,-179)
       call put_slab_seaice(r180,-180)
       call put_slab_seaice(r485,-485)
       call put_slab_seaice(r487,-487)
       call put_slab_seaice(r549,-549)
       call put_slab_seaice(r552,-552)
       call put_slab_seaice(r557,-557)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(28)
       call get_transformer4(r494,-494)
       call get_transformer4(r549,-549)
       call get_transformer4(r498,-498)
       call start_mpivis_log('transformer4 new_transformer_4 ')
       call new_transformer_4(r547,r546,r494,r549,r352,r491,r498,r348)
       call end_mpivis_log('transformer4 new_transformer_4 ')
       call put_transformer4(r494,-494)
       call put_transformer4(r549,-549)
       call put_transformer4(r498,-498)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(55)
       call get_transformer6(r475,-475)
       call get_transformer6(r479,-479)
       call get_transformer6(r481,-481)
       call get_transformer6(r483,-483)
       call get_transformer6(r485,-485)
       call get_transformer6(r487,-487)
       call get_transformer6(r489,-489)
       call start_mpivis_log('transformer6 new_transformer_6 ')
       call new_transformer_6(r547,r546,r475,r479,r481,r483,r485,r487,r489)
       call end_mpivis_log('transformer6 new_transformer_6 ')
       call put_transformer6(r475,-475)
       call put_transformer6(r479,-479)
       call put_transformer6(r481,-481)
       call put_transformer6(r483,-483)
       call put_transformer6(r485,-485)
       call put_transformer6(r487,-487)
       call put_transformer6(r489,-489)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main

