       module BFG2Target1
       use bfg_averages, only : bfg_averages_ini_averages_init=>ini_averages,&
bfg_averages_write_averages_iteration=>write_averages
       use initialise_fixedicesheet_mod, only : initialise_fixedicesheet_mod_initialise_fixedicesheet_init=>initialise_fixedicesheet
       use mpi
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(3)
       integer :: b2mmap(3)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       !its2
       integer, target :: its2
       !its3
       integer, target :: its3
       type modelInfo
       !du
       integer :: du
       !su
       integer :: su
       !period
       integer :: period
       !nesting
       integer :: nesting
       !bound
       integer :: bound
       !offset
       integer :: offset
       !its
       integer, pointer :: its
       end type modelInfo
       !info
       type(modelInfo), dimension(1:58) :: info
       !inf
       integer, parameter :: inf=32767
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function igcm_atmosphereThread()
       implicit none
       if (bfgSUID==1) then
       igcm_atmosphereThread=.true.
       else
       igcm_atmosphereThread=.false.
       end if
       end function igcm_atmosphereThread
       logical function counterinst1Thread()
       implicit none
       if (bfgSUID==1) then
       counterinst1Thread=.true.
       else
       counterinst1Thread=.false.
       end if
       end function counterinst1Thread
       logical function counter_modinst1Thread()
       implicit none
       if (bfgSUID==1) then
       counter_modinst1Thread=.true.
       else
       counter_modinst1Thread=.false.
       end if
       end function counter_modinst1Thread
       logical function counter_modinst2Thread()
       implicit none
       if (bfgSUID==1) then
       counter_modinst2Thread=.true.
       else
       counter_modinst2Thread=.false.
       end if
       end function counter_modinst2Thread
       logical function transformer1Thread()
       implicit none
       if (bfgSUID==1) then
       transformer1Thread=.true.
       else
       transformer1Thread=.false.
       end if
       end function transformer1Thread
       logical function transformer2Thread()
       implicit none
       if (bfgSUID==1) then
       transformer2Thread=.true.
       else
       transformer2Thread=.false.
       end if
       end function transformer2Thread
       logical function transformer3Thread()
       implicit none
       if (bfgSUID==1) then
       transformer3Thread=.true.
       else
       transformer3Thread=.false.
       end if
       end function transformer3Thread
       logical function transformer5Thread()
       implicit none
       if (bfgSUID==1) then
       transformer5Thread=.true.
       else
       transformer5Thread=.false.
       end if
       end function transformer5Thread
       logical function counterinst2Thread()
       implicit none
       if (bfgSUID==2) then
       counterinst2Thread=.true.
       else
       counterinst2Thread=.false.
       end if
       end function counterinst2Thread
       logical function slab_seaiceThread()
       implicit none
       if (bfgSUID==2) then
       slab_seaiceThread=.true.
       else
       slab_seaiceThread=.false.
       end if
       end function slab_seaiceThread
       logical function transformer4Thread()
       implicit none
       if (bfgSUID==2) then
       transformer4Thread=.true.
       else
       transformer4Thread=.false.
       end if
       end function transformer4Thread
       logical function transformer6Thread()
       implicit none
       if (bfgSUID==2) then
       transformer6Thread=.true.
       else
       transformer6Thread=.false.
       end if
       end function transformer6Thread
       logical function ini_weightsThread()
       implicit none
       if (bfgSUID==3) then
       ini_weightsThread=.true.
       else
       ini_weightsThread=.false.
       end if
       end function ini_weightsThread
       logical function transformer7Thread()
       implicit none
       if (bfgSUID==3) then
       transformer7Thread=.true.
       else
       transformer7Thread=.false.
       end if
       end function transformer7Thread
       logical function weight_checkThread()
       implicit none
       if (bfgSUID==3) then
       weight_checkThread=.true.
       else
       weight_checkThread=.false.
       end if
       end function weight_checkThread
       logical function bfg_averagesThread()
       implicit none
       if (bfgSUID==3) then
       bfg_averagesThread=.true.
       else
       bfg_averagesThread=.false.
       end if
       end function bfg_averagesThread
       logical function counterinst4Thread()
       implicit none
       if (bfgSUID==3) then
       counterinst4Thread=.true.
       else
       counterinst4Thread=.false.
       end if
       end function counterinst4Thread
       logical function counterinst5Thread()
       implicit none
       if (bfgSUID==3) then
       counterinst5Thread=.true.
       else
       counterinst5Thread=.false.
       end if
       end function counterinst5Thread
       logical function copy_dummyThread()
       implicit none
       if (bfgSUID==3) then
       copy_dummyThread=.true.
       else
       copy_dummyThread=.false.
       end if
       end function copy_dummyThread
       logical function fixed_chemistryThread()
       implicit none
       if (bfgSUID==3) then
       fixed_chemistryThread=.true.
       else
       fixed_chemistryThread=.false.
       end if
       end function fixed_chemistryThread
       logical function fixed_icesheetThread()
       implicit none
       if (bfgSUID==3) then
       fixed_icesheetThread=.true.
       else
       fixed_icesheetThread=.false.
       end if
       end function fixed_icesheetThread
       logical function initialise_fixedicesheet_modThread()
       implicit none
       if (bfgSUID==3) then
       initialise_fixedicesheet_modThread=.true.
       else
       initialise_fixedicesheet_modThread=.false.
       end if
       end function initialise_fixedicesheet_modThread
       logical function counterinst3Thread()
       implicit none
       if (bfgSUID==3) then
       counterinst3Thread=.true.
       else
       counterinst3Thread=.false.
       end if
       end function counterinst3Thread
       logical function goldsteinThread()
       implicit none
       if (bfgSUID==3) then
       goldsteinThread=.true.
       else
       goldsteinThread=.false.
       end if
       end function goldsteinThread
       logical function interp_ocn_atminst1Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst1Thread=.true.
       else
       interp_ocn_atminst1Thread=.false.
       end if
       end function interp_ocn_atminst1Thread
       logical function interp_ocn_atminst2Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst2Thread=.true.
       else
       interp_ocn_atminst2Thread=.false.
       end if
       end function interp_ocn_atminst2Thread
       logical function interp_ocn_atminst3Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst3Thread=.true.
       else
       interp_ocn_atminst3Thread=.false.
       end if
       end function interp_ocn_atminst3Thread
       logical function interp_ocn_atminst4Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst4Thread=.true.
       else
       interp_ocn_atminst4Thread=.false.
       end if
       end function interp_ocn_atminst4Thread
       logical function interp_ocn_atminst5Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst5Thread=.true.
       else
       interp_ocn_atminst5Thread=.false.
       end if
       end function interp_ocn_atminst5Thread
       logical function interp_ocn_atminst6Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst6Thread=.true.
       else
       interp_ocn_atminst6Thread=.false.
       end if
       end function interp_ocn_atminst6Thread
       logical function interp_ocn_atminst7Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst7Thread=.true.
       else
       interp_ocn_atminst7Thread=.false.
       end if
       end function interp_ocn_atminst7Thread
       logical function interp_ocn_atminst8Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst8Thread=.true.
       else
       interp_ocn_atminst8Thread=.false.
       end if
       end function interp_ocn_atminst8Thread
       logical function interp_ocn_atminst9Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst9Thread=.true.
       else
       interp_ocn_atminst9Thread=.false.
       end if
       end function interp_ocn_atminst9Thread
       logical function interp_ocn_atminst10Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst10Thread=.true.
       else
       interp_ocn_atminst10Thread=.false.
       end if
       end function interp_ocn_atminst10Thread
       logical function interp_ocn_atminst11Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst11Thread=.true.
       else
       interp_ocn_atminst11Thread=.false.
       end if
       end function interp_ocn_atminst11Thread
       logical function interp_ocn_atminst12Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst12Thread=.true.
       else
       interp_ocn_atminst12Thread=.false.
       end if
       end function interp_ocn_atminst12Thread
       logical function interp_ocn_atminst13Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst13Thread=.true.
       else
       interp_ocn_atminst13Thread=.false.
       end if
       end function interp_ocn_atminst13Thread
       logical function interp_ocn_atminst14Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst14Thread=.true.
       else
       interp_ocn_atminst14Thread=.false.
       end if
       end function interp_ocn_atminst14Thread
       logical function interp_ocn_atminst15Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst15Thread=.true.
       else
       interp_ocn_atminst15Thread=.false.
       end if
       end function interp_ocn_atminst15Thread
       logical function interp_ocn_atminst16Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst16Thread=.true.
       else
       interp_ocn_atminst16Thread=.false.
       end if
       end function interp_ocn_atminst16Thread
       logical function interp_ocn_atminst17Thread()
       implicit none
       if (bfgSUID==3) then
       interp_ocn_atminst17Thread=.true.
       else
       interp_ocn_atminst17Thread=.false.
       end if
       end function interp_ocn_atminst17Thread
       logical function copy_tstarinst1Thread()
       implicit none
       if (bfgSUID==3) then
       copy_tstarinst1Thread=.true.
       else
       copy_tstarinst1Thread=.false.
       end if
       end function copy_tstarinst1Thread
       logical function copy_tstarinst2Thread()
       implicit none
       if (bfgSUID==3) then
       copy_tstarinst2Thread=.true.
       else
       copy_tstarinst2Thread=.false.
       end if
       end function copy_tstarinst2Thread
       logical function copy_albedoinst1Thread()
       implicit none
       if (bfgSUID==3) then
       copy_albedoinst1Thread=.true.
       else
       copy_albedoinst1Thread=.false.
       end if
       end function copy_albedoinst1Thread
       logical function copy_albedoinst2Thread()
       implicit none
       if (bfgSUID==3) then
       copy_albedoinst2Thread=.true.
       else
       copy_albedoinst2Thread=.false.
       end if
       end function copy_albedoinst2Thread
       subroutine commsSync()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       call mpi_barrier(mpi_comm_world,ierr)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       !rc
       integer :: rc
       !globalsize
       integer :: globalsize
       !globalrank
       integer :: globalrank
       !colour
       integer :: colour
       !key
       integer :: key
       !localsize
       integer :: localsize
       !localrank
       integer :: localrank
       !b2mtemp(3)
       integer :: b2mtemp(3)
       !mpi_comm_local
       integer :: mpi_comm_local
       call mpi_init(ierr)
       call mpi_comm_size(mpi_comm_world,globalsize,ierr)
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       ! arbitrarily decide on a unique colour forthis deployment unit
       colour=1
       if (globalsize.ne.3) then
       print *,"Error: (du",colour,"):","3 threads should be requested"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       key=0
       call mpi_comm_split(mpi_comm_world,colour,key,mpi_comm_local,ierr)
       call mpi_comm_size(mpi_comm_local,localsize,ierr)
       call mpi_comm_rank(mpi_comm_local,localrank,ierr)
       if (localsize.ne.3) then
       print *,"Error: 3 threads expected in du",colour
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! arbitrarily bind model threads to a local rank (and therefore a global rank)
       b2mtemp=0
       if (localrank.ge.0.and.localrank.le.0) then
       ! model name is 'igcm_atmosphere'
       ! model name is 'counter'
       ! model nameis 'counter_mod'
       ! model name is 'counter_mod'
       ! model name is 'transformer1'
       ! model name is 'transformer2'
       ! model name is 'transformer3'
       ! model name is 'transformer5'
       bfgSUID=1
       if (localrank==0) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.ge.1.and.localrank.le.1) then
       ! model name is 'counter'
       ! model name is 'slab_seaice'
       ! model name is 'transformer4'
       ! model name is 'transformer6'
       bfgSUID=2
       if (localrank==1) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.ge.2.and.localrank.le.2) then
       ! model name is 'ini_weights'
       ! model name is 'transformer7'
       ! model name is 'weight_check'
       ! model name is 'bfg_averages'
       ! model name is 'counter'
       ! model name is 'counter'
       ! model name is 'copy_dummy'
       ! model name is 'fixed_chemistry'
       ! model name is 'fixed_icesheet'
       ! model name is 'initialise_fixedicesheet_mod'
       ! model name is 'counter'
       ! model name is 'goldstein'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! modelname is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model nameis 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is'interp_ocn_atm'
       ! model name is 'interp_ocn_atm'
       ! model name is 'copy_tstar'
       ! model name is 'copy_tstar'
       ! model name is 'copy_albedo'
       ! model name is 'copy_albedo'
       bfgSUID=3
       if (localrank==2) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.lt.0.or.localrank.gt.2) then
       print *,"'Error: (du",colour,",0): localrank has unexpected value"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! distribute id's to all su's
       call mpi_allreduce(b2mtemp,b2mmap,3,mpi_integer,mpi_sum,mpi_comm_world,ierr)
       if (localrank==0) then
       print *,"du",colour,"bfg to mpi id map is",b2mmap
       end if
       end if
       end if
       end if
       end subroutine initComms
       subroutine finaliseComms()
       use mpi
       implicit none
       !globalrank
       integer :: globalrank
       !ierr
       integer :: ierr
       call mpi_finalize(ierr)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=bfg_averages.ini_averages.
       info(1)%du=b2mmap(3)
       info(1)%su=3
       info(1)%period=1
       info(1)%nesting=0
       info(1)%bound=1
       info(1)%offset=0
       nullify(info(1)%its)
       ! model.ep=initialise_fixedicesheet_mod.initialise_fixedicesheet.
       info(2)%du=b2mmap(3)
       info(2)%su=3
       info(2)%period=1
       info(2)%nesting=0
       info(2)%bound=1
       info(2)%offset=0
       nullify(info(2)%its)
       ! model.ep=fixed_chemistry.initialise_fixedchem.
       info(3)%du=b2mmap(3)
       info(3)%su=3
       info(3)%period=1
       info(3)%nesting=0
       info(3)%bound=1
       info(3)%offset=0
       nullify(info(3)%its)
       ! model.ep=igcm_atmosphere.initialise_igcmsurf.
       info(4)%du=b2mmap(1)
       info(4)%su=1
       info(4)%period=1
       info(4)%nesting=0
       info(4)%bound=1
       info(4)%offset=0
       nullify(info(4)%its)
       ! model.ep=igcm_atmosphere.initialise_atmos.
       info(5)%du=b2mmap(1)
       info(5)%su=1
       info(5)%period=1
       info(5)%nesting=0
       info(5)%bound=1
       info(5)%offset=0
       nullify(info(5)%its)
       ! model.ep=slab_seaice.initialise_slabseaice.
       info(6)%du=b2mmap(2)
       info(6)%su=2
       info(6)%period=1
       info(6)%nesting=0
       info(6)%bound=1
       info(6)%offset=0
       nullify(info(6)%its)
       ! model.ep=goldstein.initialise_goldstein.
       info(7)%du=b2mmap(3)
       info(7)%su=3
       info(7)%period=1
       info(7)%nesting=0
       info(7)%bound=1
       info(7)%offset=0
       nullify(info(7)%its)
       ! model.ep=interp_ocn_atm.interp_ocn_atm.1
       info(8)%du=b2mmap(3)
       info(8)%su=3
       info(8)%period=1
       info(8)%nesting=0
       info(8)%bound=1
       info(8)%offset=0
       nullify(info(8)%its)
       ! model.ep=copy_tstar.copytstar.1
       info(9)%du=b2mmap(3)
       info(9)%su=3
       info(9)%period=1
       info(9)%nesting=0
       info(9)%bound=1
       info(9)%offset=0
       nullify(info(9)%its)
       ! model.ep=interp_ocn_atm.interp_ocn_atm.2
       info(10)%du=b2mmap(3)
       info(10)%su=3
       info(10)%period=1
       info(10)%nesting=0
       info(10)%bound=1
       info(10)%offset=0
       nullify(info(10)%its)
       ! model.ep=copy_albedo.copyalbedo.1
       info(11)%du=b2mmap(3)
       info(11)%su=3
       info(11)%period=1
       info(11)%nesting=0
       info(11)%bound=1
       info(11)%offset=0
       nullify(info(11)%its)
       ! model.ep=ini_weights.ini_weights.
       info(12)%du=b2mmap(3)
       info(12)%su=3
       info(12)%period=1
       info(12)%nesting=0
       info(12)%bound=1
       info(12)%offset=0
       nullify(info(12)%its)
       ! model.ep=weight_check.weightcheck.
       info(13)%du=b2mmap(3)
       info(13)%su=3
       info(13)%period=1
       info(13)%nesting=0
       info(13)%bound=1
       info(13)%offset=0
       nullify(info(13)%its)
       ! model.ep=counter.counter.1
       info(14)%du=b2mmap(1)
       info(14)%su=1
       info(14)%period=1
       info(14)%nesting=1
       info(14)%bound=864
       info(14)%offset=0
       info(14)%its=>its1
       ! model.ep=igcm_atmosphere.igcm3_adiab.
       info(15)%du=b2mmap(1)
       info(15)%su=1
       info(15)%period=1
       info(15)%nesting=1
       info(15)%bound=864
       info(15)%offset=0
       info(15)%its=>its1
       ! model.ep=transformer1.new_transformer_1.
       info(16)%du=b2mmap(1)
       info(16)%su=1
       info(16)%period=1
       info(16)%nesting=1
       info(16)%bound=864
       info(16)%offset=0
       info(16)%its=>its1
       ! model.ep=counter_mod.counter_mod.1
       info(17)%du=b2mmap(1)
       info(17)%su=1
       info(17)%period=1
       info(17)%nesting=2
       info(17)%bound=6
       info(17)%offset=0
       info(17)%its=>its2
       ! model.ep=igcm_atmosphere.igcm_land_surflux.
       info(18)%du=b2mmap(1)
       info(18)%su=1
       info(18)%period=1
       info(18)%nesting=2
       info(18)%bound=6
       info(18)%offset=0
       info(18)%its=>its2
       ! model.ep=igcm_atmosphere.igcm_land_blayer.
       info(19)%du=b2mmap(1)
       info(19)%su=1
       info(19)%period=1
       info(19)%nesting=2
       info(19)%bound=6
       info(19)%offset=0
       info(19)%its=>its2
       ! model.ep=counter_mod.counter_mod.2
       info(20)%du=b2mmap(1)
       info(20)%su=1
       info(20)%period=1
       info(20)%nesting=2
       info(20)%bound=6
       info(20)%offset=0
       info(20)%its=>its3
       ! model.ep=igcm_atmosphere.igcm_ocean_surflux.
       info(21)%du=b2mmap(1)
       info(21)%su=1
       info(21)%period=1
       info(21)%nesting=2
       info(21)%bound=6
       info(21)%offset=0
       info(21)%its=>its3
       ! model.ep=igcm_atmosphere.igcm_ocean_blayer.
       info(22)%du=b2mmap(1)
       info(22)%su=1
       info(22)%period=1
       info(22)%nesting=2
       info(22)%bound=6
       info(22)%offset=0
       info(22)%its=>its3
       ! model.ep=transformer2.new_transformer_2.
       info(23)%du=b2mmap(1)
       info(23)%su=1
       info(23)%period=1
       info(23)%nesting=1
       info(23)%bound=864
       info(23)%offset=0
       info(23)%its=>its1
       ! model.ep=igcm_atmosphere.igcm3_diab.
       info(24)%du=b2mmap(1)
       info(24)%su=1
       info(24)%period=1
       info(24)%nesting=1
       info(24)%bound=864
       info(24)%offset=0
       info(24)%its=>its1
       ! model.ep=transformer3.new_transformer_3.
       info(25)%du=b2mmap(1)
       info(25)%su=1
       info(25)%period=1
       info(25)%nesting=1
       info(25)%bound=864
       info(25)%offset=0
       info(25)%its=>its1
       ! model.ep=counter.counter.2
       info(26)%du=b2mmap(2)
       info(26)%su=2
       info(26)%period=6
       info(26)%nesting=1
       info(26)%bound=864
       info(26)%offset=0
       info(26)%its=>its1
       ! model.ep=slab_seaice.slabseaice.
       info(27)%du=b2mmap(2)
       info(27)%su=2
       info(27)%period=6
       info(27)%nesting=1
       info(27)%bound=864
       info(27)%offset=0
       info(27)%its=>its1
       ! model.ep=transformer4.new_transformer_4.
       info(28)%du=b2mmap(2)
       info(28)%su=2
       info(28)%period=6
       info(28)%nesting=1
       info(28)%bound=864
       info(28)%offset=0
       info(28)%its=>its1
       ! model.ep=transformer5.new_transformer_5.
       info(29)%du=b2mmap(1)
       info(29)%su=1
       info(29)%period=1
       info(29)%nesting=1
       info(29)%bound=864
       info(29)%offset=0
       info(29)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.3
       info(30)%du=b2mmap(3)
       info(30)%su=3
       info(30)%period=48
       info(30)%nesting=1
       info(30)%bound=864
       info(30)%offset=0
       info(30)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.4
       info(31)%du=b2mmap(3)
       info(31)%su=3
       info(31)%period=48
       info(31)%nesting=1
       info(31)%bound=864
       info(31)%offset=0
       info(31)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.5
       info(32)%du=b2mmap(3)
       info(32)%su=3
       info(32)%period=48
       info(32)%nesting=1
       info(32)%bound=864
       info(32)%offset=0
       info(32)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.6
       info(33)%du=b2mmap(3)
       info(33)%su=3
       info(33)%period=48
       info(33)%nesting=1
       info(33)%bound=864
       info(33)%offset=0
       info(33)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.7
       info(34)%du=b2mmap(3)
       info(34)%su=3
       info(34)%period=48
       info(34)%nesting=1
       info(34)%bound=864
       info(34)%offset=0
       info(34)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.8
       info(35)%du=b2mmap(3)
       info(35)%su=3
       info(35)%period=48
       info(35)%nesting=1
       info(35)%bound=864
       info(35)%offset=0
       info(35)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.9
       info(36)%du=b2mmap(3)
       info(36)%su=3
       info(36)%period=48
       info(36)%nesting=1
       info(36)%bound=864
       info(36)%offset=0
       info(36)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.10
       info(37)%du=b2mmap(3)
       info(37)%su=3
       info(37)%period=48
       info(37)%nesting=1
       info(37)%bound=864
       info(37)%offset=0
       info(37)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.11
       info(38)%du=b2mmap(3)
       info(38)%su=3
       info(38)%period=48
       info(38)%nesting=1
       info(38)%bound=864
       info(38)%offset=0
       info(38)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.12
       info(39)%du=b2mmap(3)
       info(39)%su=3
       info(39)%period=48
       info(39)%nesting=1
       info(39)%bound=864
       info(39)%offset=0
       info(39)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.13
       info(40)%du=b2mmap(3)
       info(40)%su=3
       info(40)%period=48
       info(40)%nesting=1
       info(40)%bound=864
       info(40)%offset=0
       info(40)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.14
       info(41)%du=b2mmap(3)
       info(41)%su=3
       info(41)%period=48
       info(41)%nesting=1
       info(41)%bound=864
       info(41)%offset=0
       info(41)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.15
       info(42)%du=b2mmap(3)
       info(42)%su=3
       info(42)%period=48
       info(42)%nesting=1
       info(42)%bound=864
       info(42)%offset=0
       info(42)%its=>its1
       ! model.ep=counter.counter.3
       info(43)%du=b2mmap(3)
       info(43)%su=3
       info(43)%period=48
       info(43)%nesting=1
       info(43)%bound=864
       info(43)%offset=0
       info(43)%its=>its1
       ! model.ep=goldstein.goldstein.
       info(44)%du=b2mmap(3)
       info(44)%su=3
       info(44)%period=48
       info(44)%nesting=1
       info(44)%bound=864
       info(44)%offset=0
       info(44)%its=>its1
       ! model.ep=copy_dummy.copy_dummy.
       info(45)%du=b2mmap(3)
       info(45)%su=3
       info(45)%period=48
       info(45)%nesting=1
       info(45)%bound=864
       info(45)%offset=0
       info(45)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.16
       info(46)%du=b2mmap(3)
       info(46)%su=3
       info(46)%period=48
       info(46)%nesting=1
       info(46)%bound=864
       info(46)%offset=0
       info(46)%its=>its1
       ! model.ep=copy_tstar.copytstar.2
       info(47)%du=b2mmap(3)
       info(47)%su=3
       info(47)%period=48
       info(47)%nesting=1
       info(47)%bound=864
       info(47)%offset=0
       info(47)%its=>its1
       ! model.ep=interp_ocn_atm.interp_ocn_atm.17
       info(48)%du=b2mmap(3)
       info(48)%su=3
       info(48)%period=48
       info(48)%nesting=1
       info(48)%bound=864
       info(48)%offset=0
       info(48)%its=>its1
       ! model.ep=copy_albedo.copyalbedo.2
       info(49)%du=b2mmap(3)
       info(49)%su=3
       info(49)%period=48
       info(49)%nesting=1
       info(49)%bound=864
       info(49)%offset=0
       info(49)%its=>its1
       ! model.ep=counter.counter.4
       info(50)%du=b2mmap(3)
       info(50)%su=3
       info(50)%period=240
       info(50)%nesting=1
       info(50)%bound=864
       info(50)%offset=0
       info(50)%its=>its1
       ! model.ep=fixed_chemistry.fixedchem.
       info(51)%du=b2mmap(3)
       info(51)%su=3
       info(51)%period=240
       info(51)%nesting=1
       info(51)%bound=864
       info(51)%offset=0
       info(51)%its=>its1
       ! model.ep=counter.counter.5
       info(52)%du=b2mmap(3)
       info(52)%su=3
       info(52)%period=240
       info(52)%nesting=1
       info(52)%bound=864
       info(52)%offset=0
       info(52)%its=>its1
       ! model.ep=fixed_icesheet.fixedicesheet.
       info(53)%du=b2mmap(3)
       info(53)%su=3
       info(53)%period=240
       info(53)%nesting=1
       info(53)%bound=864
       info(53)%offset=0
       info(53)%its=>its1
       ! model.ep=bfg_averages.write_averages.
       info(54)%du=b2mmap(3)
       info(54)%su=3
       info(54)%period=48
       info(54)%nesting=1
       info(54)%bound=864
       info(54)%offset=0
       info(54)%its=>its1
       ! model.ep=transformer6.new_transformer_6.
       info(55)%du=b2mmap(2)
       info(55)%su=2
       info(55)%period=6
       info(55)%nesting=1
       info(55)%bound=864
       info(55)%offset=0
       info(55)%its=>its1
       ! model.ep=transformer7.new_transformer_7.
       info(56)%du=b2mmap(3)
       info(56)%su=3
       info(56)%period=48
       info(56)%nesting=1
       info(56)%bound=864
       info(56)%offset=0
       info(56)%its=>its1
       ! model.ep=igcm_atmosphere.end_atmos.
       info(57)%du=b2mmap(1)
       info(57)%su=1
       info(57)%period=1
       info(57)%nesting=0
       info(57)%bound=1
       info(57)%offset=0
       nullify(info(57)%its)
       ! model.ep=goldstein.end_goldstein.
       info(58)%du=b2mmap(3)
       info(58)%su=3
       info(58)%period=1
       info(58)%nesting=0
       info(58)%bound=1
       info(58)%offset=0
       nullify(info(58)%its)
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !currentNesting
       integer :: currentNesting
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !targetpos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getNext=findNext(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestNext
       integer :: nestNext
       !currentMin
       integer :: currentMin
       !remainIters
       integer :: remainIters
       !waitIters
       integer :: waitIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1.or.its==0) then
       its=1
       end if
       if (list(pos).gt.point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestNext=findNext(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters.gt.0) then
       if (its+1.lt.info(list(pos))%offset) then
       waitIters=info(list(pos))%offset - its
       else if (its+1==info(list(pos))%offset) then
       waitIters=1
       else
       waitIters=info(list(pos))%period - mod(its - info(list(pos))%offset,info(list(pos))%period)
       end if
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters.lt.currentMin.and.waitIters.le.remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
       end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !currentNesting
       integer :: currentNesting
       !targetpos
       integer :: targetpos
       !currentMin
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getLast=findLast(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestLast
       integer :: nestLast
       !currentMin
       integer :: currentMin
       !elapsedIters
       integer :: elapsedIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos).ge.point) then
       its=its - 1
       end if
       if (.not.(associated(info(list(pos))%its)).and.((info(point)%nesting==1.and.its1.gt.info(point)%period).or.(info(point)%nesting.gt.1.and.its1.gt.1))) then
       continue
       else if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestLast=findLast(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       if (its.gt.0.and.its.ge.info(list(pos))%offset) then
       elapsedIters=mod(its - info(list(pos))%offset,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters.lt.currentMin.and.elapsedIters.lt.its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
       end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       !i
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       do i=pos+1,lsize
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findStartPoint=1
       return
       end if
       do i=pos-1,1,-1
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
       findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findEndPoint=lsize
       return
       end if
       do i=pos+1,lsize
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
       findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target1
