#
# $Id: makefile 3320 2007-06-14 14:22:56Z andrew-price $
#

export

include makefile.arc

C_DIR             = src/c
CONFIG_DIR        = configs
F_DIR             = src/fortran

LOCALFLAGS = $(FFLAGS) $(GENIEPREC) $(BOUNDS_FLAGS) 

OTHER_FILES = makefile makefile.arc

EXE = genie.exe
EXECUTABLES = $(EXE) nccompare

OBJS1 =	BFG2Target1.$(OBJ_EXT) \
	BFG2Main1.$(OBJ_EXT)
OBJS2 = end_graphics.$(OBJ_EXT)
OBJS3 = ini_graphics.$(OBJ_EXT)
OBJS4 = initialise_genie.$(OBJ_EXT) \
	write_averages.$(OBJ_EXT) \
	ini_averages.$(OBJ_EXT) \
	interp_ocn_atm.$(OBJ_EXT) \
	ini_weights.$(OBJ_EXT) \
        genie_restarts.$(OBJ_EXT)
OBJS5 = awi_genie.$(OBJ_EXT) \
	end_genie.$(OBJ_EXT) \
	extrap.$(OBJ_EXT)
OBJS6 = precision.$(OBJ_EXT) #\
	genie_global.$(OBJ_EXT) \
	averages.$(OBJ_EXT) \
	coasts.$(OBJ_EXT) \
	weights.$(OBJ_EXT) \
	check_fluxes.$(OBJ_EXT)	\
        genie_readsizes.$(OBJ_EXT) \
	write_netcdf_genie.$(OBJ_EXT) \
	genie_ini_wrappers.$(OBJ_EXT) \
	genie_loop_wrappers.$(OBJ_EXT) \
	genie_end_wrappers.$(OBJ_EXT)
BFGf90= bfg_averages.$(OBJ_EXT) \
	BFG2Target1.$(OBJ_EXT) \
	BFG2InPlace_bfg_averages.$(OBJ_EXT) \
	BFG2InPlace_fixed_chemistry.$(OBJ_EXT) \
        BFG2InPlace_fixed_chemistry_wrapper.$(OBJ_EXT) \
	BFG2InPlace_fixed_icesheet.$(OBJ_EXT) \
        BFG2InPlace_fixed_icesheet_wrapper.$(OBJ_EXT) \
	BFG2InPlace_initialise_fixedicesheet_mod.$(OBJ_EXT) \
	BFG2InPlace_slab_seaice.$(OBJ_EXT) \
        BFG2InPlace_slab_seaice_wrapper.$(OBJ_EXT) \
	BFG2InPlace_transformer2.$(OBJ_EXT) \
	BFG2InPlace_transformer3.$(OBJ_EXT) \
        BFG2InPlace_transformer3_wrapper.$(OBJ_EXT) \
	BFG2InPlace_transformer4.$(OBJ_EXT) \
	BFG2InPlace_transformer5.$(OBJ_EXT) \
	BFG2InPlace_transformer6.$(OBJ_EXT) \
	BFG2InPlace_transformer7.$(OBJ_EXT) \
	BFG2InPlace_igcm_atmosphere.$(OBJ_EXT) \
	BFG2InPlace_copy_albedo.$(OBJ_EXT) \
	BFG2InPlace_copy_albedo_wrapper.$(OBJ_EXT) \
	BFG2InPlace_copy_tstar.$(OBJ_EXT) \
	BFG2InPlace_copy_tstar_wrapper.$(OBJ_EXT) \
	BFG2InPlace_ini_weights.$(OBJ_EXT) \
	BFG2InPlace_interp_ocn_atm.$(OBJ_EXT) \
	BFG2InPlace_weight_check.$(OBJ_EXT)
BFGf77=	counter.$(OBJ_EXT) \
	counter_mod.$(OBJ_EXT) \
	new_transformer_1.$(OBJ_EXT) \
	new_transformer_2.$(OBJ_EXT) \
	new_transformer_3.$(OBJ_EXT) \
	new_transformer_4.$(OBJ_EXT) \
	new_transformer_5.$(OBJ_EXT) \
	new_transformer_6.$(OBJ_EXT) \
	new_transformer_7.$(OBJ_EXT) \
	copy_albedo.$(OBJ_EXT) \
	copy_dummy.$(OBJ_EXT) \
	copy_tstar.$(OBJ_EXT) \
	ini_weights-bfg.$(OBJ_EXT) \
	interp_ocn_atm-bfg.$(OBJ_EXT) \
	weight_check.$(OBJ_EXT) 
CONTROL_OBJS = 	genie_control.$(OBJ_EXT)

#OBJECTS = $(CONTROL_OBJS) $(OBJS6) $(OBJS2) $(OBJS3) $(OBJS4) $(OBJS5) $(OBJS1)
OBJECTS = $(OBJS6) $(OBJS5) $(BFGf90) $(BFGf77) $(OBJS1)

LOCAL_NAMES = gem

MODULE_NAMES = igcm3 \
               slabocean \
               slabseaice \
               fixedocean \
               fixedseaice \
               goldstein \
               embm \
               seaice \
               fixedatmos \
               land \
               fixedland \
               fixedicesheet \
               fixedchem \
	       atchem \
               biogem \
               ichem

UTIL_NAMES = aux1 \
             blas1 \
             fft1 \
             nc1
ifeq ($(GRAPHICS),ON)  # see makefile.arc
  UTIL_NAMES += zxp
endif
UTIL_NAMES += util1 


# Construct variables/lists used for linking and dependencies
# construct vars for the 'external' modules
MODULE_LIBS_PROTO=$(MODULE_NAMES:%=$(GENIE_ROOT)/genie-%/lib/$(LIB_PREFIX))
MODULE_LIBS = $(addsuffix .$(LIB_EXT),$(join $(MODULE_LIBS_PROTO),$(MODULE_NAMES)))
ifeq ($(MACHINE),WIN32)
	LINK_MODULE_LIBRARIES = $(MODULE_NAMES:%=$(LIB_SEARCH_FLAG)$(GENIE_ROOT_WIN)\\genie-%\\lib)
	LINK_MODULE_LIBRARIES += $(MODULE_NAMES:%=$(LIB_FLAG)lib%.$(LIB_EXT))
else
	LINK_MODULE_LIBRARIES = $(MODULE_NAMES:%=$(LIB_SEARCH_FLAG)$(GENIE_ROOT)/genie-%/lib)
	LINK_MODULE_LIBRARIES += $(MODULE_NAMES:%=$(LIB_FLAG)%)
endif
# link in the slap library
# note that icesheet module target builds glimmer and slap
ifeq ($(FLAG_GLIMMER),ON)  # see makefile.arc
  LINK_MODULE_LIBRARIES += -lslap
endif

# the 'local' modules
LOCAL_LIBS_PROTO=$(LOCAL_NAMES:%=$(MAIN_DIR)/src/fortran/cmn%/$(LIB_PREFIX)cmn)
LOCAL_LIBS = $(addsuffix .$(LIB_EXT),$(join $(LOCAL_LIBS_PROTO),$(LOCAL_NAMES)))
ifeq ($(MACHINE),WIN32)
	LINK_MODULE_LIBRARIES += $(LOCAL_NAMES:%=$(LIB_SEARCH_FLAG)$(GENIE_ROOT_WIN)\\genie-main\\src\\fortran\\cmn%)
	LINK_MODULE_LIBRARIES += $(LOCAL_NAMES:%=$(LIB_FLAG)libcmn%.$(LIB_EXT))
else
	LINK_MODULE_LIBRARIES += $(LOCAL_NAMES:%=$(LIB_SEARCH_FLAG)$(MAIN_DIR)/src/fortran//cmn%)
	LINK_MODULE_LIBRARIES += $(LOCAL_NAMES:%=$(LIB_FLAG)cmn%)
endif

# f90 modules included
INCLUDE_MODULE_LIBRARIES = $(MOD_INC_FLAG)$(INC_DIR)

# NB need to fix for WIN32 
ifeq ($(FLAG_GLIMMER),ON)  # see makefile.arc
  INCLUDE_MODULE_LIBRARIES += $(MOD_INC_FLAG)$(ICESHEET_DIR)/include
endif

UTIL_LIBS_PROTO=$(UTIL_NAMES:%=$(LIB_DIR)/lib%/$(LIB_PREFIX))
UTIL_LIBS = $(addsuffix .$(LIB_EXT),$(join $(UTIL_LIBS_PROTO),$(UTIL_NAMES)))
ifeq ($(MACHINE),WIN32)
	LINK_UTIL_LIBRARIES = $(UTIL_NAMES:%=$(LIB_SEARCH_FLAG)$(LIB_DIR_WIN)\\lib%)
	LINK_UTIL_LIBRARIES += $(UTIL_NAMES:%=$(LIB_FLAG)lib%.$(LIB_EXT))
else
	LINK_UTIL_LIBRARIES = $(UTIL_NAMES:%=$(LIB_SEARCH_FLAG)$(LIB_DIR)/lib%)
	LINK_UTIL_LIBRARIES += $(UTIL_NAMES:%=$(LIB_FLAG)%)
endif

# this is very odd--I don't understand why this is necessary.
all: $(EXECUTABLES)

.PHONY : cleanall clean cleanutils cleanmodules \
         $(CLEAN_UTIL_TARGETS) $(CLEAN_MODULE_TARGETS) \
	 tags tagutils tagmodules $(CLEAN_LOCAL_TARGETS) \
         $(TAG_UTIL_TARGETS) $(TAG_MODULE_TARGETS) \
	 FORCE

# ================= CLEANING RULES ===================
# construct target names for cleaning rules
CLEAN_LOCAL_TARGETS = $(LOCAL_NAMES:%=clean_%)
CLEAN_MODULE_TARGETS = $(MODULE_NAMES:%=clean_%)
CLEAN_UTIL_TARGETS = $(UTIL_NAMES:%=clean_%)

# cleanall - cleans locally, in modules and util libs
# Note that an individual module or util can be cleaned
# with an appropriate target, e.g. 'make clean_igcm3'
cleanall : clean cleanutils cleanmodules

# clean  - just local files
clean : $(CLEAN_LOCAL_TARGETS)
	\rm -f *.$(OBJ_EXT) $(EXE) *.dep *.mod
	cd $(C_DIR); $(MAKE) clean

$(CLEAN_LOCAL_TARGETS) : clean_% :
	\cd $(F_DIR)/cmn$*; $(MAKE) clean

# cleanutils - just util libs
cleanutils : $(CLEAN_UTIL_TARGETS)
$(CLEAN_UTIL_TARGETS) : clean_% :
	\cd $(LIB_DIR)/$(LIB_PREFIX)$*; $(MAKE) clean

# cleanmodules - just modules
cleanmodules : $(CLEAN_MODULE_TARGETS)
$(CLEAN_MODULE_TARGETS) : clean_% :
	\cd $(GENIE_ROOT)/genie-$*; $(MAKE) clean

# =========== OBJECT FILES AND EXECUTABLE ============

# CONTROL_OBJS is made first, so that components can use
# e.g. the grid size information
$(EXE) : $(CONTROL_OBJS) $(UTIL_LIBS) $(LOCAL_LIBS) $(MODULE_LIBS) $(OBJECTS) $(OTHER_FILES)
	$(F77_LD) $(LDFLAGS) $(OBJECTS) $(OUT_FLAG)$(EXE) \
        $(LINK_MODULE_LIBRARIES) $(LINK_UTIL_LIBRARIES) \
	$(NETCDF) $(GLIMMER) $(FORTRANLIBS) $(XLIB) $(BOUNDS_FLAGS)

# Rule to make custom netcdf comparison program
nccompare :
	\cd $(C_DIR); $(MAKE)

# Rule to make custom netcdf comparison program
errfn_ea_go_gs :
	\cd $(F_DIR); $(MAKE)

# rule to build 'local' libs
$(LOCAL_LIBS) : FORCE
	\cd $(@D); $(MAKE) $(@F)
	\cp $(@D)/*.mod $(INC_DIR)

# rule to build module libraries
$(MODULE_LIBS) : FORCE
	\cd $(@D)/..; $(MAKE) $(@F)
	if [ -d $(@D)/../mod ] ; then \
		\cp $(@D)/../mod/*.mod $(INC_DIR); \
	fi

# rule to build util libraries
$(UTIL_LIBS) : FORCE
	\cd $(@D); $(MAKE) $(@F)

# a forcing rule
FORCE:


# pattern rules to make object files
# one rule for compilation permutation
$(OBJS1): %.$(OBJ_EXT): %.f90 $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F77FLAGS) \
	$(INCLUDE_MODULE_LIBRARIES) $(DOPTS) $(NETCDF_INC) $(GLIMMER_INC) $(GENIEGLIMMEROPTS) \
	$(RESOLUTION_FLAGS) $< $(OBJ_FLAG)$@

$(BFGf90): %.$(OBJ_EXT): %.f90
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(INCLUDE_MODULE_LIBRARIES) $(F90FLAGS) $< $(OBJ_FLAG)$@

$(BFGf77): %.$(OBJ_EXT): %.F
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) -I/home/armstroc/mpich2-install/include $(F77FLAGS) $< $(OBJ_FLAG)$@

$(OBJS2): %.$(OBJ_EXT): %.F $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F77FLAGS) \
	$(DOPTS) $< $(OBJ_FLAG)$@

$(OBJS3): %.$(OBJ_EXT): %.F $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F77FLAGS) $(DOPTS) \
	$(LIBZXPPRECISIONOPTS) $(INCLUDE_MODULE_LIBRARIES) $< $(OBJ_FLAG)$@

$(OBJS4): %.$(OBJ_EXT): %.F $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F77FLAGS) \
	$(LIBUTIL1PRECISIONOPTS) $(INCLUDE_MODULE_LIBRARIES) $(GLIMMER_INC) \
	$(RESOLUTION_FLAGS) $< $(OBJ_FLAG)$@

$(OBJS5): %.$(OBJ_EXT): %.f $(OTHER_FILES)
	$(F77) $(COMPILEONLY) $(LOCALFLAGS) $(F77FLAGS) $< $(OBJ_FLAG)$@

$(OBJS6): %.$(OBJ_EXT): %.f90 $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F90FLAGS) \
	$(LIBUTIL1PRECISIONOPTS) $(INCLUDE_MODULE_LIBRARIES) $(GLIMMER_INC) $(GENIEGLIMMEROPTS) \
	$(RESOLUTION_FLAGS) $(IGCMHRLATSOPTS) $< $(OBJ_FLAG)$@

$(CONTROL_OBJS): %.$(OBJ_EXT): %.f90 $(OTHER_FILES)
	$(F77) $(FPPFLAGS) $(COMPILEONLY) $(LOCALFLAGS) $(F90FLAGS) \
	$(GENIEGLIMMEROPTS) $< $(OBJ_FLAG)$@

# ================== TESTING RULES ===================
include testing.mak

# ==== RULES TO ATUO' CREATE DEPENDENCY FILES ==== 
# Rules to create '.dep' files from sources
# We want make to halt if $(PYTHON) is not found
# A failing 'which' command coupled with a -e shell invocation
# provides the trigger to halt the compilation. 
MAKE_DEPS = @ $(SHELL) -ec 'which $(PYTHON) > /dev/null; \
		$(PYTHON) finc.py $< | sed '\''s/$*\.$(OBJ_EXT)/& $@/g'\'' > $@'

%.dep: %.f
	$(MAKE_DEPS)

%.dep: %.F
	$(MAKE_DEPS)

%.dep: %.f90
	$(MAKE_DEPS)

# Include the '.dep' files
ifneq ($(MAKECMDGOALS),clean)
include $(OBJECTS:.$(OBJ_EXT)=.dep)
endif

# ==== RULES FOR AUTOMATIC DOCUMENTATION =====

docs :
	\cd $(DOC_DIR); $(MAKE) all

# ==== RULES FOR MAKING TAGS =====
# construct target names for cleaning rules
TAG_MODULE_TARGETS = $(MODULE_NAMES:%=tag_%)
TAG_UTIL_TARGETS = $(UTIL_NAMES:%=tag_%)

tags: tagmain tagutils tagmodules

tagutils : $(TAG_UTIL_TARGETS)
$(TAG_UTIL_TARGETS) : tag_% :
	\cd $(LIB_DIR)/$(LIB_PREFIX)$*; $(MAKE) tags

tagmodules : $(TAG_MODULE_TARGETS)
$(TAG_MODULE_TARGETS) : tag_% :
	\cd $(GENIE_ROOT)/genie-$*; $(MAKE) tags

tagmain:
	if [ -f $(TAGFILE) ] ; then \rm $(TAGFILE) ; fi
	@ $(SHELL) -ec 'which $(ETAGS) > /dev/null; \
		$(ETAGS) -a -o $(TAGFILE) *.F; \
		$(ETAGS) -a -o $(TAGFILE) *.f; \
		$(ETAGS) -a -o $(TAGFILE) *.f90'
