      subroutine new_transformer_4(ilon1_atm,
     +                             ilat1_atm,
     +                             seaicefrac_atm_meanocn,
     +                             seaicefrac_atm,
     +                             ksic_loop,
     +                             kocn_loop,
     +                             conductflux_atm_meanocn,
     +                             conductflux_atm)
c
      implicit none
c
      integer i,j
c
      integer ilon1_atm,ilat1_atm
      real :: seaicefrac_atm_meanocn(ilon1_atm,ilat1_atm)
      real :: seaicefrac_atm(ilon1_atm,ilat1_atm)
      integer ksic_loop,kocn_loop
      real :: conductflux_atm_meanocn(ilon1_atm,ilat1_atm)
      real :: conductflux_atm(ilon1_atm,ilat1_atm)
c      logical first
c      data first/.true./
c      save first
c      if(first) print*,"seaicefrac_atm_meanocn=",seaicefrac_atm_meanocn
c      if(first) print*,"conductflx_atm_meanocn=",conductflux_atm_meanocn
c      first=.false.
      
            do i=1,ilon1_atm
               do j=1,ilat1_atm
                  seaicefrac_atm_meanocn(i,j)=
     :                 seaicefrac_atm_meanocn(i,j)+
     :                 seaicefrac_atm(i,j)*
     :                 ksic_loop/(1.*kocn_loop)
                  conductflux_atm_meanocn(i,j)=
     :                 conductflux_atm_meanocn(i,j)+
     :                 conductflux_atm(i,j)*
     :                 ksic_loop/(1.*kocn_loop)
               enddo
            enddo
c
      end
