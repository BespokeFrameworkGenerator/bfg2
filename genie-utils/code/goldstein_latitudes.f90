    subroutine GOLDSTEIN_LatitudesInitialise( coordsCount, latitudeCoords )
        implicit none
        integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
        
        integer, intent(IN)                       :: coordsCount
        real, intent(OUT), dimension(coordsCount) :: latitudeCoords
        integer                                   :: latLoop
        integer                                   :: latitudesCount

        ! The following code was copied from subroutine "initialise_goldstein"
        ! in genie\genie-goldstein\src\fortran\initialise_goldstein.F
        
        real(kind=dbl)                              :: pi, th0, th1
        real(kind=dbl)                              :: s0, s1, dscon
        real(kind=dbl), allocatable, dimension(:)   :: s, sv

        ! GOLDSTEIN grid has N point coords and N+1 edge coords
        latitudesCount = ( coordsCount - 1 ) / 2 
        
        allocate( s( latitudesCount ) )
        
        ! N.B. sv array has an extra element, sv(0)
        allocate( sv( 0:latitudesCount ) )
        
        pi = 4*atan(1.0)
        th0 = - pi/2
        th1 = pi/2 
        s0 = sin(th0)
        s1 = sin(th1)
        dscon = (s1-s0)/latitudesCount
        sv(0) = s0
        
        do latLoop=1,latitudesCount
            sv(latLoop) = s0 + latLoop*dscon
            s(latLoop) = sv(latLoop) - 0.5*dscon

            ! Place the point coords in the first half of the output
            ! array and the edge/corner coords in the second half
            latitudeCoords(latLoop)=real(asin(s(latLoop))*180.0/pi, &
                kind(latitudeCoords))
            latitudeCoords(latLoop + latitudesCount)= &
                real(asin(sv(latLoop-1))*180.0/pi, kind(latitudeCoords))
        end do
        latitudeCoords(latitudesCount + latitudesCount + 1)=real( &
            asin(sv(latitudesCount))*180.0/pi,kind(latitudeCoords))
            
        deallocate( s )
        deallocate( sv )

    end subroutine GOLDSTEIN_LatitudesInitialise

