      subroutine new_transformer_3(ilon1_atm,
     +                             ilat1_atm,
     +                             latent_atm_meansic,
     +                             surf_latent_atm,
     +                             katm_loop,
     +                             ksic_loop,
     +                             sensible_atm_meansic,
     +                             surf_sensible_atm,
     +                             netsolar_atm_meansic,
     +                             netsolar_atm,
     +                             netlong_atm_meansic,
     +                             netlong_atm,
     +                             stressx_atm_meansic,
     +                             surf_stressx_atm,
     +                             stressy_atm_meansic,
     +                             surf_stressy_atm,
     +                             precip_atm_meansic,
     +                             precip_atm,
     +                             kocn_loop)
c
      implicit none
c
      integer i,j
      integer ilat1_atm,ilon1_atm
      real :: latent_atm_meansic(ilon1_atm,ilat1_atm)
      real :: surf_latent_atm(ilon1_atm,ilat1_atm)
      integer katm_loop,ksic_loop,kocn_loop
      real :: sensible_atm_meansic(ilon1_atm,ilat1_atm)
      real :: surf_sensible_atm(ilon1_atm,ilat1_atm)
      real :: netsolar_atm_meansic(ilon1_atm,ilat1_atm)
      real :: netsolar_atm(ilon1_atm,ilat1_atm)
      real :: netlong_atm_meansic(ilon1_atm,ilat1_atm)
      real :: netlong_atm(ilon1_atm,ilat1_atm)
      real :: stressx_atm_meansic(ilon1_atm,ilat1_atm)
      real :: surf_stressx_atm(ilon1_atm,ilat1_atm)
      real :: stressy_atm_meansic(ilon1_atm,ilat1_atm)
      real :: surf_stressy_atm(ilon1_atm,ilat1_atm)
      real :: precip_atm_meansic(ilon1_atm,ilat1_atm)
      real :: precip_atm(ilon1_atm,ilat1_atm)
c
c     ** Average the fluxes output fron the atmosphere onto the 
c       seaice/ocean timestep **
c     In fact, stressx, stressy, and precip are not used by
c       the slabseaice.
c
       do i=1,ilon1_atm
        do j=1,ilat1_atm
                    latent_atm_meansic(i,j)=
     :                   latent_atm_meansic(i,j)+
     :                   surf_latent_atm(i,j)*katm_loop/(1.*ksic_loop)
                    sensible_atm_meansic(i,j)=
     :                   sensible_atm_meansic(i,j)+
     :                   surf_sensible_atm(i,j)*katm_loop/(1.*ksic_loop)
                    netsolar_atm_meansic(i,j)=
     :                   netsolar_atm_meansic(i,j)+
     :                   netsolar_atm(i,j)*katm_loop/(1.*ksic_loop)
                    netlong_atm_meansic(i,j)=
     :                   netlong_atm_meansic(i,j)+
     :                   netlong_atm(i,j)*katm_loop/(1.*ksic_loop)
                    stressx_atm_meansic(i,j)=
     :                   stressx_atm_meansic(i,j)+
     :                   surf_stressx_atm(i,j)*katm_loop/(1.*ksic_loop)
                    stressy_atm_meansic(i,j)=
     :                   stressy_atm_meansic(i,j)+
     :                   surf_stressy_atm(i,j)*katm_loop/(1.*ksic_loop)
                    precip_atm_meansic(i,j)=
     :                   precip_atm_meansic(i,j)+
     :                   (precip_atm(i,j))
     :                   *katm_loop/(1.*kocn_loop)
        enddo
       enddo
c
      end
