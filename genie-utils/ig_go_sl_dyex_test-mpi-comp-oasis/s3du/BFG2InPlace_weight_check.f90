       module BFG2InPlace_weight_check
       use BFG2Target3
       use mpi
       private 
       public get,put
       interface get
       module procedure getreal__1
       end interface
       interface put
       end interface
       contains
       subroutine getreal__1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !i3
       integer :: i3
       !i4
       integer :: i4
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(2) :: pp2p=(/0,0/)
       currentModel=getActiveModel()
       if (currentModel==13) then
       ! I am model=weight_check and ep=weightcheck and instance=
       if (arg2==-564) then
       ! I am weight_check.weightcheck..13
       remoteID=-1
       setSize=20
       allocate(set(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=13
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,564,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-565) then
       ! I am weight_check.weightcheck..13
       remoteID=-1
       setSize=20
       allocate(set(1:setSize))
       set=(/5,8,10,12,13,30,31,32,33,34,35,36,37,38,39,40,41,42,46,48/)
       myID=13
       myDU=info(myID)%du
       if (pp2p(2).eq.1) then
       pp2p(2)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       mysize=size(arg1)
       call mpi_recv(arg1,mysize,mpi_real,du,565,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal__1
       end module BFG2InPlace_weight_check
