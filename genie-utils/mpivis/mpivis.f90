      ! IRH: 16/09/08: Changed format specifiers for TAGs from I3 to I4
      ! as tags can be 4-digit with some GENIE builds.
      SUBROUTINE MPI_SEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       USE MPI
       IMPLICIT NONE
       REAL BUF
       INTEGER COUNT,DATATYPE,DEST,TAG,COMM,ERR
       ! My vars
       INTEGER :: UNT,MYRANK
       DOUBLE PRECISION :: T,T1,T2
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get my rank
       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
       WRITE(UNIT=*,FMT="(A11,I2,A4,I2,A5,I4)") ' send from ',MYRANK,' to ',DEST,' tag ',TAG
       T1 = MPI_WTIME()
       CALL PMPI_SEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       T2 = MPI_WTIME()
       T2= T2-T1
       WRITE(UNIT=*,FMT="(A21,I2,A4,I2,A5,I4,A6,F8.6)") ' completed send from ',MYRANK,' to ',DEST,' tag ',TAG,' took ',T2
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A24,I4,A8,I2,A8,I2,A8,F20.6,A3)") '<event type="send" tag="',TAG,'" proc="',MYRANK,'" dest="',DEST,'" time="',T,'"/>'
      END SUBROUTINE MPI_SEND

      SUBROUTINE MPI_BSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       USE MPI
       IMPLICIT NONE
       REAL BUF
       INTEGER COUNT,DATATYPE,DEST,TAG,COMM,ERR
       ! My vars
       INTEGER :: UNT,MYRANK
       DOUBLE PRECISION :: T,T1,T2
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get my rank
       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
       WRITE(UNIT=*,FMT="(A12,I2,A4,I2,A5,I4)") ' bsend from ',MYRANK,' to ',DEST,' tag ',TAG
       T1 = MPI_WTIME()
       CALL PMPI_BSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       T2 = MPI_WTIME()
       T2= T2-T1
       WRITE(UNIT=*,FMT="(A22,I2,A4,I2,A5,I4,A6,F8.6)") ' completed bsend from ',MYRANK,' to ',DEST,' tag ',TAG,' took ',T2
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A,I4,A,I2,A,I2,A,F20.6,A)") &
         '<event type="send" tag="',TAG,'" proc="',MYRANK,'" dest="',DEST,'" time="',T,'"/>'
      END SUBROUTINE MPI_BSEND

      SUBROUTINE MPI_SSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       USE MPI
       IMPLICIT NONE
       REAL BUF
       INTEGER COUNT,DATATYPE,DEST,TAG,COMM,ERR
       ! My vars
       INTEGER :: UNT,MYRANK
       DOUBLE PRECISION :: T,T1,T2
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get my rank
       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
       WRITE(UNIT=*,FMT="(A12,I2,A4,I2,A5,I4)") ' ssend from ',MYRANK,' to ',DEST,' tag ',TAG
       T1 = MPI_WTIME()
       CALL PMPI_BSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,ERR)
       T2 = MPI_WTIME()
       T2= T2-T1
       WRITE(UNIT=*,FMT="(A22,I2,A4,I2,A5,I4,A6,F8.6)") ' completed ssend from ',MYRANK,' to ',DEST,' tag ',TAG,' took ',T2
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A24,I4,A8,I2,A8,I2,A8,F20.6,A3)") '<event type="send" tag="',TAG,'" proc="',MYRANK,'" dest="',DEST,'" time="',T,'"/>'
      END SUBROUTINE MPI_SSEND

      SUBROUTINE MPI_IBSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,STAT,ERR)
       USE MPI
       IMPLICIT NONE
       REAL BUF
       INTEGER COUNT,DATATYPE,DEST,TAG,COMM,ERR
       INTEGER, DIMENSION(MPI_STATUS_SIZE) :: STAT
       ! My vars
       INTEGER :: UNT,MYRANK
       DOUBLE PRECISION :: T,T1,T2
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get my rank
       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
       WRITE(UNIT=*,FMT="(A12,I2,A4,I2,A5,I4)") ' isend from ',MYRANK,' to ',DEST,' tag ',TAG
       T1 = MPI_WTIME()
       CALL PMPI_BSEND(BUF,COUNT,DATATYPE,DEST,TAG,COMM,STAT,ERR)
       T2 = MPI_WTIME()
       T2= T2-T1
       WRITE(UNIT=*,FMT="(A22,I2,A4,I2,A5,I4,A6,F8.6)") ' completed isend from ',MYRANK,' to ',DEST,' tag ',TAG,' took ',T2
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A24,I4,A8,I2,A8,I2,A8,F20.6,A3)") '<event type="send" tag="',TAG,'" proc="',MYRANK,'" dest="',DEST,'" time="',T,'"/>'
      END SUBROUTINE MPI_IBSEND


      SUBROUTINE MPI_RECV(BUF,COUNT,DATATYPE,SRC,TAG,COMM,STAT,ERR)
       USE MPI
       IMPLICIT NONE
       REAL BUF
       INTEGER COUNT,DATATYPE,SRC,TAG,COMM,ERR
       INTEGER, DIMENSION(MPI_STATUS_SIZE) :: STAT
       ! My vars
       INTEGER :: UNT,MYRANK
       DOUBLE PRECISION :: T,T1,T2
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get my rank
       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
       WRITE(UNIT=*,FMT="(A12,I2,A6,I2,A5,I4)") ' receive on ',MYRANK,' from ',SRC,' tag ',TAG
       T1 = MPI_WTIME()
       CALL PMPI_RECV(BUF,COUNT,DATATYPE,SRC,TAG,COMM,STAT,ERR)
       T2 = MPI_WTIME()
       T2= T2-T1
       WRITE(UNIT=*,FMT="(A22,I2,A6,I2,A5,I4,A6,F8.6)") ' completed receive on ',MYRANK,' from ',SRC,' tag ',TAG,' took ',T2
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A,I4,A,I2,A,I2,A,F20.6,A)") &
         '<event type="receive" tag="',TAG,'" proc="',MYRANK,'" src="',SRC,'" time="',T,'"/>'
      END SUBROUTINE MPI_RECV

!      SUBROUTINE MPI_BARRIER(COMM,ERR)
!       USE MPI
!       IMPLICIT NONE
!       INTEGER COMM,ERR,MYRANK,UNT
!       DOUBLE PRECISION T
!       ! Get my rank
!       CALL MPI_COMM_RANK(COMM,MYRANK,ERR)
!       UNT=100+MYRANK
!       T = MPI_WTIME()
!       WRITE(UNIT=UNT,FMT="(A41,I2,A8,F20.6,A3)") '<event type="begin" name="barrier" proc="',MYRANK,'" time="',T,'"/>'
!       CALL PMPI_BARRIER(COMM,ERR)
!       T = MPI_WTIME()
!       WRITE(UNIT=UNT,FMT="(A39,I2,A8,F20.6,A3)") '<event type="end" name="barrier" proc="',MYRANK,'" time="',T,'"/>'
!      END SUBROUTINE MPI_BARRIER

      ! Subroutine outputs an XML tag for computation - marks begin and end times
      SUBROUTINE MPIVIS_COMP(MYTYPE,MYNAME,MYRANK)
       USE MPI
       IMPLICIT NONE
       CHARACTER(LEN=*), INTENT(IN) :: MYTYPE
       CHARACTER(LEN=*), INTENT(IN) :: MYNAME
       INTEGER, INTENT(IN) :: MYRANK
       INTEGER :: IERR,UNT
       DOUBLE PRECISION :: T
       CHARACTER(LEN=20) :: DIRNAME
       CHARACTER(LEN=20) :: DIRNUM
       ! Get the current time
       T = MPI_WTIME()
       ! Calculate which file to write to
       UNT=100+MYRANK
       ! Write the XML element
       WRITE(UNIT=UNT,FMT="(A,A,A,A,A,I2,A,F20.6,A)") &
        '<event type="',MYTYPE,'" name="',MYNAME,'" proc="',MYRANK,'" time="',T,'"/>'
       WRITE(UNIT=*,FMT="(A20,A1,A25)") MYTYPE," ",MYNAME
      END SUBROUTINE MPIVIS_COMP

      SUBROUTINE MPIVIS_INIT(RANK)
       USE MPI
       IMPLICIT NONE
       INTEGER, INTENT(IN) :: RANK
       INTEGER :: UNT
       DOUBLE PRECISION :: T
       CHARACTER DIRNAME*20
       CHARACTER DIRNUM*20
       UNT=100+RANK
       WRITE(UNIT=DIRNUM,FMT='(I3)')UNT
       DIRNAME='output' // DIRNUM 
       OPEN(UNIT=UNT,FILE=DIRNAME)
       T = MPI_WTIME()
       WRITE(UNIT=UNT,FMT="(A,I2,A,F20.6,A)") &
         '<proc id="',RANK,'"><start time="',T,'"/></proc>'
      END SUBROUTINE MPIVIS_INIT

      SUBROUTINE MPIVIS_FINAL(RANK)
       USE MPI
       IMPLICIT NONE
       INTEGER, INTENT(IN) :: RANK
       INTEGER :: UNT
       DOUBLE PRECISION :: T
       UNT=100+RANK
       T = MPI_WTIME()
       WRITE(UNIT=UNT,FMT="(A,I2,A,F20.6,A)") &
         '<proc id="',RANK,'"><end time="',T,'"/></proc>'
       CLOSE(UNT)
      END SUBROUTINE MPIVIS_FINAL
