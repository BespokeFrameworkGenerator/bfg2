#!/bin/bash
trap "exit" 1 2 3 6 15
#DEPLOYMENT="s2du"
#DEPLOYMENT="s2su"
DEPLOYMENT="s3du"
INTERPOLATION="bilinear"
#INTERPOLATION="bilinear bicubic nneighbour2D conservativ2D"
#CONFIGS="ig_fi_sl_dyex-oasis"
CONFIGS="ig_go_sl_dyex_test-oasis"

#GENIE configurations to run
for config in $CONFIGS
do

 echo Config $config

 # deployment choices
 for dep in $DEPLOYMENT
 do
   echo Generating code for $dep

   if [ $config = 'ig_go_sl_dyex_test-oasis' ] ; then

     # Interpolation methods
     # Use -o for oasisvis
     for interp in $INTERPOLATION
     do
       echo Interpolation method $interp
       $HOME/bfg2/bin/runbfg2.sh -k -o -d $config/$dep/$interp -f $HOME/bfg2/examples/genie/inputs/coupled_$config-$interp-$dep.xml
     done

   else
     $HOME/bfg2/bin/runbfg2.sh -k -o -d $config/$dep -f $HOME/bfg2/examples/genie/inputs/coupled_$config-$dep.xml
   fi
 done

done
