#!/bin/bash
trap "exit" 1 2 3 6 15
#CONFIGS="ig_go_sl_dyex_test-mpi-comp-oasis"
DEPLOYMENT="s3du"
CONFIGS="ig_fi_sl_dyex"
#DEPLOYMENT="s1su"
#CONFIGS="ig_fi_sl_dyex ig_sl_sl_noflux ig_go_sl_dyex_test"
#DEPLOYMENT="s1su s2su s2du s3su s3du c2su c2du c3su c3du"

#GENIE configurations to run
for arg1 in $CONFIGS
do

 echo Config $arg1

 # deployment choices
 for dep in $DEPLOYMENT
 do

 echo Generating code for $dep
 # IRH: Added -m for mpivis
 $HOME/bfg2/bin/runbfg2.sh -m -k -d $arg1/$dep -f $HOME/bfg2/examples/genie/inputs/coupled_$arg1-$dep.xml

 done

done
