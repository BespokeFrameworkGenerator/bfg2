import time
from gnuplotModel import gnuplotModel
model=gnuplotModel(xLabel='xlabel',yLabel='yLabel')
time.sleep(6)
model.plotxy([1,2,3,4,5,6],[1,1,2,3,5,8],title='myXYData')
time.sleep(6)
model.ploty([1,1,2,3,5,8],title='myYData')
time.sleep(6)
model.finalise()
