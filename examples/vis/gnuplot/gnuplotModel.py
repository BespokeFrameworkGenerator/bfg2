import Gnuplot
class gnuplotModel(object):
    def __init__(self,xLabel='x axis',yLabel='y axis'):
        print "hello from gnuplotModel:constructor"
        self.g = Gnuplot.Gnuplot()
        #g('set grid')
        self.g.xlabel(xLabel)
        self.g.ylabel(yLabel)
        self.g.plot([0],[0])
    def ploty(self,y,title='data'):
        print "hello from gnuplotModel:ploty"
        i=0
        x=[]
        for yval in y:
            i=i+1
            x[i]=i
        d = Gnuplot.Data(x,y,title=title)
        self.g.plot(d)
    def plotxy(self,x,y,title='data'):
        print "hello from gnuplotModel:plotxy"
        d = Gnuplot.Data(x,y,title=title)
        self.g.plot(d)
    def finalise(self):
        print "hello from gnuplotModel:finalise"
        self.g.reset()

