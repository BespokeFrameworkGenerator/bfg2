module radiationModule
implicit none
private
public :: radiation
contains
!
  subroutine radiation(p,t)
    real,intent(in) :: p
    real,intent(inout) :: t
    print *,"radiation called"
    t=t+p*0.1
  end subroutine radiation
!
end module radiationModule
