module dynamicsModule
implicit none
private
public :: init,dynamics
integer :: count=0
contains
!
  subroutine init(u,v,p,t)
    real,intent(out) :: u,v,p,t
    print *,"dynamicsModule:init called"
    u=0.0
    v=1.0
    p=2.0
    t=3.0
  end subroutine init
!
  subroutine dynamics(u,v,p,t)
    real,intent(inout) :: u,v,p,t
    print *,"dynamicsModule:dynamics called"
    call checkValues(u,v,p,t)
    u=u+1.0
    v=v+1.0
    p=p+1.0
    t=t+1.0
  end subroutine dynamics
!
  subroutine checkValues(u,v,p,t)
    real,intent(inout) :: u,v,p,t
    if (count==0) then
      if (u/=0.0) print *,"Error on iteration ",count," for variable u. Expecting 0.0 but found ",u
      if (v/=1.0) print *,"Error on iteration ",count," for variable v. Expecting 1.0 but found ",u
      if (p/=2.0) print *,"Error on iteration ",count," for variable p. Expecting 2.0 but found ",u
      if (t/=3.0) print *,"Error on iteration ",count," for variable t. Expecting 3.0 but found ",u
      u=0.0
      v=1.0
      p=2.0
      t=3.0
      count=count+1
    end if
  end subroutine checkValues

end module dynamicsModule
