module convectionModule
implicit none
private
public :: convection
integer :: count=0
contains
!
  subroutine convection(u,v,t)
    real, intent(inout) :: u,v
    real, intent(inout) :: t
    print *,"convection called"
    call checkValues(u,v,t)
    t=t+u-0.5*v
  end subroutine convection
!
  subroutine checkValues(u,v,t)
    real,intent(inout) :: u,v,t
    if (count==0) then
      if (u/=1.0) print *,"convection: Error on iteration ",count," for variable u. Expecting 1.0 but found ",u
      if (v/=2.0) print *,"convection: Error on iteration ",count," for variable v. Expecting 2.0 but found ",u
      u=1.0
      v=2.0
      count=count+1
    end if
  end subroutine checkValues

end module convectionModule
