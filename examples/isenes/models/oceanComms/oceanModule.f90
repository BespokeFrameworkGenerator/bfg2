module oceanModule
implicit none
private
public :: ocean
contains
!
  subroutine ocean(u,v,t)
    real,intent(in) :: u,v
    real,intent(inout) :: t
    print *,"ocean called"
    t=t+v-0.5*u
  end subroutine ocean
!
end module oceanModule
