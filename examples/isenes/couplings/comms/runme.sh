#!/bin/sh
BFGROOT=../../../..
#
DIR=build_mpi
rm -rf $DIR
mkdir $DIR
${BFGROOT}/bin/runbfg2.py -d $DIR commsMPI.xml
${BFGROOT}/bin/bfg2makefilegen.py -s -d $DIR commsMPI.xml
#
DIR=build_oasis
rm -rf $DIR
mkdir $DIR
${BFGROOT}/bin/runbfg2.py -d $DIR commsOasis4.xml
${BFGROOT}/bin/bfg2makefilegen.py -s -d $DIR commsOasis4.xml
