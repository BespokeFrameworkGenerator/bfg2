module choice_trans
implicit none
private
public :: choice_in1,choice_in2, choice_out
real :: result
contains
!
  subroutine choice_in1(value)
  real,intent(in) :: value
    result=value
  end subroutine choice_in1
!
  subroutine choice_in2(value)
  real,intent(in) :: value
    result=value
  end subroutine choice_in2
!
  subroutine choice_out(value)
    real,intent(out) :: value
    value=result
  end subroutine choice_out
!
end module choice_trans
