module average2_trans
implicit none
private
public :: average_in, average_out
real :: sum=0
real :: count=0
contains
!
  subroutine average_in(value)
  real,intent(in) :: value
    sum=sum+value
    count=count+1
  end subroutine average_in
!
  subroutine average_out(value)
    real,intent(out) :: value
    value=sum/count
    sum=0.0
    value=0.0
  end subroutine average_out
!
end module average2_trans
