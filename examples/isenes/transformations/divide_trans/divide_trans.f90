module divide_trans
implicit none
private
public :: divide_in, divide_out
real :: total1=0,total2=0,total3=0
real :: divisor=12
contains
!
  subroutine init(value)
    real,intent(in) :: value
    divisor=value
  end subroutine init
!
  subroutine divide_in(value1,value2,value3)
    real,intent(in) :: value1,value2,value3
    total1=total1+value1
    total2=total2+value2
    total3=total3+value3
  end subroutine divide_in
!
  subroutine divide_out(value1,value2,value3)
    real,intent(out) :: value1,value2,value3
    value1=total1/divisor
    value2=total2/divisor
    value3=total3/divisor
  end subroutine divide_out
!
end module divide_trans
