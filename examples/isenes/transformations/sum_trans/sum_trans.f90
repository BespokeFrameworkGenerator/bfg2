module sum_trans
implicit none
private
public :: sum_in, sum_out
real :: sum=0
contains
!
  subroutine sum_in(value)
  real,intent(in) :: value
    sum=sum+value
  end subroutine sum_in
!
  subroutine sum_out(value)
    real,intent(out) :: value
    value=sum
    sum=0.0
  end subroutine sum_out
!
end module sum_trans
