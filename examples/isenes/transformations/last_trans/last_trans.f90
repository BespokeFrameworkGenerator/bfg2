module last_trans
implicit none
private
public :: last_in, last_out
real :: last1,last2,last3
contains
!
  subroutine last_in(value1,value2,value3)
  real,intent(in) :: value1,value2,value3
    last1=value1
    last2=value2
    last3=value3
  end subroutine last_in
!
  subroutine last_out(value1,value2,value3)
    real,intent(out) :: value1,value2,value3
    value1=last1
    value2=last2
    value3=last3
  end subroutine last_out
!
end module last_trans
