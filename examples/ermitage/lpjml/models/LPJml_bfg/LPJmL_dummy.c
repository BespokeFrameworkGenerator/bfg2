#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "bfg.h"
//void bfg_put(void *,int);
//void bfg_get(void *,int);
//void bfg_init();
//void bfg_finalise();
//void bfg_eots();

//#define _FILEIN "../../../data/mnpp.bin"
//#define _FILEOUT "../../../data/mnpp_new.bin"
#define _FILEIN "/home/rupert/proj/ermitage/lpjml/data/mnpp.bin"
#define _FILEOUT "/home/rupert/proj/ermitage/lpjml/data/mnpp_new.bin"
#define _DATAOUTSIZE 59199

FILE *tmpfp=NULL;

void computeTS(short *dataIN,float *dataOUT,int month,int year) {
  char *datafile=_FILEIN;

  printf("    Computing Year %d, month %d\n",year,month);

  if (tmpfp==NULL) {
    tmpfp=(FILE*) fopen64(datafile,"rb");
    if (tmpfp==NULL) {
      fprintf(stderr,"Warning: File open failed on input-file.\n");
      exit(1);
    }
  }
  if (fread(dataOUT,sizeof(float),_DATAOUTSIZE,tmpfp)!=_DATAOUTSIZE) {
      fprintf(stderr,"Error reading data from '%s'.\n",datafile);
      exit(1);
    }

}

main(argc,argv)
int argc;
char **argv;
{  
  FILE *ofp;
  float dataOUT[_DATAOUTSIZE];
  short *dataIN;
  int i,point,month,year,tmp;
  float tempSUM,tempAV;
  int firstYear,lastYear,ncellsIN;

  bfg_init();

  if((ofp= (FILE*) fopen64(_FILEOUT,"wb"))==NULL){
    fprintf(stderr,"Warning: File open failed on output-file.\n");
    exit(1);
  }

  bfg_get(&firstYear,1);
  bfg_get(&lastYear,2);
  bfg_get(&ncellsIN,3);

  printf("Specified years '%d' to '%d'\n",firstYear,lastYear);
  printf("Specified input cells '%d'\n",ncellsIN);
  fflush(stdout);

  // allocate data space
  dataIN = (short *) malloc(sizeof(short)*ncellsIN);

  // timestep over years
  for (year=firstYear;year<=lastYear;year++){

    // timestep over months
    for (month=1;month<=12;month++) {

      printf("Timestep Year %d, month %d\n",year,month);

      // get our input data
      bfg_get(dataIN,4);

      // print out the average temperature
      tempSUM=0.0;
      for (i=0;i<ncellsIN;i++){
        tempSUM+=dataIN[i];
      }
      tempAV=tempSUM/ncellsIN;
      fprintf (stderr,"LPJmL_dummy : average input temperature is %f\n",tempAV);

      // pretend to compute this timestep
      computeTS(dataIN,dataOUT,month,year);

      // print out the average output value
      tempSUM=0.0;
      for (i=0;i<_DATAOUTSIZE;i++){
        tempSUM+=dataOUT[i];
      }
      tempAV=tempSUM/_DATAOUTSIZE;
      fprintf (stderr,"LPJmL_dummy : average output value is %f\n",tempAV);

      // write output to file
      if (fwrite(dataOUT,sizeof(float),_DATAOUTSIZE,ofp)!=_DATAOUTSIZE){
        fprintf(stderr,"Error writing data\n");
        exit(1);
      }

      bfg_eots();
    }
  }

  fclose(tmpfp);
  fclose(ofp);

  bfg_finalise();
}
