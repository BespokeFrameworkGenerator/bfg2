module netcdf_writer
use netcdf
implicit none
integer,save :: count(2),start(2)
private
public :: init,timestep,finalise
contains

  subroutine init(FILE_NAME,firstYear,lastYear,nPoints)

    ! This is the name of the data file we will create.
    character (len=*),intent(in) :: FILE_NAME
    integer,intent(in) :: firstYear,lastYear
    integer :: nMonths
    integer :: ncid
    integer, parameter :: NDIMS = 2
    integer, intent(in) :: nPoints

    ! When we create netCDF files, variables and dimensions, we get back
    ! an ID for each one.
    integer :: ncid, varid, dimids(NDIMS)
    integer :: x_dimid, rec_dimid
  
    nMonths=(lastYear-firstYear+1)*12

    ! Create the netCDF file. The nf90_clobber parameter tells netCDF to
    ! overwrite this file, if it already exists.
    call check( nf90_create(FILE_NAME, NF90_CLOBBER, ncid) )

    ! Define the dimensions. NetCDF will hand back an ID for each. 
    call check( nf90_def_dim(ncid, "x", nPoints, x_dimid) )
    call check( nf90_def_dim(ncid, "time", nMonths, rec_dimid) )

    ! The dimids array is used to pass the IDs of the dimensions of
    ! the variables. Note that in fortran arrays are stored in
    ! column-major format.
    dimids =  (/ x_dimid,rec_dimid /)

    ! Define the variable. The type of the variable in this case is
    ! NF90_INT (4-byte integer).
    call check( nf90_def_var(ncid, "data", NF90_INT, dimids, varid) )

    months=(/"January","February","March","April","May","June","July","August","September","October","November","December"/)

    do year=startYear,endYear
      do month=1,12
        write(cYear,*) year
        cYear=trim(adjustl(cYear))
        date((year-startYear)*12+month)=months(month)//" "//cYear
      end do
    end do

    ! End define mode. This tells netCDF we are done defining metadata.
    call check( nf90_enddef(ncid) )

    count = (/nPoints,1/)
    start = (/1,1/)

  end subroutine init

  subroutine timestep(temp_out)
    integer, intent(in) :: temp_out(:)
    integer :: ncid

    start(2)=start(2)+1

    call check( nf90_put_var(ncid, "data", temp_out, start = start, &
                              count = count) )

  end subroutine timestep

  subroutine finalise()

    ! Close the file.
    call check( nf90_close(ncid) )

  end subroutine finalise

  subroutine check(status)
    integer, intent ( in) :: status
    
    if(status /= nf90_noerr) then 
      print *, trim(nf90_strerror(status))
      stop "Stopped"
    end if
  end subroutine check

end module netcdf_writer



  // allocate data space
  dataIN = (short *) malloc(sizeof(short)*ncellsIN);

  // timestep over years
  for (year=firstYear;year<=lastYear;year++){

    // timestep over months
    for (month=1;month<=12;month++) {

      printf("Timestep Year %d, month %d\n",year,month);

      // get our input data
      bfg_get(dataIN,4);

      // print out the average temperature
      tempSUM=0.0;
      for (i=0;i<ncellsIN;i++){
        tempSUM+=dataIN[i];
      }
      tempAV=tempSUM/ncellsIN;
      printf ("  Average input temperature is %f\n",tempAV);

      // pretend to compute this timestep
      computeTS(dataIN,dataOUT,month,year);

      // print out the average output value
      tempSUM=0.0;
      for (i=0;i<_DATAOUTSIZE;i++){
        tempSUM+=dataOUT[i];
      }
      tempAV=tempSUM/_DATAOUTSIZE;
      printf ("  Average output value is %f\n",tempAV);

      // write output to file
      if (fwrite(dataOUT,sizeof(float),_DATAOUTSIZE,ofp)!=_DATAOUTSIZE){
        fprintf(stderr,"Error writing data\n");
        exit(1);
      }

      bfg_eots();
    }
  }

  fclose(tmpfp);
  fclose(ofp);

  bfg_finalise();
}
