#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _FILEIN "../../data/mnpp.bin"
#define _FILEOUT "../../data/mnpp_new.bin"
#define _DATAOUTSIZE 59199

FILE *tmpfp=NULL;

void readHeader(FILE *ifp, int *firstyear, int *lastyear, int *ncells) {
  typedef struct
  {
    int order;     /* order of data items , either CELLYEAR or YEARCELL */
    int firstyear; /* first year for data */
    int nyear;     /* number of years */
    int firstcell; /* index of first data item */
    int ncell;     /* number of data item per year */
    int nbands;    /* number of data elements per cell */
    /*  Type datatype; */
  } Header;
  Header header;
  char *headername,*buffer;
  int version,file_version;

  // skip data input headers
  headername="LPJCLIMATE";
  version=1;
  buffer=(char *) malloc(sizeof(char)*(strlen(headername)+1));
  if(buffer==NULL) { printf("error 1\n");exit(1);}
  if (fread(buffer,strlen(headername),1,ifp)!=1) { printf("error 2\n");exit(1); }
  buffer[strlen(headername)]='\0';
  if(strcmp(buffer,headername)) {printf("error, expecting %s but got %s\n",headername,buffer);exit(1);}
  printf("File header is %s\n",buffer);
  free(buffer);
  if(fread(&file_version,sizeof(file_version),1,ifp)!=1) {printf("error 3\n");exit(1);}
  if((file_version & 0xff)==0){printf("error 4\n");exit(1);}
  if (file_version!=version) {printf("error 5\n");exit(1);}
  printf("File version is %d\n",file_version);
  if(fread(&header,sizeof(Header),1,ifp)!=1){printf("error 6\n");exit(1);}
  printf("Order is %d\n",header.order);
  printf("Firstyear is %d\n",header.firstyear);
  printf("Nyear is %d\n",header.nyear);
  printf("Firstcell is %d\n",header.firstcell);
  printf("Ncell is %d\n",header.ncell);
  printf("Nbands is %d\n",header.nbands);

  *firstyear=header.firstyear;
  *lastyear=*firstyear+header.nyear-1;
  *ncells=header.ncell;
}

void computeTS(short *dataIN,float *dataOUT,int month,int year) {
  char *datafile=_FILEIN;

  printf("    Computing Year %d, month %d\n",year,month);

  if (tmpfp==NULL) {
    tmpfp=(FILE*) fopen64(datafile,"rb");
    if (tmpfp==NULL) {
      fprintf(stderr,"Warning: File open failed on input-file.\n");
      exit(1);
    }
  }
  if (fread(dataOUT,sizeof(float),_DATAOUTSIZE,tmpfp)!=_DATAOUTSIZE) {
      fprintf(stderr,"Error reading data from '%s'.\n",datafile);
      exit(1);
    }

}

main(argc,argv)
int argc;
char **argv;
{  
  FILE *ifp,*ofp;
  float dataOUT[_DATAOUTSIZE];
  short *dataTmp, **dataIN;
  int i,point,month,year,tmp;
  float tempSUM,tempAV;
  int firstYear,lastYear,ncellsIN;

  if(argc!=2){
    fprintf(stdout,"Usage:\n");
    fprintf(stdout,"lpjml_dummy infilename\n");
    exit(1);
  }

  if((ifp= (FILE*) fopen64(argv[1],"rb"))==NULL){
    fprintf(stderr,"Warning: File open failed on input-file.\n");
    exit(1);
  }

  if((ofp= (FILE*) fopen64(_FILEOUT,"wb"))==NULL){
    fprintf(stderr,"Warning: File open failed on output-file.\n");
    exit(1);
  }

  readHeader(ifp,&firstYear,&lastYear,&ncellsIN);

  // allocate data space
  dataTmp= (short*) malloc(sizeof(short)*12*ncellsIN);
  dataIN = (short **) malloc(sizeof(short *)*12);
  for (i = 0; i < 12; i++) {
    dataIN[i] = (short *) malloc(sizeof(short)*ncellsIN);
  }

  // timestep over years
  for (year=firstYear;year<=lastYear;year++){

    // read in our input data for the year
    if (fread(dataTmp,sizeof(short),12*ncellsIN,ifp)!=12*ncellsIN) {
      fprintf(stderr,"Error reading data from '%s'.\n",argv[i]);
      return 1;
    }

    // rejig input data so its points are contiguous
    for (point=0;point<ncellsIN;point++) {
      for (month=0;month<12;month++) {
        dataIN[month][point]=dataTmp[12*point+month];
      }
    }

    // timestep over months
    for (month=1;month<=12;month++) {

      printf("Timestep Year %d, month %d\n",year,month);

      // print out the average temperature
      tempSUM=0.0;
      for (i=0;i<ncellsIN;i++){
        tempSUM+=dataIN[month-1][i];
      }
      tempAV=tempSUM/ncellsIN;
      printf ("  Average input temperature is %f\n",tempAV);

      // pretend to compute this timestep
      computeTS(dataIN[month-1],dataOUT,month,year);

      // print out the average output value
      tempSUM=0.0;
      for (i=0;i<_DATAOUTSIZE;i++){
        tempSUM+=dataOUT[i];
      }
      tempAV=tempSUM/_DATAOUTSIZE;
      printf ("  Average output value is %f\n",tempAV);

      // write output to file
      if (fwrite(dataOUT,sizeof(float),_DATAOUTSIZE,ofp)!=_DATAOUTSIZE){
        fprintf(stderr,"Error writing data\n");
        exit(1);
      }
    }
  }

  fclose(ifp);
  fclose(tmpfp);
  fclose(ofp);
}
