/* uses LPJmL input format to create bsq-output*/

#include <stdio.h>
#include <stdlib.h>

#undef USE_MPI
static void swap(char *a,char *b)
{
  char h;
  h=*a;
  *a=*b;
  *b=h;
} /* of 'swap' */

short int swapshort(short int x)
{
  swap((char *)&x,(char *)(&x)+1);
  return x;
} /* of 'swapshort' */

int swapint(int x)
{
  swap((char *)&x,(char *)(&x)+3);
  swap((char *)&x+1,(char *)(&x)+2);
  return x;
} /* of 'swapint' */

#define newvec(type,size) (type *)malloc(sizeof(type)*(size))

#define DATA_TYPE short
#define CLIMATE 0
#define LANDUSE 1

#define RESTART_HEADER "LPJRESTART"
#define RESTART_VERSION 11
#define LPJ_CLIMATE_HEADER "LPJCLIMATE"
#define LPJ_CLIMATE_VERSION 1
#define LPJ_LANDUSE_HEADER "LPJLANDUSE"
#define LPJ_LANDUSE_VERSION 1
#define LPJGRID_HEADER "LPJGRID"
#define LPJGRID_VERSION 1
#define LPJDRAIN_HEADER "LPJDRAIN"
#define LPJDRAIN_VERSION 1
#define LPJ_COUNTRY_HEADER "LPJ_COUNTRY"
#define LPJ_COUNTRY_VERSION 1
#define LPJWATERUSE_HEADER "LPJWATERUSE"
#define LPJWATERUSE_VERSION 1
#define LPJNEIGHB_IRRIG_HEADER "LPJNEIGHBIRRIG"
#define LPJNEIGHB_IRRIG_VERSION 1
#define LPJSOIL_HEADER "LPJSOIL"
#define LPJSOIL_VERSION 1
#define LPJ_POPDENS_HEADER "LPJPOPDENS"
#define LPJ_POPDENS_VERSION 1
#define CELLYEAR 1
#define YEARCELL 2

/* Definition of datatypes */

typedef enum {LPJ_BYTE,LPJ_SHORT,LPJ_INT,LPJ_FLOAT,LPJ_DOUBLE} Type;

#define CRU2CLM_VERSION "1.0.002"
#define NYEAR 106  /* default value for number of years */
#define NMONTH 12
#define FIRSTYEAR 1901 /* default value for first year */
#define FIRSTCELL 0
#define NCELL 67420
#define USAGE    "Usage: cru2clm [-h] [-firstyear first] [-lastyear last]\n"\
                 "       [-nyear n] [-firstcell f] [-ncell n] [-nbands nb] [-swap]\n"\
                 "       [-yearcell] crufile clmfile\n"
#ifndef TRUE    /* Check whether TRUE or FALSE are already defined */
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

typedef double Real; /* Default floating point type in LPJ */

typedef int Bool; /* type boolean (TRUE/FALSE) */


typedef struct
{
  int order;     /* order of data items , either CELLYEAR or YEARCELL */
  int firstyear; /* first year for data */
  int nyear;     /* number of years */
  int firstcell; /* index of first data item */
  int ncell;     /* number of data item per year */
  int nbands;    /* number of data elements per cell */
  /*  Type datatype; */
} Header;

Bool freadheader(FILE *file, /* file pointer of binary file */
                 Header *header, /* file header to be read */
                 Bool *swap,     /* set to TRUE if data is in different order */
                 const char *headername,
                 int version
                ) /* returns TRUE on error */
{
  char *buffer;
  int file_version;
  buffer=newvec(char,strlen(headername)+1);
  if(buffer==NULL)
    return TRUE;
  if(fread(buffer,strlen(headername),1,file)!=1)
  {
    free(buffer);
    return TRUE;
  }
  /* set ending '\0' in string */
  buffer[strlen(headername)]='\0';
  if(strcmp(buffer,headername))
  {
    free(buffer);
    return TRUE;
  }
  free(buffer);
  if(fread(&file_version,sizeof(file_version),1,file)!=1)
    return TRUE;
  if((file_version & 0xff)==0)
  {
    /* byte order has to be changed in file */
    *swap=TRUE;
    file_version=swapint(file_version);
  }
  else
    *swap=FALSE;
  if (file_version!=version)
    return TRUE;
  if(fread(header,sizeof(Header),1,file)!=1)
    return TRUE;
  if(*swap)  /* is data in different byte order? */
  {
    /* yes, swap bytes */
    header->order=swapint(header->order);
    header->firstyear=swapint(header->firstyear);
    header->nyear=swapint(header->nyear);
    header->firstcell=swapint(header->firstcell);
    header->ncell=swapint(header->ncell);
    header->nbands=swapint(header->nbands);
  }
  return FALSE;
} /* of 'freadheader' */


#define check_ptr(ptr) if((ptr)==NULL) failu("Error allocating memory\n")
#define check_file(file) if((file)==NULL) failu("Error opening file\n")

void failu(const char* str){
  fprintf(stderr, "%s\n", str);
  exit(-1);
}

void usage(char* progname){
  fprintf(stderr, "Use:\n%s ifile ofile type(0=climate, 1=landuse)\n", progname);
  exit(-1);
}

int main(int argc, char* argv[]){
  int i, m,p,y,offset;
  DATA_TYPE *rbuf, *wbuf, tmp;
  char* ifilename, *ofilename;
  FILE* ifile, *ofile;
  Header header;
  int swap, type;

  /* Parse command line */
  if (argc < 4) usage(argv[0]);
  ifilename = argv[1];
  ofilename = argv[2];
  type = atoi(argv[3]);
  
  /* Open files */
  ifile = fopen64(ifilename, "rb");
  check_file(ifile);
  ofile = fopen64(ofilename, "wb");
  check_file(ofile);

  if (type==CLIMATE)
    freadheader(ifile, &header, &swap, LPJ_CLIMATE_HEADER, LPJ_CLIMATE_VERSION);
  else if (type==LANDUSE)
    freadheader(ifile, &header, &swap, LPJ_LANDUSE_HEADER, LPJ_LANDUSE_VERSION);
  offset = ftello64(ifile);
  
  wbuf=malloc(header.ncell*header.nbands*sizeof(DATA_TYPE));
  check_ptr(wbuf);
  if (header.order==YEARCELL){ /*BIP*/
    printf("Order: YEARCELL\n");
    rbuf=malloc(header.nbands*sizeof(DATA_TYPE));
    check_ptr(wbuf);
    if(swap&&sizeof(DATA_TYPE)==2)
       printf("Swapping byte order\n");
    for (y=0; y<header.nyear;++y){
      for (p=0; p<header.ncell; ++p){
	fseeko64(ifile, offset+p*header.nbands*header.nyear*sizeof(DATA_TYPE)+y*header.nbands*sizeof(DATA_TYPE), SEEK_SET);
	fread(rbuf, header.nbands, sizeof(DATA_TYPE), ifile);
	if (swap){
	  if (sizeof(DATA_TYPE)==2){
	    for(m=0; m<header.nbands;++m){
	      tmp = swapshort(rbuf[m]);
	      rbuf[m]=tmp;
	    }
	  }
	  else
	    fprintf(stderr, "Data needs to be byte swapped; not implemented for data type yet\n");
	}
	for(m=0; m<header.nbands;++m){
	  wbuf[m*header.ncell+p]=rbuf[m];
	}
      }
      fwrite(wbuf, header.ncell*header.nbands,sizeof(DATA_TYPE), ofile);
    }
  }
  else if (header.order==CELLYEAR){/*mixed bip/bsq*/
    printf("Order: CELLYEAR\n");
    if(swap&&sizeof(DATA_TYPE)==2)
       printf("Swapping byte order\n");
    rbuf=malloc(header.ncell*header.nbands*sizeof(DATA_TYPE));
    while(fread(rbuf, header.ncell*header.nbands, sizeof(DATA_TYPE), ifile)){
      if (swap){
	  if (sizeof(DATA_TYPE)==2){
	    for(m=0; m<header.nbands*header.ncell;++m){
	      tmp = swapshort(rbuf[m]);
	      rbuf[m]=tmp;
	    }
	  }
	  else
	    fprintf(stderr, "Data needs to be byte swapped; not implemented for data type yet\n");
	}
      for (m=0; m<header.nbands; ++m)
	for(p=0; p<header.ncell; ++p){
	  wbuf[m*header.ncell+p]=rbuf[p*header.nbands+m];
	}
      fwrite(wbuf, header.ncell*header.nbands,sizeof(DATA_TYPE),ofile);
    }     
  }
  free(rbuf);
  fclose(ifile);
  fclose(ofile);
  return 0;
}
