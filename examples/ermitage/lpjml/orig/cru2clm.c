/***************************************************************************/
/**                                                                       **/
/**                      c  r  u  2  c  l  m  .  c                        **/
/**                                                                       **/
/**     Converts CRU data files into LPJ climate data files               **/
/**                                                                       **/
/**     Potsdam Institute for Climate Impact Research                     **/
/**     PO Box 60 12 03                                                   **/
/**     14412 Potsdam/Germany                                             **/
/**                                                                       **/
/***************************************************************************/

#undef USE_MPI /* no MPI */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
/* Definition of constants */

#define RESTART_HEADER "LPJRESTART"
#define RESTART_VERSION 11
#define LPJ_CLIMATE_HEADER "LPJCLIMATE"
#define LPJ_CLIMATE_VERSION 1
#define LPJ_LANDUSE_HEADER "LPJLANDUSE"
#define LPJ_LANDUSE_VERSION 1
#define LPJGRID_HEADER "LPJGRID"
#define LPJGRID_VERSION 1
#define LPJDRAIN_HEADER "LPJDRAIN"
#define LPJDRAIN_VERSION 1
#define LPJ_COUNTRY_HEADER "LPJ_COUNTRY"
#define LPJ_COUNTRY_VERSION 1
#define LPJWATERUSE_HEADER "LPJWATERUSE"
#define LPJWATERUSE_VERSION 1
#define LPJNEIGHB_IRRIG_HEADER "LPJNEIGHBIRRIG"
#define LPJNEIGHB_IRRIG_VERSION 1
#define LPJSOIL_HEADER "LPJSOIL"
#define LPJSOIL_VERSION 1
#define LPJ_POPDENS_HEADER "LPJPOPDENS"
#define LPJ_POPDENS_VERSION 1
#define CELLYEAR 1
#define YEARCELL 2

static void swap(char *a,char *b)
{
  char h;
  h=*a;
  *a=*b;
  *b=h;
} /* of 'swap' */

short int swapshort(short int x)
{
  swap((char *)&x,(char *)(&x)+1);
  return x;
} /* of 'swapshort' */

/* Definition of datatypes */
typedef enum {LPJ_BYTE,LPJ_SHORT,LPJ_INT,LPJ_FLOAT,LPJ_DOUBLE} Type;

#define CRU2CLM_VERSION "1.0.002"
#define NYEAR 106  /* default value for number of years */
#define NMONTH 12
#define FIRSTYEAR 1901 /* default value for first year */
#define FIRSTCELL 0
#define NCELL 67420
#define USAGE    "Usage: cru2clm [-h] [-firstyear first] [-lastyear last]\n"\
                 "       [-nyear n] [-firstcell f] [-ncell n] [-nbands nb] [-swap]\n"\
                 "       [-yearcell] crufile clmfile\n"
#ifndef TRUE    /* Check whether TRUE or FALSE are already defined */
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

typedef short Data[NMONTH];
typedef double Real; /* Default floating point type in LPJ */

typedef int Bool; /* type boolean (TRUE/FALSE) */


typedef struct
{
  int order;     /* order of data items , either CELLYEAR or YEARCELL */
  int firstyear; /* first year for data */
  int nyear;     /* number of years */
  int firstcell; /* index of first data item */
  int ncell;     /* number of data item per year */
  int nbands;    /* number of data elements per cell */
  /*  Type datatype; */
} Header;

Bool fwriteheader(FILE *file,            /* file pointer of binary file */
                  Header header,         /* file header */
                  const char *headername,
                  int version
                 )                       /* returns TRUE on error */
{
  if(fwrite(headername,strlen(headername),1,file)!=1)
    return TRUE;
  if(fwrite(&version,sizeof(version),1,file)!=1)
    return TRUE;
  if(fwrite(&header,sizeof(Header),1,file)!=1)
    return TRUE;
  return FALSE;
} /* of 'fwriteheader' */


int main(int argc,char **argv)
{
  FILE *file;
  Data *data;
  Header header;
  long int k;
  int i,j,n;
  Bool revorder;
  struct stat filestat;
  header.nyear=NYEAR;
  header.firstyear=FIRSTYEAR;
  header.order=CELLYEAR;
  header.firstcell=FIRSTCELL;
  header.ncell=NCELL;
  header.nbands=NMONTH;
  revorder=FALSE;
  for(i=1;i<argc;i++)
    if(argv[i][0]=='-')
    {
      if(!strcmp(argv[i],"-h"))
      {
        printf("cru2clm " CRU2CLM_VERSION " (" __DATE__ ") - convert cru data files to\n"
               "       clm data files for lpj C version\n");
        printf(USAGE
               "Arguments:\n"
               "-h               print this help text\n" 
               "-firstyear first first year in cru file (default is %d)\n"
               "-lastyear last   last year in cru file\n"
               "-nyear n         number of years in cru file (default is %d)\n"
               "-firstcell       first cell in data file (default %d)\n"
               "-ncell           number of cells in data file (default is %d)\n"
               "-nbands          number of bands in data/output file (default is %d)\n"
               "-swap            change byte order in cru file\n"
               "-yearcell        does not revert order in cru file\n"
               "crufile          filename of cru data file\n"
               "clmfile          filename of clm data file\n",
               FIRSTYEAR,NYEAR,FIRSTCELL,NCELL,NMONTH);
        return EXIT_SUCCESS;
      }
      else if(!strcmp(argv[i],"-nyear"))
        header.nyear=atoi(argv[++i]);
      else if(!strcmp(argv[i],"-firstyear"))
        header.firstyear=atoi(argv[++i]);
      else if(!strcmp(argv[i],"-lastyear"))
        header.nyear=atoi(argv[++i])-header.firstyear+1;
      else if(!strcmp(argv[i],"-firstcell"))
        header.firstcell=atoi(argv[++i]);
      else if(!strcmp(argv[i],"-ncell"))
        header.ncell=atoi(argv[++i]);
      else if(!strcmp(argv[i],"-nbands"))
        header.nbands=atoi(argv[++i]);
      else if(!strcmp(argv[i],"-rev"))
        revorder=TRUE;
      else if(!strcmp(argv[i],"-yearcell"))
        header.order=YEARCELL;
      else
      {
        fprintf(stderr,"Error: invalid option '%s'.\n",argv[i]);
        fprintf(stderr,USAGE);
        return EXIT_FAILURE;
      }
    }
    else 
      break;
  if(argc<i+2)
  {
    fprintf(stderr,"Filenames missing.\n");
    fprintf(stderr,USAGE);
    return EXIT_FAILURE;
  }
  file=fopen(argv[i],"rb");
  if(file==NULL)
  {
    fprintf(stderr,"Error opening '%s': %s\n",argv[i],strerror(errno));
    return EXIT_FAILURE;
  }
  fstat(fileno(file),&filestat);
  n=filestat.st_size/header.nyear/sizeof(Data);
  printf("Number of cells: %d\n",n);
  data=(Data *)malloc(n*sizeof(Data)*header.nyear);
  if(data==NULL)
  {
    fprintf(stderr,"Error allocating memory.\n");
    return EXIT_FAILURE;
  }
  if(fread(data,sizeof(Data),n*header.nyear,file)!=n*header.nyear)
  {
    fprintf(stderr,"Error reading data from '%s'.\n",argv[i]);
    return EXIT_FAILURE;
  }
  if(revorder)
    for(k=0;k<n*header.nyear;k++)
      for(j=0;j<NMONTH;j++)
        data[k][j]=swapshort(data[k][j]); 
  fclose(file);
  file=fopen(argv[i+1],"wb");
  if(file==NULL)
  {
    fprintf(stderr,"Error creating '%s': %s\n",argv[i+1],strerror(errno));
    return EXIT_FAILURE;
  }
  header.ncell=n;
  fwriteheader(file,header,LPJ_CLIMATE_HEADER,LPJ_CLIMATE_VERSION);
  if(header.order==CELLYEAR)
  {
    for(i=0;i<header.nyear;i++)
      for(j=0;j<n;j++)
        fwrite(data+j*header.nyear+i,sizeof(Data),1,file);
  }
  else
    fwrite(data,sizeof(Data),header.nyear*n,file);
  free(data);
  fclose(file);
  return EXIT_SUCCESS;
} /* of 'main' */
