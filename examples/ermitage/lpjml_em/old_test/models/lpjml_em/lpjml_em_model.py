import subprocess
import os
import sys
#from Scientific.IO.NetCDF import NetCDFFile
#from scipy.io import netcdf

class lpjml_em_model(object):

    def __init__(self,lat,lon,month):
	self.lat=lat
	self.lon=lon
	self.month=month
        print "python:lpjml_em_model:init"

    def run(self,cloud,precipitation,temperature,wet_days):
        print "python:lpjml_em_model:run"

        print "RETURN"
	return
        # first write data to appropriate input files and formats
        self.netcdf_write(cloud,"cld","CLD_MONTHLY")
        self.netcdf_write(precipitation,"pre.nc","precip")
        self.netcdf_write(temperature,"tmp.nc","temp")
        self.netcdf_write(wet_days,"wet.nc","wet_days")

        # now run the emulator
        print "python:running lpjml emulator"
        infile = open("LPJML_NPP_code_v1.R", 'r')
        outfile = open("out.txt", 'w')
        args = ["R", "--no-restore", "--no-save"]
        proc = subprocess.Popen(args, stdin=infile, stdout=outfile, env=os.environ, close_fds=True)
        #proc = subprocess.Popen(args, stdin=infile, env=os.environ, close_fds=True)
        proc.wait()
        if proc.returncode:
            print("python Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
            sys.exit(proc.returncode)

    def netcdf_write(self,data,fileName,dataName):

        #from Scientific.IO.NetCDF import NetCDFFile
        from scipy.io import netcdf

        # note: we overwrite any existing file
        assert len(data.shape)==3,"Error, cloud expected to be 3D"
        nlat=data.shape[0]
        nlon=data.shape[1]
        nmonth=data.shape[2]
        print "python:netcdf_write:data "+dataName+" nlat="+str(nlat)+" nlon="+str(nlon)+" nmonth="+str(nmonth)

        file = netcdf.netcdf_file(fileName, 'w')
        #file = NetCDFFile(fileName, 'w')

        file.createDimension('LATITUDE',nlat)
        file.createDimension('LONGITUDE',nlon)
        file.createDimension('MONTH',nmonth)

	assert len(self.lat)==nlat,"Error, lat array is not the same size as nlat"
        dims = 'LATITUDE',
        dataVar = file.createVariable('LATITUDE', 'd', dims)
	#dataVar.assignValue(self.lat)
	dataVar[:]=self.lat

	assert len(self.lon)==nlon,"Error, lon array is not the same size as nlon"
        dims = 'LONGITUDE',
        dataVar = file.createVariable('LONGITUDE', 'd', dims)
	#dataVar.assignValue(self.lon)
	dataVar[:]=self.lon

	assert len(self.month)==nmonth,"Error, month array is not the same size as nmonth"
        dims = 'MONTH',
        dataVar = file.createVariable('MONTH', 'd', dims)
	#dataVar.assignValue(self.month)
	dataVar[:]=self.month

        dims = ('LATITUDE','LONGITUDE','MONTH')
        dataVar = file.createVariable(dataName, 'd', dims)
        #dataVar.assignValue(data)
	dataVar[:]=data
	dataVar.units='percent'

        file.close()
