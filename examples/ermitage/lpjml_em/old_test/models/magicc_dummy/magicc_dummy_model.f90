module magicc_dummy_model
!
implicit none
private
public :: init,run
integer,parameter :: nlong=10,nlat=20,nmonth=30
!
contains
!
  subroutine init(long,lat,month)
    real*8,intent(out) :: long(nlong),lat(nlat),month(nmonth)
    integer :: i

    do i=1,nlong
      long(i)=-180.25+0.5*i
    end do

    do i=1,nlat
      lat(i)=-56.25+0.5*i
    end do

    do i=1,nmonth
      month(i)=2001.0+(i-1)*100.0/nmonth
    end do

    print *,"HELLO FROM MAGICC INIT"
  end subroutine init
!
  subroutine run(cloud,precipitation,temperature,wet_days)
    real*8,intent(out) :: cloud(nlat,nlong,nmonth),precipitation(nlat,nlong,nmonth)
    real*8,intent(out) :: temperature(nlat,nlong,nmonth),wet_days(nlat,nlong,nmonth)

!    cloud=1.0
!    precipitation=2.0
!    temperature=3.0
!    wet_days=4.0

    print *,"HELLO FROM MAGICC RUN"
  end subroutine run
!
end module magicc_dummy_model

