#import Scientific.IO.NetCDF
#from Numeric import *
#from NetCDF import *

class lpjml_em_model:

    def __init__(self):
        # nothing needed in init
        print "lpjml_em_model:init"

    def run(self,cloud,precipitation,temperature,wet_days):
        print "lpjml_em_model:run"

        # first write data to appropriate input files and formats
        #self.netcdf_write(cloud,"cld.nc","CLD")
        #self.netcdf_write(precipitation,"pre.nc")
        #self.netcdf_write(temperature,"tmp.nc")
        #self.netcdf_write(wet_days,"wet.nc")

        # now run the emulator
        #infile = open("LPJML_NPP_code_v1.R", 'r')
        #outfile = open("out.txt", 'w')
        #args = ["R", "--no-restore", "--no-save"]
        #proc = subprocess.Popen(args, stdin=infile, stdout=outfile, env=os.environ, close_fds=True)
        #proc.wait()
        #if proc.returncode:
        #    print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
        #    sys.exit(proc.returncode)

    #def netcdf_write(self,data,fileName,dataName):
        # note: we overwrite any existing file
        #file = NetCDFFile(fileName, 'w')
        #file.createDimension('LATITUDE',lat)
        #file.createDimension('LONGITUDE',lon)
        #file.createDimension('MONTH',month)
        #dims = ('LATITUDE','LONGITUDE','MONTH')
        #dataVar = file.createVariable(dataName, 'f', dims)
        #dataVar.assignValue(data)
        #file.close()
