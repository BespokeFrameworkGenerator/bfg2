module magicc_dummy_model
!
implicit none
private
public :: run
integer,parameter :: size=10
!
contains
!
  subroutine run(cloud,precipitation,temperature,wet_days)
    real*8,intent(out) :: cloud(size,size,size),precipitation(size,size,size)
    real*8,intent(out) :: temperature(size,size,size),wet_days(size,size,size)
    cloud=1.0
    precipitation=2.0
    temperature=3.0
    wet_days=4.0
  end subroutine run
!
end module magicc_dummy_model

