from numpy import zeros,matrix, array

class interp_trans(object):
    def __init__(self):
        print "interp_trans:constructor"

    def init(self, fromTimeSeries, toTimeSeries):
        self.weights = zeros((len(toTimeSeries), len(fromTimeSeries)))
        # create weights
        hiIndex = 0
        print("len(fromTimeSeries): {0}".format(len(fromTimeSeries)) )
        for (toIndex, time) in enumerate(toTimeSeries):
            while (hiIndex < (len(fromTimeSeries)-1) and fromTimeSeries[hiIndex] < time):
                hiIndex += 1
            if fromTimeSeries[hiIndex] <= time:
                loIndex = hiIndex
            else:
                loIndex = hiIndex-1
            print("time, fromTimeSeries[loIndex], fromTimeSeries[hiIndex] : {0} {1} {2}".format(time, fromTimeSeries[loIndex], fromTimeSeries[hiIndex]) )
            if loIndex == hiIndex:
                self.weights[toIndex, hiIndex] = 1.0
            else:
                fromHiData = float(fromTimeSeries[hiIndex])
                fromLoData = float(fromTimeSeries[loIndex])
                fromRange = fromHiData - fromLoData
                loWeight = (fromHiData - time)/fromRange
                hiWeight = (time - fromLoData)/fromRange
                self.weights[toIndex, hiIndex] = hiWeight
                self.weights[toIndex, loIndex] = loWeight
        # ** NEED TO SORT OUT WEIGHTS AT EDGES (when toData is beyond fromData)

    def run(self, fromTimeSeries, toTimeSeries, dataIn):
        dataInM = matrix(dataIn).reshape(len(dataIn),1)
        self.init(fromTimeSeries, toTimeSeries)
        print("[interp_trans] dataInM: {0} self.weights: {1}".format(dataInM, self.weights) )
        dataOut = self.weights*dataInM
        print("[interp_trans] dataOut: {0} len: {1} type: {2}".format(dataOut, len(dataOut), type(dataOut) ) )
#        raw_input("Waiting...")

        newout = []
        for x in dataOut:
            newout.append(float(x[0]))


        print("[in interp_trans newout] {0}".format(newout) )
        print("[in interp_trans newout] len: {0} type: {1} element_type: {2}".format(len(newout), type(newout), type(newout[0])) )

        return array(newout)

#        return array(dataOut)
