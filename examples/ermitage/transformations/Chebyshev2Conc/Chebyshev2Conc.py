class Chebyshev2Conc:

  def __init__(self,CO2_base_IN=393.0,startYear=2005,endYear=2105,stepSize=1):

    self.CO2_base=CO2_base_IN

    self.nsteps=((endYear-startYear)/stepSize)+1

    self.normStart=-1.0
    self.normEnd=1.0
    self.normStepSize=(self.normEnd-self.normStart)*stepSize/(endYear-startYear)

    self.ChebPoly1Min=0
    self.ChebPoly1Max=1000
    self.ChebPoly2Min=-200
    self.ChebPoly2Max=200
    self.ChebPoly3Min=-100
    self.ChebPoly3Max=100

  def run1(self,ChebPoly1,ChebPoly2,ChebPoly3):

    from numpy import zeros,linspace

    assert ChebPoly1>=self.ChebPoly1Min and ChebPoly1<=self.ChebPoly1Max, "Error, ChebPoly1 is out of range"
    assert ChebPoly2>=self.ChebPoly2Min and ChebPoly2<=self.ChebPoly2Max, "Error, ChebPoly2 is out of range"
    assert ChebPoly3>=self.ChebPoly3Min and ChebPoly3<=self.ChebPoly3Max, "Error, ChebPoly3 is out of range"

    CO2=zeros(self.nsteps)

    index=0
    for t in linspace(self.normStart,self.normEnd,num=self.nsteps,endpoint=True):
      CO2[index]=self.CO2_base+0.5*(ChebPoly1*(t+1)+ChebPoly2*(2*t*t-2)+ChebPoly3*(4*t*t*t-4*t))
      index+=1

    return CO2

  def run2(self,ChebPoly1,ChebPoly2,ChebPoly3,ChebPoly4,ChebPoly5,ChebPoly6):

    RadForcing=self.run1(ChebPoly1,ChebPoly2,ChebPoly3)
    CO2=self.run1(ChebPoly4,ChebPoly5,ChebPoly6)

    return RadForcing,CO2

