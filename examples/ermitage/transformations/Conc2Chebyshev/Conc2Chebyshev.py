from numpy import zeros
class Conc2Chebyshev:

    def __init__(self):
        pass

    def run3(self,time,concent_co2):
        coeffs=self.run(time,concent_co2)
        return coeffs[0],coeffs[1],coeffs[2]

    def run(self,time,concent_co2):

        # check consistency of data in arguments
        n=len(time)
        assert n==len(concent_co2)
 
        # convert from time-series of concentrations to Chebyshev Coefficients
        poly1 = zeros(n)
        poly2 = zeros(n)
        poly3 = zeros(n)
        a11 = 0
        a22 = 0
        a33 = 0
        a12 = 0
        a13 = 0
        a23 = 0
        b = zeros(3)
        for i in xrange(n):
            poly1[i] = time[i] + 1
            poly2[i] = 2 * time[i]**2 - 2
            poly3[i] = 4 * time[i]**3 - 4 * time[i]
            a11 = a11 + poly1[i]**2
            a22 = a22 + poly2[i]**2
            a33 = a33 + poly3[i]**2
            a12 = a12 + poly1[i]*poly2[i]
            a13 = a13 + poly1[i]*poly3[i]
            a23 = a23 + poly2[i]*poly3[i]
            b[0] = b[0] + poly1[i] * (concent_co2[i] - concent_co2[0])
            b[1] = b[1] + poly2[i] * (concent_co2[i] - concent_co2[0])
            b[2] = b[2] + poly3[i] * (concent_co2[i] - concent_co2[0])

        e11 = (poly1[n-1]**2)/2
        e21 = (poly2[n-1]*poly1[n-1])/2
        e31 = (poly3[n-1]*poly1[n-1])/2

        if e21 == 0:
            e21 = 1e-6
        if e31 == 0:
            e31 = 1e-6

        e12 = poly1[n-1]*a12 - poly2[n-1]*a11
        e13 = poly1[n-1]*a13 - poly3[n-1]*a11
        f1 = poly1[n-1]*b[0] - (concent_co2[n-1] - concent_co2[0]) * a11

        e22 = poly1[n-1]*a22 - poly2[n-1]*a12
        e23 = poly1[n-1]*a23- poly3[n-1]*a12
        f2 = poly1[n-1]*b[1] - (concent_co2[n-1] - concent_co2[0]) * a12

        e32 = poly1[n-1]*a23 - poly2[n-1]*a13
        e33 = poly1[n-1]*a33- poly3[n-1]*a13
        f3 =  poly1[n-1]*b[2] - (concent_co2[n-1] - concent_co2[0]) * a13

        c11 = e12*e21 - e11*e22
        c12 = e13*e21 - e11*e23
        c21 = e12*e31 - e11*e32
        c22 = e13*e31 - e33*e11
        d1 = f1*e21 - f2*e11
        d2 = f1*e31 - f3*e11

        tcheb = zeros(3)
        tcheb[2] = (d1*c21 - d2*c11)/(c12*c21 - c11*c22)
        tcheb[1] = (d2 - c22 * tcheb[2]) / c21 
        delta = (f1 - e12 * tcheb[1] - e13 * tcheb[2]) / e11  
        tcheb[0] = ((concent_co2[n-1] - concent_co2[0]) - tcheb[1] * poly2[n-1] - tcheb[2] * poly3[n-1]) / poly1[n-1]

        tcheb[0] = 2*tcheb[0]
        tcheb[1] = 2*tcheb[1]
        tcheb[2] = 2*tcheb[2]

        return tcheb
