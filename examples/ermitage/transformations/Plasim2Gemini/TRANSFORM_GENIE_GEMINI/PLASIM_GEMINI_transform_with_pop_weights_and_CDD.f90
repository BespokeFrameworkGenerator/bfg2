      implicit none
      
      character input_file*100
      character header
      character region(30)*30

      real input(4,2048,10)          !INPUT DATA SEASONAL ON GENIE GRID
      real output(4,30,10)           !OUTPUT DATA, SEASONAL ON GEMINI REGIONS

      real pop_weight_GENIE(2048) !INPUTTED POPULATION FIELD ON GENIE GRID
      real pop_weight_GEMINI(30)    !POPULATION OF GEMINI REGIONS (CALCULATED)
      integer transform(2048)     !GEMINI REGIONS ON GENIE GRID

      integer ig,it,season,decade,itmax
      real dummy

! INPUT (SAT, CDD, HDD)
     
      input_file='../TRANSFORM_temperature_DD/CDD.out'
!      input_file='../TRANSFORM_temperature_DD/HDD.out'
!      input_file='../TRANSFORM_temperature_maxminav/AVERAGE_SAT.out'
!      input_file='../TRANSFORM_temperature_maxminav/MAX_SAT.out'

      open(unit=11,file=trim(input_file))
      read(11,*) header
      do decade=1,10
        do ig=1,2048
          read(11,*) dummy,dummy,(input(season,ig,decade),season=1,4)
        enddo
      enddo
      close(11)

!READ IN TRANSORMATION ARRAY. THIS IS THE DATA PROVIDED IN SANTOSH'S SPREADSHEET
!DATA IN CELLS C29 to C2076

      open(11,file='transform_GENIE_GEMINI.dat')
      do ig=1,2048
        read(11,*) transform(ig)
      enddo
! HOW MANY GEMINI REGIONS ARE THERE?
      itmax=maxval(transform)
      do it=1,itmax
        read(11,*) region(it)
      enddo
      close(11)

! POPULATION ON THE GENIE GRID
      open(11,file='../POPULATION/population.dat')
      do ig=1,2048
        read(11,*) pop_weight_GENIE(ig)
      enddo
      close(11)
      print*,sum(pop_weight_GENIE)

! TOTAL POPULATIONS/WEIGHTINGS FOR TIAM REGIONS
      pop_weight_GEMINI=0.0
      do ig=1,2048
        do it=1,itmax
          if(transform(ig).eq.it) then
            pop_weight_GEMINI(it)=pop_weight_GEMINI(it)+pop_weight_GENIE(ig)
          endif
        enddo
      enddo
      print*,sum(pop_weight_GEMINI)
 
!DO THE TRANSFORMATION
      output=0.0
      do decade=1,10
      do ig=1,2048
        do it=1,itmax
          if(transform(ig).eq.it) then
            do season=1,4
              output(season,it,decade)=output(season,it,decade)+&
                     input(season,ig,decade)*pop_weight_GENIE(ig)
            enddo
          endif
        enddo
      enddo
      enddo
      do it=1,itmax
        output(:,it,:)=output(:,it,:)/pop_weight_GEMINI(it)
        print*,pop_weight_GEMINI(it)
      enddo

! OUTPUT DATA ACCORDING TO GEMINI REGIONS
 122  format(5a9,a11)
 123  format(i9,4f9.3,1pe11.3,a30)
      open(unit=12,file='GEMINI_regions_CDD_with_pop.out')
      do decade=1,10
        write(12,122) 'region','DJF','MAM','JJA','SON','pop/weight'
        do it=1,itmax
          write(12,123) it,(output(season,it,decade),season=1,4),pop_weight_GEMINI(it),trim(region(it))
        enddo
      enddo
      close(12)

! SOME OUTPUT FOR TESTING PLOTS
      open(unit=15,file='test_GENIE.dat')
      open(unit=16,file='test_GEMINI.dat')
      do ig=1,2048
        write(15,*) input(3,ig,10)
!        write(16,*) output(3,transform(ig),10)
        write(16,*) pop_weight_GEMINI(transform(ig))
      enddo
      close(15)
      close(16)

      end

