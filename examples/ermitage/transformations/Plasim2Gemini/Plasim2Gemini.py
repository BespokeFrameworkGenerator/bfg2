import numpy
import subprocess
import os
import csv
from numpy import zeros
import sys

class Plasim2Gemini:

    def __init__(self):
	#print "Plasim2Gemini constructor\n"
	pass

    def run(self,temperature, temperature_variability):

	#temperature=temperature.transpose().reshape(20480,4)
	#temperature_variability=temperature_variability.transpose().reshape(20480,4)
	#print "Plasim2Gemini.run()"
        #print temperature.shape
        #print "temperature",temperature
        #print "temperature_variability",temperature_variability
        # write our temperature and temperature_variability data in the format expected
        self._createInputs(temperature, temperature_variability)
        self._runScript()
        return self._extractResults()

    def _extractResults(self):

        HDD=self._readData(os.path.join("emulator_output","GEMINI_regions_HDD_with_pop.out"))
        CDD=self._readData(os.path.join("emulator_output","GEMINI_regions_CDD_with_pop.out"))
        return HDD,CDD

    def _readData(self,fileName):

        reader = csv.reader(open(fileName,"rb"))  
        list_reader = []
        for row in reader:
            list_reader.append(row[0])
        n = len(list_reader)-20
        DD= zeros(n)
        for i in xrange(10):
            for j in xrange(14):
                line = list_reader[i*16+j+2].split()
                DD[i*14 + j] = float(line[1]) + float(line[2]) + float(line[3]) + float(line[4])
        return DD

    def _runScript(self):

        outfile = open("out.txt", 'w')

        try:
            # try to run the unix shell script. Windows will generate an exception as this does
            # not exist (the file is generate_CDD-HDD.sh)
            args_unix = ["generate_CDD-HDD"]
            proc_unix = subprocess.Popen(args_unix, stdout=outfile, env=os.environ)
            proc_unix.wait()
            # check for success
            if proc_unix.returncode:
                # failed in the script so abort
                print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args_unix), __name__, proc_unix.returncode))
                sys.exit(1)
        except OSError:
            try:
                # failed, so try to run the windows script
                args_win = ["generate_CDD-HDD.bat"]
                proc_win = subprocess.Popen(args_win, stdout=outfile, env=os.environ)
                proc_win.wait()
                # check for success
                if proc_win.returncode:
                    # failed in the script so abort
                    print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args_win), __name__, proc_win.returncode))
                    sys.exit(1)
            except OSError:
                print "failed to run/find either a unix or windows script in Plasim2Gemini:_runScript"
                sys.exit(1)
            except:
                print "Unknown exception in Plasim2Gemini:_runScript after trying linux and windows: ", sys.exc_info()[0]
                sys.exit(1)
        except:
            print "Unknown exception in Plasim2Gemini:_runScript after trying linux: ", sys.exc_info()[0]
            sys.exit(1)
                


    def _createInputs(self,temperature,temperature_variability):
        self._writeData(temperature,"emulator_output","_temperature.out")
        self._writeData(temperature_variability,"emulator_output","_temperature_variability.out")

    def _writeData(self,data,fileNameStart,fileNameEnd):

        options=["DJF","MAM","JJA","SON"]
        for index,name in enumerate(options):
            fileName=os.path.join(fileNameStart,name+fileNameEnd)
            dataSlice=data[:,index]
            numpy.savetxt(open(fileName,"w"),dataSlice,delimiter=" ")
