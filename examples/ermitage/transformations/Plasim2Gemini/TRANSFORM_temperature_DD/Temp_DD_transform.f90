
      implicit none

      character path*100
      character season*3
      character satdata*100
      character satsddata*100
      integer i,s,y,k

      real SAT(10,2048,4),SATSD(10,2048,4)
      real CDD(10,2048,4),HDD(10,2048,4)
      real temp(2048)
      real DD_ref(2048)

      path='../emulator_output/'
      DD_ref=18.0
      CDD=0.0
      HDD=0.0

      do s=1,4

!WHICH SEASON?
        if(s.eq.1) then
          season='DJF'
        else if (s.eq.2) then
          season='MAM'
        else if (s.eq.3) then
          season='JJA'
        else 
          season='SON'
        endif
        write(satdata,'(a,a,a)') trim(path),trim(season),'_temperature.out'
        write(satsddata,'(a,a,a)') trim(path),trim(season),'_temperature_variability.out'

        open(unit=12,file=satdata)
        do y=1,10
          do i=1,2048 
            read(12,*) sat(y,i,s)
          enddo
        enddo
        close(12)
        open(unit=12,file=satsddata)
        do y=1,10
          do i=1,2048 
            read(12,*) satsd(y,i,s)
          enddo
        enddo
        close(12)
 
! CDD_ref can be defined spatially, but here globally constant
        do y=1,10
          do k=-100,100
            temp=real(k)
! HEATING DD
            where(temp<DD_ref)
             HDD(y,:,s)=HDD(y,:,s)+(DD_ref-temp)*int(0.5+                                     &
               91.3125/(SATSD(y,:,s)*2.506628)*                       &
               exp(-(temp-SAT(y,:,s))*(temp-SAT(y,:,s))/ &
               (2.0*SATSD(y,:,s)**2)))
            endwhere
! COOLING DD
            where(temp>DD_ref) 
              CDD(y,:,s)=CDD(y,:,s)+(temp-DD_ref)*int(0.5+                                     &
                91.3125/(SATSD(y,:,s)*2.506628)*                       &
                exp(-(temp-SAT(y,:,s))*(temp-SAT(y,:,s))/ &
                (2.0*SATSD(y,:,s)**2)))
            endwhere
          enddo
        enddo
! END SEASON LOOP
      enddo 
      
      open(unit=11,file='CDD.out')
      write(11,112) 'lon','lat','DJF','MAM','JJA','SON'
 112  format(2a9,4a11)
 111  format(2f9.3,1p4e11.3)
      do y=1,10
        do i=1,2048
          write(11,111) 0.0,0.0,(CDD(y,i,s),s=1,4)
        enddo
      enddo
      close(11)
      open(unit=11,file='HDD.out')
      write(11,112) 'lon','lat','DJF','MAM','JJA','SON'
      do y=1,10
        do i=1,2048
          write(11,111) 0.0,0.0,(HDD(y,i,s),s=1,4)
        enddo
      enddo
      close(11)

      end
