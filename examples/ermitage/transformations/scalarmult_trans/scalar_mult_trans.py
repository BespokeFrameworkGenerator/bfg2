import numpy
from string import Template
from numpy import matrix


class scalar_mult_trans(object):

    def __init__(self, expression, replace):

        print("expression: {0}({1})----replace: {2}({3})".format(expression, len(expression), replace, len(replace)))
#Added...
        expression = "280*(2**($var/3.7))"
        replace = "var"

        d = {replace:'data'}
        self.expression=Template(expression).substitute(d)

    def run(self,dataIn):
        result=[]
        for data in dataIn:
            print("[scalar_mult_trans] expression: {0} data: {1}".format(self.expression, data) )
            result.append(eval(self.expression))

        print("[scalar_mult_trans] result: {0} element_type: {1}".format(result, type(result[0]) ) )
        raw_input()

        return numpy.array(result)
