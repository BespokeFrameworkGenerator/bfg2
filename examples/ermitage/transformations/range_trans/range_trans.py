import numpy


class range_trans(object):

    def timeseries(self, start, end):
        arr_range = [float(i) for i in range(start, end+1)]
        return numpy.array(arr_range)
#        return numpy.array(range(start, end+1))
