class SingleTemperature:

    def __init__(self,input_type):
	#print "HELLO FROM SINGLETEMPERATURE INIT"
	#print "INPUT_TYPE=",input_type
        if input_type==0:
	    self.input_type="smooth"
        else:
            self.input_type="linear"
	#print "[SingleTemperature] constructor, input_type={}\n".format(input_type)

    def run(self,QuantileTemperatureIncrease, GlobalTemperatureIncrease):
        #print "SingleTemperature:run:input_type=",self.input_type
        if self.input_type=="smooth":
            #print "GlobalTemperatureIncrease",GlobalTemperatureIncrease
            #print "GlobalTemperatureIncrease[4]",GlobalTemperatureIncrease[4]
            return GlobalTemperatureIncrease[4]
        elif self.input_type=="linear":
            #print "QuantileTemperatureIncrease",QuantileTemperatureIncrease
            #print "QuantileTemperatureIncrease[5,8]",QuantileTemperatureIncrease[5,8]
            return QuantileTemperatureIncrease[5,8]
        else:
            assert False,"Error"
