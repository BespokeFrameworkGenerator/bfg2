# reads in GENIE2 PC emulators for first 5 EOFs and calculates SAT anomaly (2105-2005) as a function of three chebyshev coefficients.
# GENIE2 emulations perfromed from 122 data sets. The emulators were built from 245 data dets, but parameterisations were doubled-up, so only necessary to include a single set.  

#read ensemble data frames
genie2_data <- read.table("GENIE2_data.txt")

#if emulation is done on Y'
#base_anomaly <- scan("SAT_sim.dat")

# read cubic GENIE2 PC models 
load('cm_PC1.rda')
load('cm_PC2.rda')
load('cm_PC3.rda')
load('cm_PC4.rda')
load('cm_PC5.rda')

#read first 5 EOFs and combine into matrix 
eof1 <- scan("EOF_1.dat")
eof2 <- scan("EOF_2.dat")
eof3 <- scan("EOF_3.dat")
eof4 <- scan("EOF_4.dat")
eof5 <- scan("EOF_5.dat")

eof <- eof1
eof <- cbind(eof,eof2)
eof <- cbind(eof,eof3)
eof <- cbind(eof,eof4)
eof <- cbind(eof,eof5)

#read eigenvalues and convert to diagonal matrix
d <- scan("eigenvalues.dat")
D <- diag(d[1:5])

# input chebyshev coefficients 
print("chebyshev fit to C = CO + 0.5 * (A1*(t+1) + A2*(2*t^2-2) + A3 * (4*t^3 - 4*t))")
print("1st chebyshev coefficient?")
a1g2_actual <- scan()
a1g2_actual <- a1g2_actual/2.0
a1g2 <- (a1g2_actual/500)*2-1
print(a1g2)

print("2nd chebyshev coefficient?")
a2g2_actual <- scan()
a2g2_actual <- a2g2_actual/2.0
a2g2 <- ((a2g2_actual+100)/200)*2-1
print(a2g2)

print("3rd chebyshev coefficient?")
a3g2_actual <- scan()
a3g2_actual <- a3g2_actual/2.0
a3g2 <- ((a3g2_actual+50)/100)*2-1
print(a3g2)

#TIAM warming
print("TIAM warming for scaling?")
TIAM_warming <- scan()

#replace chebyshev coeffs in ensemble data frame with input values
#genie2
test_g2 <- genie2_data
test_g2[,21] <- a1g2
test_g2[,22] <- a2g2
test_g2[,23] <- a3g2

# predict genie2 PCs as a function of chebyshev coeffs
pc1 <- predict(cm_PC1,test_g2)
pc2 <- predict(cm_PC2,test_g2)
pc3 <- predict(cm_PC3,test_g2)
pc4 <- predict(cm_PC4,test_g2)
pc5 <- predict(cm_PC5,test_g2)

#combine predicted PCs into a matrix
pc <- pc1
pc <- cbind(pc,pc2)
pc <- cbind(pc,pc3)
pc <- cbind(pc,pc4)
pc <- cbind(pc,pc5)

#calculate emulated genie2 fields: multiply EOF, eigenvalue and (transpose) PC matrices
SAT_mat <- eof %*% D %*% t(pc)

#calculate average, weighted average  and standard deviation of SAT fields
SAT <- rowMeans(SAT_mat)
SAT_SD <- sqrt(apply(SAT_mat,1,var))


#scale to TIAM warming
weights <- scan("weights.dat")
weights <- rep(weights,64)
scaling <- TIAM_warming/sum(SAT*weights)
print("GENIE2 warming ")
print(sum(SAT*weights))
SAT <- scaling*SAT
print("TIAM scaled warming ")
print(sum(SAT*weights))

#output average and standard deviations of SAT fields
write(SAT,"SAT.dat",1)
write(SAT_SD,"SAT_SD.dat",1)





