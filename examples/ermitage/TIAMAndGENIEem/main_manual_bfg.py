import os
import sys
from tiam_model import tiam_model
from genie_em_model import genie_em_model
from scalar_mult_trans import scalar_mult_trans
from interp_trans import interp_trans
from converge_trans import converge_trans
from genie2tiam_trans import genie2tiam_trans

tiam_instance=tiam_model(runTIAM=True)
genie_em_instance=genie_em_model()
scalmult_tiam2genie=scalar_mult_trans("280*(2**($var/3.7))","var")
target=1.45
tolerance=0.05
genie2tiam_instance=genie2tiam_trans(target,target)
converge_instance=converge_trans(target,tolerance)

fromTimeSeries=[]
toTimeSeries=[]

interp_instance=interp_trans()

# specify required genie timeseries
genie_timeseries=[x for x in range(2005,2100+1)]

# primed value for tiam
tiam_in=target

converged=False

while not converged:

    # run TIAM
    os.chdir(os.path.dirname(sys.modules["tiam_model"].__file__))
    tiam_timeseries,forcing=tiam_instance.run(tiam_in)

    # convert forcings to concentrations
    concent_co2=scalmult_tiam2genie.run(forcing)

    # interpolate to genie timeseries
    concent_co2=interp_instance.run(tiam_timeseries,genie_timeseries,concent_co2)

    # run genie with new concentrations
    os.chdir(os.path.dirname(sys.modules["genie_em_model"].__file__))
    quantile_temp_inc,av_temp_inc=genie_em_instance.run(genie_timeseries,concent_co2)
    print "av_temp_inc ",str(av_temp_inc)

    # convert genie output to tiam input
    os.chdir(os.path.dirname(sys.modules["genie2tiam_trans"].__file__))
    tiam_in=genie2tiam_instance.run(av_temp_inc)

    print "tiam_in is "+str(tiam_in)

    # check for convergence
    os.chdir(os.path.dirname(sys.modules["converge_trans"].__file__))
    converged=converge_instance.check(av_temp_inc)

    print "converged is "+str(converged)

exit(0)
