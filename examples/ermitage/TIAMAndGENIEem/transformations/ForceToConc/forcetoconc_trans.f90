module forcetoconc_trans
implicit none
private
public :: run
contains

subroutine run(dataIn,dataOut)
!  real,intent(in)  :: dataIn
!  real,intent(out) :: dataOut
!  dataOut=280.0*power(dataIn/3.7,2)
  double precision,intent(in)  :: dataIn
  double precision,intent(out) :: dataOut
  print *, "===> forcetoconc_trans::run()"
  dataOut = 280.0 * (dataIn/3.7)**2
end subroutine run

end module forcetoconc_trans

