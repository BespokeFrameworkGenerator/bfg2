class convergence_test:

    def __init__(self,target,precision):
        #print "convergence test transformation:init",target,precision
        self.target=target
        self.precision=precision
        #print("[convergence_test] constructed with target: {0} precision: {1}".format(target, precision))

    def continue1(self,value):
        #print "convergence_test:continue1:value",value
        #print "convergence_test:continue1:target",self.target
        #print "convergence_test:continue1:precision",self.precision
        #print "convergence_test:continue1:return",str(abs(value - self.target) > self.precision)
        if abs(value - self.target) > self.precision : return True
        else : return False

    def continue2(self,value):
        #print "convergence_test:continue2:value",value
        #print "convergence_test:continue2:return",str(abs(value - self.target) > self.precision)
        if abs(value - self.target) > self.precision : return True
        else : return False
