
# python main_manual_bfg.py 1.5 0.01 0 0
#
# Input:  1 -> temperature rise target
#         2 -> precision of the resulting increase temperature
#         3 -> linear (1) or smooth trajectory (0)
#         4 -> test RCPs emissions 2.6, 4.5, 6.0 or 8.5

from math import log
import array
import csv
import struct
import sys
import os.path
import numpy
from numpy import matrix, mat, zeros, sqrt
import random
import subprocess

from gemini_e3_bau import gemini_e3_bau
from gemini_e3_bau_filereader import gemini_e3_bau_filereader
from em2conc import em2conc
from PlasimEntsModel import PlasimEntsModel
from Conc2Chebyshev import Conc2Chebyshev
from gemini_e3_clim import gemini_e3_clim

from convergence_test import convergence_test
from emissions_control import emissions_control
from SingleTemperature import SingleTemperature
from Plasim2Gemini import Plasim2Gemini

dirname = "C:\Users\bafre\Documents\Emul_Temperature_2050"

target = float(sys.argv[1]) - 0.76
precision = float(sys.argv[2])
#quantile_target = int(sys.argv[3]) 
#fixed_year = int(sys.argv[4])
#gemini_input = float(sys.argv[5])
quantile_target = 0
fixed_year = 2010
gemini_input = 0 # Gemini is run at the end only
linear = float(sys.argv[3])
RCP = float(sys.argv[4])
Temperature_increase = 1000
base_year=2001
final_year=2050
hddcdd=True
crops=False
CEREAL=None
RICE=None
MAIZE=None
OIL=None

if RCP > 0:
  gemini_input_temp = gemini_input 
  gemini_input = 0
  fixed_year = final_year
  precision = 10
  RCP_ext = str(RCP);
else:
  RCP_ext = '';

nb_fixed_year = fixed_year - base_year
numYears=(final_year-base_year)+1



gemini_e3_bau_instance=gemini_e3_bau()
gemini_e3_bau_filereader_instance=gemini_e3_bau_filereader()
converge_instance=convergence_test(target,precision)
em2conc_instance=em2conc(nb_fixed_year,base_year)
# you can specify what you want the gemini to input here
# hddcdd,crops
gemini_e3_clim_instance=gemini_e3_clim(base_year,numYears,hddcdd,crops)
#gemini_e3_clim_instance=gemini_e3_clim(base_year,numYears)
conc2Chebyshev_instance=Conc2Chebyshev()
os.chdir(os.path.dirname(sys.modules["PlasimEntsModel"].__file__))
# you can specify what you want the emulator to output here
# temperature,temperature_variability,precipitation,evaporation,cloud_cover
# we need temperature and temperature_variability
temperature=True
temperature_variability=True
precipitation=False
evaporation=False
cloud_cover=False
plasimEntsem_instance=PlasimEntsModel(temperature,temperature_variability,precipitation,evaporation,cloud_cover)

if gemini_input == 1:
  # run initial unconstrained gemini model
  os.chdir(os.path.dirname(sys.modules["gemini_e3_bau"].__file__))
  CO2base,N2Obase,CH4base=gemini_e3_bau_instance.run(numYears,RCP_ext)
else:
  # read initial data from files
  os.chdir(os.path.dirname(sys.modules["gemini_e3_bau_filereader"].__file__))
  CO2base,N2Obase,CH4base=gemini_e3_bau_filereader_instance.run(numYears,RCP_ext)

emissions_control_instance=emissions_control(nb_fixed_year,target,precision,linear,CO2base,N2Obase,CH4base)


# initial convergence check
os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
cont=converge_instance.continue1(Temperature_increase)

f = open('temp_increase.txt', 'w')
while (cont):

  # get current emissions
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  CO2,N2O,CH4=emissions_control_instance.get_emissions()

  # run em2conc model with current emissions
  os.chdir(os.path.dirname(sys.modules["em2conc"].__file__))
  time,concent_co2=em2conc_instance.run(CO2,N2O,CH4)

  os.chdir(os.path.dirname(sys.modules["Conc2Chebyshev"].__file__))
  chebyshev=conc2Chebyshev_instance.run(time,concent_co2)

  f.write(str(chebyshev)+"\n")

  os.chdir(os.path.dirname(sys.modules["PlasimEntsModel"].__file__))
  quantile_temp_inc,av_temp_inc,temperature,temperature_variability=plasimEntsem_instance.run(chebyshev)

  f.write(str(av_temp_inc)+"\n")

  singleTemperature_instance=SingleTemperature(quantile_target)
  #if quantile_target == 0:
  #  singleTemperature_instance=SingleTemperature("linear")
  #else:
  #  singleTemperature_instance=SingleTemperature("smooth")
  os.chdir(os.path.dirname(sys.modules["SingleTemperature"].__file__))
  Temperature_increase=singleTemperature_instance.run(quantile_temp_inc,av_temp_inc)

  f.write(str(Temperature_increase)+"\n")
  print Temperature_increase + 0.76

  # plasim2gemini is not a fully modularised transformation but is BFG2 compliant and
  # gets us going. The issue is that this contains 3 transformations which we should
  # really describe and use separately
  plasim2gemini_instance=Plasim2Gemini()
  os.chdir(os.path.dirname(sys.modules["Plasim2Gemini"].__file__))
  HDD,CDD=plasim2gemini_instance.run(temperature,temperature_variability)

  # update emissions 2050 target
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  emissions_control_instance.update_2050_emissions(Temperature_increase)

  # convergence check
  os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
  cont=converge_instance.continue2(Temperature_increase)

#if RCP == 1:
#  gemini_input = gemini_input_temp

#if gemini_input == 1:

f.close()
os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
CO2,N2O,CH4=emissions_control_instance.getFinalValues()

# run final constrained gemini model
os.chdir(os.path.dirname(sys.modules["gemini_e3_clim"].__file__))
gemini_e3_clim_instance.run(CO2,N2O,CH4,HDD,CDD,CEREAL,RICE,MAIZE,OIL)
#gemini_e3_clim_instance.run(CO2,N2O,CH4,HDD,CDD)


