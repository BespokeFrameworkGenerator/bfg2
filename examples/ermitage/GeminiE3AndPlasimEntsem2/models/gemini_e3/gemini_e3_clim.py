import sys
import os
import csv
from numpy import zeros
class gemini_e3_clim:

    def __init__(self,base_year,num_years):
        self.base_year=base_year # 2001
        self.num_years=num_years # 50
	#print "[gemini_e3_clim] constructed with base_year={}, num_years={}\n".format(base_year,num_years)

    def run(self,CO2,N2O,CH4,HDD,CDD):


        #print "GEMINI_E3_CLIM:RUN"
        #print "CO2:SIZE {}".format(CO2.shape)
        #print "N2O:SIZE {}".format(N2O.shape)
        #print "CH4:SIZE {}".format(CH4.shape)
        #print "HDD:SIZE {}".format(HDD.shape)
        #print "CDD:SIZE {}".format(CDD.shape)
        #print "CO2 {}".format(CO2)
        #print "N2O {}".format(N2O)
        #print "CH4 {}".format(CH4)
        #print "HDD {}".format(HDD)
        #print "CDD {}".format(CDD)
        #sys.exit(1)

        c1 = open("CO2sum_data.gms", "wb")
        c2 = open("N2Osum_data.gms", "wb")
        c3 = open("CH4sum_data.gms", "wb")
        c1.write("parameter CO2sum(T)/\n")
        c2.write("parameter N20sum(T)/\n")
        c3.write("parameter CH4sum(T)/\n")
        for i in xrange(self.num_years):
            c1.write(str(self.base_year+i) + "    " + str(CO2[i]) + "\n")
            c2.write(str(self.base_year+i) + "    " + str(N2O[i]) + "\n")
            c3.write(str(self.base_year+i) + "    " + str(CH4[i]) + "\n")
        c1.write("/;")
        c2.write("/;")
        c3.write("/;")
        c1.close()
        c2.close()
        c3.close()

########################################
# Part added for HDD and CDD (impact of CC on heating and cooling)
########################################
        c1 = open("HDD_CDD.gms", "wb")

        HDD_init = [115.751, 2189.05, 2111.14, 853.472, 3808.371, 1164.063, 2823.344, 34.079, 791.378, 7.489,  886.908, 1399.047, 4295.619, 247.85]
        c1.write("TABLE HDD(T,reg)" + "\n")
        c1.write(("AFR").rjust(16)+("EEU").rjust(12)+("WEU").rjust(12)+("AUZ").rjust(12)+("FSU").rjust(12)+("CHI").rjust(12)+("REA").rjust(12)+("IND").rjust(12)+("RSA").rjust(12)+("SEA").rjust(12)+("MID").rjust(12)+("USA").rjust(12)+("CAN").rjust(12)+("LAT").rjust(12)+ "\n")
        #os.system("dir") 
        #file = "../genie_em/emulator_output/GEMINI_regions_HDD_with_pop.out"
        #reader = csv.reader(open(file,"rb"))  
        #list_reader = []
        #for row in reader:
        #    list_reader.append(row[0])
        #n = len(list_reader)-20
        #HDD= zeros(n)
        #for i in xrange(10):
        #    for j in xrange(14):
        #        line = list_reader[i*16+j+2].split()
        #        HDD[i*14 + j] = float(line[1]) + float(line[2]) + float(line[3]) + float(line[4])

        for i in xrange(10):
             for k in xrange(10):
                 c1.write(str(2000 + i*10 + k))
                 for j in xrange(14):
                     c1.write(str(HDD_init[j] + (HDD[i*14 + j ] - HDD_init[j])/10*k).rjust(12))
                 c1.write("\n")
             for j in xrange(14):
                 HDD_init[j] = float(HDD[i*14 + j ])
        c1.write(str(2100))  
        for j in xrange(14):
            c1.write(str(HDD[9*14 + j]).rjust(12))
        c1.write("\n;\n")


        CDD_init = [3266.205, 576.853, 433.542, 1370.845, 791.139, 1193.615, 454.297, 4778.201, 3282.989, 3027.123, 2580.368, 1494.447, 202.76, 2060.765]
        c1.write("TABLE CDD(T,reg)" + "\n")
        c1.write(("AFR").rjust(16)+("EEU").rjust(12)+("WEU").rjust(12)+("AUZ").rjust(12)+("FSU").rjust(12)+("CHI").rjust(12)+("REA").rjust(12)+("IND").rjust(12)+("RSA").rjust(12)+("SEA").rjust(12)+("MID").rjust(12)+("USA").rjust(12)+("CAN").rjust(12)+("LAT").rjust(12)+ "\n")
        #os.system("dir") 
        #file = "../genie_em/emulator_output/GEMINI_regions_CDD_with_pop.out"
        #reader = csv.reader(open(file,"rb"))  
        #list_reader = []
        #for row in reader:
        #    list_reader.append(row[0])
        #
        #n = len(list_reader)-20
        #CDD= zeros(n)
        #for i in xrange(10):
        #    for j in xrange(14):
        #        line = list_reader[i*16+j+2].split()
        #        CDD[i*14 + j] = float(line[1]) + float(line[2]) + float(line[3]) + float(line[4])

        for i in xrange(10):
             for k in xrange(10):
                 c1.write(str(2000 + i*10 + k))
                 for j in xrange(14):
                     c1.write(str(CDD_init[j] + (CDD[i*14 + j ] - CDD_init[j])/10*k).rjust(12))
                 c1.write("\n")
             for j in xrange(14):
                 CDD_init[j] = float(CDD[i*14 + j ])
        c1.write(str(2100))  
        for j in xrange(14):
            c1.write(str(CDD[9*14 + j]).rjust(12))
        c1.write("\n;\n")

        c1.close()
#########################################    
        try:
            #print "[python] gemini_e3_clim:run calling gams code"
            os.system("gams ./Variante.gms")
            #print "[python] gemini_e3_clim:run gams code complete"
            #subprocess.call('gams ./Variante.gms')
        except OSError, e:
            print "Error calling GEMINI_E3_CLIM: "+str(e)
            print "Aborting"
            exit(1)
