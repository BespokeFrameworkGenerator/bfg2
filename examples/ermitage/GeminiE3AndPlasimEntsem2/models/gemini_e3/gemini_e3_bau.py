import os
from numpy import zeros
import csv
class gemini_e3_bau:

    def run(self,nYears,RCP_ext,fileReader=False):

	print "**** RCP_ext=",RCP_ext
        print "[python] started gemini_e3_bau:run"
	print "**** RCP_ext=",RCP_ext

        if not fileReader:
            try:
                print "[python] gemini_e3_bau:run calling gams code"
                os.system("gams ./Variante_BAU.gms")
                print "[python] gemini_e3_bau:run gams code complete"
            except OSError, e:
                print "Error calling GEMINI_E3_BAU: "+str(e)
                print "Aborting"
                exit(1)

        CO2 = zeros(nYears)
        N2O = zeros(nYears)
        CH4 = zeros(nYears)
        # Retrieving emissions from Gemini-e3
        reader1 = csv.reader(open("./Gemini_CO2_results" + RCP_ext + ".txt","rb"))
        reader2 = csv.reader(open("./Gemini_NO2_results" + RCP_ext + ".txt","rb"))
        reader3 = csv.reader(open("./Gemini_CH4_results" + RCP_ext + ".txt","rb"))
        list_reader1 = []
        list_reader2 = []
        list_reader3 = []
        for row in reader1:
            list_reader1.append(row[0])
        for row in reader2:
            list_reader2.append(row[0])
        for row in reader3:
            list_reader3.append(row[0])
        for i in xrange(nYears):
            CO2[i] = float(list_reader1[i])
            N2O[i] = float(list_reader2[i])
            CH4[i] = float(list_reader3[i])

        print "[python] Completed gemini_e3_bau:run"

        return CO2,N2O,CH4
