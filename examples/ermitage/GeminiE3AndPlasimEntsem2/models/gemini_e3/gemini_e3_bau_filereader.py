from gemini_e3_bau import gemini_e3_bau
class gemini_e3_bau_filereader:

    def run(self,nYears,RCP_ext):

	if RCP_ext==0.0:
	    self.RCP_ext=""
	else:
	    self.RCP_ext=str(RCP_ext)
	#print "**** nYears={}, RCP_ext={}\n".format(nYears,RCP_ext)
        # BFG hack as strings are not being passed correctly
	#RCP_ext=RCP_ext[:10].strip()
        #print "[gemini_e3_bau_filereader:run] nYears={} ,RCP_ext='{}'\n".format(nYears,self.RCP_ext)
        gemini_e3_bau_instance=gemini_e3_bau()
        return gemini_e3_bau_instance.run(nYears,self.RCP_ext,fileReader=True)
