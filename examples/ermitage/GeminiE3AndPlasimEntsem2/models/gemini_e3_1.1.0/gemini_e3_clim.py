import os

class gemini_e3_clim:

    def __init__(self,base_year,num_years,hddcdd,crops):
        self._base_year=base_year # 2001
        self._num_years=num_years # 50
        self._hddcdd=self._setValue(hddcdd)
        self._crops=self._setValue(crops)
        self._verbose=False

    def run(self,CO2,N2O,CH4,HDD,CDD,CEREAL,RICE,MAIZE,OIL):

        self._write_concentrations("CO2",CO2)
        self._write_concentrations("N2O",N2O)
        self._write_concentrations("CH4",CH4)

        c1 = open("option.gms","w")
        c1.write("option_lpjml = "+str(int(self._crops))+";\n")
        c1.write("option_hddcdd = "+str(int(self._hddcdd))+";\n")
        c1.close()

        if self._hddcdd:
            c1 = open("HDD_CDD.gms", "wb")
            HDD_init = [115.751, 2189.05, 2111.14, 853.472, 3808.371,
                        1164.063, 2823.344, 34.079, 791.378, 7.489,
                        886.908, 1399.047, 4295.619, 247.85]
            self._write_hddcdd("HDD",c1,HDD_init,HDD)
            CDD_init = [3266.205, 576.853, 433.542, 1370.845, 791.139,
                        1193.615, 454.297, 4778.201, 3282.989, 3027.123,
                        2580.368, 1494.447, 202.76, 2060.765]
            self._write_hddcdd("CDD",c1,CDD_init,CDD)
            c1.close()

        if self._crops:
            self._write_crops("cereal",countries,CEREAL,0)
            self._write_crops("rice"  ,countries,RICE  ,1)
            self._write_crops("maize" ,countries,MAIZE ,2)
            self._write_crops("oil"   ,countries,OIL   ,3)

        try:
            if self._verbose:
                print "[python] gemini_e3_clim:run calling gams code"
            os.system("gams ./Variante.gms")
            if self._verbose:
                print "[python] gemini_e3_clim:run gams code complete"
        except OSError, e:
            print "Error calling GEMINI_E3_CLIM: "+str(e)
            print "Aborting"
            exit(1)

    def _setValue(self,origValue):
        if type(origValue) is bool:
            newValue=origValue
        elif type(origValue) is int:
            if origValue==0:
                newValue=False
            else:
                newValue=True
        else:
            raise Exception, "Error"
        return newValue

    def _write_concentrations(self,type,data):
        c1 = open(type+"sum_data.gms", "wb")
        c1.write("parameter "+type+"sum(T)/\n")
        for i in xrange(self._num_years):
            c1.write(str(self._base_year+i) + "    " + str(data[i]) + "\n")
        c1.write("/;")
        c1.close()

    def _write_hddcdd(self,type,file,XDD_init,XDD):
        file.write("TABLE "+type+"(T,reg)" + "\n")
        file.write(("AFR").rjust(16)+("EEU").rjust(12)+("WEU").rjust(12)+("AUZ").rjust(12)+("FSU").rjust(12)+("CHI").rjust(12)+("REA").rjust(12)+("IND").rjust(12)+("RSA").rjust(12)+("SEA").rjust(12)+("MID").rjust(12)+("USA").rjust(12)+("CAN").rjust(12)+("LAT").rjust(12)+ "\n")
        for i in xrange(10):
            for k in xrange(10):
                file.write(str(2000 + i*10 + k))
                for j in xrange(14):
                    file.write(str(XDD_init[j] + (XDD[i*14 + j ] - XDD_init[j])/10*k).rjust(12))
                file.write("\n")
            for j in xrange(14):
                XDD_init[j] = float(XDD[i*14 + j ])
        file.write(str(2100))  
        for j in xrange(14):
            file.write(str(XDD[9*14 + j]).rjust(12))
        file.write("\n;\n")

    def _write_crops(self,crop_type,countries,data,offset):
        # TBD: add a check to make sure data array dimensions match expected size
        c = open(crop_type+".gms", "wb")
        c.write("Table "+crop_type.capitalize()+"1(l,ty)\n")
        c.write("2000".rjust(50) + "2010".rjust(21) + "2020".rjust(21) + "2030".rjust(21) +
                "2040".rjust(21) + "2050".rjust(21) + "2060".rjust(21) + "2070".rjust(21) +
                "2080".rjust(21) + "2090".rjust(21) + "\n")
        idx=[1,5,14,23,32,41,50,59,68]
        for i in xrange(len(countries)):
            c.write( str(countries[i]).ljust(30,' ') + " ")
            for j in idx:
                c.write(str(data[j+offset,i]).rjust(20) + " ")
            c.write( str(data(data[68+offset,i]) + float(data[64+offset,i])).rjust(20) + "\n")
        c.write(";\n\n")
        c.close()

