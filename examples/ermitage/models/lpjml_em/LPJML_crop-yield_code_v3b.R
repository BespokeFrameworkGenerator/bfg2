###################################
args <- commandArgs(trailingOnly = TRUE)
print(args)
j <- as.integer(args[1])
GCM <- args[2]
rcp <- args[3]
rm(args)
path=""

dirname <- getwd()
v3=sub("rcp",rcp,(sub("GCM",GCM,"dat_GCM_rcp")))
#v1=gsub("ukmo-hadgem1",GCM,paste(path,"/dat_ukmo-hadgem1_RCP3PD"))
#v2=gsub("ukmo-hadgem1",GCM,"dat_ukmo-hadgem1_RCP45")
#v3=gsub("ukmo-hadgem1",GCM,"dat_ukmo-hadgem1_RCP6")
#v4=gsub("ukmo-hadgem1",GCM,"dat_ukmo-hadgem1_RCP85")
#load(v3)
load(paste("climate",v3,sep="/"))
s1=eval(parse(text = as.name(v3)))
s2=array(s1,c(59199,8,45))
inp=s2[,j,]
colnames(inp)=colnames(s1)
load("N")
load("grid_out")
input=as.data.frame(inp)
################cereal
load("form1")
load("coef1")
t=terms(f_cereal1,input,keep.order = TRUE,specials=NULL)
x=model.matrix(t,input)
cereal <- as.vector(x%*%coef_cereal1)
cereal[N1]=0
################rice
t=terms(f_rice1,input,keep.order = TRUE,specials=NULL)
x=model.matrix(t,input)
rice <- as.vector(x%*%coef_rice1)
rice[N2]=0
#################maize
t=terms(f_maize1,input,keep.order = TRUE,specials=NULL)
x=model.matrix(t,input)
maize <- as.vector(x%*%coef_maize1)
maize[N3]=0
################oil
t=terms(f_oil1,input,keep.order = TRUE,specials=NULL)
x=model.matrix(t,input)
oil <- as.vector(x%*%coef_oil1)
oil[N4]=0
#################################################
#####include PCA analysis
####file(l_cereal,emu_cereal,w_cereal,error_cereal)
##################PCA for cereal
load("err1")
e1=rowMeans(matrix(e_cereal,nrow=8*nrow(grid_out)))

e2=rowMeans(matrix(e_rice,nrow=8*nrow(grid_out)))

e3=rowMeans(matrix(e_maize,nrow=8*nrow(grid_out)))

e4=rowMeans(matrix(e_oil,nrow=8*nrow(grid_out)))

e11=array(e1,c(nrow(grid_out),8))

e22=array(e2,c(nrow(grid_out),8))

e33=array(e3,c(nrow(grid_out),8))

e44=array(e4,c(nrow(grid_out),8))
#load("emu_cereal")
#load("l_cereal")
#load("w_cereal")
#load("error_cereal")
#cere=cereal[-N1]
#cere=rep(cere,ncol(emu_cereal))
#cere=matrix(cere,nrow=nrow(cere))
#new_sce=cere%*%l_cereal
#d_ij=colSums((emu_cereal-new_sce))
#w_cereal=as.matrix(w_cereal)
#D=d_ij*w_cereal   ## compute metric distance
#U=sum(1/D)
#ee=matrix(0,nrow=nrow(cere),ncol=ncol(emu_cereal))
#for (r in 1:ncol(emu_cereal)){
#ee[,r]=error_cereal[,r]/(U*D[r,])}
E_cereal=e11[,j]
E_rice=e22[,j]
E_maize=e33[,j]
E_oil=e44[,j]
cereal=rowSums(cbind(cereal,E_cereal))
rice=rowSums(cbind(rice,E_rice))
maize=rowSums(cbind(maize,E_maize))
oil=rowSums(cbind(oil,E_oil))
########################################
crop=cbind(grid_out,cereal,rice,maize,oil)

dirname <- getwd()
write.csv(crop,file=paste(dirname,sub("j",j+1,(sub("j",j,"crop_j_j decadal-change.csv"))),sep="/"),row.names=FALSE)
###############END

