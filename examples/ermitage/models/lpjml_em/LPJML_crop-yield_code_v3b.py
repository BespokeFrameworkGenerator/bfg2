class LPJML_crop-yield_code_v3b:

    def __init__(self):
        pass

    def run(self,decade,gcm,rcp):

        import sys
        import subprocess
        import os
        import csv

        # Check options are valid
        jMin=1; jMax=8
        gcmOptions=["ukmo_hadgem1","ccsr_miroch32hi","giss_modeleh","ipsl_cm4","ccsr_miroc32med","cccma_cgcm31","giss_modeler","ECHAM5ALL","gfdl_cm20","gfdl_cm21"]
        rcpOptions=["RCP3PD","RCP45","RCP6","RCP85"]
        assert j>=jMin and j<=jMax and gcm in gcmOptions and rcp in rcpOptions, "Error, invalid values selected"

        # Call the R script with our arguments
        infile = open("LPJML_crop-yield_code_v3b.py", 'r')
        outfile = open("out.txt", 'w')
        args = ["R", "--no-restore", "--no-save", "--args", str(decade), gcm, rcp]
        proc = subprocess.Popen(args, stdin=infile, stdout=outfile, env=os.environ, close_fds=True)
        proc.wait()

        # check for success
        if proc.returncode:
            print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
            sys.exit(proc.returncode)

        # extract the required results and return
        fileName="crop_"+str(decade)+"_"+str(decade+1)+" decadal-change.csv"
        reader = csv.reader(open(fileName,"rb"),delimiter=",")
        #format expected is : lat, lon, cereal, rice, maize, oil
        lat=[];lon=[];cereal=[];rice=[];maize=[];oil=[]
        first=True
        for row in reader:
            if first:
                # skip the first row as it contains the titles
                first=False
            else:
                lat.append(row[0])
                lon.append(row[1])
                cereal.append(row[2])
                rice.append(row[3])
                maize.append(row[4])
                oil.append(row[5])
        return lat,lon,cereal,rice,maize,oil

if __name__ == "__main__":

    # Simple sample wrapper
    # If called from the command line I will run

    # Chose values manually for simplicity - could easily read a file or input from command line
    j=4
    gcm="ukmo_hadgem1"
    rcp="RCP6"

    # Call model
    myLPJML_crop_em = LPJML_crop-yield_code_v3b()
    lat,lon,cereal,rice,maize,oil=myLPJML_crop_em.run(j,rcp,gcm)

    # Do something with the output
    print str(len(lat))
    print str(len(lon))
    print str(len(cereal))
    print str(len(rice))
    print str(len(maize))
    print str(len(oil))
    print "complete"

