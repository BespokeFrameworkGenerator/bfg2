from subprocess import call

import csv

class tiam_model:

    def __init__(self, runTIAM=True):
        print "tiam_model:constructor"
        self.outputFile="TWout.put"
        self.gamsFile="TWTempInc.gms"
        self.runTIAM=runTIAM
    
    def run(self,tiam_in):
        print "tiam_model:run()"

        # create TIAM input file
        c = open("TempInc.INC", "wb")
        c.write("e374247..    x969963 =L= "+str(tiam_in)+";\n")
        c.write("e374248..    x969964 =L= "+str(tiam_in)+";\n")
        c.write("e374249..    x969965 =L= "+str(tiam_in)+";\n")
        c.write("e374250..    x969966 =L= "+str(tiam_in)+";\n")
        c.write("e374251..    x969967 =L= "+str(tiam_in)+";\n")
        c.write("e374252..    x969968 =L= "+str(tiam_in)+";\n")
        c.write("e374253..    x969969 =L= "+str(tiam_in)+";\n")
        c.write("e374254..    x969975 =L= "+str(tiam_in)+";\n")
        c.close()


        # run TIAM
        if self.runTIAM:
            try:
                ret_code=call(["gams",self.gamsFile])
            except Exception:
                print "Call to gams failed"
                exit(1)
            if ret_code!=0:
                print "Call to gams failed with code "+str(ret_code)
                exit(1)

        # extract data from output file
        f=open(self.outputFile,"r")
        rawdata=csv.reader(f,delimiter=";")
        first=True
        year=[]
        forcing=[]
        for line in rawdata:
            if first:
                first=False
            else:
                year.append(int(line[0]))
                forcing.append(float(line[1]))

        return year,forcing
