#!/usr/bin/env python
import math
from numpy import zeros

class sine:

    def __init__(self,nSamples=256,height=1.0,offset=0.0):
        self.nSamples=nSamples
        self.height=height
        self.offset=offset

    def compute(self,nPeriods=1.0):
        value=zeros(self.nSamples)
        for x in range(self.nSamples):
            y = self.height * math.sin (nPeriods * 2.0 * math.pi * ((x / float(self.nSamples)+self.offset)))
            value[x]=y
        return value

if __name__ == "__main__":
    test=sine()
    value=test.compute()
    print str(value)
    print type(value)
