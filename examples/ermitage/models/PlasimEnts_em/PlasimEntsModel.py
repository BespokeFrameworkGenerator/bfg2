import sys
import subprocess
import os
import csv
import numpy

class PlasimEntsModel:

    def __init__(self,temperature,temperature_variability,precipitation,evaporation,cloud_cover):
	self.temperature=self._setValue(temperature)
        self.temperature_variability=self._setValue(temperature_variability)
        self.precipitation=self._setValue(precipitation)
        self.evaporation=self._setValue(evaporation)
        self.cloud_cover=self._setValue(cloud_cover)
        file = open(os.path.join("emulator_input","output_config.txt"), "w");
        file.write("temperature,temperature_variability,precipitation,evaporation,cloud_cover\n");
        file.write(",".join(str(int(conf)) for conf in [self.temperature,self.temperature_variability,self.precipitation,self.evaporation,self.cloud_cover]) + "\n");
        file.close();

    def _setValue(self,origValue):
	if type(origValue) is bool:
	    newValue=origValue
        elif type(origValue) is int:
            if origValue==0:
	        newValue=False
            else:
                newValue=True
        else:
	    raise Exception, "Error"
	return newValue

    def run3(self, ChebPoly1, ChebPoly2, ChebPoly3):
        result1,result2,result3,result4 = self.run([ChebPoly1, ChebPoly2, ChebPoly3])
	return result1,result2,result3,result4

    def run6(self, ChebPoly1, ChebPoly2, ChebPoly3, ChebPoly4, ChebPoly5, ChebPoly6):
        return self.run([ChebPoly1, ChebPoly2, ChebPoly3, ChebPoly4, ChebPoly5, ChebPoly6])

    def run(self,ChebyshevCoefficients):
        self._createInputs(ChebyshevCoefficients)
        self._runEmulator()
        result1,result2,result3,result4 = self._extractResults()
        return result1,result2,result3,result4

    def _createInputs(self,ChebyshevCoefficients):
        # Write the Chebyshev Coefficients into the file "emulator_input/coeffs.txt" and check data
        file = open( os.path.join("emulator_input","coeffs.txt"), "wb")
        if len(ChebyshevCoefficients)==3:
            file.write("TC1,TC2,TC3\n")
        elif len(ChebyshevCoefficients)==6:
            file.write("TC1e,TC2e,TC3e,TC1a,TC2a,TC3a\n")
        else:
            print "BFG2: Fatal error, expecting 3 or 6 Chebyshev Coefficients as input"
            sys.exit(1)
        for idx, coeff in enumerate(ChebyshevCoefficients):
            file.write(str(coeff))
            if idx+1!=len(ChebyshevCoefficients): file.write(",")
        file.write("\n")
        file.close()
        
    def _runEmulator(self):
        # run the emulator
        infile = open("emul.R", 'r')
        outfile = open("out.txt", 'w')
        args = ["R", "--no-restore", "--no-save"]
        proc = subprocess.Popen(args, stdin=infile, stdout=outfile, env=os.environ)
        proc.wait()

        # check for success
        if proc.returncode:
            print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
            sys.exit(proc.returncode)

    def _extractResults(self):
        import numpy
        QuantileTemperatureIncrease=numpy.empty([0,0])
        GlobalTemperatureIncrease=numpy.empty([0])
        Temperature=numpy.empty([0,0])
        Temperature_Variability=numpy.empty([0,0])

        if self.temperature:
            QuantileTemperatureIncrease=self._getData(os.path.join("emulator_output","warming_quantiles.out"))
            GlobalTemperatureIncrease=self._getData(os.path.join("emulator_output","global_warming.out"))
        if self.temperature_variability:
            Temperature=self._getAggregatedData("emulator_output","_temperature.out")
            Temperature_Variability=self._getAggregatedData("emulator_output","_temperature_variability.out")
        return QuantileTemperatureIncrease, GlobalTemperatureIncrease, Temperature, Temperature_Variability

    def _getData(self,fileName):
        return numpy.loadtxt(open(fileName,"rb"),delimiter=" ")

    def _getAggregatedData(self,fileNameStart,fileNameEnd):
        aggregatedData=None
        options=["DJF","MAM","JJA","SON"]
        for index,name in enumerate(options):
            fileName=os.path.join(fileNameStart,name+fileNameEnd)
            data=numpy.loadtxt(open(fileName,"rb"),delimiter=" ")
            if aggregatedData==None:
                aggregatedData=numpy.empty([data.size,len(options)])
            aggregatedData[:,index]=data
        return aggregatedData

