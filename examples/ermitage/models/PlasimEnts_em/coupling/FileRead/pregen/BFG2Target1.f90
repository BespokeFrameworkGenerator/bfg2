       module BFG2Target1
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !firstFixedIteration
       logical, dimension(10) :: firstFixedIteration
       !lastFixedIteration
       logical, dimension(10) :: lastFixedIteration
       !firstConvergeIteration
       logical, dimension(10) :: firstConvergeIteration
       !firstConvergeControl
       logical, dimension(10) :: firstConvergeControl
       !convergeCount
       integer, dimension(10) :: convergeCount
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer  , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       subroutine setLoopInfo(id,start,end,current)
       implicit none
       integer  , intent(in) :: id
       integer  , intent(in) :: start
       integer  , intent(in) :: end
       integer  , intent(in) :: current
       firstFixedIteration(id)=.false.
       if (current==start) then
       firstFixedIteration(id)=.true.
       end if
       lastFixedIteration(id)=.false.
       if (current==end) then
       lastFixedIteration(id)=.true.
       end if
       end subroutine setLoopInfo
       subroutine setConvergeInfo(id)
       implicit none
       integer  , intent(in) :: id
       firstConvergeIteration(id)=.false.
       if (convergeCount(id)==0) then
       firstConvergeIteration(id)=.true.
       convergeCount(id)=convergeCount(id)+1
       end if
       end subroutine setConvergeInfo
       subroutine setfirstConvergeControl(id)
       implicit none
       integer  , intent(in) :: id
       firstConvergeControl(id)=.true.
       end subroutine setfirstConvergeControl
       subroutine unsetfirstConvergeControl(id)
       implicit none
       integer  , intent(in) :: id
       firstConvergeControl(id)=.false.
       end subroutine unsetfirstConvergeControl
       ! in sequence support routines end
       ! concurrency support routines start
       logical function PlasimEntsModelThread()
       implicit none
       ! only one thread so always true
       PlasimEntsModelThread=.true.
       end function PlasimEntsModelThread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
