
program BFG2Main1
  use BFG2Target1
  
  implicit none
!
  


namelist /coef/ ChebCoefs



double precision :: ChebCoefs(3)

!
  call initComms()
!
  

open(unit=1012,file='ChebCoefs.nml')
read(1012,coef)

close(1012)




if (PlasimEntsModelThread()) then
  
call setActiveModel (1)






call PlasimEntsModel_run (ChebCoefs)






end if



!
  call finaliseComms()
!
end program BFG2Main1


