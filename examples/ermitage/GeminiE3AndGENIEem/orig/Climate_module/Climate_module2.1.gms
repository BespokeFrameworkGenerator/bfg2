* based on the excel sheet Climate Module Version 2.1.xls

set
T /1990*2061/
regT region/CHE,EUR,USA,OEC,BRI,ROW/
reg(regT)  region /CHE,EUR,USA,OEC,BRI,ROW/


alias(T,T1,T2)


Parameter

CS Climate sensitivity
Lag Lag parameter
A0_C02 CO2 response fucntion parameter
A1_C02 CO2 response fucntion parameter
A2_C02 CO2 response fucntion parameter
A3_C02 CO2 response fucntion parameter
Tau1_CO2 CO2 response fucntion parameter
Tau2_CO2 CO2 response fucntion parameter
Tau3_CO2 CO2 response fucntion parameter
CONSPRE_CO2 pre-industrial atmospherich CO2 concentration

Natural_N20  current natural emissions (in addition to anthropogenic emissions)
Natural_CH4  current natural emissions (in addition to anthropogenic emissions)
CoefCO2ppmv  convert giga tonne to ppmv
GWP_CH4 greenhouse warming potential CH4
LFT_CH4 lifetime CH4
CONSPRE_CH4 pre-industrial atmospherich CH4 concentration
GWP_N20 greenhouse warming potential CH4
LFT_N20 lifetime CH4
CONSPRE_N20 pre-industrial atmospherich N20 concentration


SP_CO2 CO2 scaling parameter
SP_CH4 CH4 scaling parameter
SP_N20 N2O scaling parameter
CH4N2OOVERP_1 CH4 N2O overlap parameter 1
CH4N2OOVERP_2 CH4 N2O overlap parameter 2
CH4N2OOVERP_3 CH4 N2O overlap parameter 3
CH4N2OOVERP_4 CH4 N2O overlap parameter 4
CH4N2OOVERP_5 CH4 N2O overlap parameter 5



CO2CONSBOX0(T) CO2 concentration box 0
CO2CONSBOX1(T) CO2 concentration box 1
CO2CONSBOX2(T) CO2 concentration box 2
CO2CONSBOX3(T) CO2 concentration box 3



CO2CONS(T) CO2 concentration
CH4CONS(T) CH4 concentration
N20CONS(T) N20 concentration
TOTALCONS(T) Total concentration
CO2RF(T)   Radiative forcing CO2
CH4RF(T)   Radiative forcing CH4
N20RF(T)   Radiative forcing N20
EXORF(T)   Exogenous radiative forcing

CO2REMAIN(T) remaining CO2
DECAY(T,T)

CO2_eff(T)

TEMPERATURE_Eq(T) Change in equilibrium temperature in Celcius
TEMPERATURE(T)  Change in actual temperature in Celcius
;

$include CO2sum_data.gms
$include CH4sum_data.gms
$include N2Osum_data.gms
$include Climate_module/Exosum_data.gms

CoefCO2ppmv=2.123;

CS=2;
Lag=57;
A0_C02=0.217;
A1_C02=0.259;
A2_C02=0.338;
A3_C02=0.186;
Tau1_CO2=172.9;
Tau2_CO2=18.51;
Tau3_CO2=1.186;
CONSPRE_CO2=280;

GWP_CH4=21;
LFT_CH4=12;
* LFT_CH4=10.9; TIAM value

CONSPRE_CH4=742.2;
Natural_CH4=199/2.78;
Natural_N20=11.0/4.81;


GWP_N20=310;
LFT_N20=114;
* LFT_N20=113.6;  TIAM value
CONSPRE_N20=272;

SP_CO2=5.35;
SP_CH4=0.036;
SP_N20=0.12;
CH4N2OOVERP_1=0.47;
CH4N2OOVERP_2=0.0000201;
CH4N2OOVERP_3=5.31E-15;
CH4N2OOVERP_4=0.75;
CH4N2OOVERP_5=1.52;


TEMPERATURE(T)=0;
TEMPERATURE("2001")=0.6;

CO2CONS("2000")=370.35;
CH4CONS("2001")=1773;
N20CONS("2001")=317;
* EXORF(T)=-0.473;

loop(T,
 if (ord(T) eq 12,N20CONS(T)=N20CONS("2001");
                  CH4CONS(T)=CH4CONS("2001");
                  CO2CONSBOX0("2001")=CONSPRE_CO2+0.387763482*(CO2CONS("2000")-CONSPRE_CO2);
                  CO2CONSBOX1("2001")=0.388083872*(CO2CONS("2000")-CONSPRE_CO2);
                  CO2CONSBOX2("2001")=0.210446866*(CO2CONS("2000")-CONSPRE_CO2);
                  CO2CONSBOX3("2001")=0.01370578*(CO2CONS("2000")-CONSPRE_CO2);
                  CO2CONS("2001")=CO2CONSBOX0("2001")+CO2CONSBOX1("2001")+CO2CONSBOX2("2001")+CO2CONSBOX3("2001");

    );
 if (ord(T) gt 12,
   CO2CONSBOX0(T)=CO2CONSBOX0(T-1)+A0_C02*CO2sum(T-1)/1000/CoefCO2ppmv;
   CO2CONSBOX1(T)=CO2CONSBOX1(T-1)*EXP(-1/Tau1_CO2)+A1_C02*CO2sum(T-1)/1000/CoefCO2ppmv;
   CO2CONSBOX2(T)=CO2CONSBOX2(T-1)*EXP(-1/Tau2_CO2)+A2_C02*CO2sum(T-1)/1000/CoefCO2ppmv;
   CO2CONSBOX3(T)=CO2CONSBOX3(T-1)*EXP(-1/Tau3_CO2)+A3_C02*CO2sum(T-1)/1000/CoefCO2ppmv;
   CO2CONS(T)=CO2CONSBOX0(T)+CO2CONSBOX1(T)+CO2CONSBOX2(T)+CO2CONSBOX3(T);
   N20CONS(T)=N20CONS(T-1)*(1-1/LFT_N20)+N20Sum(T-1)/1000/CoefCO2ppmv/GWP_N20*1000+Natural_N20;
   CH4CONS(T)=CH4CONS(T-1)*(1-1/LFT_CH4)+CH4Sum(T-1)/1000/CoefCO2ppmv/GWP_CH4*1000+Natural_CH4;
   TOTALCONS(T)=CO2CONS(T)+N20CONS(T)+CH4CONS(T)
    );
);

Loop(T,
if (ord(T) ge 12,
 CO2RF(T)=SP_CO2*LOG(CO2CONS(T)/CONSPRE_CO2);
 CH4RF(T)=SP_CH4*(CH4CONS(T)**(1/2)-CONSPRE_CH4**(1/2))-
  ( CH4N2OOVERP_1*LOG(1+CH4N2OOVERP_2*(CH4CONS(T)*CONSPRE_N20)**CH4N2OOVERP_4+CH4N2OOVERP_3*CH4CONS(T)*(CH4CONS(T)*CONSPRE_N20)**CH4N2OOVERP_5)
    -CH4N2OOVERP_1*LOG(1+CH4N2OOVERP_2*(CONSPRE_CH4*CONSPRE_N20)**CH4N2OOVERP_4+CH4N2OOVERP_3*CONSPRE_CH4*(CONSPRE_CH4*CONSPRE_N20)**CH4N2OOVERP_5)) ;
 N20RF(T)=SP_N20*(N20CONS(T)**(1/2)-CONSPRE_N20**(1/2))-
  ( CH4N2OOVERP_1*LOG(1+CH4N2OOVERP_2*(CONSPRE_CH4*N20CONS(T))**CH4N2OOVERP_4+CH4N2OOVERP_3*CONSPRE_CH4*(CONSPRE_CH4*N20CONS(T))**CH4N2OOVERP_5)-
    CH4N2OOVERP_1*LOG(1+CH4N2OOVERP_2*(CONSPRE_CH4*CONSPRE_N20)**CH4N2OOVERP_4+CH4N2OOVERP_3*CONSPRE_CH4*(CONSPRE_CH4*CONSPRE_N20)**CH4N2OOVERP_5));
 TEMPERATURE_Eq(T)=CS/(SP_CO2*LOG(2))*(CO2RF(T)+CH4RF(T)+N20RF(T)+EXORF(T));
 if (ord(T) gt 12, TEMPERATURE(T)=TEMPERATURE(T-1)+(TEMPERATURE_Eq(T)-TEMPERATURE(T-1))/Lag;);

);
* CO2_eff(T)= CONSPRE_CO2 * 2**((CO2RF(T)+CH4RF(T)+N20RF(T)+EXORF(T)) / 3.71);
CO2_eff(T)= CONSPRE_CO2 * exp((CO2RF(T)+CH4RF(T)+N20RF(T)+EXORF(T)) / SP_CO2);
);

display temperature;
display CO2_eff;


fILE varref variableref /concent-gemini.txt/;


put varref;

loop(T,if(ord(T) gt 15, if(ord(T) lt 62,
put T.tl;put @5 ';';put @6,CO2_eff(T); put/;  ))
   );