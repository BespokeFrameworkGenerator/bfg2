set
 var1(varmac) liste1 /GDPZ,IMPZ,CONZ,GCVZ,INVZ,EXPZ/
 var2(varmac) liste2 /GDPV,IMPV,CONV,GCVV,INVV,EXPV/
 var3(varmac) liste3 /PGDP,PIMP,PCON,PGCV,PINV,PEXP/
 var4(varmac) liste4 /RLTL,TSAV,CO2H,BALC,SAVG,SAVH,SAVW/
 var5(varsec) listedesvolumes /YD,XD,LAV,KAV,FF,INVV,EXP,IMP,EN,HCV,CO2/
 var6(varsec) listedesprix /PY,PB,PD,PK,PF,PINV,PEX,PIMP,PE,PHC,ELAR,ELAP,INCO/
 var7(varmac) liste7 /PGDP,SUR1,SPRI,TAXE,QUOT/
 var8(varmac) liste8 /HENC,HENG,HENO,HENP,HENE/
 var9(varsec) liste9 /ENEC,ENEG,ENEO,ENEP,ENEE/

 var10(varmac) liste10 /CLAN,CBIC,CCOA,CENT,CFUE,CIND,COIL,CMAN,CRIC,COAG,CWAS,COTH,CGAS/
 var11(varmac) liste11 /NAGS,NOAG,NBIC,NFUE,NMAN,NOTH,NOIN,NADI,NNIT,NHUM/
 var12(varmac) liste12 /FAEN,FFIR,FFOA,FSOL,FAEM,FREF,FHFC,FEPS,FPAP,FSEM,FMAM/
 TTout(T) Timeoutput   /2001,2010,2015,2020,2025,2030,2035,2040,2045,2050/


;
scalar
 col,col1,col2,col3  numero de colonne
 idist distance entre 2 nombres
 Xsum  variable de sum
;






idist=14;


fILE Result resultat /resulvar.txt/;
result.nd=10

put result;

put system.date,' ',system.time;put /;

col=0;loop (TTout, col=col+idist;put @col,TTout.tl;); put /;


* Ecriture des donnees Macro en volume

loop (regT,

   loop (var1,
     col=0;
        put var1.tl,@5,'_',@6,regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var1,TTout,regT);

              );
        put /;
        );
       );

* Ecriture des donnees Macro en valeur

loop (regT,

   loop (var2,
     col=0;
        put var2.tl,@5,'_',@6,regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var2,TTout,regT);

              );
        put /;
        );
       );

* Ecriture des donnees Macro en prix

loop (regT,

   loop (var3,
     col=0;
        put var3.tl,@5,'_',@6,regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var3,TTout,regT);

              );
        put /;
        );
       );

* Ecriture des taux de change

loop (reg,
      col=0;
        put 'EXCH',@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC("EXCH",TTout,reg);

              );
        put /;
      );

loop (reg,
      col=0;
        put 'EXCR',@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,((SORTIEMAC("EXCH",TTout,reg)/SORTIEMAC("EXCH",TTout,"OEC"))*(SORTIEMAC("PGDP",TTout,reg)/SORTIEMAC("PGDP",TTout,"OEC")));
              );
        put /;
      );

* Ecriture des taux d'interet et taux d'epargne

loop (var4,

   loop (reg,
     col=0;
        put var4.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var4,TTout,reg);

              );
        put /;
        );
       );


loop (var5,

   loop (regT,
     loop(sec,
     col=0;col1=ncol(var5)+1;col2=col1+1+2;col3=col2;
        put var5.tl,@col1,'_',sec.tl,@col2,'_',regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIESEC(var5,TTout,sec,regT);
        );
         put /;
      );
      put var5.tl,@col1,'_sm',@col3,'_',regT.tl;
      col=0;
        loop (TTout,
          col=col+idist;Xsum=sum(sec,SORTIESEC(var5,TTout,sec,regT));
          put @col,Xsum;
        );
         put /;
     );
);

loop (Var6,

   loop (regT,
     loop(sec,
     col=0;col1=ncol(var6)+1;col2=col1+1+2;col3=col2+1;
        put var6.tl,@col1,'_',sec.tl,@col2,'_',regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIESEC(var6,TTout,sec,regT);
        );
         put /;
      );
     );
);


loop (reg,

   loop (var10,
     col=0;
        put var10.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var10,TTout,reg);

              );
        put /;
        );
       );


loop (reg,

   loop (var11,
     col=0;
        put var11.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var11,TTout,reg);

              );
        put /;
        );
       );

loop (reg,

   loop (var12,
     col=0;
        put var12.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var12,TTout,reg);

              );
        put /;
        );
       );



result.nd=1;idist=5;
  col=8;loop(reg,col=col+idist; put @col,reg.tl;);put/;
  loop (sec,col=0;put 'SIGMAX';put @7,'_',@8,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAX(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMAI';put @7,'_',@8,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAI(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMA';put @6,'_',@7,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMA(sec,reg):>4:1; ); put /;);
  loop (secff,col=0;put 'SIGMAPF';put @8,'_',@9,secff.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAPF(secff,reg):>4:1; ); put /;);
  col=0;put 'SIGMAPP';put @7,'_04';col=8;loop(reg, col=col+idist; put @col,SIGMAE("04",reg):>4:1; ); put /;
  loop (sec,col=0;put 'SIGMAE';put @7,'_',@8,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAE(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMAEF';put @8,'_',@9,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAEF(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMAM';put @7,'_',@8,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAM(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMAMM';put @8,'_',@9,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMAMM(sec,reg):>4:1; ); put /;);
  loop (sec,col=0;put 'SIGMATRA';put @9,'_',@10,sec.tl;col=8;loop(reg, col=col+idist; put @col,SIGMATRA(sec,reg):>4:1; ); put /;);

* Ecriture des donnees annuelles pour calcul de surplus



idist=14;
col=0;loop (TTout, col=col+idist;put @col,TTout.tl;); put /;
result.nd=15;

loop (reg,

   loop (var7,
     col=0;
        put var7.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var7,TTout,reg);

              );
        put /;
        );
       );

loop (reg,
      col=0;
        put 'PERM',@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC("PERM",TTout,reg);

              );
        put /;
      );

loop (reg,
     loop(sec,
     col=0;col2=8;
        put 'QUOTAS_',sec.tl,@col2,'_',reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIESEC("QUOT",TTout,sec,reg);
        );
         put /;
      );
     );


idist=14;
col=0;loop (TTout, col=col+idist;put @col,TTout.tl;); put /;
result.nd=10;

loop (reg,

   loop (var7,
     col=0;
        put var7.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var7,TTout,reg);

              );
        put /;
        );
       );



loop (reg,
     col=0;
        put "ENER",@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC("ENER",TTout,reg);

              );
        put /;

       );


loop (reg,

   loop (var8,
     col=0;
        put var8.tl,@5,'_',@6,reg.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIEMAC(var8,TTout,reg);

              );
        put /;
        );
       );

loop (var9,
    loop (regT,
     loop(sec,
     col=0;
        put var9.tl,@5,'_',@6,sec.tl,@8,'_',@9,regT.tl;
        loop (TTout,
          col=col+idist;
          put @col,SORTIESEC(var9,TTout,sec,regT);
        );
         put /;
      );
      put var9.tl,@5,'_sm',@8,'_',@9,regT.tl;
      col=0;
        loop (TTout,
          col=col+idist;Xsum=sum(sec,SORTIESEC(var9,TTout,sec,regT));
          put @col,Xsum;
        );
         put /;
     );
);




putclose Result;