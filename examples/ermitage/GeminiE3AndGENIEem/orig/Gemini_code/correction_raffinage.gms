

parameter

prixraf(sece,reg)

;

prixraf(sece,reg)=(IOV0(sece,"04",reg)/IOENER0(sece,"04",reg))$(IOENER0(sece,"04",reg) ne 0);

* we assume that refined petroleum sector does not consume coal

IOV0("01","04",reg)=0;IOENER0("01","04",reg)=0;

* we assume that refined petroleum sector does not consumec natural gas

IOV0("03","04",reg)=0;IOENER0("03","04",reg)=0;


* we correc oil consumption by refined petroleum

IOV0("04","04",reg)=IOENER0("02","04",reg)*Coefref(reg)*prixraf("04",reg);

IOENER0("04","04",reg)=IOENER0("02","04",reg)*Coefref(reg);