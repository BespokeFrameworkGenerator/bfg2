* GEMINI-E3 MODEL VERSION 5 SEPTEMBER 2006  V1.0

$ONTEXT
*Sector and product classification


01 Coal
02 Oil
03 Gas
04 Petroleum Products
05 Electricity
06 Agriculture
07 Forestry
08 Mineral Products
09 Chemical, rubber, Plastic
10 Metal and Metal products
11 Paper products publishing
12 Transport nec
13 Sea Transport
14 Air Transport
15 Consuming goods
16 Equipment goods
17 Services
18 Dwelings


$OFFTEXT

$sysinclude gams-f
$exit


SET

ELEC Electricity generation type /COA,OIL,GAS,NUC,REN,HYD,FOS/
regT region/OEC,EUR,BRI,ROW/
pri primary factor /K,L,F/
Ag  agent /H,G,W/
tt  type of taxe /DT,IT,DUT,VATIO,VATINV,VATST,VATHC,VATGC/
T /1990*2051/
itw indirect taxes on labor /SS/
Ghg Non CO2 GHG /CH4_LAN,CH4_BIC,CH4_COA,CH4_ENT,CH4_FUE,CH4_IND,CH4_OIL,CH4_GAS,CH4_MAN,CH4_RIC,
                 CH4_OAG,CH4_WAS,CH4_OTH,N2O_AGS,N2O_OAG,N2O_BIC,N2O_FUE,N2O_MAN,N2O_OTH,N2O_OIN
                 N2O_HUM,PFC_AEM,PFC_AEN,PFC_FIR,PFC_FOA,PFC_SOL,HFC_22,SF6_EPS,PFC_PAP,
                 PFC_SEM,SF6_MAM,PFC_REF,N2O_ADI,N2O_NIT,CH4_AGR,N2O_AGR/
Ch4(ghg) CH4 gas /CH4_LAN,CH4_BIC,CH4_COA,CH4_ENT,CH4_FUE,CH4_IND,CH4_OIL,CH4_GAS,CH4_MAN,CH4_RIC,
                 CH4_OAG,CH4_WAS,CH4_OTH/
N2O(ghg) N20 gas /N2O_OAG,N2O_BIC,N2O_FUE,N2O_MAN,N2O_OTH,N2O_OIN,N2O_HUM,N2O_ADI,N2O_NIT/
Fluo(ghg) fluor. gas   /PFC_AEM,PFC_AEN,PFC_FIR,PFC_FOA,PFC_SOL,HFC_22,SF6_EPS,PFC_PAP,
                 PFC_SEM,SF6_MAM,PFC_REF/

Sec sector /01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18/

reg(regT)  region /OEC,EUR,BRI,ROW/
regeu(reg)  region-EU /EUR/
regeuT(reg) region-Europe /EUR/
regmeu(reg) region-EU /OEC,BRI,ROW/
regpvd(reg) Pvd /BRI,ROW/
regpind(reg) pays indus/OEC,EUR/


Sece(sec)   energy sector /01,02,03,04,05/
Secef(sece) Fossil Fuel product /01,02,03,04/
Secff(sece)  Fossil Fuel Sector including fix factor /01,02,03/
Secmp(sece)  Fossil Fuel Sector excluding 02 /01,03,04/
Secmr(sece)  Fossil Fuel Sector excluding 04 /01,02,03,04/
Secnf(sec)  Sector excluding fossil fuel sector /06,07,08,09,10,11,12,13,14,15,16,17,18/
Secrp(sec)  Refined petroleum /04/
Secm(sec)   material sector /06,07,08,09,10,11,12,13,14,15,16,17,18/
sectra(sec)   transport sector /12,13,14/
secotra(sec)  material sector minus transport sector /06,07,08,09,10,11,15,16,17,18/


IIN intervalle/10*22/

alias(pro,sec,sp);
alias(reg,rege,regf,regh,regi);
alias(regmeu,regmeu2);
alias(regeu,regeu2);
alias(ag,ag2);
alias(secmp,secmp2);


scalar
stop
iter
iterfin
sumdiff
iterm
crit
;


SET

 VARMAC/GDPZ,IMPZ,CONZ,GCVZ,INVZ,STVZ,EXPZ,GDPV,IMPV,CONV,GCVV,INVV,STVV,EXPV,
         PGDP,PIMP,PCON,PGCV,PINV,PSTV,PEXP,EXCH,RLTL,TSAV,CO2H,BALC,SAVG,SAVH,SAVW,SUR1,ENER,
         HENC,HENG,HENO,HENP,HENE,
         CLAN,CBIC,CCOA,CENT,CFUE,CIND,COIL,CMAN,CRIC,COAG,CWAS,COTH,CGAS,
         NAGS,NOAG,NBIC,NFUE,NMAN,NOTH,NOIN,NADI,NNIT,NHUM,
         FAEN,FFIR,FFOA,FSOL,FAEM,FREF,FHFC,FEPS,FPAP,FSEM,FMAM,SPRI,TAXE,PERM,QUOT
      /
 VARSEC/YD,PY,PB,XD,PD,LAV,KAV,FF,PK,PF,INVV,PINV,EXP,PEX,IMP,PIMP,EN,PE,HCV,PHC,CO2,ELAR,ELAP,INCO,
       ENEC,ENEG,ENEO,ENEP,ENEE,QUOT/
 ;
parameter ncol(Varsec)
/ YD 2,PY 2, PB 2,XD 2,PD 2,LAV 3,KAV 3,FF 2,PK 2,INVV 4,EXP 3,IMP 3,PIMP 4,EN 2,PE 2,HCV 3,PHC 3,CO2 2
  PF 2,PINV 4,PEX 3,ELAR 4,ELAP 4,INCO 4/



parameter
FABAT(iin,ghg,reg)      function of abatement
CO2QW               CO2 constraint
SHARE(reg)          repartition of CollecttaxeW
PENERGY(sece,sec,reg)

OILPRICE(T,reg)
COALPRICE(T,reg)
GASPRICE(T,reg)

POIL(reg)           exogenous price of oil
PGAS(reg)           exogenous price of natural gas
PCOAL(reg)          exogenous price of coal

IORD(iin)
SORTIEMAC(varmac,T,regT)
SORTIESEC(varsec,T,sec,regT)
diff(reg)

TAXECO2MT(T,reg)
TAXECO2M(reg)
ACXD(ghg,sec,reg)   Coefficient for ghg emission
ACXDPF(ghg,sec,reg) Coefficient for ghg emission
ACYD(ghg,sec,reg)   Coefficient for ghg emission
ACHCVT(ghg,sec,reg) Coefficient for ghg emission
IA(reg)
IB(reg)
FA(ghg,reg)         function of abatement  value A
FB(ghg,reg)         function of abatement  value B


ACXDT(T,ghg,sec,reg)   Coefficient for ghg emission
ACXDPFT(T,ghg,sec,reg) Coefficient for ghg emission
ACYDT(T,ghg,sec,reg)   Coefficient for ghg emission
ACHCVTT(T,ghg,sec,reg)     Coefficient for ghg emission

ABAT(ghg,sec,reg)      level of abatement for ghg emission
COUTUABAT(ghg,sec,reg)  unit cost of abatement
SAVECC_G(reg)           constraint of government saving
INVCC(reg)              investment by sector
FFS(secff,reg)          Supply of fix factor



TINV(pro,sec,reg)  Coefficient de passage investissement produit x secteur

APIMP(sec,reg)     Lambda parameter of CES import
CIMP(sec,rege,reg) Alpha  parameter of CES import
SIGMAI(sec,reg)    elasticity of CES  import


APY(sec,reg)       Lambda parameter of CES domestic prod and import
CX(sec,reg)        Alpha  parameter of CES domestic prod and import
SIGMAX(sec,reg)    elasticity of CES domestic prod and import

APPF(sec,reg)    Lambda parameter of CES domestic prod and fix factor
CPF(sec,reg)     Alpha parameter of CES domestic prod and fix factor
SIGMAPF(sec,reg) elasticity of CES domestic prod and fix factor

APPP(Sec,reg)    Lambda parameter of CES domestic prod and crude oil
CPP(Sec,reg)     Alpha parameter of CES domestic prod and crude oil
SIGMAPP(Sec,reg) elasticity of CES domestic prod and crude oil

APD(sec,reg)       Lambda parameter of CES domestic prod
DK(sec,reg)        Alpha parameter of CES domestic prod
DE(sec,reg)        Alpha parameter of CES domestic prod
DL(sec,reg)        Alpha parameter of CES domestic prod
DM(sec,reg)        Alpha parameter of CES domestic prod
SIGMA(sec,reg)     Elasticity parameter of CES domestic prod

TGK(sec,reg)       Technical progress on capital
TGKA(sec,reg)      anticipated Technical progress on capital
TGE(sec,reg)       Technical progress on energy
TGL(sec,reg)       Technical progress on labor
TGC(reg)                Impact of CC on productivity
TGM(sec,reg)       Technical progress on material
TGTRA(sec,reg)     Technical progress of transport
TGOTRA(sec,reg)    Technical progress of material minus transport


APE(sec,reg)       Lambda parameter of CES energy
DEPR(pro,sec,reg) Alpha parameter of CES energy
DEPR0(pro,sec,reg) Alpha parameter of CES energy
SIGMAE(sec,reg)    Elasticity parameter of CES energy

GEPR(pro,sec,reg) Technical progress on individual energy
TGENF(sec,reg)     Technical progress on fossil fuel energy

APEF(sec,reg)      Lambda parameter of CES fossil fuel energy
SIGMAEF(sec,reg)   Elasticity of CES fossil fuel energy

APMM(sec,reg)      Lambda parameter of  material
CMM(sec,reg)       Alpha parameter of CES  material
SIGMAMM(sec,reg)   Elasticity parameter of CES  material

APTRA(sec,reg)      Lambda parameter of  transport
SIGMATRA(sec,reg)   Elasticity parameter of CES  transport

GMPR(pro,sec,reg) Technical progress on individual material
DMPR(pro,sec,reg) Alpha parameter of CES individual material

APM(sec,reg)       Lambda parameter of CES material minus transport
SIGMAM(sec,reg)    Elasticity of CES material minus transport

CIMPMTRA(sectra,sec,rege,reg) Coefficient share of Transport margin of Import
TIMPMTRA(sectra,sec,reg,rege,regf) transfert matrix between transport margin of import and transport margin export

TXEXCISE(sec,reg)    excise rate on household consumption
TXEXCISE0(sec,reg)   excise rate on household consumption
TXTVAIO(pro,sec,reg) tax on intermediaite consumption
TXTVAGC(sec,reg)     Tax on government consumption
TXTVAIN(pro,sec,reg) tax on investment
TXTVAHC(sec,reg)     tax on household consumption
TXTVAST(sec,reg)     tax on stock variation
TXTVAEX(sec,reg)     Tax on export
TXIT(sec,reg)        indirect taxes rate
TXW(itw,sec,reg)     employers social contribution

TXDUT(sec,reg,rege)  rate of duties
DTXTVAHC(sec,reg)    link between carbon tax and incremental value of price
DTXTVAIO(pro,sec,reg) link between carbon tax and incremental value of price

TXFSESE_H_G(reg)   rate of transfert between agent
TXFSESE_W_G(reg)   rate of transfert between agent

CHCV(sec,reg)      incompresible consumption
BHCV(sec,reg)      parameter of household consumption
BETA(sec,reg)      decrease paramet of incompresible consumption

TGCV(sec,reg)      parameter of government consumption
ALPD(sec,reg)      paramater
ALKAVA(sec,reg)    paramater
ERRCK(sec,reg)     dynamic adjustment factor
DECL(sec,reg)      rate of decay
ERRINVV(sec,reg)   dynamic adjustment factor

CTEP(sece,sec,reg) Link between intermediaite consumption and Carbon emission
CTEPH(sece,reg)    Link between Household consumption and Carbon emission
CO2CONTENT(secef)   Carbon content

PEXB(sec,reg)          Price of exports for base year
PHCB(sec,reg)          Price of consumption for base year
PINVB(sec,reg)         price of investment for base year
PBB(sec,reg)           Base price for base year
PIMPB(sec,reg)         Price of import for base year
PGCB(sec,reg)          Price of government consumption for base year
EC(reg)                deficit of trade balance

XDA(sec,reg)            anticipated Domestic production
XDM1(sec,reg)           Domestic production t-1
XDM2(sec,reg)           Domestic production t-2
KAVC(sec,reg)           fix capital
KAVCM1(sec,reg)         fix capital t-1
INVVM1(sec,reg)         investment by sector
GCVTOT(reg)             Total government consumption
STV(sec,reg)            Stock variation
CO2Q(reg)               CO2 limit

CO2_TOTAL(T)
N2O_TOTAL(T)
CH4_TOTAL(T)
;
Variable
FGRS_GHG_G(reg)         Taxes
PFES(secff,reg)         price of fix factor
GCV(sec,reg)            government consumption

HCDTOT(reg)             Total household consumption
INVPVP(pro,reg)         investment in product
INVV(sec,reg)           investment by sector
YD(sec,reg)             Production
IOV(sec,pro,reg)        Intermediate consumption
HCV(sec,reg)            Household consumption

EXPOP(sec,reg)          Export
IMPTRA(sec)          World Transport
IMPO(sec,reg)           Import including transport margin
IMPOR(sec,rege,reg)     import from country rege to country reg
XD(sec,reg)             Domestic production

XDPP(sec,reg)               Domestif production for petroleum product
XDPF(sec,reg)           Domestif production for fossil fuel
KAV(sec,reg)            fix capital

KAVO(sec,reg)           optimal capital
KAVA(sec,reg)           anticipated capital
LAV(sec,reg)            Labor
EN(sec,reg)             Energy
MA(sec,reg)             Material
ENF(sec,reg)            Fossil fuel energy
TRA(sec,reg)            Transport
OTRA(sec,reg)           Intermediaite minus transport
FF(secff,reg)           Fix factor
NSUP(reg)               supply of labor
CO2(reg)                CO2 Emissions

PPIBM1(reg)             GDP price t-1
PY(sec,reg)             Price of production
PD(sec,reg)             Price of domestic production
PIMP(sec,reg)           Price of import
PIMPR(sec,rege,reg)     Price of import for country reg coming from contry rege
PDPF(sec,reg)           Price of domestic production for fossil fuel
PDPP(sec,reg)           Price of domestic production for petroleum product
PK(sec,reg)             Rental price of capital
PL(sec,reg)             Price of labor
PM(sec,reg)             Price of material
PE(sec,reg)             Price of energy
PB(sec,reg)             Base price
PEF(sec,reg)            Price of fossil fuel energy
PTRA(sec,reg)           Price of transport
POTRA(sec,reg)          Price of intermediaite  minus transport
PIO(pro,sec,reg)        Price of intermediaite consumption
PM(sec,reg)             Price of material
PHC(sec,reg)            Price of consumption
PDR(sec,reg)            price of ratio PD on PINV
PDRA(sec,reg)           anticipated price of PDR
PDRM1(sec,reg)          anticipated price of PDR t-1
PDRM2(sec,reg)          anticipated price of PDR t-2
PINV(sec,reg)           price of investment
RLTL(reg)               interest rate
PGC(sec,reg)            Price of government consumption
PEX(sec,reg)            Price of exports
W(reg)                  wage
PF(secff,reg)           price of fix factor



FSEFA_H_K(reg)          income coming from primary factor
FSEFA_H_L(reg)          income coming from primary factor
FSEFA_H_F(reg)          income coming from primary factor
FSEFA_G_L(reg)          income coming from primary factor
FSESE_H_G(reg)          transfert between agent
FGRS_DT_H(reg)          taxes
FGRS_IT_G(reg)          taxes
FGRS_DUT_G(reg)         taxes
FGRS_VATHC_G(reg)       taxes
FGRS_VATIO_G(reg)       taxes
FGRS_VATST_G(reg)       taxes
FGRS_VATGC_G(reg)       taxes
FGRS_VATINV_G(reg)      taxes
SAVE_H(reg)             saving
SAVE_G(reg)             saving
TSAVE(reg)              rate of saving
TAXECO2(reg)            Carbon tax
QUOTANET(reg)           net supply of permits
EX(reg)                 exchange rate

EGHG(ghg,sec,reg)       non Carbon GHG emission
COUTABAT(ghg,sec,reg)   cost of abatement for non carbon GHG emission
TAXEGHG(sec,reg)        taxes on non carbon GHG

PIBVA(reg)              PIB en valeur
PIBVO(reg)              PIB en volume

TXFGRS_H(reg)           rate of direct tax
TAXECO2W                World CO2 tax


Positive Variable
PY,PD,PIMP,PDPF,PDPP,PK,PL,PM,PE,PB,PEF,PIO,PM,PHC,PDR,PDRA,PDRM1,PDRM2,
PINV,RLTL,PGC,PEX,W,PF,
HCDTOT,INVPVP,INVV,YD,IOV,HCV,EXPOP,EXPO,IMPO,
XD,XDPP,XDPF,KAVO,KAVA,LAV,EN,MA,ENF,
FF,NSUP,CO2,TAXECO2W
;



Equation
EQUOTANET(reg)
EQUOTANETB(reg)
EPK(sec,reg)             PK
EPDPFES(secff,reg)
EPIBVA(reg)              PIB en valeur
EPIBVO(reg)              PIB en volume
EPIMP(sec,reg)           Price of import
EPIMPR(sec,rege,reg)     Price of import for region reg coming from region rege
EINVPVP(pro,reg)         Investment in product
EEXPOP(sec,reg)          Export
EYD(sec,reg)             Production
EPY(sec,reg)             Price of production
EXD(sec,reg)             Domestic production
EFF(secff,reg)           Demand of fix factor
EFF2(secff,reg)          Demand of fix factor
EIMPO(sec,reg)           Import
EXDPF(secff,reg)         Domestif production for fossil fuel
EPDPF(secff,reg)         Price of domestic production for fossil fuel
EPDPF2(secff,reg)        Price of domestic production for fossil fuel
EXDPP(secrp,reg)         Domestif production for petroleum product
EPDPP(secrp,reg)         Price of petroleum product
EPD(sec,reg)             Price of domestic production
EKAV(sec,reg)            Capital
ELAV(sec,reg)            Labor
EEN(sec,reg)             Energy
EMA(sec,reg)             Material
EPE(sec,reg)             Price of energy
EPEF(sec,reg)            Price of fossil energy
EENF(sec,reg)            Fossil fuel energy
EIOV(pro,sec,reg)        Intermediaite conusmption
ETRA(sec,reg)            Transport
EOTRA(sec,reg)           Material minus transport
EPTRA(sec,reg)           Price of transport
EPOTRA(sec,reg)          Price of material minus transport
EPM(sec,reg)             Price of material
EPDR(sec,reg)            price of ratio PD on PINV
EPDRA(sec,reg)           anticipated price of PDR
EKAVO(sec,reg)           optimal capital
EKAVA(sec,reg)           anticipated capital
EINVV(sec,reg)           investment by sector
EHCV(sec,reg)            Household consumption
EHCDTOT(reg)             Total household consumption
EPB(sec,reg)             Base price
EPHC(sec,reg)            Price of consumption
EPGC(sec,reg)            Price of government consumption
EPINV(sec,reg)           price of investment
EPEX(sec,reg)            price of exports
EPL(sec,reg)             price of labor
EFSEFA1(reg)             remuneration of labor
EFSEFA2(reg)             remuneration of capital
EFSEFA3(reg)             remuneration of fix factor
EFSEFA4(reg)             remuneration of labor to G
EFGRS1(reg)              Direct tax on household
EFSEFE1(reg)             transfert between government and households
EFSEFE2(reg)             transfert between government and ROW
EFGRS2(reg)              Tax
EFGRS3(reg)              duties
EFGRS4(reg)              VAT on households
EFGRS5(reg)              VAT on intermediaite consumption
EFGRS6(reg)              VAT on stock variation
EFGRS7(reg)              VAT on investment
EFGRS8(reg)              VAT on Government
EFGRS9(reg)              Taxes on non carbon GHG
EFGRS9B(reg)             Taxes on non carbon GHG
ETAXEGHG(sec,reg)        Taxes on non carbon GHG
ETAXEGHGB(sec,reg)        Taxes on non carbon GHG

ESAVEG(reg)              Government saving
ESAVEH(reg)              Household saving
EIMPOR(sec,rege,reg)     import from country rege to country reg including transport margins
*EIMPMTRA(sectra,sec,reg,reg)  Transport margin og import
MRKL(reg)                market clearing of labor
MRKK(sec,reg)            market clearing of capital
MRKF(secff,reg)          market clearing of fix factor
MRCOM(reg)               market clearing exchange rate
EPIO(pro,sec,reg)        Price of intermediaite consumption
ECO2(reg)                CO2 emission
EPF(secff,reg)           Price of fix factor in reference simulation
EIMPTRA(sectra)          Word demand in transport for international trade
MRINV(reg)               market clearing total investment
MRSAVEG(reg)             market clearing govenment budget
ETAXECO2B(reg)           TAXECO2
ETAXECO2(reg)            TAXECO2
ETXFGRSPRED_H(reg)       TXFGRS_H.L
ETSAVE(reg)              TSAVE(reg)
EEGHG(ghg,sec,reg)       non Carbon GHG emission
EGCV(sec,reg)            Government consumption
ECOUTABATB(ghg,sec,reg)  cost of abatement for non carbon GHG
ECOUTABAT(ghg,sec,reg)   cost of abatement for non carbon GHG
MRCO2                    CO2 constraint

;

IORD("10")=0;IORD("11")=15;IORD("12")=30;IORD("13")=45;IORD("14")=60;
IORD("15")=75;IORD("16")=90;IORD("17")=105;IORD("18")=120;IORD("19")=135;
IORD("20")=150;IORD("21")=165;IORD("22")=180;

$include Gemini_code/energyprice.gms

$include Gemini_code/calib.gms

$include Gemini_code/datarefcclt.gms


* we estimate 2001 data = 2000 year
GHGR(reg,"2001",GHG)=GHGR(reg,"2000",GHG);
* we convert CO2 into Carbone
GHGR(reg,T,GHG)=GHGR(reg,T,GHG)/(44/12);


EPIBVA(reg)..       PIBVA(reg) =E=   (sum(sec,EXPOP(sec,reg)*PEX(sec,reg)+PHC(sec,reg)*HCV(sec,reg)+PGC(sec,reg)*GCV(sec,reg)
                             +INVV(sec,reg)*PINV(sec,reg)+STV(sec,reg)*PB(sec,reg)*(1+TXTVAST(sec,reg))-
                             IMPO(sec,reg)*PIMP(sec,reg)));


EPIBVO(reg)..       PIBVO(reg) =E=  (sum(sec,EXPOP(sec,reg)*PEXB(sec,reg)+PHCB(sec,reg)*HCV(sec,reg)+PGCB(sec,reg)*GCV(sec,reg)
                                     +INVV(sec,reg)*PINVB(sec,reg)+STV(sec,reg)*PBB(sec,reg)*(1+TXTVAST(sec,reg))-
                                     IMPO(sec,reg)*PIMPB(sec,reg)));


EPF(secff,reg)..      PF(secff,reg) =E= PFES(secff,reg);

EINVPVP(pro,reg)$(INVPVP0(pro,reg) ne 0)..  INVPVP(pro,reg) =E= sum(sec,TINV(pro,sec,reg)*INVV(sec,reg));


EYD(sec,reg)..      YD(sec,reg) =E= sum(pro,IOV(sec,pro,reg))+HCV(sec,reg)+
                    GCV(sec,reg)+EXPOP(sec,reg)+INVPVP(sec,reg)+STV(sec,reg);


EPY(sec,reg)..       PY(sec,reg)=E= (APY(sec,reg)*(CX(sec,reg)*PD(sec,reg)**(1-SIGMAX(sec,reg))
                    +(1-CX(sec,reg))*PIMP(sec,reg)**(1-SIGMAX(sec,reg)))**(1/(1-SIGMAX(sec,reg))))
                     $(ord(sec) gt 4)+
                     (APY(sec,reg)*(CX(sec,reg)*PDPF(sec,reg)**(1-SIGMAX(sec,reg))
                    +(1-CX(sec,reg))*PIMP(sec,reg)**(1-SIGMAX(sec,reg)))**(1/(1-SIGMAX(sec,reg))))
                     $(ord(sec) le 3)+
                     (APY(sec,reg)*(CX(sec,reg)*PDPP(sec,reg)**(1-SIGMAX(sec,reg))
                    +(1-CX(sec,reg))*PIMP(sec,reg)**(1-SIGMAX(sec,reg)))**(1/(1-SIGMAX(sec,reg))))
                     $(ord(sec) eq 4);


EXD(sec,reg)..      XD(sec,reg) =E= ((YD(sec,reg)*APY(sec,reg)*CX(sec,reg)*
                                    (PY(sec,reg)/(APY(sec,reg)*PD(sec,reg)))**SIGMAX(sec,reg))
                                     $(ord(sec) gt 4)+
                                    (XDPF(sec,reg)*APPF(sec,reg)*CPF(sec,reg)*
                                    (PDPF(sec,reg)/(APPF(sec,reg)*PD(sec,reg)))**SIGMAPF(sec,reg))
                                    $(ord(sec) le 3)+
                                    (XDPP(sec,reg)*APPP(sec,reg)*CPP(sec,reg)*
                                    (PDPP(sec,reg)/(APPP(sec,reg)*PD(sec,reg)))**SIGMAPP(sec,reg))
                                    $(ord(sec) eq 4))$(XD0(sec,reg) ne 0)+0;;



EXDPP(secrp,reg)..         XDPP(secrp,reg) =E= (YD(secrp,reg)*APY(secrp,reg)*CX(secrp,reg)*
                         (PY(secrp,reg)/(APY(secrp,reg)*PDPP(secrp,reg)))**SIGMAX(secrp,reg));




EPDPP(secrp,reg)..         PDPP(secrp,reg) =E= (APPP(secrp,reg)*(CPP(secrp,reg)*PD(secrp,reg)**(1-SIGMAPP(secrp,reg))
                       +(1-CPP(secrp,reg))*PIO("02",secrp,reg)**(1-SIGMAPP(secrp,reg)))**(1/(1-SIGMAPP(secrp,reg))));


EXDPF(secff,reg)..   XDPF(secff,reg) =E= YD(secff,reg)*APY(secff,reg)*CX(secff,reg)*
                    (PY(secff,reg)/(APY(secff,reg)*PDPF(secff,reg)))**SIGMAX(secff,reg);



EPDPF(secff,reg)..   PDPF(secff,reg) =E=PCOAL(reg)$(ord(secff) eq 1)+POIL(reg)$(ord(secff) eq 2)+PGAS(reg)$(ord(secff) eq 3);

EPDPF2(secff,reg)..   PDPF(secff,reg) =E= (APPF(secff,reg)*(CPF(secff,reg)*PD(secff,reg)**(1-SIGMAPF(secff,reg))+
                     (1-CPF(secff,reg))*PF(secff,reg)**(1-SIGMAPF(secff,reg)))**(1/(1-SIGMAPF(secff,reg))))$(XDPF0(secff,reg) ne 0)
                     +1$(XDPF0(secff,reg) eq 0);


EFF(secff,reg)..     FF(secff,reg) =E= (XDPF(secff,reg)*APPF(secff,reg)*(1-CPF(secff,reg))*
                                       (PDPF(secff,reg)/(APPF(secff,reg)*PFES(secff,reg)))**SIGMAPF(secff,reg))
                                       $(FF0(secff,reg) ne 0);

EFF2(secff,reg)..    FF(secff,reg) =E= (XDPF(secff,reg)*APPF(secff,reg)*(1-CPF(secff,reg))*
                                       (PDPF(secff,reg)/(APPF(secff,reg)*PF(secff,reg)))**SIGMAPF(secff,reg))
                                       $(FF0(secff,reg) ne 0);


EPDPFES(secff,reg)..     PFES(secff,reg) =E=( (   (    (PDPF(secff,reg)/APPF(secff,reg))**(1-SIGMAPF(secff,reg))-CPF(secff,reg)*PD(secff,reg)**(1-SIGMAPF(secff,reg)) )
                          /(1-CPF(secff,reg)) )**(1/(1-SIGMAPF(secff,reg))) )$(FF0(secff,reg) ne 0) ;





EIMPO(sec,reg)$(IMPO0(sec,reg) ne 0)..     IMPO(sec,reg) =E= YD(sec,reg)*APY(sec,reg)*(1-CX(sec,reg))*
                                          (PY(sec,reg)/(APY(sec,reg)*PIMP(sec,reg)))**SIGMAX(sec,reg);


EPD(sec,reg)..       PD(sec,reg) =E= (APD(sec,reg)*(
                                     DK(sec,reg)*(PK(sec,reg)/TGK(sec,reg))**(1-SIGMA(sec,reg))+
                                     DL(sec,reg)*(PL(sec,reg)/(TGL(sec,reg)*TGC(reg)))**(1-SIGMA(sec,reg))+
                                     DE(sec,reg)*(PE(sec,reg)/TGE(sec,reg))**(1-SIGMA(sec,reg))+
                                     DM(sec,reg)*(PM(sec,reg)/TGM(sec,reg))**(1-SIGMA(sec,reg)))
                                     **(1/(1-SIGMA(sec,reg))))$(XD0(sec,reg) ne 0)+1$(XD0(sec,reg) eq 0) ;


EKAV(sec,reg)..     KAV(sec,reg)*TGK(sec,reg) =E=  (XD(sec,reg)*APD(sec,reg)*DK(sec,reg)*
                                      (PD(sec,reg)/((PK(sec,reg)*APD(sec,reg))/(TGK(sec,reg))))
                                      **SIGMA(sec,reg))$(KAV0(sec,reg) ne 0)+0;


ELAV(sec,reg)..     LAV(sec,reg)*(TGL(sec,reg)*TGC(reg)) =E=  (XD(sec,reg)*APD(sec,reg)*DL(sec,reg)*
                                      (PD(sec,reg)/((PL(sec,reg)*APD(sec,reg))/((TGL(sec,reg)*TGC(reg)))))
                                      **SIGMA(sec,reg))$(LAV0(sec,reg) ne 0)+0;

EEN(sec,reg)..     EN(sec,reg)*TGE(sec,reg) =E=  (XD(sec,reg)*APD(sec,reg)*DE(sec,reg)*
                                      (PD(sec,reg)/((PE(sec,reg)*APD(sec,reg))/(TGE(sec,reg))))
                                      **SIGMA(sec,reg))$(EN0(sec,reg) ne 0)+0;

EMA(sec,reg)..     MA(sec,reg)*TGM(sec,reg) =E=  (XD(sec,reg)*APD(sec,reg)*DM(sec,reg)*
                                      (PD(sec,reg)/((PM(sec,reg)*APD(sec,reg))/(TGM(sec,reg))))
                                      **SIGMA(sec,reg))$(MA0(sec,reg) ne 0)+0;


EPE(sec,reg)..     PE(sec,reg) =E= (APE(sec,reg)*(DEPR("05",sec,reg)*(PIO("05",sec,reg)/
                                   GEPR("05",sec,reg))**(1-SIGMAE(sec,reg))+
                                   (1-DEPR("05",sec,reg))*(PEF(sec,reg)/TGENF(sec,reg))**(1-SIGMAE(sec,reg))
                                   )**(1/(1-SIGMAE(sec,reg))))$(EN0(sec,reg) ne 0) +
                                   1$(EN0(sec,reg) eq 0);

EPEF(sec,reg)..    PEF(sec,reg) =E= (APEF(sec,reg)*(
                                    sum(Secmr,DEPR(secmr,sec,reg)*(PIO(secmr,sec,reg)/GEPR(secmr,sec,reg))
                                    **(1-SIGMAEF(sec,reg))))**(1/(1-SIGMAEF(sec,reg))))$(ENF0(sec,reg) ne 0) +
                                    1$(ENF0(sec,reg) eq 0) ;



EENF(sec,reg)..    ENF(sec,reg)*TGENF(sec,reg) =E= EN(sec,reg)*APE(sec,reg)*(1-DEPR("05",sec,reg))*(PE(sec,reg)/
                                    ((PEF(sec,reg)*APE(sec,reg))/TGENF(sec,reg)))**SIGMAE(sec,reg)$(EN0(sec,reg) ne 0) +
                                                   0$(EN0(sec,reg) eq 0) ;


EPIO(pro,sec,reg)..  PIO(pro,sec,reg) =E= (PB(pro,reg)*(1+TXTVAIO(pro,sec,reg))+DTXTVAIO(pro,sec,reg)*TAXECO2(reg))$(IOV0(pro,sec,reg) ne 0)+
                                           1$(IOV0(pro,sec,reg) eq 0);



EIOV(pro,sec,reg)$(IOV0(pro,sec,reg) ne 0)..   IOV(pro,sec,reg) =E=(
                       (((ENF(sec,reg)*APEF(sec,reg)*DEPR(pro,sec,reg)*
                       (PEF(sec,reg)/((PIO(pro,sec,reg)*APEF(sec,reg))/(GEPR(pro,sec,reg))))**SIGMAEF(sec,reg))/
                        GEPR(pro,sec,reg)))
                        $ ((ord(pro) eq 1) or (ord(pro) eq 3)  or (ord(pro) eq 4))+
                        (((EN(sec,reg)*APE(sec,reg)*DEPR(pro,sec,reg)*
                       (PE(sec,reg)/((PIO(pro,sec,reg)*APE(sec,reg))/(GEPR(pro,sec,reg))))**SIGMAE(sec,reg))/
                        GEPR(pro,sec,reg)))
                       $ (ord(pro) eq 5) +
                        ((OTRA(sec,reg)*APM(sec,reg)*DMPR(pro,sec,reg)*
                       (POTRA(sec,reg)/((PIO(pro,sec,reg)*APM(sec,reg))/(GMPR(pro,sec,reg))))**SIGMAM(sec,reg))/
                        GMPR(pro,sec,reg))
                        $ ((ord(pro) eq 6) or (ord(pro) eq 7)  or (ord(pro) eq 8) or (ord(pro) eq 9) or (ord(pro) eq 10)
                          or (ord(pro) eq 11) or (ord(pro) gt 14) ) +
                        (XDPP(sec,reg)*APPP(sec,reg)*(1-CPP(sec,reg))*
                       (PDPP(sec,reg)/(APPP(sec,reg)*PIO(pro,sec,reg)))**SIGMAPP(sec,reg))
                        $ ((ord(pro) eq 2) and (ord(sec) eq 4)) +
                        (((ENF(sec,reg)*APEF(sec,reg)*DEPR(pro,sec,reg)*
                       (PEF(sec,reg)/((PIO(pro,sec,reg)*APEF(sec,reg))/(GEPR(pro,sec,reg))))**SIGMAEF(sec,reg))/
                        GEPR(pro,sec,reg)))
                        $ ((ord(pro) eq 2) and (ord(sec) ne 4)) +
                        ((TRA(sec,reg)*APTRA(sec,reg)*DMPR(pro,sec,reg)*
                       (PTRA(sec,reg)/((PIO(pro,sec,reg)*APTRA(sec,reg))/(GMPR(pro,sec,reg))))**SIGMATRA(sec,reg))/
                        GMPR(pro,sec,reg))
                        $ ((ord(pro) eq 12) or (ord(pro) eq 13)  or (ord(pro) eq 14)))
                       ;


EPM(sec,reg)..         PM(sec,reg) =E=(APMM(sec,reg)*(
                                   CMM(sec,reg)*(PTRA(sec,reg)/TGTRA(sec,reg))**(1-SIGMAMM(sec,reg))+
                                   (1-CMM(sec,reg))*(POTRA(sec,reg)/TGOTRA(sec,reg))**(1-SIGMAMM(sec,reg))
                                   )**(1/(1-SIGMAMM(sec,reg))))$(MA0(sec,reg) ne 0) +
                                    1$(MA0(sec,reg) eq 0) ;


ETRA(sec,reg)..         TRA(sec,reg)*TGTRA(sec,reg) =E= (MA(sec,reg)*APMM(sec,reg)*CMM(sec,reg)*
                                                        (PM(sec,reg)/((PTRA(sec,reg)*APMM(sec,reg))/TGTRA(sec,reg)))
                                                        **SIGMAMM(sec,reg))$(TRA0(sec,reg) ne 0)+0;

EOTRA(sec,reg)..        OTRA(sec,reg)*TGOTRA(sec,reg) =E=(MA(sec,reg)*APMM(sec,reg)*(1-CMM(sec,reg))*
                                                        (PM(sec,reg)/((POTRA(sec,reg)*APMM(sec,reg))/TGOTRA(sec,reg)))
                                                        **SIGMAMM(sec,reg))$(OTRA0(sec,reg) ne 0)+0;

EPTRA(sec,reg)..         PTRA(sec,reg) =E=  (APTRA(sec,reg)*(
                                    sum(Sectra,DMPR(sectra,sec,reg)*(PIO(sectra,sec,reg)/GMPR(sectra,sec,reg))
                                    **(1-SIGMATRA(sec,reg))))**(1/(1-SIGMATRA(sec,reg))))$(TRA0(sec,reg) ne 0)+
                                    1$(TRA0(sec,reg) eq 0);

EPOTRA(sec,reg)..        POTRA(sec,reg) =E= (APM(sec,reg)*(
                                    sum(Secotra,DMPR(secotra,sec,reg)*(PIO(secotra,sec,reg)/GMPR(secotra,sec,reg))
                                    **(1-SIGMAM(sec,reg))))**(1/(1-SIGMAM(sec,reg))))$(OTRA0(sec,reg) ne 0)+
                                    1$(OTRA0(sec,reg) eq 0);


EPDR(sec,reg)..         PDR(sec,reg) =E= PD(sec,reg)/PINV(sec,reg);

EPDRA(sec,reg)..        PDRA(sec,reg) =E= PDR(sec,reg)*(1+ALPD(sec,reg)*
                        ((PDR(sec,reg)-PDRM1(sec,reg))/PDRM1(sec,reg))+
                        (1-ALPD(sec,reg))*((PDRM1(sec,reg)-PDRM2(sec,reg))/PDRM2(sec,reg)));


EKAVO(sec,reg)..        KAVO(sec,reg)*TGKA(sec,reg) =E=  (XDA(sec,reg)*APD(sec,reg)*DK(sec,reg)*
                                      (PDRA(sec,reg)/(((RLTL(reg)+DECL(sec,reg)+ERRCK(sec,reg))*APD(sec,reg))/(TGKA(sec,reg))))
                                      **SIGMA(sec,reg))$(KAVO0(sec,reg) ne 0)+0;


EKAVA(sec,reg)..        KAVA(sec,reg) =E= (((1-ALKAVA(sec,reg))*KAVO(sec,reg)+ALKAVA(sec,reg)*(KAVC(sec,reg)
                                           *KAVC(sec,reg))/KAVCM1(sec,reg)))$(KAVCM10(sec,reg) ne 0)+0;

EINVV(sec,reg)..        INVV(sec,reg) =E=  KAVA(sec,reg)-(1-DECL(sec,reg))*KAVC(sec,reg)+ERRINVV(sec,reg);


EPB(sec,reg)..          PB(sec,reg)   =E=  (PY(sec,reg)/(1-TXIT(sec,reg)))*(1+TAXEGHG(sec,reg));


EPHC(sec,reg)..         PHC(sec,reg)  =E=  (PB(sec,reg)+TXEXCISE(sec,reg))*(1+TXTVAHC(sec,reg))+DTXTVAHC(sec,reg)*TAXECO2(reg);

EPGC(sec,reg)..         PGC(sec,reg)  =E=  PB(sec,reg)*(1+TXTVAGC(sec,reg));

EPINV(sec,reg)..        PINV(sec,reg) =E=  sum(pro,PB(pro,reg)*TINV(pro,sec,reg)*(1+TXTVAIN(pro,sec,reg)));


EPEX(sec,reg)..         PEX(sec,reg)  =E=  PB(sec,reg)*(1+TXTVAEX(sec,reg));

EPL(sec,reg)..          PL(sec,reg)   =E=   W(reg)*(1+sum(itw,TXW(itw,sec,reg)));

EHCV(sec,reg)..         HCV(sec,reg) =E= CHCV(sec,reg)+(BHCV(sec,reg)/PHC(sec,reg))*(HCDTOT(reg)-
                                         sum(pro,PHC(pro,reg)*CHCV(pro,reg)));

EHCDTOT(reg)..          HCDTOT(reg) =E= (1-TSAVE(reg))*(FSEFA_H_K(reg)+FSEFA_H_L(reg)+FSEFA_H_F(reg)+
                                        FSESE_H_G(reg)-FGRS_DT_H(reg));

EFSEFA1(reg)..          FSEFA_H_L(reg) =E= sum(sec,LAV(sec,reg))*W(reg);

EFSEFA2(reg)..          FSEFA_H_K(reg) =E= sum(sec,KAVC(sec,reg)*PK(sec,reg));

EFSEFA3(reg)..          FSEFA_H_F(reg) =E= sum(secff,FF(secff,reg)*PF(secff,reg));

EFSEFA4(reg)..          FSEFA_G_L(reg) =E= sum(sec,LAV(sec,reg)*TXW("SS",sec,reg))*W(reg);


EFGRS1(reg)..           FGRS_DT_H(reg) =E= TXFGRS_H(reg)*
                                               (FSEFA_H_K(reg)+FSEFA_H_L(reg)+FSEFA_H_F(reg)+FSESE_H_G(reg));

EFGRS3(reg)..           FGRS_DUT_G(reg) =E= sum(sec,sum(rege,IMPOR(sec,rege,reg)
                                               *TXDUT(sec,rege,reg)*PIMPR(sec,rege,reg)));


EFSEFE1(reg)..          FSESE_H_G(reg) =E=  TXFSESE_H_G(reg)*(FSEFA_H_K(reg)+FSEFA_H_L(reg)+FSEFA_H_F(reg));


EFGRS2(reg)..           FGRS_IT_G(reg) =E= sum(sec,YD(sec,reg)*PY(sec,reg)*TXIT(sec,reg)/(1-TXIT(sec,reg)));


EFGRS4(reg)..           FGRS_VATHC_G(reg)  =E= sum(sec,HCV(sec,reg)*(PB(sec,reg)+TXEXCISE(sec,reg))*TXTVAHC(sec,reg))+
                                               sum(sec,HCV(sec,reg)*TXEXCISE(sec,reg))+
                                               sum(sec,HCV(sec,reg)*DTXTVAHC(sec,reg))*TAXECO2(reg);

EFGRS5(reg)..           FGRS_VATIO_G(reg)  =E= sum(pro,sum(sec,IOV(pro,sec,reg)*TXTVAIO(pro,sec,reg)*PB(pro,reg)))+
                                                   sum(sece,sum(sec,IOV(sece,sec,reg)*DTXTVAIO(sece,sec,reg)))
                                                   *TAXECO2(reg);


EFGRS6(reg)..           FGRS_VATST_G(reg)  =E= sum(sec,STV(sec,reg)*PB(sec,reg)*TXTVAST(sec,reg));

EFGRS7(reg)..           FGRS_VATINV_G(reg)  =E= sum(pro,PB(pro,reg)*
                                                sum(sec,TINV(pro,sec,reg)*INVV(sec,reg)*TXTVAIN(pro,sec,reg)));

EFGRS8(reg)..           FGRS_VATGC_G(reg)  =E= sum(sec,GCV(sec,reg)*PB(sec,reg)*TXTVAGC(sec,reg));

ESAVEG(reg)..           SAVE_G(reg) =E= FGRS_IT_G(reg)+FGRS_VATIO_G(reg)+FGRS_VATHC_G(reg)
                                          +FGRS_VATST_G(reg)+FGRS_VATINV_G(reg)
                                          +FGRS_DT_H(reg)+FGRS_DUT_G(reg)+FSEFA_G_L(reg)+FGRS_VATGC_G(reg)
                                          +FGRS_GHG_G(reg)
                                          -sum(sec,GCV(sec,reg)*PGC(sec,reg))-FSESE_H_G(reg)
                                          +QUOTANET(reg)*TAXECO2(reg)*1000;




ESAVEH(reg)..           SAVE_H(reg) =E= FSEFA_H_K(reg)+FSEFA_H_L(reg)+FSEFA_H_F(reg)+FSESE_H_G(reg)
                                          -sum(sec,PHC(sec,reg)*HCV(sec,reg))-FGRS_DT_H(reg);

ECO2(reg)..              CO2(reg)     =E= sum(sec,sum(secef,CTEP(secef,sec,reg)*IOV(secef,sec,reg)*CO2CONTENT(secef)))+
                                         sum(secef,CTEPH(secef,reg)*HCV(secef,reg)*CO2CONTENT(secef));

* Prendre en compte le prix des transports dans le prix des imports


EPIMP(sec,reg)..        PIMP(sec,reg) =E= ((APIMP(sec,reg)*sum(rege,CIMP(sec,rege,reg)*
                                         ( PIMPR(sec,rege,reg)*(1+TXDUT(sec,rege,reg)))**(1-SIGMAI(sec,reg)))
                                         **(1/(1-SIGMAI(sec,reg))))$(IMPO0(sec,reg) ne 0) + 1$(IMPO0(sec,reg) eq 0));


EIMPOR(sec,rege,reg)$(IMPOR0(sec,rege,reg) ne 0).. IMPOR(sec,rege,reg)  =E= (IMPO(sec,reg)*APIMP(sec,reg)*CIMP(sec,rege,reg)*(PIMP(sec,reg)/(APIMP(sec,reg)*
                                             PIMPR(sec,rege,reg)*(1+TXDUT(sec,rege,reg))))**SIGMAI(sec,reg));

EPIMPR(sec,rege,reg)$(IMPOR0(sec,rege,reg) ne 0).. PIMPR(sec,rege,reg)  =E= sum(sectra,CIMPMTRA(sectra,sec,rege,reg)*sum(regf,TIMPMTRA(sectra,sec,regf,rege,reg)
                                                *PEX(sectra,regf)*(EX(regf)/EX(reg))))+
                                               (1-sum(sectra,CIMPMTRA(sectra,sec,rege,reg)))*PEX(sec,rege)*(EX(rege)/EX(reg));

EIMPTRA(sectra)..        IMPTRA(sectra) =E= sum(sec,sum(rege,sum(reg,CIMPMTRA(sectra,sec,rege,reg)*IMPOR(sec,rege,reg))));

EEXPOP(sec,reg)$(EXPOP0(sec,reg) ne 0)..   EXPOP(sec,reg) =E= sum(rege,IMPOR(sec,reg,rege))
                                           -sum(sectra,sum(rege, CIMPMTRA(sectra,sec,reg,rege)*IMPOR(sec,reg,rege)))
                                            +TIMPMTRA0(sec,reg)*IMPTRA(sec)$((ord(sec) eq 12) or (ord(sec) eq 13) or (ord(sec) eq 14))
;


EPK(sec,reg)..          PK(sec,reg)     =E= PK0(sec,reg);


MRKL(reg)..             NSUP(reg)     =E= sum(sec,LAV(sec,reg));

MRKK(sec,reg)$(KAVC0(sec,reg) ne 0)..          KAV(sec,reg)     =E= KAVC(sec,reg);

MRKF(secff,reg)..       FF(secff,reg)  =E= FFS(secff,reg);


MRCOM(reg)$(ord(reg) ne 1)..   (sum(sec,IMPO(sec,reg)*PIMP(sec,reg))-FGRS_DUT_G(reg)) =E=
                                (sum(sec,EXPOP(sec,reg)*PEX(sec,reg))+EC(reg))+QUOTANET(reg)*TAXECO2(reg)*1000;


MRINV(reg)..            INVCC(reg) =E= sum(sec,PINVB(sec,reg)*INVV(sec,reg));

MRSAVEG(reg)..            SAVECC_G(reg) =E= SAVE_G(reg)+
                        sum(sec,PGC(sec,reg)*TGCV(sec,reg)*(sum(ghg,sum(pro,COUTABAT(ghg,pro,reg)))/sum(pro,PGC(pro,reg)*TGCV(pro,reg))));

MRCO2..                 CO2QW =G= Sum(reg,CO2(reg)+sum(ghg,sum(sec,EGHG(ghg,sec,reg))));


ETAXECO2(reg)..          TAXECO2(reg)   =E=  (TAXECO2W*EX("OEC")/EX(reg));

ETAXECO2B(reg)..          TAXECO2(reg)   =E=  0;

ETXFGRSPRED_H(reg)..     TXFGRS_H(reg)   =E= TXFGRS0_H(reg);

ETSAVE(reg)..            TSAVE(reg)   =E= TSAVE0(reg);

EEGHG(ghg,sec,reg)$((ACXDT("2001",ghg,sec,reg)+ACXDPFT("2001",ghg,sec,reg)+ACYDT("2001",ghg,sec,reg)+ACHCVTT("2001",ghg,sec,reg)) ne 0).. EGHG(ghg,sec,reg)=E=
                                          ((ACXD(ghg,sec,reg)*XD(sec,reg)+ACXDPF(ghg,sec,reg)*XDPF(sec,reg)+ACYD(ghg,sec,reg)*YD(sec,reg)
                                          +ACHCVT(ghg,sec,reg)*sum(pro,HCV(pro,reg)*PHC0(pro,reg))
                                          )/1000)
                                          *(1-ABAT(ghg,sec,reg));


EFGRS9B(reg)..           FGRS_GHG_G(reg)  =E= 0;

EFGRS9(reg)..            FGRS_GHG_G(reg)  =E= (Sum(ghg,sum(sec,EGHG(ghg,sec,reg))))*1000*TAXECO2(reg);

ETAXEGHG(sec,reg)..  TAXEGHG(sec,reg)=E=((Sum(ghg,EGHG(ghg,sec,reg))*1000*TAXECO2(reg))/(YD(sec,reg)*PY(sec,reg)/(1-TXIT(sec,reg))))$(YD0(sec,reg) ne 0)
                                        +0;

ETAXEGHGB(sec,reg)..  TAXEGHG(sec,reg)=E=0;

EGCV(sec,reg)..         GCV(sec,reg) =E= TGCV(sec,reg)*(GCVTOT(reg)+sum(ghg,sum(pro,COUTABAT(ghg,pro,reg)))/(sum(pro,PGC(pro,reg)*TGCV(pro,reg))));


ECOUTABATB(ghg,sec,reg)$((ACXDT("2001",ghg,sec,reg)+ACXDPFT("2001",ghg,sec,reg)+ACYDT("2001",ghg,sec,reg)+ACHCVTT("2001",ghg,sec,reg)) ne 0)..  COUTABAT(ghg,sec,reg)=E=0;


ECOUTABAT(ghg,sec,reg)$((ACXDT("2001",ghg,sec,reg)+ACXDPFT("2001",ghg,sec,reg)+ACYDT("2001",ghg,sec,reg)+ACHCVTT("2001",ghg,sec,reg)) ne 0)..  COUTABAT(ghg,sec,reg)=E=COUTUABAT(ghg,sec,reg)*
                                          ((ACXD(ghg,sec,reg)*XD(sec,reg)+ACXDPF(ghg,sec,reg)*XDPF(sec,reg)+ACYD(ghg,sec,reg)*YD(sec,reg)
                                           +ACHCVT(ghg,sec,reg)*sum(pro,HCV(pro,reg)*PHC0(pro,reg))
                                           )/1000)*ABAT(ghg,sec,reg);

EQUOTANETB(reg).. QUOTANET(reg) =E= 0;

EQUOTANET(reg).. QUOTANET(reg) =E=  CO2Q(reg)-CO2(reg)-sum(ghg,sum(sec,EGHG(ghg,sec,reg)));

Option decimals=7;


MODEL GEMINI_calage /
EPIBVA.PIBVA,EPIBVO.PIBVO,EPIMP.PIMP,EIMPOR.IMPOR,EPIMPR.PIMPR,EIMPTRA.IMPTRA,
EINVPVP.INVPVP,EEXPOP.EXPOP,EYD.YD,EPY.PY,EXD.XD,EFF.FF,EIMPO.IMPO,EXDPF.XDPF,
EPDPF.PDPF,EXDPP.XDPP,EPDPP.PDPP,EPD.PD,EKAV.KAV,ELAV.LAV,EEN.EN,EMA.MA,ETRA.TRA,
EOTRA.OTRA,EPTRA.PTRA,EPOTRA.POTRA,EPE.PE,EPEF.PEF,EENF.ENF,EIOV.IOV,EPM.PM,EPDR.PDR,
EPDRA.PDRA,EKAVO.KAVO,EKAVA.KAVA,EINVV.INVV,EHCV.HCV,EHCDTOT.HCDTOT,EPB.PB,EPHC.PHC,
EPGC.PGC,EPINV.PINV,EPEX.PEX,EPL.PL,EPIO.PIO,EPF.PF,EFSEFA1.FSEFA_H_L,
EFSEFA2.FSEFA_H_K,EFSEFA3.FSEFA_H_F,EFSEFA4.FSEFA_G_L,EFGRS1.FGRS_DT_H,
EFSEFE1.FSESE_H_G,EFGRS2.FGRS_IT_G,EFGRS3.FGRS_DUT_G,EFGRS4.FGRS_VATHC_G,
EFGRS5.FGRS_VATIO_G,EFGRS6.FGRS_VATST_G,EFGRS7.FGRS_VATINV_G,EFGRS8.FGRS_VATGC_G,
ESAVEG.SAVE_G,ESAVEH.SAVE_H,MRKL.RLTL,EPK.PK,MRCOM.EX,
EGCV.GCV,EPDPFES.PFES,ETSAVE.TSAVE,ETXFGRSPRED_H.TXFGRS_H,

EQUOTANETB.QUOTANET,
ECOUTABATB.COUTABAT,ETAXECO2B.TAXECO2,EFGRS9B.FGRS_GHG_G,ETAXEGHGB.TAXEGHG
/;



MODEL GEMINI /
EPIBVA.PIBVA,EPIBVO.PIBVO,EPIMP.PIMP,EIMPOR.IMPOR,EPIMPR.PIMPR,EIMPTRA.IMPTRA,
EINVPVP.INVPVP,EEXPOP.EXPOP,EYD.YD,EPY.PY,EXD.XD,EFF.FF,EIMPO.IMPO,EXDPF.XDPF,
EPDPF.PDPF,EXDPP.XDPP,EPDPP.PDPP,EPD.PD,EKAV.KAV,ELAV.LAV,EEN.EN,EMA.MA,ETRA.TRA,
EOTRA.OTRA,EPTRA.PTRA,EPOTRA.POTRA,EPE.PE,EPEF.PEF,EENF.ENF,EIOV.IOV,EPM.PM,EPDR.PDR,
EPDRA.PDRA,EKAVO.KAVO,EKAVA.KAVA,EINVV.INVV,EHCV.HCV,EHCDTOT.HCDTOT,EPB.PB,EPHC.PHC,
EPGC.PGC,EPINV.PINV,EPEX.PEX,EPL.PL,EPIO.PIO,EPF.PF,EFSEFA1.FSEFA_H_L,
EFSEFA2.FSEFA_H_K,EFSEFA3.FSEFA_H_F,EFSEFA4.FSEFA_G_L,EFGRS1.FGRS_DT_H,
EFSEFE1.FSESE_H_G,EFGRS2.FGRS_IT_G,EFGRS3.FGRS_DUT_G,EFGRS4.FGRS_VATHC_G,
EFGRS5.FGRS_VATIO_G,EFGRS6.FGRS_VATST_G,EFGRS7.FGRS_VATINV_G,EFGRS8.FGRS_VATGC_G,
ESAVEG.SAVE_G,ESAVEH.SAVE_H,MRKL.RLTL,MRKK.PK,MRCOM.EX,EGCV.GCV
EPDPFES.PFES,ETSAVE.TSAVE,ETXFGRSPRED_H.TXFGRS_H,

ECO2.CO2,EEGHG.EGHG,EQUOTANETB.QUOTANET,
ECOUTABATB.COUTABAT,ETAXECO2B.TAXECO2,EFGRS9B.FGRS_GHG_G,ETAXEGHGB.TAXEGHG
/;


MODEL GEMINIV /
EPIBVA.PIBVA,EPIBVO.PIBVO,EPIMP.PIMP,EIMPOR.IMPOR,EPIMPR.PIMPR,EIMPTRA.IMPTRA,
EINVPVP.INVPVP,EEXPOP.EXPOP,EYD.YD,EPY.PY,EXD.XD,EFF2.FF,EIMPO.IMPO,EXDPF.XDPF,
EPDPF2.PDPF,EXDPP.XDPP,EPDPP.PDPP,EPD.PD,EKAV.KAV,ELAV.LAV,EEN.EN,EMA.MA,ETRA.TRA,
EOTRA.OTRA,EPTRA.PTRA,EPOTRA.POTRA,EPE.PE,EPEF.PEF,EENF.ENF,EIOV.IOV,EPM.PM,EPDR.PDR,
EPDRA.PDRA,EKAVO.KAVO,EKAVA.KAVA,EINVV.INVV,EHCV.HCV,EHCDTOT.HCDTOT,EPB.PB,EPHC.PHC,
EPGC.PGC,EPINV.PINV,EPEX.PEX,EPL.PL,EPIO.PIO,EFSEFA1.FSEFA_H_L,
EFSEFA2.FSEFA_H_K,EFSEFA3.FSEFA_H_F,EFSEFA4.FSEFA_G_L,EFGRS1.FGRS_DT_H,
EFSEFE1.FSESE_H_G,EFGRS2.FGRS_IT_G,EFGRS3.FGRS_DUT_G,EFGRS4.FGRS_VATHC_G,
EFGRS5.FGRS_VATIO_G,EFGRS6.FGRS_VATST_G,EFGRS7.FGRS_VATINV_G,EFGRS8.FGRS_VATGC_G,
ESAVEG.SAVE_G,ESAVEH.SAVE_H,MRKL.RLTL,MRKK.PK,MRCOM.EX,EGCV.GCV


ECO2.CO2,EEGHG.EGHG,MRSAVEG.TXFGRS_H,MRKF.PF,EQUOTANET.QUOTANET,

MRINV.TSAVE,


ECOUTABAT.COUTABAT,ETAXEGHG.TAXEGHG,EFGRS9.FGRS_GHG_G,
MRCO2.TAXECO2W,ETAXECO2.TAXECO2

/;

* DEBUT ITERATION POUR CALAGE
fILE infoiter information sur la convergence /iter.txt/;

TXEXCISE0(sec,reg) =TXEXCISE(sec,reg);

sumdiff=100;crit=0.005;
stop=0;

DEPR0(sec,pro,reg)=DEPR(sec,pro,reg);

iter=2001;
iterfin=2050;

TXEXCISE(sec,reg)=TXEXCISE0(sec,reg);


SORTIEMAC("PGDP","2000",reg)=1;

HCDTOT.L(reg)      =        HCDTOT0(reg) ;
INVV.L(sec,reg)    =        INVV0(sec,reg)  ;
YD.L(sec,reg)      =        YD0(sec,reg)    ;
HCV.L(sec,reg)     =        HCV0(sec,reg)     ;
IMPTRA.L(sectra)   =        IMPTRA0(sectra)   ;
XD.L(sec,reg)      =        XD0(sec,reg)      ;
XDPP.L(secrp,reg)  =        XDPP0(secrp,reg)  ;
XDPF.L(secff,reg)  =        XDPF0(secff,reg)  ;
KAV.L(sec,reg)    =         KAV0(sec,reg)    ;
KAVO.L(sec,reg)    =        KAVO0(sec,reg)    ;
KAVA.L(sec,reg)    =        KAVA0(sec,reg)    ;
LAV.L(sec,reg)     =        LAV0(sec,reg)     ;
EN.L(sec,reg)      =        EN0(sec,reg)      ;
MA.L(sec,reg)      =        MA0(sec,reg)      ;
ENF.L(sec,reg)     =        ENF0(sec,reg)     ;
TRA.L(sec,reg)     =        TRA0(sec,reg)     ;
OTRA.L(sec,reg)     =       OTRA0(sec,reg)     ;
FF.L(secff,reg)    =        FF0(secff,reg)    ;
PY.L(sec,reg)      =        PY0(sec,reg)      ;
PD.L(sec,reg)      =        PD0(sec,reg)      ;
PDPF.L(secff,reg)  =        PDPF0(secff,reg)  ;
PDPP.L(secrp,reg)  =        PDPP0(secrp,reg)  ;
PL.L(sec,reg)      =        PL0(sec,reg)      ;
PM.L(sec,reg)      =        PM0(sec,reg)      ;
PE.L(sec,reg)      =        PE0(sec,reg)      ;
PB.L(sec,reg)      =        PB0(sec,reg)      ;
PEF.L(sec,reg)     =        PEF0(sec,reg)     ;
PM.L(sec,reg)      =        PM0(sec,reg)      ;
PTRA.L(sec,reg)    =        PTRA0(sec,reg)    ;
POTRA.L(sec,reg)    =       POTRA0(sec,reg)    ;
PHC.L(sec,reg)     =        PHC0(sec,reg)     ;
PDR.L(sec,reg)     =        PDR0(sec,reg)     ;
PDRA.L(sec,reg)    =        PDRA0(sec,reg)    ;
PINV.L(sec,reg)    =        PINV0(sec,reg)    ;
RLTL.L(reg)        =        RLTL0(reg)        ;
PEX.L(sec,reg)     =        PEX0(sec,reg)     ;
PF.L(secff,reg)    =        PF0(secff,reg)    ;
FSEFA_H_K.L(reg)   =        FSEFA0("H","K",reg);
FSEFA_H_L.L(reg)   =        FSEFA0("H","L",reg);
FSEFA_H_F.L(reg)   =        FSEFA0("H","F",reg);
FSEFA_G_L.L(reg)   =        FSEFA0("G","L",reg);
FSESE_H_G.L(reg)  =         FSESE0_H_G(reg);
FGRS_DT_H.L(reg)     =     FGRS0("DT","H",reg)  ;
FGRS_IT_G.L(reg)     =     FGRS0("IT","G",reg)  ;
FGRS_DUT_G.L(reg)    =     FGRS0("DUT","G",reg)  ;
FGRS_VATHC_G.L(reg)  =     FGRS0("VATHC","G",reg)  ;
FGRS_VATIO_G.L(reg)  =     FGRS0("VATIO","G",reg)  ;
FGRS_VATST_G.L(reg)  =     FGRS0("VATST","G",reg)  ;
FGRS_VATINV_G.L(reg) =     FGRS0("VATINV","G",reg)  ;
FGRS_VATGC_G.L(reg)  =     FGRS0("VATGC","G",reg)  ;
SAVE_H.L(reg)       =      SAVE0("H",reg)     ;
SAVE_G.L(reg)       =      SAVE0("G",reg)     ;
CO2.L(reg)           =        CO20(reg)         ;
PK.L(sec,reg)      =        PK0(sec,reg)      ;
PIMP.L(sec,reg)    =         PIMP0(sec,reg)    ;
EX.L(Reg)           =      EX0(reg);
EX.fx("OEC")           =      EX0("OEC");
PF.L(secff,reg)= PF0(secff,reg);
PFES.L(secff,reg)= PF0(secff,reg);


COUTABAT.L(ghg,sec,reg)=0;
ABAT(ghg,sec,reg)=0;
ACXD(ghg,sec,reg)=ACXDT("2001",ghg,sec,reg);
ACYD(ghg,sec,reg)=ACYDT("2001",ghg,sec,reg);
ACHCVT(ghg,sec,reg)=ACHCVTT("2001",ghg,sec,reg);
ACXDPF(ghg,sec,reg)=ACXDPFT("2001",ghg,sec,reg);
COUTUABAT(ghg,sec,reg)=0;


loop(sec,loop(ghg,loop(reg,
if (((ACXD(ghg,sec,reg)+ACXDPF(ghg,sec,reg)+ACYD(ghg,sec,reg)+ACHCVT(ghg,sec,reg)) ne 0),

 EGHG.L(ghg,sec,reg)=(ACXD(ghg,sec,reg)*XD0(sec,reg)+ACXDPF(ghg,sec,reg)*XDPF0(sec,reg)+ACYD(ghg,sec,reg)*YD0(sec,reg)+
                     ACHCVT(ghg,sec,reg)*sum(pro,HCV0(pro,reg)*PHC0(pro,reg))
                     );
 COUTABAT.L(ghg,sec,reg)=0;
else
 EGHG.fx(ghg,sec,reg)=0;
 COUTABAT.fx(ghg,sec,reg)=0;
   )
)));
;


loop(sec,loop(pro,loop(reg,
if (IOV0(sec,pro,reg) eq 0,
   IOV.fx(sec,pro,reg)=0;
   PIO.fx(sec,pro,reg)=1;
                      else
   IOV.L(sec,pro,reg)= IOV0(sec,pro,reg);
   PIO.L(sec,pro,reg)=PIO0(sec,pro,reg);
   )
)));

loop(sec,loop(rege,loop(reg,
if (IMPOR0(sec,rege,reg) eq 0,
    IMPOR.fx(sec,rege,reg)=0;
    PIMPR.fx(sec,rege,reg)=1;
                    else
    IMPOR.L(sec,rege,reg)=IMPOR0(sec,rege,reg);
    PIMPR.L(sec,rege,reg)=PIMPR0(sec,rege,reg);
   )
)));

loop(pro,loop(reg,
if (INVPVP0(pro,reg) eq 0,
    INVPVP.fx(pro,reg)=0;
                  else
    INVPVP.L(pro,reg)=INVPVP0(pro,reg);
   )
if (GCV0(pro,reg) eq 0,
     PGC.fx(pro,reg)=1;
                  else
     PGC.L(pro,reg)=PGC0(pro,reg);
    )
if (EXPOP0(pro,reg) eq 0,
     EXPOP.fx(pro,reg)=0;
                  else
      EXPOP.L(pro,reg)=EXPOP0(pro,reg);
    )
if (IMPO0(pro,reg) eq 0,
     IMPO.fx(pro,reg)=0;
                  else
      IMPO.L(pro,reg)=IMPO0(pro,reg);
    )
));

W.fx(reg)           =        W0(reg)           ;
NSUP.fx(reg)        =        NSUP0(reg) ;
PDRM1.fx(sec,reg)   =        PDRM10(sec,reg)   ;
PDRM2.fx(sec,reg)   =        PDRM20(sec,reg)   ;



TSAVE.L(reg)       =        TSAVE0(reg)       ;
TXFGRS_H.L(reg)       =        TXFGRS0_H(reg);



PPIBM1.fx(reg)          =        1;
PIBVA.L(reg)=          PIBVA0(reg);
PIBVO.L(reg)=          PIBVO0(reg);

PY.LO(sec,reg)      =        PY0(sec,reg)*0.00001      ;
PD.LO(sec,reg)      =        PD0(sec,reg)*0.00001      ;
PDPF.LO(secff,reg)  =        PDPF0(secff,reg)*0.00001  ;
PDPP.LO(secrp,reg)  =        PDPP0(secrp,reg)*0.00001  ;
PL.LO(sec,reg)      =        PL0(sec,reg)*0.00001      ;
PM.LO(sec,reg)      =        PM0(sec,reg)*0.00001      ;
PE.LO(sec,reg)      =        PE0(sec,reg)*0.00001      ;
PB.LO(sec,reg)      =        PB0(sec,reg)*0.00001      ;
PEF.LO(sec,reg)     =        PEF0(sec,reg)*0.00001     ;
PIO.LO(pro,sec,reg) =        PIO0(pro,sec,reg)*0.0001 ;
PIMP.LO(sec,reg)    =        PIMP0(sec,reg)*0.00001   ;
PM.LO(sec,reg)      =        PM0(sec,reg)*0.0001      ;
PHC.LO(sec,reg)     =        PHC0(sec,reg)*0.0001     ;
PDR.LO(sec,reg)     =        PDR0(sec,reg)*0.0001     ;
PDRA.LO(sec,reg)    =        PDRA0(sec,reg)*0.0001    ;
PINV.LO(sec,reg)    =        PINV0(sec,reg)*0.0001    ;
RLTL.LO(reg)        =        RLTL0(reg)*0.0001        ;
PGC.LO(sec,reg)     =        PGC0(sec,reg)*0.0001     ;
PEX.LO(sec,reg)     =        PEX0(sec,reg)*0.0001     ;
PF.LO(secff,reg)    =        PF0(secff,reg)*0.000001    ;
PK.LO(sec,reg)      =        PK0(sec,reg)*0.0000001      ;
PK.UP(sec,reg)      =        1.0;


XDA(sec,reg)     =        XDA0(sec,reg)     ;
XDM1(sec,reg)    =        XDM10(sec,reg)    ;
XDM2(sec,reg)    =        XDM20(sec,reg)    ;
KAVC(sec,reg)    =        KAVC0(sec,reg)    ;
KAVCM1(sec,reg)  =        KAVCM10(sec,reg)   ;
GCV.L(sec,reg)   =        GCV0(sec,reg)     ;
GCVTOT(reg)      =        GCVTOT0(reg) ;
STV(sec,reg)     =        STV0(sec,reg)     ;



*OPTION iterlim = 0 ;
OPTION Limrow  =0;
OPTION Limcol  =0;
OPTION sysout  = off;
OPTION solprint = off;

OPTION reslim = 10000;
OPTION MCP=path;



infoiter.nd=0;
put infoiter system.date,' ',system.time;put /;

TGC(reg)=1;
TGE(sec,reg)=1;
TGM(sec,reg)=1;
TGL(sec,reg)=1;
TGK(sec,reg)=1;
DEPR(sec,pro,reg)=DEPR0(sec,pro,reg);
GEPR(pro,sec,reg)=1;
CHCV(sec,reg)=CHCV0(sec,reg);

PCOAL(reg)=PDPF0("01",reg);
POIL(reg)=PDPF0("02",reg);
PGAS(reg)=PDPF0("03",reg);
PGDP_CC("2000",reg)=1;


SOLVE GEMINI_calage USING MCP;
KAVC(sec,reg)    =        KAV.L(sec,reg)    ;


Loop(T$(iter le iterfin),

Gemini.optfile =1 ;   geminiv.optfile =1 ;

if (ord(T) ge 12,

ACXD(ghg,sec,reg)=ACXDT(T,ghg,sec,reg);
ACXDPF(ghg,sec,reg)=ACXDPFT(T,ghg,sec,reg);
ACYD(ghg,sec,reg)=ACYDT(T,ghg,sec,reg);
ACHCVT(ghg,sec,reg)=ACHCVTT(T,ghg,sec,reg);

   if (iter le 2010,
       FABAT(iin,ghg,reg)=FABAT2010(reg,ghg,iin);
       else if (iter ge 2020, FABAT(iin,ghg,reg)=FABAT2020(reg,ghg,iin);
           else FABAT(iin,ghg,reg)=FABAT2010(reg,ghg,iin)+(iter-2010)*(FABAT2020(reg,ghg,iin)-FABAT2010(reg,ghg,iin))/10;
        );
      );


CO2QW=100000 ;
CO2Q(reg)=CO2QW*(POP(T,reg)/sum(rege,POP(T,rege)));
FFS(secff,reg)=FF_CC(T,secff,reg);
INVCC(reg)=INVV_CC(T,reg);
SAVECC_G(reg)=SAVE_G_CC(T,reg);

SOLVE GEMINIv USING MCP;



put infoiter iter,GEMINIv.modelstat,geminiv.resusd /;
if (GEMINIv.modelstat ne 1, stop=1);





SORTIEMAC("GDPZ",T,reg)=sum(sec,EXPOP.L(sec,reg)*PEX0(sec,reg)+PHC0(sec,reg)*HCV.L(sec,reg)+PGC0(sec,reg)*GCV.L(sec,reg)
                    +INVV.L(sec,reg)*PINV0(sec,reg)+STV(sec,reg)*PB0(sec,reg)*(1+TXTVAST(sec,reg))-
                    IMPO.L(sec,reg)*PIMP0(sec,reg));
SORTIEMAC("IMPZ",T,reg)=sum(sec,IMPO.L(sec,reg)*PIMP0(sec,reg));
SORTIEMAC("CONZ",T,reg)=sum(sec,PHC0(sec,reg)*HCV.L(sec,reg));
SORTIEMAC("GCVZ",T,reg)=sum(sec,PGC0(sec,reg)*GCV.L(sec,reg));
SORTIEMAC("INVZ",T,reg)=sum(sec,INVV.L(sec,reg)*PINV0(sec,reg));
SORTIEMAC("STVZ",T,reg)=sum(sec,STV(sec,reg)*PB0(sec,reg));
SORTIEMAC("EXPZ",T,reg)=sum(sec,EXPOP.L(sec,reg)*PEX0(sec,reg));
SORTIEMAC("GDPV",T,reg)=sum(sec,EXPOP.L(sec,reg)*PEX.L(sec,reg)+PHC.L(sec,reg)*HCV.L(sec,reg)+PGC.L(sec,reg)*GCV.L(sec,reg)
            +INVV.L(sec,reg)*PINV.L(sec,reg)+STV(sec,reg)*PB.L(sec,reg)*(1+TXTVAST(sec,reg))-
            IMPO.L(sec,reg)*PIMP.L(sec,reg));
SORTIEMAC("IMPV",T,reg)=sum(sec,IMPO.L(sec,reg)*PIMP.L(sec,reg));
SORTIEMAC("CONV",T,reg)=sum(sec,PHC.L(sec,reg)*HCV.L(sec,reg));
SORTIEMAC("GCVV",T,reg)=sum(sec,PGC.L(sec,reg)*GCV.L(sec,reg));
SORTIEMAC("INVV",T,reg)=sum(sec,INVV.L(sec,reg)*PINV.L(sec,reg));
SORTIEMAC("STVV",T,reg)=sum(sec,STV(sec,reg)*PB.L(sec,reg));
SORTIEMAC("EXPV",T,reg)=sum(sec,EXPOP.L(sec,reg)*PEX.L(sec,reg));
SORTIEMAC("PGDP",T,reg)=(SORTIEMAC("GDPV",T,reg)/SORTIEMAC("GDPZ",T,reg));
SORTIEMAC("PIMP",T,reg)=(SORTIEMAC("IMPV",T,reg)/SORTIEMAC("IMPZ",T,reg))/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("PCON",T,reg)=(SORTIEMAC("CONV",T,reg)/SORTIEMAC("CONZ",T,reg))/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("PGCV",T,reg)=(SORTIEMAC("GCVV",T,reg)/SORTIEMAC("GCVZ",T,reg))/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("PINV",T,reg)=(SORTIEMAC("INVV",T,reg)/SORTIEMAC("INVZ",T,reg))/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("PSTV",T,reg)=((SORTIEMAC("STVV",T,reg)/SORTIEMAC("STVZ",T,reg))/SORTIEMAC("PGDP",T,reg))
                       $(SORTIEMAC("STVZ",T,reg) ne 0)+0$(SORTIEMAC("STVZ",T,reg) eq 0);
SORTIEMAC("PEXP",T,reg)=(SORTIEMAC("EXPV",T,reg)/SORTIEMAC("EXPZ",T,reg))/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("EXCH",T,reg)=EX.L(reg);
SORTIEMAC("RLTL",T,reg)=RLTL.L(reg);
SORTIEMAC("TSAV",T,reg)=TSAVE.L(reg);
SORTIEMAC("SAVG",T,reg)=SAVE_G.L(reg);
SORTIEMAC("SAVH",T,reg)=SAVE_H.L(reg);
SORTIEMAC("SAVW",T,reg)=FSEFA_H_K.L(reg)+FSEFA_H_L.L(reg)+FSEFA_H_F.L(reg)+FSESE_H_G.L(reg)
                        -sum(sec,PHC.L(sec,reg)*HCV.L(sec,reg))-FGRS_DT_H.L(reg);
SORTIEMAC("CO2H",T,reg)=sum(secef,CTEPH(secef,reg)*HCV.L(secef,reg)*CO2CONTENT(secef));
SORTIEMAC("BALC",T,reg)=-sum(sec,IMPO.L(sec,reg)*PIMP.L(sec,reg))+FGRS_DUT_G.L(reg)
                        +sum(sec,EXPOP.L(sec,reg)*PEX.L(sec,reg));
SORTIEMAC("ENER",T,reg)=sum(sec,sum(sece,CTEP(sece,sec,reg)*IOV.L(sece,sec,reg)))+
                        sum(sece,CTEPH(sece,reg)*HCV.L(sece,reg));
SORTIEMAC("HENC",T,reg)=CTEPH("01",reg)*HCV.L("01",reg);
SORTIEMAC("HENO",T,reg)=CTEPH("02",reg)*HCV.L("02",reg);
SORTIEMAC("HENG",T,reg)=CTEPH("03",reg)*HCV.L("03",reg);
SORTIEMAC("HENP",T,reg)=CTEPH("04",reg)*HCV.L("04",reg);
SORTIEMAC("HENE",T,reg)=CTEPH("05",reg)*HCV.L("05",reg);
SORTIESEC("YD",T,sec,reg)=YD.L(sec,reg);

SORTIESEC("PY",T,sec,reg)=PY.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PD",T,sec,reg)=PD.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PB",T,sec,reg)=PB.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("XD",T,secnf,reg)=XD.L(secnf,reg);
SORTIESEC("XD",T,"05",reg)=XD.L("05",reg);
SORTIESEC("XD",T,secff,reg)=XDPF.L(secff,reg);
SORTIESEC("XD",T,"04",reg)=XDPP.L("04",reg);
SORTIESEC("PD",T,secnf,reg)=PD.L(secnf,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PD",T,"05",reg)=PD.L("05",reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PD",T,secff,reg)=PDPF.L(secff,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PD",T,"04",reg)=PDPP.L("04",reg)/SORTIEMAC("PGDP",T,reg);

SORTIESEC("LAV",T,sec,reg)=LAV.L(sec,reg);
SORTIESEC("KAV",T,sec,reg)=KAV.L(sec,reg);
SORTIESEC("FF",T,secff,reg)=FF.L(secff,reg);
SORTIESEC("PK",T,sec,reg)=PK.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("PF",T,secff,reg)=PF.L(secff,reg)/SORTIEMAC("PGDP",T,reg);

SORTIESEC("INVV",T,sec,reg)=INVV.L(sec,reg);
SORTIESEC("PINV",T,sec,reg)=PINV.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("EXP",T,sec,reg)=EXPOP.L(sec,reg);
SORTIESEC("PEX",T,sec,reg)=PEX.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("IMP",T,sec,reg)=IMPO.L(sec,reg);
SORTIESEC("PIMP",T,sec,reg)=PIMP.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("EN",T,sec,reg)=EN.L(sec,reg);
SORTIESEC("PE",T,sec,reg)=PE.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("HCV",T,sec,reg)=HCV.L(sec,reg);
SORTIESEC("PHC",T,sec,reg)=PHC.L(sec,reg)/SORTIEMAC("PGDP",T,reg);
SORTIESEC("CO2",T,sec,reg)= sum(secef,CTEP(secef,sec,reg)*IOV.L(secef,sec,reg)*CO2CONTENT(secef));
SORTIESEC("ELAR",T,sec,reg)= ((HCDTOT.L(reg)*BHCV(sec,reg))/(HCV.L(sec,reg)*PHC.L(sec,reg)))$(HCV.L(sec,reg) ne 0);
SORTIESEC("ELAP",T,sec,reg)= (-(BHCV(sec,reg)*(HCDTOT.L(reg)-sum(pro,CHCV(pro,reg)*PHC.L(pro,reg)))+
                               BHCV(sec,reg)*CHCV(sec,reg)*PHC.L(sec,reg))/(PHC.L(sec,reg)*HCV.L(sec,reg)))
                               $(HCV.L(sec,reg) ne 0);
SORTIESEC("INCO",T,sec,reg)=  CHCV(sec,reg);
SORTIESEC("ENEC",T,sec,reg)=  CTEP("01",sec,reg)*IOV.L("01",sec,reg);
SORTIESEC("ENEO",T,sec,reg)=  CTEP("02",sec,reg)*IOV.L("02",sec,reg);
SORTIESEC("ENEG",T,sec,reg)=  CTEP("03",sec,reg)*IOV.L("03",sec,reg);
SORTIESEC("ENEP",T,sec,reg)=  CTEP("04",sec,reg)*IOV.L("04",sec,reg);
SORTIESEC("ENEE",T,sec,reg)=  CTEP("05",sec,reg)*IOV.L("05",sec,reg);

SORTIEMAC("SUR1",T,reg)=HCDTOT.L(reg)-sum(sec,CHCV(sec,reg)*PHC.L(sec,reg));
SORTIEMAC("SPRI",T,reg)=prod(sec,(PHC.L(sec,reg)/PHC_CC(T,sec,reg))**BHCV(sec,reg));
SORTIEMAC("TAXE",T,reg)=TAXECO2.L(reg)/SORTIEMAC("PGDP",T,reg);
SORTIEMAC("QUOT",T,reg)=QUOTANET.L(reg);

SORTIEMAC("CRIC",T,reg)=EGHG.L("CH4_RIC","06",reg);
SORTIEMAC("CENT",T,reg)=EGHG.L("CH4_ENT","06",reg);
SORTIEMAC("CMAN",T,reg)=EGHG.L("CH4_MAN","06",reg);
SORTIEMAC("COAG",T,reg)=EGHG.L("CH4_OAG","06",reg);
SORTIEMAC("CBIC",T,reg)=EGHG.L("CH4_BIC","17",reg);
SORTIEMAC("CLAN",T,reg)=EGHG.L("CH4_LAN","17",reg);
SORTIEMAC("CCOA",T,reg)=EGHG.L("CH4_COA","01",reg);
SORTIEMAC("CFUE",T,reg)=EGHG.L("CH4_FUE","04",reg);
SORTIEMAC("CIND",T,reg)=EGHG.L("CH4_IND","09",reg);
SORTIEMAC("COIL",T,reg)=EGHG.L("CH4_OIL","02",reg);
SORTIEMAC("CWAS",T,reg)=EGHG.L("CH4_WAS","17",reg);
SORTIEMAC("COTH",T,reg)=EGHG.L("CH4_OTH","17",reg);
SORTIEMAC("CGAS",T,reg)=EGHG.L("CH4_GAS","03",reg);


SORTIEMAC("NAGS",T,reg)=EGHG.L("N2O_AGS","06",reg);
SORTIEMAC("NOAG",T,reg)=EGHG.L("N2O_OAG","06",reg);
SORTIEMAC("NBIC",T,reg)=EGHG.L("N2O_BIC","17",reg);
SORTIEMAC("NFUE",T,reg)=EGHG.L("N2O_FUE","04",reg);
SORTIEMAC("NMAN",T,reg)=EGHG.L("N2O_MAN","06",reg);
SORTIEMAC("NOTH",T,reg)=EGHG.L("N2O_OTH","17",reg);
SORTIEMAC("NOIN",T,reg)=EGHG.L("N2O_OIN","10",reg);
SORTIEMAC("NADI",T,reg)=EGHG.L("N2O_ADI","09",reg);
SORTIEMAC("NNIT",T,reg)=EGHG.L("N2O_NIT","09",reg);
SORTIEMAC("NHUM",T,reg)=EGHG.L("N2O_HUM","17",reg);

SORTIEMAC("FAEN",T,reg)=EGHG.L("PFC_AEN","09",reg);
SORTIEMAC("FAEM",T,reg)=EGHG.L("PFC_AEM","09",reg);
SORTIEMAC("FFIR",T,reg)=EGHG.L("PFC_FIR","09",reg);
SORTIEMAC("FFOA",T,reg)=EGHG.L("PFC_FOA","09",reg);
SORTIEMAC("FSOL",T,reg)=EGHG.L("PFC_SOL","09",reg);
SORTIEMAC("FHFC",T,reg)=EGHG.L("HFC_22","09",reg);
SORTIEMAC("FREF",T,reg)=EGHG.L("PFC_REF","09",reg);
SORTIEMAC("FEPS",T,reg)=EGHG.L("SF6_EPS","05",reg);
SORTIEMAC("FMAM",T,reg)=EGHG.L("SF6_MAM","10",reg);
SORTIEMAC("FPAP",T,reg)=EGHG.L("PFC_PAP","10",reg);
SORTIEMAC("FSEM",T,reg)=EGHG.L("PFC_SEM","16",reg);

iter=iter+1;


if (stop eq 1, iter=2051);

$include Gemini_code/calage.gms

*change in productivity due to change in temperature
*TGC(reg)= TGLCC(T,reg),
*TGC(reg)= 1 + TGCC(reg) * Temperature(T);


TXEXCISE(sec,reg)=TXEXCISE(sec,reg)*(PGDP_CC(T,reg)/PGDP_CC(T-1,reg));



NSUP.fx(reg)        =        NSUP.L(reg)*POP(T,reg)/POP(T-1,reg);
KAVCM1(sec,reg)     =        KAVC(sec,reg)      ;
INVVM1(sec,reg)     =        INVV.L(sec,reg)      ;
PDRM2.fx(sec,reg)   =        PDRM1.L(sec,reg)     ;
PDRM1.fx(sec,reg)   =        PDR.L(sec,reg)       ;
XDM2(sec,reg)    =           XDM1(sec,reg)      ;
XDM1(sec,reg)    =           XD.L(sec,reg)        ;
XDA(sec,reg)     =           (XDM1(sec,reg)*(XDM1(sec,reg)/XDM2(sec,reg))**2)$(XDA0(sec,reg) ne 0)+0;
KAVC(sec,reg)    =           (1-DECL(sec,reg))*KAVCM1(sec,reg)+INVVM1(sec,reg);
PPIBM1.fx(reg)      =        (sum(sec,EXPOP.L(sec,reg)*PEX.L(sec,reg)+PHC.L(sec,reg)*HCV.L(sec,reg)+PGC.L(sec,reg)*GCV.L(sec,reg)
                             +INVV.L(sec,reg)*PINV.L(sec,reg)+STV(sec,reg)*PB.L(sec,reg)*(1+TXTVAST(sec,reg))-
                             IMPO.L(sec,reg)*PIMP.L(sec,reg)))/
                            (sum(sec,EXPOP.L(sec,reg)*PEX0(sec,reg)+PHC0(sec,reg)*HCV.L(sec,reg)+PGC0(sec,reg)*GCV.L(sec,reg)
                            +INVV.L(sec,reg)*PINV0(sec,reg)+STV(sec,reg)*PB0(sec,reg)*(1+TXTVAST(sec,reg))-
                             IMPO.L(sec,reg)*PIMP0(sec,reg))) ;

CO2_TOTAL(T)=sum(reg,sum(sec,SORTIESEC("CO2",T,sec,reg))+ SORTIEMAC("CO2H",T,reg));
N2O_TOTAL(T)=sum(reg,    SORTIEMAC("NAGS",T,reg)+
SORTIEMAC("NOAG",T,reg)+
SORTIEMAC("NBIC",T,reg)+
SORTIEMAC("NFUE",T,reg)+
SORTIEMAC("NMAN",T,reg)+
SORTIEMAC("NOTH",T,reg)+
SORTIEMAC("NOIN",T,reg)+
SORTIEMAC("NADI",T,reg)+
SORTIEMAC("NNIT",T,reg)+
SORTIEMAC("NHUM",T,reg));
CH4_TOTAL(T)=sum(reg,    SORTIEMAC("CRIC",T,reg)+
                         SORTIEMAC("CENT",T,reg)+
                         SORTIEMAC("CMAN",T,reg)+
                         SORTIEMAC("COAG",T,reg)+
                         SORTIEMAC("CBIC",T,reg)+
                         SORTIEMAC("CLAN",T,reg)+
                         SORTIEMAC("CCOA",T,reg)+
                         SORTIEMAC("CFUE",T,reg)+
                         SORTIEMAC("CIND",T,reg)+
                         SORTIEMAC("COIL",T,reg)+
                         SORTIEMAC("CWAS",T,reg)+
                         SORTIEMAC("COTH",T,reg)+
                         SORTIEMAC("CGAS",T,reg));

TAXECO2MT(T,reg)= 1000*TAXECO2.L(reg)/SORTIEMAC("PGDP",T,reg);

TAXECO2M(reg)=(TAXECO2MT(T-3,reg)+TAXECO2MT(T-2,reg)+TAXECO2MT(T-1,reg)+TAXECO2MT(T,reg))/4;

COUTUABAT(ghg,sec,reg)=0;ABAT(ghg,sec,reg)=0;
IA(reg)=0;IB(reg)=0;FB(ghg,reg)=0;FA(ghg,reg)=0;

loop(reg, if (TAXECO2M(reg) lt 0, TAXECO2M(reg)=0;));

if (iter gt 2009,
  loop(reg,

     loop(iin,
        if (TAXECO2M(reg) ge IORD(iin), IA(reg)=IORD(iin);FA(ghg,reg)=FABAT(iin,ghg,reg);
                       if (ord(iin) ne card(iin),IB(reg)=IORD(iin+1);FB(ghg,reg)=FABAT(iin+1,ghg,reg);
                       else IB(reg)=IORD("22");FB(ghg,reg)=FABAT("22",ghg,reg);)
            );
          );

        If ((IA(reg) ne 0) or (IB(reg) ne 0),
             if (IA(reg) eq IB(reg),ABAT(ghg,sec,reg)=FA(ghg,reg);
             else ABAT(ghg,sec,reg)=FA(ghg,reg)+(TAXECO2M(reg)-IA(reg))*
                                                              (FB(ghg,reg)-FA(ghg,reg))/(IB(reg)-IA(reg))));
       );

* Calcul du cout d'Abatement
loop(reg,loop(ghg,
       loop( iin,
         if (iord(iin) lt IA(reg),
         COUTUABAT(ghg,sec,reg)=COUTUABAT(ghg,sec,reg)+(IORD(iin)-IORD("10"))*(FABAT(iin+1,ghg,reg)-
         FABAT(iin,ghg,reg))+(IORD(iin+1)-IORD(iin))*(FABAT(iin+1,ghg,reg)-FABAT(iin,ghg,reg))/2;)

            );

         COUTUABAT(ghg,sec,reg)=COUTUABAT(ghg,sec,reg)+(IA(reg)-IORD("10"))*(ABAT(ghg,sec,reg)-
                     FA(ghg,reg))+(TAXECO2M(reg)-IA(reg))*(ABAT(ghg,sec,reg)-FA(ghg,reg))/2;


 );););
COUTUABAT(ghg,sec,reg)=COUTUABAT(ghg,sec,reg)*SORTIEMAC("PGDP",T,reg);


loop(reg, if (TAXECO2M(reg) le 0, COUTUABAT(ghg,sec,reg)=0; ABAT(ghg,sec,reg)=0;));

) ;
);


put infoiter system.date,' ',system.time;put /;
putclose infoiter;


display TGL,TGC,Temperature;

*$include Gemini_code/sortiecc.gms
$include Gemini_code/sortievar.gms

display CO2_TOTAL;
display N2O_TOTAL;
display CH4_TOTAL;

file CO2_results /Gemini_CO2_results.txt/;
put CO2_results;
loop(T,if(ord(T) gt 11, if(ord(T) lt 62,
     put CO2_TOTAL(T)/ ))
);
putclose;

file N2O_results /Gemini_NO2_results.txt/;
put N2O_results;
loop(T,if(ord(T) gt 11, if(ord(T) lt 62,
     put N2O_TOTAL(T)/ ))
);

putclose;


file CH4_results /Gemini_CH4_results.txt/;
put CH4_results;
loop(T,if(ord(T) gt 11, if(ord(T) lt 62,
     put CH4_TOTAL(T)/ ))
);

putclose;