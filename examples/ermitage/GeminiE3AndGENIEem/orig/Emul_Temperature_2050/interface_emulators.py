"""
Run : python interface_emulators.py concent-tiam.txt
Input : profil de concentration dans concent-tiam.txt + temperature de tiam en 2030, 2050 et 2100.
Output : Resultats des emulateurs dans outfile.txt
"""
from math import log
import array
import csv
import struct
import sys
import os.path
import numpy
from numpy import matrix, mat, zeros, sqrt
import random

dirname = "C:\Users\bafre\Documents\Emul_Temperature_2050"

file = sys.argv[1]
reader = csv.reader(open(file,"rb"))  
list_reader = []
for row in reader:
  list_reader.append(row[0])

# On place les concentrations dans une liste nommee concent
n = len(list_reader) + 1
concent_co2 = zeros(n)
time = zeros(n)
for i in xrange(n-1):
  line = list_reader[i].split(';')
  time[i] = line[0]
  concent_co2[i] = line[1]

time[n-1] = 2100
concent_co2[n-1] = concent_co2[n-2]

# On calcule les coefficients de chebitchev
#time = zeros(n) - 1
#for i in xrange(n-1):
#  time[i+1] = time[i] + 0.02

for i in xrange(n):
  time[i] = -1 + (time[i] - 2005)/50 

poly1 = zeros(n)
poly2 = zeros(n)
poly3 = zeros(n)
a11 = 0
a22 = 0
a33 = 0
a12 = 0
a13 = 0
a23 = 0
b = zeros(3)
for i in xrange(n):
  poly1[i] = time[i] + 1
  poly2[i] = 2 * time[i]**2 - 2
  poly3[i] = 4 * time[i]**3 - 4 * time[i]
  a11 = a11 + poly1[i]**2
  a22 = a22 + poly2[i]**2
  a33 = a33 + poly3[i]**2
  a12 = a12 + poly1[i]*poly2[i]
  a13 = a13 + poly1[i]*poly3[i]
  a23 = a23 + poly2[i]*poly3[i]
  b[0] = b[0] + poly1[i] * (concent_co2[i] - concent_co2[0])
  b[1] = b[1] + poly2[i] * (concent_co2[i] - concent_co2[0])
  b[2] = b[2] + poly3[i] * (concent_co2[i] - concent_co2[0])

e11 = (poly1[n-1]**2)/2
e21 = (poly2[n-1]*poly1[n-1])/2
e31 = (poly3[n-1]*poly1[n-1])/2

if e21 == 0:
  e21 = 1e-6
if e31 == 0:
  e31 = 1e-6

e12 = poly1[n-1]*a12 - poly2[n-1]*a11
e13 = poly1[n-1]*a13 - poly3[n-1]*a11
f1 = poly1[n-1]*b[0] - (concent_co2[n-1] - concent_co2[0]) * a11

e22 = poly1[n-1]*a22 - poly2[n-1]*a12
e23 = poly1[n-1]*a23- poly3[n-1]*a12
f2 = poly1[n-1]*b[1] - (concent_co2[n-1] - concent_co2[0]) * a12

e32 = poly1[n-1]*a23 - poly2[n-1]*a13
e33 = poly1[n-1]*a33- poly3[n-1]*a13
f3 =  poly1[n-1]*b[2] - (concent_co2[n-1] - concent_co2[0]) * a13
  
c11 = e12*e21 - e11*e22
c12 = e13*e21 - e11*e23
c21 = e12*e31 - e11*e32
c22 = e13*e31 - e33*e11
d1 = f1*e21 - f2*e11
d2 = f1*e31 - f3*e11

tcheb = zeros(3)
tcheb[2] = (d1*c21 - d2*c11)/(c12*c21 - c11*c22)
tcheb[1] = (d2 - c22 * tcheb[2]) / c21 
delta = (f1 - e12 * tcheb[1] - e13 * tcheb[2]) / e11  
tcheb[0] = ((concent_co2[n-1] - concent_co2[0]) - tcheb[1] * poly2[n-1] - tcheb[2] * poly3[n-1]) / poly1[n-1]

tcheb[0] = 2*tcheb[0]
tcheb[1] = 2*tcheb[1]
tcheb[2] = 2*tcheb[2]


# Ecriture des concentrations approximees dans le fichier ".\concent_approx.txt"
c = open("Emul_Temperature_2050/concent_approx.txt", "wb")
for i in xrange(n):
  c.write(str(0.5* (poly1[i]* tcheb[0] + poly2[i] * tcheb[1] + poly3[i] * tcheb[2]) + concent_co2[0]) + "\n")
c.close()


# Ecriture des coefficients dans le fichier ".\coeffs.txt"
c = open( "Emul_Temperature_2050/coeffs.txt", "wb")
c.write("TC1,TC2,TC3 \n")
c.write(str(tcheb[0]) + "," + str(tcheb[1]) + "," + str(tcheb[2])+ "\n\n")
c.close()

# Lancement des emulateurs
os.system("R --no-restore --no-save  <Emul_Temperature_2050/emul.R > Emul_Temperature_2050/out.txt")
