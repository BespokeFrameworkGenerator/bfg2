# python main.py 1.5 0.01 0 2010 1 0 
#
# Input:  1 -> temperature rise target
#         2 -> precision of the resulting increase temperature
#         3 -> quantile target: (1) activated (0) default average target
#         4 -> starting date for the climate polica
#         5 -> enable GEMINI-E3 run
#         6 -> linear (1) or smooth trajectory (0)

from math import log
import array
import csv
import struct
import sys
import os.path
import numpy
from numpy import matrix, mat, zeros, sqrt
import random
from compute_trajectory import get_smooth_traj, get_linear_traj

dirname = "C:\Users\bafre\Documents\Emul_Temperature_2050"

target = float(sys.argv[1]) - 0.76
precision = float(sys.argv[2])
quantile_target = int(sys.argv[3]) 
fixed_year = int(sys.argv[4])
gemini_input = float(sys.argv[5])
linear = float(sys.argv[6])
Temperature_increase = 1000


##  # Fixed emissions for the period 2001-2010
##  CO2_base = (6502.84, 6591.18, 6721.44, 6858.11, 7016.75, 7025.08, 7089.35, 7180.49, 7290.98, 7410.71)
##  N2O_base = (849.21, 861.65, 874.15, 885.13, 896.08, 908.55, 921.65, 934.81, 947.23, 958.60)
##  CH4_base = (1641.86, 1667.54, 1694.23, 1720.02, 1747.50, 1766.54, 1792.35, 1820.69, 1848.84, 1875.04)
##
##  # Max emissions for 2050 from BAU
##  CO2_2050_max = 13000.0
##  N2O_2050_max = 1598.0
##  CH4_2050_max = 3264.0

if gemini_input == 1:
  # Gemini-E3 run without emissions constraints
  os.system("gams ./Gemini_code/Variante_BAU.gms")

nb_fixed_year = fixed_year - 2001
  
CO2_base = zeros(nb_fixed_year)
N2O_base = zeros(nb_fixed_year)
CH4_base = zeros(nb_fixed_year)
# Retrieving emissions from Gemini-e3
reader1 = csv.reader(open("./Gemini_CO2_results.txt","rb"))
reader2 = csv.reader(open("./Gemini_NO2_results.txt","rb"))
reader3 = csv.reader(open("./Gemini_CH4_results.txt","rb"))
list_reader1 = []
list_reader2 = []
list_reader3 = []
for row in reader1:
  list_reader1.append(row[0])
for row in reader2:
  list_reader2.append(row[0])
for row in reader3:
  list_reader3.append(row[0])
for i in xrange(nb_fixed_year):
  CO2_base[i] = float(list_reader1[i])
  N2O_base[i] = float(list_reader2[i])
  CH4_base[i] = float(list_reader3[i])

# Max emissions for 2050 from BAU
CO2_2050_max = float(list_reader1[49])
N2O_2050_max = float(list_reader2[49])
CH4_2050_max = float(list_reader3[49])


# Min possible emissions in 2050
CO2_2050_min = -CO2_2050_max
N2O_2050_min = -N2O_2050_max
CH4_2050_min = -CH4_2050_max

# Setting current emissions in 2050 to max
CO2_2050_current = (CO2_2050_max + CO2_2050_min)/2
N2O_2050_current = (N2O_2050_max + N2O_2050_min)/2
CH4_2050_current = (CH4_2050_max + CH4_2050_min)/2
#CO2_2050_current = CO2_2050_max
#N2O_2050_current = N2O_2050_max
#CH4_2050_current = CH4_2050_max


while (abs(Temperature_increase - target) > precision):
  # Construction of input files for the climate module 
  c1 = open("CO2sum_data.gms", "wb")
  c2 = open("N2Osum_data.gms", "wb")
  c3 = open("CH4sum_data.gms", "wb")
  c1.write("parameter CO2sum(T)/\n")
  c2.write("parameter N20sum(T)/\n")
  c3.write("parameter CH4sum(T)/\n")

  if linear == 0:
    emissions_CO2 = get_smooth_traj(CO2_base, CO2_2050_current, nb_fixed_year);
    emissions_N2O = get_smooth_traj(N2O_base, N2O_2050_current, nb_fixed_year);
    emissions_CH4 = get_smooth_traj(CH4_base, CH4_2050_current, nb_fixed_year);
  else:
    emissions_CO2 = get_linear_traj(CO2_base, CO2_2050_current, nb_fixed_year);
    emissions_N2O = get_linear_traj(N2O_base, N2O_2050_current, nb_fixed_year);
    emissions_CH4 = get_linear_traj(CH4_base, CH4_2050_current, nb_fixed_year);    
  for i in xrange(50):
    if i <= nb_fixed_year - 1:
      c1.write(str(2001+i) + "    " + str(CO2_base[i]) + "\n")
      c2.write(str(2001+i) + "    " + str(N2O_base[i]) + "\n")
      c3.write(str(2001+i) + "    " + str(CH4_base[i]) + "\n")
    else:
      c1.write(str(2001+i) + "    " + str(emissions_CO2[i]) + "\n")
      c2.write(str(2001+i) + "    " + str(emissions_N2O[i]) + "\n")
      c3.write(str(2001+i) + "    " + str(emissions_CH4[i]) + "\n")
  c1.write("/;")
  c2.write("/;")
  c3.write("/;")
  c1.close()
  c2.close()
  c3.close()

  # Launch Climate module
  os.system("gams ./Climate_module/Climate_module2.1.gms")

  # Launch emulator
  os.system("python ./Emul_Temperature_2050/interface_emulators.py concent-gemini.txt")


  # Quantile target option or average temparture target
  if quantile_target == 1: 
    reader = csv.reader(open("Emul_Temperature_2050/SAT_quant.dat","rb"))
    list_reader = []
    for row in reader:
      list_reader.append(row[0])
    line = list_reader[1].split(' ')
    Temperature_increase  = float(line[0])
  else:
    # Retrieve average tempetaure increase from the emulator
    reader = csv.reader(open("Emul_Temperature_2050/outfile_genie2.txt","rb"))			  
    list_reader = []
    for row in reader:
      list_reader.append(row[0])
    line = list_reader[0].split(' ')
    Temperature_increase  = float(line[1])

  print Temperature_increase + 0.76

  # Update the target emissions (min, max and current) of CO2, N2O and CH4 in 2050
  if abs(Temperature_increase - target) > precision:
    if Temperature_increase > target:
      CO2_2050_max = CO2_2050_current
      N2O_2050_max = N2O_2050_current
      CH4_2050_max = CH4_2050_current
    else:
      CO2_2050_min = CO2_2050_current
      N2O_2050_min = N2O_2050_current
      CH4_2050_min = CH4_2050_current
    CO2_2050_current = CO2_2050_min + 0.5 * (CO2_2050_max - CO2_2050_min)
    N2O_2050_current = N2O_2050_min + 0.5 * (N2O_2050_max - N2O_2050_min)
    CH4_2050_current = CH4_2050_min + 0.5 * (CH4_2050_max - CH4_2050_min)

# Build input file for Gemini-e3
c1 = open("CO2sum_data.gms", "wb")
c2 = open("N2Osum_data.gms", "wb")
c3 = open("CH4sum_data.gms", "wb")
c1.write("parameter CO2sum(T)/\n")
c2.write("parameter N20sum(T)/\n")
c3.write("parameter CH4sum(T)/\n")
for i in xrange(50):
  if i <= nb_fixed_year - 1:
    c1.write(str(2001+i) + "    " + str(100000) + "\n")
    c2.write(str(2001+i) + "    " + str(100000) + "\n")
    c3.write(str(2001+i) + "    " + str(100000) + "\n")
  else:
    c1.write(str(2001+i) + "    " + str(emissions_CO2[i]) + "\n")
    c2.write(str(2001+i) + "    " + str(emissions_N2O[i]) + "\n")
    c3.write(str(2001+i) + "    " + str(emissions_CH4[i]) + "\n")
c1.write("/;")
c2.write("/;")
c3.write("/;")
c1.close()
c2.close()
c3.close()
    

# Code for impact of climate change 
#os.system("R --no-restore --no-save  <Emul_Temperature_2030/emul.R > Emul_Temperature_2030/out.txt")
# Retrieving tempetaure increase from the emulator
#reader = csv.reader(open("Emul_Temperature_2030/outfile_genie2.txt","rb"))		  
#list_reader = []
#for row in reader:
#  list_reader.append(row[0])
#line = list_reader[0].split(' ')
#Temperature_increase_30  = float(line[1])

#c = open("temperatures.gms", "wb")
#c.write("scalar Temp50 / " + str(Temperature_increase + 0.76) + "/;\n\n")
#c.write("scalar Temp30 / " + str(Temperature_increase_30 + 0.76) + "/;\n")
#c.close()

# Gemini-E3 run with emissions constraints
if gemini_input == 1:
  os.system("gams ./Gemini_code/Variante.gms")

