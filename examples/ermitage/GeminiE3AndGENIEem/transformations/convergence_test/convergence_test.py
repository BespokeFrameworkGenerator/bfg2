class convergence_test(object):

    def __init__(self,target,precision):
        print "convergence test transformation:init"
        print("[convergence_test] constructed with target: {0} precision: {1}".format(target, precision))
        self.target=target
        self.precision=precision
	self.f = open("converge.out", "w")
        print "convergence_test:constructor:precision",self.precision
        print "convergence_test:constructor:target",self.target

    def continue1(self,value):
        print("[continue1] value: {0} target: {1} precision: {2}".format(value, self.target, self.precision))
	self.f.write("[continue1] value: {0} target: {1} precision: {2}\n".format(value, self.target, self.precision))
	self.f.flush()
        if abs(value - self.target) > self.precision : return True
        else : return False

    # two versions of this method needed as we do not yet support multiple instances of ep's
    def continue2(self,value):
        print("[continue2] value: {0} target: {1} precision: {2}".format(value, self.target, self.precision))
	self.f.write("[continue2] value: {0} target: {1} precision: {2}\n".format(value, self.target, self.precision))
	self.f.flush()
        if abs(value - self.target) > self.precision : return True
        else : return False
# Added
    def converged1(self,value):
        print("[converged1] value: {0} target: {1} precision: {2}".format(value, self.target, self.precision))
	self.f.write("[converged1] value: {0} target: {1} precision: {2}\n".format(value, self.target, self.precision))
	self.f.flush()
        if abs(value - self.target) > self.precision : return False
        else : return True

    def converged2(self,value):
        print("[converged2] value: {0} target: {1} precision: {2}".format(value, self.target, self.precision))
	self.f.write("[converged2] value: {0} target: {1} precision: {2}\n".format(value, self.target, self.precision))
	self.f.flush()
        if abs(value - self.target) > self.precision : return False
        else : return True

