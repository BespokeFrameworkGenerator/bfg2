from numpy import zeros
import csv
from compute_trajectory import get_smooth_traj, get_linear_traj

class emissions_control(object):

    def __init__(self,nb_fixed_year,target,precision,linear,CO2,N2O,CH4):

#        fp = open("emissions_control_init.out", "a")
#        fp.write("[emissions_control() constructed with {0} {1} {2} {3}\n{4}\n{5}\n{6}\n".format(nb_fixed_year,target,precision,linear,CO2,N2O,CH4))

        self.nb_fixed_year=nb_fixed_year
        self.target=target
        self.precision=precision
        self.linear=linear

        self.CO2_base = CO2
        self.N2O_base = N2O
        self.CH4_base = CH4

        # Max emissions for 2050 from BAU
        self.CO2_2050_max = CO2[49]
        self.N2O_2050_max = N2O[49]
        self.CH4_2050_max = CH4[49]

#        fp.write("self.CO2_2050_max: {0}\tself.N20_2050_max: {1}\tself.CH4_2050_max: {2}".format( CO2[49], N2O[49], CH4[49] ))

        # Min possible emissions in 2050
        self.CO2_2050_min = -self.CO2_2050_max
        self.N2O_2050_min = -self.N2O_2050_max
        self.CH4_2050_min = -self.CH4_2050_max

        # Setting current emissions in 2050 to max
        self.CO2_2050_current = (self.CO2_2050_max + self.CO2_2050_min)/2
        self.N2O_2050_current = (self.N2O_2050_max + self.N2O_2050_min)/2
        self.CH4_2050_current = (self.CH4_2050_max + self.CH4_2050_min)/2

        self.emissions_CO2=zeros(50)
        self.emissions_N2O=zeros(50)
        self.emissions_CH4=zeros(50)

#        fp.close()

    def get_emissions(self):

        CO2=zeros(50)
        N2O=zeros(50)
        CH4=zeros(50)

#        fp = open("emissions_control_init.out", "a")
#        fp.write("[get_emissions() has {0} {1} {2} {3} {4} {5} {6}\n".format(self.nb_fixed_year,self.target,self.precision,self.linear,self.CO2_base,self.N2O_base,self.CH4_base))
#        fp.write("[get_emissions() initially:\nself.CO2: {0}\nself.N2O: {1}\nself.CH4: {2}\n".format(self.CO2_base, self.N2O_base, self.CH4_base))

        if self.linear == 0:
              self.emissions_CO2 = get_smooth_traj(self.CO2_base, self.CO2_2050_current, self.nb_fixed_year)
              self.emissions_N2O = get_smooth_traj(self.N2O_base, self.N2O_2050_current, self.nb_fixed_year)
              self.emissions_CH4 = get_smooth_traj(self.CH4_base, self.CH4_2050_current, self.nb_fixed_year)
        else:
            self.emissions_CO2 = get_linear_traj(self.CO2_base, self.CO2_2050_current, self.nb_fixed_year)
            self.emissions_N2O = get_linear_traj(self.N2O_base, self.N2O_2050_current, self.nb_fixed_year)
            self.emissions_CH4 = get_linear_traj(self.CH4_base, self.CH4_2050_current, self.nb_fixed_year)

        for i in xrange(50):
            if i <= (self.nb_fixed_year - 1):
                CO2[i]=self.CO2_base[i]
                N2O[i]=self.N2O_base[i]
                CH4[i]=self.CH4_base[i]
            else:
                CO2[i]=self.emissions_CO2[i]
                N2O[i]=self.emissions_N2O[i]
                CH4[i]=self.emissions_CH4[i]


#        fp.write("[get_emissions() returns:\nCO2: {0}\nN2O: {1}\nCH4: {2}\n".format(CO2, N2O, CH4))
#        fp.close()

        return CO2,N2O,CH4

    def update_2050_emissions(self,Temperature_increase):

        if abs(Temperature_increase - self.target) > self.precision:
            if Temperature_increase > self.target:
                self.CO2_2050_max = self.CO2_2050_current
                self.N2O_2050_max = self.N2O_2050_current
                self.CH4_2050_max = self.CH4_2050_current
            else:
                self.CO2_2050_min = self.CO2_2050_current
                self.N2O_2050_min = self.N2O_2050_current
                self.CH4_2050_min = self.CH4_2050_current

            self.CO2_2050_current = self.CO2_2050_min + 0.5 * (self.CO2_2050_max - self.CO2_2050_min)
            self.N2O_2050_current = self.N2O_2050_min + 0.5 * (self.N2O_2050_max - self.N2O_2050_min)
            self.CH4_2050_current = self.CH4_2050_min + 0.5 * (self.CH4_2050_max - self.CH4_2050_min)

#    def getFinalValues(self,CO2_IN,N2O_IN,CH4_IN):
    def getFinalValues(self):

        CO2=zeros(50)
        N2O=zeros(50)
        CH4=zeros(50)

        for i in xrange(50):
            if i <= (self.nb_fixed_year - 1):
                CO2[i]=100000
                N2O[i]=100000
                CH4[i]=100000
            else:
                CO2[i]=self.emissions_CO2[i]
                N2O[i]=self.emissions_N2O[i]
                CH4[i]=self.emissions_CH4[i]

        print "emissions_control:getFinalValues"
        print "CO2",CO2
        print "N20",N2O
        print "CH4",CH4
        return CO2,N2O,CH4
