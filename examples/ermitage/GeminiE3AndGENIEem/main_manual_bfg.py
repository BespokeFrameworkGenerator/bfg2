# python main.py 1.5 0.01 0 2010 1 0 
#
# Input:  1 -> temperature rise target
#         2 -> precision of the resulting increase temperature
#         3 -> quantile target: (1) activated (0) default average target
#         4 -> starting date for the climate polica
#         5 -> enable GEMINI-E3 run
#         6 -> linear (1) or smooth trajectory (0)

from math import log
import array
import csv
import struct
import sys
import os.path
import numpy
from numpy import matrix, mat, zeros, sqrt
import random
import subprocess

from gemini_e3_bau import gemini_e3_bau
from gemini_e3_bau_filereader import gemini_e3_bau_filereader
from em2conc import em2conc
from genie_em_model import genie_em_model
from gemini_e3_clim import gemini_e3_clim

from convergence_test import convergence_test
from emissions_control import emissions_control

dirname = "C:\Users\bafre\Documents\Emul_Temperature_2050"

target = float(sys.argv[1]) - 0.76
precision = float(sys.argv[2])
quantile_target = int(sys.argv[3]) 
fixed_year = int(sys.argv[4])
gemini_input = float(sys.argv[5])
linear = float(sys.argv[6])
Temperature_increase = 1000
base_year=2001
final_year=2050
nb_fixed_year = fixed_year - base_year
numYears=(final_year-base_year)+1

gemini_e3_bau_instance=gemini_e3_bau()
gemini_e3_bau_filereader_instance=gemini_e3_bau_filereader()
converge_instance=convergence_test(target,precision)
em2conc_instance=em2conc(nb_fixed_year,base_year)
genie_em_instance=genie_em_model()
gemini_e3_clim_instance=gemini_e3_clim(base_year,numYears)

if gemini_input == 1:
  # run initial unconstrained gemini model
  os.chdir(os.path.dirname(sys.modules["gemini_e3_bau"].__file__))
  CO2base,N2Obase,CH4base=gemini_e3_bau_instance.run(numYears)
else:
  # read initial data from files
  os.chdir(os.path.dirname(sys.modules["gemini_e3_bau_filereader"].__file__))
  CO2base,N2Obase,CH4base=gemini_e3_bau_filereader_instance.run(numYears)

emissions_control_instance=emissions_control(nb_fixed_year,target,precision,linear,CO2base,N2Obase,CH4base)

# initial convergence check
os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
cont=converge_instance.converged1(Temperature_increase)

while (cont):

  # get current emissions
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  CO2,N2O,CH4=emissions_control_instance.get_emissions()

  # run em2conc model with current emissions
  os.chdir(os.path.dirname(sys.modules["em2conc"].__file__))
  time,concent_co2=em2conc_instance.run(CO2,N2O,CH4)

  # run genie with new concentrations
  os.chdir(os.path.dirname(sys.modules["genie_em_model"].__file__))
  quantile_temp_inc,av_temp_inc=genie_em_instance.run(time,concent_co2)

  # Quantile target option or average temparture target
  if quantile_target == 1: 
    Temperature_increase  = quantile_temp_inc
  else:
    Temperature_increase  = av_temp_inc

  print Temperature_increase + 0.76

  # update emissions 2050 target
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  emissions_control_instance.update_2050_emissions(Temperature_increase)

  # convergence check
  os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
  cont=converge_instance.converged2(Temperature_increase)

if gemini_input == 1:

  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  CO2,N2O,CH4=emissions_control_instance.getFinalValues(CO2,N2O,CH4)

  # run final constrained gemini model
  os.chdir(os.path.dirname(sys.modules["gemini_e3_clim"].__file__))
  gemini_e3_clim_instance.run(CO2,N2O,CH4)
