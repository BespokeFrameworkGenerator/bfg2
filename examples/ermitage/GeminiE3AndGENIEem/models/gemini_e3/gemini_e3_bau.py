import os
import sys
import subprocess
import csv

from numpy import zeros


class gemini_e3_bau(object):

    def run(self,nYears,fileReader=False):

        print "[python] started gemini_e3_bau:run"

        if not fileReader:
            args = ["gams", "./Variante_BAU.gms"]

            proc = subprocess.Popen(args, env=os.environ, close_fds=True)
            proc.wait()

            if proc.returncode:
                print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
                sys.exit(proc.returncode)



        CO2 = zeros(nYears)
        N2O = zeros(nYears)
        CH4 = zeros(nYears)
        # Retrieving emissions from Gemini-e3
        reader1 = csv.reader(open("./Gemini_CO2_results.txt","rb"))
        reader2 = csv.reader(open("./Gemini_NO2_results.txt","rb"))
        reader3 = csv.reader(open("./Gemini_CH4_results.txt","rb"))
        list_reader1 = []
        list_reader2 = []
        list_reader3 = []
        for row in reader1:
            list_reader1.append(row[0])
        for row in reader2:
            list_reader2.append(row[0])
        for row in reader3:
            list_reader3.append(row[0])
        for i in xrange(nYears):
            CO2[i] = float(list_reader1[i])
            N2O[i] = float(list_reader2[i])
            CH4[i] = float(list_reader3[i])

        print "[python] Completed gemini_e3_bau:run"

        return CO2,N2O,CH4
