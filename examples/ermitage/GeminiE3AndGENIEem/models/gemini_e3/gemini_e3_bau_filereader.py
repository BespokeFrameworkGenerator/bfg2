from gemini_e3_bau import gemini_e3_bau
class gemini_e3_bau_filereader:

    def run(self,nYears):

        gemini_e3_bau_instance=gemini_e3_bau()
        return gemini_e3_bau_instance.run(nYears,fileReader=True)
