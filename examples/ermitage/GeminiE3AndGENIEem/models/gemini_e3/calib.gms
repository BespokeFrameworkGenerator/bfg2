$TITLE  CALIBRATION DU MODELE GEMINI-E3

$ONTEXT


-------------------------------------------------------------------------
GEMINI-E3 Model
Toulouse, July 2002
Vielle Marc : mvielle@cict.fr
--------------------------------------------------------------------------



$OFFTEXT


$sysinclude gams-f
$exit

Parameter


sig    elasticity of substitution

X        factor of agregation
X1       factor 1 in volume
X2       factor 2 in volume
X3       factor 3 in volume
X4       factor 4 in volume
X5       factor 5 in volume
X6       factor 6 in volume
X7       factor 7 in volume
X8       factor 8 in volume
X9       factor 9 in volume
X10      factor 10 in volume


P        price of fator  X
P1       price of factor X1
P2       price of factor X2
P3       price of factor X3
P4       price of factor X4
P5       price of factor X5
P6       price of factor X6
P7       price of factor X7
P8       price of factor X8
P9       price of factor X9
P10      price of factor X10

;


*   CREATION DES FUNCTION POUR LA CALIBRATION

* CES a deux facteurs

alpha1CES2(X1,X2,P1,P2,sig) == X1*P1**sig/(X1*P1**sig+X2*P2**sig);
alpha2CES2(X1,X2,P1,P2,sig) == X2*P2**sig/(X1*P1**sig+X2*P2**sig);
lambdaCES2(X1,X2,P1,P2,sig,X,P) == P*( (X1*P1**sig+X2*P2**sig)/(P*X) )
                                            **(1/(1-sig));


* CES a trois facteurs

alpha1CES3(X1,X2,X3,P1,P2,P3,sig) == X1*P1**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig);
alpha2CES3(X1,X2,X3,P1,P2,P3,sig) == X2*P2**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig);
alpha3CES3(X1,X2,X3,P1,P2,P3,sig) == X3*P3**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig);
lambdaCES3(X1,X2,X3,P1,P2,P3,sig,X,P) == P*( (X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig)/(P*X) )
                                            **(1/(1-sig));



* CES a quatre facteurs

alpha1CES4(X1,X2,X3,X4,P1,P2,P3,P4,sig) == X1*P1**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig+X4*P4**sig);
alpha2CES4(X1,X2,X3,X4,P1,P2,P3,P4,sig) == X2*P2**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig+X4*P4**sig);
alpha3CES4(X1,X2,X3,X4,P1,P2,P3,P4,sig) == X3*P3**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig+X4*P4**sig);
alpha4CES4(X1,X2,X3,X4,P1,P2,P3,P4,sig) == X4*P4**sig/(X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig+X4*P4**sig);
lambdaCES4(X1,X2,X3,X4,P1,P2,P3,P4,sig,X,P) == P*( (X1*P1**sig+X2*P2**sig
                                             +X3*P3**sig+X4*P4**sig)/(P*X) )
                                            **(1/(1-sig));


* CES a six facteurs

alpha1CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X1*P1**sig/(X1*P1**sig+
                                                       X2*P2**sig+X3*P3**sig+
                                                       X4*P4**sig+X5*P5**sig+
                                                       X6*P6**sig);


alpha2CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X2*P2**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+
                                                   X4*P4**sig+X5*P5**sig+
                                                   X6*P6**sig);

alpha3CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X3*P3**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+
                                                   X4*P4**sig+X5*P5**sig+
                                                   X6*P6**sig);

alpha4CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X4*P4**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+
                                                   X4*P4**sig+X5*P5**sig+
                                                   X6*P6**sig);

alpha5CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X5*P5**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+
                                                   X4*P4**sig+X5*P5**sig+
                                                   X6*P6**sig);

alpha6CES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig) == X6*P6**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+
                                                   X4*P4**sig+X5*P5**sig+
                                                   X6*P6**sig);

lambdaCES6(X1,X2,X3,X4,X5,X6,P1,P2,P3,P4,P5,P6,sig,X,P) ==   P*( (X1*P1**sig+X2*P2**sig+
                                                      X3*P3**sig+X4*P4**sig+
                                                      X5*P5**sig+X6*P6**sig
                                                      )/(P*X) )**(1/(1-sig));





* CES a dix facteurs

al1CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig)==
                 X1*P1**sig/(X1*P1**sig+X2*P2**sig+X3*P3**sig+
                             X4*P4**sig+X5*P5**sig+X6*P6**sig+
                             X7*P7**sig+X8*P8**sig+X9*P9**sig+
                             X10*P10**sig);

al2CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X2*P2**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al3CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X3*P3**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al4CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X4*P4**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al5CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X5*P5**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al6CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X6*P6**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al7CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X7*P7**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al8CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X8*P8**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al9CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X9*P9**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

al10CES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig) == X10*P10**sig/(X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig);

lambCES10(X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,sig,X,P) ==   P*( (X1*P1**sig+
                                                   X2*P2**sig+X3*P3**sig+X4*P4**sig+X5*P5**sig+X6*P6**sig+
                                                       X7*P7**sig+X8*P8**sig+X9*P9**sig+X10*P10**sig)/(P*X)
                                                     )**(1/(1-sig));





$include dataV5_Ermitage.gms


parameter


CO2CONTENT(secef)
/
01  1.123
02  0.83
03  0.653
04  0.83
/;



parameter
EX0(reg)                 exchange rate
INVVM10(sec,reg)         investment by sector
PINV0(sec,reg)           price of investment
ELASREV(sec,reg)         Elasticite revenu
FRISCH(reg)              Frish parameter
CHCV0(sec,reg)           Incompressible consumption
GCVTOT0(reg)             Total government consumption
HCDTOT0(reg)             Total government consumption
NSUP0                    supply of labor
PB0(sec,reg)             Base price
PM0(sec,reg)             Price of material
PE0(sec,reg)             Price of energy
PEF0(sec,reg)            Price of fossil fuel energy
PTRA0(sec,reg)           Price of transport
POTRA0(sec,reg)          Price of intermediaite  minus transport
W0(reg)                  wage
PHC0(sec,reg)            Price of consumption
PL0(sec,reg)             Price of labor
PF0(secff,reg)           price of fix factor
PGC0(sec,reg)            Price of government consumption
PEX0(sec,reg)            Price of exports
RLTL0(reg)               interest rate
growth0(reg)             growth rate
KTOT0(reg)               Total capital
INVV0(sec,reg)           investment by sector
KAV0(sec,reg)            Capital
KAVC0(sec,reg)           fix capital
KAVCM10(sec,reg)         fix capital t-1
PK0(sec,reg)             Rental price of capital
CK0(sec,reg)             cost of capital
XD0(sec,reg)             Domestic production
XDPF0(sec,reg)         Domestic production including fix factor
XDPP0(sec,reg)         Domestic production including crude oil
XDM10(sec,reg)           Domestic production t-1
XDM20(sec,reg)           Domestic production t-2
XDA0(sec,reg)            anticipated Domestic production
EN0(sec,reg)             Energy
MA0(sec,reg)             Material
ENF0(sec,reg)            Fossil fuel energy
PD0(sec,reg)             Price of domestic production
PDR0(sec,reg)            price of ratio PD on PINV
PDRA0(sec,reg)           anticipated price of PDR
PDRM10(sec,reg)          anticipated price of PDR t-1
PDRM20(sec,reg)          anticipated price of PDR t-2
KAVO0(sec,reg)           optimal capital
KAVA0(sec,reg)           anticipated capital
PIMP0(sec,reg)           Price of import
PIMPR0(sec,rege,reg)     Price of import for country reg coming from contry rege
PY0(sec,reg)             Price of production
YD0(sec,reg)             Production
PDPF0(secff,reg)         Price of domestic production for fossil fuel
PDPP0(sec,reg)         Price of domestic production for petroleum product
FSEFA0(ag,pri,reg)       income coming from primary factor
FGRS0(tt,ag,reg)         Taxes
TAXECO20(reg)            Carbon tax
TRA0(sec,reg)            Transport
OTRA0(sec,reg)           Intermediaite minus transport
SAVE0(ag,reg)            saving
QUOTANET0(reg)           net supply of permits
EXPOP0(sec,reg)          Export
EXPO0(sec,reg)           Export
TSAVE0(reg)              rate of saving
PIO0(pro,sec,reg)        Price of intermediaite consumption
CO20(reg)                CO2 emission
PIBVA0(reg)              PIB en valeur
PIBVO0(reg)              PIB en volume
TXFGRS0_H(reg)           Rate of direct tax
IMPMTRA0(sectra,sec,regh,reg)  Transport linked to import
TXDUT1(sec,reg,rege)     Initial rate of duty
IMPOR0(sec,rege,reg)     Import including transport margin
DUT0B(sec,reg)           New Duties
STV0(sec,reg)            Stock variation
VATSTV0(sec,reg)         Tax on stock variation
IMPTRA0(sectra)          World transport

CHECKR(sec,reg)          Total resources
CHECKE(sec,reg)          Total uses
ERR(sec,reg)             diff between uses and resources
TIMPMTRA0(sec,regf)   variable

DUTI(reg)

;

FGRS0("DT","H",reg)=FGRS0_DT_H(reg);




* We transpose the matrix of margin of transport

IMPMTRA0("12",sec,reg,regh)= IMPTRAT120(reg,sec,regh);
IMPMTRA0("13",sec,reg,regh)= IMPTRAT130(reg,sec,regh);
IMPMTRA0("14",sec,reg,regh)= IMPTRAT140(reg,sec,regh);


* we correct data due to commercial trade error (check that ERR is small)



CHECKR(sec,reg) = sum(pro,IOV0(pro,sec,reg))+REMK0(sec,reg)+LAV0(sec,reg)+FF0(sec,reg)+
                  TAXFF0(sec,reg)+TAXKAV0(sec,reg)+TXIT0(sec,reg)+EXCISE0(sec,reg)+
                  VATGCV0(sec,reg)+ VATHCV0(sec,reg)+VATINV0(sec,reg)+SS0(sec,reg)+
                  DUT0(sec,reg)+EXPSUB0(sec,reg)+
                  sum(pro,VATIOV0(pro,sec,reg))+
                  sum(rege,sum(sectra,IMPMTRA0(sectra,sec,rege,reg)))+
                  sum(rege,IMPORHTRA0(sec,rege,reg));
CHECKE(sec,reg) =sum(pro,IOV0(sec,pro,reg))+EXPOP0(sec,reg)+GCV0(sec,reg)+HCV0(sec,reg)+INVPVP0(sec,reg);

display checke,checkr;

ERR(sec,reg)=CHECKR(sec,reg)-CHECKE(sec,reg);
display ERR;

LAV0(sec,reg)$(LAV0(sec,reg) ne 0)=LAV0(sec,reg)-ERR(sec,reg);
GCV0(sec,reg)$(LAV0(sec,reg) eq 0)=GCV0(sec,reg)+ERR(sec,reg);

CHECKR(sec,reg) = sum(pro,IOV0(pro,sec,reg))+REMK0(sec,reg)+LAV0(sec,reg)+FF0(sec,reg)+
                  TAXFF0(sec,reg)+TAXKAV0(sec,reg)+TXIT0(sec,reg)+EXCISE0(sec,reg)+
                  VATGCV0(sec,reg)+ VATHCV0(sec,reg)+VATINV0(sec,reg)+SS0(sec,reg)+
                  DUT0(sec,reg)+EXPSUB0(sec,reg)+
                  sum(pro,VATIOV0(pro,sec,reg))+
                  sum(rege,sum(sectra,IMPMTRA0(sectra,sec,rege,reg)))+
                  sum(rege,IMPORHTRA0(sec,rege,reg));
CHECKE(sec,reg) =sum(pro,IOV0(sec,pro,reg))+EXPOP0(sec,reg)+GCV0(sec,reg)+HCV0(sec,reg)+INVPVP0(sec,reg);
ERR(sec,reg)=CHECKR(sec,reg)-CHECKE(sec,reg);
display ERR;

* we correct refined petroleum consumption

$include Correction_raffinage.gms


* We substract to uses the imputed tax, uses are now free of taxes

GCV0(sec,reg)=GCV0(sec,reg)-VATGCV0(sec,reg);
HCV0(sec,reg)=HCV0(sec,reg)-VATHCV0(sec,reg)-EXCISE0(sec,reg);
INVPVP0(sec,reg)=INVPVP0(sec,reg)-VATINV0(sec,reg);

* We suppose that fix factor only exist in sector 01 02 03

REMK0(sec,reg)$(ord(sec) ge 4)=REMK0(sec,reg)+FF0(sec,reg);
FF0(sec,reg)$(ord(sec) ge 4)=0;


* definition des elasticites

SIGMA(sec,reg)=0.3;


SIGMAE("01",reg)=0.2;SIGMAE("02",reg)=0.1;SIGMAE("03",reg)=0.1;SIGMAE("04",reg)=0.1;
SIGMAE("05",reg)=0.1;SIGMAE("06",reg)=0.2;SIGMAE("07",reg)=0.2;SIGMAE("08",reg)=0.4;
SIGMAE("09",reg)=0.4;SIGMAE("10",reg)=0.4;SIGMAE("11",reg)=0.4;
SIGMAE("12",reg)=0.2;SIGMAE("13",reg)=0.2;SIGMAE("14",reg)=0.2;
SIGMAE("15",reg)=0.4;SIGMAE("16",reg)=0.4;SIGMAE("17",reg)=0.4;SIGMAE("18",reg)=0.4;

SIGMAEF("01",reg)=0.1;SIGMAEF("02",reg)=0.1;SIGMAEF("03",reg)=0.1;SIGMAEF("04",reg)=0.1;
*SIGMAEF("05",reg)=1.5;
SIGMAEF("05",reg)=0.9;
SIGMAEF("06",reg)=0.9;SIGMAEF("07",reg)=0.9;SIGMAEF("08",reg)=0.9;
SIGMAEF("09",reg)=0.3;SIGMAEF("10",reg)=0.9;SIGMAEF("11",reg)=0.9;
SIGMAEF("12",reg)=0.3;SIGMAEF("13",reg)=0.3;SIGMAEF("14",reg)=0.3;
SIGMAEF("15",reg)=0.9;SIGMAEF("16",reg)=0.9;SIGMAEF("17",reg)=0.9;SIGMAEF("18",reg)=0.9;

SIGMAM(sec,reg)=0.2;
SIGMAMM(sec,reg)=0.2;
SIGMATRA(sec,reg)=0.6;

SIGMAPF("01",reg)=0.4;SIGMAPF("02",reg)=0.2;SIGMAPF("03",reg)=0.2;
SIGMAPP("04",reg)=0.1;


SIGMAX("01",reg)=2;SIGMAX("02",reg)=10;SIGMAX("03",reg)=2;SIGMAX("04",reg)=3;
SIGMAX("05",reg)=0.5;SIGMAX("06",reg)=3;SIGMAX("07",reg)=3;SIGMAX("08",reg)=3;
SIGMAX("09",reg)=3;SIGMAX("10",reg)=3;SIGMAX("11",reg)=3;
SIGMAX("12",reg)=0.1;SIGMAX("13",reg)=0.1;SIGMAX("14",reg)=0.1;
SIGMAX("15",reg)=3;SIGMAX("16",reg)=3;SIGMAX("17",reg)=0.1;SIGMAX("18",reg)=0.05;

SIGMAI(sec,reg)=2;SIGMAI("02",reg)=10;
SIGMAI("05",reg)=0.3;SIGMAI("17",reg)=0.1;SIGMAI("18",reg)=0.05;


FRISCH(reg)=0.1;
ELASREV("01",reg)=0.5;ELASREV("02",reg)=0.7;ELASREV("03",reg)=0.7;ELASREV("04",reg)=0.7;
ELASREV("05",reg)=0.7;ELASREV("06",reg)=1;ELASREV("07",reg)=1;ELASREV("08",reg)=1;
ELASREV("09",reg)=1;ELASREV("10",reg)=1;ELASREV("11",reg)=1;ELASREV("12",reg)=1;
ELASREV("13",reg)=1;ELASREV("14",reg)=1;ELASREV("15",reg)=1;ELASREV("16",reg)=1;
ELASREV("17",reg)=1;ELASREV("18",reg)=1;
ELASREV("01",regpvd)=0.7;ELASREV("02",regpvd)=0.7;ELASREV("03",regpvd)=0.8;ELASREV("04",regpvd)=0.8;
ELASREV("05",regpvd)=1;
FRISCH(regpvd)=0.2;

ALPD(sec,reg)=0.5;
ALKAVA(sec,reg)=0.5;

TGK(sec,reg)=1;TGKA(sec,reg)=1;
TGE(sec,reg)=1;TGL(sec,reg)=1;TGM(sec,reg)=1;
GEPR(Sece,sec,reg)=1;GMPR(secm,sec,reg)=1;
TGENF(sec,reg)=1;
TGTRA(sec,reg)=1;TGOTRA(sec,reg)=1;

TAXECO20(reg)=0;
QUOTANET0(reg)=0;

GCVTOT0(reg)=sum(sec,GCV0(sec,reg));
NSUP0(reg)=sum(sec,LAV0(sec,reg));

PB0(sec,reg)=1.;
PF0(secff,reg)=1.;

W0(reg)=1.;
EX0(reg)=1.;
STV0(sec,reg)=0;
VATSTV0(sec,reg)=0;
TXTVAST(sec,reg)=0;

* New calibration imports



* We compute initial duty rate

TXDUT1(sec,rege,regmeu)= (DUT0(sec,regmeu)/sum(regf,IMPORHTRA0(sec,regf,regmeu)+sum(sectra,IMPMTRA0(sectra,sec,regf,regmeu))))
                         $(sum(regf,IMPORHTRA0(sec,regf,regmeu)+sum(sectra,IMPMTRA0(sectra,sec,regf,regmeu))) ne 0) ;
TXDUT1(sec,regeu,regeu2)=0;
TXDUT1(sec,regmeu,regeu)=(DUT0(sec,regeu)/sum(regf,IMPORHTRA0(sec,regf,regeu)+sum(sectra,IMPMTRA0(sectra,sec,regf,regeu))))
                         $(sum(regf,IMPORHTRA0(sec,regf,regeu)+sum(sectra,IMPMTRA0(sectra,sec,regf,regeu))) ne 0) ;


* We substract intra trade and integrate trade transport in import d'ont forget to modify duti

EXPOP0(sectra,reg)=EXPOP0(sectra,reg)-sum(sec,IMPMTRA0(sectra,sec,reg,reg));
IOV0(sectra,sec,reg)=IOV0(sectra,sec,reg)+IMPMTRA0(sectra,sec,reg,reg);

* penser a reequilibrer la SAM du ROW
VST0(sectra,reg)=VST0(sectra,reg)-sum(sec,IMPMTRA0(sectra,sec,reg,reg));


IMPMTRA0(sectra,sec,reg,reg)=0;
IMPO0(sec,reg)=sum(rege,IMPORHTRA0(sec,rege,reg)+sum(sectra,IMPMTRA0(sectra,sec,rege,reg)))
                -IMPORHTRA0(sec,reg,reg) ;
EXPOP0(sec,reg)=EXPOP0(sec,reg)-IMPORHTRA0(sec,reg,reg);
IMPORHTRA0(sec,reg,reg)=0;
IMPOR0(sec,rege,reg)=IMPORHTRA0(sec,rege,reg)+sum(sectra,IMPMTRA0(sectra,sec,rege,reg));
CIMPMTRA(sectra,sec,rege,reg)=(IMPMTRA0(sectra,sec,rege,reg)/IMPOR0(sec,rege,reg))$(IMPOR0(sec,rege,reg) ne 0);
TIMPMTRA0(sectra,regf)=(VST0(sectra,regf)/sum(pro,sum(regi,sum(regh,IMPMTRA0(sectra,pro,regi,regh)))))$(sum(pro,sum(regi,sum(regh,IMPMTRA0(sectra,pro,regi,regh)))) ne 0);
TIMPMTRA(sectra,sec,regf,rege,reg)=TIMPMTRA0(sectra,regf);

IMPTRA0(sectra)=sum(sec,sum(rege,sum(reg,CIMPMTRA(sectra,sec,rege,reg)*IMPOR0(sec,rege,reg))));


* We recompute Duties

DUT0B(sec,regmeu)=sum(rege,TXDUT1(sec,rege,regmeu)*IMPOR0(sec,rege,regmeu));
DUT0B(sec,regeu)=sum(rege,TXDUT1(sec,rege,regeu)*IMPOR0(sec,rege,regeu));
ERR(sec,reg)=DUT0(sec,reg)-DUT0B(sec,reg);

TXDUT(sec,rege,regmeu)= (DUT0B(sec,regmeu)/IMPO0(sec,regmeu))$(IMPO0(sec,regmeu) ne 0);
TXDUT(sec,regeu,regeu2)=0;
TXDUT(sec,regmeu,regeu)=(DUT0B(sec,regeu)/sum(regmeu2,IMPOR0(sec,regmeu2,regeu)))
                         $(sum(regmeu2,IMPOR0(sec,regmeu2,regeu)) ne 0);

* we balance SAM
TXIT0(sec,reg)=TXIT0(sec,reg)+DUT0(sec,reg)-DUT0B(sec,reg)+
               TAXFF0(sec,reg)+TAXKAV0(sec,reg)+EXPSUB0(sec,reg);
DUT0(sec,reg)=DUT0B(sec,reg);
TXTVAEX(sec,reg)= 0;


PEX0(sec,reg)=PB0(sec,reg)*(1+TXTVAEX(sec,reg));
PIMP0(sec,reg)=1;

PIMPR0(sec,rege,reg) = sum(sectra,CIMPMTRA(sectra,sec,rege,reg)*sum(regf,TIMPMTRA(sectra,sec,regf,rege,reg)
                      *PEX0(sectra,regf)*(EX0(regf)/EX0(reg))))+
                       (1-sum(sectra,CIMPMTRA(sectra,sec,rege,reg)))*PEX0(sec,rege)*(EX0(rege)/EX0(reg));

PIMP0(sec,reg)$(IMPO0(sec,reg) ne 0)=sum(rege,PIMPR0(sec,rege,reg)*(1+TXDUT(sec,rege,reg))*IMPOR0(sec,rege,reg))/IMPO0(sec,reg);




CIMP(sec,rege,reg)$(IMPO0(sec,reg) ne 0)=(IMPOR0(sec,rege,reg)*(PIMPR0(sec,rege,reg)*(1+TXDUT(sec,rege,reg)))
                                         **SIGMAI(sec,reg)/sum(regf,IMPOR0(sec,regf,reg)*
                                        (PIMPR0(sec,regf,reg)*(1+TXDUT(sec,regf,reg)))**SIGMAI(sec,reg)))
                                         $(sum(regf,IMPOR0(sec,regf,reg) ne 0));



APIMP(sec,reg)=1;

APIMP(sec,reg)$(IMPO0(sec,reg) ne 0)= PIMP0(sec,reg)*
                       ( (sum(regf,IMPOR0(sec,regf,reg)*(PIMPR0(sec,regf,reg)*(1+TXDUT(sec,regf,reg)))**SIGMAI(sec,reg)))
                      / ((PIMP0(sec,reg))*IMPO0(sec,reg)) )**(1/(1-SIGMAI(sec,reg)));





PTRA0(sec,reg)=(sum(Sectra,IOV0(Sectra,sec,reg)+VATIOV0(sectra,sec,reg))/
              sum(Sectra,IOV0(Sectra,sec,reg)))$(sum(Sectra,IOV0(Sectra,sec,reg)) ne 0)
              +1$(sum(Sectra,IOV0(Sectra,sec,reg)) eq 0);
POTRA0(sec,reg)=(sum(Secotra,IOV0(Secotra,sec,reg)+VATIOV0(secotra,sec,reg))/
              sum(Secotra,IOV0(Secotra,sec,reg)))$(sum(Secotra,IOV0(Secotra,sec,reg)) ne 0)+
              1$(sum(Secotra,IOV0(Secotra,sec,reg)) eq 0);
PM0(sec,reg)=(sum(Secm,IOV0(Secm,sec,reg)+VATIOV0(secm,sec,reg))/sum(Secm,IOV0(Secm,sec,reg)))$
                (sum(Secm,IOV0(Secm,sec,reg)) ne 0)+1$(sum(Secm,IOV0(Secm,sec,reg)) eq 0);
PE0(sec,reg)=1;
PE0(sec,reg)$(sum(Sece,IOV0(Sece,sec,reg)) ne 0)=(sum(Sece,IOV0(Sece,sec,reg)+VATIOV0(sece,sec,reg)))/
                                                 sum(Sece,IOV0(Sece,sec,reg));

PE0("04",reg)$(sum(Sece,IOV0(Sece,"04",reg)) ne 0)=(sum(Sece,IOV0(Sece,"04",reg)+VATIOV0(sece,"04",reg))-
                                                  IOV0("02","04",reg)-VATIOV0("02","04",reg))/
                                                  (sum(Sece,IOV0(Sece,"04",reg))-IOV0("02","04",reg));


PEF0(sec,reg)=1;
PEF0(sec,reg)$(sum(Secef,IOV0(Secef,sec,reg)) ne 0)=
             sum(Secef,IOV0(Secef,sec,reg)+VATIOV0(secef,sec,reg))/sum(Secef,IOV0(Secef,sec,reg));

PEF0("04",reg)$(sum(Secef,IOV0(Secef,"04",reg)) ne 0)=(sum(Secef,IOV0(Secef,"04",reg)+VATIOV0(secef,"04",reg))-
                      IOV0("02","04",reg)-VATIOV0("02","04",reg))/(sum(Secef,IOV0(Secef,"04",reg))-IOV0("02","04",reg));

MA0(sec,reg)=sum(Secm,IOV0(Secm,sec,reg));
EN0(sec,reg)=sum(Sece,IOV0(Sece,sec,reg));
EN0("04",reg)=sum(Sece,IOV0(Sece,"04",reg))-IOV0("02","04",reg);

ENF0(sec,reg)=sum(Secef,IOV0(Secef,sec,reg));
ENF0("04",reg)=sum(Secef,IOV0(Secef,"04",reg))-IOV0("02","04",reg);
TRA0(sec,reg)=sum(sectra,IOV0(sectra,sec,reg));
OTRA0(sec,reg)=sum(secotra,IOV0(secotra,sec,reg));

EXPO0(sec,reg)=EXPOP0(sec,reg);


TXW("SS",sec,reg)=(SS0(sec,reg)/(LAV0(sec,reg)*W0(reg)))$((LAV0(sec,reg)*W0(reg)) ne 0);


PL0(sec,reg)=W0(reg)*(1+sum(itw,TXW(itw,sec,reg)));

* calibration du module investment

* initialisation du taux de croissance de long terme de l'eco

$include TG.gms
*growth0(reg)=TGGROWTH(reg,"2")-1;
growth0(reg)=(TGGROWTH(reg,"1")-1+TGGROWTH(reg,"2")-1+TGGROWTH(reg,"3")-1
             +TGGROWTH(reg,"4")-1)/4;



* initialisation du taux de declassement
DECL(sec,reg)=0.045;

* calcul du capital total  !!!ATTENTION LE TAUX DE DECLASSEMENT DOIT ETRE IDENTIQUE PAR SECTEUR
KTOT0(reg)=sum(sec,INVPVP0(sec,reg))/(growth0(reg)+DECL("01",reg));
RLTL0(reg)=(sum(sec,REMK0(sec,reg))/KTOT0(reg))-DECL("01",reg);
INVV0(sec,reg)=(growth0(reg)+DECL("01",reg))/(RLTL0(reg)+DECL("01",reg))*REMK0(sec,reg);
KAV0(sec,reg)=INVV0(sec,reg)/(growth0(reg)+DECL("01",reg));
KAVC0(sec,reg)=KAV0(sec,reg);
KAVCM10(sec,reg)=KAVC0(sec,reg)/(1+growth0(reg));
PK0(sec,reg)=(REMK0(sec,reg)/KAV0(sec,reg))$(KAV0(sec,reg) ne 0)+1$(KAV0(sec,reg) eq 0);
ERRCK(sec,reg)=PK0(sec,reg)-RLTL0(reg)-DECL(sec,reg);
CK0(sec,reg)=RLTL0(reg)+DECL(sec,reg)+ERRCK(sec,reg);
XD0(sec,reg)=MA0(sec,reg)+EN0(sec,reg)+SS0(sec,reg)+LAV0(sec,reg)+REMK0(sec,reg);
*XD0(sec,reg)=MA0(sec,reg)+EN0(sec,reg)+LAV0(sec,reg)+KAV0(sec,reg);
XDPF0(secff,reg)=XD0(secff,reg)+FF0(secff,reg);
XDPP0(secrp,reg)=XD0(secrp,reg)+IOV0("02",secrp,reg);
XDM10(sec,reg)=XD0(sec,reg)/(1+growth0(reg));
XDM20(sec,reg)=XDM10(sec,reg)/(1+growth0(reg));
XDA0(sec,reg)=XDM10(sec,reg)*(XDM10(sec,reg)/XDM20(sec,reg))**2;
PD0(sec,reg)=((MA0(sec,reg)*PM0(sec,reg)+PE0(sec,reg)*EN0(sec,reg)+
              KAV0(sec,reg)*PK0(sec,reg)+LAV0(sec,reg)*PL0(sec,reg))/XD0(sec,reg))$(XD0(sec,reg) ne 0)
              +1$(XD0(sec,reg) eq 0);


PB0(sec,reg)=1.;
TINV(pro,sec,reg)=INVPVP0(pro,reg)/sum(sp,INVPVP0(sp,reg));
TXTVAIO(pro,sec,reg)$(IOV0(pro,sec,reg) ne 0)=VATIOV0(pro,sec,reg)/IOV0(pro,sec,reg);
TXTVAIN(pro,sec,reg)$(INVPVP0(pro,reg) ne 0) =VATINV0(pro,reg)/INVPVP0(pro,reg);
TXTVAST(sec,reg)$(STV0(sec,reg) ne 0) =VATSTV0(sec,reg)/STV0(sec,reg);
TXTVAGC(sec,reg)$(GCV0(sec,reg) ne 0) =VATGCV0(sec,reg)/GCV0(sec,reg);

PINV0(sec,reg)=sum(pro,(PB0(pro,reg)*TINV(pro,sec,reg)*(1+TXTVAIN(pro,sec,reg))));



PDR0(sec,reg)=PD0(sec,reg)/PINV0(sec,reg);
PDRM10(sec,reg)=PDR0(sec,reg);
PDRM20(sec,reg)=PDR0(sec,reg);
PDRA0(sec,reg)=PDR0(sec,reg)*(1+ALPD(sec,reg)*
                        ((PDR0(sec,reg)-PDRM10(sec,reg))/PDRM10(sec,reg))+
                        (1-ALPD(sec,reg))*((PDRM10(sec,reg)-PDRM20(sec,reg))/PDRM20(sec,reg)));



DL(sec,reg)$(XD0(sec,reg) ne 0)=
alpha1CES4(LAV0(sec,reg),KAV0(sec,reg),EN0(sec,reg),MA0(sec,reg),PL0(sec,reg),
PK0(sec,reg),PE0(sec,reg),PM0(sec,reg),sigma(sec,reg));
DK(sec,reg)$(XD0(sec,reg) ne 0)=
alpha2CES4(LAV0(sec,reg),KAV0(sec,reg),EN0(sec,reg),MA0(sec,reg),PL0(sec,reg),PK0(sec,reg),PE0(sec,reg),PM0(sec,reg),sigma(sec,reg));
DE(sec,reg)$(XD0(sec,reg) ne 0)=
alpha3CES4(LAV0(sec,reg),KAV0(sec,reg),EN0(sec,reg),MA0(sec,reg),PL0(sec,reg),PK0(sec,reg),PE0(sec,reg),PM0(sec,reg),sigma(sec,reg));
DM(sec,reg)$(XD0(sec,reg) ne 0)=
alpha4CES4(LAV0(sec,reg),KAV0(sec,reg),EN0(sec,reg),MA0(sec,reg),PL0(sec,reg),PK0(sec,reg),PE0(sec,reg),PM0(sec,reg),sigma(sec,reg));


APD(sec,reg)$(XD0(sec,reg) ne 0)=
LambdaCES4(LAV0(sec,reg),KAV0(sec,reg),EN0(sec,reg),MA0(sec,reg),PL0(sec,reg),PK0(sec,reg),PE0(sec,reg),
PM0(sec,reg),sigma(sec,reg),XD0(sec,reg),PD0(sec,reg));
KAVO0(sec,reg)=(1/TGKA(sec,reg))*XDA0(sec,reg)*APD(sec,reg)*DK(sec,reg)*
                                      (PDRA0(sec,reg)/((CK0(sec,reg)*APD(sec,reg))/(TGKA(sec,reg))))
                                      **SIGMA(sec,reg);
KAVA0(sec,reg)=((1-ALKAVA(sec,reg))*KAVO0(sec,reg)+ALKAVA(sec,reg)*((KAVC0(sec,reg)*KAVC0(sec,reg))/KAVCM10(sec,reg)))
                $(KAVCM10(sec,reg) ne 0);

ERRINVV(sec,reg)=INVV0(sec,reg)-(KAVA0(sec,reg)-(1-DECL(sec,reg))*KAVC0(sec,reg));
INVVM10(sec,reg)=KAVC0(sec,reg)-(1-DECL(sec,reg))*KAVCM10(sec,reg);

* Calibration de la consommation

TXEXCISE(sec,reg)$(HCV0(sec,reg) ne 0)=EXCISE0(sec,reg)/HCV0(sec,reg);
TXTVAHC(sec,reg)$(HCV0(sec,reg) ne 0)=VATHCV0(sec,reg)/(HCV0(sec,reg)+EXCISE0(sec,reg));
PHC0(sec,reg)=(PB0(sec,reg)+TXEXCISE(sec,reg))*(1+TXTVAHC(sec,reg));
HCDTOT0(reg)=sum(sec,HCV0(sec,reg)*PHC0(sec,reg));


BHCV(sec,reg)=(ELASREV(sec,reg)*HCV0(sec,reg)*PHC0(sec,reg))/HCDTOT0(reg);
BHCV("17",reg)=0;BHCV("17",reg)=1-sum(sec,BHCV(sec,reg));

CHCV(sec,reg)=HCV0(sec,reg)-(BHCV(sec,reg)*(HCDTOT0(reg)*(1-FRISCH(reg))))/PHC0(sec,reg);
CHCV0(sec,reg)=CHCV(sec,reg);


TGCV(sec,reg)=GCV0(sec,reg)/GCVTOT0(reg);



PGC0(sec,reg)=PB0(sec,reg)*(1+TXTVAGC(sec,reg));

PIO0(pro,sec,reg)=PB0(pro,reg)*(1+TXTVAIO(pro,sec,reg));




YD0(sec,reg)=sum(pro,IOV0(pro,sec,reg)+VATIOV0(pro,sec,reg))+LAV0(sec,reg)+SS0(sec,reg)
             +REMK0(sec,reg)+DUT0(sec,reg)+IMPO0(sec,reg)+TXIT0(sec,reg);
YD0(secff,reg)=sum(pro,IOV0(pro,secff,reg)+VATIOV0(pro,secff,reg))+LAV0(secff,reg)+SS0(secff,reg)
             +REMK0(secff,reg)+FF0(secff,reg)+DUT0(secff,reg)+IMPO0(secff,reg)+TXIT0(secff,reg);

PDPF0(secff,reg)=((XD0(secff,reg)*PD0(secff,reg)+FF0(secff,reg)*PF0(secff,reg))/XDPF0(secff,reg))
                 $(XDPF0(secff,reg) ne 0)+1$(XDPF0(secff,reg) eq 0);
PDPP0(sec,reg)=1;
PDPP0(secrp,reg)=(XD0(secrp,reg)*PD0(secrp,reg)+IOV0("02",secrp,reg)+VATIOV0("02",secrp,reg))/XDPP0(secrp,reg);

PY0(sec,reg)=((PIMP0(sec,reg)*IMPO0(sec,reg)+PD0(sec,reg)*XD0(sec,reg))/YD0(sec,reg))$(YD0(sec,reg) ne 0);
PY0(secff,reg)=(PIMP0(secff,reg)*IMPO0(secff,reg)+PDPF0(secff,reg)*XDPF0(secff,reg))/YD0(secff,reg);
PY0(Secrp,reg)=(PIMP0(Secrp,reg)*IMPO0(Secrp,reg)+PDPP0(Secrp,reg)*XDPP0(Secrp,reg))/YD0(Secrp,reg);



FSEFA0("H","L",reg)=sum(sec,LAV0(sec,reg))*W0(reg);
FSEFA0("H","K",reg)=sum(sec,KAV0(sec,reg)*PK0(sec,reg));
FSEFA0("H","F",reg)=sum(secff,FF0(secff,reg)*PF0(secff,reg));
FSEFA0("G","L",reg)=sum(sec,LAV0(sec,reg)*TXW("SS",sec,reg))*W0(reg);
FGRS0("DUT","G",reg)=sum(sec,sum(rege,IMPOR0(sec,rege,reg)*TXDUT(sec,rege,reg)*PIMPR0(sec,rege,reg)));
DUTI(reg)=sum(sec,DUT0(sec,reg));


*FGRS0("DUT","G",regmeu)=sum(sec,IMPO0(sec,regmeu)*TXDUT(sec,regmeu)*PIMP0(sec,regmeu)/(1+TXDUT(sec,regmeu)));
*FGRS0("DUT","G","FRA")=sum(sec,TXDUT(sec,"FRA")*sum(regmeu,PEX0(sec,regmeu)*EX0(regmeu)
*                                                    *IMPOR0(sec,regmeu,"FRA")));
*FGRS0("DUT","G","EUC")=sum(sec,TXDUT(sec,"EUC")*sum(regmeu,PEX0(sec,regmeu)*(EX0(regmeu)/EX0("EUC"))
*                                                    *IMPOR0(sec,regmeu,"EUC")));

* We recompute coal consumpion in sector 5

IOENER0("01","05",reg)$(ELENER(reg) ne 0) =ELENER(reg);


*
*
* ATTENTION FORMULE A REVOIR POUR VERSION 4 PAS FAIT
*
*
*
*
*
* We recompute petroleum consumption consumption in sector 14
*


IOENER0("04","17",reg)$(TENER("04",reg) ne 0)=TENER("04",reg)-IOENER0("04","07",reg)-(IOENER0("04","08",reg)-NOENER("04",reg))
                                             -IOENER0("04","09",reg)-IOENER0("04","10",reg);



DTXTVAHC(secef,reg)$(HCV0(secef,reg)+VATHCV0(secef,reg)+EXCISE0(secef,reg) ne 0)=
            (1000*CO2CONTENT(secef)*HCENER0(secef,reg))/(HCV0(secef,reg)+VATHCV0(secef,reg)+EXCISE0(secef,reg));

display DTXTVAHC;

DTXTVAIO(secef,sec,reg)$(IOV0(secef,sec,reg) ne 0)=(1000*CO2CONTENT(secef)*IOENER0(secef,sec,reg))/IOV0(secef,sec,reg);

* we substract non energy use in sector 9
DTXTVAIO("04","09",reg)$(IOV0("04","09",reg) ne 0)=(1000*CO2CONTENT("04")*(IOENER0("04","09",reg)-NOENER("04",reg)))/IOV0("04","09",reg);

TXIT(sec,reg)=(TXIT0(sec,reg)/YD0(sec,reg))$(YD0(sec,reg) ne 0);


CTEP(sece,sec,reg)$(IOV0(sece,sec,reg) ne 0)=IOENER0(sece,sec,reg)/IOV0(sece,sec,reg);
CTEP("04","09",reg)$(IOV0("04","09",reg) ne 0)=(IOENER0("04","09",reg)-NOENER("04",reg))/IOV0("04","09",reg);
CTEPH(sece,reg)$(HCV0(sece,reg) ne 0)=HCENER0(sece,reg)/HCV0(sece,reg);
CTEP("02","04",reg)=0;


FGRS0("VATHC","G",reg)=sum(sec,VATHCV0(sec,reg))+sum(sec,EXCISE0(sec,reg));
FGRS0("VATIO","G",reg)=sum(sec,sum(pro,VATIOV0(pro,sec,reg)));
FGRS0("VATST","G",reg)=sum(sec,VATSTV0(sec,reg));
FGRS0("VATGC","G",reg)=sum(sec,VATGCV0(sec,reg));
FGRS0("VATINV","G",reg)=sum(sec,VATINV0(sec,reg));
FGRS0("IT","G",reg)=sum(sec,YD0(sec,reg)*PY0(sec,reg)*TXIT(sec,reg)/(1-TXIT(sec,reg)));


TXFGRS0_H(reg)=FGRS0("DT","H",reg)/(sum(pri,FSEFA0("H",pri,reg))+FSESE0_H_G(reg));
TXFSESE_H_G(reg)=FSESE0_H_G(reg)/sum(pri,FSEFA0("H",pri,reg));


SAVE0("G",reg)=FGRS0("IT","G",reg)+FGRS0("VATIO","G",reg)+FGRS0("VATHC","G",reg)
                                          +FGRS0("VATST","G",reg)+FGRS0("VATINV","G",reg)
                                          +FGRS0("DT","H",reg)+FGRS0("DUT","G",reg)+FSEFA0("G","L",reg)
                                          -sum(sec,GCV0(sec,reg)*PGC0(sec,reg))-FSESE0_H_G(reg)
                                          +QUOTANET0(reg)*TAXECO20(reg)*1000;

SAVE0("H",reg)=FSEFA0("H","K",reg)+FSEFA0("H","L",reg)+FSEFA0("H","F",reg)+FSESE0_H_G(reg)
               -sum(sec,PHC0(sec,reg)*HCV0(sec,reg))-FGRS0("DT","H",reg);

SAVE0("W",reg)=sum(sec,IMPO0(sec,reg)*PIMP0(sec,reg)-EXPO0(sec,reg)*PEX0(sec,reg))
                                         -FGRS0("DUT","G",reg)-QUOTANET0(reg)*TAXECO20(reg)*1000;
TSAVE0(reg)=1-(HCDTOT0(reg)/(HCDTOT0(reg)+SAVE0("H",reg)));


APY(sec,reg)=lambdaCES2(XD0(sec,reg),IMPO0(sec,reg),PD0(sec,reg),PIMP0(sec,reg),sigmax(sec,reg),
               YD0(sec,reg),PY0(sec,reg));
CX(sec,reg)$(YD0(sec,reg) ne 0)=alpha1CES2(XD0(sec,reg),IMPO0(sec,reg),PD0(sec,reg),PIMP0(sec,reg),sigmax(sec,reg));

APY(secff,reg)=lambdaCES2(XDPF0(secff,reg),IMPO0(secff,reg),PDPF0(secff,reg),PIMP0(secff,reg),sigmax(secff,reg),
               YD0(secff,reg),PY0(secff,reg));
CX(secff,reg)=alpha1CES2(XDPF0(secff,reg),IMPO0(secff,reg),PDPF0(secff,reg),PIMP0(secff,reg),sigmax(secff,reg));
APY(Secrp,reg)=lambdaCES2(XDPP0(Secrp,reg),IMPO0(Secrp,reg),PDPP0(Secrp,reg),PIMP0(Secrp,reg),sigmax(Secrp,reg),
               YD0(Secrp,reg),PY0(Secrp,reg));
CX(Secrp,reg)=alpha1CES2(XDPP0(Secrp,reg),IMPO0(Secrp,reg),PDPP0(Secrp,reg),PIMP0(Secrp,reg),sigmax(Secrp,reg));


APE(sec,reg)=1;DEPR(sece,sec,reg)=0;APEF(sec,reg)=1;
APE(sec,reg)$(EN0(sec,reg) ne 0)=lambdaCES2(IOV0("05",sec,reg),ENF0(sec,reg),PIO0("05",sec,reg),PEF0(sec,reg),
                                 sigmaE(sec,reg),EN0(sec,reg),PE0(sec,reg));
DEPR("05",sec,reg)$(EN0(sec,reg) ne 0)=alpha1CES2(IOV0("05",sec,reg),ENF0(sec,reg),PIO0("05",sec,reg),
                                 PEF0(sec,reg),sigmaE(sec,reg));


APEF(sec,reg)$(ENF0(sec,reg) ne 0)=lambdaCES4(IOV0("01",sec,reg),IOV0("02",sec,reg),IOV0("03",sec,reg),IOV0("04",sec,reg),
              PIO0("01",sec,reg),PIO0("02",sec,reg),PIO0("03",sec,reg),PIO0("04",sec,reg),Sigmaef(sec,reg),ENF0(sec,reg),PEF0(sec,reg));


DEPR("01",sec,reg)$(ENF0(sec,reg) ne 0)=alpha1CES4(IOV0("01",sec,reg),IOV0("02",sec,reg),IOV0("03",sec,reg),IOV0("04",sec,reg),
                         PIO0("01",sec,reg),PIO0("02",sec,reg),PIO0("03",sec,reg),PIO0("04",sec,reg),Sigmaef(sec,reg));
DEPR("02",sec,reg)$(ENF0(sec,reg) ne 0)=alpha2CES4(IOV0("01",sec,reg),IOV0("02",sec,reg),IOV0("03",sec,reg),IOV0("04",sec,reg),
                         PIO0("01",sec,reg),PIO0("02",sec,reg),PIO0("03",sec,reg),PIO0("04",sec,reg),Sigmaef(sec,reg));
DEPR("03",sec,reg)$(ENF0(sec,reg) ne 0)=alpha3CES4(IOV0("01",sec,reg),IOV0("02",sec,reg),IOV0("03",sec,reg),IOV0("04",sec,reg),
                         PIO0("01",sec,reg),PIO0("02",sec,reg),PIO0("03",sec,reg),PIO0("04",sec,reg),Sigmaef(sec,reg));
DEPR("04",sec,reg)$(ENF0(sec,reg) ne 0)=alpha4CES4(IOV0("01",sec,reg),IOV0("02",sec,reg),IOV0("03",sec,reg),IOV0("04",sec,reg),
                         PIO0("01",sec,reg),PIO0("02",sec,reg),PIO0("03",sec,reg),PIO0("04",sec,reg),Sigmaef(sec,reg));



APEF("04",reg)$(ENF0("04",reg) ne 0)=lambdaCES3(IOV0("01","04",reg),IOV0("03","04",reg),IOV0("04","04",reg),
              PIO0("01","04",reg),PIO0("03","04",reg),PIO0("04","04",reg),Sigmaef("04",reg),ENF0("04",reg),PEF0("04",reg));

DEPR("01","04",reg)$(ENF0("04",reg) ne 0)=alpha1CES3(IOV0("01","04",reg),IOV0("03","04",reg),IOV0("04","04",reg),
                         PIO0("01","04",reg),PIO0("03","04",reg),PIO0("04","04",reg),Sigmaef("04",reg));
DEPR("02","04",reg)=0;

DEPR("03","04",reg)$(ENF0("04",reg) ne 0)=alpha2CES3(IOV0("01","04",reg),IOV0("03","04",reg),IOV0("04","04",reg),
                         PIO0("01","04",reg),PIO0("03","04",reg),PIO0("04","04",reg),Sigmaef("04",reg));
DEPR("04","04",reg)$(ENF0("04",reg) ne 0)=alpha3CES3(IOV0("01","04",reg),IOV0("03","04",reg),IOV0("04","04",reg),
                         PIO0("01","04",reg),PIO0("03","04",reg),PIO0("04","04",reg),Sigmaef("04",reg));


APMM(sec,reg)$(MA0(sec,reg) ne 0) = lambdaCES2(TRA0(sec,reg),OTRA0(sec,reg),PTRA0(sec,reg),POTRA0(sec,reg),
                                 sigmamm(sec,reg),MA0(sec,reg),PM0(sec,reg));
CMM(sec,reg)$(MA0(sec,reg) ne 0)  = alpha1CES2(TRA0(sec,reg),OTRA0(sec,reg),PTRA0(sec,reg),POTRA0(sec,reg),
                                 sigmamm(sec,reg));

APTRA(sec,reg)$(TRA0(sec,reg) ne 0)=lambdaCES3(IOV0("12",sec,reg),IOV0("13",sec,reg),IOV0("14",sec,reg),
                          PIO0("12",sec,reg),PIO0("13",sec,reg),PIO0("14",sec,reg),
                          Sigmatra(sec,reg),TRA0(sec,reg),PTRA0(sec,reg));
DMPR("12",sec,reg)$(TRA0(sec,reg) ne 0)=alpha1CES3(IOV0("12",sec,reg),IOV0("13",sec,reg),IOV0("14",sec,reg),
                          PIO0("12",sec,reg),PIO0("13",sec,reg),PIO0("14",sec,reg),
                          Sigmatra(sec,reg));
DMPR("13",sec,reg)$(TRA0(sec,reg) ne 0)=alpha2CES3(IOV0("12",sec,reg),IOV0("13",sec,reg),IOV0("14",sec,reg),
                          PIO0("12",sec,reg),PIO0("13",sec,reg),PIO0("14",sec,reg),
                          Sigmatra(sec,reg));
DMPR("14",sec,reg)$(TRA0(sec,reg) ne 0)=alpha3CES3(IOV0("12",sec,reg),IOV0("13",sec,reg),IOV0("14",sec,reg),
                          PIO0("12",sec,reg),PIO0("13",sec,reg),PIO0("14",sec,reg),
                          Sigmatra(sec,reg));


DMPR("06",sec,reg)$(OTRA0(sec,reg) ne 0)=al1CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));

DMPR("07",sec,reg)$(OTRA0(sec,reg) ne 0)=al2CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("08",sec,reg)$(OTRA0(sec,reg) ne 0)=al3CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("09",sec,reg)$(OTRA0(sec,reg) ne 0)=al4CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("10",sec,reg)$(OTRA0(sec,reg) ne 0)=al5CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("11",sec,reg)$(OTRA0(sec,reg) ne 0)=al6CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("15",sec,reg)$(OTRA0(sec,reg) ne 0)=al7CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("16",sec,reg)$(OTRA0(sec,reg) ne 0)=al8CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("17",sec,reg)$(OTRA0(sec,reg) ne 0)=al9CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
DMPR("18",sec,reg)$(OTRA0(sec,reg) ne 0)=al10CES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),PIO0("11",sec,reg),
                        PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg));
APM(sec,reg)$(OTRA0(sec,reg) ne 0)=lambCES10(IOV0("06",sec,reg),IOV0("07",sec,reg),IOV0("08",sec,reg),IOV0("09",sec,reg),
                        IOV0("10",sec,reg),IOV0("11",sec,reg),IOV0("15",sec,reg),IOV0("16",sec,reg),IOV0("17",sec,reg),IOV0("18",sec,reg),
                        PIO0("06",sec,reg),PIO0("07",sec,reg),PIO0("08",sec,reg),PIO0("09",sec,reg),PIO0("10",sec,reg),
                        PIO0("11",sec,reg),PIO0("15",sec,reg),PIO0("16",sec,reg),PIO0("17",sec,reg),PIO0("18",sec,reg),
                        Sigmam(sec,reg),OTRA0(sec,reg),POTRA0(sec,reg));


APPF(secff,reg)$(XDPF0(secff,reg) ne 0)=lambdaCES2(XD0(secff,reg),FF0(secff,reg),PD0(secff,reg),PF0(secff,reg),sigmapf(secff,reg),XDPF0(secff,reg),PDPF0(secff,reg));
CPF(secff,reg)$(XDPF0(secff,reg) ne 0)=alpha1CES2(XD0(secff,reg),FF0(secff,reg),PD0(secff,reg),PF0(secff,reg),sigmapf(secff,reg));

APPP("04",reg)=lambdaCES2(XD0("04",reg),IOV0("02","04",reg),PD0("04",reg),PIO0("02","04",reg),sigmapp("04",reg),XDPP0("04",reg),PDPP0("04",reg));
CPP("04",reg)=alpha1CES2(XD0("04",reg),IOV0("02","04",reg),PD0("04",reg),PIO0("02","04",reg),sigmapp("04",reg));

CO20(reg)=sum(sec,sum(secef,CTEP(secef,sec,reg)*IOV0(secef,sec,reg)*CO2CONTENT(secef)))+
                                         sum(secef,CTEPH(secef,reg)*HCV0(secef,reg)*CO2CONTENT(secef));


PEXB(sec,reg)= PEX0(sec,reg);
PHCB(sec,reg)=PHC0(sec,reg);
PINVB(sec,reg)=PINV0(sec,reg);
PBB(sec,reg)=PB0(sec,reg);
PIMPB(sec,reg)=PIMP0(sec,reg);
PGCB(sec,reg)=PGC0(sec,reg);


PIBVA0(reg)=(sum(sec,EXPOP0(sec,reg)*PEX0(sec,reg)+PHC0(sec,reg)*HCV0(sec,reg)+PGC0(sec,reg)*GCV0(sec,reg)
                                     +INVV0(sec,reg)*PINV0(sec,reg)+STV0(sec,reg)*PB0(sec,reg)*(1+TXTVAST(sec,reg))-
                                     IMPO0(sec,reg)*PIMP0(sec,reg)));



PIBVO0(reg)=(sum(sec,EXPOP0(sec,reg)*PEXB(sec,reg)+PHCB(sec,reg)*HCV0(sec,reg)+PGCB(sec,reg)*GCV0(sec,reg)
                                     +INVV0(sec,reg)*PINVB(sec,reg)+STV0(sec,reg)*PBB(sec,reg)*(1+TXTVAST(sec,reg))-
                                     IMPO0(sec,reg)*PIMPB(sec,reg)));

EC(reg) =sum(sec,IMPO0(sec,reg)*PIMP0(sec,reg))-FGRS0("DUT","G",reg)-
           sum(sec,EXPO0(sec,reg)*PEX0(sec,reg));

