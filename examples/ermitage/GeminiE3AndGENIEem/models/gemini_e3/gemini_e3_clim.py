import os
class gemini_e3_clim:

    def __init__(self,base_year,num_years):
        self.base_year=base_year # 2001
        self.num_years=num_years # 50

    def run(self,CO2,N2O,CH4):
        c1 = open("CO2sum_data.gms", "wb")
        c2 = open("N2Osum_data.gms", "wb")
        c3 = open("CH4sum_data.gms", "wb")
        c1.write("parameter CO2sum(T)/\n")
        c2.write("parameter N20sum(T)/\n")
        c3.write("parameter CH4sum(T)/\n")
        for i in xrange(self.num_years):
            c1.write(str(self.base_year+i) + "    " + str(CO2[i]) + "\n")
            c2.write(str(self.base_year+i) + "    " + str(N2O[i]) + "\n")
            c3.write(str(self.base_year+i) + "    " + str(CH4[i]) + "\n")
        c1.write("/;")
        c2.write("/;")
        c3.write("/;")
        c1.close()
        c2.close()
        c3.close()
    
        try:
            print "[python] gemini_e3_clim:run calling gams code"
            os.system("gams ./Variante.gms")
            print "[python] gemini_e3_clim:run gams code complete"
            #subprocess.call('gams ./Variante.gms')
        except OSError, e:
            print "Error calling GEMINI_E3_CLIM: "+str(e)
            print "Aborting"
            exit(1)
