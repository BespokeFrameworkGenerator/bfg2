import csv
import os
import subprocess
import sys

from numpy import zeros


class genie_em_model:

    def __init__(self):
        print "genie_em_model:init"

    def run(self,time,concent_co2):

        print "entered genie_em_run"

        #print "type of time is ",type(time)
        #print "size of time is ",len(time)
        #print "type of concent_co2 is ",type(concent_co2)
        #print "size of concent_co2 is ",len(concent_co2)
        #exit(0)

        n=len(time)
        assert n==len(concent_co2)

        #print "concent_co2 "+str(concent_co2)
        #print "Time "+str(time)
        #exit(1)
        # *** RF *** same as original

        poly1 = zeros(n)
        poly2 = zeros(n)
        poly3 = zeros(n)
        a11 = 0
        a22 = 0
        a33 = 0
        a12 = 0
        a13 = 0
        a23 = 0
        b = zeros(3)
        for i in xrange(n):
            poly1[i] = time[i] + 1
            poly2[i] = 2 * time[i]**2 - 2
            poly3[i] = 4 * time[i]**3 - 4 * time[i]
            a11 = a11 + poly1[i]**2
            a22 = a22 + poly2[i]**2
            a33 = a33 + poly3[i]**2
            a12 = a12 + poly1[i]*poly2[i]
            a13 = a13 + poly1[i]*poly3[i]
            a23 = a23 + poly2[i]*poly3[i]
            b[0] = b[0] + poly1[i] * (concent_co2[i] - concent_co2[0])
            b[1] = b[1] + poly2[i] * (concent_co2[i] - concent_co2[0])
            b[2] = b[2] + poly3[i] * (concent_co2[i] - concent_co2[0])

        e11 = (poly1[n-1]**2)/2
        e21 = (poly2[n-1]*poly1[n-1])/2
        e31 = (poly3[n-1]*poly1[n-1])/2

        if e21 == 0:
            e21 = 1e-6
        if e31 == 0:
            e31 = 1e-6

        e12 = poly1[n-1]*a12 - poly2[n-1]*a11
        e13 = poly1[n-1]*a13 - poly3[n-1]*a11
        f1 = poly1[n-1]*b[0] - (concent_co2[n-1] - concent_co2[0]) * a11

        e22 = poly1[n-1]*a22 - poly2[n-1]*a12
        e23 = poly1[n-1]*a23- poly3[n-1]*a12
        f2 = poly1[n-1]*b[1] - (concent_co2[n-1] - concent_co2[0]) * a12

        e32 = poly1[n-1]*a23 - poly2[n-1]*a13
        e33 = poly1[n-1]*a33- poly3[n-1]*a13
        f3 =  poly1[n-1]*b[2] - (concent_co2[n-1] - concent_co2[0]) * a13

        c11 = e12*e21 - e11*e22
        c12 = e13*e21 - e11*e23
        c21 = e12*e31 - e11*e32
        c22 = e13*e31 - e33*e11
        d1 = f1*e21 - f2*e11
        d2 = f1*e31 - f3*e11

        tcheb = zeros(3)
        tcheb[2] = (d1*c21 - d2*c11)/(c12*c21 - c11*c22)
        tcheb[1] = (d2 - c22 * tcheb[2]) / c21 
        delta = (f1 - e12 * tcheb[1] - e13 * tcheb[2]) / e11  
        tcheb[0] = ((concent_co2[n-1] - concent_co2[0]) - tcheb[1] * poly2[n-1] - tcheb[2] * poly3[n-1]) / poly1[n-1]

        tcheb[0] = 2*tcheb[0]
        tcheb[1] = 2*tcheb[1]
        tcheb[2] = 2*tcheb[2]

        # Ecriture des concentrations approximees dans le fichier ".\concent_approx.txt"
        c = open("concent_approx.txt", "wb")
        for i in xrange(n):
            c.write(str(0.5* (poly1[i]* tcheb[0] + poly2[i] * tcheb[1] + poly3[i] * tcheb[2]) + concent_co2[0]) + "\n")
        c.close()

        # Writing chebyshev coefficients into the file ".\coeffs.txt"
        c = open( "coeffs.txt", "wb")
        c.write("TC1,TC2,TC3 \n")
        c.write(str(tcheb[0]) + "," + str(tcheb[1]) + "," + str(tcheb[2])+ "\n\n")
        c.close()


        #print "Look in coeffs.txt and concent_approx.txt"
        #exit(1)
        # *** RF *** OK up to here

        #################################################
        #
        # 3. Running GENIE Emulator
        #
        # Used files: coeffs.txt
        #
        # Created files: outfile_genie2.txt          
        #
        ##################################################
        
        infile = open("emul.R", 'r')
        outfile = open("out.txt", 'w')

        args = ["R", "--no-restore", "--no-save"]

        proc = subprocess.Popen(args, stdin=infile, stdout=outfile, env=os.environ, close_fds=True)
        proc.wait()

        if proc.returncode:
            print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
            sys.exit(proc.returncode)


        # Quantile target output
        reader = csv.reader(open("SAT_quant.dat","rb"))
        list_reader = []
        for row in reader:
            list_reader.append(row[0])
        line = list_reader[1].split(' ')
        Quantile_temperature_increase  = float(line[0])

        #print "CHECK outfile_genie2.txt"
        #exit(1)

        # Average temperature output
        reader = csv.reader(open("outfile_genie2.txt","rb"))
        list_reader = []
        for row in reader:
            list_reader.append(row[0])
        line = list_reader[0].split(' ')
        Average_temperature_increase  = float(line[1])
        #print "GENIE_em: Average Temperature increase is "+str(Average_temperature_increase)

        #exit(1)
        os.system("rm coeffs.txt")
        os.system("rm SAT_quant.dat")
        os.system("rm outfile_genie2.txt")

        print "genie_em_run complete"

#        print("genie_em_run: quantile {0}\t\taverage {1}".format( Quantile_temperature_increase,Average_temperature_increase ))
        return Quantile_temperature_increase,Average_temperature_increase
