class convergence_test:

    def __init__(self,target,precision):
        print "convergence test transformation:init"
        self.target=target
        self.precision=precision

    def converged(self,value):
        if abs(value - self.target) > self.precision : return False
        else : return True
