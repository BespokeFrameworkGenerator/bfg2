import csv
import os
import sys
import subprocess

from numpy import zeros


class em2conc(object):

    def __init__(self,nb_fixed_year,base_year):

        #print type(nb_fixed_year)
        #print type(base_year)
        #exit(0)
        print "gemini_e3_model:__init__"

        self.nb_fixed_year=nb_fixed_year
        self.base_year=base_year

    def run(self,CO2,N2O,CH4):

        print "Entered gemini_e3_run"
        print "CO2",CO2
        print "N2O",N2O
        print "CH4",CH4

        #################################################################
        # Preparing input files for the climate module of GEMINI-E3 
        c1 = open("CO2sum_data.gms", "wb")
        c2 = open("N2Osum_data.gms", "wb")
        c3 = open("CH4sum_data.gms", "wb")
        c1.write("parameter CO2sum(T)/\n")
        c2.write("parameter N20sum(T)/\n")
        c3.write("parameter CH4sum(T)/\n")

        for i in xrange(50):
            c1.write(str(self.base_year+i) + "    " + str(CO2[i]) + "\n")
            c2.write(str(self.base_year+i) + "    " + str(N2O[i]) + "\n")
            c3.write(str(self.base_year+i) + "    " + str(CH4[i]) + "\n")

        c1.write("/;")
        c2.write("/;")
        c3.write("/;")
        c1.close()
        c2.close()
        c3.close()

        #################################################
        #
        # 2. Running Climate module of Gemini-E3 
        #
        # Used files: CO2sum_data.gms, N2Osum_data.gms and CH4sum_data.gms
        #
        # Created files: concent-gemini.txt          
        #
        ################################################## 
        print "HERE IS WHERE I EXEC GAMS"
#        os.system("gams Climate_module2.1.gms")
        args = ["gams", "Climate_module2.1.gms"]

        proc = subprocess.Popen(args, env=os.environ, close_fds=True)
        proc.wait()

        if proc.returncode:
            print("[BFG] Subprocess \"{0}\" in module {1} returned error {2}, aborting.".format(" ".join(args), __name__, proc.returncode))
            sys.exit(proc.returncode)

        print "GAMS EXEC COMPLETE"

        #################################################################
        # Preparing input files for GENIE
        file = "concent-gemini.txt"
        reader = csv.reader(open(file,"rb"))  
        list_reader = []
        for row in reader:
            list_reader.append(row[0])

        # +1 in latest version
        n = len(list_reader)+1
        concent_co2 = zeros(n)
        time = zeros(n)
        for i in xrange(n-1):
            line = list_reader[i].split(';')
            time[i] = line[0]
            concent_co2[i] = line[1]

# Some new hack introduced in latest version. No idea why
        time[n-1] = 2100
        concent_co2[n-1] = concent_co2[n-2]


# RF : HOW DO WE REMOVE THE HARDCODED 2005 and 50?
# RF : IS IT (FIXED_YEAR-(BASE_YEAR+1))/2 and MAX_YEAR-MIN_YEAR+1?
        for i in xrange(n):
            time[i] = -1 + (time[i] - 2005)/50


        os.system("rm CO2sum_data.gms")
        os.system("rm CH4sum_data.gms")
        os.system("rm N2Osum_data.gms")
        os.system("rm concent-gemini.txt")
        os.system("rm Climate_module2.1.lst")

        print "Completed gemini_e3_run"

        print "type(CO2) is ",type(CO2)
        print "size(CO2) is ",CO2.size
        print "type(time) is ",type(time)
        print "size(time) is ",time.size
        print "type(concent_co2) is ",type(concent_co2)
        print "size(concent_co2) is ",concent_co2.size
        print "time",time
        print "concent_co2",concent_co2
        return time,concent_co2
