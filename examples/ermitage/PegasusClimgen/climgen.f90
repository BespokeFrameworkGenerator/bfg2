module climgen
implicit none
private
public ts
contains
  subroutine ts(temp,precip,rad)
    real,intent(out) :: temp(4),precip(4),rad(4)
    temp=1.0
    precip=2.0
    rad=3.0
  end subroutine
end module climgen
