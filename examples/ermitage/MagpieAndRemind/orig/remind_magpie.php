<?php

// REMIND - MAgPIE soft coupling 2.14

//#################LAST CHANGES################################
//2.14: added R script magpie_plot.R
//2.13: R scripts create now separate log-files (jpd)
//2.12: iteration is now stopped instantaneously if REMIND or MAgPIE becomes infeasible (jpd)
//2.11: included SSPplot
//2.10: new recombine functions for both directions (rem2mag and mag2rem) now using transformation matrix for region matching
//2.09: added transmissions of regional bioenergy costs and emissions
//      region matching for emissions and costs has to be changed (currently they are treated like prices)
//2.08: added transmission rules for emission prices (ch4, co2, n2o)
//      script can now handle global data in addition to regional data (jpd)
//2.07: added model running timer, exports calculation times to file
//      added possibility to stop script externally after current iteration
//2.06: increased maximal allowed bioenergy prices
//2.05: bugfixed division by zero error in calculate_stage()
//2.04: added backups of all files write2csv needs no path
// 		relative to main folder
//2.03: removed file copy commands
//2.02: info.txt contained in magpie/input/cellular is now 
//		also copied to the results folder (needed for plot.php)
//2.01: MAgPIE is now started over magpie_stat.php instead
//		of directly executing magpie.gms
//2.00: File transfer is now managed object-based
//1.7: remind_modelstat=7 is now also handled as modelstat<3
//1.6: made path-declaration compatible to cluster
//1.5: magpie_modelstat=7 is now handled as modelstat<3
//     (results are used without correction)
//1.4: added information wheter lowpass filter was applied 
//     or not
//1.3: echo output corrected. bioenergy_demand.csv corrected
//     for remind modelstat>2
//1.2: fixed error that headers of bioenergy_demand.put and
//     bioenergy_prices.put get lost
//1.1: some bugfixes concerning echo output in remind_to_magpie
//     added "unset" command after foreach-loops
//1.0: defined $output_folder additionally to $transfer_folder
//     results are now plotted directly after calculation
//0.9: moved folder manipulations behind the loop,
//     saving additional important data at the end
//0.8: added folder manipulations before loop starts
//0.7: added remind_modelstat evaluation
//0.6: included low-pass filters for REMIND_to_MAgPIE and
//     MAgPIE_to_REMIND
//0.5: added base bioenergy supply which isnt send to MAgPIE
//     demand in case actual demand = base: 0.01 to prevent
//     division by zero in MAgPIE
//0.4: bug fixed with GAMS-Path: replaced OS_TYPE with PHP_OS
//0.3: absolute GAMS-Path for cluster inserted (dk 16022009)
//0.2: -put help functions in a separate file php/functions.php
//     -introduced max and min values for shadow prices
//     -using magpie modelstatus for data correction
//     -added logoption-parameter
//
//0.1: First version
###############################################################

//#############################################################
//#####################INCLUDE CLASSES#########################
//#############################################################

include ("php/container.php");		//Container class
include ("php/transformer.php");	//Transformer class
include ("php/rem2mag.php");		//rem2mag class
include ("php/mag2rem.php");		//mag2rem class
include ("php/metainfo.php");		//metainfo class


//#############################################################
//#################GENERAL CONFIGURATION#######################
//#############################################################

//Defining a maximum number of iteration
//no boundary condition -> $max_iterations = -1
$max_iterations = 1;

//Defining tolerance for the comparison of old and new output
//for each solving stage
//not using this condition -> $tolerance[$i] = -1
$tolerance = array(0.05,0.01);

//The stage defines the actual solving state, a high stage means that 
//the solver is far away from equilibrium whereas stage = 0 means that
//the iteration is finished because the variation between iterations
//is smaller than the smallest tolerance defined in $tolerance
$stage = count($tolerance);

//Should REMIND output be shown in console? (0:no, 3:yes)
$logoption = 0;

//Should r be executed and produce nice pictures in each loop? (0:no, 1:yes)
$run_r = 1;

//Dummy run? (that means that remind_magpie.php runs without magpie and remind
// This is useful for debugging)
$dummy = FALSE;

//Setting the GAMS PATH
if(PHP_OS=="Linux") {
  if(is_dir('/iplex/01/landuse')) {  //Iplex directory is reachable
    $gamspath = '/iplex/01/sys/applications/gams/'; 
	$phppath = '/iplex/01/landuse/bin/php/bin/';
    $rpath = '/iplex/01/landuse/bin/r/bin/';
  } else {
    $gamspath = '/usr/local/gams/';
	$phppath = '/scratch/02/dietrich/php/bin/';
    $rpath = '/scratch/02/dietrich/r/bin/';  
  }
} else {
  $gamspath = '';
  $phppath = '';
  $rpath = '';
}

//Defining basic folder paths
if($dummy) {
	$dummy_folder = 'data/dummy/';
    transformer::$PUT_FOLDER 		= $dummy_folder;
	transformer::$TRANSFER_FOLDER 	= $dummy_folder;
	rem2mag::$OUTPUT_FOLDER 		= $dummy_folder;
	mag2rem::$OUTPUT_FOLDER 		= $dummy_folder;
	metainfo::$PUT_FOLDER 		    = $dummy_folder;
	$shiftfolder                    = $dummy_folder;
}else{
    transformer::$PUT_FOLDER 		= 'data/results/';
	transformer::$TRANSFER_FOLDER 	= 'data/results/output/';
	rem2mag::$OUTPUT_FOLDER 		= 'magpie/input/regional/scenarios/';
	mag2rem::$OUTPUT_FOLDER 		= 'remind/types/recipe/in/';
	metainfo::$PUT_FOLDER    		= 'data/results/';
	$shiftfolder                    = 'shift/';
}


// defining which files should be saved (additional to results)
//                         FOLDER                  FILE 
$save_files = array(array("remind/types/recipe/in/" => "exp.inc"),
                    array("magpie/"            		=> "magpie.gms"),
					array("magpie/input/cellular/" 	=> "info.txt"));



//#############################################################
//####################SOME USEFUL FUNCTIONS####################
//#############################################################

//#############################################################
//function calculate_stage
//calculates the actual solution stage under use of a
//tolerance array
//A lower stage means less variation between iterations,
//the calculation is finished when stage = 0 is reached 
//which means that the variance is less than the smallest
//tolerance defined in $tolerance.

function calculate_stage($tolerance,$olddata,$newdata) {
	$stage = count($tolerance);
	foreach($tolerance as $value) {
		$within_tolerance = TRUE;
		for($i=0;$i<count($newdata);$i++) {
			for($j=0;$j<count($newdata[$i]);$j++) {
				if($newdata[$i][$j]+$olddata[$i][$j]!=0) if(abs(2*($olddata[$i][$j] - $newdata[$i][$j])/($newdata[$i][$j]+$olddata[$i][$j])) > $value) $within_tolerance = FALSE;
			}
		}
		if($within_tolerance) $stage -= 1;
	}
   return $stage;
}

//#############################################################


//#############################################################
//####################CALCULATIONS START#######################
//#############################################################

//move results to archive and preparing new results folder
if(file_exists('data/results/bioenergy_demand_raw.put') & !$dummy) {
	$date = date("Y-m-d-His"); // part of new foldername
	if(!rename("data/results","data/archive/results".$date)) echo "\n**** Error: could NOT move data/results to data/archive/results".$date."\n";
	if(!mkdir("data/results/")) echo "\n**** Error: could NOT create new folder data/results\n";
	if(!rename("data/archive/results".$date."/.svn","data/results/.svn")) echo "\n**** Error: could NOT move data/archive/results".$date."/.svn to data/results/.svn\n";
	}

// save important data
if(!$dummy) {
  foreach ($save_files as $array) {
	foreach($array as $path => $file) {
		if(!copy($path."".$file,"data/results/".$file)) echo "\n**** Error: could NOT copy ".$path."".$file." to data/results \n"; 
		}
	}	
}
//setting loop counter to initial value 1
$loopcount = 0;

//starting do-while-loop
do {
	$loopcount += 1;
	
    //execute remind, transfer bioenergy demand to magpie and save outputs
    echo "\n\nLoop: ".$loopcount."	Stage: " . $stage . "\n   Starting ReMind!\n";
  
	if(!$dummy) mkdir('data/results/output');
	$rem_start = time();
	if(!$dummy) {
		passthru('gunzip -f remind/types/recipe/in/*.gdx.gz');
		@exec($gamspath .'gams remind.gms ps=9999 pw=185 -WORKDIR "remind/types/recipe/in/" -INPUTDIR "../../../core/" -PUTDIR "../../../../data/results/output/"  > remind/types/recipe/in/remind.log 2>&1');
		//passthru('cp -rp data/results/rem/*.* data/results/output/'); 
		passthru('gzip remind/types/recipe/in/*.gdx');
		}
	$rem_stop = time();
	$rem_time = round($rem_stop-$rem_start,2);

	echo "   ReMIND results:\n\n";
	
	//##############################################################################################
	//########################### FILE TRANSFER: REMIND --> MAGPIE #################################
	//##############################################################################################	
	
    //Bioenergy demand
	$file 		= "bioenergy_demand_purpose_reg.rem.csv";	//file name
	$min  		= 0.0001;			//minimal allowed value (in output units)
	$max  		= 100000000.0;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "TWa";		//data unit (input)
	$unit_out 	= "PJ";			//data unit (output)
	$factor 	= 31557.6;		//transformation factor from input unit to output unit
	$transrule  = "share";

	$demand = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	
	if($demand->modelstat!=2 & $demand->modelstat!=7) exit("REMIND is infeasible, iteration process stopped!");
	
	$demand->write2csv(rem2mag::$OUTPUT_FOLDER . "bioenergy_demand_reg.csv");
	$demand->write2csv(mag2rem::$OUTPUT_FOLDER . "bioenergy_demand_purpose_reg.rem.csv","rem");
	//$demand->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_demand_purpose_reg.rem.csv","rem"); //Backup
	$demand->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_demand_purpose_reg.mag.csv"); 	  //Backup
	
	$demand->write2put("bioenergy_demand",$loopcount);
	$demand->display("remmag");

	//CO2 prices
	$file 		= "res_co2price.csv";	//file name
	$min  		= 0.001;		//minimal allowed value (in output units)
	$max  		= 100000000;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "T$2005/GtC";	//data unit (input)
	$unit_out 	= "$1995/tC";	//data unit (output)
	$factor 	= 0.708/0.907*1000;		//transformation factor from input unit to output unit
	$transrule  = "empty";

	$cprices = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$cprices->write2csv(rem2mag::$OUTPUT_FOLDER . "c_prices.csv");
	$cprices->write2csv(transformer::$TRANSFER_FOLDER . "c_prices.rem.csv","rem"); //Backup
	$cprices->write2csv(transformer::$TRANSFER_FOLDER . "c_prices.mag.csv"); 	   //Backup
	
	$cprices->write2put("c_prices",$loopcount);
	$cprices->display("remmag");
	
	//CH4 prices
	$file 		= "res_ch4price.csv";	//file name
	$min  		= 0.001;		//minimal allowed value (in output units)
	$max  		= 100000000;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "T$2005/MtCH4";	//data unit (input)
	$unit_out 	= "$1995/tCH4";	//data unit (output)
	$factor 	= 0.708/0.907*1000000;		//transformation factor from input unit to output unit
	$transrule  = "empty";

	$ch4prices = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$ch4prices->write2csv(rem2mag::$OUTPUT_FOLDER . "ch4_prices.csv");
	$ch4prices->write2csv(transformer::$TRANSFER_FOLDER . "ch4_prices.rem.csv","rem"); //Backup
	$ch4prices->write2csv(transformer::$TRANSFER_FOLDER . "ch4_prices.mag.csv"); 	//Backup
	
	$ch4prices->write2put("ch4_prices",$loopcount);
	$ch4prices->display("remmag");
	
	//N2O prices
	$file 		= "res_n2oprice.csv";	//file name
	$min  		= 0.001;		//minimal allowed value (in output units)
	$max  		= 100000000;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "T$2005/MtN";	//data unit (input)
	$unit_out 	= "$1995/tN";	//data unit (output)
	$factor 	= 0.708/0.907*1000000;		//transformation factor from input unit to output unit
	$transrule  = "empty";

	$n2oprices = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$n2oprices->write2csv(rem2mag::$OUTPUT_FOLDER . "n2on_prices.csv");
	$n2oprices->write2csv(transformer::$TRANSFER_FOLDER . "n2on_prices.rem.csv","rem"); //Backup
	$n2oprices->write2csv(transformer::$TRANSFER_FOLDER . "n2on_prices.mag.csv"); 	//Backup
	
	$n2oprices->write2put("n2on_prices",$loopcount);
	$n2oprices->display("remmag");	
	
	//##############################################################################################
	//########################### FILE TRANSFER: REMIND --> SHIFT  #################################
	//##############################################################################################

    //Bioenergy costs from Remind (emulator costs)
	$file 		= "bioenergy_costsemu_purpose_reg.rem.csv";	//file name
	$min  		= 0.0001;			//minimal allowed value (in output units)
	$max  		= 100000000.0;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "T$2005";		//data unit (input)
	$unit_out 	= "T$2005";			//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit
	$transrule  = "empty";

	$costs_rem = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$costs_rem->write2csv($shiftfolder . "bioenergy_costsemu_purpose_reg.rem.csv","rem");
	$costs_rem->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_costsemu_purpose_reg.rem.csv","rem"); //Backup

	$costs_rem->write2put("bioenergy_costemu",$loopcount);
	$costs_rem->display("remmag");

    //Bioenergy prices from Remind (emulator costs)
	$file 		= "bioenergy_priceemu_purpose_reg.rem.csv";	//file name
	$min  		= 0.0001;			//minimal allowed value (in output units)
	$max  		= 100000000.0;	//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "T$2005";		//data unit (input)
	$unit_out 	= "T$2005";			//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit
	$transrule  = "empty";

	$prices_rem = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$prices_rem->write2csv($shiftfolder . "bioenergy_priceemu_purpose_reg.rem.csv","rem");
	$prices_rem->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_priceemu_purpose_reg.rem.csv","rem"); //Backup

	$prices_rem->write2put("bioenergy_priceemu",$loopcount);
	$prices_rem->display("remmag");
	
	// Write meta infos to put-file
	$meta = new metainfo($loopcount,"rem",$rem_time,1,$demand->modelstat,$stage);
	$meta->write2put("metainfo.put");


	//##############################################################################################
	//##############################################################################################
		
	
    $new_demand = $demand->data_in->data;  

    // copy remind output into remind input for next iteration
    if(!$dummy) {
		//negishi weights 
		if(!copy("data/results/output/nwres.put","remind/types/recipe/in/nwres.put")) echo "\n**** Error: could NOT copy nwres.put to /in \n"; 
		if(!copy("remind/types/recipe/in/hybrid_p.gdx.gz","remind/types/recipe/in/hybrid.gdx.gz")) echo "\n**** Error: could NOT copy gdx \n"; 
		//only used by remind in tax cases
		if(!copy("data/results/output/fromres_emineg_alloc.put","remind/types/recipe/in/fromres_emineg_alloc.put")) echo "\n**** Error: could NOT copy fromres_emineg_alloc.put to /in \n"; 
		if(!copy("data/results/output/fromres_emieng_alloc.put","remind/types/recipe/in/fromres_emieng_alloc.put")) echo "\n**** Error: could NOT copy fromres_emieng_alloc.put to /in \n"; 
		//save Remind list file for diagnosis
		if(!copy("remind/types/recipe/in/remind.lst","remind/types/recipe/in/remind_last.lst")) echo "\n**** Error: could NOT copy remind.lst \n"; 
	}
    // rename output-folder
	if(!$dummy) rename('data/results/output',"data/results/remind_$loopcount");
	// create new output-folder
	if(!$dummy) mkdir('data/results/output');

	//execute magpie, transfer shadowprices for bioenergy to remind and save outputs
	echo "   Starting MAgPIE!\n";

	// Start Magpie
	$mag_start = time();
	if(!$dummy) {
		chdir("magpie/");
		passthru($phppath .'php magpie_start.php remind.cfg');
		//passthru('cp -rp ../data/results/mag/*.* ../data/results/output/');
		chdir("../");
	}
    $mag_stop = time();
	$mag_time = round($mag_stop-$mag_start,2);

	echo "   MAgPIE results:\n\n";
   
   	//##############################################################################################
	//########################### FILE TRANSFER: MAGPIE --> REMIND #################################
	//##############################################################################################

	//Bioenergy prices (purpose grown)
	$file 		= "reg.bioenergy_prices.mag.csv";	//file name
	$min  		= 0.00001;			//minimal allowed value  (in output units)
	$max  		= 100.0;			//maximal allowed value (in output units)
	$base 		= 100.0;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "USD/GJ";			//data unit (input)
	$unit_out 	= "USD/Wa";			//data unit (output)
	$factor 	= 0.0315576*(0.907/0.708);	//transformation factor from input unit to output unit; 0.907/0.708=conversion from $1995 to $2005 using CPI from http://oregonstate.edu/cla/polisci/sahr-robert
	$transrule  = "mean";

	$prices = new mag2rem($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	
	if(!$prices->allyears_feasible) exit("MAgPIE is infeasible, iteration process stopped!");
	
	$prices->write2csv($shiftfolder . "bioenergy_pricemag_purpose_reg.rem.csv");
	$prices->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_price_purpose_reg.rem.csv"); //Backup
	$prices->write2put("bioenergy_prices",$loopcount);
	$prices->display("remmag");
	
	//Bioenergy production costs (purpose grown)
	$file 		= "reg.costs_bioenergy.mag.csv";	//file name
	$min  		= 0.000001;		//minimal allowed value (in output units)
	$max  		= 100;			//maximal allowed value (in output units)
	$base 		= 0.01;			//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "MegaUSD1995";			//data unit (input)
	$unit_out 	= "TeraUSD2005";			//data unit (output)
	$factor 	= 0.907/0.708/1000000;	//transformation factor from input unit to output unit; 
	$transrule  = "share";
	
	$costs = new mag2rem($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$costs->write2csv($shiftfolder . "bioenergy_costsmag_purpose_reg.rem.csv");
	$costs->write2csv(transformer::$TRANSFER_FOLDER . "bioenergy_costs_purpose_reg.rem.csv"); //Backup
	$costs->write2put("bioenergy_costs",$loopcount);
	$costs->display("remmag");

	//CO2 emissions
	$file 		= "reg.c.mag.csv";	//file name
	$min  		= -10.0;	//minimal allowed value  (in output units)
	$max  		= 10.0;	//maximal allowed value (in output units)
	$base 		= 0.01;	//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "MtC";	//data unit (input)
	$unit_out 	= "GtC";	//data unit (output)
	$factor 	= 1/1000;		//transformation factor from input unit to output unit; 
	$transrule  = "share";

	$co2emissions = new mag2rem($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$co2emissions->write2csv(mag2rem::$OUTPUT_FOLDER . "co2emissions.rem.csv");
	$co2emissions->write2csv(transformer::$TRANSFER_FOLDER . "co2emissions.rem.csv"); //Backup
	
	$co2emissions->write2put("co2emissions",$loopcount);
	$co2emissions->display("remmag");

	//N2O emissions
	$file 		= "reg.n2o-n.mag.csv";	//file name
	$min  		= 0.01;	//minimal allowed value  (in output units)
	$max  		= 50.0;	//maximal allowed value (in output units)
	$base 		= 5.0;	//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "MtN";	//data unit (input)
	$unit_out 	= "MtN";	//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit; 
	$transrule  = "share";

	$n2oemissions = new mag2rem($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$n2oemissions->write2csv(mag2rem::$OUTPUT_FOLDER . "n2oemissions.rem.csv");
	$n2oemissions->write2csv(transformer::$TRANSFER_FOLDER . "n2oemissions.rem.csv"); //Backup
	
	$n2oemissions->write2put("n2oemissions",$loopcount);
	$n2oemissions->display("remmag");

	//CH4 emissions
	$file 		= "reg.ch4.mag.csv";	//file name
	$min  		= 0.01;	//minimal allowed value  (in output units)
	$max  		= 1000.0;	//maximal allowed value (in output units)
	$base 		= 100.0;	//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "MtCH4";	//data unit (input)
	$unit_out 	= "MtCH4";	//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit; 
	$transrule  = "share";

	$ch4emissions = new mag2rem($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$ch4emissions->write2csv(mag2rem::$OUTPUT_FOLDER . "ch4emissions.rem.csv");
	$ch4emissions->write2csv(transformer::$TRANSFER_FOLDER . "ch4emissions.rem.csv"); //Backup
	
	$ch4emissions->write2put("ch4emissions",$loopcount);
	$ch4emissions->display("remmag");
	
	// Write meta infos to file
	$meta = new metainfo($loopcount,"mag",$mag_time,1,$prices->modelstat,$stage);
	$meta->write2put("metainfo.put");

	if(!$dummy) rename('data/results/output',"data/results/magpie_$loopcount");

	//Start shift factor calculation
	echo "   Starting SHIFT FACTOR CALCULATION!\n";
	if(!$dummy) mkdir('data/results/output');
	chdir("shift/");
	passthru($gamspath .'gams calculate_shift.gms ps=9999 pw=185 -PUTDIR "../data/results/output/" -LOGOPTION=' . $logoption);
	chdir("../");

	//##############################################################################################
	//########################### FILE TRANSFER: SHIFT --> REMIND  #################################
	//##############################################################################################
	
	//Shift factor: from shift-calculation-script to Remind
	$file 		= "res_shift_price.csv";	//file name
	$min  		= 0.01;	//minimal allowed value  (in output units)
	$max  		= 50.0;	//maximal allowed value (in output units)
	$base 		= 1.0;	//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "-";	//data unit (input)
	$unit_out 	= "-";	//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit; 
	$transrule  = "empty";

	$shift_factor = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$shift_factor->display("remmag");
	$shift_factor->write2csv(mag2rem::$OUTPUT_FOLDER . "res_shift_price.csv","rem");
	//$shift_factor->write2csv(transformer::$TRANSFER_FOLDER . "shift_factor_price.rem.csv","rem");
	$shift_factor->write2put("shift_factor_price",$loopcount);

	//Shift factor: from shift-calculation-script to Remind
	$file 		= "res_shift_cost.csv";	//file name
	$min  		= 0.01;	//minimal allowed value  (in output units)
	$max  		= 50.0;	//maximal allowed value (in output units)
	$base 		= 1.0;	//base value (is used as replacement if model was infeasible) (in output units)
	$unit_in 	= "-";	//data unit (input)
	$unit_out 	= "-";	//data unit (output)
	$factor 	= 1;		//transformation factor from input unit to output unit; 
	$transrule  = "empty";

	$shift_factor = new rem2mag($file,$min,$max,$base,$unit_in,$unit_out,$factor,$stage,$transrule);
	$shift_factor->display("remmag");
	$shift_factor->write2csv(mag2rem::$OUTPUT_FOLDER . "res_shift_cost.csv","rem");
	//$shift_factor->write2csv(transformer::$TRANSFER_FOLDER . "shift_factor_cost.rem.csv","rem");
	$shift_factor->write2put("shift_factor_cost",$loopcount);

	// Delete results from shift factor calculatio after they were transferred to remind and log files
	passthru('rm -rf data/results/output');

	if ($run_r ==1) {
	   echo "   Starting R-scripts.\n\n";
	   chdir("./r/");
	   if($loopcount==1) $loopend = $loopcount;
	   if($loopcount==2) $loopend = $loopcount-1;
	   if($loopcount>2)  $loopend = $loopcount-2;
	   @exec($rpath . 'Rscript calc_glob.r file_folder=../' . transformer::$PUT_FOLDER . ' > calc_glob.log 2>&1');
	   @exec($rpath . 'Rscript plot.r file_folder=../' . transformer::$PUT_FOLDER . ' iterations=' . $loopcount . ':' . $loopend . ' > plot.log 2>&1');
	   @exec($rpath . 'Rscript plot_supplycurve.r file_folder=../' . transformer::$PUT_FOLDER . ' iterations=' . $loopcount . ':' . $loopend . ' > plot_supplycurve.log 2>&1');
	   @exec($rpath . 'Rscript SSPplot.r file_folder=../' . transformer::$PUT_FOLDER . ' iterations=' . $loopcount . ':' . $loopend . ' last_iter=' . $loopend . ' > sspplot.log 2>&1');
	   @exec($rpath . 'Rscript magpie_plot.R i=' . $loopcount . ' > magpie_plot.log 2>&1');
	   chdir("../");
	}

	//calculate actual solving stage
	if($loopcount > 1) {
	$stage = calculate_stage($tolerance,$old_demand,$new_demand);
	} 
 
	//the new data become old data for the next iteration  
	$old_demand = $new_demand;

	// read infromation form settings.ini
	$externalstop = 0;
	$data = file('php/settings.ini');
	$externalstop = trim($data[0]);
 
} while (($stage>0) & (($loopcount<$max_iterations) | ($max_iterations<0)) & $externalstop!=1);

echo "\nIteration finished!	Last iteration: {$loopcount}	Final stage: {$stage} \n\n";
if ($externalstop==1) echo "  !!! Externally stopped !!!";
echo "\n\n";

?>
