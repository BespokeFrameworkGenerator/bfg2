#! /usr/bin/env python
# manually created main program to couple MAgPIE and REMIND.
# The models are dummy models which simply read data from a file.
# Currently this is a one wasy coupling.
# The actual coupling is 2 way.
# The one way coupling is representative and does not give the same results as the real example.

# import models and transformations
from magpie_model import magpie_model
from remind_model import remind_model

from matscalmult_trans import matscalmult_trans
from timesegment_trans import timesegment_trans
from matmult_trans import matmult_trans
from interp_trans import interp_trans

# instantiate and initialise models and transformations
magpie_instance=magpie_model()
remind_instance=remind_model()
# MAgPIE US$/GJ to REMIND US$/Wa
matscalmult_instance=matscalmult_trans(0.0315576)

m_timeseries=magpie_instance.getTimeSeries()
r_timeseries=remind_instance.getTimeSeries()
timeseg_instance=timesegment_trans(m_timeseries,r_timeseries)
mag2rem_instance=matmult_trans("mag2rem.csv")
m_seg_timeseries=timeseg_instance.getNewTimeSeries()
interp_instance=interp_trans(m_seg_timeseries,r_timeseries)

# run magpie
m_data_out=magpie_instance.run()

# (temporal) take the required timeslice of the data
m_data_out=timeseg_instance.run(m_data_out)

# (units) convert from magpie to remind units
matscalmult_instance.run(m_data_out)

# (spatial) translation from magpie's representation to remind's representation
m_data_out=mag2rem_instance.run(m_data_out)

# (temporal) interpolation to remind's timeseries
m_data_out=interp_instance.run(m_data_out)

# run remind
r_data_out=remind_instance.run(m_data_out)

exit(0)
