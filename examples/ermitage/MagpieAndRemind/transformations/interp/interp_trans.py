from numpy import zeros,matrix
class interp_trans:
    def __init__(self,fromTimeSeries,toTimeSeries):
        #print "fromTimeSeries is of size : ",len(fromTimeSeries)
        #print "toTimeSeries is of size : ",len(toTimeSeries)
        self.weights=zeros((len(toTimeSeries),len(fromTimeSeries)))
        # create weights
        hiIndex=0
        #print len(fromTimeSeries)
        for (toIndex, time) in enumerate(toTimeSeries):
            while (hiIndex<(len(fromTimeSeries)-1) and fromTimeSeries[hiIndex]<time):
                hiIndex+=1
            if fromTimeSeries[hiIndex]<=time:
                loIndex=hiIndex
            else:
                loIndex=hiIndex-1
            #print time,fromTimeSeries[loIndex],fromTimeSeries[hiIndex]
            if loIndex==hiIndex:
                self.weights[toIndex,hiIndex]=1.0
            else:
                fromHiData=float(fromTimeSeries[hiIndex])
                fromLoData=float(fromTimeSeries[loIndex])
                fromRange=fromHiData-fromLoData
                loWeight=(fromHiData-time)/fromRange
                hiWeight=(time-fromLoData)/fromRange
                self.weights[toIndex,hiIndex]=hiWeight
                self.weights[toIndex,loIndex]=loWeight
        # ** NEED TO SORT OUT WEIGHTS AT EDGES (when toData is beyond fromData)

    def run(self,dataIn):
        dataOut=self.weights*dataIn
        print "interp_trans:run: shape of dataIn is ",dataIn.shape
        print "interp_trans:run: shape of dataOut is ",dataOut.shape
        return dataOut
