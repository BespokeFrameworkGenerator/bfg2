class timesegment_trans:
    def __init__(self,timeSeries,reference):
        # check for strictly increasing values
        if not(all(x<y for x, y in zip(reference, reference[1:]))):
            print "ERROR: list values should be increasing"
            exit(1)
        if not(all(x<y for x, y in zip(timeSeries, timeSeries[1:]))):
            print "ERROR: list values should be increasing"
            exit(1)
        startYear=min(reference)
        endYear=max(reference)

        # check that reference range overlaps timeseries
        if startYear>max(timeSeries) or endYear<min(timeSeries):
            print "Error: timeseries must be overlapping"
            print "MIN of timeseries is ",min(timeSeries)
            print "MAX of timeseries is ",max(timeSeries)
            exit(1)

        if startYear in timeSeries:
            self.loIndex=timeSeries.index(startYear)
        else:
            for value in timeSeries:
                if value>startYear:
                    self.loIndex=min(0,timeSeries.index(value)-1)
                    break
        if endYear in timeSeries:
            self.hiIndex=timeSeries.index(endYear)
        else:
            self.hiIndex=timeSeries.index(max(timeSeries))
            for value in timeSeries:
                if value>endYear:
                    self.hiIndex=timeSeries.index(value)
                    break
        self.newTimeSeries=timeSeries[self.loIndex:self.hiIndex+1]
        #print "timeseg: reference startyear=",startYear
        #print "timeseg: reference endyear=",endYear
        #print "timeseg: data startyear=",min(timeSeries)
        #print "timeseg: data endyear=",max(timeSeries)
        maxIndex=timeSeries.index(max(timeSeries))
        #print "timeseg: data max index=",maxIndex
        #print "timeseg: loIndex is ",self.loIndex
        #print "timeseg: hiIndex is ",self.hiIndex
        #print "timeseg: I will remove ",self.loIndex," rows from the start of the data"
        #print "timeseg: I will remove ",maxIndex-self.hiIndex," rows from the end of the data"

    def getNewTimeSeries(self):
        return self.newTimeSeries

    def run(self,dataIN):
        # return appropriately sliced array
        #print "HELLO FROM timesegment.run"
        #print "data type of data is ",type(dataIN)
        sliced=dataIN[self.loIndex:self.hiIndex+1,:]
        print "timesegment_trans:run: removing unnecessary data"
        print "timesegment_trans:run: shape of dataIn is ",dataIN.shape
        print "timesegment_trans:run: shape of dataOut is ",sliced.shape
        return sliced

