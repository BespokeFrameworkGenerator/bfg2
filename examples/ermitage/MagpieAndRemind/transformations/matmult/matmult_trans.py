from CSVReader import CSVReader


class matmult_trans(object):

    def __init__(self, fileName):
        self.csvdata = CSVReader(fileName, rhSep=",")

    def run(self, magData):
#  Translate from one form to the other...
#       outData = magData * self.csvdata.matrix
#  This now uses the dot() method to explicitly form a matrix product,
#  numpy overloads the * operator to perform this for the matrix type,
#  but not for the array type, where * means elementwise scalar multiplication.

        outData = magData.dot(self.csvdata.matrix)

        print("[matmult_trans::run] Shape of magData: {0}".format(magData.shape))
        print("[matmult_trans::run] Shape of self.csvdata.matrix: {0}".format(self.csvdata.matrix.shape))
        print("[matmult_trans::run] Shape of outData: {0}".format(outData.shape))

        return outData
