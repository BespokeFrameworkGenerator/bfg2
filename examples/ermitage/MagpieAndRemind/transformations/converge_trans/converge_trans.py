class converge_trans:

    def __init__(self,tolerances,max_iterations,old):
        self.max_iterations=max_iterations
        self.tolerances=tolerances
        self.nTolerance=len(self.tolerances)
        self.old=old
        self.iterations=0

    def first(self):
        if self.max_iterations<1:
            return True
        else:
            return False

    def check(self,new):

        assert len(self.old)==len(new), "Error, length of old is "+str(len(self.old))+" but length of new is "+str(len(new))
        assert len(self.old.shape)==2 and len(new.shape)==2, "Error"

        stage=self.nTolerance
        for tolerance in self.tolerances:
            within_tolerance=True
            for i in range(0,self.old.shape[0]):
                if not within_tolerance : break
                for j in range(0,self.old.shape[1]):
                    if new[i][j]+self.old[i][j]!=0:
                        if abs(2*(self.old[i][j] - new[i][j])/(new[i][j]+self.old[i][j])) > tolerance:
                            within_tolerance = False
                            break
            if within_tolerance:
                stage -= 1

        self.old=new

        self.iterations+=1

        if stage==0 or self.iterations>=self.max_iterations:
            return True,stage
        else:
            return False,stage
