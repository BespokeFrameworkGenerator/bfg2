from utils import readCSVData
class remind_model:

    def __init__(self):
        #Read my bioenergy demand from file bioenergy_demand_reg.rem.csv
        file = "bioenergy_demand_reg.rem.csv"
        self.colLabels,self.rowLabels,self.data=readCSVData(file)
        self.time=[]
        for label in self.rowLabels:
            self.time.append(int(label))

    def getTimeSeries(self):
        return self.time

    def getBioDemand(self):
        return self.data

    def run(self,dataIn):
        print "remind:run: shape of dataIn is ",dataIn.shape
        print "remind:run: shape of internal data is ",self.data.shape
        #print "colLabels = ",self.colLabels
        #print "rowLabels = ",self.rowLabels
        #print "time = ",self.time
        #print "data = ",self.data
        return self.data

