import csv
from numpy import array
def readCSVData(file):
    f=open(file,"r")
    rawdata=csv.reader(f)
    first=True
    colLabels=[]
    rowLabels=[]
    dataList=[]
    for line in rawdata:
        if first:
            first=False
            colLabels=line[1:]
        else:
            rowLabels.append(line[0])
            dataList.append(line[1:])
    data=array(dataList,dtype=float)
    return colLabels,rowLabels,data

