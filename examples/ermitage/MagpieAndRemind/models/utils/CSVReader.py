from numpy import array, matrix


class CSVReader(object):

    def __init__(self, filename, sep=",", rhSep=" ", noRowHeaders=False):
        self.filename = filename
        self.sep = sep
        self.rhSep = rhSep
        self.noRowHeaders = noRowHeaders
        self.colOffset = 0 if noRowHeaders else 1

        try:
            fp = open(self.filename, 'r')
        except IOError as e:
            print("[{0}] Error: unable to open file {1}".format(self.__class__.__name__, self.filename))
            raise

        self.cheaders = [ h for h in fp.readline().rstrip().split(self.sep)[self.colOffset:] ]
        self.rheaders = []
        self.data = []

        body = fp.read().splitlines()
        fp.close()

        if noRowHeaders:
            for line in body:
                splitline = line.split(self.sep)
                stripline = [ e.strip() for e in splitline ]
                self.data.append(stripline)
        else:
            for line in body:
                rheader, rdata = line.split(self.rhSep, 1)
        
                self.rheaders.append(rheader.strip())
                splitline = rdata.split(self.sep)
                stripline = [ e.strip() for e in splitline ]
                self.data.append(stripline)



    @property
    def cols(self):
        return self.cheaders

    @property
    def rows(self):
        return self.rheaders
    
    @property
    def colcount(self):
        return len(self.cheaders)
    
    @property
    def rowcount(self):
        return len(self.data)

    @property
    def array(self):
        return array(self.data, dtype=float)

    @property
    def matrix(self):
        return matrix(self.data, dtype=float)


    def __str__(self):
        width = max(len(self.data[0][0]), max(map(len, self.cheaders))) + 1
       
        ret = " "*width if self.colOffset else ""

        for h in self.cheaders:
            ret += "{0:^{1}}".format(h, width)
        
        ret += "\n"

        for row, line in enumerate(self.data):
            rh = "" if self.noRowHeaders else self.rheaders[row]
            if rh:
                ret += "{0:^{1}}".format(rh, width)
    
            for elem in line:
                ret += elem.center(width)

            ret += "\n"

        return ret + "\n"


if __name__ == "__main__":

    c = CSVReader("rem2mag.csv")
    print("Cols: {0}, Rows: {1}\nColumn headers: {2}\nRow headers: {3}".format(c.colcount, c.rowcount, c.cols, c.rows))
    print(c)
#    print("Data:\n{0}".format(c.array))

    r = CSVReader("bioenergy_demand_reg.rem.csv", rhSep=",")
    print("Cols: {0}, Rows: {1}\nColumn headers: {2}\nRow headers: {3}".format(r.colcount, r.rowcount, r.cols, r.rows))
    print(r)
#    print("Data:\n{0}".format(r.array))

#    t = CSVReader("TWout.put", sep=";", noRowHeaders=True)
#    print("Cols: {0}, Rows: {1}\nColumn headers: {2}\nRow headers: {3}".format(t.colcount, t.rowcount, t.cols, t.rows))
#    print(t)
#    print("Data:\n{0}".format(t.array))

