from utils import readCSVData
class magpie_model:

    def __init__(self):
        # Read my bioenergy prices from file bioenergy_prices_reg.mag.csv
        file = "bioenergy_prices_reg.mag.csv"
        self.colLabels,self.rowLabels,self.data=readCSVData(file)
        # time is returned in the form "yYEAR", so strip off "y"
        self.time=[]
        for data in self.rowLabels:
            self.time.append(int(data[1:]))

    def getTimeSeries(self):
        return self.time

    def getBioPrices(self):
        return self.data

    def run(self,dataIn):
        #print "colLabels = ",self.colLabels
        #print "rowLabels = ",self.rowLabels
        #print "time = ",self.time
        #print "data = ",self.data
        print "magpie:run: shape of dataOut is ",self.data.shape
        return self.data

