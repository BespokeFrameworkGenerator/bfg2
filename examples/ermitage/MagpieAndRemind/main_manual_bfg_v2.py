#! /usr/bin/env python
# manually created main program to couple MAgPIE and REMIND.
# The models are dummy models which simply read data from a file.
# This is a copy of the remind_magpie php file

from array import array

# import models and transformations
from remind_model import remind_model
from magpie_model import magpie_model
from converge_trans import converge_trans
from count_trans import count_trans
from matscalmult_trans import matscalmult_trans
from timesegment_trans import timesegment_trans
from matmult_trans import matmult_trans
from interp_trans import interp_trans

max_iterations=30
tolerance=array('f',[0.05,0.01])

# instantiate and initialise models and transformations
magpie_instance=magpie_model()
remind_instance=remind_model()

count_instance=count_trans(0)
matscalmult_rem2mag=matscalmult_trans(31557.6)
matmult_rem2mag=matmult_trans("rem2mag.csv")
matscalmult_mag2rem=matscalmult_trans(0.0315576*(0.907/0.708))
matmult_mag2rem=matmult_trans("mag2rem.csv")

m_timeseries=magpie_instance.getTimeSeries()
r_timeseries=remind_instance.getTimeSeries()

interp_rem2mag=interp_trans(r_timeseries,m_timeseries)
interp_mag2rem=interp_trans(m_timeseries,r_timeseries)

Bio_demand=remind_instance.getBioDemand()
converge_instance=converge_trans(tolerance,max_iterations,Bio_demand)
Bio_prices=magpie_instance.getBioPrices()

# compute
complete=converge_instance.first()

while not complete:
    
    # run remind
    Bio_demand=remind_instance.run(Bio_prices)
    Bio_demand_orig=Bio_demand
    # (units) convert from remind to magpie units
    matscalmult_rem2mag.run(Bio_demand)
    # (spatial) translation from remind's representation to magpie's representation
    Bio_demand=matmult_rem2mag.run(Bio_demand)
    # (temporal) interpolation to magpie's timeseries
    Bio_demand=interp_rem2mag.run(Bio_demand)

    # run magpie
    Bio_prices=magpie_instance.run(Bio_demand)

    # (units) convert from magpie to remind units
    matscalmult_mag2rem.run(Bio_prices)
    # (spatial) translation from magpie's representation to remind's representation
    Bio_prices=matmult_mag2rem.run(Bio_prices)
    # (temporal) interpolation to remind's timeseries
    Bio_prices=interp_mag2rem.run(Bio_prices)

    # check for convergence
    complete,stage=converge_instance.check(Bio_demand_orig)

    # increment counter
    count_instance.inc()

print "Run completed after "+str(count_instance.value())+" iterations."
print "Tolerance stage is "+str(stage)
exit(0)
