       program BFG2Main
       use BFG2Target
       use BFG2InPlace
       ! Begin declaration of Control
       integer :: nts1
       integer :: ModuleMagiccEsm_freq
       integer :: ModuleMagiccScm_freq
       integer :: ModuleIclipsIm_freq
       namelist /time/ nts1,ModuleMagiccEsm_freq,ModuleMagiccScm_freq,ModuleIclipsIm_freq
       ! End declaration of Control
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       call initComms()
       its1=0
       do its1=1,nts1
       if (ModuleMagiccEsmThread()) then
       if(mod(its1,ModuleMagiccEsm_freq).eq.0)then
       call setActiveModel(1)
       call ModuleMagiccEsm()
       end if
       end if
       if (ModuleMagiccScmThread()) then
       if(mod(its1,ModuleMagiccScm_freq).eq.0)then
       call setActiveModel(2)
       call ModuleMagiccScm()
       end if
       end if
       if (ModuleIclipsImThread()) then
       if(mod(its1,ModuleIclipsIm_freq).eq.0)then
       call setActiveModel(3)
       call ModuleIclipsIm()
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main

