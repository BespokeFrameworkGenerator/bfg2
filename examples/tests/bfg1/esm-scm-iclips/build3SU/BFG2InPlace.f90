       module BFG2InPlace
       use BFG2Target
       private 
       public get,put
       interface get
       module procedure getinteger0,getcharacter1,getinteger1,getreal1,getreal0
       end interface
       interface put
       module procedure putinteger0,putcharacter1,putinteger1,putreal1,putreal0
       end interface
       contains
       subroutine getinteger0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       if (arg2==999) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=1000
       du=b2mmap(1)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_integer,du,22,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine getinteger0
       subroutine getcharacter1(arg1,arg2)
       implicit none
       character (len=*) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       if (arg2==998) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=1001
       du=b2mmap(1)
       mysize=len(arg1)
       call mpi_recv(arg1,mysize,mpi_character,du,23,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine getcharacter1
       subroutine getinteger1(arg1,arg2)
       implicit none
       integer , dimension(:) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       if (arg2==0) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=0
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0
       end do
       call mpi_recv(arg1,mysize,mpi_integer,du,24,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine getinteger1
       subroutine getreal1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       if (arg2==1) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=1
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,25,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==2) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=2
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,26,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==3) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=3
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,27,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==4) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=4
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,28,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==5) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=5
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,29,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==6) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=6
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,30,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==7) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=7
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,31,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==8) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=8
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,32,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==9) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=9
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,33,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==10) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=10
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,34,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==11) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=11
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,35,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==12) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=12
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,36,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==13) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=13
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,37,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==14) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=14
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,38,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==15) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=15
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,39,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==16) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=16
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,40,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==17) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=17
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,41,mpi_comm_world,istatus,ierr)
       end if
       if (arg2==18) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccEsm ep=ModuleMagiccEsm id=18
       du=b2mmap(1)
       mysize=100
       do i1=1,size(arg1,1)
       arg1(i1)=0.0
       end do
       call mpi_recv(arg1,mysize,mpi_real,du,42,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine getreal1
       subroutine getreal0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       if (arg2==0) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=ModuleMagiccScm ep=ModuleMagiccScm id=22
       du=b2mmap(2)
       mysize=1
       call mpi_recv(arg1,mysize,mpi_real,du,44,mpi_comm_world,istatus,ierr)
       end if
       end if
       end subroutine getreal0
       subroutine putinteger0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       if (arg2==1000) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=999
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,22,mpi_comm_world,ierr)
       end if
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine putinteger0
       subroutine putcharacter1(arg1,arg2)
       implicit none
       character (len=*) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       if (arg2==1001) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=998
       mysize=len(arg1)
       call mpi_send(arg1,mysize,mpi_character,du,23,mpi_comm_world,ierr)
       end if
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine putcharacter1
       subroutine putinteger1(arg1,arg2)
       implicit none
       integer , dimension(:) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       if (arg2==0) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=0
       mysize=100
       call mpi_send(arg1,mysize,mpi_integer,du,24,mpi_comm_world,ierr)
       end if
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine putinteger1
       subroutine putreal1(arg1,arg2)
       implicit none
       real , dimension(:) :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       if (arg2==1) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=1
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,25,mpi_comm_world,ierr)
       end if
       if (arg2==2) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=2
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,26,mpi_comm_world,ierr)
       end if
       if (arg2==3) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=3
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,27,mpi_comm_world,ierr)
       end if
       if (arg2==4) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=4
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,28,mpi_comm_world,ierr)
       end if
       if (arg2==5) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=5
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,29,mpi_comm_world,ierr)
       end if
       if (arg2==6) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=6
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,30,mpi_comm_world,ierr)
       end if
       if (arg2==7) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=7
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,31,mpi_comm_world,ierr)
       end if
       if (arg2==8) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=8
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,32,mpi_comm_world,ierr)
       end if
       if (arg2==9) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=9
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,33,mpi_comm_world,ierr)
       end if
       if (arg2==10) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=10
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,34,mpi_comm_world,ierr)
       end if
       if (arg2==11) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=11
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,35,mpi_comm_world,ierr)
       end if
       if (arg2==12) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=12
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,36,mpi_comm_world,ierr)
       end if
       if (arg2==13) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=13
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,37,mpi_comm_world,ierr)
       end if
       if (arg2==14) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=14
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,38,mpi_comm_world,ierr)
       end if
       if (arg2==15) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=15
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,39,mpi_comm_world,ierr)
       end if
       if (arg2==16) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=16
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,40,mpi_comm_world,ierr)
       end if
       if (arg2==17) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=17
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,41,mpi_comm_world,ierr)
       end if
       if (arg2==18) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=ModuleMagiccScm ep=ModuleMagiccScm id=18
       mysize=100
       call mpi_send(arg1,mysize,mpi_real,du,42,mpi_comm_world,ierr)
       end if
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine putreal1
       subroutine putreal0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: mysize
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer :: currentModel
       integer :: i1
       integer, allocatable, dimension(:) :: set
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=ModuleMagiccEsm and ep=ModuleMagiccEsm
       end if
       if (currentModel==2) then
       ! I am model=ModuleMagiccScm and ep=ModuleMagiccScm
       if (arg2==22) then
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(3)
       ! receiving model: name=ModuleIclipsIm ep=ModuleIclipsIm id=0
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,44,mpi_comm_world,ierr)
       end if
       end if
       if (currentModel==3) then
       ! I am model=ModuleIclipsIm and ep=ModuleIclipsIm
       end if
       end subroutine putreal0
       end module BFG2InPlace
