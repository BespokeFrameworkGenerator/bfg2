       module BFG2Target
       integer :: bfgSUID
       integer :: b2mmap(3)
       integer :: activeModelID
       integer, target :: its1
       type modelInfo
        integer :: du
        integer :: period
        integer :: nesting
        integer :: bound
        integer :: offset
        integer, pointer :: its
       end type modelInfo

       type(modelInfo), dimension(3) :: info
       integer, parameter :: inf=32767
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer, intent(in)  :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function ModuleMagiccEsmThread()
       implicit none
       if (bfgSUID==1) then
       ModuleMagiccEsmThread=.true.
       else
       ModuleMagiccEsmThread=.false.
       end if
       end function ModuleMagiccEsmThread
       logical function ModuleMagiccScmThread()
       implicit none
       if (bfgSUID==2) then
       ModuleMagiccScmThread=.true.
       else
       ModuleMagiccScmThread=.false.
       end if
       end function ModuleMagiccScmThread
       logical function ModuleIclipsImThread()
       implicit none
       if (bfgSUID==3) then
       ModuleIclipsImThread=.true.
       else
       ModuleIclipsImThread=.false.
       end if
       end function ModuleIclipsImThread
       subroutine commsSync()
       implicit none
       include 'mpif.h'
       integer :: ierr
       call mpi_barrier(mpi_comm_world,ierr)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       include 'mpif.h'
       integer :: ierr
       integer :: globalsize
       integer :: globalrank
       integer :: colour
       integer :: key
       integer :: localsize
       integer :: localrank
       integer :: b2mtemp(3)
       integer :: mpi_comm_local
       call mpi_init(ierr)
       call mpi_comm_size(mpi_comm_world,globalsize,ierr)
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       ! arbitrarily decide on a unique colour for this deployment unit
       colour=1
       if (globalsize.ne.3) then
       print *,"Error: (du",colour,"):","3 threads should be requested"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,0)
       else
       key=0
       call mpi_comm_split(mpi_comm_world,colour,key,mpi_comm_local,ierr)
       call mpi_comm_size(mpi_comm_local,localsize,ierr)
       call mpi_comm_rank(mpi_comm_local,localrank,ierr)
       if (localsize.ne.3) then
       print *,"Error: 3 threads expected in du",colour
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,0)
       else
       ! arbitrarily bind model threads to a local rank (and therefore a global rank)
       b2mtemp=0
       if (localrank>=0.and.localrank<=0) then
       ! model name is 'ModuleMagiccEsm'
       bfgSUID=1
       if (localrank==0) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank>=1.and.localrank<=1) then
       ! model name is 'ModuleMagiccScm'
       bfgSUID=2
       if (localrank==1) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank>=2.and.localrank<=2) then
       ! model name is 'ModuleIclipsIm'
       bfgSUID=3
       if (localrank==2) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank<0.or.localrank>2) then
       print *,"'Error: (du",colour,",0): localrank has unexpected value"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,0)
       else
       ! distribute id's to all su's
       call mpi_allreduce(b2mtemp,b2mmap,3,mpi_integer,mpi_sum,mpi_comm_world,ierr)
       if (localrank==0) then
       print *,"du",colour,"bfg to mpi id map is",b2mmap
       end if
       end if
       end if
       end if
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       include 'mpif.h'
       integer :: ierr
       call mpi_finalize(ierr)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=ModuleMagiccEsm.ModuleMagiccEsm
       info(1)%du=1
       info(1)%period=1
       info(1)%nesting=1
       info(1)%bound=1
       info(1)%offset=0
       info(1)%its=>its1
       ! model.ep=ModuleMagiccScm.ModuleMagiccScm
       info(2)%du=1
       info(2)%period=1
       info(2)%nesting=1
       info(2)%bound=1
       info(2)%offset=0
       info(2)%its=>its1
       ! model.ep=ModuleIclipsIm.ModuleIclipsIm
       info(3)%du=1
       info(3)%period=1
       info(3)%nesting=1
       info(3)%bound=1
       info(3)%offset=0
       info(3)%its=>its1
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, allocatable, dimension(:) :: newlist
       integer, pointer :: its
       integer :: i
       integer :: newlsize
       integer :: currentNesting
       integer :: startPoint
       integer :: endPoint
       integer :: pos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getNext=findNext(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, intent(inout)  :: pos
       integer :: i
       integer :: j
       integer :: currentNesting
       integer, pointer :: previousIts
       integer :: startPoint
       integer :: endPoint
       integer :: newlsize
       integer :: nestNext
       integer :: currentMin
       integer :: remainIters
       integer :: waitIters
       integer :: its
       integer :: newpos
       integer :: saveits
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       previousIts=>info(list(pos))%its
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       its=info(list(pos))%its
       if (its==info(list(pos))%bound + 1) then
       its=1
       end if
       if (list(pos)>point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting>currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its>0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestNext=findNext(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       deallocate(newlist)
       else if (.not.(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       deallocate(newlist)
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters>0) then
       waitIters=info(list(pos))%period - mod(its,info(list(pos))%period)
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters<currentMin.and.waitIters<=remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
       end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, allocatable, dimension(:) :: newlist
       integer, pointer :: its
       integer :: i
       integer :: newlsize
       integer :: startPoint
       integer :: endPoint
       integer :: pos
       integer :: currentNesting
       integer :: targetpos
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getLast=findLast(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, intent(inout)  :: pos
       integer :: i
       integer :: j
       integer :: currentNesting
       integer, pointer :: previousIts
       integer :: startPoint
       integer :: endPoint
       integer :: newlsize
       integer :: nestLast
       integer :: currentMin
       integer :: elapsedIters
       integer :: its
       integer :: newpos
       integer :: saveits
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       previousIts=>info(list(pos))%its
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       its=info(list(pos))%its
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos)>=point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting>currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       saveits=info(list(pos))%its
       if (info(list(pos))%its>0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestLast=findLast(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       deallocate(newlist)
       else if (.not.(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       deallocate(newlist)
       else
       if (its>0) then
       elapsedIters=mod(its,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters<currentMin.and.elapsedIters<its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
       end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting<info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       do i=pos+1,lsize
       if (info(list(i))%nesting<info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer, pointer  :: its
       integer, intent(in)  :: nesting
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting<=nesting.and..not.(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
       findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer, pointer  :: its
       integer, intent(in)  :: nesting
       integer :: i
       do i=pos+1,lsize
       if (info(list(i))%nesting<=nesting.and..not.(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
       findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target
