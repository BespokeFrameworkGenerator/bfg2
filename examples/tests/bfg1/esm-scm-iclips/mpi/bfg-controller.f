
cnc This is auto-generated code and should not be modified
cnc Created by f77mpigenerator.xsl
cnc Author: C. Armstrong 
 
c
     
       program localhost
c
       include 'mpif.h'
c
       integer localrank,localsize,ierr,time
c
       common /cnccnc/ localrank,time
c
       call mpi_init(ierr)
       if (ierr .ne. mpi_success) stop 'error initializing MPI'
       call mpi_comm_size(mpi_comm_world,localsize,ierr)
       call mpi_comm_rank(mpi_comm_world,localrank,ierr)
c
       if(localsize .lt. 3) then
         print *,'Error: at least 3 threads should be requested'
         print *,'aborting ...'
       else 

         do time=1,
           if(localrank.eq.0)then
             if(mod(time, ).eq.0)then
               call ModuleMagiccEsm()
             end if
           end if

           if(localrank.eq.1)then
             if(mod(time, ).eq.0)then
               call ModuleMagiccScm()
             end if
           end if

           if(localrank.eq.2)then
             if(mod(time, ).eq.0)then
               call ModuleIclipsIm()
             end if
           end if

           call mpi_barrier(mpi_comm_world,ierr)
         end do
       end if
c
       call mpi_barrier(mpi_comm_world,ierr)
       call mpi_finalize(ierr)
c
       end
c
c
       subroutine put(field,field_tag)
c
       include 'mpif.h'
c
       real field
       integer field_tag,to,tag,ierr,myrank,time
c
       common /cnccnc/ myrank,time
c
100    format('PUT ',I4)

       if(myrank.eq.0)then
c  Hello there! My name is  ModuleMagiccEsm...
       
         if(field_tag.eq.1000)then
           write(*,100),1000
           tag=1
           to=1
           call mpi_send(field,1,
     +          MPI_INTEGER,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.1001)then
           write(*,100),1001
           tag=2
           to=1
           call mpi_send(field,20,
     +          MPI_CHARACTER,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.0)then
           write(*,100),0
           tag=3
           to=1
           call mpi_send(field,100,
     +          MPI_INTEGER,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.1)then
           write(*,100),1
           tag=4
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.2)then
           write(*,100),2
           tag=5
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.3)then
           write(*,100),3
           tag=6
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.4)then
           write(*,100),4
           tag=7
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.5)then
           write(*,100),5
           tag=8
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.6)then
           write(*,100),6
           tag=9
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.7)then
           write(*,100),7
           tag=10
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.8)then
           write(*,100),8
           tag=11
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.9)then
           write(*,100),9
           tag=12
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.10)then
           write(*,100),10
           tag=13
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.11)then
           write(*,100),11
           tag=14
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.12)then
           write(*,100),12
           tag=15
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.13)then
           write(*,100),13
           tag=16
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.14)then
           write(*,100),14
           tag=17
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.15)then
           write(*,100),15
           tag=18
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.16)then
           write(*,100),16
           tag=19
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.17)then
           write(*,100),17
           tag=20
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
         if(field_tag.eq.18)then
           write(*,100),18
           tag=21
           to=1
           call mpi_send(field,100,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
       end if
c

       if(myrank.eq.1)then
c  Hello there! My name is  ModuleMagiccScm...
       
         if(field_tag.eq.22)then
           write(*,100),22
           tag=0
           to=2
           call mpi_send(field,1,
     +          MPI_REAL,to,tag,MPI_COMM_WORLD,ierr)
         end if
       
       end if
c

       end
 
       subroutine get(field,field_tag)
c
       include 'mpif.h'
c
       real field
       integer field_tag,from,tag,ierr,istatus(mpi_status_size),myrank
       integer time
c
       common /cnccnc/ myrank,time
c
101    format('GET ',I4)

       if(myrank.eq.1)then
c  Hello there! My name is  ModuleMagiccScm...
   

        if(mod(time, ).eq.0)then
 
       
         if(field_tag.eq.999)then
           write(*,101),999
           tag=1
           from=0
           call mpi_recv(field,1,MPI_INTEGER,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.998)then
           write(*,101),998
           tag=2
           from=0
           call mpi_recv(field,20,MPI_CHARACTER,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.0)then
           write(*,101),0
           tag=3
           from=0
           call mpi_recv(field,100,MPI_INTEGER,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.1)then
           write(*,101),1
           tag=4
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.2)then
           write(*,101),2
           tag=5
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.3)then
           write(*,101),3
           tag=6
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.4)then
           write(*,101),4
           tag=7
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.5)then
           write(*,101),5
           tag=8
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.6)then
           write(*,101),6
           tag=9
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.7)then
           write(*,101),7
           tag=10
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.8)then
           write(*,101),8
           tag=11
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.9)then
           write(*,101),9
           tag=12
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.10)then
           write(*,101),10
           tag=13
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.11)then
           write(*,101),11
           tag=14
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.12)then
           write(*,101),12
           tag=15
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.13)then
           write(*,101),13
           tag=16
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.14)then
           write(*,101),14
           tag=17
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.15)then
           write(*,101),15
           tag=18
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.16)then
           write(*,101),16
           tag=19
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.17)then
           write(*,101),17
           tag=20
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
         if(field_tag.eq.18)then
           write(*,101),18
           tag=21
           from=0
           call mpi_recv(field,100,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
        end if
       end if

       if(myrank.eq.2)then
c  Hello there! My name is  ModuleIclipsIm...
   

        if(mod(time, ).eq.0)then
 
       
         if(field_tag.eq.0)then
           write(*,101),0
           tag=0
           from=1
           call mpi_recv(field,1,MPI_REAL,
     +          from,tag,MPI_COMM_WORLD,istatus,ierr)
         end if
       
        end if
       end if

       end
c
