      program ModuleMagiccEsmControl
cnc This is auto-generated code.
cnc Any modifications will therefore be overwritten.
cnc Created by f77tdtcontrolgen.xsl
cnc Author: R. Ford
      implicit none
      integer tdtConf
      integer out(1)
      common /cnctdtout/ out
      integer i
      call tdt_fconfigure(tdtConf,'ModuleMagiccEsmTDTConfig.xml')
cnc original tdt_fopen connection is to ModuleMagiccScm
      call tdt_fopen(out(1),tdtConf,'Out1')
      do i=1,1
        call ModuleMagiccEsm()
      end do
      call tdt_fclose(out(1))
      call tdt_fend(tdtConf)
      end
