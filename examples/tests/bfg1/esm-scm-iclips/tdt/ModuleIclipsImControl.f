      program ModuleIclipsImControl
cnc This is auto-generated code.
cnc Any modifications will therefore be overwritten.
cnc Created by f77tdtcontrolgen.xsl
cnc Author: R. Ford
      implicit none
      integer tdtConf
      integer in(1)
      common /cnctdtin/ in
      integer i
      call tdt_fconfigure(tdtConf,'ModuleIclipsImTDTConfig.xml')
cnc original tdt_fopen connection is from ModuleMagiccScm
      call tdt_fopen(in(1),tdtConf,'In1')
      do i=1,1
        call ModuleIclipsIm()
      end do
      call tdt_fclose(in(1))
      call tdt_fend(tdtConf)
      end
