      program ModuleMagiccScmControl
cnc This is auto-generated code.
cnc Any modifications will therefore be overwritten.
cnc Created by f77tdtcontrolgen.xsl
cnc Author: R. Ford
      implicit none
      integer tdtConf
      integer out(1)
      integer in(1)
      common /cnctdtout/ out
      common /cnctdtin/ in
      integer i
      call tdt_fconfigure(tdtConf,'ModuleMagiccScmTDTConfig.xml')
cnc original tdt_fopen connection is to ModuleIclipsIm
      call tdt_fopen(out(1),tdtConf,'Out1')
cnc original tdt_fopen connection is from ModuleMagiccEsm
      call tdt_fopen(in(1),tdtConf,'In1')
      do i=1,1
        call ModuleMagiccScm()
      end do
      call tdt_fclose(out(1))
      call tdt_fclose(in(1))
      call tdt_fend(tdtConf)
      end
