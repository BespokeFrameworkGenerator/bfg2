      subroutine InterfaceMagiccScm(nval,mnem,iy1,fos,def,dch4,dn2o,   &
       dnox,dvoc,dco,                                        &
       dso21,dso22,dso23,dcf4,dc2f6,d125,                    &
       d134a,d143a,d227,d245,dsf6)

! --- MKB: module version of interface with ESM

! ***MKB: mods for f77


      IMPLICIT NONE
      INTEGER maxarraysize
      PARAMETER (maxarraysize=100)
      INTEGER IY1(maxarraysize)

! use REAL
      REAL FOS, DEF, DCH4, DN2O, &
           DNOX, DVOC, DCO, &
           DSO21, DSO22, DSO23, DCF4, DC2F6, &
           D125, D134A, D143A, D227, D245, DSF6

      DIMENSION FOS(maxarraysize), DEF(maxarraysize), DCH4(maxarraysize), DN2O(maxarraysize), &
           DNOX(maxarraysize), DVOC(maxarraysize), DCO(maxarraysize), &
           DSO21(maxarraysize), DSO22(maxarraysize), DSO23(maxarraysize), DCF4(maxarraysize), DC2F6(maxarraysize), &
           D125(maxarraysize), D134A(maxarraysize), D143A(maxarraysize), D227(maxarraysize), D245(maxarraysize), DSF6(maxarraysize)

      CHARACTER*20 mnem

! declarations determined by Michael
      INTEGER:: nval, i


! --- MKB: get the scalars and character strings
      write (*,*) 'InterfaceMagiccScm: retrieving data'
      call get(nval,999)
      call get(mnem,998)

! --- MKB:  get each variable as complete vector (ie get iy1(*) then fos(*)...)
      call get(iy1,0)
      call get(fos,1)
      call get(def,2)
      call get(dch4,3)
      call get(dn2o,4)
      call get(dnox,5)
      call get(dvoc,6)
      call get(dco,7)
      call get(dso21,8)
      call get(dso22,9)
      call get(dso23,10)
      call get(dcf4,11)
      call get(dc2f6,12)
      call get(d125,13)
      call get(d134a,14)
      call get(d143a,15)
      call get(d227,16)
      call get(d245,17)
      call get(dsf6,18)

                                                                         
      return
      end subroutine InterfaceMagiccScm
