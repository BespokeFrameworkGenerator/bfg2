    subroutine LoaderIclipsIm()

    implicit none

! ***MKB: mods for f77
    
      real :: biomeChangeReal, temperatureReal = 0.675
      INTEGER maxarraysize
      PARAMETER (maxarraysize=100)
      INTEGER IY1(maxarraysize)

      CHARACTER*56 filename_ECHAM4
      INTEGER:: readunit_ECHAM4, fileflag

      readunit_ECHAM4 = 98
! Win:      filename_ECHAM4 = 'data\ecochange_prot.echam4_m.biogeo.out'
      filename_ECHAM4 = 'data/ecochange_prot.echam4_m.biogeo.out'

      write (*,*) 'LoaderIclipsIm: retrieving data'
      write(*,*) 'file=',filename_ECHAM4
      call get(temperatureReal,0)
      write(*,*)  'LoaderIclipsIm: received temp=',temperatureReal

      open(unit=readunit_ECHAM4, file=filename_ECHAM4, status='OLD',iostat=fileflag)
      if (fileflag.gt.0) then
         write(*,*) 'LoaderIclipsIm: error opening file ',filename_ECHAM4,' - aborting'
         stop 'file open error'
      else
         call ReadDataIclipsIm(readunit_ECHAM4, biomeChangeReal, temperatureReal)
         write(*,*) 'LoaderIclipsIm: Temperature: ', temperatureReal
         write(*,*) 'LoaderIclipsIm: Biome Change: ', biomeChangeReal
         close(readunit_ECHAM4)
      end if

      end subroutine LoaderIclipsIm

