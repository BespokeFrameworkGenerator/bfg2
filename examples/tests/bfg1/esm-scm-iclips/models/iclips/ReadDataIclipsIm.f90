      subroutine ReadDataIclipsIm(readfrom, biomeChangeReal, temperatureReal)
        implicit none

! ***MKB: mods for f77

      real aBIOMERealArray(10)

      real :: biomeChangeReal, temperatureReal
      real :: temperatureReal_sensitivity = 0.225/2, temperatureReal_upper, temperatureReal_lower
    
      integer:: max_file_length=100000, readfrom, i, j, col_of_data, ios, biomeChange_index=4, temp_index=2
      character (len=120) :: scratchline

      temperatureReal_upper = temperatureReal + temperatureReal_sensitivity
      temperatureReal_lower = temperatureReal - temperatureReal_sensitivity

! rewind to ensure we start from the beginning of the file
      rewind(readfrom)

!  Loop down to the data            
      do i=1, max_file_length                                                     
!         read(readfrom,'(a)',IOSTAT=ios), scratchline
         read(readfrom,4679,IOSTAT=ios,ERR=4680) (aBIOMERealArray(col_of_data), col_of_data=1,7)
!         print*, scratchline
        if (ios /= 0) exit

        if (aBIOMERealArray(temp_index) >= temperatureReal_lower) then
      if (aBIOMERealArray(temp_index) <= temperatureReal_upper) then
        biomeChangeReal = aBIOMERealArray(biomeChange_index)
      exit
      endif
        endif
4680  end do

  4679 FORMAT ((F5.3, 1X), (F5.3, 1X), (F4.0, 1X), (F10.6, 1X), (F10.6, 1X), (F4.0, 1X), (F4.0, 1X))

      return
      end subroutine ReadDataIclipsIm

