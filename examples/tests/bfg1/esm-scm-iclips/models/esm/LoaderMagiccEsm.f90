    subroutine LoaderMagiccEsm()

! ***MKB: mods for f77

! program/subroutine to act as wrapper around a database
! this particular case is for extracting only the relevant data from GAS.EMK in the format defined by the ESM interface

! (1) read in all the data
! (2) provide required 'put' as defined by interface

    implicit none
    
! declarations from magtar.for

! use parameter instead of explicit
      INTEGER maxarraysize
      PARAMETER (maxarraysize=100)
      INTEGER IY1(maxarraysize)

! use REAL
      REAL FOS, DEF, DCH4, DN2O, &
           DNOX, DVOC, DCO, &
           DSO21, DSO22, DSO23, DCF4, DC2F6, &
           D125, D134A, D143A, D227, D245, DSF6

      DIMENSION FOS(maxarraysize), DEF(maxarraysize), DCH4(maxarraysize), DN2O(maxarraysize), &
           DNOX(maxarraysize), DVOC(maxarraysize), DCO(maxarraysize), &
           DSO21(maxarraysize), DSO22(maxarraysize), DSO23(maxarraysize), DCF4(maxarraysize), DC2F6(maxarraysize), &
           D125(maxarraysize), D134A(maxarraysize), D143A(maxarraysize), D227(maxarraysize), D245(maxarraysize), DSF6(maxarraysize)

      REAL DSO2
      DIMENSION DSO2(maxarraysize)

      CHARACTER*20 mnem

! declarations determined by Michael
      INTEGER:: nval

! additional declarations used
      CHARACTER*56 filename
      INTEGER:: readunit, fileflag

      COMMON/MKB_DB_WRAPPER/nval,mnem,iy1,fos,def,dch4,dn2o,dnox,dvoc,dco,dso21,dso22,dso23,dcf4,dc2f6,d125, &
           d134a,d143a,d227,d245,dsf6

! define file to read from
      readunit = 99
! *** we should really be able to give relative filenames too
      filename = './GAS.EMK'
      open(unit=readunit, file=filename, status='OLD',iostat=fileflag)
      if (fileflag.gt.0) then
         write(*,*) 'LoaderMagiccEsm: error opening file ',filename,' - aborting'
         stop 'file open error'
      else
         call ReadGasEmkMagiccEsm(readunit)
         write(*,*) 'LoaderMagiccEsm: all data read'
         close(readunit)
      end if

!                                                                          
!  ADJUST SO2 EMISSIONS INPUT
! *** this was in the 'read' loop but now has to be explicit in the SCM module ***
!                                                                          
! --- MKB:       DSO21(I)= DSO21(I)+ES1990                                            
! --- MKB:       DSO22(I)= DSO22(I)+ES1990                                            
! --- MKB:       DSO23(I)= DSO23(I)+ES1990                                            
! --- MKB:       DSO2(I) = DSO21(I)+DSO22(I)+DSO23(I)-2.0*ES1990                      


! 'put' the data 
      call PutAllMagiccEsm()

!                                                                          
      end subroutine LoaderMagiccEsm
