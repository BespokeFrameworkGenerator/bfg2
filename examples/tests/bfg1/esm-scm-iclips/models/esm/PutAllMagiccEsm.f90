
      subroutine PutAllMagiccEsm()
        implicit none

! ***MKB: mods for f77

! use parameter instead of explicit
      INTEGER maxarraysize
      PARAMETER (maxarraysize=100)
      INTEGER IY1(maxarraysize)

! use REAL
      REAL FOS, DEF, DCH4, DN2O, &
           DNOX, DVOC, DCO, &
           DSO21, DSO22, DSO23, DCF4, DC2F6, &
           D125, D134A, D143A, D227, D245, DSF6

      DIMENSION FOS(maxarraysize), DEF(maxarraysize), DCH4(maxarraysize), DN2O(maxarraysize), &
           DNOX(maxarraysize), DVOC(maxarraysize), DCO(maxarraysize), &
           DSO21(maxarraysize), DSO22(maxarraysize), DSO23(maxarraysize), DCF4(maxarraysize), DC2F6(maxarraysize), &
           D125(maxarraysize), D134A(maxarraysize), D143A(maxarraysize), D227(maxarraysize), D245(maxarraysize), DSF6(maxarraysize)

      REAL DSO2
      DIMENSION DSO2(maxarraysize)

      CHARACTER*20 mnem

! declarations determined by Michael
      INTEGER:: nval

      COMMON/MKB_DB_WRAPPER/nval,mnem,iy1,fos,def,dch4,dn2o,dnox,dvoc,dco,dso21,dso22,dso23,dcf4,dc2f6,d125, &
           d134a,d143a,d227,d245,dsf6


      write (*,*) 'PutAllMagiccEsm: sending data'
      call put(nval,1000)
      call put(mnem,1001)

      call put(iy1,0)
      call put(fos,1)
      call put(def,2)
      call put(dch4,3)
      call put(dn2o,4)
      call put(dnox,5)
      call put(dvoc,6)
      call put(dco,7)
      call put(dso21,8)
      call put(dso22,9)
      call put(dso23,10)
      call put(dcf4,11)
      call put(dc2f6,12)
      call put(d125,13)
      call put(d134a,14)
      call put(d143a,15)
      call put(d227,16)
      call put(d245,17)
      call put(dsf6,18)

      return
      end subroutine PutAllMagiccEsm
