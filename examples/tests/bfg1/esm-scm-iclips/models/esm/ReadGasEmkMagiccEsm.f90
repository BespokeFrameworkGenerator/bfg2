
      subroutine ReadGasEmkMagiccEsm(readfrom)
        implicit none

! ***MKB: mods for f77

! use parameter instead of explicit
      INTEGER maxarraysize
      PARAMETER (maxarraysize=100)
      INTEGER IY1(maxarraysize)

! use REAL
      REAL FOS, DEF, DCH4, DN2O, &
           DNOX, DVOC, DCO, &
           DSO21, DSO22, DSO23, DCF4, DC2F6, &
           D125, D134A, D143A, D227, D245, DSF6

      DIMENSION FOS(maxarraysize), DEF(maxarraysize), DCH4(maxarraysize), DN2O(maxarraysize), &
           DNOX(maxarraysize), DVOC(maxarraysize), DCO(maxarraysize), &
           DSO21(maxarraysize), DSO22(maxarraysize), DSO23(maxarraysize), DCF4(maxarraysize), DC2F6(maxarraysize), &
           D125(maxarraysize), D134A(maxarraysize), D143A(maxarraysize), D227(maxarraysize), D245(maxarraysize), DSF6(maxarraysize)

      CHARACTER*20 mnem

! declarations determined by Michael
      INTEGER:: nval, readfrom, i

      COMMON/MKB_DB_WRAPPER/nval,mnem,iy1,fos,def,dch4,dn2o,dnox,dvoc,dco,dso21,dso22,dso23,dcf4,dc2f6,d125, &
           d134a,d143a,d227,d245,dsf6

! rewind to ensure we start from the beginning of the file
      rewind(readfrom)

!                                                                          
!  READ HEADER AND NUMBER OR ROWS OF EMISIONS DATA FROM GAS.EMK            
!                                                                          
      read(readfrom,4243)  NVAL                                                 
      read(readfrom,'(a)') mnem                                                 
      read(readfrom,*) !   skip description                                     
      read(readfrom,*) !   skip column headings                                 
      read(readfrom,*) !   skip units                                           
!                                                                          
!  READ INPUT EMISSIONS DATA FROM GAS.EMK                                  
!  NOTE THAT, IN CONTRAST TO STAG.FOR AND EARLIER VERSIONS OF MAGICC,      
!   THE SO2 EMISSIONS (BY REGION) MUST BE INPUT AS CHANGES FROM 1990.      
!                                                                          
      do i=1,NVAL                                                          
         read(readfrom,4242) IY1(I),FOS(I),DEF(I),DCH4(I),DN2O(I),               &
              DNOX(I),DVOC(I),DCO(I),                                            &
              DSO21(I),DSO22(I),DSO23(I),DCF4(I),DC2F6(I),D125(I),               &
              D134A(I),D143A(I),D227(I),D245(I),DSF6(I)                          
      end do

 4242 FORMAT (1X,I5,18F10.0)
 4243 FORMAT (I2)

      return
      end subroutine ReadGasEmkMagiccEsm

