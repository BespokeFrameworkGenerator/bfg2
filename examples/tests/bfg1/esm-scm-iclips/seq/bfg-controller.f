
      program sequential
c
cnc This is auto-generated code and should not be modified
cnc Created by f77sequentialgenerator.xsl
cnc Author: R. Ford (significantly helped by the existing
cnc                  f77mpigenerator.xsl by C. Armstrong)
c
      implicit none
c
      integer i
c
      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
c
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
c
      do i=1,1
c
       if (mod(i, 1).eq.0) then
         ModuleMagiccEsmflag=.true.
         call ModuleMagiccEsm()
         ModuleMagiccEsmflag=.false.
       end if
     
       if (mod(i, 1).eq.0) then
         ModuleMagiccScmflag=.true.
         call ModuleMagiccScm()
         ModuleMagiccScmflag=.false.
       end if
     
       if (mod(i, 1).eq.0) then
         ModuleIclipsImflag=.true.
         call ModuleIclipsIm()
         ModuleIclipsImflag=.false.
       end if
     
c
      end do
c
      end

      subroutine put(data,tag)
      implicit none
      real data
      integer tag
      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      print*,"put ",tag


      if (ModuleMagiccEsmflag) then
        
        if (tag.eq.1000) then
          call put(data,tag)
        end if
        
        if (tag.eq.1001) then
          call put(data,tag)
        end if
        
        if (tag.eq.0) then
          call put(data,tag)
        end if
        
        if (tag.eq.1) then
          call put(data,tag)
        end if
        
        if (tag.eq.2) then
          call put(data,tag)
        end if
        
        if (tag.eq.3) then
          call put(data,tag)
        end if
        
        if (tag.eq.4) then
          call put(data,tag)
        end if
        
        if (tag.eq.5) then
          call put(data,tag)
        end if
        
        if (tag.eq.6) then
          call put(data,tag)
        end if
        
        if (tag.eq.7) then
          call put(data,tag)
        end if
        
        if (tag.eq.8) then
          call put(data,tag)
        end if
        
        if (tag.eq.9) then
          call put(data,tag)
        end if
        
        if (tag.eq.10) then
          call put(data,tag)
        end if
        
        if (tag.eq.11) then
          call put(data,tag)
        end if
        
        if (tag.eq.12) then
          call put(data,tag)
        end if
        
        if (tag.eq.13) then
          call put(data,tag)
        end if
        
        if (tag.eq.14) then
          call put(data,tag)
        end if
        
        if (tag.eq.15) then
          call put(data,tag)
        end if
        
        if (tag.eq.16) then
          call put(data,tag)
        end if
        
        if (tag.eq.17) then
          call put(data,tag)
        end if
        
        if (tag.eq.18) then
          call put(data,tag)
        end if
        
      end if
  
      if (ModuleMagiccScmflag) then
        
        if (tag.eq.22) then
          call put(data,tag)
        end if
        
      end if
  
 
      end

      subroutine get(data,tag)
      implicit none
      real data
      integer tag
      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      print*,"get ",tag


      if (ModuleMagiccScmflag) then
        
        if (tag.eq.999) then
          call get(data,tag)
        end if
        
        if (tag.eq.998) then
          call get(data,tag)
        end if
        
        if (tag.eq.0) then
          call get(data,tag)
        end if
        
        if (tag.eq.1) then
          call get(data,tag)
        end if
        
        if (tag.eq.2) then
          call get(data,tag)
        end if
        
        if (tag.eq.3) then
          call get(data,tag)
        end if
        
        if (tag.eq.4) then
          call get(data,tag)
        end if
        
        if (tag.eq.5) then
          call get(data,tag)
        end if
        
        if (tag.eq.6) then
          call get(data,tag)
        end if
        
        if (tag.eq.7) then
          call get(data,tag)
        end if
        
        if (tag.eq.8) then
          call get(data,tag)
        end if
        
        if (tag.eq.9) then
          call get(data,tag)
        end if
        
        if (tag.eq.10) then
          call get(data,tag)
        end if
        
        if (tag.eq.11) then
          call get(data,tag)
        end if
        
        if (tag.eq.12) then
          call get(data,tag)
        end if
        
        if (tag.eq.13) then
          call get(data,tag)
        end if
        
        if (tag.eq.14) then
          call get(data,tag)
        end if
        
        if (tag.eq.15) then
          call get(data,tag)
        end if
        
        if (tag.eq.16) then
          call get(data,tag)
        end if
        
        if (tag.eq.17) then
          call get(data,tag)
        end if
        
        if (tag.eq.18) then
          call get(data,tag)
        end if
        
      end if
  
      if (ModuleIclipsImflag) then
        
        if (tag.eq.0) then
          call get(data,tag)
        end if
        
      end if
  
 
      end


      subroutine rput(data,tag)
      implicit none
      real data(*)
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      real rbuf(1801)
      common /cncrdata/ rbuf
      save /cncrdata/

      if (ModuleMagiccEsmflag) then
        
      end if
      if (ModuleMagiccScmflag) then
        
      end if

      end

      subroutine rget(data,tag)
      implicit none
      real data(*)
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      real rbuf(1801)
      common /cncrdata/ rbuf
      save /cncrdata/

      if (ModuleMagiccScmflag) then
        
      end if
      if (ModuleIclipsImflag) then
        
      end if

      end

      subroutine iput(data,tag)
      implicit none
      integer data(*)
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      integer ibuf(101)
      common /cncidata/ ibuf
      save /cncidata/

      if (ModuleMagiccEsmflag) then
        
      end if

      end

      subroutine iget(data,tag)
      implicit none
      integer data(*)
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      integer ibuf(101)
      common /cncidata/ ibuf
      save /cncidata/

      if (ModuleMagiccScmflag) then
        
      end if

      end

      subroutine sput(data,tag)
      implicit none
      character*(*) data
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      character*20 sbuf
      common /cncsdata/ sbuf
      save /cncsdata/

      if (ModuleMagiccEsmflag) then
        
      end if

      end

      subroutine sget(data,tag)
      implicit none
      character*(*) data
      integer tag
      integer i,offset

      logical 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      common /cnccontrol/ 
     +ModuleMagiccEsmflag
     +,ModuleMagiccScmflag
     +,ModuleIclipsImflag
      character*20 sbuf
      common /cncsdata/ sbuf
      save /cncsdata/

      if (ModuleMagiccScmflag) then
        
      end if

      end
