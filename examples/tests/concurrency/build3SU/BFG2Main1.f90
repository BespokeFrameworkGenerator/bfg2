       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_modOUT, only : put_modOUT=>put,&
get_modOUT=>get
       use BFG2InPlace_modINOUT1, only : put_modINOUT1=>put,&
get_modINOUT1=>get
       use BFG2InPlace_modINOUT2, only : put_modINOUT2=>put,&
get_modINOUT2=>get
       use BFG2InPlace_modIN, only : put_modIN=>put,&
get_modIN=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !modIN__freq
       integer :: modIN__freq
       !modINOUT1__freq
       integer :: modINOUT1__freq
       !modINOUT2__freq
       integer :: modINOUT2__freq
       !modOUT__freq
       integer :: modOUT__freq
       namelist /time/ nts1,modIN__freq,modINOUT1__freq,modINOUT2__freq,modOUT__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !ComplexVar
       complex :: r1
       !CharacterVar
       character :: r2
       !LogicalVar
       logical :: r3
       !IntegerVar
       integer :: r4
       !RealVar
       real :: r5
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       do its1=1,nts1
       if (modOUTThread()) then
       if(mod(its1,modOUT__freq).eq.0)then
       call setActiveModel(1)
       call modOUT_run_iteration(r5,r4,r3,r2,r1)
       call put_modOUT(r5,-5)
       call put_modOUT(r4,-4)
       call put_modOUT(r3,-3)
       call put_modOUT(r2,-2)
       call put_modOUT(r1,-1)
       end if
       end if
       if (modINOUT1Thread()) then
       if(mod(its1,modINOUT1__freq).eq.0)then
       call setActiveModel(2)
       call get_modINOUT1(r5,-5)
       call get_modINOUT1(r4,-4)
       call get_modINOUT1(r3,-3)
       call get_modINOUT1(r2,-2)
       call get_modINOUT1(r1,-1)
       call modINOUT1_run_iteration(r5,r4,r3,r2,r1)
       call put_modINOUT1(r5,-5)
       call put_modINOUT1(r4,-4)
       call put_modINOUT1(r3,-3)
       call put_modINOUT1(r2,-2)
       call put_modINOUT1(r1,-1)
       end if
       end if
       if (modINOUT2Thread()) then
       if(mod(its1,modINOUT2__freq).eq.0)then
       call setActiveModel(3)
       call get_modINOUT2(r1,-1)
       call get_modINOUT2(r2,-2)
       call get_modINOUT2(r3,-3)
       call get_modINOUT2(r4,-4)
       call get_modINOUT2(r5,-5)
       call modINOUT2_run_iteration(r1,r2,r3,r4,r5)
       call put_modINOUT2(r1,-1)
       call put_modINOUT2(r2,-2)
       call put_modINOUT2(r3,-3)
       call put_modINOUT2(r4,-4)
       call put_modINOUT2(r5,-5)
       end if
       end if
       if (modINThread()) then
       if(mod(its1,modIN__freq).eq.0)then
       call setActiveModel(4)
       call get_modIN(r1,-1)
       call get_modIN(r2,-2)
       call get_modIN(r3,-3)
       call get_modIN(r4,-4)
       call get_modIN(r5,-5)
       call modIN_run_iteration(r1,r2,r3,r4,r5)
       end if
       end if
       call commsSync()
       end do
       call finaliseComms()
       end program BFG2Main

