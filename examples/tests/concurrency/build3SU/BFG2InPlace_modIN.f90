       module BFG2InPlace_modIN
       use BFG2Target1
       use mpi
       private 
       public get,put
       interface get
       module procedure getcomplex__0,getcharacter__0,getlogical__0,getinteger__0,getreal__0
       end interface
       interface put
       end interface
       contains
       subroutine getcomplex__0(arg1,arg2)
       implicit none
       complex  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=modIN and ep=run and instance=
       if (arg2==-1) then
       ! I am modIN.run..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,2,3,4/)
       ins=(/0,0,0,1/)
       outs=(/1,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_complex,du,1,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_complex,du,1,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine getcomplex__0
       subroutine getcharacter__0(arg1,arg2)
       implicit none
       character  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=modIN and ep=run and instance=
       if (arg2==-2) then
       ! I am modIN.run..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,2,3,4/)
       ins=(/0,0,0,1/)
       outs=(/1,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=len(arg1)
       call mpi_send(arg1,mysize,mpi_character,du,2,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=len(arg1)
       call mpi_send(arg1,mysize,mpi_character,du,2,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine getcharacter__0
       subroutine getlogical__0(arg1,arg2)
       implicit none
       logical  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=modIN and ep=run and instance=
       if (arg2==-3) then
       ! I am modIN.run..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,2,3,4/)
       ins=(/0,0,0,1/)
       outs=(/1,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_logical,du,3,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_logical,du,3,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine getlogical__0
       subroutine getinteger__0(arg1,arg2)
       implicit none
       integer  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=modIN and ep=run and instance=
       if (arg2==-4) then
       ! I am modIN.run..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,2,3,4/)
       ins=(/0,0,0,1/)
       outs=(/1,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,4,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_integer,du,4,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine getinteger__0
       subroutine getreal__0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !outs
       integer, allocatable, dimension(:) :: outs
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==4) then
       ! I am model=modIN and ep=run and instance=
       if (arg2==-5) then
       ! I am modIN.run..4
       setSize=4
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       allocate(outs(1:setSize))
       set=(/1,2,3,4/)
       ins=(/0,0,0,1/)
       outs=(/1,0,0,0/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU.and.outs(current).ne.1) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,5,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       ins(its)=ins(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       mysize=1
       call mpi_send(arg1,mysize,mpi_real,du,5,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine getreal__0
       end module BFG2InPlace_modIN
