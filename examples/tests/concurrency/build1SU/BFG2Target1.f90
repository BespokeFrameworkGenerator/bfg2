       module BFG2Target1
       use modIN, only : modIN_run_iteration=>run
       use modINOUT1, only : modINOUT1_run_iteration=>run
       use modINOUT2, only : modINOUT2_run_iteration=>run
       use modOUT, only : modOUT_run_iteration=>run
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function modOUTThread()
       implicit none
       ! only one thread so always true
       modOUTThread=.true.
       end function modOUTThread
       logical function modINOUT1Thread()
       implicit none
       ! only one thread so always true
       modINOUT1Thread=.true.
       end function modINOUT1Thread
       logical function modINOUT2Thread()
       implicit none
       ! only one thread so always true
       modINOUT2Thread=.true.
       end function modINOUT2Thread
       logical function modINThread()
       implicit none
       ! only one thread so always true
       modINThread=.true.
       end function modINThread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
