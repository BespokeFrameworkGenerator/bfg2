       program BFG2Main
       use BFG2Target1
       use BFG2InPlace_goldstein, only : put_goldstein=>put,&
get_goldstein=>get
       ! Begin declaration of Control
       real :: t1,t2
       !nts1
       integer :: nts1
       namelist /time/ nts1
       ! ****End declaration of Control****
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !seaSurfaceTemps
       real, allocatable, dimension(:,:) :: r1
       !longitudesNumGOLDSTEIN
       integer :: r5
       !latitudesNumGOLDSTEIN
       integer :: r6
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           ! 1is allocatable; cannot be written to yet
           r5=72
           r6=72
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (goldsteinThread()) then
       call setActiveModel(1)
       call GOLDSTEIN_Initialise()
       end if
       call cpu_time(t1)
       do its1=1,nts1
       if (goldsteinThread()) then
       call setActiveModel(3)
       if (.not.(allocated(r1))) then
       allocate(r1(1:r5,1:r6))
       r1=273.0
       end if
       call GOLDSTEIN_Update(r1,r5,r6)
       call put_goldstein(r1,-1)
       end if
       end do
       call cpu_time(t2)
       print*,"cpu time=",t2-t1
       if (goldsteinThread()) then
       call setActiveModel(5)
       call GOLDSTEIN_DeInitialise()
       end if
       call finaliseComms()
       end program BFG2Main

