       module BFG2InPlace_igcm
       use BFG2Target2
       ! oasis4IncludeTarget
       use prism
       private 
       public get,put
       interface get
       module procedure getreal2
       end interface
       interface put
       end interface
       contains
       subroutine getreal2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/0/)
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=igcm and ep=IGCM_Initialise and instance=
       end if
       if (currentModel==4) then
       ! I am model=igcm and ep=IGCM_Update and instance=
       if (arg2==-1) then
       ! I am igcm.IGCM_Update..4
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/3,4/)
       myID=4
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p(1)=0
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU
       ! oasis4Receive
       ! The BFG ID string for this coupling field is: igcm_IGCM_Update_arg1
       coupling_field=>component%coupling_fields(igcm_IGCM_Update_arg1%coupling_field_no)
       call prism_get(coupling_field%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       end if
       end if
       if (currentModel==6) then
       ! I am model=igcm and ep=IGCM_DeInitialise and instance=
       end if
       end subroutine getreal2
       end module BFG2InPlace_igcm
