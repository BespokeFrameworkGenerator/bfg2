       module BFG2InPlace_goldstein
       use BFG2Target1
       ! oasis4IncludeTarget
       use prism
       private 
       public get,put
       interface get
       end interface
       interface put
       module procedure putreal2
       end interface
       contains
       subroutine putreal2(arg1,arg2)
       implicit none
       real , dimension(:,:) :: arg1
       integer  :: arg2
       ! oasis4DeclareSendRecvVars
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !ierror
       integer :: ierror
       !coupling_info
       integer :: coupling_info
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !i2
       integer :: i2
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=goldstein and ep=GOLDSTEIN_Initialise and instance=
       end if
       if (currentModel==3) then
       ! I am model=goldstein and ep=GOLDSTEIN_Update and instance=
       if (arg2==-1) then
       ! I am goldstein.GOLDSTEIN_Update..3
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       set=(/3,4/)
       ins=(/0,1/)
       myID=3
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: goldstein_GOLDSTEIN_Update_arg1
       coupling_field=>component%coupling_fields(goldstein_GOLDSTEIN_Update_arg1%coupling_field_no)
       call prism_put(coupling_field%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       newSize=setSize
       newSet=set
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
              do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       end do
              newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU
       ! oasis4Send
       ! The BFG ID string for this coupling field is: goldstein_GOLDSTEIN_Update_arg1
       coupling_field=>component%coupling_fields(goldstein_GOLDSTEIN_Update_arg1%coupling_field_no)
       call prism_put(coupling_field%prism_id,model_time,model_time_bounds,arg1,coupling_info,ierror)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
              end do
       end if
       end if
       if (currentModel==5) then
       ! I am model=goldstein and ep=GOLDSTEIN_DeInitialise and instance=
       end if
       end subroutine putreal2
       end module BFG2InPlace_goldstein
