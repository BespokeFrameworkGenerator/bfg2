       module BFG2Target2
       ! oasis4IncludeTarget
       use prism
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(2)
       integer :: b2mmap(2)
       ! Constant declarations
       !name_len_max
       integer, parameter :: name_len_max=256
       !ndim
       integer, parameter :: ndim=3
       ! Derived type definitions
       type shape_type
       !bounds
       integer, dimension(2,ndim) :: bounds
       end type shape_type
       type coords_type
       !longitudes
       real, allocatable, dimension(:) :: longitudes
       !latitudes
       real, allocatable, dimension(:) :: latitudes
       !verticals
       real, allocatable, dimension(:) :: verticals
       end type coords_type
       type corners_type
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type corners_type
       type points_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type points_type
       type grid_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !type_id
       integer :: type_id
       !valid_shape
       type(shape_type) :: valid_shape
       !corners
       type(corners_type) :: corners
       !point_sets
       type(points_type), dimension(1) :: point_sets
       end type grid_type
       type coupling_field_type
       !prism_id
       integer :: prism_id
       !bfg_id
       character(len=name_len_max) :: bfg_id
       !name
       character(len=name_len_max) :: name
       !nodims
       integer, dimension(2) :: nodims
       !actual_shape
       type(shape_type) :: actual_shape
       !type_id
       integer :: type_id
       end type coupling_field_type
       type component_type
       !name
       character(len=name_len_max) :: name
       !id
       integer :: id
       !local_comm
       integer :: local_comm
       !grid
       type(grid_type) :: grid
       !coupling_fields
       type(coupling_field_type), allocatable, dimension(:) :: coupling_fields
       end type component_type
       type coupling_field_key_type
       !coupling_field_no
       integer :: coupling_field_no
       end type coupling_field_key_type
       ! Variable declarations
       ! Coupling field keys for this deployment unit
       ! Model name: igcm
       ! Model instance:
       ! Entry point name: IGCM_Update
       ! Argument ID: 1
       ! Argument name: seaSurfaceTempsInput
       ! Argument type: real
       ! The BFG ID string for this coupling field is: igcm_IGCM_Update_arg1
       !igcm_IGCM_Update_arg1
       type(coupling_field_key_type) :: igcm_IGCM_Update_arg1
       !component
       type(component_type), target :: component
       !model_time
       type(PRISM_Time_Struct) :: model_time
       !model_time_bounds
       type(PRISM_Time_Struct), dimension(2) :: model_time_bounds
       ! Current rank, and all ranks for this deployment unit
       !my_local_rank
       integer :: my_local_rank
       !SUigcm_rank
       integer :: SUigcm_rank
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       type modelInfo
       !du
       integer :: du
       !period
       integer :: period
       !nesting
       integer :: nesting
       !bound
       integer :: bound
       !offset
       integer :: offset
       !its
       integer, pointer :: its
       end type modelInfo
       !info
       type(modelInfo), dimension(1:6) :: info
       !inf
       integer, parameter :: inf=32767
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function igcmThread()
       implicit none
       if (bfgSUID==2) then
       igcmThread=.true.
       else
       igcmThread=.false.
       end if
       end function igcmThread
       subroutine commsSync()
       implicit none
       ! oasis4CommsSync
       !ierror
       integer :: ierror
       call mpi_barrier(component%local_comm,ierror)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       use prism
       implicit none
       !RANK_UNKNOWN
       integer, parameter :: RANK_UNKNOWN=-1
       !ierror
       integer :: ierror
       !comp_loop
       integer :: comp_loop
       !points_loop
       integer :: points_loop
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !grid
       type(grid_type), pointer :: grid
       !points
       type(points_type), pointer :: points
       !corners
       type(corners_type), pointer :: corners
       !coord_array
       real, dimension(:), pointer :: coord_array
       !corners_longitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_longitudes_OASIS4
       !corners_latitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_latitudes_OASIS4
       !corners_verticals_OASIS4
       real, allocatable, dimension(:,:) :: corners_verticals_OASIS4
       !all_coords_array
       real, allocatable, dimension(:) :: all_coords_array
       !index_loop
       integer :: index_loop
       !coord_loop
       integer :: coord_loop
       !appl_name
       character(len=name_len_max) :: appl_name
       !local_comm
       integer :: local_comm
       !rank_lists
       integer, dimension(1,3) :: rank_lists
       ! Set a PRISM application name for this deployment unit
       appl_name='DUSUigcm'
       ! Initialise the coupling environment (must be called by each process)
       call prism_init(appl_name,ierror)
       ! Get the rank for this component's process
       call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       call MPI_Comm_rank(local_comm,my_local_rank,ierror)
       ! Reset all sequence unit ranks
       b2mmap=RANK_UNKNOWN
       ! Get the ranks for all component processes
       call prism_get_ranklists('SUigcm',1,rank_lists,ierror)
       SUigcm_rank=rank_lists(1,1)
       ! Store mapping from sequence unit number to local rank
       b2mmap(2)=SUigcm_rank
       ! Initialise the PRISM component
       if (my_local_rank==SUigcm_rank) then
       ! Assign sequence unit number (unique across deployment units)
       bfgSUID=2
       component%name='SUigcm'
       call prism_init_comp(component%id,component%name,ierror)
       call prism_get_localcomm(component%id,component%local_comm,ierror)
       ! Initialising grid for component SUigcm
       grid=>component%grid
       grid%name='IGCM_T21'
       grid%type_id=PRISM_reglonlatvrt
       grid%valid_shape%bounds(1,1)=1
       grid%valid_shape%bounds(2,1)=64
       grid%valid_shape%bounds(1,2)=1
       grid%valid_shape%bounds(2,2)=32
       grid%valid_shape%bounds(1,3)=1
       grid%valid_shape%bounds(2,3)=1
       call prism_def_grid(grid%id,grid%name,component%id,grid%valid_shape%bounds,grid%type_id,ierror)
       points=>grid%point_sets(1)
       corners=>grid%corners
       points%actual_shape=grid%valid_shape
       corners%actual_shape=grid%valid_shape
       allocate(points%coords%longitudes(1:64))
       allocate(corners%coords%longitudes(1:65))
       allocate(points%coords%latitudes(1:32))
       allocate(corners%coords%latitudes(1:33))
       ! Coordinates specified by start and increment values in gridspec
       ! Initialise longitudes for grid points
       coord_array=>points%coords%longitudes
       coord_array(1)=5.625
       coord_array(2)=11.25
       coord_array(3)=16.875
       coord_array(4)=22.5
       coord_array(5)=28.125
       coord_array(6)=33.75
       coord_array(7)=39.375
       coord_array(8)=45
       coord_array(9)=50.625
       coord_array(10)=56.25
       coord_array(11)=61.875
       coord_array(12)=67.5
       coord_array(13)=73.125
       coord_array(14)=78.75
       coord_array(15)=84.375
       coord_array(16)=90
       coord_array(17)=95.625
       coord_array(18)=101.25
       coord_array(19)=106.875
       coord_array(20)=112.5
       coord_array(21)=118.125
       coord_array(22)=123.75
       coord_array(23)=129.375
       coord_array(24)=135
       coord_array(25)=140.625
       coord_array(26)=146.25
       coord_array(27)=151.875
       coord_array(28)=157.5
       coord_array(29)=163.125
       coord_array(30)=168.75
       coord_array(31)=174.375
       coord_array(32)=180
       coord_array(33)=185.625
       coord_array(34)=191.25
       coord_array(35)=196.875
       coord_array(36)=202.5
       coord_array(37)=208.125
       coord_array(38)=213.75
       coord_array(39)=219.375
       coord_array(40)=225
       coord_array(41)=230.625
       coord_array(42)=236.25
       coord_array(43)=241.875
       coord_array(44)=247.5
       coord_array(45)=253.125
       coord_array(46)=258.75
       coord_array(47)=264.375
       coord_array(48)=270
       coord_array(49)=275.625
       coord_array(50)=281.25
       coord_array(51)=286.875
       coord_array(52)=292.5
       coord_array(53)=298.125
       coord_array(54)=303.75
       coord_array(55)=309.375
       coord_array(56)=315
       coord_array(57)=320.625
       coord_array(58)=326.25
       coord_array(59)=331.875
       coord_array(60)=337.5
       coord_array(61)=343.125
       coord_array(62)=348.75
       coord_array(63)=354.375
       coord_array(64)=360
       ! Initialise longitudes for grid corners
       coord_array=>corners%coords%longitudes
       coord_array(1)=2.8125
       coord_array(2)=8.4375
       coord_array(3)=14.0625
       coord_array(4)=19.6875
       coord_array(5)=25.3125
       coord_array(6)=30.9375
       coord_array(7)=36.5625
       coord_array(8)=42.1875
       coord_array(9)=47.8125
       coord_array(10)=53.4375
       coord_array(11)=59.0625
       coord_array(12)=64.6875
       coord_array(13)=70.3125
       coord_array(14)=75.9375
       coord_array(15)=81.5625
       coord_array(16)=87.1875
       coord_array(17)=92.8125
       coord_array(18)=98.4375
       coord_array(19)=104.0625
       coord_array(20)=109.6875
       coord_array(21)=115.3125
       coord_array(22)=120.9375
       coord_array(23)=126.5625
       coord_array(24)=132.1875
       coord_array(25)=137.8125
       coord_array(26)=143.4375
       coord_array(27)=149.0625
       coord_array(28)=154.6875
       coord_array(29)=160.3125
       coord_array(30)=165.9375
       coord_array(31)=171.5625
       coord_array(32)=177.1875
       coord_array(33)=182.8125
       coord_array(34)=188.4375
       coord_array(35)=194.0625
       coord_array(36)=199.6875
       coord_array(37)=205.3125
       coord_array(38)=210.9375
       coord_array(39)=216.5625
       coord_array(40)=222.1875
       coord_array(41)=227.8125
       coord_array(42)=233.4375
       coord_array(43)=239.0625
       coord_array(44)=244.6875
       coord_array(45)=250.3125
       coord_array(46)=255.9375
       coord_array(47)=261.5625
       coord_array(48)=267.1875
       coord_array(49)=272.8125
       coord_array(50)=278.4375
       coord_array(51)=284.0625
       coord_array(52)=289.6875
       coord_array(53)=295.3125
       coord_array(54)=300.9375
       coord_array(55)=306.5625
       coord_array(56)=312.1875
       coord_array(57)=317.8125
       coord_array(58)=323.4375
       coord_array(59)=329.0625
       coord_array(60)=334.6875
       coord_array(61)=340.3125
       coord_array(62)=345.9375
       coord_array(63)=351.5625
       coord_array(64)=357.1875
       coord_array(65)=362.8125
       ! Coordinates listed explicitly in gridspec
       ! Initialise latitudes for grid points
       coord_array=>points%coords%latitudes
       coord_array(1)=85.7605743408203125
       coord_array(2)=80.268768310546875
       coord_array(3)=74.74454498291015625
       coord_array(4)=69.21297454833984375
       coord_array(5)=63.6786346435546875
       coord_array(6)=58.142955780029296875
       coord_array(7)=52.6065216064453125
       coord_array(8)=47.06964111328125
       coord_array(9)=41.532459259033203125
       coord_array(10)=35.995075225830078125
       coord_array(11)=30.457550048828125
       coord_array(12)=24.91992950439453125
       coord_array(13)=19.3822307586669921875
       coord_array(14)=13.84448337554931640625
       coord_array(15)=8.30670261383056640625
       coord_array(16)=2.76890277862548828125
       coord_array(17)=-2.76890277862548828125
       coord_array(18)=-8.30670261383056640625
       coord_array(19)=-13.84448337554931640625
       coord_array(20)=-19.3822307586669921875
       coord_array(21)=-24.91992950439453125
       coord_array(22)=-30.457550048828125
       coord_array(23)=-35.995075225830078125
       coord_array(24)=-41.532459259033203125
       coord_array(25)=-47.06964111328125
       coord_array(26)=-52.6065216064453125
       coord_array(27)=-58.142955780029296875
       coord_array(28)=-63.6786346435546875
       coord_array(29)=-69.21297454833984375
       coord_array(30)=-74.74454498291015625
       coord_array(31)=-80.268768310546875
       coord_array(32)=-85.7605743408203125
       ! Initialise latitudes for grid corners
       coord_array=>corners%coords%latitudes
       coord_array(1)=90
       coord_array(2)=83.2076873779296875
       coord_array(3)=77.6092681884765625
       coord_array(4)=72.0479736328125
       coord_array(5)=66.4972686767578125
       coord_array(6)=60.951045989990234375
       coord_array(7)=55.407138824462890625
       coord_array(8)=49.864574432373046875
       coord_array(9)=44.3228607177734375
       coord_array(10)=38.781707763671875
       coord_array(11)=33.240947723388671875
       coord_array(12)=27.7004604339599609375
       coord_array(13)=22.1601715087890625
       coord_array(14)=16.6200199127197265625
       coord_array(15)=11.07996463775634765625
       coord_array(16)=5.539968013763427734375
       coord_array(17)=2.668042498044087551534175872802734375E-7
       coord_array(18)=-5.539967060089111328125
       coord_array(19)=-11.07996368408203125
       coord_array(20)=-16.6200199127197265625
       coord_array(21)=-22.1601715087890625
       coord_array(22)=-27.7004604339599609375
       coord_array(23)=-33.240947723388671875
       coord_array(24)=-38.781707763671875
       coord_array(25)=-44.3228607177734375
       coord_array(26)=-49.864574432373046875
       coord_array(27)=-55.407135009765625
       coord_array(28)=-60.95104217529296875
       coord_array(29)=-66.4972686767578125
       coord_array(30)=-72.0479736328125
       coord_array(31)=-77.60926055908203125
       coord_array(32)=-83.20767974853515625
       coord_array(33)=-89.99217987060546875
       allocate(points%coords%verticals(1:1))
       allocate(corners%coords%verticals(1:1))
       points%coords%verticals=0.0
       corners%coords%verticals=0.0
       points%name='IGCM_T21_point_set_1'
       call prism_set_points(points%id,points%name,grid%id,points%actual_shape%bounds,points%coords%longitudes,points%coords%latitudes,points%coords%verticals,.true.,ierror)
       ! Copy corners to OASIS4 format 2D array where
       ! leading/trailing corners are stored separately
       allocate(corners_longitudes_OASIS4(1:64,1:2))
       corners_longitudes_OASIS4( 1:64, 1)=corners%coords%longitudes( 1:64 )
       corners_longitudes_OASIS4( 1:64, 2)=corners%coords%longitudes( 2:( 64 + 1 ) )
       allocate(corners_latitudes_OASIS4(1:32,1:2))
       corners_latitudes_OASIS4( 1:32, 1)=corners%coords%latitudes( 1:32 )
       corners_latitudes_OASIS4( 1:32, 2)=corners%coords%latitudes( 2:( 32 + 1 ) )
       allocate(corners_verticals_OASIS4(1:1,1:2))
       corners_verticals_OASIS4=0.0
       call prism_set_corners(grid%id,8,corners%actual_shape%bounds,corners_longitudes_OASIS4,corners_latitudes_OASIS4,corners_verticals_OASIS4,ierror)
       if(allocated(corners_longitudes_OASIS4))deallocate(corners_longitudes_OASIS4)
       if(allocated(corners_latitudes_OASIS4))deallocate(corners_latitudes_OASIS4)
       if(allocated(corners_verticals_OASIS4))deallocate(corners_verticals_OASIS4)
       ! Initialising coupling fields for component SUigcm
       ! Model name: igcm
       ! Model instance:
       allocate(component%coupling_fields(1:1))
       ! Entry point name: IGCM_Update
       ! Argument ID: 1
       ! Argument name: seaSurfaceTempsInput
       ! Argument type: real
       ! The BFG ID string for this coupling field is: igcm_IGCM_Update_arg1
       igcm_IGCM_Update_arg1%coupling_field_no=1
       grid=>component%grid
       coupling_field=>component%coupling_fields(igcm_IGCM_Update_arg1%coupling_field_no)
       coupling_field%bfg_id='igcm_IGCM_Update_arg1'
       coupling_field%name='seaSurfaceTempsInput'
       coupling_field%type_id=PRISM_Real
       coupling_field%nodims(1)=ndim
       coupling_field%nodims(2)=0
       coupling_field%actual_shape=grid%valid_shape
       call prism_def_var(coupling_field%prism_id,coupling_field%bfg_id,grid%id,grid%point_sets(1)%id,PRISM_UNDEFINED,coupling_field%nodims,coupling_field%actual_shape%bounds,coupling_field%type_id,ierror)
       end if
       ! Initialisation phase is complete
       call prism_enddef(ierror)
       ! Set the date/time bounds within which coupling will be valid
       model_time=PRISM_jobstart_date
       model_time_bounds(1)=model_time
       model_time_bounds(2)=model_time
       call prism_calc_newdate(model_time_bounds(1),-3600.0,ierror)
       call prism_calc_newdate(model_time_bounds(2),3600.0,ierror)
       end subroutine initComms
       subroutine finaliseComms()
       use prism
       implicit none
       !ierror
       integer :: ierror
       call prism_terminate(ierror)
       if(allocated(component%coupling_fields))deallocate(component%coupling_fields)
       if(allocated(component%grid%point_sets(1)%coords%longitudes))deallocate(component%grid%point_sets(1)%coords%longitudes)
       if(allocated(component%grid%point_sets(1)%coords%latitudes))deallocate(component%grid%point_sets(1)%coords%latitudes)
       if(allocated(component%grid%point_sets(1)%coords%verticals))deallocate(component%grid%point_sets(1)%coords%verticals)
       if(allocated(component%grid%corners%coords%longitudes))deallocate(component%grid%corners%coords%longitudes)
       if(allocated(component%grid%corners%coords%latitudes))deallocate(component%grid%corners%coords%latitudes)
       if(allocated(component%grid%corners%coords%verticals))deallocate(component%grid%corners%coords%verticals)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=goldstein.GOLDSTEIN_Initialise.
       info(1)%du=b2mmap(1)
       info(1)%period=1
       info(1)%nesting=0
       info(1)%bound=1
       info(1)%offset=0
       nullify(info(1)%its)
       ! model.ep=igcm.IGCM_Initialise.
       info(2)%du=b2mmap(2)
       info(2)%period=1
       info(2)%nesting=0
       info(2)%bound=1
       info(2)%offset=0
       nullify(info(2)%its)
       ! model.ep=goldstein.GOLDSTEIN_Update.
       info(3)%du=b2mmap(1)
       info(3)%period=1
       info(3)%nesting=1
       info(3)%bound=24
       info(3)%offset=0
       info(3)%its=>its1
       ! model.ep=igcm.IGCM_Update.
       info(4)%du=b2mmap(2)
       info(4)%period=1
       info(4)%nesting=1
       info(4)%bound=24
       info(4)%offset=0
       info(4)%its=>its1
       ! model.ep=goldstein.GOLDSTEIN_DeInitialise.
       info(5)%du=b2mmap(1)
       info(5)%period=1
       info(5)%nesting=0
       info(5)%bound=1
       info(5)%offset=0
       nullify(info(5)%its)
       ! model.ep=igcm.IGCM_DeInitialise.
       info(6)%du=b2mmap(2)
       info(6)%period=1
       info(6)%nesting=0
       info(6)%bound=1
       info(6)%offset=0
       nullify(info(6)%its)
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !currentNesting
       integer :: currentNesting
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !targetpos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
              its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
              getNext=findNext(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestNext
       integer :: nestNext
       !currentMin
       integer :: currentMin
       !remainIters
       integer :: remainIters
       !waitIters
       integer :: waitIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1.or.its==0) then
       its=1
       end if
       if (list(pos).gt.point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
              saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestNext=findNext(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
              nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters.gt.0) then
       waitIters=info(list(pos))%period - mod(its,info(list(pos))%period)
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters.lt.currentMin.and.waitIters.le.remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
              end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       !newlist
       integer, allocatable, dimension(:) :: newlist
       !its
       integer, pointer :: its
       !i
       integer :: i
       !newlsize
       integer :: newlsize
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !pos
       integer :: pos
       !currentNesting
       integer :: currentNesting
       !targetpos
       integer :: targetpos
       !currentMin
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
              its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
              getLast=findLast(newlist,newlsize,point,targetpos)
       if(allocated(newlist))deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: point
       integer , intent(inout) :: pos
       !i
       integer :: i
       !j
       integer :: j
       !currentNesting
       integer :: currentNesting
       !previousIts
       integer, pointer :: previousIts
       !startPoint
       integer :: startPoint
       !endPoint
       integer :: endPoint
       !newlsize
       integer :: newlsize
       !nestLast
       integer :: nestLast
       !currentMin
       integer :: currentMin
       !elapsedIters
       integer :: elapsedIters
       !its
       integer :: its
       !newpos
       integer :: newpos
       !saveits
       integer :: saveits
       !newlist
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       if (associated(info(list(pos))%its)) then
       previousIts=>info(list(pos))%its
       end if
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       if (associated(info(list(pos))%its)) then
       its=info(list(pos))%its
       else
       its=1
       end if
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos).ge.point) then
       its=its - 1
       end if
       if (.not.(associated(info(list(pos))%its)).and.((info(point)%nesting==1.and.its1.gt.info(point)%period).or.(info(point)%nesting.gt.1.and.its1.gt.1))) then
       continue
       else if (info(list(pos))%nesting.gt.currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
              saveits=info(list(pos))%its
       if (info(list(pos))%its.gt.0) then
       info(list(pos))%its=info(list(pos))%bound+1
       end if
       nestLast=findLast(newlist,newlsize,point,newpos)
       info(list(pos))%its=saveits
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else if (associated(info(list(pos))%its).and..not.(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(1:newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
              nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       if(allocated(newlist))deallocate(newlist)
       else
       if (its.gt.0) then
       elapsedIters=mod(its,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters.lt.currentMin.and.elapsedIters.lt.its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
              end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       !i
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
              do i=pos+1,lsize
       if (info(list(i))%nesting.lt.info(list(pos))%nesting.and..not.(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
              getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findStartPoint=1
       return
       end if
       do i=pos-1,1,-1
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
              findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer , intent(in), dimension(*) :: list
       integer , intent(in) :: lsize
       integer , intent(in) :: pos
       integer, pointer  :: its
       integer , intent(in) :: nesting
       !i
       integer :: i
       if (.not.(associated(its))) then
       findEndPoint=lsize
       return
       end if
       do i=pos+1,lsize
       if (info(list(i))%nesting.le.nesting.and..not.(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
              findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target2
