       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_igcm, only : put_igcm=>put,&
get_igcm=>get
       ! Begin declaration of Control
       real :: t1,t2
       !nts1
       integer :: nts1
       namelist /time/ nts1
       ! ****End declaration of Control****
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !seaSurfaceTemps
       real, allocatable, dimension(:,:) :: r1
       !longitudesNumIGCM
       integer :: r2
       !latitudesNumIGCM
       integer :: r3
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           ! 1is allocatable; cannot be written to yet
           r2=64
           r3=32
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (igcmThread()) then
       call setActiveModel(2)
       call IGCM_Initialise()
       end if
       call cpu_time(t1)
       do its1=1,nts1
       if (igcmThread()) then
       call setActiveModel(4)
       if (.not.(allocated(r1))) then
       allocate(r1(1:r2,1:r3))
       r1=273.0
       end if
       call get_igcm(r1,-1)
       if (.not.(allocated(r1))) then
       allocate(r1(1:r2,1:r3))
       r1=273.0
       end if
       call IGCM_Update(r1,r2,r3)
       end if
       end do
       call cpu_time(t2)
       print*,"cpu time=",t2-t1
       if (igcmThread()) then
       call setActiveModel(6)
       call IGCM_DeInitialise()
       end if
       call finaliseComms()
       end program BFG2Main

