! SEAICE_OUT stub model.

module SEAICE_OUT

    use PRISM

    implicit none
    save
    private
        
    public :: SEAICE_OUT_Update
    
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
contains

    subroutine SEAICE_OUT_Update(seaSurfaceTempsOutput,longitudesNum,latitudesNum)
      integer, intent(in)  :: longitudesNum
      integer, intent(in)  :: latitudesNum
      real, intent(out)    :: seaSurfaceTempsOutput(longitudesNum,latitudesNum)
      integer              :: i, j

      ! Set it to 100 degrees celsius, just so it sticks out from the 
      ! GOLDSTEIN-generated ssts in the IGCM output.
      ! If we make this an "inout", we can +100 or something to all the temps
      ! coming from goldstein.
      seaSurfaceTempsOutput(:,:) = 373.0 

      ! DEBUG: print out all data received.  Little point in this - 
      ! the data (as it was put/get) can be output to a NetCDF file
      ! during the run by setting <debug_mode>true</debug_mode>
      ! for the transient in the SMIOC.
      !write( *, '(A)' ) 'SEAICE_OUT: Received data: '
      !write( *, '(A,2I3,F6.1)' ) (( 'Getting: ', i, j, seaSurfaceTempsOutput(i,j), &
      !    j=1,latitudesNum), i=1,longitudesNum )
    
    end subroutine SEAICE_OUT_Update

end module SEAICE_OUT

