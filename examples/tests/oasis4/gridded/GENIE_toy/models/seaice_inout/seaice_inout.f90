! SEAICE_INOUT stub model.

module SEAICE_INOUT

    use PRISM

    implicit none
    save
    private
        
    public :: SEAICE_INOUT_Update
    
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
contains

    subroutine SEAICE_INOUT_Update(seaSurfaceTempsInputOutput,longitudesNum,latitudesNum)
      integer, intent(in)  :: longitudesNum
      integer, intent(in)  :: latitudesNum
      real, intent(inout)  :: seaSurfaceTempsInputOutput(longitudesNum,latitudesNum)
      integer              :: i, j

      ! DEBUG: print out all data received.  Little point in this - 
      ! the data (as it was put/get) can be output to a NetCDF file
      ! during the run by setting <debug_mode>true</debug_mode>
      ! for the transient in the SMIOC.
      write( *, '(A)' ) 'SEAICE_INOUT: Received data: '
      write( *, '(A,2I3,F6.1)' ) (( 'Getting: ', i, j, seaSurfaceTempsInputOutput(i,j), &
          j=1,latitudesNum), i=1,longitudesNum )
    
      ! Add 100 degrees celsius, just so it sticks out from the 
      ! SSTs that have come direct from GOLDSTEIN in the IGCM output.
      seaSurfaceTempsInputOutput(:,:) = seaSurfaceTempsInputOutput(:,:) + 100.0

    end subroutine SEAICE_INOUT_Update

end module SEAICE_INOUT

