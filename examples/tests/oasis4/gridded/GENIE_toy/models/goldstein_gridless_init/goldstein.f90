! GOLDSTEIN stub model.  Just sets sea surface temperatures to fixed values
! according to latitude.

module GOLDSTEIN

    implicit none
    save
    private
        
    public :: GOLDSTEIN_Initialise, GOLDSTEIN_Update, GOLDSTEIN_DeInitialise 

    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double

    ! Hardcoded, but could be primed by BFG and sent to
    ! GOLDSTEIN_Initialise if we preferred, for example.
    integer, parameter :: longitudesNum = 64
    integer, parameter :: latitudesNum = 32
    
contains

    subroutine GOLDSTEIN_Initialise(gridDimensionsOutput)

      real, intent(out)    :: gridDimensionsOutput(2)

      gridDimensionsOutput(1) = longitudesNum
      gridDimensionsOutput(2) = latitudesNum

    end subroutine GOLDSTEIN_Initialise


    subroutine GOLDSTEIN_Update(seaSurfaceTempsOutput)
      real, intent(out)    :: seaSurfaceTempsOutput(longitudesNum,latitudesNum)
      integer              :: latLoop
      real(kind=dbl)       :: tempLow, tempHigh
      real(kind=dbl)       :: tempLatitudeStep, tempLatitude

      ! Temperature varying by latitude (could also vary over time)
      tempLow = 273.0
      tempHigh = 309.0
      tempLatitudeStep = ( tempHigh - tempLow ) / &
          real(latitudesNum / 2, kind(tempLatitudeStep)) 
      
      ! Starting at South pole (-90 degrees), move North increasing sea
      ! surface and ocean temperature until the equator, then decrease it
      tempLatitude = tempLow
      do latLoop = 1, latitudesNum
          seaSurfaceTempsOutput(:,latLoop)  = tempLatitude
          !seaSurfaceTempsOutput(:,latLoop,:)  = tempLatitude
          
          if ( latLoop < ( latitudesNum / 2 ) ) then
              ! Southern Hemisphere - increase temperature                
              tempLatitude = tempLatitude + tempLatitudeStep
          else if ( latLoop > ( latitudesNum / 2 ) ) then
              ! Northern Hemisphere - decrease temperature
              tempLatitude = tempLatitude - tempLatitudeStep
          else
              ! Equator - don't change temperature
          end if
      end do
    end subroutine GOLDSTEIN_Update

    
    subroutine GOLDSTEIN_DeInitialise

      ! Nothing to do currently
        
    end subroutine GOLDSTEIN_DeInitialise
    
end module GOLDSTEIN

