! GOLDSTEIN stub model.  Just sets sea surface temperatures to fixed values
! according to latitude.

module GOLDSTEIN

    implicit none
    save
    private
        
    public :: GOLDSTEIN_Initialise, GOLDSTEIN_Update, GOLDSTEIN_DeInitialise 
    public :: GOLDSTEIN_LatitudesInitialise 

    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
contains

    subroutine GOLDSTEIN_Initialise

       ! Nothing to do currently - could set initial values for
       ! seaSurfaceTempsOutput here rather than prime them using compose.xml though.
       ! TODO: Could seaSurfaceTempsOutput be a global variable within this
       ! module, rather than within BFG2Main?

    end subroutine GOLDSTEIN_Initialise


    subroutine GOLDSTEIN_LatitudesInitialise( &
        coordsCount, latitudeCoords )
    !subroutine GOLDSTEIN_LatitudesInitialise( &
    !    pointLatitudes, cornerLatitudes, latitudesCount )
        
        ! IRH: TODO: How to deal with "kind" when describing the interface
        ! to this function in the gridspec and processing it with BFG?
        real, intent(OUT), dimension(:) :: latitudeCoords
        !real(kind=dbl), intent(INOUT), dimension(:) :: pointLatitudes
        !real(kind=dbl), intent(INOUT), dimension(:) :: cornerLatitudes
        integer, intent(IN)                         :: coordsCount
        !integer, intent(IN)                         :: latitudesCount
        integer                                     :: errCode, latLoop
        integer                                     :: latitudesCount

        ! The following code was copied from subroutine "initialise_goldstein"
        ! in genie\genie-goldstein\src\fortran\initialise_goldstein.F
        
        real(kind=dbl)                              :: pi, th0, th1
        real(kind=dbl)                              :: s0, s1, dscon
        real(kind=dbl), allocatable, dimension(:)   :: s, sv

        ! GOLDSTEIN grid has N point coords and N+1 edge coords
        latitudesCount = ( coordsCount - 1 ) / 2 
        
        allocate( s( latitudesCount ) )
        !allocate( s( latitudesCount ), stat=errCode )
        !if ( errCode /= 0 ) &
        !    call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        ! N.B. sv array has an extra element, sv(0)
        allocate( sv( 0:latitudesCount ) )
        !allocate( sv( 0:latitudesCount ), stat=errCode )
        !if ( errCode /= 0 ) &
        !    call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        pi = 4*atan(1.0)
        th0 = - pi/2
        th1 = pi/2 
        s0 = sin(th0)
        s1 = sin(th1)
        dscon = (s1-s0)/latitudesCount
        sv(0) = s0
        
        do latLoop=1,latitudesCount
            sv(latLoop) = s0 + latLoop*dscon
            s(latLoop) = sv(latLoop) - 0.5*dscon

            ! Place the point coords in the first half of the output
            ! array and the edge/corner coords in the second half
            latitudeCoords(latLoop)=real(asin(s(latLoop))*180.0/pi, &
                kind(latitudeCoords))
            latitudeCoords(latLoop + latitudesCount)= &
                real(asin(sv(latLoop-1))*180.0/pi, kind(latitudeCoords))

            !pointLatitudes(latLoop)=real(asin(s(latLoop))*180.0/pi, &
            !    kind(pointLatitudes))
            !cornerLatitudes(latLoop)=real(asin(sv(latLoop-1))*180.0/pi, &
            !    kind(cornerLatitudes))            
        end do
        latitudeCoords(latitudesCount + latitudesCount + 1)=real( &
            asin(sv(latitudesCount))*180.0/pi,kind(latitudeCoords))
        !cornerLatitudes(latitudesCount + 1)=real( &
        !    asin(sv(latitudesCount))*180.0/pi,kind(cornerLatitudes))
            
        deallocate( s )
        !deallocate( s, stat=errCode )
        !if ( errCode /= 0 ) &
        !    call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        deallocate( sv )
        !deallocate( sv, stat=errCode )
        !if ( errCode /= 0 ) &
        !    call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )

    end subroutine GOLDSTEIN_LatitudesInitialise
    

    subroutine GOLDSTEIN_Update(seaSurfaceTempsOutput,longitudesNum,latitudesNum)
      integer, intent(in)  :: longitudesNum
      integer, intent(in)  :: latitudesNum
      real, intent(out)    :: seaSurfaceTempsOutput(longitudesNum,latitudesNum)
      integer              :: latLoop
      real(kind=dbl)       :: tempLow, tempHigh
      real(kind=dbl)       :: tempLatitudeStep, tempLatitude

      ! Temperature varying by latitude (could also vary over time)
      tempLow = 273.0
      tempHigh = 309.0
      tempLatitudeStep = ( tempHigh - tempLow ) / &
          real(latitudesNum / 2, kind(tempLatitudeStep)) 
      
      ! Starting at South pole (-90 degrees), move North increasing sea
      ! surface and ocean temperature until the equator, then decrease it
      tempLatitude = tempLow
      do latLoop = 1, latitudesNum
          seaSurfaceTempsOutput(:,latLoop)  = tempLatitude
          !seaSurfaceTempsOutput(:,latLoop,:)  = tempLatitude
          
          if ( latLoop < ( latitudesNum / 2 ) ) then
              ! Southern Hemisphere - increase temperature                
              tempLatitude = tempLatitude + tempLatitudeStep
          else if ( latLoop > ( latitudesNum / 2 ) ) then
              ! Northern Hemisphere - decrease temperature
              tempLatitude = tempLatitude - tempLatitudeStep
          else
              ! Equator - don't change temperature
          end if
      end do
    end subroutine GOLDSTEIN_Update

    
    subroutine GOLDSTEIN_DeInitialise

      ! Nothing to do currently
        
    end subroutine GOLDSTEIN_DeInitialise
    
end module GOLDSTEIN

