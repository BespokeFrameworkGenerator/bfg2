! Very basic trace functionality for Fortran programs
! Enable with fpp command line option '-DTRACE'
! Different trace levels (verbose, info, warn, error) could be added

#if defined(TRACE)
#   define SEPARATOR (": ")
#   define TRACE_WRITE( prefix, str ) write( *, * ) (prefix)//SEPARATOR//(str)
#else
#   define TRACE_WRITE( prefix, str )
#endif
