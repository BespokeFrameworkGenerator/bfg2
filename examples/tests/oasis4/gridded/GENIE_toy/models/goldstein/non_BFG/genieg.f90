! Prototype demonstrating initialisation and coupling for the GOLDSTEIN grid
! ihenderson, 11/08/06

! Control code for GOLDSTEIN model.
! When operating with OASIS, models are responsible for their own control
! (time-stepping) code.
! When operating with BFG, the control code can be created by BFG itself
! if desired

! Note: Renamed from "GOLDSTEINController" to GENIEG - the name of the
! model in the PRISM Standard Compiling Environment (ihenderson, 6/9/06)
program GENIEG

    use GOLDSTEIN

    implicit none
    
    integer :: timeStep, timeStepCount
    
    call GOLDSTEIN_Initialise()      
    
    ! Run GOLDSTEIN for 24 timesteps (= 24 hours)
    ! TODO: What are the timesteps in IGCM/GOLDSTEIN?
    ! (They don't have to be the same.)
    timeStepCount = 24
    do timeStep = 1, timeStepCount
    
        call GOLDSTEIN_Update()
    
    end do
    
    call GOLDSTEIN_DeInitialise()
     
end program GENIEG
