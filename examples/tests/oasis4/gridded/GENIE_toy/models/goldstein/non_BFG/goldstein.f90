! Prototype demonstrating initialisation and coupling for the GOLDSTEIN grid
! Two grids are set up: a 2D and a 3D grid.  Note that the 3D coupling carried
! out with the 3D grid doesn't make any sense - the units for the vertical 
! levels are different for GOLDSTEIN and IGCM (depth and sigma respectively).
! In GENIE I assume ocean biogeochemistry will use the same grid as GOLDSTEIN,
! and atmospheric chemistry will use the same grid as IGCM.  3D coupling will
! therefore be straightforward (no interpolation necessary).
! ihenderson, 11/08/06

! TODO:
! - Test use of a land/sea mask?
! - Fortran assert/error handling
! - Flag to check initialisation status

#include 'trace.h'

module GOLDSTEIN

    use PRISM

    implicit none
    save
    private
        
    public :: GOLDSTEIN_Initialise, GOLDSTEIN_Update, GOLDSTEIN_DeInitialise 
        
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
    character(len=*), parameter :: MODULE_NAME                  = 'GOLDSTEIN'
    character(len=*), parameter :: COMPONENT_NAME               = 'ocean'
    character(len=*), parameter :: COUPLING_3D_FIELD_NAME       = 'OCN_3DFIELD'
    character(len=*), parameter :: SEA_SURFACE_TEMP_FIELD_NAME  = 'OCN_SST'

    character(len=16)       :: m_appName = MODULE_NAME
    character(len=16)       :: m_componentName = COMPONENT_NAME
    
    integer                 :: m_componentID
    integer                 :: m_3DGridID
    integer                 :: m_2DGridID
    integer                 :: m_3DPointsID
    integer                 :: m_surfPointsID
    integer                 :: m_3DFieldID              ! 3D coupling field
    integer                 :: m_seaSurfTempFieldID     ! 2D coupling field
    Type(PRISM_Time_Struct) :: m_modelTime    
    
    ! Grid metadata.  This will be read in from the gridspec file eventually
    ! rather than hardcoded. 
    character(len=*), parameter :: GRID_3D_NAME         = 'GOLD_HIGH_3D'
    character(len=*), parameter :: GRID_2D_NAME         = 'GOLD_HIGH_2D'
    character(len=*), parameter :: POINTS_3D_NAME       = '3D_points'
    character(len=*), parameter :: POINTS_SURF_NAME     = 'surf_points'
    integer, parameter          :: DIMENSIONS_NUM       = 3
    integer, parameter          :: GRID_TYPE            = PRISM_reglonlatvrt
    integer, parameter          :: CORNERS_NUM          = 8
    integer, parameter          :: LONGITUDES_NUM       = 72
    integer, parameter          :: LATITUDES_NUM        = 72
    integer, parameter          :: LEVELS_NUM           = 16
    
    logical, parameter          :: NEW_POINTS_TRUE      = .true.
    logical, parameter          :: NEW_POINTS_FALSE     = .false.
    logical, parameter          :: PRISM_ERROR_TRUE     = .true.
    logical, parameter          :: PRISM_ERROR_FALSE    = .false.
    integer, parameter          :: MASK_ID_INVALID      = PRISM_UNDEFINED
    
    ! Grid "valid" shapes happens to coincide with the "actual" shapes.
    ! The valid area could be smaller though.  How is this used?
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DGridValidShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_2DGridValidShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,1 /)
                
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DPointsActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_surfPointsActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,1 /)
        
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DCornerActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_2DCornerActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    
    ! Field data
    real(kind=dbl), dimension(:,:,:), allocatable :: m_oceanTemps    

    ! SSTs are 2D - the third dimension is required by OASIS, 
    ! but will be of size 1 when the array is allocated.
    real(kind=dbl), dimension(:,:,:), allocatable :: m_seaSurfaceTemps
        
contains

    
    subroutine GOLDSTEIN_Initialise

        integer :: errCode
    
        TRACE_WRITE( MODULE_NAME, 'Initialising...' )
        
        call prism_init( m_appName, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! The component name must match that in the SMIOC
        call prism_init_comp( m_componentID, m_componentName, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        call GOLDSTEIN_GridsInitialise()
        
        call GOLDSTEIN_FieldsInitialise()
        
        ! Finished the definition phase
        call prism_enddef( errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! Set the start time for the model
        m_modelTime = PRISM_jobstart_date
        
        TRACE_WRITE( MODULE_NAME, 'Initialised.' )
        
    end subroutine GOLDSTEIN_Initialise

    
    subroutine GOLDSTEIN_GridsInitialise

        ! Grid points (defining the centre of each grid square)
        real(kind=dbl), dimension( LONGITUDES_NUM )     :: pointLongitudes
        real(kind=dbl), dimension( LATITUDES_NUM )      :: pointLatitudes
        real(kind=dbl), dimension( LEVELS_NUM )         :: pointLevels3D
        real(kind=dbl), dimension( 1 )                  :: pointLevels2D
   
        ! Grid corners (defining the edges of each grid square) as used by GENIE
        real(kind=dbl), dimension( LONGITUDES_NUM + 1 ) :: cornerLongitudes
        real(kind=dbl), dimension( LATITUDES_NUM + 1 )  :: cornerLatitudes
        real(kind=dbl), dimension( LEVELS_NUM + 1 )     :: cornerLevels
    
        ! Grid corners, arranged in the correct format for use by PSMILe/OASIS
        real(kind=dbl), dimension( LONGITUDES_NUM, 2 )  :: cornerLongitudesOASIS
        real(kind=dbl), dimension( LATITUDES_NUM, 2 )   :: cornerLatitudesOASIS
        real(kind=dbl), dimension( LEVELS_NUM, 2 )      :: cornerLevelsOASIS3D
        real(kind=dbl), dimension( 1, 2 )               :: cornerLevelsOASIS2D
       
        integer     :: errCode, longitudesLoop
        
        TRACE_WRITE( MODULE_NAME, 'Grids: initialising...' )
            
        ! The grid name must match that in the SMIOC
        call prism_def_grid( m_3DGridID, GRID_3D_NAME, m_componentID, &
            m_3DGridValidShape, GRID_TYPE, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )

        call prism_def_grid( m_2DGridID, GRID_2D_NAME, m_componentID, &
            m_2DGridValidShape, GRID_TYPE, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )


!#define LONGITUDES_WITHOUT_OFFSET
#ifdef LONGITUDES_WITHOUT_OFFSET
      
        ! Longitudes without -260 degree offset.  When passing a uniform SST
        ! from GOLDSTEIN to IGCM, e.g. 300K, the temperatures received by
        ! IGCM are correct (300-273=27 degs) at all longitudes apart from
        ! ~100 to ~180 degrees where the temperature received is 0 degrees.
        ! IGCM longitudes go from 0-360, whereas GOLDSTEIN longitudes go from
        ! -260 to 100 - the OASIS4 UG says this is ok, and the toy example
        ! has different starting points for longitudes between atmosphere
        ! and ocean too - I can't see why I'm getting the 0 degs temperature
        ! longitudes here though.  This problem doesn't occur if I vary SST
        ! with latitude, or if I remove the -260 offset, as in the code below.
        ! I am in contact with Sophie Valcke about this to try to resolve it.

        ! Calculate longitudes, as in GENIE-GOLDSTEIN
        do longitudesLoop=1, LONGITUDES_NUM
            pointLongitudes(longitudesLoop) = real( 360.0*(longitudesLoop-0.5) &
                / real(LONGITUDES_NUM), kind(pointLongitudes) )
            cornerLongitudes(longitudesLoop) = real( 360.0*(longitudesLoop-1.0) &
                / real(LONGITUDES_NUM), kind(cornerLongitudes) )
        end do
        cornerLongitudes(LONGITUDES_NUM + 1) = real( 360.0*(LONGITUDES_NUM) &
                / real(LONGITUDES_NUM), kind(cornerLongitudes) )
                
#else
                
        ! The following code was copied from subroutine "initialise_goldstein"
        ! in genie\genie-goldstein\src\fortran\initialise_goldstein.F

        ! Calculate longitudes, as in GENIE-GOLDSTEIN
        do longitudesLoop=1, LONGITUDES_NUM
            pointLongitudes(longitudesLoop) = real( 360.0*(longitudesLoop-0.5) &
                / real(LONGITUDES_NUM)-260.0, kind(pointLongitudes) )
            cornerLongitudes(longitudesLoop) = real( 360.0*(longitudesLoop-1.0) &
                / real(LONGITUDES_NUM)-260.0, kind(cornerLongitudes) )
        end do
        cornerLongitudes(LONGITUDES_NUM + 1) = real( 360.0*(LONGITUDES_NUM) &
                / real(LONGITUDES_NUM)-260.0, kind(cornerLongitudes) )                
                
#endif


        ! Calculate latitudes, as in GENIE-GOLDSTEIN
        call GOLDSTEIN_LatitudesInitialise( &
            pointLatitudes, cornerLatitudes, LATITUDES_NUM )
      
        ! Hard coded depth levels (36x36x8)
        !cornerLevels(1) = 0.0 
        !pointLevels(1) = 80.84071
        !cornerLevels(2) = 174.7519
        !pointLevels(2) = 283.8467
        !cornerLevels(3) = 410.5801
        !pointLevels(3) = 557.804
        !cornerLevels(4) = 728.8313
        !pointLevels(4) = 927.5105
        !cornerLevels(5) = 1158.312
        !pointLevels(5) = 1426.431
        !cornerLevels(6) = 1737.899
        !pointLevels(6) = 2099.725
        !cornerLevels(7) = 2520.053
        !pointLevels(7) = 3008.339
        !cornerLevels(8) = 3575.572
        !pointLevels(8) = 4234.517
        !cornerLevels(9) = 5000.0
        
        ! Calculate depth levels, as in GENIE-GOLDSTEIN
        call GOLDSTEIN_LevelsInitialise( pointLevels3D, cornerLevels, LEVELS_NUM )
        
        ! For the 2D grid, levels are meaningless, arbitrarily chosen 
        ! to be zero here and on the IGCM 2D grid.
        pointLevels2D(:) = 0.0
        
        ! Register 3D grid points with OASIS
        ! Note: can have several sets of points for the same grid. I think all
        ! sets of points must have the same shape (e.g. 72x72x16), but obviously
        ! they can have different values (e.g. offsets from the grid cell 
        ! centres - some fields may be located at the centres of grid cells, 
        ! others at the corners).
        call prism_set_points( m_3DPointsID, POINTS_3D_NAME, m_3DGridID, &
            m_3DPointsActualShape, pointLongitudes, pointLatitudes, &
            pointLevels3D, NEW_POINTS_TRUE, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! Register 2D grid points with OASIS that cover the area where
        ! the ocean meets the atmosphere (vertical level = 0.0 - no units)
        call prism_set_points( m_surfPointsID, POINTS_SURF_NAME, m_2DGridID, &
            m_surfPointsActualShape, pointLongitudes, pointLatitudes, &
            pointLevels2D, NEW_POINTS_TRUE, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! Instead of one array with points_num + 1 entries for the corners
        ! (as in native GENIE coupling), OASIS requires *two* arrays each
        ! with points_num entries, the first for the left/top edge of each
        ! grid cell, the second for the right/bottom edge of each grid cell.
        ! I.e. it's the same coordinates, but stored differently.
        ! I didn't realise this at first, hence the two arrays "corner..." and
        ! "corner...OASIS".
        
        cornerLongitudesOASIS( 1:LONGITUDES_NUM, 1 ) = &
            cornerLongitudes( 1:LONGITUDES_NUM )
        cornerLongitudesOASIS( 1:LONGITUDES_NUM, 2 ) = &
            cornerLongitudes( 2:(LONGITUDES_NUM + 1) )
            
        cornerLatitudesOASIS( 1:LATITUDES_NUM, 1 ) = &
            cornerLatitudes( 1:LATITUDES_NUM )
        cornerLatitudesOASIS( 1:LATITUDES_NUM, 2 ) = &
            cornerLatitudes( 2:(LATITUDES_NUM + 1 ) )
            
        cornerLevelsOASIS3D( 1:LEVELS_NUM, 1 ) = &
            cornerLevels( 1:LEVELS_NUM )
        cornerLevelsOASIS3D( 1:LEVELS_NUM, 2 ) = &
            cornerLevels( 2:(LEVELS_NUM + 1 ) )        
            
        ! For the 2D grid, corner levels are meaningless, arbitrarily chosen 
        ! to be zero here and on the IGCM 2D grid.
        cornerLevelsOASIS2D(:,:) = 0.0
        
        ! Register corners with OASIS
        call prism_set_corners( m_3DGridID, CORNERS_NUM, m_3DCornerActualShape, &
            cornerLongitudesOASIS, cornerLatitudesOASIS, cornerLevelsOASIS3D, &
            errCode )
        if ( errCode /= 0 ) & 
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )

        call prism_set_corners( m_2DGridID, CORNERS_NUM, m_2DCornerActualShape, &
            cornerLongitudesOASIS, cornerLatitudesOASIS, cornerLevelsOASIS2D, &
            errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        TRACE_WRITE( MODULE_NAME, 'Grids: initialised.' )
    
    end subroutine GOLDSTEIN_GridsInitialise

    
    subroutine GOLDSTEIN_LevelsInitialise( &
        pointLevels, cornerLevels, levelsCount )
        
        real(kind=dbl), intent(INOUT), dimension(:) :: pointLevels
        real(kind=dbl), intent(INOUT), dimension(:) :: cornerLevels
        integer, intent(IN)                         :: levelsCount
        integer                                     :: errCode, levLoop
     
        ! The following code was copied from subroutine "initialise_goldstein"
        ! in genie\genie-goldstein\src\fortran\initialise_goldstein.F
     
        real(kind=dbl) :: dsc
        real(kind=dbl) ez0, z1, tv1, tv2, tv3, tv4, tv5, zro(levelsCount), &
            zw(0:levelsCount), dz(levelsCount), dza(levelsCount)
            
        dsc = 5e3
        
        ! set up grid
        ! For variable (exponential) dz use ez0 > 0, else use ez0 < 0
        ez0 = 0.1
        z1 = ez0*((1.0 + 1/ez0)**(1.0/levelsCount) - 1.0)
        !print*,'z1',z1
        tv4 = ez0*((z1/ez0+1)**0.5-1)
        tv2 = 0
        tv1 = 0
        zro(levelsCount) = -tv4
        zw(levelsCount) = tv2
        do levLoop=1,levelsCount
            if(ez0.gt.0)then
                tv3 = ez0*((z1/ez0+1)**levLoop-1)
                dz(levelsCount-levLoop+1) = tv3 - tv2
                tv2 = tv3
                tv5 = ez0*((z1/ez0+1)**(levLoop+0.5)-1)
                if(levLoop.lt.levelsCount)dza(levelsCount-levLoop) = tv5 - tv4
                tv4 = tv5
                tv1 = tv1 + dz(levelsCount-levLoop+1)
                ! tv3 is the depth of the levLoopth w level from the top
                ! tv5 is the depth of the levLoop+1th density level from the top
            else
                dz(levLoop) = real(1d0/levelsCount)
                dza(levLoop) = real(1d0/levelsCount)
            endif
        enddo

        do levLoop=levelsCount,1,-1
            if(levLoop.gt.1)zro(levLoop-1) = zro(levLoop) - dza(levLoop-1)
            zw(levLoop-1) = zw(levLoop) - dz(levLoop)
        enddo

        do levLoop=1,levelsCount
            pointLevels(levLoop)= &
            real(abs(dsc*zro(levelsCount+1-levLoop)),kind(pointLevels))
        end do
        do levLoop=0,levelsCount
            cornerLevels(levLoop+1)= &
            real(abs(dsc*zw(levelsCount-levLoop)),kind(cornerLevels))
        end do
        
    end subroutine GOLDSTEIN_LevelsInitialise
    
    
    subroutine GOLDSTEIN_LatitudesInitialise( &
        pointLatitudes, cornerLatitudes, latitudesCount )
        
        real(kind=dbl), intent(INOUT), dimension(:) :: pointLatitudes
        real(kind=dbl), intent(INOUT), dimension(:) :: cornerLatitudes
        integer, intent(IN)                         :: latitudesCount
        integer                                     :: errCode, latLoop
        
        ! The following code was copied from subroutine "initialise_goldstein"
        ! in genie\genie-goldstein\src\fortran\initialise_goldstein.F
        
        real(kind=dbl)                              :: pi, th0, th1
        real(kind=dbl)                              :: s0, s1, dscon
        real(kind=dbl), allocatable, dimension(:)   :: s, sv
        
        allocate( s( latitudesCount ), stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        ! N.B. sv array has an extra element, sv(0)
        allocate( sv( 0:latitudesCount ), stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        pi = 4*atan(1.0)
        th0 = - pi/2
        th1 = pi/2 
        s0 = sin(th0)
        s1 = sin(th1)
        dscon = (s1-s0)/latitudesCount
        sv(0) = s0
        
        do latLoop=1,latitudesCount
            sv(latLoop) = s0 + latLoop*dscon
            s(latLoop) = sv(latLoop) - 0.5*dscon
            pointLatitudes(latLoop)=real(asin(s(latLoop))*180.0/pi, &
                kind(pointLatitudes))
            cornerLatitudes(latLoop)=real(asin(sv(latLoop-1))*180.0/pi, &
                kind(cornerLatitudes))            
        end do
        cornerLatitudes(latitudesCount + 1)=real( &
            asin(sv(latitudesCount))*180.0/pi,kind(cornerLatitudes))
            
        deallocate( s, stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
        
        deallocate( sv, stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )

    end subroutine GOLDSTEIN_LatitudesInitialise
    
    
    subroutine GOLDSTEIN_FieldsInitialise
    
        integer                 :: errCode, latLoop
        real(kind=dbl)          :: tempLow, tempHigh
        real(kind=dbl)          :: tempLatitudeStep, tempLatitude
        integer, dimension(2)   :: var_nodims        
        
        TRACE_WRITE( MODULE_NAME, 'Fields: initialising...' )
        
        ! Both fields have 3 dimensions, although the SST
        ! field is effectively 2D - it only has one vertical level
        ! TODO: Should be using a land/sea mask - currently just
        ! pretending that we have an aquaplanet
        var_nodims(1) = 3
        var_nodims(2) = 0
        
        ! Define the 3D transient (note: no mask)
        call prism_def_var( m_3DFieldID, COUPLING_3D_FIELD_NAME, &
            m_3DGridID, m_3DPointsID, MASK_ID_INVALID, var_nodims, &
            m_3DPointsActualShape, PRISM_Double_Precision, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )

        ! Define the SST transient (note: no mask)
        call prism_def_var( m_seaSurfTempFieldID, SEA_SURFACE_TEMP_FIELD_NAME, &
            m_2DGridID, m_surfPointsID, MASK_ID_INVALID, var_nodims, &
            m_surfPointsActualShape, PRISM_Double_Precision, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )

        allocate( m_oceanTemps( LONGITUDES_NUM, LATITUDES_NUM, &
            LEVELS_NUM ), stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )      

        allocate( m_seaSurfaceTemps( LONGITUDES_NUM, LATITUDES_NUM, 1 ), &
            stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )        
        
        ! Temperatures in Kelvin
        
        ! Temperature varying by latitude
        tempLow = 273.0
        tempHigh = 309.0
        tempLatitudeStep = ( tempHigh - tempLow ) / &
            real(LATITUDES_NUM / 2, kind(tempLatitudeStep)) 
        
        ! Starting at South pole (-90 degrees), move North increasing sea
        ! surface and ocean temperature until the equator, then decrease it
        tempLatitude = tempLow
        do latLoop = 1, LATITUDES_NUM
            m_oceanTemps(:, latLoop,:)      = tempLatitude
            !m_seaSurfaceTemps(:,latLoop,:)  = tempLatitude
            
            if ( latLoop < ( LATITUDES_NUM / 2 ) ) then
                ! Southern Hemisphere - increase temperature                
                tempLatitude = tempLatitude + tempLatitudeStep
            else if ( latLoop > ( LATITUDES_NUM / 2 ) ) then
                ! Northern Hemisphere - decrease temperature
                tempLatitude = tempLatitude - tempLatitudeStep
            else
                ! Equator - don't change temperature
            end if
        end do
        
        ! Constant global SST
        ! When passing a uniform SST
        ! from GOLDSTEIN to IGCM, e.g. 300K, the temperatures received by
        ! IGCM are correct (300-273=27 degs) at all longitudes apart from
        ! ~100 to ~180 degrees where the temperature received is 0 degrees.  
        ! IGCM longitudes go from 0-360, whereas GOLDSTEIN longitudes go from
        ! -260 to 100 - the OASIS4 UG says this is ok, and the toy example
        ! has different offsets/starting points for longitudes between
        ! atmosphere and ocean too - I can't see why I'm getting the 0 degrees
        ! longitudes here though.  This problem doesn't occur if I vary SST
        ! with latitude, or if I remove the -260 offset, as below.
        ! I am in contact with Sophie Valcke about this to try to resolve it.
        m_seaSurfaceTemps(:,:,:) = 300.0
        
        TRACE_WRITE( MODULE_NAME, 'Fields: initialised.' )
    
    end subroutine GOLDSTEIN_FieldsInitialise
    
    
    subroutine GOLDSTEIN_Update

        integer                                 :: errCode, info
        Type(PRISM_Time_Struct), dimension( 2 ) :: modelTimeBounds
        integer                                 :: i, j, k        
    
        TRACE_WRITE( MODULE_NAME, 'Updating...' )

        ! Increment the time in the model (+1 hour)
        call prism_calc_newdate( m_modelTime, 3600, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! Set the date bounds to +/- 0.5 hours
        modelTimeBounds(1) = m_modelTime
        modelTimeBounds(2) = m_modelTime
        call prism_calc_newdate( modelTimeBounds(1), -1800.0, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        call prism_calc_newdate( modelTimeBounds(2),  1800.0, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        ! DEBUG: print out all data sent.  Little point in this - 
        ! the data (as it was put/get) can be output to a NetCDF file
        ! during the run by setting <debug_mode>true</debug_mode>
        ! for the transient in the SMIOC.
        !write( *, '(A)' ) 'GOLDSTEIN: Putting data: '
        !write( *, '(A,3I3,F6.1)' ) ((( 'Putting: ', &
        !   i, j, k, m_oceanTemps(i,j,k), k=1,7 ), j=1,32), i=1,64 )

        ! Send the 3D field data from the ocean, valid within the given
        ! bounds around the given time
        call prism_put( m_3DFieldID, m_modelTime, &
            modelTimeBounds, m_oceanTemps, info, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )

        ! Send the sea surface temp data from the ocean, valid within the given
        ! bounds around the given time
        call prism_put( m_seaSurfTempFieldID, m_modelTime, &
            modelTimeBounds, m_seaSurfaceTemps, info, errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        TRACE_WRITE( MODULE_NAME, 'Updated.' )
    
    end subroutine GOLDSTEIN_Update
    
    
    subroutine GOLDSTEIN_DeInitialise

        integer :: errCode
        
        TRACE_WRITE( MODULE_NAME, 'Deinitialising...' )
        
        call prism_terminate( errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_TRUE )
        
        call GOLDSTEIN_FieldsDeInitialise()
        
        TRACE_WRITE( MODULE_NAME, 'Deinitialised.' )
    
    end subroutine GOLDSTEIN_DeInitialise
    
    
    subroutine GOLDSTEIN_FieldsDeInitialise
    
        integer :: errCode
    
        deallocate( m_oceanTemps, stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )

        deallocate( m_seaSurfaceTemps, stat=errCode )
        if ( errCode /= 0 ) &
            call GOLDSTEIN_Error( __LINE__, errCode, PRISM_ERROR_FALSE )
    
    end subroutine GOLDSTEIN_FieldsDeInitialise

    
    subroutine GOLDSTEIN_Error( lineNum, errCode, isPrismError )
    
        integer, intent(IN)     :: lineNum, errCode
        logical, intent(IN)     :: isPrismError
        
        ! 256 chars should be long enough, judging by prism_error.F90
        character(len=256)      :: errorDesc
        character(len=256)      :: errorLocation
        
        TRACE_WRITE( MODULE_NAME, 'Aborting following error.' )
        
        ! Get error string
        if ( isPrismError ) then
            call prism_error( errCode, errorDesc )
        else
            write( errorDesc, '(A,I)' ) &
                'Non-PRISM error code was: ', errCode
        end if
        
        ! Get error location
        write( errorLocation, '(I)' ) lineNum
        errorLocation = __FILE__//':'//errorLocation
        
        ! Abort and record error information
        ! (Will this work if prism_terminate has already been called?
        ! I expect so, but haven't checked.)
        call prism_abort ( m_componentID, errorLocation, errorDesc )
    
    end subroutine GOLDSTEIN_Error
    
end module GOLDSTEIN

