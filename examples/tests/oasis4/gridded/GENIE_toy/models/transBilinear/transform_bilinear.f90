      ! A generic transformation
      module transform_bilinear

        implicit none
        
        public :: run
        private:: transform_bilinear_2D_int
        private:: transform_bilinear_2D_real

        interface run
          module procedure transform_bilinear_2D_int, transform_bilinear_2D_real
        end interface

      contains

        subroutine transform_bilinear_2D_int( coupling_field_IN, coupling_field_OUT )
          implicit none

          integer, intent(in)  :: coupling_field_IN( :, : )
          integer, intent(out)  :: coupling_field_OUT( :, : )

          !stop "Do the transform_bilinear_2D_int"

        end subroutine

        subroutine transform_bilinear_2D_real( coupling_field_IN, coupling_field_OUT )
          implicit none

          real, intent(in)  :: coupling_field_IN( :, : )
          real, intent(out)  :: coupling_field_OUT( :, : )

          !stop "Do the transform_bilinear_2D_real"

        end subroutine

      end module transform_bilinear
