! SEAICE_IN stub model.

module SEAICE_IN

    use PRISM

    implicit none
    save
    private
        
    public :: SEAICE_IN_Update
    
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
contains

    subroutine SEAICE_IN_Update(seaSurfaceTempsInput,longitudesNum,latitudesNum)
      integer, intent(in)  :: longitudesNum
      integer, intent(in)  :: latitudesNum
      real, intent(in)     :: seaSurfaceTempsInput(longitudesNum,latitudesNum)
      integer              :: i, j

      ! DEBUG: print out all data received.  Little point in this - 
      ! the data (as it was put/get) can be output to a NetCDF file
      ! during the run by setting <debug_mode>true</debug_mode>
      ! for the transient in the SMIOC.
      write( *, '(A)' ) 'SEAICE_IN: Received data: '
      write( *, '(A,2I3,F6.1)' ) (( 'Getting: ', i, j, seaSurfaceTempsInput(i,j), &
          j=1,latitudesNum), i=1,longitudesNum )
    
    end subroutine SEAICE_IN_Update

end module SEAICE_IN

