      subroutine transform_temperature( temperatures, longitudesNum, latitudesNum )
        implicit none

        integer, intent(in)  :: longitudesNum
        integer, intent(in)  :: latitudesNum
        real, intent(inout)  :: temperatures( longitudesNum, latitudesNum )

        temperatures(:,:) = temperatures(:,:) - 273
      end subroutine
