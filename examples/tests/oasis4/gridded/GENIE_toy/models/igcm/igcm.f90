! IGCM stub model.

module IGCM

    !use PRISM

    implicit none
    save
    private
        
    public :: IGCM_Initialise, IGCM_Update, IGCM_DeInitialise 
    
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
contains

    subroutine IGCM_Initialise

      ! Nothing to do currently
        
    end subroutine IGCM_Initialise

    
    subroutine IGCM_Update(seaSurfaceTempsInput,longitudesNum,latitudesNum)
      integer, intent(in)  :: longitudesNum
      integer, intent(in)  :: latitudesNum
      real, intent(in)     :: seaSurfaceTempsInput(longitudesNum,latitudesNum)
      integer              :: i, j

      ! DEBUG: print out all data received.  Little point in this - 
      ! the data (as it was put/get) can be output to a NetCDF file
      ! during the run by setting <debug_mode>true</debug_mode>
      ! for the transient in the SMIOC.
      write( *, '(A)' ) 'IGCM: Received data: '
      write( *, '(A,2I3,F6.1)' ) (( 'Getting: ', i, j, seaSurfaceTempsInput(i,j), &
          j=1,latitudesNum), i=1,longitudesNum )
    
    end subroutine IGCM_Update
    
    
    subroutine IGCM_DeInitialise

      ! Nothing to do currently
    
    end subroutine IGCM_DeInitialise

end module IGCM

