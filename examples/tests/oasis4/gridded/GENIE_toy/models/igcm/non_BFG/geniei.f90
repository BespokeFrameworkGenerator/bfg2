! Prototype demonstrating initialisation and coupling for the IGCM grid
! ihenderson, 11/08/06

! Control code for IGCM model.
! When operating with OASIS, models are responsible for their own control
! (time-stepping) code.
! When operating with BFG, the control code can be created by BFG itself
! if desired

! Note: Previously called "IGCMController"; now GENIEI - this is the name
! of the program in the PRISM Standard Compiling Environment (ihenderson, 6/9/06)
program GENIEI

    use IGCM

    implicit none
    
    integer :: timeStep, timeStepCount
    
    call IGCM_Initialise()
    
    ! Run IGCM for 24 timesteps (= 24 hours)
    ! TODO: What are the timesteps in IGCM/GOLDSTEIN?
    ! (They don't have to be the same.)
    timeStepCount = 24
    do timeStep = 1, timeStepCount
    
        call IGCM_Update()
    
    end do
    
    call IGCM_DeInitialise()
     
end program GENIEI
