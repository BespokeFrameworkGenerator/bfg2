! Prototype demonstrating initialisation and coupling for the IGCM grid
! Two grids are set up: a 2D and a 3D grid.  Note that the 3D coupling carried
! out with the 3D grid doesn't make any sense - the units for the vertical levels
! are diferent for GOLDSTEIN and IGCM (depth and sigma respectively).  In GENIE
! I assume ocean biogeochemistry will use the same grid as GOLDSTEIN, and 
! atmospheric chemistry will use the same grid as IGCM.  3D coupling will
! therefore be straightforward (no interpolation necessary).
! ihenderson, 11/08/06

! TODO:
! - Test use of a land/sea mask?
! - Fortran assert/error handling
! - Flag to check initialisation status

#include 'trace.h'

module IGCM

    use PRISM

    implicit none
    save
    private
        
    public :: IGCM_Initialise, IGCM_Update, IGCM_DeInitialise 
    
    integer, parameter :: dbl = SELECTED_REAL_KIND(12,307) ! double
    
    character(len=*), parameter :: MODULE_NAME              = 'IGCM'
    character(len=*), parameter :: COMPONENT_NAME           = 'atmos'
    character(len=*), parameter :: COUPLING_3D_FIELD_NAME   = 'ATM_3DFIELD'
    character(len=*), parameter :: SURFACE_TEMP_FIELD_NAME  = 'ATM_SURFTEMP'

    character(len=16)       :: m_appName        = MODULE_NAME
    character(len=16)       :: m_componentName  = COMPONENT_NAME
    
    integer                 :: m_componentID
    integer                 :: m_3DGridID
    integer                 :: m_2DGridID
    integer                 :: m_3DPointsID
    integer                 :: m_surfPointsID
    integer                 :: m_3DFieldID          ! 3D coupling field
    integer                 :: m_surfTempFieldID    ! 2D coupling field
    Type(PRISM_Time_Struct) :: m_modelTime
    
    ! Grid metadata.  This will be read in from the gridspec file eventually
    ! rather than hardcoded. 
    character(len=*), parameter :: GRID_3D_NAME         = 'IGCM_T21_3D'
    character(len=*), parameter :: GRID_2D_NAME         = 'IGCM_T21_2D'
    character(len=*), parameter :: POINTS_3D_NAME       = '3D_points'
    character(len=*), parameter :: POINTS_SURF_NAME     = 'surf_points'
    integer, parameter          :: DIMENSIONS_NUM       = 3
    ! N.B. PRISM_reglonlat_sigmavrt not supported in OASIS4.0.2 release
    !integer, parameter          :: GRID_TYPE           = PRISM_reglonlat_sigmavrt
    integer, parameter          :: GRID_TYPE            = PRISM_reglonlatvrt
    integer, parameter          :: CORNERS_NUM          = 8
    integer, parameter          :: LONGITUDES_NUM       = 64
    integer, parameter          :: LATITUDES_NUM        = 32
    integer, parameter          :: LEVELS_NUM           = 7
    logical, parameter          :: NEW_POINTS_TRUE      = .true.
    logical, parameter          :: NEW_POINTS_FALSE     = .false.
    integer, parameter          :: MASK_ID_INVALID      = PRISM_UNDEFINED
    
    ! Grid "valid" shapes happens to coincide with the "actual" shapes.
    ! The valid area could be smaller though.  How is this used?  Halo regions?
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DGridValidShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_2DGridValidShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,1 /)
        
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DPointsActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_surfPointsActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,1 /)
        
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_3DCornerActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,LEVELS_NUM /)   
    integer, dimension( 2, DIMENSIONS_NUM ) :: m_2DCornerActualShape = &
        (/ 1,LONGITUDES_NUM, 1,LATITUDES_NUM, 1,1 /)
    
contains

    subroutine IGCM_Initialise

        integer :: errCode
    
        TRACE_WRITE( MODULE_NAME, 'Initialising...' )
        
        call prism_init( m_appName, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! The component name must match that in the SMIOC
        call prism_init_comp( m_componentID, m_componentName, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        call IGCM_GridsInitialise()
        
        call IGCM_FieldsInitialise()
        
        ! Finished the definition phase
        call prism_enddef( errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! Set the start time for the model
        m_modelTime = PRISM_jobstart_date
        
        TRACE_WRITE( MODULE_NAME, 'Initialised.' )
        
    end subroutine IGCM_Initialise

    
    subroutine IGCM_GridsInitialise

        ! Grid points (defining the centre of each grid square)
        real(kind=dbl), dimension( LONGITUDES_NUM )     :: pointLongitudes
        real(kind=dbl), dimension( LATITUDES_NUM )      :: pointLatitudes
        real(kind=dbl), dimension( LEVELS_NUM )         :: pointLevels3D
        real(kind=dbl), dimension( 1 )                  :: pointLevels2D
   
        ! Grid corners (defining the edges of each grid square) as used by GENIE
        ! N.B. Subroutine gwtbox() expects an array of reals as argument, so
        ! cornerLatitudes are declared as "real", not "real(kind=dbl)".
        real(kind=dbl), dimension( LONGITUDES_NUM + 1 ) :: cornerLongitudes
        real,           dimension( LATITUDES_NUM + 1 )  :: cornerLatitudes
        real(kind=dbl), dimension( LEVELS_NUM + 1)      :: cornerLevels
        
        ! Grid corners, arranged in the correct format for use by PSMILe/OASIS        
        real(kind=dbl), dimension( LONGITUDES_NUM, 2 )  :: cornerLongitudesOASIS
        real(kind=dbl), dimension( LATITUDES_NUM, 2 )   :: cornerLatitudesOASIS
        real(kind=dbl), dimension( LEVELS_NUM, 2 )      :: cornerLevelsOASIS3D
        real(kind=dbl), dimension( 1, 2 )               :: cornerLevelsOASIS2D        
    
        integer         :: errCode, longitudesLoop, levelsLoop
        real(kind=dbl)  :: spacing
       
        
#ifdef SWAP_LATITUDES
        integer         :: latitudesLoop
        real(kind=dbl)  :: latitudeSwap
#endif        
        
        TRACE_WRITE( MODULE_NAME, 'Grids: initialising...' )
            
        ! The grid name must match that in the SMIOC
        call prism_def_grid( m_3DGridID, GRID_3D_NAME, m_componentID, &
            m_3DGridValidShape, GRID_TYPE, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )

        call prism_def_grid( m_2DGridID, GRID_2D_NAME, m_componentID, &
            m_2DGridValidShape, GRID_TYPE, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )

        ! Vertical (sigma) levels, as in GENIE-IGCM
        ! See psigma in genie\genie-igcm3\src\fortran\igcm3_adiab.F
        ! for points; corners I've 'assumed' from the points (I might
        ! have got them from the genie source at some point, but if so
        ! I can't find where...)
        spacing = 1.0 / real(LEVELS_NUM)
        do levelsLoop = 1, LEVELS_NUM
            cornerLevels( levelsLoop ) = ( real(levelsLoop) - 1.0 ) * spacing
            pointLevels3D( levelsLoop ) = ( real(levelsLoop) - 0.5 ) * spacing
        end do
        cornerLevels( LEVELS_NUM + 1 ) = 1.0
        
        ! For the 2D grid, levels are meaningless, arbitrarily chosen 
        ! to be zero here and on the GOLDSTEIN 2D grid.
        pointLevels2D(:) = 0.0

        ! The following code was copied from subroutine "INITIALISE_ATMOS"
        ! in genie\genie-igcm3\src\fortran\initialise_atmos.F
        ! "gwtcnr.f" and "gwtbox.f" can be found in genie\genie-lib\libutil1\
        
        ! Calculate longitudes, as in GENIE-IGCM
        spacing=360.0/real(LONGITUDES_NUM)
        do longitudesLoop=1,LONGITUDES_NUM
            
            ! Following line is as in GENIE, but this results in points not
            ! corresponding to corners - e.g. pointLongitudes(1) is 0.0, but
            ! cornerLongitude(1) and (2) are (2.81.., 8.43...).  Evidently
            ! this is not a problem in native GENIE, and I'm not sure it's a 
            ! problem with OASIS either, but it's inconsistent so I've changed 
            ! it so that pointLongitudes(1) = 5.625 rather than 0.0.
            !pointLongitudes(longitudesLoop)=(real(longitudesLoop)-1.0)*spacing
            pointLongitudes(longitudesLoop) = &
                ( real(longitudesLoop) * spacing ) !- 80.0
            
            cornerLongitudes(longitudesLoop)= &
                ( ( real(longitudesLoop) - 0.5 ) * spacing ) !- 80.0
        end do
        cornerLongitudes(LONGITUDES_NUM+1)= &
            ( ( real(LONGITUDES_NUM) + 0.5 ) * spacing ) !- 80.0
    
        ! Calculate latitudes using functions defined in gwtbox.f and gwtcnr.f
        ! (these functions are used for calculating the latitudes in GENIE-IGCM) 
        ! For the prototype, these functions are compiled as part of a separate
        ! library (common_genie), also under the PRISM SCE.
        !
        ! N.B. Subroutine gwtbox expects an array of reals as argument, so
        ! cornerLatitudes are declared as "real", not "real(kind=dbl)".
        call gwtcnr8( pointLatitudes, LATITUDES_NUM / 2 )
        call gwtbox( cornerLatitudes, LATITUDES_NUM / 2 )
        
        
#ifdef SWAP_LATITUDES
        ! TEST: Now reverse the arrays so that negative latitudes come first
        do latitudesLoop = 1, LATITUDES_NUM / 2
            latitudeSwap = pointLatitudes( latitudesLoop )

            pointLatitudes( latitudesLoop ) &
                = pointLatitudes( LATITUDES_NUM - latitudesLoop + 1 )
            pointLatitudes( LATITUDES_NUM - latitudesLoop + 1 ) &
                = latitudeSwap

            latitudeSwap = cornerLatitudes( latitudesLoop )

            ! Note: there are LATITUDES_NUM+1 corners - the array has
            ! an odd number of entries, so the middle entry isn't swapped.
            cornerLatitudes( latitudesLoop ) &
                = cornerLatitudes( LATITUDES_NUM - latitudesLoop + 2 )
            cornerLatitudes( LATITUDES_NUM - latitudesLoop + 2 ) &
                = latitudeSwap
        end do
#endif        
        
        ! Register 3D grid points with OASIS
        call prism_set_points( m_3DPointsID, POINTS_3D_NAME, m_3DGridID, &
            m_3DPointsActualShape, pointLongitudes, pointLatitudes, &
            pointLevels3D, NEW_POINTS_TRUE, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
                
        ! Register 2D grid points with OASIS (vertical level = 0.0 - no units)
        call prism_set_points( m_surfPointsID, POINTS_SURF_NAME, m_2DGridID, &
            m_surfPointsActualShape, pointLongitudes, pointLatitudes, &
            pointLevels2D, NEW_POINTS_TRUE, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! Instead of one array with points_num + 1 entries for the corners
        ! (as in native GENIE coupling), OASIS requires *two* arrays each
        ! with points_num entries, the first for the left/top edge of each
        ! grid cell, the second for the right/bottom edge of each grid cell.
        ! I.e. the same coordinates, but stored differently.
        ! I didn't realise this at first, hence the two arrays "corner..." and
        ! "corner...OASIS".
        
        cornerLongitudesOASIS( 1:LONGITUDES_NUM, 1 ) = &
            cornerLongitudes( 1:LONGITUDES_NUM )
        cornerLongitudesOASIS( 1:LONGITUDES_NUM, 2 ) = &
            cornerLongitudes( 2:(LONGITUDES_NUM + 1) )
            
        cornerLatitudesOASIS( 1:LATITUDES_NUM, 1 ) = &
            cornerLatitudes( 1:LATITUDES_NUM )
        cornerLatitudesOASIS( 1:LATITUDES_NUM, 2 ) = &
            cornerLatitudes( 2:(LATITUDES_NUM + 1 ) )
            
        cornerLevelsOASIS3D( 1:LEVELS_NUM, 1 ) = &
            cornerLevels( 1:LEVELS_NUM )
        cornerLevelsOASIS3D( 1:LEVELS_NUM, 2 ) = &
            cornerLevels( 2:(LEVELS_NUM + 1 ) )
            
        ! For the 2D grid, corner levels are meaningless, arbitrarily chosen 
        ! to be zero here and on the GOLDSTEIN 2D grid.
        cornerLevelsOASIS2D(:,:) = 0.0
            
        ! Register corners with OASIS
        call prism_set_corners( m_3DGridID, CORNERS_NUM, m_3DCornerActualShape, &
            cornerLongitudesOASIS, cornerLatitudesOASIS, cornerLevelsOASIS3D, &
            errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )

        call prism_set_corners( m_2DGridID, CORNERS_NUM, m_2DCornerActualShape, &
            cornerLongitudesOASIS, cornerLatitudesOASIS, cornerLevelsOASIS2D, &
            errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        TRACE_WRITE( MODULE_NAME, 'Grids: initialised.' )
    
    end subroutine IGCM_GridsInitialise


    subroutine IGCM_FieldsInitialise
    
        integer                 :: errCode
        integer, dimension(2)   :: var_nodims        
        
        TRACE_WRITE( MODULE_NAME, 'Fields: initialising...' )               
        
        ! Both fields have 3 dimensions, although the surface temperature
        ! field is effectively 2D - it only has one vertical level
        var_nodims(1) = 3
        var_nodims(2) = 0
        
        ! Define the 3D transient (note: no mask)
        call prism_def_var( m_3DFieldID, COUPLING_3D_FIELD_NAME, &
            m_3DGridID, m_3DPointsID, MASK_ID_INVALID, var_nodims, &
            m_3DPointsActualShape, PRISM_Double_Precision, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )

        ! Define the surface temperature transient (note: no mask)
        call prism_def_var( m_surfTempFieldID, SURFACE_TEMP_FIELD_NAME, &
            m_2DGridID, m_surfPointsID, MASK_ID_INVALID, var_nodims, &
            m_surfPointsActualShape, PRISM_Double_Precision, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode ) 
        
        TRACE_WRITE( MODULE_NAME, 'Fields: initialised.' )
    
    end subroutine IGCM_FieldsInitialise
    
    
    subroutine IGCM_Update

        integer                                 :: errCode, info
        Type(PRISM_Time_Struct), dimension( 2 ) :: modelTimeBounds
        integer                                 :: i, j, k

        ! The 3D field to get
        real(kind=dbl), dimension( LONGITUDES_NUM, LATITUDES_NUM, LEVELS_NUM ) &
            :: atmosTemps
        
        ! The surface temperature field to get
        real(kind=dbl), dimension( LONGITUDES_NUM, LATITUDES_NUM, 1 ) &
            :: surfaceTemperatures 
    
        TRACE_WRITE( MODULE_NAME, 'Updating...' )

        ! Increment the time in the model (+1 hour)
        call prism_calc_newdate( m_modelTime, 3600, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! Set the date bounds to +/- 0.5 hours
        modelTimeBounds(1) = m_modelTime
        modelTimeBounds(2) = m_modelTime
        call prism_calc_newdate( modelTimeBounds(1), -1800.0, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        call prism_calc_newdate( modelTimeBounds(2),  1800.0, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! Get the 3D field data from the ocean, 
        ! valid within the given bounds around the given time
        call prism_get( m_3DFieldID, m_modelTime, & 
            modelTimeBounds, atmosTemps, info, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )

        ! Get the surface temperature data from the ocean (sst), 
        ! valid within the given bounds around the given time
        call prism_get( m_surfTempFieldID, m_modelTime, & 
            modelTimeBounds, surfaceTemperatures, info, errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        ! DEBUG: print out all data received.  Little point in this - 
        ! the data (as it was put/get) can be output to a NetCDF file
        ! during the run by setting <debug_mode>true</debug_mode>
        ! for the transient in the SMIOC.
        !write( *, '(A)' ) 'IGCM: Getting data: '
        !write( *, '(A,3I3,F6.1)' ) ((( 'Getting: ', &
        !    i, j, k, atmosTemps(i,j,k), k=1,LEVELS_NUM ), &
        !    j=1,LATITUDES_NUM), i=1,LONGITUDES_NUM )

        !write( *, '(A)' ) 'IGCM: Getting data: '
        !write( *, '(A,2I3,F6.1)' ) (( 'Getting: ', &
        !    i, j, surfaceTemperatures(i,j,1), &
        !    j=1,LATITUDES_NUM), i=1,LONGITUDES_NUM )
        
        TRACE_WRITE( MODULE_NAME, 'Updated.' )
    
    end subroutine IGCM_Update
    
    
    subroutine IGCM_DeInitialise

        integer :: errCode
        
        TRACE_WRITE( MODULE_NAME, 'Deinitialising...' )
        
        call prism_terminate( errCode )
        if ( errCode /= 0 ) call IGCM_Error( __LINE__, errCode )
        
        TRACE_WRITE( MODULE_NAME, 'Deinitialised.' )
    
    end subroutine IGCM_DeInitialise

    
    subroutine IGCM_Error( lineNum, errCode )
    
        integer, intent(IN)     :: lineNum, errCode
        
        ! 256 chars should be long enough, judging by prism_error.F90
        character(len=256)      :: errorDesc
        character(len=256)      :: errorLocation
        
        TRACE_WRITE( MODULE_NAME, 'Aborting following error.' )
        
        ! Get error string
        call prism_error( errCode, errorDesc )
        
        ! Get error location
        write( errorLocation, '(I)' ) lineNum
        errorLocation = __FILE__//':'//errorLocation
        
        ! Abort and record error information
        call prism_abort ( m_componentID, errorLocation, errorDesc )
    
    end subroutine IGCM_Error
      
end module IGCM



