GENIE_toy consists of the following example configurations based on the same set
of models.  More detailed information on each example can be found in the README
for that example, which can be found in the subdirectory for the example.  All
examples have models with different grids, hence intrinsic transformations are
used to represent the interpolation from one grid to another, EXCEPT for eg4
where grids are identical and thus no transformation is required.

eg1: one output transient to one input transient, different DUs
eg2: one output transient to one input transient, same DU

eg3: one output transient to one input transient, different DUs, SAME grids 
     In addition to eg4, the dimensions of the grid are sent from one model to
     the other as a 2-element 1D array during initialisation, mimicking the
     GENIE configuration ig_fi_sl.  This example therefore tests components
     with >1 grid - a gridless grid is used to send the 1D array during
     initialisation, and a gridded grid is used for the coupling.

eg4: one output transient to one input transient, different DUs, SAME grids

eg5: one output transient to two input transients, same DU - hangs when run with
     OASIS4, looks like PRISM bug.  I reported this to Sophie.
eg6: one output transient to two input transients, different DUs
eg7: two output transients to one input transient, different DUs

eg8: inout transient with 1 input, 1 output, different DUs
eg9: inout transient with 1 input, 2 outputs +
     inout transient with 2 inputs, 1 output, different DUs
eg10: inout with 2 inputs and 2 outputs, different DUs

eg10 doesn't quite work ATM, and won't until the "offset" attribute for models
in the schedule has been fully implemented - there's code in Main/2/Control.xsl
for adding offsets (as <nmldata> elements) to the namelist, but they don't seem
to be processed when it comes to writing BFG2Control.nam.  The info()%offset
variable also needs to be set in BFG2Target (it's currently always 0), and
getLast/Next() amended to use the offset in their calculations.  I've assumed
that the offset implementation is "work in progress" (I see it's being used in
the timestep loops in BFG2Main now), so have left that to you to complete.  eg10
gives the results I expect when run with OASIS4 though, considering that the
offsets aren't fully implemented. 

IRH: Tue Dec 18 12:32:05 GMT 2007.  I think eg10 should work now - Chris had 
this work in hand, and I think he's committed it.  Should test this.
