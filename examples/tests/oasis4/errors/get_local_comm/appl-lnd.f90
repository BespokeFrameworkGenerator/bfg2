  program appl_lnd

  use PRISM
  use MPI
  implicit none

  character(len=128) :: appl_name = 'lnd'
  character(len=128) :: comp_name
  integer :: comp_id
  integer :: ierror
  integer :: ranklist(1,3) ! assume only one rank per comp
  integer :: localComm
  integer :: myrank
  integer :: lndRank
  integer :: n
!
  call prism_init(appl_name,ierror)
!
  call prism_get_localcomm (PRISM_Appl_id, localComm, ierror)
!
  call MPI_Comm_Rank ( localComm, myrank, ierror )
!
  write (*,*), "Application ",trim(appl_name)," has local rank ",myrank
!
  call prism_get_ranklists('lnd',1,ranklist,ierror)
  lndrank=ranklist(1,1)
!
  if (myrank.eq.lndrank) then
    comp_name='lnd'
  else
    write (*,*) "Error, expecting myrank=",myrank," to equal lndrank=",lndrank
  end if
!
  call prism_init_comp (comp_id, comp_name, ierror )
!
  write (*,*) 'I am component ', trim(comp_name), ' in application ', trim(appl_name)
!
  call prism_terminate ( ierror )
!
  end program appl_lnd
