  program appl_ocn

  use PRISM
  implicit none

  character(len=128) :: appl_name = 'ocn'
  character(len=128) :: comp_name = 'ocn'
  integer :: comp_id
  integer :: ierror
!
  call prism_init_comp (comp_id, comp_name, ierror )

  write ( * , * ) 'I am component ', trim(comp_name), ' in application ', trim(appl_name)

  call prism_terminate ( ierror )

  end program appl_ocn