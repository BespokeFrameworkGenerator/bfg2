  program appl_atmocn

  use PRISM
  implicit none

  character(len=128) :: appl_name = 'atmocn'
  character(len=128) :: comp_name
  integer :: comp_id
  integer :: ierror
  integer :: ranklist(1,3) ! assume only one rank per comp
  integer :: localComm
  integer :: myrank
  integer :: atmRank, ocnRank
!
  call prism_init(appl_name,ierror)
!
  call prism_get_localcomm (PRISM_Appl_id, localComm, ierror)
!
  call MPI_Comm_Rank ( localComm, myrank, ierror )
!
  call prism_get_ranklists('atm',1,ranklist,ierror)
  atmrank=ranklist(1,1)
  call prism_get_ranklists('ocn',1,ranklist,ierror)
  ocnrank=ranklist(1,1)

  if (myrank.eq.atmrank) then
    comp_name='atm'
  else if (myrank.eq.ocnrank) then
    comp_name='ocn'
  end if
!
  call prism_init_comp (comp_id, comp_name, ierror )
!
  write (*,*) 'I am component ', trim(comp_name), ' in application ', trim(appl_name)
!
  call prism_terminate ( ierror )
!
  end program appl_atmocn
