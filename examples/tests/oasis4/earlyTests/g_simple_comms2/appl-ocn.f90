  program appl_ocn

  use PRISM
  implicit none
!
! MPI stuff
  include 'mpif.h'
  integer :: mype, npes
  integer :: PRISM_Comp
  integer :: localComm
!
  character(len=128) :: appl_name = 'ocn'
  character(len=128) :: comp_name = 'ocn'
  integer :: comp_id
  integer :: ierror
  integer :: info
  Type(PRISM_Time_Struct) :: model_time
  Type(PRISM_Time_Struct) :: model_time_bounds(2)
  Double Precision :: dincr1, dincr2
!
  integer, parameter :: is = 1
  integer            :: ie = 2
  integer, parameter :: js = 1
  integer            :: je = 2
  integer, parameter :: ks = 1
  integer, parameter :: ke = 1
!
! Define size of local problem
!
  integer :: is_pe
  integer :: ie_pe
  integer :: js_pe
  integer :: je_pe
!
  integer   :: var_id
  character(len=132) :: var_name,grid_name,point_name
  integer :: grid_type
  integer :: grid_id
  integer :: offset_array(1,3),extent_array(1,3)
  integer :: method_id
  integer :: var_nodims(2)
  integer :: var_type
  integer :: i,j,k
  integer :: valid_shape(2,3)
  integer :: point_id
  logical :: flag
!
! coupling data
  real, allocatable :: g_wind(:,:,:)
  real, allocatable :: test_var(:,:,:)
  integer :: val ! used to init coupling array
!
! other locals
!
  integer :: npes_i, npes_j, my_blk_i, my_blk_j
  integer :: rest
  integer :: i_blk, j_blk, k_blk
!
! INIT
!
  call prism_init_comp (comp_id, comp_name, ierror )

  write (*,*) 'I am component ',trim(comp_name), &
              ' in application ',trim(appl_name)
!
! Get local communicator
!
  call prism_get_localcomm ( comp_id, localComm, ierror )
!
! Get MPI sizes
!
  call MPI_Comm_Size ( localComm, npes, ierror )
  call MPI_Comm_Rank ( localComm, mype, ierror )
!
  print *,'mype = ', mype
!
  call prism_initialized (flag, ierror)
  if (.not. flag) then
     write (*, *) trim(comp_name), "PRISM is NOT initialized"
  endif
!
! Determine processors for i and j dim
! npes_i and npes_j are determined to be
! as close together as possible
! npes_j >= npes_i 
!
  npes_i = 1
  npes_j = npes
  do j = npes/2 + 1, 2, -1
     rest = mod(npes,j)
     if ( rest == 0 ) npes_j = j
     npes_i = npes/npes_j
     if ( npes_i >= npes_j ) exit
  enddo
  print *,'npes_i, npes_j', npes_i, npes_j
!
! Determine process local size of the problem assuming 2-d partitioning
! (bit more complexity required to deal with halo - see simple-mg in
!  prism distribution)
!
  my_blk_i = mod(mype,npes_i) + 1
  my_blk_j = mype/npes_i + 1
  print *,'my_blk_i, my_blk_j', my_blk_i, my_blk_j

  i_blk = (ie-is+1)/npes_i
  j_blk = (je-js+1)/npes_j
  k_blk = (ke-ks+1)

  is_pe = (my_blk_i-1)*i_blk + 1
  ie_pe = min(my_blk_i*i_blk,ie)

  js_pe = (my_blk_j-1)*j_blk + 1
  je_pe = min(my_blk_j*j_blk,je)
!
  write ( * , '(17x,3(5x,a5))' ) 'i-Dim', 'j-Dim', 'k-Dim'
  write ( * , '(17x,a30)')      '------------------------------'
  write ( * , '(a17,6i10)' ) 'Block Size is:   ', i_blk, j_blk, k_blk
  write ( * , '(a17,6i5)' ) 'Global Dims are: ', is, ie, js, je, ks, ke 
  write ( * , '(a17,6i5)' ) 'Local  Dims are: ', is_pe, ie_pe, js_pe, je_pe, ks, ke 
 ! define a gridless grid
  grid_type = PRISM_Gridless
  grid_name  = 'gdr_ocn_grid'
!
! define valid shape, no halo involved.
  valid_shape(1,1) = is_pe
  valid_shape(2,1) = ie_pe
  valid_shape(1,2) = js_pe
  valid_shape(2,2) = je_pe
  valid_shape(1,3) = ks
  valid_shape(2,3) = ke
!
  print *,'ocn: mype, valid shape ',mype, valid_shape

! define the type of grid and its size
  call prism_def_grid ( grid_id, grid_name, comp_id, valid_shape, &
                        grid_type, ierror )

!
! partition for def_partition - NOT NEEDED WHEN USING GLOBAL INDICES
! ... but the code with 0 offset works too.
!
  offset_array(1,1) = 0
  offset_array(1,2) = 0
  offset_array(1,3) = 0

  extent_array(1,1) = 2
  extent_array(1,2) = 1
  extent_array(1,3) = 1

!  print *,'ocn: mype, offset ', mype, offset_array
!  print *,'ocn: mype, extent ', mype, extent_array

  call prism_def_partition (grid_id, 1, &
                         offset_array, extent_array, ierror)
!
! This next calls prism_set_points_gridless()
!
  call prism_set_points_Gridless (method_id, point_name, grid_id, &
                         .true., ierror)

! define a variable

  var_name = 'gdr_ocn_wind'
  var_nodims(1) = 3
  var_nodims(2) = 0
  var_type = PRISM_Real
  call prism_def_var ( var_id,  var_name, grid_id, method_id, &
                  PRISM_UNDEFINED, var_nodims, valid_shape, var_type, ierror )

! assert that all definitions are complete
  call prism_enddef(ierror)

!
! Test prism_get
!
! initialise coupling data buffer


  allocate(g_wind(valid_shape(1,1):valid_shape(2,1), &
                valid_shape(1,2):valid_shape(2,2), &
                valid_shape(1,3):valid_shape(2,3)), STAT=ierror )
  if ( ierror /= 0 ) print *, 'Error allocating wind'

  allocate(test_var(valid_shape(1,1):valid_shape(2,1), &
                valid_shape(1,2):valid_shape(2,2), &
                valid_shape(1,3):valid_shape(2,3)), STAT=ierror )
  if ( ierror /= 0 ) print *, 'Error allocating wind'
!
! set up initial data
! val = cell number (in 1 to ncells) + 1000 to make ocn different!!
!
  val = (js_pe-1)*ie+is_pe+1000
  do k = valid_shape(1,3), valid_shape(2,3)
     do j = valid_shape(1,2), valid_shape(2,2)
        do i = valid_shape(1,1), valid_shape(2,1)
              g_wind (i,j,k) = val
              val = val+1
        end do
     end do
  end do
!
! Initialise receive array
!
  test_var = -1

  model_time           = PRISM_jobstart_date
  model_time_bounds(1) = PRISM_jobstart_date
  model_time_bounds(2) = PRISM_jobstart_date

  dincr1 = -3600.0
  dincr2 =  3600.0

  call PRISM_calc_newdate ( model_time_bounds(1), dincr1, ierror )
  call PRISM_calc_newdate ( model_time_bounds(2), dincr2, ierror )

  call prism_put ( var_id, model_time, model_time_bounds, g_wind, &
                   info, ierror )

  call prism_get ( var_id, model_time, model_time_bounds, test_var, &
                   info, ierror )
!
! Compare with result expected
!

  print *, '------------------------------'
  print *, 'ocn: g_wind value of put: ', g_wind
  print *, 'ocn: test_var value of get: ', test_var
  print *, '------------------------------'
  call prism_terminate ( ierror )

  end program appl_ocn
