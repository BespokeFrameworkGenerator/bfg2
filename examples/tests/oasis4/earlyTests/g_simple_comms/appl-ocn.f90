  program appl_ocn

  use PRISM
  implicit none

  character(len=128) :: appl_name = 'ocn'
  character(len=128) :: comp_name = 'ocn'
  integer :: comp_id
  integer :: ierror
  integer :: info
  Type(PRISM_Time_Struct) :: model_time
  Type(PRISM_Time_Struct) :: model_time_bounds(2)
  Double Precision :: dincr1, dincr2
!
  integer   :: var_id
  character(len=132) :: var_name,grid_name,point_name
  integer :: grid_type
  integer :: grid_id
  integer :: offset_array(1,3),extent_array(1,3)
  integer :: method_id
  integer :: var_nodims(2)
  integer :: var_type
  integer :: valid_shape(2,3)
  integer :: point_id
  logical :: flag
! coupling data
  real :: g_wind(2,1,1)
  real :: test_var(2,1,1)

  call prism_init_comp (comp_id, comp_name, ierror )

  write (*,*) 'I am component ',trim(comp_name), &
              ' in application ',trim(appl_name)

  call prism_initialized (flag, ierror)
  if (.not. flag) then
     write (*, *) trim(comp_name), "PRISM is NOT initialized"
  endif

  ! define a gridless grid
  grid_type = PRISM_Gridless
  grid_name  = 'gdr_ocn_grid'
  valid_shape(1,1) = 1
  valid_shape(2,1) = 2
  valid_shape(1,2) = 1
  valid_shape(2,2) = 1
  valid_shape(1,3) = 1
  valid_shape(2,3) = 1

! define the type of grid and its size
  call prism_def_grid ( grid_id, grid_name, comp_id, valid_shape, &
                        grid_type, ierror )

! partition for def_partition
!
  offset_array(1,1) = 0
  offset_array(1,2) = 0
  offset_array(1,3) = 0
!
  extent_array(1,1) = 2
  extent_array(1,2) = 1
  extent_array(1,3) = 1
!
 call prism_def_partition (grid_id, 1, &
                        offset_array, extent_array, ierror)
!------------------------------------------------------------------

  call prism_set_points_Gridless (method_id, point_name, grid_id, &
                         .true., ierror)

! define a variable

  var_name = 'gdr_ocn_wind'
  var_nodims(1) = 3
  var_nodims(2) = 0
  var_type = PRISM_Real
  call prism_def_var ( var_id,  var_name, grid_id, method_id, &
                  PRISM_UNDEFINED, var_nodims, valid_shape, var_type, ierror )

! assert that all definitions are complete
  call prism_enddef(ierror)

!------------------------------------------------------------
! Test prism coupling
!------------------------------------------------------------

! initialise coupling data buffer
  g_wind(1,1,1) = 10
  g_wind(2,1,1) = 11
  test_var(1,1,1) = -1
  test_var(2,1,1) = -1

  model_time           = PRISM_jobstart_date
  model_time_bounds(1) = PRISM_jobstart_date
  model_time_bounds(2) = PRISM_jobstart_date

  dincr1 = -3600.0
  dincr2 =  3600.0

  call PRISM_calc_newdate ( model_time_bounds(1), dincr1, ierror )
  call PRISM_calc_newdate ( model_time_bounds(2), dincr2, ierror )

  call prism_put ( var_id, model_time, model_time_bounds, g_wind, &
                   info, ierror )

  call prism_get ( var_id, model_time, model_time_bounds, test_var, &
                   info, ierror )
!
! Compare with result expected
!

  print *, '------------------------------'
  print *, 'ocn: g_wind value of put: ', g_wind
  print *, 'ocn: test_var value of get: ', test_var
  print *, '------------------------------'
  call prism_terminate ( ierror )

  end program appl_ocn
