! dummy nemo ocean model
module nemo
!
private
public nemoTimestep
!
contains
!
  subroutine nemoTimestep(heatFlux,dim1,dim2)
    integer, intent(in) :: dim1
    integer, intent(in) :: dim2
    real, intent(in) :: heatFlux(dim1,dim2)
    print *,"nemo received heatFlux:"
    print *,heatFlux
  end subroutine nemoTimestep
!
end module nemo
