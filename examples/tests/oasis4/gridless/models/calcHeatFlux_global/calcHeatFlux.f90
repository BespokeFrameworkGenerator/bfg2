! calcHeatFlux is an example of a stateless f90 transformation
module calcHeatFlux
!
private
public calcHeatFlux_ts
!
contains
!
subroutine calcHeatFlux_ts(solar,blue,landSeaMask,missingDataIndicator,longwave, &
                              sensible,latentHeatofCond,evap,lb2,ub2,lb1,ub1,heatFlux)
!
  integer, intent(in) :: lb2,ub2,lb1,ub1
  real, intent(in) :: missingDataIndicator, latentHeatofCond
  real, intent(in) :: solar(lb1:ub1,lb2:ub2),blue(lb1:ub1,lb2:ub2)
  integer, intent(in) :: landSeaMask(lb1:ub1,lb2:ub2)
  real, intent(in) :: longwave(lb1:ub1,lb2:ub2),sensible(lb1:ub1,lb2:ub2)
  real, intent(in) :: evap(lb1:ub1,lb2:ub2)
  real, intent(out) :: heatFlux(lb1:ub1,lb2:ub2)
! local vars
  integer :: i,j

  ! Calculate surface heat flux at sea-points
  do j=lb2,ub2
     do i=lb1,ub1
        if (landSeaMask(i,j)==1) Then
           heatFlux(i,j)=missingDataIndicator
        else
           heatFlux(i,j)=solar(i,j) - blue(i,j) + longwave(i,j) &
           - (sensible(i,j) + latentHeatOfCond * evap(i,j))
        endif
     end do
  end do
  return
  end subroutine calcHeatFlux_ts
!
end module
