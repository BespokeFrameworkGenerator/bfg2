! dummy UM atmos model
module atmos
!
private
public atmosTimestep
!
contains
!
  subroutine atmosTimestep(solar,blue,lb1,ub1,lb2,ub2)
    integer, intent(in) :: lb1,ub1
    integer, intent(in) :: lb2,ub2
    real, intent(out) :: solar(lb1:ub1,lb2:ub2)
    real, intent(out) ::  blue(lb1:ub1,lb2:ub2)
    solar=1.0
    blue=2.0
  end subroutine atmosTimestep
!
end module atmos
