       program BFG2Main
       use BFG2Target
       use BFG2InPlace
       ! Begin declaration of Control
       integer :: its1
       integer :: its2
       integer :: nts1
       integer :: nts2
       namelist /time/ nts1,nts2
       ! End declaration of Control
       ! Begin declaration of arguments
       real :: r1
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       common/iterations/its1,its2
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       call initModelInfo()
       ! Begin initial values data
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       call initComms()
       if (mod1Thread()) then
       call mod1_init_init(r1)
       end if
       its1=0
       its2=0
       do its1=1,nts1
       if (mod1Thread()) then
       call get(r1,-2)
       call mod1_run_iteration(r1)
       call put(r1,-2)
       end if
       do its2=1,nts2
       end do
       end do
       if (mod1Thread()) then
       call mod1_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main

