       module mod2
       contains

       subroutine run(a)
        implicit none
        real a
        a=a+1.0
        print*,"mod2: a=",a
       end subroutine run

       subroutine finalise
        print*,"mod2 final"
       end subroutine finalise

       end module
