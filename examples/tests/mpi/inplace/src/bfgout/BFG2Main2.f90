       program BFG2Main
       use BFG2Target2
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !mod1_freq
       integer :: mod1_freq
       !mod2_freq
       integer :: mod2_freq
       namelist /time/ nts1,mod1_freq,mod2_freq
       ! ****End declaration of Control****
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2Pnotation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       call initComms()
       its1=0
       do its1=1,nts1
       if (mod2Thread()) then
       if(mod(its1,mod2_freq).eq.0)then
       call setActiveModel(2)
       call run2()
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main

