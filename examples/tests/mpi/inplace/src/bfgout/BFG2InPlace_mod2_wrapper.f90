       ! f77 to f90put/get wrappers start
       subroutine put(data,tag)
       use BFG2Target2
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==2) then
       if (tag==1) then
       call putreal1(data,tag)
       end if
       end if
       end subroutine put
       subroutine get(data,tag)
       use BFG2Target2
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==2) then
       if (tag==0) then
       call getreal1(data,tag)
       end if
       end if
       end subroutine get
       subroutine getreal1(data,tag)
       use BFG2InPlace_mod2, only : get=>getreal1
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal1
       subroutine putreal1(data,tag)
       use BFG2InPlace_mod2, only : put=>putreal1
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal1
       ! f77 tof90 put/get wrappers end
