       module BFG2InPlace_mod2
       use BFG2Target2
       use mpi
       contains
       subroutine getreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=mod2 and ep=run2and instance=
       if (arg2==0) then
       ! Iam mod2.run2..2
       ! I am coupled using point to point notation
       ! to something on a different sequenceunit to me
       ! Use requested targetfor comms
       ! sending model: name=mod1 ep=run1 id=1
       du=b2mmap(1)
       mysize=1000
       call mpi_recv(arg1,mysize,mpi_real,du,3,mpi_comm_world,istatus,ierr)
       end if
       end if
       end subroutine getreal1
       subroutine putreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=mod2 and ep=run2 and instance=
       if (arg2==1) then
       ! I am mod2.run2..2
       ! I am coupled using point to point notation
       ! to something ona different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(1)
       ! receivingmodel: name=mod1 ep=run1 id=0
       mysize=1000
       call mpi_send(arg1,mysize,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       end if
       end subroutine putreal1
       end module BFG2InPlace_mod2
