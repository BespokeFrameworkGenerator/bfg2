       module BFG2InPlace_mod1
       use BFG2Target1
       use mpi
       contains
       subroutine getreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=mod1and ep=run1 and instance=
       if (arg2==0) then
       ! I am mod1.run1..1
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! sending model: name=mod2 ep=run2 id=1
       du=b2mmap(2)
       mysize=1000
       call mpi_recv(arg1,mysize,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       end if
       end if
       end subroutine getreal1
       subroutine putreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==1) then
       ! I am model=mod1 and ep=run1 and instance=
       if (arg2==1) then
       ! I am mod1.run1..1
       ! I am coupled using point to point notation
       ! to something on a different sequence unit to me
       ! Use requested target for comms
       ! I connect to 1 others
       du=b2mmap(2)
       ! receiving model: name=mod2 ep=run2 id=0
       mysize=1000
       call mpi_send(arg1,mysize,mpi_real,du,3,mpi_comm_world,ierr)
       end if
       end if
       end subroutine putreal1
       end module BFG2InPlace_mod1
