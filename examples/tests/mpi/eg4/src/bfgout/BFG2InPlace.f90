       module BFG2InPlace
       use BFG2Target
       private 
       public get,put
       interface get
       module procedure getreal0
       end interface
       interface put
       module procedure putreal0
       end interface
       contains
       subroutine getreal0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer, allocatable, dimension(:) :: set
       logical :: primed
       integer :: pDU
       if (arg2==-2) then
       ! I am mod1.run receiving from:
       primed=.true.
       pDU=info(1)%du
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=2
       myDU=info(myID)%du
       remoteID=getLast(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (primed.and.its1==1) then
       if (pDU.ne.myDU) then
       ! receive from priming source: on different DU to receiver
       du=pDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       else
       ! receive from priming source: on same DU as receiver
       end if
       else if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-3) then
       ! I am mod2.run receiving from:
       primed=.false.
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=4
       myDU=info(myID)%du
       remoteID=getLast(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (primed.and.its1==1) then
       if (pDU.ne.myDU) then
       ! receive from priming source: on different DU to receiver
       du=pDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       else
       ! receive from priming source: on same DU as receiver
       end if
       else if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       end if
       end if
       if (arg2==-4) then
       ! I am mod3.run receiving from:
       primed=.false.
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=3
       myDU=info(myID)%du
       remoteID=getLast(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (primed.and.its1==1) then
       if (pDU.ne.myDU) then
       ! receive from priming source: on different DU to receiver
       du=pDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       else
       ! receive from priming source: on same DU as receiver
       end if
       else if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU-1
       call mpi_recv(arg1,1,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       end if
       end if
       end subroutine getreal0
       subroutine putreal0(arg1,arg2)
       implicit none
       real  :: arg1
       integer  :: arg2
       include 'mpif.h'
       integer :: istatus
       integer :: ierr
       integer :: myID
       integer :: myDU
       integer :: remoteID
       integer :: remoteDU
       integer :: du
       integer :: setSize
       integer, allocatable, dimension(:) :: set
       logical :: primed
       integer :: pDU
       if (arg2==-2) then
       ! I am mod1.run sending to:
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=2
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       du=remoteDU-1
       call mpi_send(arg1,1,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       end if
       if (arg2==-3) then
       ! I am mod2.run sending to:
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=4
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       du=remoteDU-1
       call mpi_send(arg1,1,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       end if
       if (arg2==-4) then
       ! I am mod3.run sending to:
       setSize=3
       allocate(set(setSize))
       set=(/2,3,4/)
       myID=3
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       du=remoteDU-1
       call mpi_send(arg1,1,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       end if
       end subroutine putreal0
       end module BFG2InPlace
