       module BFG2Target
       use mod1, only : mod1_init_init=>init,&
mod1_run_iteration=>run,&
mod1_finalise_final=>finalise
       use mod2, only : mod2_run_iteration=>run,&
mod2_finalise_final=>finalise
       use mod3, only : mod3_run_iteration=>run,&
mod3_finalise_final=>finalise
       integer, target :: its1
       integer, target :: its2
       integer, target :: its3
       type modelInfo
        integer :: du
        integer :: period
        integer :: nesting
        integer :: bound
        integer :: offset
        integer, pointer :: its
       end type modelInfo

       type(modelInfo), dimension(7) :: info
       integer, parameter :: inf=32767
       common/iterations/its1,its2,its3
       contains
       logical function mod1Thread()
       implicit none
       ! only one thread so always true
       mod1Thread=.true.
       end function mod1Thread
       logical function mod2Thread()
       implicit none
       ! only one thread so always true
       mod2Thread=.true.
       end function mod2Thread
       logical function mod3Thread()
       implicit none
       ! only one thread so always true
       mod3Thread=.true.
       end function mod3Thread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       subroutine initComms()
       implicit none
       include 'mpif.h'
       integer :: ierr
       call mpi_init(ierr)
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       include 'mpif.h'
       integer :: ierr
       call mpi_finalize(ierr)
       end subroutine finaliseComms
       subroutine initModelInfo()
       implicit none
       ! model.ep=mod1.init
       info(1)%du=1
       info(1)%period=1
       info(1)%nesting=0
       info(1)%bound=0
       info(1)%offset=0
       nullify(info(1)%its)
       ! model.ep=mod1.run
       info(2)%du=1
       info(2)%period=1
       info(2)%nesting=1
       info(2)%bound=10
       info(2)%offset=0
       info(2)%its=>its1
       ! model.ep=mod1.finalise
       info(5)%du=1
       info(5)%period=1
       info(5)%nesting=0
       info(5)%bound=0
       info(5)%offset=0
       nullify(info(5)%its)
       ! model.ep=mod2.run
       info(4)%du=2
       info(4)%period=1
       info(4)%nesting=2
       info(4)%bound=2
       info(4)%offset=0
       info(4)%its=>its3
       ! model.ep=mod2.finalise
       info(6)%du=2
       info(6)%period=1
       info(6)%nesting=0
       info(6)%bound=0
       info(6)%offset=0
       nullify(info(6)%its)
       ! model.ep=mod3.run
       info(3)%du=3
       info(3)%period=2
       info(3)%nesting=2
       info(3)%bound=2
       info(3)%offset=0
       info(3)%its=>its2
       ! model.ep=mod3.finalise
       info(7)%du=3
       info(7)%period=2
       info(7)%nesting=0
       info(7)%bound=0
       info(7)%offset=0
       nullify(info(7)%its)
       end subroutine initModelInfo
       integer function getNext(list,lsize,point)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, allocatable, dimension(:) :: newlist
       integer, pointer :: its
       integer :: i
       integer :: newlsize
       integer :: currentNesting
       integer :: startPoint
       integer :: endPoint
       integer :: pos
       integer :: targetpos
       getNext=-1
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getNext==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getNext=findNext(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getNext==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getNext=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getNext
       integer recursive function findNext(list,lsize,point,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, intent(inout)  :: pos
       integer :: i
       integer :: j
       integer :: currentNesting
       integer, pointer :: previousIts
       integer :: startPoint
       integer :: endPoint
       integer :: newlsize
       integer :: nestNext
       integer :: currentMin
       integer :: remainIters
       integer :: waitIters
       integer :: its
       integer :: newpos
       integer, allocatable, dimension(:) :: newlist
       findNext=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       previousIts=>info(list(pos))%its
       if (list(pos).ne.point) then
       pos=pos-1
       end if
       do i=1,lsize
       pos=mod(pos+1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       its=info(list(pos))%its
       if (its==info(list(pos))%bound + 1) then
       its=1
       end if
       if (list(pos)>point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting>currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       deallocate(newlist)
       else if (not(associated(previousIts,info(list(pos))%its))) then
       if (findNext.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestNext=findNext(newlist,newlsize,point,newpos)
       if (nestNext.ne.-1) then
       findNext=nestNext
       return
       end if
       else
       remainIters=info(list(pos))%bound - its
       if (remainIters>0) then
       waitIters=info(list(pos))%period - mod(its,info(list(pos))%period)
       if (waitIters==1) then
       findNext=list(pos)
       return
       else if (waitIters<currentMin.and.waitIters<=remainIters) then
       findNext=list(pos)
       currentMin=waitIters
       end if
       end if
       end if
       end do
       end function findNext
       integer function getLast(list,lsize,point)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, allocatable, dimension(:) :: newlist
       integer, pointer :: its
       integer :: i
       integer :: newlsize
       integer :: startPoint
       integer :: endPoint
       integer :: pos
       integer :: currentNesting
       integer :: targetpos
       integer :: currentMin
       getLast=-1
       currentMin=inf
       do i=1,lsize
       if (list(i)==point) then
       pos=i
       end if
       end do
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       do while(getLast==-1)
       startPoint=findStartPoint(list,lsize,pos,its,currentNesting)
       endPoint=findEndPoint(list,lsize,pos,its,currentNesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       targetpos=1
       do i=1,newlsize
       if (point==newlist(i)) then
       targetpos=i
       end if
       end do
       getLast=findLast(newlist,newlsize,point,targetpos)
       deallocate(newlist)
       if (getLast==-1) then
       pos=getNextPos(list,lsize,pos)
       if (pos==-1) then
       getLast=-1
       return
       end if
       its=>info(list(pos))%its
       currentNesting=info(list(pos))%nesting
       end if
       end do
       end function getLast
       integer recursive function findLast(list,lsize,point,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: point
       integer, intent(inout)  :: pos
       integer :: i
       integer :: j
       integer :: currentNesting
       integer, pointer :: previousIts
       integer :: startPoint
       integer :: endPoint
       integer :: newlsize
       integer :: nestLast
       integer :: currentMin
       integer :: elapsedIters
       integer :: its
       integer :: newpos
       integer, allocatable, dimension(:) :: newlist
       findLast=-1
       currentMin=inf
       currentNesting=info(list(pos))%nesting
       previousIts=>info(list(pos))%its
       if (list(pos).ne.point) then
       pos=pos+1
       end if
       do i=1,lsize
       pos=mod(pos-1,lsize)
       if (pos==0) then
       pos=lsize
       end if
       its=info(list(pos))%its
       if (its==info(list(pos))%bound + 1) then
       its=info(list(pos))%bound
       end if
       if (list(pos)>=point) then
       its=its - 1
       end if
       if (info(list(pos))%nesting>currentNesting) then
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       deallocate(newlist)
       else if (not(associated(previousIts,info(list(pos))%its))) then
       if (findLast.ne.-1) then
       return
       end if
       startPoint=findStartPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       endPoint=findEndPoint(list,lsize,pos,info(list(pos))%its,info(list(pos))%nesting)
       newlsize=endPoint - startPoint + 1
       allocate(newlist(newlsize))
       newlist(1:newlsize)=list(startPoint:endPoint)
       newpos=1
       do j=1,newlsize
       if (list(pos)==newlist(j)) then
       newpos=j
       end if
       end do
       nestLast=findLast(newlist,newlsize,point,newpos)
       if (nestLast.ne.-1) then
       findLast=nestLast
       return
       end if
       else
       if (its>0) then
       elapsedIters=mod(its,info(list(pos))%period)
       if (elapsedIters==0) then
       findLast=list(pos)
       return
       else if (elapsedIters<currentMin.and.elapsedIters<its) then
       findLast=list(pos)
       currentMin=elapsedIters
       end if
       end if
       end if
       end do
       end function findLast
       integer function getNextPos(list,lsize,pos)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting<info(list(pos))%nesting.and.not(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       do i=pos+1,lsize
       if (info(list(i))%nesting<info(list(pos))%nesting.and.not(associated(info(list(i))%its,info(list(pos))%its))) then
       getNextPos=i
       return
       end if
       end do
       getNextPos=-1
       end function getNextPos
       integer function findStartPoint(list,lsize,pos,its,nesting)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer, pointer  :: its
       integer, intent(in)  :: nesting
       integer :: i
       do i=pos-1,1,-1
       if (info(list(i))%nesting<=nesting.and.not(associated(info(list(i))%its,its))) then
       findStartPoint=i + 1
       return
       end if
       end do
       findStartPoint=1
       end function findStartPoint
       integer function findEndPoint(list,lsize,pos,its,nesting)
       implicit none
       integer, intent(in) , dimension(:) :: list
       integer, intent(in)  :: lsize
       integer, intent(in)  :: pos
       integer, pointer  :: its
       integer, intent(in)  :: nesting
       integer :: i
       do i=pos+1,lsize
       if (info(list(i))%nesting<=nesting.and.not(associated(info(list(i))%its,its))) then
       findEndPoint=i - 1
       return
       end if
       end do
       findEndPoint=lsize
       end function findEndPoint
       end module BFG2Target
