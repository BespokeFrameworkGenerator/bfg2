       program BFG2Main
       use BFG2Target
       use BFG2InPlace
       ! Begin declaration of Control
       integer :: its1
       integer :: its2
       integer :: its3
       integer :: nts1
       integer :: nts2
       integer :: nts3
       integer :: mod3_freq
       namelist /time/ nts1,nts2,nts3,mod3_freq
       ! End declaration of Control
       ! Begin declaration of arguments
       real :: r1
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       common/iterations/its1,its2,its3
       ! Begin control values file read
       open(unit=101112,file='BFG2Control.nam')
       read(101112,time)
       close(101112)
       ! End control values file read
       call initModelInfo()
       ! Begin initial values data
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       call initComms()
       do its1=1,nts1
       do its2=1,nts2
       if (mod3Thread()) then
       if(mod(its2,mod3_freq).eq.0)then
       call get(r1,-4)
       call mod3_run_iteration(r1)
       call put(r1,-4)
       end if
       end if
       end do
       do its3=1,nts3
       end do
       end do
       if (mod3Thread()) then
       call mod3_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main

