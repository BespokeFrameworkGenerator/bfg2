       module mod1
       contains

       subroutine init(a)
        implicit none
        real a
        a=0.0
        print*,"mod1.init: a=",a
       end subroutine init

       subroutine run(a)
        implicit none
        real a
        a=a+1.0
        print*,"mod1.run: a=",a
       end subroutine run

       subroutine finalise
        print*,"mod1 final"
       end subroutine finalise

       end module
