       module mod3
       contains

       subroutine init(a)
        implicit none
        real a
        a=a+1.0
        print*,"mod3.init: a=",a
       end subroutine init

       subroutine run(a)
        implicit none
        real a
        a=a+1.0
        print*,"mod3.run: a=",a
       end subroutine run

       subroutine finalise
        print*,"mod3 final"
       end subroutine finalise

       end module
