       program BFG2Main
       use BFG2Target2
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !mod1_freq
       integer :: mod1_freq
       !mod2_freq
       integer :: mod2_freq
       namelist /time/ nts1,mod1_freq,mod2_freq
       ! ****End declarationof Control****
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !
       real, dimension(1:10) :: r1
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notationpriming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       call nmlinit12(r1)
       ! netcdf files
       ! End initial values file read
       call initComms()
       its1=0
       do its1=1,nts1
       if (mod2Thread()) then
       if(mod(its1,mod2_freq).eq.0)then
       call setActiveModel(2)
       call get_mod2(r1,-1)
       call run2(r1)
       call put_mod2(r1,-1)
       end if
       end if
       end do
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit12(a)
        implicit none
       real, dimension(1:10) :: a
       namelist /areal/ a
       open(unit=12,file='areal.nam')
       read(12,areal)
       close(12)
       end subroutine nmlinit12
