       module BFG2InPlace_mod2
       use BFG2Target2
       use mpi
       contains
       subroutine getreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       !pp2p
       integer, dimension(1) :: pp2p=(/1/)
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=mod2 and ep=run2 and instance=
       if (arg2==-1) then
       ! I am mod2.run2..2
       remoteID=-1
       setSize=2
       allocate(set(1:setSize))
       set=(/1,2/)
       myID=2
       myDU=info(myID)%du
       if (pp2p(1).eq.1) then
       pp2p=.false.
       else
       remoteID=getLast(set,setSize,myID)
       end if
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! receive from remote source
       du=remoteDU-1
       mysize=10
       call mpi_recv(arg1,mysize,mpi_real,du,1,mpi_comm_world,istatus,ierr)
       end if
       end if
       end if
       end subroutine getreal1
       subroutine putreal1(arg1,arg2)
       implicit none
       real , dimension(*) :: arg1
       integer  :: arg2
       !ierr
       integer :: ierr
       !istatus
       integer, dimension(1:mpi_status_size) :: istatus
       !mysize
       integer :: mysize
       !myID
       integer :: myID
       !myDU
       integer :: myDU
       !remoteID
       integer :: remoteID
       !remoteDU
       integer :: remoteDU
       !du
       integer :: du
       !setSize
       integer :: setSize
       !newSize
       integer :: newSize
       !current
       integer :: current
       !its
       integer :: its
       !currentModel
       integer :: currentModel
       !i1
       integer :: i1
       !set
       integer, allocatable, dimension(:) :: set
       !newset
       integer, allocatable, dimension(:) :: newset
       !ins
       integer, allocatable, dimension(:) :: ins
       !pDU
       integer :: pDU
       currentModel=getActiveModel()
       if (currentModel==2) then
       ! I am model=mod2 and ep=run2 and instance=
       if (arg2==-1) then
       ! I am mod2.run2..2
       setSize=2
       allocate(set(1:setSize))
       allocate(newSet(1:setSize))
       allocate(ins(1:setSize))
       set=(/1,2/)
       ins=(/0,0/)
       myID=2
       myDU=info(myID)%du
       remoteID=getNext(set,setSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU-1
       mysize=10
       call mpi_send(arg1,mysize,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       newSize=setSize
       newSet=set
       do its=1,setSize
       if (set(its)==remoteID) then
       current=its
       end if
       end do
       do while(newSize.gt.2.and.ins(current)==1.and.remoteID.ne.myID)
       do its=current,newSize-1
       newSet(its)=newSet(its+1)
       end do
       newSize=newSize-1
       remoteID=getNext(newSet,newSize,myID)
       if (remoteID==-1) then
       remoteID=myID
       end if
       remoteDU=info(remoteID)%du
       if (remoteDU.ne.myDU) then
       ! send to remote source
       du=remoteDU-1
       mysize=10
       call mpi_send(arg1,mysize,mpi_real,du,1,mpi_comm_world,ierr)
       end if
       do its=1,newSize
       if (newSet(its)==remoteID) then
       current=its
       end if
       end do
       end do
       end if
       end if
       end subroutine putreal1
       end module BFG2InPlace_mod2
