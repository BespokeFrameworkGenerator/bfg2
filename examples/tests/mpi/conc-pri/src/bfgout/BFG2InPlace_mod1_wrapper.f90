       ! f77 to f90 put/get wrappers start
       subroutine put_mod1(data,tag)
       use BFG2Target1
       implicit none
       real , intent(in) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==1) then
       if (tag==-1) then
       call putreal1_mod1(data,tag)
       end if
       end if
       end subroutine put_mod1
       subroutine get_mod1(data,tag)
       use BFG2Target1
       implicit none
       real , intent(out) :: data
       integer , intent(in) :: tag
       ! data is really void *
       !currentModel
       integer :: currentModel
       currentModel=getActiveModel()
       if (currentModel==1) then
       if (tag==-1) then
       call getreal1_mod1(data,tag)
       end if
       end if
       end subroutine get_mod1
       subroutine getreal1_mod1(data,tag)
       use BFG2InPlace_mod1, only : get=>getreal1
       implicit none
       real , intent(in), dimension(*) :: data
       integer , intent(in) :: tag
       call get(data,tag)
       end subroutine getreal1_mod1
       subroutine putreal1_mod1(data,tag)
       use BFG2InPlace_mod1, only : put=>putreal1
       implicit none
       real , intent(out), dimension(*) :: data
       integer , intent(in) :: tag
       call put(data,tag)
       end subroutine putreal1_mod1
       ! f77 to f90 put/get wrappers end
