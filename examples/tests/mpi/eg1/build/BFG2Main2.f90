       program BFG2Main
       use BFG2Target2
       use BFG2InPlace_mod2, only : put_mod2=>put,&
get_mod2=>get
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !mod2__freq
       integer :: mod2__freq
       namelist /time/ nts1,mod2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       !
       real, dimension(1:64) :: r1
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=1011,file='BFG2Control.nam')
       read(1011,time)
       close(1011)
       ! End control values file read
       ! Init model data structures
       ! (for concurrent models coupled using set notation)
       call initModelInfo()
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       do its1=1,nts1
       if (mod2Thread()) then
       if(mod(its1,mod2__freq).eq.0)then
       call setActiveModel(3)
       call get_mod2(r1,-1)
       call mod2_run_iteration(r1)
       call put_mod2(r1,-1)
       end if
       end if
       call commsSync()
       end do
       if (mod2Thread()) then
       call setActiveModel(5)
       call mod2_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main

