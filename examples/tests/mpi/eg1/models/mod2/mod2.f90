       module mod2
       contains

       subroutine run(a)
        implicit none
        real, dimension(64) :: a
        print*,"mod2:run on entry a=",a
        a=a+1.0
        print*,"mod2:run on exit a=",a
       end subroutine run

       subroutine finalise
        print*,"mod2 final"
       end subroutine finalise

       end module
