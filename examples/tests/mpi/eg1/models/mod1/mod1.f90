       module mod1
       contains

       subroutine init(a)
        implicit none
        real, dimension(64) :: a
        a=1.0
        print*,"mod1:init on exit a=",a
       end subroutine init

       subroutine run(a)
        implicit none
        real, dimension(64) :: a
        print*,"mod1:run on entry a=",a
        a=a+1.0
        print*,"mod1:run on exit a=",a
       end subroutine run

       subroutine finalise
        print*,"mod1 final"
       end subroutine finalise

       end module
