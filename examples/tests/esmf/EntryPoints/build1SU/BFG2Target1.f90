       module BFG2Target1
       use single, only : single_singlesub_iteration=>singlesub
       use multiple, only : multiple_init1_init=>init1,&
multiple_init2_init=>init2,&
multiple_tstep1_iteration=>tstep1,&
multiple_finish_final=>finish
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer  , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function singleThread()
       implicit none
       ! only one thread so always true
       singleThread=.true.
       end function singleThread
       logical function multipleThread()
       implicit none
       ! only one thread so always true
       multipleThread=.true.
       end function multipleThread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
