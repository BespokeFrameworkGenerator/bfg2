module simpleMixed2
private
public ts
real :: r4,r5
contains
!
  subroutine ts(r1,r2,r3)
!
  real,intent(in)    :: r1
  real,intent(inout) :: r2
  real,intent(out)   :: r3
!
  print *,"Hello from module simpleMixed2 subroutine ts"
!
  call get(r4,10)
!
  print *,"r1 (argpass in) on input is ",r1
  print *,"r2 (argpass inout) on input is ",r2
  print *,"r4 (inplace 'get') on input is ",r4
!
  r2=r2+1.0
  r3=r2
  r5=-r2
!
  call put(r5,11)
!
  print *,"r2 (argpass inout) on output is ",r2
  print *,"r3 (argpass out) on output is ",r3
  print *,"r5 (inplace 'put') on output is ",r5
!
  end subroutine ts
!
end module simpleMixed2
