#! /usr/bin/env python
import os, fnmatch, sys
from optparse import OptionParser

usage = "usage: %prog [top_level_directory]"
version = "1.0"
parser = OptionParser(usage)
(options, args) = parser.parse_args()

if len(args) > 1:
    parser.error("incorrect number of arguments")
elif (len(args) == 1):
    root=args[0]
    # check it is a valid directory
    if not os.path.isdir(root):
        parser.error("specified directory does not exist")
    rootList=[root]
else:
    #root=os.curdir
    #root="1SingleModel"
    #temporarily explicitly list directories until we remove original ones.
    #eventually root=os.curdir should do what we want.
    rootList=["1SingleModel","2MultipleModels"]
    #rootList=["2MultipleModels","1SingleModel"]
	
def locate(pattern, rootList=rootList):
    '''Locate path to all files matching supplied filename pattern in and below
    supplied root directory.'''
    rootDir=os.path.abspath(os.path.dirname(sys.argv[0]))
    for root in rootList:
        os.chdir(rootDir) # needed as os.walk changes the base directory
        basePath=os.path.abspath(root)
    	for path, dirs, files in os.walk(basePath):
            for filename in fnmatch.filter(files, pattern):
                # HACK to limit to 'f90' only
                if '/f90' in path:
                    yield path

error=0
success=0
errorPaths=[]
for path in locate("Makefile",rootList):
    os.chdir(path)
    ok=os.system("make")
    print path
    if ok==0:
        success+=1
    else :
        error+=1
        errorPaths.append(path)
    os.system("make clean")


print
print str(error)+" error(s) found in "+str(error+success)+" test(s)"
for path in errorPaths:
    print path
if error==0:
    print "All tests passed"
