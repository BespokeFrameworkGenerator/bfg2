       module BFG2Target1
       use single, only : single_singlesub_iteration=>singlesub
       use single2, only : single2_singlesub_iteration=>singlesub
       use mpi
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(2)
       integer :: b2mmap(2)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function singleThread()
       implicit none
       if (bfgSUID==1) then
       singleThread=.true.
       else
       singleThread=.false.
       end if
       end function singleThread
       logical function single2Thread()
       implicit none
       if (bfgSUID==1) then
       single2Thread=.true.
       else
       single2Thread=.false.
       end if
       end function single2Thread
       subroutine commsSync()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       call mpi_barrier(mpi_comm_world,ierr)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       use mpi
       implicit none
       !ierr
       integer :: ierr
       !rc
       integer :: rc
       !globalsize
       integer :: globalsize
       !globalrank
       integer :: globalrank
       !colour
       integer :: colour
       !key
       integer :: key
       !localsize
       integer :: localsize
       !localrank
       integer :: localrank
       !b2mtemp(2)
       integer :: b2mtemp(2)
       !mpi_comm_local
       integer :: mpi_comm_local
       call mpi_init(ierr)
       call mpi_comm_size(mpi_comm_world,globalsize,ierr)
       call mpi_comm_rank(mpi_comm_world,globalrank,ierr)
       ! arbitrarily decide on a unique colour for this deployment unit
       colour=1
       if (globalsize.ne.5) then
       print *,"Error: (du",colour,"):","5 threads should be requested"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       key=0
       call mpi_comm_split(mpi_comm_world,colour,key,mpi_comm_local,ierr)
       call mpi_comm_size(mpi_comm_local,localsize,ierr)
       call mpi_comm_rank(mpi_comm_local,localrank,ierr)
       if (localsize.ne.2) then
       print *,"Error: 2 threads expected in du",colour
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! arbitrarily bind model threads to a local rank (and therefore a global rank)
       b2mtemp=0
       if (localrank.ge.0.and.localrank.le.1) then
       ! model name is 'single'
       ! model name is 'single2'
       bfgSUID=1
       if (localrank==0) then
       b2mtemp(bfgSUID)=globalrank
       end if
       end if
       if (localrank.lt.0.or.localrank.gt.1) then
       print *,"'Error: (du",colour,",0): localrank has unexpected value"
       print *,"aborting ..."
       call mpi_abort(mpi_comm_world,rc,ierr)
       else
       ! distribute id's to all su's
       call mpi_allreduce(b2mtemp,b2mmap,2,mpi_integer,mpi_sum,mpi_comm_world,ierr)
       if (localrank==0) then
       print *,"du",colour,"bfg to mpi id map is",b2mmap
       end if
       end if
       end if
       end if
       end subroutine initComms
       subroutine finaliseComms()
       use mpi
       implicit none
       !globalrank
       integer :: globalrank
       !ierr
       integer :: ierr
       call mpi_finalize(ierr)
       end subroutine finaliseComms
       end module BFG2Target1
