program NetcdfFileRead

 use netcdf
 implicit none
 integer :: ncid, status, varid
 integer :: dimids(0)

 real :: value1,value2,value3,value4

 status = nf90_open("bfg2Data1.nc",nf90_NoWrite,ncid) 
 if (status /= nf90_noerr) call handle_err(status,1)

 status = nf90_inq_varid(ncid, "prime1", varid)
 if (status /= nf90_noerr) stop "Error in netcdf inquire for prime1 in BFG2Data1.nc"
 status = nf90_get_var(ncid, varid, value1)
 if (status /= nf90_noerr) stop "Error in netcdf get_var for variable value1 called prime1 in file BFG2Data1.nc"
 print *,"prime1 value is ",value1

 status = nf90_inq_varid(ncid, "prime2", varid)
 if (status /= nf90_noerr) stop "Error in netcdf inquire for prime2 in BFG2Data1.nc"
 status = nf90_get_var(ncid, varid, value2)
 if (status /= nf90_noerr) stop "Error in netcdf get_var for value2 in BFG2Data1.nc"
 print *,"prime2 value is ",value2

 status = nf90_inq_varid(ncid, "prime3", varid)
 if (status /= nf90_noerr) stop "Error in netcdf inquire for prime3 in BFG2Data1.nc"
 status = nf90_get_var(ncid, varid, value3)
 if (status /= nf90_noerr) stop "Error in netcdf get_var for value3 in BFG2Data1.nc"
 print *,"prime3 value is ",value3

 status = nf90_close(ncid)
 if (status /= nf90_noerr) call handle_err(status,9)


 status = nf90_open("bfg2Data2.nc",nf90_NoWrite,ncid) 
 if (status /= nf90_noerr) call handle_err(status,1)

 status = nf90_inq_varid(ncid, "prime4", varid)
 if (status /= nf90_noerr) stop "Error in netcdf inquire for prime4 in BFG2Data2.nc"
 status = nf90_get_var(ncid, varid, value4)
 if (status /= nf90_noerr) stop "Error in netcdf get_var for variable value4 called prime4 in file BFG2Data2.nc"
 print *,"prime4 value is ",value4

 status = nf90_close(ncid)
 if (status /= nf90_noerr) call handle_err(status,9)


contains

      subroutine handle_err(status,pos)
        integer, intent ( in) :: status,pos
     
        if(status /= nf90_noerr) then
          print *, trim(nf90_strerror(status)),". Position is ",pos
          stop "Stopped"
        end if
      end subroutine handle_err


end program netcdfFileRead
