program NetcdfFileCreate

 use netcdf
 implicit none
 integer :: ncid, status, varid1, varid2, varid3
 integer :: dimids(0)

 status = nf90_create(path = "bfg2Data1.nc", cmode = nf90_clobber, ncid = ncid) 
 if (status /= nf90_noerr) call handle_err(status,1)

 ! nf90_def_dim

 status = nf90_def_var(ncid, "prime1", NF90_FLOAT, dimids, varid1)
 if (status /= nf90_noerr) call handle_err(status,2)

 status = nf90_def_var(ncid, "prime2", NF90_FLOAT, dimids, varid2)
 if (status /= nf90_noerr) call handle_err(status,3)

 status = nf90_def_var(ncid, "prime3", NF90_FLOAT, dimids, varid3)
 if (status /= nf90_noerr) call handle_err(status,4)

 ! NF90_PUT_ATT       ! assign attribute values

 status = nf90_enddef(ncid)
 if (status /= nf90_noerr) call handle_err(status,5)

 status = nf90_put_var(ncid, varid1, 3.1415926536)
 if (status /= nf90_noerr) call handle_err(status,6)
 status = nf90_put_var(ncid, varid2, 1.414)
 if (status /= nf90_noerr) call handle_err(status,7)
 status = nf90_put_var(ncid, varid3, 0.732)
 if (status /= nf90_noerr) call handle_err(status,8)

 status = nf90_close(ncid)
 if (status /= nf90_noerr) call handle_err(status,9)

 status = nf90_create(path = "bfg2Data2.nc", cmode = nf90_clobber, ncid = ncid) 
 if (status /= nf90_noerr) call handle_err(status,1)

 ! nf90_def_dim

 status = nf90_def_var(ncid, "prime4", NF90_FLOAT, dimids, varid1)
 if (status /= nf90_noerr) call handle_err(status,2)

 status = nf90_enddef(ncid)
 if (status /= nf90_noerr) call handle_err(status,5)

 status = nf90_put_var(ncid, varid1, 3.1415926536)
 if (status /= nf90_noerr) call handle_err(status,6)

 status = nf90_close(ncid)
 if (status /= nf90_noerr) call handle_err(status,9)

contains

      subroutine handle_err(status,pos)
        integer, intent ( in) :: status,pos
     
        if(status /= nf90_noerr) then
          print *, trim(nf90_strerror(status)),". Position is ",pos
          stop "Stopped"
        end if
      end subroutine handle_err


end program netcdfFileCreate
