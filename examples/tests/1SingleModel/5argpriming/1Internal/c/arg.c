#include <stdlib.h>
#include <stdio.h>
/*
  we can't internally prime an 'in' as 'in' means always in.

  internal priming means the data is an 'out' *for that call* but
  it may be that in other circumstances the code is an 'inout'.
  E.g. on a particular call a by external data (e.g. via internal
  switches that BFG knows nothing about). This example is coded
  show two types of potential internal switch (hence the
  possibility of the data being an inout, rather than simply an out.
*/
_Bool InternallyPrimed=1;

      void init(float *data1) {
        if (InternallyPrimed) {
          *data1=1.0;
        }
        *data1+=1.0;
        printf("init: internally primed\n");
      } 
_Bool first=1;
      void ts1(float *data1) {
        if (first) {
          *data1=1.0;
          first=0;
          printf("ts1: internally primed\n");
        }
        else {
          printf("ts1\n");
        }
        *data1+=1.0;
      }

float myData=1.0;
      void ts2(float *data1) {
        myData+=1.0;
        if (*data1!=myData) {
          printf("Error: expecting input data to be %f\n",myData);
          exit(1);
        }
        printf("ts2\n");
      }

