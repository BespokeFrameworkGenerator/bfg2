C we can't internally prime an 'in' as 'in' means always in.
C
C internal priming means the data is an 'out' *for that call* but
C it may be that in other circumstances the code is an 'inout'.
C E.g. on a particular call a by external data (e.g. via internal
C switches that BFG knows nothing about). This example is coded
C show two types of potential internal switch (hence the
C possibility of the data being an inout, rather than simply an out.
C
      subroutine init(data1)
C       inout
        real data1
        logical InternallyPrimed
        parameter (InternallyPrimed=.true.)
        if (InternallyPrimed) then
          data1=1.0
        end if
        data1=data1+1.0
        print *,"init: internally primed"
      end
C
      subroutine ts1(data1)
C       inout
        real data1
        logical first
        data first /.true./
        save first
        if (first) then
          data1=1.0
          first=.false.
          print *,"ts1: internally primed"
        else
          print *,"ts1"
        end if
        data1=data1+1.0   
      end
C
      subroutine ts2(data1)
C       in
        real data1
        real myData
        data myData /1.0/
        save myData
        myData=myData+1.0
        if (data1.ne.myData) then
          print *,"Error: expecting input data to be ",myData
          call abort()
        end if
        print *,"ts2"
      end

