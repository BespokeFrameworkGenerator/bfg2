module arg
private
public init,ts1,ts2
logical :: InternallyPrimed=.true.
logical :: first=.true.
real :: myData=1.0
contains
!
! we can't internally prime an 'in' as 'in' means always in.
!
! internal priming means the data is an 'out' *for that call* but
! it may be that in other circumstances the code is an 'inout'.
! E.g. on a particular call a by external data (e.g. via internal
! switches that BFG knows nothing about). This example is coded
! show two types of potential internal switch (hence the
! possibility of the data being an inout, rather than simply an out.
!
  subroutine init(data1)
    real,intent(inout) :: data1
    if (InternallyPrimed) then
      data1=1.0
    end if
    data1=data1+1.0
    print *,"init: internally primed"
  end subroutine init
!
  subroutine ts1(data1)
    real,intent(inout) :: data1
    if (first) then
      data1=1.0
      first=.false.
      print *,"ts1: internally primed"
    else
      print *,"ts1"
    end if
    data1=data1+1.0   
  end subroutine ts1
!
  subroutine ts2(data1)
    real,intent(in) :: data1
    myData=myData+1.0
    if (data1/=myData) then
      print *,"Error: expecting input data to be ",myData
      call abort()
    end if
    print *,"ts2"
  end subroutine ts2
!
end module arg
