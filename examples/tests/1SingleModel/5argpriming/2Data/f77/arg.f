      subroutine init1(data1)
C       in
        real data1
        if (data1.ne.1.0) then
          print *,"Error, expecting init1:data1 to be primed with the "
          print *,"value 1.0 but found ",data1
          call exit(1)
        end if
        print *,"init1: primed"
      end
C
      subroutine init2(data1)
C       inout
        real data1
        if (data1.ne.1.0) then
          print *,"Error, expecting init2:data1 to be primed with the "
          print *,"value 1.0 but found ",data1
          call exit(1)
        end if
        data1=data1+1.0
        print *,"init2: primed"
      end
C
      subroutine ts1(data1)
C       in
        real data1
        logical first
        data first /.true./
        save first
        if (first) then
          first=.false.
          if (data1/=1.0) then
            print *,"Error, expecting ts1:data1 to be primed"
            print *,"with the value 1.0 but found ",data1
            call exit(1)
          end if
          print *,"ts1: primed"
        else
          print *,"ts1"
        end if
      end
C
      subroutine ts2(data1)
C       inout
        real data1
        logical first
        data first /.true./
        save first
        if (first) then
          first=.false.
          if (data1/=1.0) then
            print *,"Error, expecting ts2:data1 to be primed"
            print *,"with the value 1.0 but found ",data1
            call exit(1)
          end if
          print *,"ts2: primed"
        else
          print *,"ts2"
        end if
        data1=data1+1.0   
      end
C
      subroutine ts3(data1)
C       in
        real data1
        real myData
        data myData /1.0/
        save myData
        myData=myData+1.0
        if (data1/=myData) then
          print *,"Error in ts3: expecting input data to be "
          print *,myData," but found ",data1
          call exit(1)
        end if
        print *,"ts3"
      end
