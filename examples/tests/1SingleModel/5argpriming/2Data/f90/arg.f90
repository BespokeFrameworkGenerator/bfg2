module arg
private
public init1,init2,ts1,ts2,ts3
logical :: first1=.true.
logical :: first2=.true.
real :: myData=1.0
contains
!
  subroutine init1(data1)
    real,intent(in) :: data1
    if (data1/=1.0) then
      print *,"Error, expecting init:data1 to be primed with the value 1.0 but found ",data1
      call exit(1)
    end if
    print *,"init1: primed"
  end subroutine init1
!
  subroutine init2(data1)
    real,intent(inout) :: data1
    if (data1/=1.0) then
      print *,"Error, expecting init:data1 to be primed with the value 1.0 but found ",data1
      call exit(1)
    end if
    data1=data1+1.0
    print *,"init2: primed"
  end subroutine init2
!
  subroutine ts1(data1)
    real,intent(in) :: data1
    if (first1) then
      first1=.false.
      if (data1/=1.0) then
        print *,"Error, expecting ts1:data1 to be primed with the value 1.0 but found ",data1
        call exit(1)
      end if
      print *,"ts1: primed"
    else
      print *,"ts1"
    end if
  end subroutine ts1
!
  subroutine ts2(data1)
    real,intent(inout) :: data1
    if (first2) then
      first2=.false.
      if (data1/=1.0) then
        print *,"Error, expecting ts2:data1 to be primed with the value 1.0 but found ",data1
        call exit(1)
      end if
      print *,"ts2: primed"
    else
      print *,"ts2"
    end if
    data1=data1+1.0   
  end subroutine ts2
!
  subroutine ts3(data1)
    real,intent(in) :: data1
    myData=myData+1.0
    if (data1/=myData) then
      print *,"Error in ts3: expecting input data to be ",myData," but found ",data1
      call exit(1)
    end if
    print *,"ts3"
  end subroutine ts3
!
end module arg
