#include <stdlib.h>
#include <stdio.h>

void init1(float *data1) {
  if (*data1!=1.0) {
    printf("Error, expecting init1:data1 to be primed with the value 1.0 but found %f\n",*data1);
    exit(1);
  }
  printf("init1: primed\n");
}

void init2(float *data1) {
  if (*data1!=1.0) {
    printf("Error, expecting init2:data1 to be primed with the value 1.0 but found %f\n",*data1);
    exit(1);
  }
  *data1+=1.0;
  printf("init2: primed\n");
}

_Bool first1=1;
void ts1(float *data1) {
  if (first1) {
    first1=0;
    if (*data1!=1.0) {
      printf ("Error, expecting ts1:data1 to be primed with the value 1.0 but found %f\n",*data1);
      exit(1);
    }
    printf("ts1: primed\n");
  }
  else {
    printf("ts1\n");
  }
}

_Bool first2=1;
void ts2(float *data1) {
  if (first2) {
    first2=0;
    if (*data1!=1.0) {
      printf ("Error, expecting ts2:data1 to be primed with the value 1.0 but found %f\n",*data1);
      exit(1);
    }
    printf("ts2: primed\n");
  }
  else {
    printf("ts2\n");
  }
  *data1+=1.0;
}

float myData=1.0;
void ts3(float *data1) {
  myData+=1.0;
  if (*data1!=myData) {
    printf("Error in ts2: expecting input data to be %f but found %f",myData,*data1);
    exit(1);
  }
  printf("ts2\n");
}
