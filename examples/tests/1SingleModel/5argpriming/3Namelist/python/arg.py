class arg:

    def __init__(self):
        self.first1=True
        self.first2=True
        self.myData=1.0
        print "HELLO FROM CONSTRUCTOR"

    def init1(self,data1):
        assert data1==1.0, "Error, expecting init:data1 to be primed with the value 1.0 but found "+str(data1)
        print "init1: primed"

    def init2(self,data1,data2):
        assert data2==1.0, "Error, expecting init2:data2 to be primed with the value 1.0 but found "+str(data2)
        assert data1==1.0, "Error, expecting init:data1 to be primed with the value 1.0 but found "+str(data1)
        data1+=1.0
        print "init2: primed"
        return data1

    def ts1(self,data1):
        if self.first1:
            self.first1=False
            assert data1==1.0, "Error, expecting ts1:data1 to be primed with the value 1.0 but found "+str(data1)
            print "ts1: primed"
        else:
            print "ts1"

    def ts2(self,data1):
        if self.first2:
            self.first2=False
            assert data1==1.0, "Error, expecting ts2:data1 to be primed with the value 1.0 but found "+str(data1)
            print "ts2: primed"
        else:
            print "ts2"
        data1+=1.0
        print "ts2: data is "+str(data1)
        return data1

    def ts3(self,data1):
        self.myData+=1.0
        assert data1==self.myData, "Error in ts3: expecting input data to be "+str(self.myData)+" but found "+str(data1)
        print "ts3"

