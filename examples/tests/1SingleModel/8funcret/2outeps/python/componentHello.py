class componentHello(object):

    def init1(self):
        print("Hello world: init1")
        return 11

    def init2(self):
        print("Hello world: init2")
        return 12

    def ts1(self):
        print("Hello world: ts1")
        return 21
            
    def ts2(self):
        print("Hello world: ts2")
        return 22

    def final1(self):
        print("Hello world: final1")
        return 31

    def final2(self):
        print("Hello world: final2")
        return 32
