import numpy
class componentHello(object):

  def _createArrays(self):

    int32Array1d=numpy.arange(4,dtype=numpy.int)
    int64Array1d=int32Array1d.astype(numpy.int64)
    float32Array1d=int32Array1d.astype(numpy.float32)
    float64Array1d=int32Array1d.astype(numpy.float)
    return int32Array1d,int64Array1d,float32Array1d,float64Array1d

  def hello1(self):
    return self._createArrays()

  def hello2(self,int32Array1d,int64Array1d,float32Array1d,float64Array1d):
    checkInt32Array1d,checkInt64Array1d,checkFloat32Array1d,checkFloat64Array1d=self._createArrays()
    error=False
    if not numpy.array_equal(checkInt32Array1d,int32Array1d):
      print "componentHello:hello2: Error, int array differs from original"
      error=True
    if not numpy.array_equal(checkInt64Array1d,int64Array1d):
      print "componentHello:hello2: Error, int64 array differs from original"
      error=True
    if not numpy.array_equal(checkFloat32Array1d,float32Array1d):
      print "componentHello:hello2: Error, float32 array differs from original"
      error=True
    if not numpy.array_equal(checkFloat64Array1d,float64Array1d):
      print "componentHello:hello2: Error, float array differs from original"
      error=True

    if error:
      print "test failed"
      exit(1)
    else:
      print "success"

if __name__ == "__main__":

    c = componentHello()
    i32,i64,f32,f64=c.hello1()
    c.hello2(i32,i64,f32,f64)
