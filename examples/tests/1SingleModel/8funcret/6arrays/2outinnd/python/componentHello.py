import numpy
class componentHello(object):

  def _createArrays(self):
    floatArray2d=numpy.linspace(0.0,29.0,30).reshape(6,5)
    floatArray3d=numpy.linspace(0.0,119.0,120).reshape(6,5,4)
    floatArray4d=numpy.linspace(0.0,359.0,360).reshape(6,5,4,3)
    floatArray5d=numpy.linspace(0.0,719.0,720).reshape(6,5,4,3,2)
    return floatArray2d,floatArray3d,floatArray4d,floatArray5d

  def hello1(self):
    return self._createArrays()

  def hello2(self,floatArray2d,floatArray3d,floatArray4d,floatArray5d):
    checkFloatArray2d,checkFloatArray3d,checkFloatArray4d,checkFloatArray5d=self._createArrays()
    error=False
    if not numpy.array_equal(checkFloatArray2d,floatArray2d):
      print "componentHello:hello2: Error, 2d array differs from original"
      error=True
    if not numpy.array_equal(checkFloatArray3d,floatArray3d):
      print "componentHello:hello2: Error, 3d array differs from original"
      error=True
    if not numpy.array_equal(checkFloatArray4d,floatArray4d):
      print "componentHello:hello2: Error, 4d array differs from original"
      error=True
    if not numpy.array_equal(checkFloatArray5d,floatArray5d):
      print "componentHello:hello2: Error, 5d array differs from original"
      error=True

    if error:
      print "test failed"
      exit(1)
    else:
      print "success"

if __name__ == "__main__":

    c = componentHello()
    f2d,f3d,f4d,f5d=c.hello1()
    c.hello2(f2d,f3d,f4d,f5d)
