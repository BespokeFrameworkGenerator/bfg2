import numpy
class componentHello(object):

  def hello1(self):
    floatArray=numpy.linspace(0,9,10)
    return floatArray

  def hello2(self, floatArray):
    error=False
    if not type(floatArray)==numpy.ndarray: # should be a numpy array
      print "componentHello:hello2: Error, expecting array type {0} but found {1}".format(numpy.ndArray,type(floatArray))
      error=True
    elif not floatArray.dtype==numpy.float64: # should contain float64
      print "componentHello:hello2: Error, expecting data type {0} but found {1}".format(numpy.float64,floatArray.dtype)
      error=True
    elif not len(floatArray.shape)==1: # should be 1d
      print "componentHello:hello2: Error, expecting array dimension {0} but found {1}".format(1,len(intArray.shape))
      error=True
    elif not floatArray.size==10: # should be of size 10
      print "componentHello:hello2: Error, expecting array to be of size {0} but found {1}".format(10,intArray.size)
      error=True
    else:
      arrayValueError=False
      for i,value in enumerate(floatArray): # values should be equal to index
        if not value==float(i):
          arrayValueError=True
      if arrayValueError:
        print "componentHello:hello2: Error, one or more arrays values are incorrect"
        print "expecting 0...9 but found {0}".format(floatArray)
        error=True
    if error:
      print "test failed"
      exit(1)
    else:
      print "success"

if __name__ == "__main__":

    c = componentHello()
    intArray=c.hello1()
    c.hello2(intArray)
