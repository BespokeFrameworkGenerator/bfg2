class componentHello(object):

    def hello1(self):

    	a = 77
    	b = 2.57
        c = True
        d = "hello"
   	print("componentHello:hello1: funcret[0]={0} funcret[1]={1} funcret[0]={2} funcret[1]={3}".format(a, b, c, d))
    	return a,b,c,d


    def hello2(self,a,b,c,d):

        error=False

        error=self._test(a,int  ,77     ,1,error)
        error=self._test(b,float,2.57   ,2,error)
        error=self._test(c,bool ,True   ,3,error)
        error=self._test(d,str  ,"hello",4,error)

        if error:
            print("componentHello:hello2: test failed")
            exit(1)
        else:
            print("componentHello:hello2: success")


    def _test(self,a,expectedType,expectedValue,argPosition,errorIn):
        errorOut=errorIn
        if not type(a)==expectedType: # check type
            print "Error, incorrect type found. Expecting {0} but found {1}".format(expectedType,type(a))
            errorOut=True
        elif type(a)==str: # check length as well as value for strings
            if not len(a)==len(expectedValue):
                print "Error, incorrect string length found. Expecting {0} but found {1} ({2})".format(len("hello"),len(a),a)
                error=True
            elif not a==expectedValue:
                print "Error, expected {0} for arg {1} but found {2}".format(expectedValue,argPosition,a)
                errorOut=True
        elif not a==expectedValue: # just check value
            print "Error, expected {0} for arg {1} but found {2}".format(expectedValue,argPosition,a)
            errorOut=True
        return errorOut


