#include <stdio.h>
void init1(int *a) {
    *a=10;
    printf ("Hello World! : init1 arg=%d\n",*a);
}
void init2(int *a) {
    *a=20;
    printf ("Hello World! : init2 arg=%d\n",*a);
}
void ts1(int *a) {
    *a=30;
    printf ("Hello World! : ts1 arg=%d\n",*a);
}
void ts2(int *a) {
    *a=40;
    printf ("Hello World! : ts2 arg=%d\n",*a);
}
void final1(int *a) {
    *a=50;
    printf ("Hello World! : final1 arg=%d\n",*a);
}
void final2(int *a) {
    *a=60;
    printf ("Hello World! : final2 arg=%d\n",*a);
}

