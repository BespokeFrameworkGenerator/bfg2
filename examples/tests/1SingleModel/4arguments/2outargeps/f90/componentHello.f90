module componentHello
!
contains
!
  subroutine init1(a)
    integer, intent(out) :: a
    a=10
    print *,"Hello World! : init1 arg=",a
  end subroutine init1
  subroutine init2(a)
    integer, intent(out) :: a
    a=20
    print *,"Hello World! : init2 arg=",a
  end subroutine init2
  subroutine ts1(a)
    integer, intent(out) :: a
    a=30
    print *,"Hello World! : ts1 arg=",a
  end subroutine ts1
  subroutine ts2(a)
    integer, intent(out) :: a
    a=40
    print *,"Hello World! : ts2 arg=",a
  end subroutine ts2
  subroutine final1(a)
    integer, intent(out) :: a
    a=50
    print *,"Hello World! : final1 arg=",a
  end subroutine final1
  subroutine final2(a)
    integer, intent(out) :: a
    a=60
    print *,"Hello World! : final2 arg=",a
  end subroutine final2
!
end module componentHello
