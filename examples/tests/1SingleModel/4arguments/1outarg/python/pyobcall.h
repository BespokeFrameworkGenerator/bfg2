#ifndef PYOBCALL_H
#define PYOBCALL_H

#include <Python.h>
#include <stdlib.h>
#include <stdio.h>


int tlf_ia(const char const *module, const char const *method, int *arg)
{
	PyObject *clientMod, *clientDict, *pfunc, *pargs, *ret;
	
	*arg = 77;
	setenv("PYTHONPATH", ".", 0);

	Py_Initialize();

	clientMod = PyImport_ImportModule(module);
	clientDict = PyModule_GetDict(clientMod);

	pfunc = PyObject_GetAttrString(clientMod, method);
	pargs = Py_BuildValue("(i)", *arg);

	ret = PyEval_CallObject(pfunc, pargs);

	Py_DECREF(clientMod);
	Py_DECREF(clientDict);
	Py_DECREF(pfunc);
	Py_DECREF(pargs);
	Py_DECREF(ret);

	Py_Finalize();

	return 0;

}


int pyobcall(const char const *module, const char const *cls, const char const *method)
{

	PyObject *clientMod, *clientClass, *instance, *clientDict;
	PyObject *ret, *pfunc, *pargs;
	PyObject *num;
	char *r;

	setenv("PYTHONPATH", ".", 0);

	Py_Initialize();

	clientMod = PyImport_ImportModule(module);
	clientDict = PyModule_GetDict(clientMod);

	clientClass = PyObject_GetAttrString(clientMod, cls);
	instance = PyEval_CallObject(clientClass, (PyObject*)NULL);	// Default constructor

	pfunc = PyObject_GetAttrString(instance, method);
	ret = PyEval_CallObject(pfunc, (PyObject*)NULL);

        PyArg_Parse(ret, "s", &r);
	//printf("func(args) = %s\n", r);

	Py_DECREF(clientMod);
	Py_DECREF(clientClass);
	Py_DECREF(instance);
	Py_DECREF(ret);
	Py_DECREF(pfunc);

	Py_Finalize();


	return 0;

}


#endif
