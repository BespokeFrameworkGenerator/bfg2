module componentHello
!
contains
!
  subroutine hello(a)
    integer, intent(out) :: a
    a=10
    print *,"Hello World! arg=",a
  end subroutine hello
!
end module componentHello
