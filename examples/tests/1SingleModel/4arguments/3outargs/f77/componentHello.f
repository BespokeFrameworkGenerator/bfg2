      subroutine hello(i,i1,i2,i4,i8,r,r4,r8,r16,d,l,l1,l2,l4,l8,c,c1,c65535,clen1,clen65535,co,dco,b)

        integer    :: i
        integer*1  :: i1
        integer*2  :: i2
        integer*4  :: i4
        integer*8  :: i8
        !integer*16 :: i16

        real    :: r
        !real*1 :: r1
        !real*2 :: r2
        real*4  :: r4
        real*8  :: r8
        real*16 :: r16
        !real*32 :: r32

        double precision :: d

        logical   :: l
        logical*1 :: l1
        logical*2 :: l2
        logical*4 :: l4
        logical*8 :: l8
        !logical*16 :: l16

        character :: c
        !character*0 :: c0 ! valid (in ifort) but makes no sense
        character*1 :: c1
        character*65535 :: c65535
        !character(len=0) :: clen0 ! valid (in ifort) but makes no sense
        character(len=1) :: clen1
        character(len=65535) :: clen65535

        !complex ...
        complex :: co
        double complex :: dco

        !byte ...
        byte :: b
 
        i=10
        i1=10
        i2=10
        i4=10
        i8=10

        r=10.0 
        r4=10.0 
        r8=10.0 
        r16=10.0 

        d=10.0

        l=.true.
        l1=.true.
        l2=.true.
        l4=.true.
        l8=.true.

        c='a'
        c1='a'
        c65535='a'
        clen1='a'
        clen65535='a'

        co=(10.0,10.0)
        dco=(10.0,10.0)

        b=1

        print *,"Hello World!"

      end subroutine
