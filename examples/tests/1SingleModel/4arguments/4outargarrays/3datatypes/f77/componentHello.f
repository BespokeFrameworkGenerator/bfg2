      subroutine hello(i,i1,i2,i4,i8,r,r4,r8,r16,d,l,l1,l2,l4,l8,c,c1,c65535,clen1,clen65535,co,dco,b)

        integer    :: i(2,2)
        integer*1  :: i1(2,2)
        integer*2  :: i2(2,2)
        integer*4  :: i4(2,2)
        integer*8  :: i8(2,2)

        real    :: r(2,2)
        real*4  :: r4(2,2)
        real*8  :: r8(2,2)
        real*16 :: r16(2,2)

        double precision :: d(2,2)

        logical   :: l(2,2)
        logical*1 :: l1(2,2)
        logical*2 :: l2(2,2)
        logical*4 :: l4(2,2)
        logical*8 :: l8(2,2)

        character :: c(2,2)
        character*1 :: c1(2,2)
        character*65535 :: c65535(2,2)
        character(len=1) :: clen1(2,2)
        character(len=65535) :: clen65535(2,2)

        complex :: co(2,2)
        double complex :: dco(2,2)

        byte :: b(2,2)
 
        i=10
        i1=10
        i2=10
        i4=10
        i8=10

        r=10.0 
        r4=10.0 
        r8=10.0 
        r16=10.0 

        d=10.0

        l=.true.
        l1=.true.
        l2=.true.
        l4=.true.
        l8=.true.

        c='a'
        c1='a'
        c65535='a'
        clen1='a'
        clen65535='a'

        co=(10.0,10.0)
        dco=(10.0,10.0)

        b=1

        print *,"Hello World!"
      end subroutine


