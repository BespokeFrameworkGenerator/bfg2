      subroutine hello(a,b,c)
        implicit none
        integer a(4)
        integer b(4)
        integer c(2:5)
        integer i
        do i=1,4
          a(i)=10
          b(i)=9
          c(i+1)=8
        end do
        print *,"Hello World!"
      end
