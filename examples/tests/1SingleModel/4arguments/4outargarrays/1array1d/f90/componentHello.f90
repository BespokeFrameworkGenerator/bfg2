module componentHello
!
contains
!
  subroutine hello(a,b,c)
    integer, intent(out) :: a(4)
    integer, intent(out) :: b(4)
    integer, intent(out) :: c(2:5)
    a=10
    b=9
    c=8
    print *,"Hello World!"
  end subroutine hello
!
end module componentHello
