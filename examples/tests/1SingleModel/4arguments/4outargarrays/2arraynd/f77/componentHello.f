      subroutine hello(a1,a2,a3,a4,a5,a6,a7,aa7)
        integer, intent(out) :: a1(2)
        integer, intent(out) :: a2(2,2)
        integer, intent(out) :: a3(2,2,2)
        integer, intent(out) :: a4(2,2,2,2)
        integer, intent(out) :: a5(2,2,2,2,2)
        integer, intent(out) :: a6(2,2,2,2,2,2)
        integer, intent(out) :: a7(2,2,2,2,2,2,2)
        integer, intent(out) :: aa7(1:2,2:3,3:4,4:5,5:6,6:7,7:8)
        a1=1
        a2=2
        a3=3
        a4=4
        a5=5
        a6=6
        a7=7
        aa7=88
        print *,"Hello World!"
      end subroutine
