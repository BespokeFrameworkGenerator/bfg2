module inplace
use BFG2, only : put,get
private
public init1,ts1,ts2
logical :: first1=.true.
logical :: first2=.true.
real :: myData=1.0
contains
!
  subroutine init1()
    real :: data1
    call get(data1,1)
    if (data1/=1.0) then
      print *,"Error, expecting init:data1 to be primed with the value 1.0 but found ",data1
      call exit(1)
    end if
    print *,"init1: primed"
  end subroutine init1
!
  subroutine ts1()
    real :: data1
    call get(data1,2)
    if (first1) then
      first1=.false.
      if (data1/=1.0) then
        print *,"Error, expecting ts1:data1 to be primed with the value 1.0 but found ",data1
        call exit(1)
      end if
      print *,"ts1: primed"
    else
      print *,"ts1"
    end if
    call put(data1,3)
  end subroutine ts1
!
  subroutine ts2()
    real :: data1
    call get(data1,4)
    print *,"ts2"
    data1=data1+1.0   
    call put(data1,5)
  end subroutine ts2
!
end module inplace
