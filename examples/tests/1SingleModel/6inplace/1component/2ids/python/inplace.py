# import or inherit?
#from BFG2, import put,get
#class inplace(object)
class inplace(BFG2)

  def __init__(self):
      first1=True

  def init1(self):
      data1=0.0
      get(data1,1)
      if data1!=1.0:
          print "Error, expecting init:data1 to be primed with the value 1.0 but found ",data1
          exit(1)
      print "init1: primed"

  def ts1(self):
      data1=0.0
      get(data1,2)
      if first1:
          first1=False
          if data1!=1.0:
              print "Error, expecting ts1:data1 to be primed with the value 1.0 but found ",data1
              exit(1)
          print "ts1: primed"
      else:
          print "ts1"
      put(data1,3)

  def ts2(self):
    data1=0.0
    get(data1,4)
    print "ts2"
    data1+=1.0
    put(data1,5)

