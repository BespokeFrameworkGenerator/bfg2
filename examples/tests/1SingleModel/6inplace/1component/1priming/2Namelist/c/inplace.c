void init1() {
  float data1;
  get(&data1,1);
  if (data1!=1.0) {
    printf ("Error, expecting init:data1 to be primed with the value 1.0 but found %f\n",data1);
    exit(1);
  }
  printf("init1: primed\n");
} 
//
void ts1() {
  float data1;
  _Bool first1=1;
  get(&data1,1);
  if (first1) {
    first1=0;
    if (data1!=1.0) {
      printf ("Error, expecting ts1:data1 to be primed with the value 1.0 but found %f\n",data1);
      exit(1);
    }
    printf ("ts1: primed\n");
  }
  else {
    printf("ts1\n");
  }
  put(&data1,1);
}
//
void ts2() {
  float data1;
  get(&data1,1);
  printf("ts2\n");
  data1+=1.0;
  put(&data1,1);
}

