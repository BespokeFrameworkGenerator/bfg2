CCCuse BFG2, only : put,get
C
      subroutine init1()
        real data1
        call get(data1,1)
        if (data1.ne.1.0) then
          print *,"Error, expecting init:data1 to be primed with "
          print *,"the value 1.0 but found ",data1
          call exit(1)
        end if
        print *,"init1: primed"
      end
C
      subroutine ts1()
        real data1
        logical first1
        save first1
        data first1 /.true./
        call get(data1,1)
        if (first1) then
          first1=.false.
          if (data1.ne.1.0) then
            print *,"Error, expecting ts1:data1 to be primed with "
            print *,"the value 1.0 but found ",data1
            call exit(1)
          end if
          print *,"ts1: primed"
        else
          print *,"ts1"
        end if
        call put(data1,1)
      end
C
      subroutine ts2()
        real data1
        call get(data1,1)
        print *,"ts2"
        data1=data1+1.0   
        call put(data1,1)
      end

