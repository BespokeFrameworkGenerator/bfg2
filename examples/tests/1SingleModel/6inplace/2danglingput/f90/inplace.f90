module inplace
use BFG2, only : put,get
private
public init1,ts1,ts2
contains
!
  subroutine init1()
    real :: data1
    data1=1.0
    call put(data1,1)
  end subroutine init1
!
  subroutine ts1()
    real :: data1
    data1=2.0
    call put(data1,1)
  end subroutine ts1
!
  subroutine ts2()
    real :: data1
    data1=3.0
    call put(data1,1)
  end subroutine ts2
!
end module inplace
