


#include <Python.h>
#include <stdlib.h>
#include <stdio.h>

static const char const *MODULE = "programHello.py";

int main(void)
{
        setenv("PYTHONPATH", ".", 0);

        Py_Initialize();

        FILE* pyfile = fopen(MODULE, "r");
        PyRun_AnyFile(pyfile, MODULE);
 
        Py_Finalize();

	return 0;

}

