      subroutine hello(not_converged)
        implicit none
        integer count
        data count /4/
        save count
        logical not_converged

        count=count-1
        print *,"Hello World!, count=",count
        not_converged=.true.
        if (count==0) then
          print *,"Converged"
          not_converged=.false.
        end if
      end
