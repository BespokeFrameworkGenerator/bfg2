#include <stdio.h>
static int count=4;
void hello(_Bool not_converged) {
  count--;
  printf("Hello World!, count =%d",count);
  not_converged=1;
  if (count==0) {
    printf("Converged\n");
    not_converged=0;
  }
  printf("Mapping between c bool and fortran logical not yet working, so aborting\n");
  exit(1);
}

