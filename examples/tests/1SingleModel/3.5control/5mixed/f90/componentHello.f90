module componentHello
!
implicit none
private
public :: init1, init2, ts1, ts2, ts3, final1, final2
integer::count_ts1=3
integer::count_ts2=3
!
contains
!
  subroutine init1
    print *,"Hello World!: init1"
  end subroutine init1
  subroutine init2
    print *,"Hello World!: init2"
  end subroutine init2
  subroutine ts1(not_converged)
    logical :: not_converged
    count_ts1=count_ts1-1
    print *,"Hello World!: ts1: count=",count_ts1
    not_converged=.true.
    if (count_ts1==0) then
      print *,"Converged"
      not_converged=.false.
      count_ts1=3
    end if
  end subroutine ts1
  subroutine ts2(not_converged)
    logical :: not_converged
    count_ts2=count_ts2-1
    print *,"Hello World!: ts2: count=",count_ts2
    not_converged=.true.
    if (count_ts2==0) then
      print *,"Converged"
      not_converged=.false.
      count_ts2=3
    end if
  end subroutine ts2
  subroutine ts3
    print *,"Hello World!: ts3"
  end subroutine ts3
  subroutine final1
    print *,"Hello World!: final1"
  end subroutine final1
  subroutine final2
    print *,"Hello World!: final2"
  end subroutine final2
!
end module componentHello
