#ifndef PYOBCALL_H
#define PYOBCALL_H

#include <Python.h>
#include <stdlib.h>
#include <stdio.h>

#include "rettype.h"

PyObject *gInstance = NULL;



PFreturn *vacall(const char const *module, const char const *cls, const char const *method, char *fmt, ...)
{

	PyObject *clientMod, *clientClass, *instance, *clientDict;
	PyObject *pargs=NULL, *ret=NULL, *pfunc=NULL;
	va_list ap;

	setenv("PYTHONPATH", ".", 0);

	Py_Initialize();

	fprintf(stderr, "[PYOBCALL] %s %s %s\n", module, cls, method);

	clientMod = PyImport_ImportModule(module);
	clientDict = PyModule_GetDict(clientMod);

	clientClass = PyObject_GetAttrString(clientMod, cls);

	if(!gInstance)
		gInstance = PyEval_CallObject(clientClass, (PyObject*)NULL);	// Default constructor

	instance = gInstance;

	if(fmt){
	        va_start(ap, fmt);
        	pargs = Py_VaBuildValue(fmt, ap);
	        va_end(ap);
	}

	PFreturn *pfr = (PFreturn *)malloc(sizeof(PFreturn));

	if(method){
		if(strcmp(method, "constructor")){
			fprintf(stderr, "----> calling method %s\n", method);
			pfunc = PyObject_GetAttrString(instance, method);
			ret = PyEval_CallObject(pfunc, pargs);
		}
		if(ret){
			PyArg_ParseTuple(ret, PFreturn_fmt, RETVARS);
//			printf("func(args) = (%d, %f)\n", pfr->r0, pfr->r1);
			Py_DECREF(ret);
		}

		if(pfunc)
			Py_DECREF(pfunc);
	}


	Py_DECREF(clientMod);
	Py_DECREF(clientClass);

	if(!gInstance)
		Py_DECREF(instance);

	Py_Finalize();

	return pfr;

}


#endif
