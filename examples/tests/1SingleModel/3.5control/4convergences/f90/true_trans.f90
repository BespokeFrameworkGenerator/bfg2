module true_trans
!
implicit none
private
public true
!
contains
!
  subroutine true(value)
    logical, intent(out) :: value
    value=.true.
  end subroutine true
!
end module true_trans
