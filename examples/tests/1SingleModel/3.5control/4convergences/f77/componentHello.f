      subroutine init1
        print *,"Hello World! : init1"
      end
      subroutine init2
        print *,"Hello World! : init2"
      end
      subroutine ts1(not_converged)
        logical not_converged
        integer count_ts1
        data count_ts1 /3/
	save count_ts1
        count_ts1=count_ts1-1
        print *,"Hello World!: ts1: count=",count_ts1
        not_converged=.true.
        if (count_ts1==0) then
          print *,"Converged"
          not_converged=.false.
          count_ts1=3
        end if
      end
      subroutine ts2(not_converged)
        logical :: not_converged
        integer count_ts2
        data count_ts2 /3/
	save count_ts2
        count_ts2=count_ts2-1
        print *,"Hello World!: ts2: count=",count_ts2
        not_converged=.true.
        if (count_ts2==0) then
          print *,"Converged"
          not_converged=.false.
          count_ts2=3
        end if
      end
      subroutine final1
        print *,"Hello World! : final1"
      end
      subroutine final2
        print *,"Hello World! : final2"
      end

