module componentHello
!
contains
!
  subroutine init1
    print *,"Hello World! : init1"
  end subroutine init1
  subroutine init2
    print *,"Hello World! : init2"
  end subroutine init2
  subroutine ts1
    print *,"Hello World! : ts1"
  end subroutine ts1
  subroutine ts2
    print *,"Hello World! : ts2"
  end subroutine ts2
  subroutine final1
    print *,"Hello World! : final1"
  end subroutine final1
  subroutine final2
    print *,"Hello World! : final2"
  end subroutine final2
!
end module componentHello
