module model1
implicit none
private
public :: model1ep
contains
  subroutine model1ep(myreal1,myreal2)
    double precision, intent(out) :: myreal1
    double precision, intent(in) :: myreal2
    myreal1=3.14d0
    if (myreal2/=3.14d0) then
      print *,"Error, expecting 3.14d0 but received ",myreal2
    else
      print *,"Success: expected value returned"
    end if
  end subroutine model1ep
end module model1
