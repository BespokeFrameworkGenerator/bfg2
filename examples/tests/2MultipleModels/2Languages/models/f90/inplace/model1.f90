module model1
implicit none
private
public :: model1ep
contains
  subroutine model1ep()
    use bfg2, only : put,get
    double precision :: myreal1
    double precision :: myreal2
    call get(myreal2,2)
    myreal1=3.14d0
    if (myreal2/=3.14d0) then
      print *,"Error f90:inplace, expecting 3.14d0 but received ",myreal2
    else
      print *,"Success: expected value returned"
    end if
    call put(myreal1,1)
  end subroutine model1ep
end module model1
