      subroutine model1ep(myreal1,myreal2)
      double precision myreal1,myreal2
      myreal1=3.14d0
      if (myreal2.ne.3.14d0) then
        print *,"Error, expecting 3.14d0 but received ",myreal2
      else
        print *,"Success: expected value returned"
      end if
      end subroutine
