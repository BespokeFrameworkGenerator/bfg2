module simplePut
private
public ts
!
real :: r1=0.0
!
contains
!
  subroutine ts()
    !
    use bfg2, only : put
    !
    print *,"Hello from module simplePut subroutine ts"
    !
    print *,"simplePut r1 (inplace out) value is ",r1
    call put(r1,1)
    !
    r1=r1+1.0
    !
  end subroutine ts
!
end module simplePut
