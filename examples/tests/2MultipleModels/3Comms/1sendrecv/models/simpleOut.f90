module simpleOut
private
public ts
!
real :: r1_keep=0.0
!
contains
!
  subroutine ts(r1)
    !
    real, intent(out) :: r1
    !
    print *,"Hello from module simplePut subroutine ts"
    !
    r1_keep=r1_keep+1.0
    r1=r1_keep
    !
    print *,"simplePut r1 (argpass out) value is ",r1
    !
  end subroutine ts
!
end module simpleOut
