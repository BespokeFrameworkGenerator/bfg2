module simpleGet
private
public ts
!
real :: r1
!
contains
!
  subroutine ts()
    !
    use bfg2, only : get
    !
    print *,"Hello from module simpleGet subroutine ts"
    !
    call get(r1,1)
    print *,"simpleGet r1 (inplace in) value is ",r1
    !
  end subroutine ts
!
end module simpleGet
