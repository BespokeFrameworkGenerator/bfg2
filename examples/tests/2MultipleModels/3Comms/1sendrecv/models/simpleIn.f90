module simpleIn
private
public ts
!
contains
!
  subroutine ts(r1)
    !
    real, intent(in) :: r1
    !
    print *,"Hello from module simpleIn subroutine ts"
    !
    print *,"simpleIn r1 (argpass in) value is ",r1
    !
  end subroutine ts
!
end module simpleIn
