module arg2
private
public init,ts
contains
!
  subroutine init(data1)
    real,intent(inout) :: data1
    if (data1/=1.0) then
      print *,"Error, expecting init:data1 to be primed with the value 1.0 but found ",data1
      call exit(1)
    end if
    print *,"init: OK"
  end subroutine init
!
  subroutine ts()
    print *,"ts"
  end subroutine ts
!
end module arg2
