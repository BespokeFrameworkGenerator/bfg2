from numpy  import *
class componentIn:
  def input(self,a1in,a2in,a3in,a4in,a5in,a6in,a7in):
      a1 = arange(2)
      assert (a1==a1in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a1in,a1)
      a2 = arange(4).reshape(2,2)
      assert (a2==a2in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a2in,a2)
      a3 = arange(8).reshape(2,2,2)
      assert (a3==a3in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a3in,a3)
      a4 = arange(16).reshape(2,2,2,2)
      assert (a4==a4in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a4in,a4)
      a5 = arange(32).reshape(2,2,2,2,2)
      assert (a5==a5in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a5in,a5)
      a6 = arange(64).reshape(2,2,2,2,2,2)
      assert (a6==a6in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a6in,a6)
      a7 = arange(128).reshape(2,2,2,2,2,2,2)
      assert (a7==a7in).all(), "Error, arrays do no match\nfound: {0}\nexpected: {1}\n".format(a7in,a7)
      #aa7 = arange(4).reshape(1:2,2:3,3:4,4:5,5:6,6:7,7:8) ???

