from numpy  import *
class componentOut:
  def output(self):
      a1 = arange(2)
      a2 = arange(4).reshape(2,2)
      a3 = arange(8).reshape(2,2,2)
      a4 = arange(16).reshape(2,2,2,2)
      a5 = arange(32).reshape(2,2,2,2,2)
      a6 = arange(64).reshape(2,2,2,2,2,2)
      a7 = arange(128).reshape(2,2,2,2,2,2,2)
      #aa7 = arange(4).reshape(1:2,2:3,3:4,4:5,5:6,6:7,7:8) ???
      return a1,a2,a3,a4,a5,a6,a7
