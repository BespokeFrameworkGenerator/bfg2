module model1
implicit none
private
public init, timestep, finalise
contains
!
  subroutine init(a)
    integer,intent(out) :: a
    a=1
    print *,"Model1:init()"
  end subroutine init
!
  subroutine timestep(a)
    integer,intent(inout) :: a
    a=a+1
    print *,"Model1:timestep()"
  end subroutine timestep
!
  subroutine finalise(a)
    integer,intent(in) :: a
    print *,"Model1:finalise()"
    if (a.ne.5) then
      call abort("Error expecting data=5")
    else
      print *,"Model1:finalise():Data is correct"
    end if
  end subroutine finalise
!
end module model1
