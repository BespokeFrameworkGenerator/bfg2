module model3
implicit none
private
public timestep
contains
!
  subroutine timestep(a)
    integer,intent(inout) :: a
    a=a+1
    print *,"Model3:timestep()"
  end subroutine timestep
!
end module model3
