module model2
implicit none
private
public init, timestep1, timestep2, finalise
contains
!
  subroutine init()
    print *,"Model2:init()"
  end subroutine init
!
  subroutine timestep1()
    print *,"Model2:timestep1()"
  end subroutine timestep1
!
  subroutine timestep2()
    print *,"Model2:timestep2()"
  end subroutine timestep2
!
  subroutine finalise()
    print *,"Model2:finalise()"
  end subroutine finalise
!
end module model2
