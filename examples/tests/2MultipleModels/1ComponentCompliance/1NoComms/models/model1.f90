module model1
implicit none
private
public init, timestep, finalise
contains
!
  subroutine init()
    print *,"Model1:init()"
  end subroutine init
!
  subroutine timestep()
    print *,"Model1:timestep()"
  end subroutine timestep
!
  subroutine finalise()
    print *,"Model1:finalise()"
  end subroutine finalise
!
end module model1
