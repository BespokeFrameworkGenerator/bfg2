module componentHello3
!
implicit none
private
public :: hello3
!
contains
!
  subroutine hello3()
    print *,"Hello World!, from componentHello3:hello"
  end subroutine hello3
!
end module componentHello3
