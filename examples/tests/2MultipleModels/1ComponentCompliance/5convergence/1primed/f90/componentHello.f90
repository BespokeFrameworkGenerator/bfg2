module componentHello
!
implicit none
private
public :: hello
integer::count=4
!
contains
!
  subroutine hello(not_converged)
    logical :: not_converged
    count=count-1
    print *,"Hello World!, count=",count
    not_converged=.true.
    if (count==0) then
      print *,"Converged"
      not_converged=.false.
    end if
  end subroutine hello
!
end module componentHello
