module componentHello
!
implicit none
private
public :: hello
!
contains
!
  subroutine hello(enter_convergence)
    logical :: enter_convergence
    enter_convergence=.true.
  end subroutine hello
!
end module componentHello
