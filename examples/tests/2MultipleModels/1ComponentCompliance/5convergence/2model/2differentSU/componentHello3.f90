module componentHello3
!
implicit none
private
public :: hello3,final
integer::count=4
!
contains
!
  subroutine hello3(not_converged)
    logical :: not_converged
    count=count-1
    print *,"Hello World!, count=",count
    not_converged=.true.
    if (count==0) then
      print *,"Converged"
      not_converged=.false.
    end if
  end subroutine hello3
!
  subroutine final()
    if (.not.(count==0)) then
      stop "Error in convergence test"
    end if
  end subroutine final
!
end module componentHello3
