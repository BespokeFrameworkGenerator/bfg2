module componentHello2
!
implicit none
private
public :: hello2
!
contains
!
  subroutine hello2()
    print *,"Hello World!, from componentHello2:hello"
  end subroutine hello2
!
end module componentHello2
