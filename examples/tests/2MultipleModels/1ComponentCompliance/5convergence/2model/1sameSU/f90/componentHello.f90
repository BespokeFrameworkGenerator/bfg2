module componentHello
!
implicit none
private
public :: init,hello
integer::count=4
!
contains
!
  subroutine init(enter_loop)
  logical :: enter_loop
  enter_loop=.true.
  end subroutine init
!
  subroutine hello(not_converged)
    logical :: not_converged
    count=count-1
    print *,"Hello World!, count=",count
    not_converged=.true.
    if (count==0) then
      print *,"Converged"
      not_converged=.false.
    end if
  end subroutine hello
!
end module componentHello
