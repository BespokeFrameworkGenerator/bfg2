BFGCHECK=${BFG2HOME}/bin/bfg2check.py
BFGGEN=${BFG2HOME}/bin/runbfg2.py
BFGDOTGEN=${BFG2HOME}/bin/bfg2dotgen.py
MKFILEGEN=${BFG2HOME}/bin/bfg2makefilegen.py
# Provide default Makefile and coupled document names
MAKEFILE=Makefile
BFG2COUPLED=coupled.xml
all:
	make -f ${MAKEFILE} gen
	make -f ${MAKEFILE} build
	make -f ${MAKEFILE} run
gen:
	${BFGGEN} ${BFG2COUPLED}
build:
	${MKFILEGEN} -o Makefile.bfg2 ${BFG2COUPLED}
	make -f Makefile.bfg2
clean:
	make -f Makefile.bfg2 clean
check:
	${BFGCHECK} ${BFG2COUPLED}
vis:
	${BFGDOTGEN} ${BFG2COUPLED}
	dot -Tpng CoupledSchedule.dot -o CoupledSchedule.png
	dot -Tpng CoupledGraph.dot -o CoupledGraph.png
	eog CoupledSchedule.png CoupledGraph.png
cleanall:
	make -f ${MAKEFILE} clean
	rm -f Makefile.bfg2 *.o *.mod BFG2* exe1 *.png *.dot *.pyc *.h
