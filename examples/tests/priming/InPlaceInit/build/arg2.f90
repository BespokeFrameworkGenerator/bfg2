module arg2
use bfg2
private
public ts
contains

  subroutine ts(r3a,r3b,r3c)
  real,intent(inout) :: r3a,r3b,r3c
  real :: r3d,r3e
  call get(r3d,4)
  call get(r3e,5)
  print *,"Hello from module arg1 subroutine ts"
  print *,"Args are r3a,r3b,r3c,r3d,r3e",r3a,r3b,r3c,r3d,r3e
  end subroutine ts

end module arg2
