module BFG2
!
  use bfg2target1, getActiveEP=>getActiveModel
  use mpi
  implicit none
  private 
  public :: get,put
  interface get
    module procedure getreal__0
    module procedure getlogical__0
  end interface
  interface put
    module procedure putreal__0
  end interface
!
  contains
!
    subroutine getlogical__0(arg1,arg2)
      implicit none
      logical,intent(out) :: arg1
      integer,intent(in) :: arg2
      
      HELLO FROM OASIS4 ROUTINE
      integer :: currentEP,mysize
      integer,save :: prime(1)
      data prime /.true./
      currentEP=getActiveEP()
      if (currentEP==3) then
        if (arg2==11) then
          ! local data : inplace1.ts.0.11
          mysize=1
          if (prime(1)) then
            prime(1)=.false.
            arg1=.true.
          else
            ! I am not connected to anything
          end if
        end if
      end if
    end subroutine getlogical__0
!
    subroutine getreal__0(arg1,arg2)
      implicit none
      real,intent(out) :: arg1
      integer,intent(in) :: arg2
      
      HELLO FROM OASIS4 ROUTINE
      integer :: currentEP,mysize
      integer,save :: prime(10)
      data prime /.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true./
      currentEP=getActiveEP()
      if (currentEP==3) then
        if (arg2==1) then
          ! local data : inplace1.ts.0.1
          mysize=1
          if (prime(1)) then
            prime(1)=.false.
            ! I am internally primed
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==2) then
          ! local data : inplace1.ts.0.2
          mysize=1
          if (prime(2)) then
            prime(2)=.false.
            arg1=3.14
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==3) then
          ! local data : inplace1.ts.0.3
          mysize=1
          if (prime(3)) then
            prime(3)=.false.
            ! TBD file load goes here
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==4) then
          ! local data : inplace1.ts.0.4
          mysize=1
          if (prime(4)) then
            prime(4)=.false.
            ! TBD file load goes here
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==5) then
          ! local data : inplace1.ts.0.5
          mysize=1
          if (prime(5)) then
            prime(5)=.false.
            HELLO FROM OASIS4 ROUTINE
          else
            ! I am not connected to anything
          end if
        end if
      end if
      if (currentEP==2) then
        if (arg2==1) then
          ! local data : inplace1.init2.0.1
          mysize=1
          if (prime(6)) then
            prime(6)=.false.
            HELLO FROM OASIS4 ROUTINE
          else
            ! I am not connected to anything
          end if
        end if
      end if
      if (currentEP==1) then
        if (arg2==1) then
          ! local data : inplace1.init1.0.1
          mysize=1
          if (prime(7)) then
            prime(7)=.false.
            ! I am internally primed
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==2) then
          ! local data : inplace1.init1.0.2
          mysize=1
          if (prime(8)) then
            prime(8)=.false.
            arg1=3.14
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==3) then
          ! local data : inplace1.init1.0.3
          mysize=1
          if (prime(9)) then
            prime(9)=.false.
            ! TBD file load goes here
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==4) then
          ! local data : inplace1.init1.0.4
          mysize=1
          if (prime(10)) then
            prime(10)=.false.
            ! TBD file load goes here
          else
            ! I am not connected to anything
          end if
        end if
      end if
    end subroutine getreal__0
!
    subroutine putreal__0(arg1,arg2)
      implicit none
      real,intent(in) :: arg1
      integer,intent(in) :: arg2
      
      HELLO FROM OASIS4 ROUTINE
      integer :: currentEP,mysize
      integer,save :: prime(2)
      data prime /.true.,.true./
      currentEP=getActiveEP()
      if (currentEP==2) then
        if (arg2==2) then
          ! local data : inplace1.init2.0.2
          mysize=1
          if (prime(1)) then
            prime(1)=.false.
            HELLO FROM OASIS4 ROUTINE
          else
            ! I am not connected to anything
          end if
        end if
      end if
      if (currentEP==1) then
        if (arg2==5) then
          ! local data : inplace1.init1.0.5
          mysize=1
          if (prime(2)) then
            prime(2)=.false.
            HELLO FROM OASIS4 ROUTINE
          else
            ! I am not connected to anything
          end if
        end if
      end if
    end subroutine putreal__0
!
end module BFG2
