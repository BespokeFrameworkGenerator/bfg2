       module BFG2Target1
       ! oasis4IncludeTarget
       use prism
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(2)
       integer :: b2mmap(2)
       ! Constant declarations
       !name_len_max
       integer, parameter :: name_len_max=256
       !ndim
       integer, parameter :: ndim=3
       !ep_instance_count
       integer, parameter :: ep_instance_count=5
       ! Derived type definitions
       type shape_type
       !bounds
       integer, dimension(2,ndim) :: bounds
       end type shape_type
       type coords_type
       !longitudes
       real, allocatable, dimension(:) :: longitudes
       !latitudes
       real, allocatable, dimension(:) :: latitudes
       !verticals
       real, allocatable, dimension(:) :: verticals
       end type coords_type
       type corners_type
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type corners_type
       type points_type
       !name
       character, dimension(name_len_max) :: name
       !id
       integer :: id
       !actual_shape
       type(shape_type) :: actual_shape
       !coords
       type(coords_type) :: coords
       end type points_type
       type mask_type
       !id
       integer :: id
       !actual_shape
       type(shape_type) :: actual_shape
       !array
       logical, allocatable, dimension(:,:) :: array
       end type mask_type
       type grid_type
       !name
       character, dimension(name_len_max) :: name
       !id
       integer :: id
       !type_id
       integer :: type_id
       !valid_shape
       type(shape_type) :: valid_shape
       !corners
       type(corners_type) :: corners
       !point_sets
       type(points_type), dimension(1) :: point_sets
       !landmask
       type(mask_type) :: landmask
       end type grid_type
       type coupling_field_ep_instance_type
       !get_instance
       type(coupling_field_arg_instance_type), pointer :: get_instance
       !put_instances
       type(coupling_field_arg_instance_type), pointer :: put_instances
       end type coupling_field_ep_instance_type
       type coupling_field_arg_instance_type
       !prism_id
       integer :: prism_id
       !bfg_ref
       integer :: bfg_ref
       !msg_tag
       character, dimension(name_len_max) :: msg_tag
       !next_instance
       type(coupling_field_arg_instance_type), pointer :: next_instance
       end type coupling_field_arg_instance_type
       type coupling_field_type
       !ep_instances
       type(coupling_field_ep_instance_type), dimension(ep_instance_count) :: ep_instances
       !bfg_id
       character, dimension(name_len_max) :: bfg_id
       !name
       character, dimension(name_len_max) :: name
       !nodims
       integer, dimension(2) :: nodims
       !actual_shape
       type(shape_type) :: actual_shape
       !type_id
       integer :: type_id
       end type coupling_field_type
       type component_type
       !name
       character, dimension(name_len_max) :: name
       !id
       integer :: id
       !local_comm
       integer :: local_comm
       !gridded_grids
       type(grid_type), allocatable, dimension(:) :: gridded_grids
       !gridless_grids
       type(grid_type), allocatable, dimension(:) :: gridless_grids
       !coupling_fields
       type(coupling_field_type), allocatable, dimension(:) :: coupling_fields
       !coupling_fields_count
       integer :: coupling_fields_count
       end type component_type
       type coupling_field_key_type
       !coupling_field_no
       integer :: coupling_field_no
       end type coupling_field_key_type
       ! Variable declarations
       ! Coupling field keys for this deployment unit
       !component
       type(component_type), target :: component
       !model_time
       type(PRISM_Time_Struct) :: model_time
       !model_time_bounds
       type(PRISM_Time_Struct), dimension(2) :: model_time_bounds
       ! Current rank, and all ranks for this deployment unit
       !my_local_rank
       integer :: my_local_rank
       !SU1_rank
       integer :: SU1_rank
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer  , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function inplace1Thread()
       implicit none
       if (bfgSUID==1) then
       inplace1Thread=.true.
       else
       inplace1Thread=.false.
       end if
       end function inplace1Thread
       subroutine commsSync()
       implicit none
       ! oasis4CommsSync
       !ierror
       integer :: ierror
       call mpi_barrier(component%local_comm,ierror)
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       use prism
       use FoX_wkml
       implicit none
       !kml_file_handle
       type(xmlf_t) :: kml_file_handle
       !RANK_UNKNOWN
       integer, parameter :: RANK_UNKNOWN=-1
       !ierror
       integer :: ierror
       !comp_loop
       integer :: comp_loop
       !points_loop
       integer :: points_loop
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       !grid
       type(grid_type), pointer :: grid
       !points
       type(points_type), pointer :: points
       !corners
       type(corners_type), pointer :: corners
       !mask
       type(mask_type), pointer :: mask
       !coord_array
       real, dimension(:), pointer :: coord_array
       !corners_longitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_longitudes_OASIS4
       !corners_latitudes_OASIS4
       real, allocatable, dimension(:,:) :: corners_latitudes_OASIS4
       !corners_verticals_OASIS4
       real, allocatable, dimension(:,:) :: corners_verticals_OASIS4
       !all_coords_array
       real, allocatable, dimension(:) :: all_coords_array
       !index_loop
       integer :: index_loop
       !coord_loop
       integer :: coord_loop
       !lon_loop
       integer :: lon_loop
       !lat_loop
       integer :: lat_loop
       !appl_name
       character, dimension(name_len_max) :: appl_name
       !local_comm
       integer :: local_comm
       !rank_lists
       integer, dimension(1,3) :: rank_lists
       ! Set a PRISM application name for this deployment unit
       appl_name='DU1'
       ! Initialise the coupling environment (must be called by each process)
       call prism_init(trim(appl_name),ierror)
       ! Get the rank for the current process
       ! This rank is relative to the current deployment unit - it is not global
       call prism_get_localcomm(PRISM_appl_id,local_comm,ierror)
       call MPI_Comm_rank(local_comm,my_local_rank,ierror)
       ! Reset all sequence unit ranks
       b2mmap=RANK_UNKNOWN
       ! Get the ranks for all component processes
       call prism_get_ranklists('SU1',1,rank_lists,ierror)
       SU1_rank=rank_lists(1,1)
       ! Store mapping from sequence unit number to local rank
       b2mmap(1)=SU1_rank
       ! Initialise the PRISM component
       if (my_local_rank==SU1_rank) then
       ! Assign sequence unit number (unique across deployment units)
       bfgSUID=1
       component%name='SU1'
       call prism_init_comp(component%id,trim(component%name),ierror)
       call prism_get_localcomm(component%id,component%local_comm,ierror)
       ! Initialise grids and coupling data for component: SU1
       component%coupling_fields_count=0
       end if
       ! Initialisation phase is complete
       call prism_enddef(ierror)
       ! Set the date/time bounds within which coupling will be valid
       model_time=PRISM_jobstart_date
       model_time_bounds(1)=model_time
       model_time_bounds(2)=model_time
       call prism_calc_newdate(model_time_bounds(1),-3600.0,ierror)
       call prism_calc_newdate(model_time_bounds(2),3600.0,ierror)
       end subroutine initComms
       subroutine finaliseComms()
       use prism
       implicit none
       !ierror
       integer :: ierror
       !coupling_field_loop
       integer :: coupling_field_loop
       !ep_instance_loop
       integer :: ep_instance_loop
       !coupling_field
       type(coupling_field_type), pointer :: coupling_field
       !coupling_field_ep_inst
       type(coupling_field_ep_instance_type), pointer :: coupling_field_ep_inst
       !coupling_field_arg_inst
       type(coupling_field_arg_instance_type), pointer :: coupling_field_arg_inst
       call prism_terminate(ierror)
       ! Deallocate lists of coupling field instances
       do coupling_field_loop=1,component%coupling_fields_count
       coupling_field=>component%coupling_fields(coupling_field_loop)
       do ep_instance_loop=1,ep_instance_count
       coupling_field_ep_inst=>coupling_field%ep_instances(ep_instance_loop)
       if(associated(coupling_field_ep_inst%get_instance))deallocate(coupling_field_ep_inst%get_instance)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       do while(associated(coupling_field_arg_inst))
       coupling_field_ep_inst%put_instances=>coupling_field_arg_inst%next_instance
       if(associated(coupling_field_arg_inst))deallocate(coupling_field_arg_inst)
       coupling_field_arg_inst=>coupling_field_ep_inst%put_instances
       end do
       end do
       end do
       ! Deallocate the arrays of coupling fields and grids
       if(allocated(component%coupling_fields))deallocate(component%coupling_fields)
       if(allocated(component%gridless_grids))deallocate(component%gridless_grids)
       if(allocated(component%gridded_grids))deallocate(component%gridded_grids)
       end subroutine finaliseComms
       end module BFG2Target1
