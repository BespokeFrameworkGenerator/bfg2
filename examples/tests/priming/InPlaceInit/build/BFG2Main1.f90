       program BFG2Main
       use netcdf
       use BFG2Target1
       use inplace1, only : inplace1_init1_init=>init1,&
inplace1_init2_init=>init2,&
inplace1_ts_iteration=>ts,&
inplace1_finalise_final=>finalise
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !inplace1__freq
       integer :: inplace1__freq
       !inplace2__freq
       integer :: inplace2__freq
       namelist /time/ nts1,inplace1__freq,inplace2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=1012,file='BFG2Control.nam')
       read(1012,time)
       close(1012)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(inplace1Thread())then
       call nmlinit11(r3)
       end if
       ! netcdf files
       ! End initial values file read
       if (inplace1Thread()) then
       call setActiveModel(1)
       call inplace1_init1_init()
       end if
       if (inplace1Thread()) then
       call setActiveModel(2)
       call inplace1_init2_init()
       end if
       do its1=1,nts1
       if (inplace1Thread()) then
       if(mod(its1,inplace1__freq).eq.0)then
       call setActiveModel(3)
       call inplace1_ts_iteration()
       end if
       end if
       call commsSync()
       end do
       if (inplace1Thread()) then
       call setActiveModel(5)
       call inplace1_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit11(r2a)
        implicit none
        :: r2a
       namelist /data/ r2a
       open(unit=11,file='bfg2Data.in')
       read(11,data)
       close(11)
       end subroutine nmlinit11
