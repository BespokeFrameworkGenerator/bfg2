module arg1
use bfg2
private
public init1,init2,ts,finalise
contains

  subroutine init1()
  real :: r1a,r1b,r1e
  real :: r1c,r1d
  call get(r1a,1)
  call get(r1b,2)
  call get(r1e,5)
  print *,"Hello from module arg1 subroutine init1"
  print *,"inplace are r1a,r1b,r1c,r1d,r1e",r1a,r1b,r1c,r1d,r1e
  print *,"Internally setting r1c and r1d to 3.14"
  r1c=3.14
  r1d=3.14
  call put(r1d,4)
  call put(r1e,5)
  end subroutine init1

  subroutine init2(r2c)
  real,intent(inout) :: r2c
  real :: r1c
  real :: r3c
  call get(r1c,1)
  print *,"Hello from module arg1 subroutine init2"
  print *,"Args are r1c,r2c,r3c",r1c,r2c,r3c
  print *,"Internally setting r2c and r3c to 3.14"
  r2c=3.14
  r3c=3.14
  call put(r3c,3)
  end subroutine init2

  subroutine ts(r2c,r3a,r3b,r3c,r3d,r3e)
  real,intent(inout) :: r2c,r3a,r3b,r3c,r3d,r3e
  real :: r2a,r2b,r2d,r2e
  logical :: first
  call get(r2a,1)
  call get(r2b,2)
  call get(r2e,5)
  call get(first,11)
  print *,"Hello from module arg1 subroutine ts"
  print *,"Args are r2a,r2b,r2c,r2d,r2e,r3a,r3b,r3c,r3d,r3e",&
          r2a,r2b,r2c,r2d,r2e,r3a,r3b,r3c,r3d,r3e
  if (first) then
    print *,"Internally setting r2d and r3d to 3.14 on first call"
    first=.false.
    r2d=3.14
    r3d=3.14
  end if
  call put(r2d,4)
  end subroutine ts

  subroutine finalise()
  print *,"Hello from module arg1 subroutine finalise"
  print *,"I have no args"
  end subroutine 

end module arg1
