module BFG2
!
  use bfg2target1, getActiveEP=>getActiveModel
  use mpi
  implicit none
  private 
  public :: get,put
  interface get
    module procedure getreal__0
    module procedure getlogical__0
  end interface
  interface put
    module procedure putreal__0
  end interface
!
  contains
!
    subroutine getlogical__0(arg1,arg2)
      implicit none
      logical,intent(in) :: arg1
      integer,intent(in) :: arg2
      
      integer :: istatus(mpi_status_size),ierr
      integer :: currentEP,mysize
      integer,save :: prime(1)
      data prime /.true./
      currentEP=getActiveEP()
      if (currentEP==3) then
        if (arg2==11) then
          ! local data : arg1.ts..11
          mysize=1
          if (prime(1)) then
            prime(1)=.false.
            arg1=.true.
          else
            ! I am not connected to anything
          end if
        end if
      end if
    end subroutine getlogical__0
!
    subroutine getreal__0(arg1,arg2)
      implicit none
      real,intent(in) :: arg1
      integer,intent(in) :: arg2
      
      integer :: istatus(mpi_status_size),ierr
      integer :: currentEP,mysize
      integer,save :: prime(10)
      data prime /.true.,.true.,.true.,.true.,.true.,.false.,.false.,.true.,.true.,.true./
      currentEP=getActiveEP()
      if (currentEP==2) then
        if (arg2==2) then
          ! local data : arg1.init2..2
          mysize=1
          if (prime(1)) then
            prime(1)=.false.
            ! I am internally primed
          else
            ! I am not connected to anything
          end if
        end if
      end if
      if (currentEP==3) then
        if (arg2==7) then
          ! local data : arg1.ts..7
          mysize=1
          if (prime(2)) then
            prime(2)=.false.
            ! file load goes here
          else
            call mpi_recv(arg1,mysize,mpi_real,b2mmap(1),10,mpi_comm_world,istatus,ierr)
          end if
        end if
        if (arg2==8) then
          ! local data : arg1.ts..8
          mysize=1
          if (prime(3)) then
            prime(3)=.false.
            call mpi_recv(arg1,mysize,mpi_real,b2mmap(1),11,mpi_comm_world,istatus,ierr)
          else
            call mpi_recv(arg1,mysize,mpi_real,b2mmap(1),11,mpi_comm_world,istatus,ierr)
          end if
        end if
        if (arg2==9) then
          ! local data : arg1.ts..9
          mysize=1
          if (prime(4)) then
            prime(4)=.false.
            ! I am internally primed
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==10) then
          ! local data : arg1.ts..10
          mysize=1
          if (prime(5)) then
            prime(5)=.false.
            arg1=3.14
          else
            ! I am not connected to anything
          end if
        end if
      end if
      if (currentEP==4) then
        if (arg2==4) then
          ! local data : arg2.ts..4
          mysize=1
          if (prime(6)) then
            prime(6)=.false.
            arg1=3.14
          else
            call mpi_recv(arg1,mysize,mpi_real,b2mmap(1),24,mpi_comm_world,istatus,ierr)
          end if
        end if
        if (arg2==5) then
          ! local data : arg2.ts..5
          mysize=1
          if (prime(7)) then
            prime(7)=.false.
            arg1=3.14
          else
            call mpi_recv(arg1,mysize,mpi_real,b2mmap(1),25,mpi_comm_world,istatus,ierr)
          end if
        end if
      end if
      if (currentEP==1) then
        if (arg2==1) then
          ! local data : arg1.init1..1
          mysize=1
          if (prime(8)) then
            prime(8)=.false.
            ! I am internally primed
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==2) then
          ! local data : arg1.init1..2
          mysize=1
          if (prime(9)) then
            prime(9)=.false.
            ! file load goes here
          else
            ! I am not connected to anything
          end if
        end if
        if (arg2==5) then
          ! local data : arg1.init1..5
          mysize=1
          if (prime(10)) then
            prime(10)=.false.
            arg1=3.14
          else
            ! I am not connected to anything
          end if
        end if
      end if
    end subroutine getreal__0
!
    subroutine putreal__0(arg1,arg2)
      implicit none
      real,intent(inout) :: arg1
      integer,intent(in) :: arg2
      
      integer :: istatus(mpi_status_size),ierr
      integer :: currentEP,mysize
      currentEP=getActiveEP()
      if (currentEP==4) then
        if (arg2==2) then
          ! local data : arg2.ts..2
          mysize=1
          call mpi_send(arg1,mysize,mpi_real,b2mmap(1),10,mpi_comm_world,ierr)
        end if
        if (arg2==3) then
          ! local data : arg2.ts..3
          mysize=1
          call mpi_send(arg1,mysize,mpi_real,b2mmap(1),11,mpi_comm_world,ierr)
        end if
      end if
      if (currentEP==3) then
        if (arg2==5) then
          ! local data : arg1.ts..5
          mysize=1
          call mpi_send(arg1,mysize,mpi_real,b2mmap(1),24,mpi_comm_world,ierr)
        end if
        if (arg2==6) then
          ! local data : arg1.ts..6
          mysize=1
          call mpi_send(arg1,mysize,mpi_real,b2mmap(1),25,mpi_comm_world,ierr)
        end if
      end if
    end subroutine putreal__0
!
end module BFG2
