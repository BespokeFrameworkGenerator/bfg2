       program BFG2Main
       use netcdf
       use BFG2Target1
       use arg1, only : arg1_init1_init=>init1,&
arg1_init2_init=>init2,&
arg1_ts_iteration=>ts,&
arg1_finalise_final=>finalise
       use arg2, only : arg2_ts_iteration=>ts
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !arg1__freq
       integer :: arg1__freq
       !arg2__freq
       integer :: arg2__freq
       namelist /time/ nts1,arg1__freq,arg2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !
       real :: r6
       !
       real :: r12
       !
       real :: r15
       !
       real :: r16
       !
       real :: r17
       !
       real :: r18
       !
       real :: r19
       !
       real :: r21
       !
       real :: r22
       !
       real :: r23
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
           r18=3.14
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(arg1Thread())then
       call nmlinit11(r12,r19)
       end if
       ! netcdf files
       ! End initial values file read
       if (arg1Thread()) then
       call setActiveModel(1)
       call arg1_init1_init()
       end if
       if (arg1Thread()) then
       call setActiveModel(2)
       call arg1_init2_init(r6)
       end if
       do its1=1,nts1
       if (arg1Thread()) then
       if(mod(its1,arg1__freq).eq.0)then
       call setActiveModel(3)
       call arg1_ts_iteration(r12,r15,r16,r17,r18,r19)
       r21=r15
       r22=r16
       r23=r17
       end if
       end if
       if (arg2Thread()) then
       if(mod(its1,arg2__freq).eq.0)then
       call setActiveModel(4)
       call arg2_ts_iteration(r21,r22,r23)
       r19=r21
       end if
       end if
       call commsSync()
       end do
       if (arg1Thread()) then
       call setActiveModel(5)
       call arg1_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit11(r2a,r3a)
        implicit none
       real :: r2a
       real :: r3a
       namelist /data/ r2a,r3a
       open(unit=11,file='bfg2Data.in')
       read(11,data)
       close(11)
       end subroutine nmlinit11
