       program BFG2Main
       use netcdf
       use BFG2Target1
       use arg1, only : arg1_init1_init=>init1,&
arg1_init2_init=>init2,&
arg1_ts_iteration=>ts,&
arg1_finalise_final=>finalise
       use arg2, only : arg2_ts_iteration=>ts
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !arg1__freq
       integer :: arg1__freq
       !arg2__freq
       integer :: arg2__freq
       namelist /time/ nts1,arg1__freq,arg2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !
       real :: r1
       !
       real :: r2
       !
       real :: r3
       !
       real :: r4
       !
       real :: r5
       !
       real :: r6
       !
       real :: r7
       !
       real :: r8
       !
       real :: r10
       !
       real :: r11
       !
       real :: r12
       !
       real :: r13
       !
       real :: r14
       !
       real :: r15
       !
       real :: r16
       !
       real :: r17
       !
       real :: r18
       !
       real :: r19
       !
       logical :: r20
       !
       real :: r21
       !
       real :: r22
       !
       real :: r23
       !
       real :: r24
       !
       real :: r25
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
           r5=3.14
           r14=3.14
           r19=3.14
           r20=.true.
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       if(arg1Thread())then
       call nmlinit11(r1,r10,r15)
       end if
       ! netcdf files
       ! End initial values file read
       if (arg1Thread()) then
       call setActiveModel(1)
       call arg1_init1_init(r1,r2,r3,r4,r5)
       r6=r3
       end if
       if (arg1Thread()) then
       call setActiveModel(2)
       call arg1_init2_init(r6,r7,r8)
       r12=r7
       r17=r8
       end if
       do its1=1,nts1
       if (arg1Thread()) then
       if(mod(its1,arg1__freq).eq.0)then
       call setActiveModel(3)
       call arg1_ts_iteration(r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20)
       r21=r15
       r22=r16
       r23=r17
       r24=r18
       r25=r19
       end if
       end if
       if (arg2Thread()) then
       if(mod(its1,arg2__freq).eq.0)then
       call setActiveModel(4)
       call arg2_ts_iteration(r21,r22,r23,r24,r25)
       r15=r21
       r16=r22
       r17=r23
       end if
       end if
       call commsSync()
       end do
       if (arg1Thread()) then
       call setActiveModel(5)
       call arg1_finalise_final()
       end if
       call finaliseComms()
       end program BFG2Main


       subroutine nmlinit11(r1a,r2a,r3a)
        implicit none
       real :: r1a
       real :: r2a
       real :: r3a
       namelist /data/ r1a,r2a,r3a
       open(unit=11,file='bfg2Data.in')
       read(11,data)
       close(11)
       end subroutine nmlinit11
