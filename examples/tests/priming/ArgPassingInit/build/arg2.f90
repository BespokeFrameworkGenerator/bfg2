module arg2
private
public ts
contains

  subroutine ts(r3a,r3b,r3c,r3d,r3e)
  real,intent(in) :: r3a,r3b,r3c,r3d,r3e
  print *,"Hello from module arg1 subroutine ts"
  print *,"Args are r3a,r3b,r3c,r3d,r3e",r3a,r3b,r3c,r3d,r3e
  end subroutine ts

end module arg2
