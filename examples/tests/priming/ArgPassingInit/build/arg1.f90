module arg1
private
public init1,init2,ts,finalise
contains

  subroutine init1(r1a,r1b,r1c,r1d,r1e)
  real,intent(in)  :: r1a,r1b,r1e
  real,intent(out) :: r1c,r1d
  print *,"Hello from module arg1 subroutine init1"
  print *,"Args are r1a,r1b,r1c,r1d,r1e",r1a,r1b,r1c,r1d,r1e
  print *,"Internally setting r1c and r1d to 3.14"
  r1c=3.14
  r1d=3.14
  end subroutine init1

  subroutine init2(r1c,r2c,r3c)
  real,intent(in)  :: r1c
  real,intent(out) :: r2c,r3c
  print *,"Hello from module arg1 subroutine init2"
  print *,"Args are r1c,r2c,r3c",r1c,r2c,r3c
  print *,"Internally setting r2c and r3c to 3.14"
  r2c=3.14
  r3c=3.14
  end subroutine init2

  subroutine ts(r2a,r2b,r2c,r2d,r2e,r3a,r3b,r3c,r3d,r3e,first)
  logical,intent(inout) :: first
  real,intent(in) :: r2a,r2b,r2c,r2e,r3a,r3b,r3c,r3e
  real,intent(out) :: r2d,r3d
  print *,"Hello from module arg1 subroutine ts"
  print *,"Args are r2a,r2b,r2c,r2d,r2e,r3a,r3b,r3c,r3d,r3e",&
          r2a,r2b,r2c,r2d,r2e,r3a,r3b,r3c,r3d,r3e
  if (first) then
    print *,"Internally setting r2d and r3d to 3.14 on first call"
    first=.false.
    r2d=3.14
    r3d=3.14
  end if
  end subroutine ts

  subroutine finalise()
  print *,"Hello from module arg1 subroutine finalise"
  print *,"I have no args"
  end subroutine 

end module arg1
