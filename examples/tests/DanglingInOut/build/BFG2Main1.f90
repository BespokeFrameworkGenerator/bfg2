       program BFG2Main
       use BFG2Target1
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !simpleArg1__freq
       integer :: simpleArg1__freq
       !simpleArg2__freq
       integer :: simpleArg2__freq
       namelist /time/ nts1,simpleArg1__freq,simpleArg2__freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !
       real :: r1
       !
       real :: r2
       !
       real :: r3
       !
       real :: r4
       !
       real :: r5
       ! Set Notation Vars
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(10,file='xxx.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (simpleArg1Thread()) then
       call setActiveModel(1)
       call simpleArg1_init_init(r1,r2,r3)
       r4=r1
       end if
       do its1=1,nts1
       if (simpleArg1Thread()) then
       if(mod(its1,simpleArg1__freq).eq.0)then
       call setActiveModel(2)
       call simpleArg1_ts_iteration(r4)
       r5=r4
       end if
       end if
       if (simpleArg2Thread()) then
       if(mod(its1,simpleArg2__freq).eq.0)then
       call setActiveModel(3)
       call simpleArg2_ts_iteration(r5)
       end if
       end if
       call commsSync()
       end do
       call finaliseComms()
       end program BFG2Main

