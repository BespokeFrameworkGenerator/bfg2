       module BFG2Target1
       use simpleArg1, only : simpleArg1_init_init=>init,&
simpleArg1_ts_iteration=>ts
       use simpleArg2, only : simpleArg2_ts_iteration=>ts
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer  , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function simpleArg1Thread()
       implicit none
       ! only one thread so always true
       simpleArg1Thread=.true.
       end function simpleArg1Thread
       logical function simpleArg2Thread()
       implicit none
       ! only one thread so always true
       simpleArg2Thread=.true.
       end function simpleArg2Thread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
