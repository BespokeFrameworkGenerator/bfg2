       program BFG2Main
       use BFG2Target1
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !nts2
       integer :: nts2
       !nts3
       integer :: nts3
       !bfg_averages__freq
       integer :: bfg_averages__freq
       !fixed_chemistry__freq
       integer :: fixed_chemistry__freq
       !fixed_icesheet__freq
       integer :: fixed_icesheet__freq
       !initialise_fixedicesheet_mod__freq
       integer :: initialise_fixedicesheet_mod__freq
       !slab_ocean__freq
       integer :: slab_ocean__freq
       !igcm_atmosphere__freq
       integer :: igcm_atmosphere__freq
       !slab_seaice__freq
       integer :: slab_seaice__freq
       !counter__freq
       integer :: counter__freq
       !counter_mod__freq
       integer :: counter_mod__freq
       !transformer1__freq
       integer :: transformer1__freq
       !transformer2__freq
       integer :: transformer2__freq
       !transformer3__freq
       integer :: transformer3__freq
       !transformer4__freq
       integer :: transformer4__freq
       !transformer5__freq
       integer :: transformer5__freq
       !transformer6__freq
       integer :: transformer6__freq
       !transformer7__freq
       integer :: transformer7__freq
       !counter_2_freq
       integer :: counter_2_freq
       !counter_3_freq
       integer :: counter_3_freq
       !counter_4_freq
       integer :: counter_4_freq
       !counter_5_freq
       integer :: counter_5_freq
       namelist /time/ nts1,nts2,nts3,bfg_averages__freq,fixed_chemistry__freq,fixed_icesheet__freq,initialise_fixedicesheet_mod__freq,slab_ocean__freq,igcm_atmosphere__freq,slab_seaice__freq,counter__freq,counter_mod__freq,transformer1__freq,transformer2__freq,transformer3__freq,transformer4__freq,transformer5__freq,transformer6__freq,transformer7__freq,counter_2_freq,counter_3_freq,counter_4_freq,counter_5_freq
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !alon1_ocn
       real, dimension(1:36) :: r7
       !alat1_ocn
       real, dimension(1:36) :: r8
       !alon1_sic
       real, dimension(1:36) :: r9
       !alat1_sic
       real, dimension(1:36) :: r10
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r12
       !netsolar_sic
       real, dimension(1:36,1:36) :: r13
       !netlong_ocn
       real, dimension(1:36,1:36) :: r15
       !netlong_sic
       real, dimension(1:36,1:36) :: r16
       !sensible_ocn
       real, dimension(1:36,1:36) :: r18
       !sensible_sic
       real, dimension(1:36,1:36) :: r19
       !latent_ocn
       real, dimension(1:36,1:36) :: r21
       !latent_sic
       real, dimension(1:36,1:36) :: r22
       !stressx_ocn
       real, dimension(1:36,1:36) :: r24
       !stressx_sic
       real, dimension(1:36,1:36) :: r25
       !stressy_ocn
       real, dimension(1:36,1:36) :: r27
       !stressy_sic
       real, dimension(1:36,1:36) :: r28
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r30
       !conductflux_sic
       real, dimension(1:36,1:36) :: r31
       !evap_ocn
       real, dimension(1:36,1:36) :: r33
       !evap_sic
       real, dimension(1:36,1:36) :: r34
       !precip_ocn
       real, dimension(1:36,1:36) :: r36
       !precip_sic
       real, dimension(1:36,1:36) :: r37
       !runoff_ocn
       real, dimension(1:36,1:36) :: r39
       !runoff_sic
       real, dimension(1:36,1:36) :: r40
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r42
       !waterflux_sic
       real, dimension(1:36,1:36) :: r43
       !seaicefrac_ocn
       real, dimension(1:36,1:36) :: r45
       !seaicefrac_sic
       real, dimension(1:36,1:36) :: r46
       !tstar_ocn
       real, dimension(1:36,1:36) :: r48
       !tstar_sic
       real, dimension(1:36,1:36) :: r49
       !albedo_ocn
       real, dimension(1:36,1:36) :: r51
       !albedo_sic
       real, dimension(1:36,1:36) :: r52
       !alon2_atm
       real, dimension(1:64) :: r108
       !alat2_atm
       real, dimension(1:32) :: r109
       !aboxedge1_lon_atm
       real, dimension(1:65) :: r110
       !aboxedge2_lon_atm
       real, dimension(1:65) :: r111
       !aboxedge1_lat_atm
       real, dimension(1:33) :: r112
       !aboxedge2_lat_atm
       real, dimension(1:33) :: r113
       !ilandmask2_atm
       integer, dimension(1:64,1:32) :: r115
       !land_tice_ice
       real, allocatable, dimension(:,:) :: r132
       !land_fxco2_atm
       real, allocatable, dimension(:,:) :: r133
       !hrlons_atm
       real, dimension(1:320) :: r146
       !hrlats_atm
       real, dimension(1:160) :: r147
       !hrlonsedge_atm
       real, dimension(1:321) :: r148
       !hrlatsedge_atm
       real, dimension(1:161) :: r149
       !u10m_atm
       real, dimension(1:64,1:32) :: r156
       !v10m_atm
       real, dimension(1:64,1:32) :: r157
       !q2m_atm
       real, dimension(1:64,1:32) :: r159
       !rh2m_atm
       real, dimension(1:64,1:32) :: r160
       !psigma
       real, dimension(1:7) :: r164
       !massair
       real, dimension(1:64,1:32,1:7) :: r167
       !ddt14co2
       real, dimension(1:64,1:32,1:7) :: r192
       !test_water_land
       real :: r236
       ! Set Notation Vars
       !t2m_atm
       real, dimension(1:64,1:32) :: r158
       !ocean_tstarinst_atm
       real, dimension(1:64,1:32) :: r284
       !surfdsigma
       real :: r163
       !land_evapinst_atm
       real, dimension(1:64,1:32) :: r225
       !conductflux_atm
       real, dimension(1:64,1:32) :: r291
       !ksic_loop
       integer :: r295
       !precip_atm
       real, dimension(1:64,1:32) :: r124
       !landsnowdepth_atm
       real, dimension(1:64,1:32) :: r139
       !ilandmask1_atm
       integer, dimension(1:64,1:32) :: r75
       !albedo_atm
       real, dimension(1:64,1:32) :: r121
       !netlong_atm_meansic
       real, dimension(1:64,1:32) :: r389
       !ocean_evapinst_atm
       real, dimension(1:64,1:32) :: r278
       !istep_sic
       integer :: r767
       !test_energy_ocean
       real :: r84
       !stressx_atm_meanocn
       real, dimension(1:64,1:32) :: r419
       !surf_tstarinst_atm
       real, allocatable, dimension(:,:) :: r355
       !land_runoff_atm
       real, dimension(1:64,1:32) :: r131
       !netsolar_atm
       real, dimension(1:64,1:32) :: r122
       !ocean_lowestlv_atm
       real, allocatable, dimension(:,:) :: r332
       !land_lowestlq_atm
       real, allocatable, dimension(:,:) :: r330
       !alat1_atm
       real, dimension(1:32) :: r107
       !klnd_loop
       integer :: r129
       !land_stressy_atm
       real, dimension(1:64,1:32) :: r229
       !surf_evap_atm
       real, dimension(1:64,1:32) :: r352
       !conductflux_atm_meanocn
       real, dimension(1:64,1:32) :: r404
       !precip_atm_meansic
       real, dimension(1:64,1:32) :: r395
       !land_sensible_atm
       real, dimension(1:64,1:32) :: r227
       !mass14co2
       real, dimension(1:64,1:32,1:7) :: r166
       !surf_salb_atm
       real, allocatable, dimension(:,:) :: r120
       !ch4_atm
       real, dimension(1:64,1:32) :: r63
       !ocean_lowestlp_atm
       real, allocatable, dimension(:,:) :: r336
       !atmos_lowestlp_atm
       real, dimension(1:64,1:32) :: r154
       !ocean_stressx_atm
       real, dimension(1:64,1:32) :: r281
       !latent_atm_meanocn
       real, dimension(1:64,1:32) :: r408
       !ocean_stressxinst_atm
       real, dimension(1:64,1:32) :: r276
       !atmos_dt_tim
       real :: r118
       !ocean_sensible_atm
       real, dimension(1:64,1:32) :: r280
       !iconv_che
       integer :: r189
       !alon1_atm
       real, dimension(1:64) :: r106
       !ocean_salb_atm
       real, dimension(1:64,1:32) :: r287
       !landsnowvegfrac_atm
       real, dimension(1:64,1:32) :: r138
       !ocean_sensibleinst_atm
       real, dimension(1:64,1:32) :: r275
       !runoff_atm_meanocn
       real, dimension(1:64,1:32) :: r427
       !surf_latent_atm
       real, dimension(1:64,1:32) :: r339
       !sensible_atm_meanocn
       real, dimension(1:64,1:32) :: r413
       !surf_sensible_atm
       real, dimension(1:64,1:32) :: r343
       !ocean_evap_atm
       real, dimension(1:64,1:32) :: r283
       !n2o_atm
       real, dimension(1:64,1:32) :: r62
       !atmos_lowestlv_atm
       real, dimension(1:64,1:32) :: r151
       !land_latentinst_atm
       real, dimension(1:64,1:32) :: r221
       !dt_write
       integer :: r59
       !ocean_stressyinst_atm
       real, dimension(1:64,1:32) :: r277
       !netlong_atm
       real, dimension(1:64,1:32) :: r123
       !ocean_lowestlt_atm
       real, allocatable, dimension(:,:) :: r333
       !surf_orog_atm
       real, dimension(1:64,1:32) :: r76
       !landicealbedo_atm
       real, dimension(1:64,1:32) :: r77
       !istep_lic
       integer :: r2120
       !water_flux_atmos
       real :: r144
       !land_niter_tim
       integer :: r116
       !test_water_ocean
       real :: r85
       !atmos_lowestlq_atm
       real, dimension(1:64,1:32) :: r153
       !stressx_atm_meansic
       real, dimension(1:64,1:32) :: r391
       !atmos_lowestlt_atm
       real, dimension(1:64,1:32) :: r152
       !precip_atm_meanocn
       real, dimension(1:64,1:32) :: r423
       !katm_loop
       integer :: r383
       !write_flag_sic
       logical :: r3
       !ocean_lowestlh_atm
       real, allocatable, dimension(:,:) :: r335
       !write_flag_atm
       logical :: r1
       !land_stressyinst_atm
       real, dimension(1:64,1:32) :: r224
       !latent_atm_meansic
       real, dimension(1:64,1:32) :: r381
       !land_lowestlt_atm
       real, allocatable, dimension(:,:) :: r329
       !waterflux_atm_meanocn
       real, dimension(1:64,1:32) :: r41
       !land_stressx_atm
       real, dimension(1:64,1:32) :: r228
       !co2_atm
       real, dimension(1:64,1:32) :: r61
       !ocean_lowestlu_atm
       real, allocatable, dimension(:,:) :: r331
       !fname_restart_main
       character(len=200) :: r58
       !surf_stressx_atm
       real, dimension(1:64,1:32) :: r346
       !istep_atm
       integer :: r316
       !evap_atm_meanocn
       real, dimension(1:64,1:32) :: r425
       !surf_qstar_atm
       real, allocatable, dimension(:,:) :: r361
       !land_rough_atm
       real, dimension(1:64,1:32) :: r232
       !istep_che
       integer :: r1669
       !landsnowicefrac_atm
       real, dimension(1:64,1:32) :: r137
       !outputdir_name
       character(len=200) :: r60
       !netsolar_atm_meansic
       real, dimension(1:64,1:32) :: r387
       !test_energy_seaice
       real :: r293
       !stressy_atm_meansic
       real, dimension(1:64,1:32) :: r393
       !istep_ocn
       integer :: r1218
       !seaicefrac_atm
       real, dimension(1:64,1:32) :: r290
       !ocean_lowestlq_atm
       real, allocatable, dimension(:,:) :: r334
       !test_water_seaice
       real :: r294
       !sensible_atm_meansic
       real, dimension(1:64,1:32) :: r385
       !iconv_ice
       integer :: r165
       !surfsigma
       real :: r162
       !temptop_atm
       real, dimension(1:64,1:32) :: r82
       !land_lowestlv_atm
       real, allocatable, dimension(:,:) :: r328
       !kocn_loop
       integer :: r397
       !glim_covmap
       real, dimension(1:64,1:32) :: r145
       !land_salb_atm
       real, allocatable, dimension(:,:) :: r134
       !tstar_atm
       real, dimension(1:64,1:32) :: r119
       !land_latent_atm
       real, dimension(1:64,1:32) :: r226
       !ocean_latent_atm
       real, dimension(1:64,1:32) :: r279
       !land_stressxinst_atm
       real, dimension(1:64,1:32) :: r223
       !seaicefrac_atm_meanocn
       real, dimension(1:64,1:32) :: r400
       !ocean_qstar_atm
       real, dimension(1:64,1:32) :: r286
       !surf_iter_tim_bfg_land
       integer :: r317
       !surf_iter_tim_bfg_ocean
       integer :: r768
       !ocean_latentinst_atm
       real, dimension(1:64,1:32) :: r274
       !ocean_niter_tim
       integer :: r117
       !surf_stressy_atm
       real, dimension(1:64,1:32) :: r349
       !land_evap_atm
       real, dimension(1:64,1:32) :: r230
       !lgraphics
       logical :: r125
       !landicefrac_atm
       real, dimension(1:64,1:32) :: r78
       !ilat1_atm
       integer :: r105
       !ocean_rough_atm
       real, dimension(1:64,1:32) :: r285
       !netsolar_atm_meanocn
       real, dimension(1:64,1:32) :: r415
       !genie_timestep
       real :: r57
       !ilon1_atm
       integer :: r104
       !atmos_lowestlh_atm
       real, allocatable, dimension(:,:) :: r130
       !land_tstarinst_atm
       real, dimension(1:64,1:32) :: r231
       !surf_rough_atm
       real, allocatable, dimension(:,:) :: r358
       !land_qstar_atm
       real, dimension(1:64,1:32) :: r233
       !atmos_lowestlu_atm
       real, dimension(1:64,1:32) :: r150
       !write_flag_ocn
       logical :: r2
       !land_sensibleinst_atm
       real, dimension(1:64,1:32) :: r222
       !stressy_atm_meanocn
       real, dimension(1:64,1:32) :: r421
       !netlong_atm_meanocn
       real, dimension(1:64,1:32) :: r417
       !land_lowestlu_atm
       real, allocatable, dimension(:,:) :: r327
       !ocean_stressy_atm
       real, dimension(1:64,1:32) :: r282
       !flag_land
       logical :: r128
       !flag_goldsic
       logical :: r127
       !flag_goldocn
       logical :: r126
       !glim_coupled
       logical :: r244
       !glim_snow_model
       logical :: r243
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       its2=0
       its3=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r284=0.0
           r163=0.0
           r225=0.0
           r291=0.0
           r295=6
           r124=0.0
           r139=0.0
           r75=0
           r121=0.0
           r389=0.0
           r278=0.0
           r767=0
           r84=0.0
           r419=0.0
           ! 355is allocatable; cannot be written to yet
           r131=0.0
           r122=0.0
           ! 332is allocatable; cannot be written to yet
           ! 330is allocatable; cannot be written to yet
           r107=0.0
           r129=1
           r229=0.0
           r352=0.0
           r404=0.0
           r395=0.0
           r227=0.0
           r166=0.0
           ! 120is allocatable; cannot be written to yet
           r63=0.0
           ! 336is allocatable; cannot be written to yet
           r154=0.0
           r281=0.0
           r408=0.0
           r276=0.0
           r118=0.0
           r280=0.0
           r189=0
           r106=0.0
           r287=0.0
           r138=0.0
           r275=0.0
           r427=0.0
           r339=0.0
           r413=0.0
           r343=0.0
           r283=0.0
           r62=0.0
           r151=0.0
           r221=0.0
           r59=720
           r277=0.0
           r123=0.0
           ! 333is allocatable; cannot be written to yet
           r76=0.0
           r77=0.0
           r2120=0
           r144=0.0
           r116=0
           r85=0.0
           r153=0.0
           r391=0.0
           r152=0.0
           r423=0.0
           r383=1
           r3=.false.
           ! 335is allocatable; cannot be written to yet
           r1=.true.
           r224=0.0
           r381=0.0
           ! 329is allocatable; cannot be written to yet
           r41=0.0
           r228=0.0
           r61=0.0
           ! 331is allocatable; cannot be written to yet
           r58='/home/armstroc/genie/genie-main/data/input/main_restart_0.nc'
           r346=0.0
           r316=0
           r425=0.0
           ! 361is allocatable; cannot be written to yet
           r232=0.0
           r1669=0
           r137=0.0
           r60='/home/armstroc/genie_output/genie_ig_sl_sl_noflux/main'
           r387=0.0
           r293=0.0
           r393=0.0
           r1218=0
           r290=0.0
           ! 334is allocatable; cannot be written to yet
           r294=0.0
           r385=0.0
           r165=0
           r162=0.0
           r82=0.0
           ! 328is allocatable; cannot be written to yet
           r397=48
           r145=0.0
           ! 134is allocatable; cannot be written to yet
           r119=0.0
           r226=0.0
           r279=0.0
           r223=0.0
           r400=0.0
           r286=0.0
           r317=0
           r768=0
           r274=0.0
           r117=0
           r349=0.0
           r230=0.0
           r125=.false.
           r78=0.0
           r105=32
           r285=0.0
           r415=0.0
           r57=3600.0
           r104=64
           ! 130is allocatable; cannot be written to yet
           r231=0.0
           ! 358is allocatable; cannot be written to yet
           r233=0.0
           r150=0.0
           r2=.true.
           r222=0.0
           r421=0.0
           r417=0.0
           ! 327is allocatable; cannot be written to yet
           r282=0.0
           r128=.false.
           r127=.false.
           r126=.false.
           r244=.true.
           r243=.false.
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (bfg_averagesThread()) then
       call setActiveModel(1)
       call bfg_averages_ini_averages_init(r1,r2,r3)
       end if
       if (initialise_fixedicesheet_modThread()) then
       call setActiveModel(2)
       call initialise_fixedicesheet_mod_initialise_fixedicesheet_init(r75,r76,r77,r78)
       end if
       if (fixed_chemistryThread()) then
       call setActiveModel(3)
       call initialise_fixedchem(r61,r62,r63)
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(4)
       call igcm_atmosphere_initialise_igcmsurf_init()
       end if
       if (igcm_atmosphereThread()) then
       call setActiveModel(5)
       if (.not.(allocated(r132))) then
       allocate(r132(1:r104,1:r105))
       end if
       if (.not.(allocated(r133))) then
       allocate(r133(1:r104,1:r105))
       end if
       if (.not.(allocated(r120))) then
       allocate(r120(1:r104,1:r105))
       r120=0.0
       end if
       if (.not.(allocated(r134))) then
       allocate(r134(1:r104,1:r105))
       r134=0.0
       end if
       if (.not.(allocated(r130))) then
       allocate(r130(1:r104,1:r105))
       r130=0.0
       end if
       call igcm_atmosphere_initialise_atmos_init(r104,r105,r106,r107,r108,r109,r110,r111,r112,r113,r75,r115,r116,r117,r118,r119,r120,r121,r122,r123,r124,r125,r126,r127,r128,r129,r130,r131,r132,r133,r134,r76,r78,r137,r138,r139,r77,r61,r62,r63,r144,r145,r146,r147,r148,r149)
       end if
       if (slab_seaiceThread()) then
       call setActiveModel(6)
       call initialise_slabseaice(r119,r121,r290,r291,r75,r293,r294,r295)
       end if
       if (slab_oceanThread()) then
       call setActiveModel(7)
       call initialise_slabocean(r119,r121,r290,r82,r75,r84,r85)
       end if
       do its1=1,nts1
       if (counterinst1Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(8)
       call counter(r316)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(9)
       call igcm_atmosphere_igcm3_adiab_iteration(r150,r151,r152,r153,r154,r130,r156,r157,r158,r159,r160,r76,r162,r163,r164,r165,r166,r167)
       end if
       end if
       if (transformer1Thread()) then
       if(mod(its1,transformer1__freq).eq.0)then
       call setActiveModel(10)
       if (.not.(allocated(r332))) then
       allocate(r332(1:r104,1:r105))
       r332=0.0
       end if
       if (.not.(allocated(r330))) then
       allocate(r330(1:r104,1:r105))
       r330=0.0
       end if
       if (.not.(allocated(r336))) then
       allocate(r336(1:r104,1:r105))
       r336=0.0
       end if
       if (.not.(allocated(r333))) then
       allocate(r333(1:r104,1:r105))
       r333=0.0
       end if
       if (.not.(allocated(r335))) then
       allocate(r335(1:r104,1:r105))
       r335=0.0
       end if
       if (.not.(allocated(r329))) then
       allocate(r329(1:r104,1:r105))
       r329=0.0
       end if
       if (.not.(allocated(r331))) then
       allocate(r331(1:r104,1:r105))
       r331=0.0
       end if
       if (.not.(allocated(r334))) then
       allocate(r334(1:r104,1:r105))
       r334=0.0
       end if
       if (.not.(allocated(r328))) then
       allocate(r328(1:r104,1:r105))
       r328=0.0
       end if
       if (.not.(allocated(r327))) then
       allocate(r327(1:r104,1:r105))
       r327=0.0
       end if
       call transformer1_new_transformer_1_iteration(r104,r105,r150,r151,r152,r153,r130,r154,r327,r328,r329,r330,r331,r332,r333,r334,r335,r336)
       end if
       end if
       do its2=1,nts2
       if (counter_modinst1Thread()) then
       if(mod(its2,counter_mod__freq).eq.0)then
       call setActiveModel(11)
       call counter_mod(r317,r116)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(12)
       call igcm_atmosphere_igcm_land_surflux_iteration(r316,r317,r116,r118,r162,r75,r122,r123,r124,r327,r328,r329,r330,r154,r221,r222,r223,r224,r225,r226,r227,r228,r229,r230,r231,r232,r233,r134,r131,r236,r78,r137,r138,r139,r77,r145,r243,r244)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its2,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(13)
       call igcm_atmosphere_igcm_land_blayer_iteration(r317,r116,r118,r163,r75,r222,r223,r224,r225,r327,r328,r329,r330,r154)
       end if
       end if
       end do
       do its3=1,nts3
       if (counter_modinst2Thread()) then
       if(mod(its3,counter_mod__freq).eq.0)then
       call setActiveModel(14)
       call counter_mod(r768,r117)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(15)
       call igcm_atmosphere_igcm_ocean_surflux_iteration(r316,r768,r117,r162,r75,r122,r123,r124,r331,r332,r333,r334,r154,r121,r119,r274,r275,r276,r277,r278,r279,r280,r281,r282,r283,r284,r285,r286,r287)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its3,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(16)
       call igcm_atmosphere_igcm_ocean_blayer_iteration(r768,r117,r118,r163,r75,r275,r276,r277,r278,r331,r332,r333,r334,r154)
       end if
       end if
       end do
       if (transformer2Thread()) then
       if(mod(its1,transformer2__freq).eq.0)then
       call setActiveModel(17)
       if (.not.(allocated(r355))) then
       allocate(r355(1:r104,1:r105))
       r355=0.0
       end if
       if (.not.(allocated(r361))) then
       allocate(r361(1:r104,1:r105))
       r361=0.0
       end if
       if (.not.(allocated(r358))) then
       allocate(r358(1:r104,1:r105))
       r358=0.0
       end if
       call transformer2_new_transformer_2_iteration(r104,r105,r339,r75,r226,r279,r343,r227,r280,r346,r228,r281,r349,r229,r282,r352,r230,r283,r355,r231,r284,r358,r232,r285,r361,r233,r286,r120,r134,r287,r150,r327,r331,r151,r328,r332,r152,r329,r333,r153,r330,r334)
       end if
       end if
       if (igcm_atmosphereThread()) then
       if(mod(its1,igcm_atmosphere__freq).eq.0)then
       call setActiveModel(18)
       call igcm_atmosphere_igcm3_diab_iteration(r150,r151,r152,r153,r120,r361,r355,r358,r339,r343,r346,r349,r122,r123,r124,r139,r158,r230,r61,r62,r63,r189,r144,r166,r192)
       end if
       end if
       if (transformer3Thread()) then
       if(mod(its1,transformer3__freq).eq.0)then
       call setActiveModel(19)
       call new_transformer_3(r104,r105,r381,r339,r383,r295,r385,r343,r387,r122,r389,r123,r391,r346,r393,r349,r395,r124,r397)
       end if
       end if
       if (counterinst2Thread()) then
       if(mod(its1,counter_2_freq).eq.0)then
       call setActiveModel(20)
       call counter(r767)
       end if
       end if
       if (slab_seaiceThread()) then
       if(mod(its1,slab_seaice__freq).eq.0)then
       call setActiveModel(21)
       call slabseaice(r767,r119,r381,r385,r387,r389,r339,r343,r122,r123,r391,r393,r290,r82,r291,r121,r75,r293,r294,r295)
       end if
       end if
       if (transformer4Thread()) then
       if(mod(its1,transformer4__freq).eq.0)then
       call setActiveModel(22)
       call transformer4_new_transformer_4_iteration(r104,r105,r400,r290,r295,r397,r404,r291)
       end if
       end if
       if (transformer5Thread()) then
       if(mod(its1,transformer5__freq).eq.0)then
       call setActiveModel(23)
       call transformer5_new_transformer_5_iteration(r104,r105,r408,r339,r290,r383,r397,r413,r343,r415,r122,r417,r123,r419,r346,r421,r349,r423,r124,r425,r352,r427,r131)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(24)
       call counter(r1218)
       end if
       end if
       if (slab_oceanThread()) then
       if(mod(its1,slab_ocean__freq).eq.0)then
       call setActiveModel(25)
       call slabocean(r1218,r119,r408,r413,r415,r417,r419,r421,r423,r425,r427,r290,r82,r404,r121,r75,r84,r85)
       end if
       end if
       if (counterinst4Thread()) then
       if(mod(its1,counter_4_freq).eq.0)then
       call setActiveModel(26)
       call counter(r1669)
       end if
       end if
       if (fixed_chemistryThread()) then
       if(mod(its1,fixed_chemistry__freq).eq.0)then
       call setActiveModel(27)
       call fixedchem(r1669,r61,r62,r63,r189)
       end if
       end if
       if (counterinst5Thread()) then
       if(mod(its1,counter_5_freq).eq.0)then
       call setActiveModel(28)
       call counter(r2120)
       end if
       end if
       if (fixed_icesheetThread()) then
       if(mod(its1,fixed_icesheet__freq).eq.0)then
       call setActiveModel(29)
       call fixedicesheet(r2120,r75,r76,r77,r78,r165)
       end if
       end if
       if (bfg_averagesThread()) then
       if(mod(its1,bfg_averages__freq).eq.0)then
       call setActiveModel(30)
       call bfg_averages_write_averages_iteration(r1218,r106,r107,r7,r8,r9,r10,r415,r12,r13,r417,r15,r16,r413,r18,r19,r408,r21,r22,r419,r24,r25,r421,r27,r28,r404,r30,r31,r425,r33,r34,r423,r36,r37,r427,r39,r40,r41,r42,r43,r400,r45,r46,r119,r48,r49,r121,r51,r52,r1,r2,r3,r397,r57,r58,r59,r60)
       end if
       end if
       if (transformer6Thread()) then
       if(mod(its1,transformer6__freq).eq.0)then
       call setActiveModel(31)
       call transformer6_new_transformer_6_iteration(r104,r105,r381,r385,r387,r389,r391,r393,r395)
       end if
       end if
       if (transformer7Thread()) then
       if(mod(its1,transformer7__freq).eq.0)then
       call setActiveModel(32)
       call transformer7_new_transformer_7_iteration(r104,r105,r408,r413,r415,r417,r419,r421,r423,r425,r427,r400,r404,r41)
       end if
       end if
       call commsSync()
       end do
       if (igcm_atmosphereThread()) then
       call setActiveModel(33)
       call igcm_atmosphere_end_atmos_final()
       end if
       call finaliseComms()
       end program BFG2Main

