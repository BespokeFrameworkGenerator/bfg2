       module BFG2Target1
       use weight_check, only : weight_check_weightcheck_init=>weightcheck
       use surflux, only : surflux_surflux_iteration=>surflux
       use embm, only : embm_initialise_embm_init=>initialise_embm,&
embm_embm_iteration=>embm,&
embm_end_embm_final=>end_embm
       use biogem, only : biogem_initialise_biogem_init=>initialise_biogem,&
biogem_biogem_iteration=>biogem,&
biogem_end_biogem_final=>end_biogem,&
biogem_rest_biogem_final=>rest_biogem,&
biogem_diag_biogem_timeslice_iteration=>diag_biogem_timeslice,&
biogem_diag_biogem_timeseries_iteration=>diag_biogem_timeseries
       use goldstein, only : goldstein_initialise_goldstein_init=>initialise_goldstein,&
goldstein_goldstein_iteration=>goldstein,&
goldstein_end_goldstein_final=>end_goldstein
       use atchem, only : atchem_initialise_atchem_init=>initialise_atchem,&
atchem_atchem_iteration=>atchem,&
atchem_end_atchem_final=>end_atchem,&
atchem_rest_atchem_final=>rest_atchem,&
atchem_cpl_comp_atmocn_iteration=>cpl_comp_atmocn,&
atchem_cpl_flux_ocnatm_iteration=>cpl_flux_ocnatm
       ! running in sequence so no includes required
       !bfgSUID
       integer :: bfgSUID
       !b2mmap(1)
       integer :: b2mmap(1)
       !activeModelID
       integer :: activeModelID
       !its1
       integer, target :: its1
       contains
       ! in sequence support routines start
       subroutine setActiveModel(idIN)
       implicit none
       integer , intent(in) :: idIN
       activeModelID=idIN
       end subroutine setActiveModel
       integer function getActiveModel()
       implicit none
       getActiveModel=activeModelID
       end function getActiveModel
       ! in sequence support routines end
       ! concurrency support routines start
       logical function goldsteinThread()
       implicit none
       ! only one thread so always true
       goldsteinThread=.true.
       end function goldsteinThread
       logical function embmThread()
       implicit none
       ! only one thread so always true
       embmThread=.true.
       end function embmThread
       logical function goldstein_seaiceThread()
       implicit none
       ! only one thread so always true
       goldstein_seaiceThread=.true.
       end function goldstein_seaiceThread
       logical function biogemThread()
       implicit none
       ! only one thread so always true
       biogemThread=.true.
       end function biogemThread
       logical function atcheminst1Thread()
       implicit none
       ! only one thread so always true
       atcheminst1Thread=.true.
       end function atcheminst1Thread
       logical function atcheminst2Thread()
       implicit none
       ! only one thread so always true
       atcheminst2Thread=.true.
       end function atcheminst2Thread
       logical function bfg_increment_genie_clockThread()
       implicit none
       ! only one thread so always true
       bfg_increment_genie_clockThread=.true.
       end function bfg_increment_genie_clockThread
       logical function surfluxThread()
       implicit none
       ! only one thread so always true
       surfluxThread=.true.
       end function surfluxThread
       logical function weight_checkThread()
       implicit none
       ! only one thread so always true
       weight_checkThread=.true.
       end function weight_checkThread
       logical function counterinst1Thread()
       implicit none
       ! only one thread so always true
       counterinst1Thread=.true.
       end function counterinst1Thread
       logical function counterinst2Thread()
       implicit none
       ! only one thread so always true
       counterinst2Thread=.true.
       end function counterinst2Thread
       logical function counterinst3Thread()
       implicit none
       ! only one thread so always true
       counterinst3Thread=.true.
       end function counterinst3Thread
       subroutine commsSync()
       implicit none
       ! running in sequence so no sync is required
       end subroutine commsSync
       ! concurrency support routines end
       subroutine initComms()
       implicit none
       ! nothing needed for sequential
       end subroutine initComms
       subroutine finaliseComms()
       implicit none
       ! nothing needed for sequential
       end subroutine finaliseComms
       end module BFG2Target1
