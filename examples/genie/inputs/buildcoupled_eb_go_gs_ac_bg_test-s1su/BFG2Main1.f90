       program BFG2Main
       use BFG2Target1
       ! Begin declaration of Control
       !nts1
       integer :: nts1
       !bfg_increment_genie_clock__freq
       integer :: bfg_increment_genie_clock__freq
       !counter__freq
       integer :: counter__freq
       !weight_check__freq
       integer :: weight_check__freq
       !surflux__freq
       integer :: surflux__freq
       !embm__freq
       integer :: embm__freq
       !biogem__freq
       integer :: biogem__freq
       !goldstein_seaice__freq
       integer :: goldstein_seaice__freq
       !atchem__freq
       integer :: atchem__freq
       !counter_1_freq
       integer :: counter_1_freq
       !counter_3_freq
       integer :: counter_3_freq
       !goldstein__freq
       integer :: goldstein__freq
       !counter_1_offset
       integer :: counter_1_offset
       !surflux__offset
       integer :: surflux__offset
       namelist /time/ nts1,bfg_increment_genie_clock__freq,counter__freq,weight_check__freq,surflux__freq,embm__freq,biogem__freq,goldstein_seaice__freq,atchem__freq,counter_1_freq,counter_3_freq,goldstein__freq,counter_1_offset,surflux__offset
       ! ****End declaration of Control****
       ! Declare size ref variables (for assumed arrays)
       ! Begin declaration of arguments
       ! Point to Point and Uncoupled Vars
       !plumin
       real :: r8
       !weight_atm
       real, allocatable, dimension(:,:) :: r13
       !weight_ocn
       real, allocatable, dimension(:,:) :: r14
       !atmos_lowestlh_atm
       real, dimension(1:36,1:36) :: r45
       !alon1
       real, dimension(1:36) :: r50
       !alat1
       real, dimension(1:36) :: r51
       !alon2
       real, dimension(1:36) :: r52
       !alat2
       real, dimension(1:36) :: r53
       !alon3
       real, dimension(1:36) :: r54
       !alat3
       real, dimension(1:36) :: r55
       !aboxedge2_lon
       real, dimension(1:37) :: r58
       !aboxedge2_lat
       real, dimension(1:37) :: r59
       !aboxedge3_lon
       real, dimension(1:37) :: r60
       !aboxedge3_lat
       real, dimension(1:37) :: r61
       !ilandmask1
       integer, dimension(1:36,1:36) :: r62
       !ilandmask2
       integer, dimension(1:36,1:36) :: r63
       !ilandmask3
       integer, dimension(1:36,1:36) :: r64
       !atmos_dt_tim
       real :: r78
       !olon1
       real, dimension(1:36) :: r158
       !olat1
       real, dimension(1:36) :: r159
       !olon2
       real, dimension(1:36) :: r160
       !olat2
       real, dimension(1:36) :: r161
       !olon3
       real, dimension(1:36) :: r162
       !olat3
       real, dimension(1:36) :: r163
       !oboxedge2_lon
       real, dimension(1:37) :: r166
       !oboxedge2_lat
       real, dimension(1:37) :: r167
       !oboxedge3_lon
       real, dimension(1:37) :: r168
       !oboxedge3_lat
       real, dimension(1:37) :: r169
       !depth
       real, dimension(1:8) :: r170
       !depth1
       real, dimension(1:9) :: r171
       !ilandmask1
       integer, dimension(1:36,1:36) :: r172
       !ilandmask2
       integer, dimension(1:36,1:36) :: r173
       !ilandmask3
       integer, dimension(1:36,1:36) :: r174
       !jsf_out
       integer :: r185
       !test_energy_ocean
       real :: r232
       !test_water_ocean
       real :: r233
       !ilon1
       real, dimension(1:36) :: r240
       !ilat1
       real, dimension(1:36) :: r241
       !ilon2
       real, dimension(1:36) :: r242
       !ilat2
       real, dimension(1:36) :: r243
       !ilon3
       real, dimension(1:36) :: r244
       !ilat3
       real, dimension(1:36) :: r245
       !iboxedge1_lon
       real, dimension(1:37) :: r246
       !iboxedge1_lat
       real, dimension(1:37) :: r247
       !iboxedge2_lon
       real, dimension(1:37) :: r248
       !iboxedge2_lat
       real, dimension(1:37) :: r249
       !iboxedge3_lon
       real, dimension(1:37) :: r250
       !iboxedge3_lat
       real, dimension(1:37) :: r251
       !ilandmask1
       integer, dimension(1:36,1:36) :: r252
       !ilandmask2
       integer, dimension(1:36,1:36) :: r253
       !ilandmask3
       integer, dimension(1:36,1:36) :: r254
       !test_water_seaice
       real :: r273
       ! Set Notation Vars
       !intrac_atm_max
       integer :: r48
       !go_cd
       real :: r189
       !go_solconst
       real :: r79
       !albedo_ocn
       real, dimension(1:36,1:36) :: r180
       !go_jsf
       integer :: r206
       !precip_atm
       real, dimension(1:36,1:36) :: r42
       !go_cost
       real, dimension(1:36,1:36) :: r237
       !aboxedge1_lon_ocn
       real, dimension(1:37) :: r164
       !go_cv
       real, dimension(0:36) :: r208
       !precip_ocn
       real, dimension(1:36,1:36) :: r35
       !ustar_ocn
       real, dimension(1:36,1:36) :: r178
       !sensible_ocn
       real, dimension(1:36,1:36) :: r31
       !istep_sic
       integer :: r591
       !go_tau
       real, dimension(1:2,1:36,1:36) :: r239
       !go_solfor
       real, dimension(1:36) :: r46
       !go_cpsc
       real :: r199
       !netsolar_atm
       real, dimension(1:36,1:36) :: r39
       !go_dsc
       real :: r195
       !evap_atm
       real, dimension(1:36,1:36) :: r41
       !go_ts
       real, dimension(1:14,1:36,1:36,1:8) :: r211
       !go_ips
       integer, dimension(1:36) :: r192
       !go_s
       real, dimension(0:36) :: r209
       !go_ts1
       real, dimension(1:14,1:36,1:36,1:8) :: r212
       !tstar_ocn
       real, dimension(1:36,1:36) :: r176
       !genie_sfxocn1
       real, dimension(1:49,1:36,1:36) :: r124
       !go_rhoair
       real :: r188
       !go_iaf
       integer, dimension(1:36) :: r205
       !vstar_ocn
       real, dimension(1:36,1:36) :: r179
       !genie_sfcocn1
       real, dimension(1:49,1:36,1:36) :: r123
       !ipf_out
       integer, dimension(1:36) :: r184
       !go_ipf
       integer, dimension(1:36) :: r193
       !go_usc
       real :: r194
       !conductflux_ocn
       real, dimension(1:36,1:36) :: r271
       !go_dphi
       real :: r191
       !aboxedge1_lat_atm
       real, dimension(1:37) :: r57
       !ilat1_ocn
       integer :: r7
       !surf_latent_atm
       real, dimension(1:36,1:36) :: r37
       !frac_sic
       real, dimension(1:36,1:36) :: r257
       !surf_sensible_atm
       real, dimension(1:36,1:36) :: r38
       !ocean_stressx2_ocn
       real, dimension(1:36,1:36) :: r72
       !go_ds
       real, dimension(1:36) :: r190
       !latent_ocn
       real, dimension(1:36,1:36) :: r30
       !hght_sic
       real, dimension(1:36,1:36) :: r256
       !netlong_atm
       real, dimension(1:36,1:36) :: r40
       !go_rh0sc
       real :: r197
       !waterflux_ocn
       real, dimension(1:36,1:36) :: r270
       !runoff_ocn
       real, dimension(1:36,1:36) :: r36
       !genie_sfcatm1
       real, dimension(1:19,1:36,1:36) :: r121
       !go_saln0
       real :: r187
       !go_dz
       real, dimension(1:8) :: r202
       !go_u
       real, dimension(1:3,1:36,1:36,1:8) :: r238
       !genie_sfxsumatm
       real, dimension(1:19,1:36,1:36) :: r275
       !go_dza
       real, dimension(1:8) :: r203
       !ilon1_ocn
       integer :: r6
       !ias_out
       integer, dimension(1:36) :: r181
       !co2_atm
       real, dimension(1:36,1:36) :: r71
       !koverall_total
       integer :: r175
       !genie_clock
       integer*8 :: r1
       !go_sv
       real, dimension(0:36) :: r210
       !genie_sfcatm
       real, dimension(1:19,1:36,1:36) :: r279
       !temp_sic
       real, dimension(1:36,1:36) :: r258
       !istep_atm
       integer :: r297
       !ocean_stressy3_ocn
       real, dimension(1:36,1:36) :: r75
       !go_scf
       real :: r200
       !go_ias
       integer, dimension(1:36) :: r204
       !surf_qstar_atm
       real, dimension(1:36,1:36) :: r77
       !go_fsc
       real :: r196
       !dfrac_sic
       real, dimension(1:36,1:36) :: r44
       !netsolar_ocn
       real, dimension(1:36,1:36) :: r32
       !genie_sfcsed1
       real, dimension(1:42,1:36,1:36) :: r125
       !iaf_out
       integer, dimension(1:36) :: r182
       !test_energy_seaice
       real :: r260
       !go_rhosc
       real :: r198
       !evap_ocn
       real, dimension(1:36,1:36) :: r34
       !istep_ocn
       integer :: r3
       !genie_sfxatm1
       real, dimension(1:19,1:36,1:36) :: r122
       !dhght_sic
       real, dimension(1:36,1:36) :: r43
       !ocean_stressy2_ocn
       real, dimension(1:36,1:36) :: r73
       !tstar_atm
       real, dimension(1:36,1:36) :: r76
       !go_c
       real, dimension(0:36) :: r207
       !sstar_ocn
       real, dimension(1:36,1:36) :: r177
       !genie_sfxsed1
       real, dimension(1:42,1:36,1:36) :: r126
       !albd_sic
       real, dimension(1:36,1:36) :: r259
       !aboxedge1_lon_atm
       real, dimension(1:37) :: r56
       !ilat1_atm
       integer :: r5
       !ilon1_atm
       integer :: r4
       !netlong_ocn
       real, dimension(1:36,1:36) :: r33
       !ocean_stressx3_ocn
       real, dimension(1:36,1:36) :: r74
       !ips_out
       integer, dimension(1:36) :: r183
       !go_fxsw
       real, dimension(1:36,1:36) :: r47
       !aboxedge1_lat_ocn
       real, dimension(1:37) :: r165
       !go_k1
       integer, dimension(1:36,1:36) :: r201
       !ac_bg_timestep
       real :: r127
       !genie_timestep
       real :: r2
       !lrestart_genie
       logical :: r186
       ! p2p between set notation vars
       ! End declaration of arguments
       ! Begin declaration of namelist input
       ! End declaration of namelist input
       call initComms()
       its1=0
       ! Begin control values file read
       open(unit=10,file='BFG2Control.nam')
       read(10,time)
       close(10)
       ! End control values file read
       ! Begin initial values data
       ! Begin P2P notation priming
       ! End P2P notation priming
       ! Begin set notation priming
           r48=19
           r189=1.3e-3
           r591=0
           r195=5e3
           r188=1.25
           r7=36
           r197=1e3
           r6=36
           r175=2500
           r1=0
           r297=0
           r196=2*7.2921e-5
           r198=1e3
           r3=0
           r5=36
           r4=36
           r127=1577880.0
           r2=63115.2
           r186=.false.
       ! End set notation priming
       ! End initial values data
       ! Begin initial values file read
       ! namelist files
       ! netcdf files
       ! End initial values file read
       if (goldsteinThread()) then
       call setActiveModel(1)
       call goldstein_initialise_goldstein_init(r158,r159,r160,r161,r162,r163,r164,r165,r166,r167,r168,r169,r170,r171,r172,r173,r174,r175,r176,r177,r178,r179,r180,r181,r182,r183,r184,r185,r186,r187,r188,r189,r190,r191,r192,r193,r194,r195,r196,r197,r198,r199,r200,r201,r202,r203,r204,r205,r206,r207,r208,r209,r210,r211,r212)
       end if
       if (embmThread()) then
       call setActiveModel(2)
       call embm_initialise_embm_init(r50,r51,r52,r53,r54,r55,r56,r57,r58,r59,r60,r61,r62,r63,r64,r181,r182,r183,r184,r176,r175,r71,r72,r73,r74,r75,r76,r77,r78,r79)
       end if
       if (goldstein_seaiceThread()) then
       call setActiveModel(3)
       call initialise_seaice(r240,r241,r242,r243,r244,r245,r246,r247,r248,r249,r250,r251,r252,r253,r254,r175,r256,r257,r258,r259,r260)
       end if
       if (biogemThread()) then
       call setActiveModel(4)
       call biogem_initialise_biogem_init(r187,r188,r189,r190,r191,r194,r195,r196,r197,r198,r199,r79,r200,r192,r193,r204,r205,r206,r201,r202,r203,r207,r208,r209,r210,r211,r212,r121,r122,r123,r124,r125,r126)
       end if
       if (atcheminst1Thread()) then
       call setActiveModel(5)
       call atchem_initialise_atchem_init(r275,r279)
       end if
       if (atcheminst1Thread()) then
       call setActiveModel(6)
       call atchem_cpl_comp_atmocn_iteration(r48,r4,r5,r6,r7,r279,r121)
       end if
       if (weight_checkThread()) then
       call setActiveModel(7)
       if (.not.(allocated(r13))) then
       allocate(r13(1:r4,1:r5))
       end if
       if (.not.(allocated(r14))) then
       allocate(r14(1:r6,1:r7))
       end if
       call weight_check_weightcheck_init(r4,r5,r6,r7,r8,r57,r56,r165,r164,r13,r14)
       end if
       do its1=1,nts1
       if (bfg_increment_genie_clockThread()) then
       if(mod(its1,bfg_increment_genie_clock__freq).eq.0)then
       call setActiveModel(8)
       call increment_genie_clock(r1,r2)
       end if
       end if
       if (counterinst1Thread()) then
       if(mod(its1,counter_1_freq).eq.counter_1_offset)then
       call setActiveModel(9)
       call counter(r3)
       end if
       end if
       if (surfluxThread()) then
       if(mod(its1,surflux__freq).eq.surflux__offset)then
       call setActiveModel(10)
       call surflux_surflux_iteration(r3,r176,r177,r76,r77,r256,r257,r258,r259,r72,r73,r74,r75,r71,r180,r30,r31,r32,r33,r34,r35,r36,r37,r38,r39,r40,r41,r42,r43,r44,r45,r46,r47,r48,r121)
       end if
       end if
       if (counterinst2Thread()) then
       if(mod(its1,counter__freq).eq.0)then
       call setActiveModel(11)
       call counter(r297)
       end if
       end if
       if (embmThread()) then
       if(mod(its1,embm__freq).eq.0)then
       call setActiveModel(12)
       call embm_embm_iteration(r297,r37,r38,r39,r40,r41,r42,r72,r73,r74,r75,r76,r77,r297)
       end if
       end if
       if (counterinst3Thread()) then
       if(mod(its1,counter_3_freq).eq.0)then
       call setActiveModel(13)
       call counter(r591)
       end if
       end if
       if (goldstein_seaiceThread()) then
       if(mod(its1,goldstein_seaice__freq).eq.0)then
       call setActiveModel(14)
       call gold_seaice(r591,r43,r44,r178,r179,r256,r257,r258,r259,r270,r271,r260,r273,r297)
       end if
       end if
       if (goldsteinThread()) then
       if(mod(its1,goldstein__freq).eq.0)then
       call setActiveModel(15)
       call goldstein_goldstein_iteration(r3,r30,r31,r32,r33,r271,r34,r35,r36,r270,r72,r73,r74,r75,r176,r177,r178,r179,r180,r232,r233,r297,r211,r212,r237,r238,r239)
       end if
       end if
       if (biogemThread()) then
       if(mod(its1,biogem__freq).eq.0)then
       call setActiveModel(16)
       call biogem_biogem_iteration(r127,r1,r211,r212,r256,r257,r237,r46,r47,r238,r239,r121,r122,r123,r124,r125,r126)
       end if
       end if
       if (biogemThread()) then
       if(mod(its1,biogem__freq).eq.0)then
       call setActiveModel(17)
       call biogem_diag_biogem_timeslice_iteration(r127,r1,r121,r122,r124,r125,r126)
       end if
       end if
       if (biogemThread()) then
       if(mod(its1,biogem__freq).eq.0)then
       call setActiveModel(18)
       call biogem_diag_biogem_timeseries_iteration(r127,r1,r121,r122,r124,r125,r126)
       end if
       end if
       if (atcheminst1Thread()) then
       if(mod(its1,atchem__freq).eq.0)then
       call setActiveModel(19)
       call atchem_cpl_flux_ocnatm_iteration(r127,r48,r4,r5,r6,r7,r122,r275)
       end if
       end if
       if (atcheminst1Thread()) then
       if(mod(its1,atchem__freq).eq.0)then
       call setActiveModel(20)
       call atchem_atchem_iteration(r127,r275,r279)
       end if
       end if
       if (atcheminst2Thread()) then
       if(mod(its1,atchem__freq).eq.0)then
       call setActiveModel(21)
       call atchem_cpl_comp_atmocn_iteration(r48,r4,r5,r6,r7,r279,r121)
       end if
       end if
       call commsSync()
       end do
       if (biogemThread()) then
       call setActiveModel(22)
       call biogem_rest_biogem_final()
       end if
       if (atcheminst2Thread()) then
       call setActiveModel(23)
       call atchem_atchem_iteration(r127,r275,r279)
       end if
       if (atcheminst1Thread()) then
       call setActiveModel(24)
       call atchem_rest_atchem_final()
       end if
       if (goldsteinThread()) then
       call setActiveModel(25)
       call goldstein_end_goldstein_final()
       end if
       if (embmThread()) then
       call setActiveModel(26)
       call embm_end_embm_final()
       end if
       if (goldstein_seaiceThread()) then
       call setActiveModel(27)
       call end_seaice()
       end if
       if (biogemThread()) then
       call setActiveModel(28)
       call biogem_end_biogem_final()
       end if
       if (atcheminst1Thread()) then
       call setActiveModel(29)
       call atchem_end_atchem_final()
       end if
       call finaliseComms()
       end program BFG2Main

