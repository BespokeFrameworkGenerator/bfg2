<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="CoupledDocument" select="'coupled.xml'"/>
<xsl:variable name="root" select="document($CoupledDocument)"/>

<xsl:output method="xml" indent="yes"/>

<xsl:template match="/">
 <xsl:apply-templates select="document($root/coupled/composition)/composition"/>
</xsl:template>

<xsl:template match="composition">
 <composition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="/home/armstroc/bfg2/schema/composition.xsd">
  <xsl:apply-templates select="connections"/>
 </composition>
</xsl:template>

<xsl:template match="connections">
 <connections>
  <xsl:apply-templates select="timestepping"/>
 </connections>
</xsl:template>

<xsl:template match="timestepping">
 <timestepping>
  <xsl:apply-templates select="set"/>
 </timestepping>
</xsl:template>

<xsl:template match="set">
 <set name="{@name}">
  <xsl:variable name="modelName" select="field[1]/@modelName"/>
  <xsl:variable name="epName" select="field[1]/@epName"/>
  <xsl:variable name="id" select="field[1]/@id"/>
  <xsl:choose>
   <xsl:when test="primed">
    <xsl:copy-of select="*"/>
   </xsl:when>
   <xsl:otherwise>
   <xsl:variable name="myzero">
    <xsl:choose>
     <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@dataType='integer'">
      <xsl:text>0</xsl:text>
     </xsl:when>
     <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@dataType='real'">
      <xsl:text>0.0</xsl:text>
     </xsl:when>
     <xsl:when test="document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@dataType='logical'">
      <xsl:text>.false.</xsl:text>
     </xsl:when>
     <xsl:otherwise>
      <xsl:text>?ZERO?</xsl:text>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:variable>
    <primed>
     <data dimension="{document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@dimension}"
           datatype="{document($root/coupled/models/model)/definition[name=$modelName]//entryPoint[@name=$epName]/data[@id=$id]/@dataType}" 
           value="{$myzero}"/>
    </primed>
    <xsl:copy-of select="*"/>
   </xsl:otherwise>
  </xsl:choose>
 </set>
</xsl:template>

</xsl:stylesheet>
