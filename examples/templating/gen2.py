from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template2.xml")
content={"code":[
                  {"moduleName":"myModule1",
                   "subroutines":[
                      {"subName":"mysub1",
                       "content":"! hello"},
                      {"subName":"mysub2",
                       "content":"! hello again"}
                    ]
                  },
                  {"moduleName":"myModule2",
                   "subroutines":[
                      {"subName":"mysub3",
                       "content":"! hello yet again"}
                    ]
                  }
                ]
}
result=map(content,template)
print(ET.tostring(result,method="text",pretty_print=True))
