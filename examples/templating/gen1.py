from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template1.xml")
content={
  "moduleName":"myModule",
  "id":1,
  "subName":"mysub",
  "content":ET.XML("<comment>! hello</comment>")   
}
result=map(content,template)
print(ET.tostring(result,method="text",pretty_print=True))
