from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template3.xml")

# we don't need to specify all placeholders, they will just be ignored
# in this case we don't bother specifying <epArgs/>
content={"epName":"check"}
result=map(content,template,snippetName="ep")

# use the result from the ep snippet as content for the condition template
content={"clause":".true.",
         "content":result}
result=map(content,template,snippetName="condition")

# use the result from the condition snippet as content for the modules template
content={"snippet":[
                  {"moduleName":"myModule",
                   "subroutines":[
                      {"subName":"mysub",
                       "content":result}
                    ]
                  }
                ]
}
result=map(content,template,snippetName="modules")
print(ET.tostring(result,method="text",pretty_print=True))
