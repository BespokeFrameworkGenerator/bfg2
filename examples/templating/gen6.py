from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template6.xml")

content={"moduleName":"myModule",
         "subName":"sub1",
         "id":1,
         "calls":[{"subName":"internalSub1"},
                  {"subName":"internalSub2"}]
         }
result=map(content,template,recurse=False)

print(ET.tostring(result,method="text",pretty_print=True))
