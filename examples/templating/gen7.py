from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template7.xml")

content={"moduleName":"myModule"}
result=map(content,template)
# add in some mpi
content={"use":"use mpi",
         "declarations":"integer :: ierr",
         "content":ET.XML("<a>call mpi_init(ierr)</a>")}
result=map(content,result)
# overwrite "use mpi" with "use netcdf"
content={"use":"use netcdf"}
result=map(content,result,overwrite=True)
#append new declaration value to a string
content={"declarations":ET.XML("<b>\nlogical :: test</b>")}
result=map(content,result,overwrite=True)
#replace original ierr string declaration
content={"declarations":"integer :: ierr2"}
result=map(content,result,overwrite=True)
#pre-pend new content without overwrite being set as there is no
#existing string/int value
content={"content":"call hello()\n"}
result=map(content,result)

print(ET.tostring(result,method="text",pretty_print=True))
