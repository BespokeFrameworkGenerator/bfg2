from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template4.xml")

# in the code below we could use template in all map functions
# and ignore result as they are the same. This is not the case
# for templates with snippets where the result is simply the
# snippet but the template includes all snippets
content={"moduleName":"myModule"}
result=map(content,template)
content={"id":1}
result=map(content,result)
content={"subName":"mysub"}
result=map(content,result)
content={"content":ET.XML("<comment>! hello</comment>")}
result=map(content,result)

print(ET.tostring(result,method="text",pretty_print=True))
