from bfgutils import map,xmlread
from lxml import etree as ET

template=xmlread("Template5.xml")

# partially fill an ep template
content={"epArgs":"arg1,arg2"}
result=map(content,template,snippetName="ep")

# create two copies of the ep template (no need to copy the second
# as we are not going to re-use this snippet)
result1=map({"epName":"put"},result,copy=True)
result2=map({"epName":"get"},result)

content={"moduleName":"myModule",
         "subroutines":[
           {"subName":"sub",
            "id":1,
            "content":result1},
           {"subName":"sub",
            "id":2,
            "content":result2}
         ]}

result=map(content,template,snippetName="module")
print(ET.tostring(result,method="text",pretty_print=True))
