module modIN
!
implicit none
private
public :: run
!
contains
!
  subroutine run(complexArg,charArg,logicalArg,intArg,realArg)
  real,intent(in) :: realArg
  integer,intent(in) :: intArg
  logical,intent(in) :: logicalArg
  character,intent(in) :: charArg
  complex,intent(in) :: complexArg

  print *,"modIN:run called"
  print *,"realArg is",realArg
  print *,"intArg is",intArg
  print *,"logicalArg is",logicalArg
  print *,"charArg is",charArg
  print *,"complexArg is",complexArg
  print *,"modIN:run complete"
  end subroutine run
!
end module modIN
