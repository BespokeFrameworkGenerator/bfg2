       module dims
       private 
       public :: ts
       contains
       subroutine ts(dim1,dim2,expl,arg4,impl,assumed3d,arg7,assumed1d,arg9,charArr,str,arg12,arg13)
       implicit none
       integer  , intent(in) :: dim1
       integer  , intent(in) :: dim2
       integer  , intent(out), dimension(4,2:5) :: expl
       double precision  , intent(inout), dimension(2,2:5) :: arg4
       real  , intent(out), dimension(dim1,2:dim2) :: impl
       real  , intent(in), dimension(:,:,:) :: assumed3d
       logical  , intent(inout), dimension(:,:) :: arg7
       complex  , intent(out), dimension(:) :: assumed1d
       character  , intent(out) :: arg9
       character  , intent(inout), dimension(2:4) :: charArr
       character (len=4)  , intent(in) :: str
       character (len=2)  , intent(inout), dimension(2:4) :: arg12
       character (len=2)  , intent(out), dimension(2:4,2:dim2) :: arg13
       ! Declare in-place data
       print *,"Entered module dims subroutine ts"
       ! Perform any gets
       ! Printing input vars
       print *,"On entry in and inout variables have the following values ..."
       print *,"Variable dim1"
       print *,dim1
       print *,"Variable dim2"
       print *,dim2
       print *,"Variable arg4"
       print *,arg4
       print *,"Variable assumed3d"
       print *,assumed3d
       print *,"Variable arg7"
       print *,arg7
       print *,"Variable charArr"
       print *,charArr
       print *,"Variable str"
       print *,str
       print *,"Variable arg12"
       print *,arg12
       ! Initialise out variables to sensible values
       ! Setting expl
       expl=100
       ! Setting impl
       impl=1.0
       ! Setting assumed1d
       assumed1d=(1.0,1.0)
       ! Setting arg9
       arg9='c'
       ! Setting arg13
       arg13="#"
       ! Modify inout variables
       ! Modifying arg4
       arg4=arg4+0.01
       ! Modifying arg7
       arg7=.false.
       ! Modifying charArr
       charArr='m'
       ! Modifying arg12
       arg12="!"
       ! Perform any puts
       ! Printing output vars
       print *,"On exit out and inout variables have the following values ..."
       print *,"Variable expl"
       print *,expl
       print *,"Variable arg4"
       print *,arg4
       print *,"Variable impl"
       print *,impl
       print *,"Variable arg7"
       print *,arg7
       print *,"Variable assumed1d"
       print *,assumed1d
       print *,"Variable arg9"
       print *,arg9
       print *,"Variable charArr"
       print *,charArr
       print *,"Variable arg12"
       print *,arg12
       print *,"Variable arg13"
       print *,arg13
       print *,"Exiting module dims subroutine ts"
       end subroutine ts
       end module dims
