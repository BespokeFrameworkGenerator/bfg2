module simpleArg2
private
public ts
contains

  subroutine ts(r1a)
  real,intent(inout) :: r1a
  print *,"Hello from module simpleArg2 subroutine ts"
  print *,"inout arg r1a on input is ",r1a
  r1a=r1a-1.0
  print *,"inout arg r1a on output is ",r1a
  end subroutine ts

end module simpleArg2
