module modOUT
!
implicit none
private
public :: run
!
contains
!
  subroutine run(realArg,intArg,logicalArg,charArg,complexArg)
  real,intent(out) :: realArg
  integer,intent(out) :: intArg
  logical,intent(out) :: logicalArg
  character,intent(out) :: charArg
  complex,intent(out) :: complexArg

  print *,"modOUT:run called"
  realArg=1.23
  intArg=123
  logicalArg=.true.
  charArg='a'
  complexArg=(1.23,4.56)
  print *,"on exit realArg is",realArg
  print *,"on exit intArg is",intArg
  print *,"on exit logicalArg is",logicalArg
  print *,"on exit charArg is",charArg
  print *,"on exit complexArg is",complexArg
  print *,"modOUT:run complete"
  end subroutine run
!
end module modOUT
