  module BFG2Target

  use PRISM

  implicit none

  interface bfgput
    module procedure put_r2d
  end interface

  interface bfgget
    module procedure get_r2d
  end interface

  private
  public initComms, finaliseComms, commsSync, bfgput, bfgget
  public atmosThread, nemoThread

  integer :: localComm
  integer :: myrank, atmRank, nemoRank
  integer :: ierror
  integer :: valid_shape(2,3)
  integer :: grid_id
  character(len=132) :: var_name,grid_name,point_name
  integer :: grid_type
  integer   :: var_id
  integer :: var_nodims(2)
  integer :: var_type
  Type(PRISM_Time_Struct) :: model_time
  Type(PRISM_Time_Struct) :: model_time_bounds(2)
  Double Precision :: dincr1, dincr2

  contains

  logical function atmosThread()
  if (myrank.eq.atmRank) then
    atmosThread = .true.
  else
    atmosThread = .false.
  end if
  end function atmosThread

  logical function nemoThread()
  if (myrank.eq.nemoRank) then
    nemoThread = .true.
  else
    nemoThread = .false.
  end if
  end function nemoThread

  subroutine commsSync()
  call mpi_barrier(localcomm,ierror)
  end subroutine commsSync

  subroutine initComms()

  character(len=128) :: appl_name = 'atm2nemo'
  character(len=128) :: comp_name
  integer :: comp_id
  integer :: ranklist(1,3) ! assume only one rank per comp
  integer :: method_id
!
  call prism_init(appl_name,ierror)
!
  call prism_get_localcomm (PRISM_Appl_id, localComm, ierror)
!
  call MPI_Comm_Rank ( localComm, myrank, ierror )
!
  call prism_get_ranklists('atm',1,ranklist,ierror)
  atmRank=ranklist(1,1)
  call prism_get_ranklists('nemo',1,ranklist,ierror)
  nemoRank=ranklist(1,1)

  if (myrank.eq.atmRank) then
    comp_name='atm'
  else if (myrank.eq.nemoRank) then
    comp_name='nemo'
  end if

  call prism_init_comp (comp_id, comp_name, ierror )

  call prism_get_localcomm (comp_id, localComm, ierror)
!
  write (*,*) 'I am component ', trim(comp_name),&
 ' in application ', trim(appl_name)

! define the grid
!
!   define a gridless grid for atm
    grid_type = PRISM_Gridless
    grid_name  = 'gridless_grid'
    valid_shape(1,1) = 1
    valid_shape(2,1) = 2
    valid_shape(1,2) = 1
    valid_shape(2,2) = 4
    valid_shape(1,3) = 1
    valid_shape(2,3) = 1

! define the type of grid and its size
    call prism_def_grid ( grid_id, grid_name, comp_id, valid_shape, &
                          grid_type, ierror )
!
    call prism_set_points_Gridless (method_id, point_name, grid_id, &
                         .true., ierror)

    var_name = 'heat_flux'
    var_nodims(1) = 3
    var_nodims(2) = 0
    var_type = PRISM_Real
    call prism_def_var ( var_id,  var_name, grid_id, method_id, &
               PRISM_UNDEFINED, var_nodims, valid_shape, var_type, ierror )

! assert that all definitions are complete
  call prism_enddef(ierror)

! RF not sure about setting valid date ranges so just set once here?
  model_time           = PRISM_jobstart_date
  model_time_bounds(1) = PRISM_jobstart_date
  model_time_bounds(2) = PRISM_jobstart_date

  dincr1 = -3600.0
  dincr2 =  3600.0

  call PRISM_calc_newdate ( model_time_bounds(1), dincr1, ierror )
  call PRISM_calc_newdate ( model_time_bounds(2), dincr2, ierror )

  end subroutine initComms

  subroutine finaliseComms()
    call prism_terminate (ierror)
  end subroutine finaliseComms

  subroutine put_r2d(var,dim1,dim2,id)
    integer, intent(in) :: id
    integer, intent(in) :: dim1,dim2
    real, intent(in) :: var(dim1,dim2)
    real :: hackvar(dim1,dim2,1)
    integer :: i,j,info
    do i=1,dim1
      do j=1,dim2
        hackvar(i,j,1)=var(i,j)
      end do
    end do
    print *,"Hello from put_r2d with id ",id
    print *,"Putting hackvar ",hackvar
    call prism_put ( var_id, model_time, model_time_bounds, hackvar, &
                     info, ierror )

  end subroutine put_r2d

  subroutine get_r2d(var,dim1,dim2,id)
    integer, intent(in) :: id
    integer :: dim1, dim2, info
    real, intent(out) :: var(dim1,dim2)
    real :: hackvar(dim1,dim2,1)
    integer :: i,j
    print *,"Hello from get_r2d with id ",id
    call prism_get ( var_id, model_time, model_time_bounds, hackvar, &
                     info, ierror )
    print *,"Received hackvar ",hackvar
    do i=1,dim1
      do j=1,dim2
        var(i,j)=hackvar(i,j,1)
      end do
    end do

  end subroutine get_r2d

  end module BFG2Target

