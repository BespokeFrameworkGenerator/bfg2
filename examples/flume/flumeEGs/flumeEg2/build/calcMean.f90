! calcMean is an example of a stateless f90 transformation
! calcMean is also an example of a generic transformation
module calcMean
!
implicit none
private
public calcMean_init, calcMean_in, calcMean_out
!
interface calcMean_init
  module procedure calcMean_init_rscalar, calcMean_init_r1d, calcMean_init_r2d
end interface
interface calcMean_in
  module procedure calcMean_in_rscalar, calcMean_in_r1d, calcMean_in_r2d
end interface
interface calcMean_out
  module procedure calcMean_out_rscalar, calcMean_out_r1d, calcMean_out_r2d
end interface
!
contains
!
  subroutine calcMean_init_rscalar(count,sum)
    integer, intent(out) :: count
    real, intent(out) :: sum
    count=0
    sum=0.0
  end subroutine calcMean_init_rscalar
!
  subroutine calcMean_in_rscalar(count,sum,value)
    integer, intent(inout) :: count
    real, intent(inout) :: sum
    real, intent(in) ::  value
    sum=sum+value
    count=count+1
  end subroutine calcMean_in_rscalar
!
  subroutine calcMean_out_rscalar(count,sum,mean)
    integer, intent(inout) :: count
    real, intent(inout) :: sum
    real, intent(out) ::  mean
    mean=sum/count
    call calcMean_init_rscalar(count,sum)
  end subroutine calcMean_out_rscalar
!
!
  subroutine calcMean_init_r1d(count,sum,dim1)
    integer, intent(out) :: count
    integer, intent(in) :: dim1
    real, intent(out) :: sum(dim1)
    count=0
    sum=0.0
  end subroutine calcMean_init_r1d
!
  subroutine calcMean_in_r1d(count,sum,value,dim1)
    integer, intent(inout) :: count
    integer, intent(in) :: dim1
    real, intent(inout) :: sum(dim1)
    real, intent(in) ::  value(dim1)
    sum=sum+value
    count=count+1
  end subroutine calcMean_in_r1d
!
  subroutine calcMean_out_r1d(count,sum,mean,dim1)
    integer, intent(inout) :: count
    integer, intent(in) :: dim1
    real, intent(inout) :: sum(dim1)
    real, intent(out) ::  mean(dim1)
    mean=sum/count
    call calcMean_init_r1d(count,sum,dim1)
  end subroutine calcMean_out_r1d
!
!
  subroutine calcMean_init_r2d(count,sum,dim1,dim2)
    integer, intent(out) :: count
    integer, intent(in) :: dim1,dim2
    real, intent(out) :: sum(dim1,dim2)
    count=0
    sum=0.0
  end subroutine calcMean_init_r2d
!
  subroutine calcMean_in_r2d(count,sum,value,dim1,dim2)
    integer, intent(inout) :: count
    integer, intent(in) :: dim1,dim2
    real, intent(inout) :: sum(dim1,dim2)
    real, intent(in) ::  value(dim1,dim2)
    sum=sum+value
    count=count+1
  end subroutine calcMean_in_r2d
!
  subroutine calcMean_out_r2d(count,sum,mean,dim1,dim2)
    integer, intent(inout) :: count
    integer, intent(in) :: dim1,dim2
    real, intent(inout) :: sum(dim1,dim2)
    real, intent(out) ::  mean(dim1,dim2)
    mean=sum/count
    call calcMean_init_r2d(count,sum,dim1,dim2)
  end subroutine calcMean_out_r2d
!
end module
