      subroutine get(data,id)
      integer id
      real :: data ! should be void
      integer dim1,dim2
        dim1=2
        dim2=4
        call get_r2d_heatflux(data,dim1,dim2,id)
      end subroutine

      subroutine get_r2d_heatflux(data,dim1,dim2,id)
      use BFG2Target, only : bfgget
      integer id,dim1,dim2
      real data(dim1,dim2)
        call bfgget(data,dim1,dim2,id)
      end subroutine
