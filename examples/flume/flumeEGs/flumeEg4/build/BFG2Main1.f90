
  program BFG2Main
    use BFG2Target
    use atmos, only : atmosTimestep
    use calcMean, only : calcMean_init,&
                         calcMean_in,&
                         calcMean_out
    use calcHeatFlux, only : calcHeatFlux_ts

    implicit none

    ! Begin declaration of arguments
    real, allocatable :: solar(:,:)
    real, allocatable :: blue(:,:)
    integer :: count1
    integer :: count2
    real, allocatable :: sum1(:,:)
    real, allocatable :: sum2(:,:)
    integer :: lb1,ub1
    integer :: lb2,ub2
    real, allocatable :: solarmean(:,:)
    real, allocatable :: bluemean(:,:)
    real :: missingDataIndicator
    real :: latentHeatofCond
    integer, allocatable :: landSeaMask(:,:)
    real, allocatable :: longwave(:,:)
    real, allocatable :: sensible(:,:)
    real, allocatable :: evap(:,:)
    real, allocatable :: heatFlux(:,:)
    integer :: myid
    ! End declaration of arguments

    ! Begin declaration of Control
    integer :: i1,i2
    integer :: nts1,nts2
    namelist /time/ nts1,nts2
    ! End declaration of Control

    ! Begin control values file read
    open(unit=10,file='BFG2Control.nam')
    read(10,time)
    close(10)
    ! End control values file read

    ! Begin initial values namelist read
    call nmlread(lb1,ub1,lb2,ub2)
    ! End initial values namelist read

    call initComms()

!   parallel partition 1x4 for 4 pes
    myid=getmyid()
    lb2=myid+1
    ub2=myid+1
    call setpartition(lb1,ub1,lb2,ub2)

    allocate(solar(lb1:ub1,lb2:ub2))
    allocate(blue(lb1:ub1,lb2:ub2))
    allocate(sum1(lb1:ub1,lb2:ub2))
    allocate(sum2(lb1:ub1,lb2:ub2))
    allocate(solarmean(lb1:ub1,lb2:ub2))
    allocate(bluemean(lb1:ub1,lb2:ub2))
    allocate(landSeaMask(lb1:ub1,lb2:ub2))
    allocate(longwave(lb1:ub1,lb2:ub2))
    allocate(sensible(lb1:ub1,lb2:ub2))
    allocate(evap(lb1:ub1,lb2:ub2))
    allocate(heatFlux(lb1:ub1,lb2:ub2))

    ! Begin initial values data read
    landseamask=1
    missingdataindicator=-1
    longwave=1.0
    sensible=1.0
    latentHeatofCond=1.0
    evap=1.0
    ! End initial values data read


    call calcMean_init(count1,sum1,lb1,ub1,lb2,ub2) ! instance 1
    call calcMean_init(count2,sum2,lb1,ub1,lb2,ub2) ! instance 2
    do i1=1,nts1 ! 1 hour timestep
      do i2=1,nts2 ! 20 minute timestep
        call atmosTimestep(solar,blue,lb1,ub1,lb2,ub2)
        call calcMean_in(count1,sum1,solar,lb1,ub1,lb2,ub2) ! instance 1
        call calcMean_in(count2,sum2,blue,lb1,ub1,lb2,ub2) ! instance 2
      end do
      call calcMean_out(count1,sum1,solarmean,lb1,ub1,lb2,ub2) ! instance 1
      call calcMean_out(count2,sum2,bluemean,lb1,ub1,lb2,ub2) ! instance 2
      call calcHeatFlux_ts(solarmean,bluemean,landSeaMask,&
                           missingDataIndicator,longwave,&
                           sensible,latentHeatofCond,evap,&
                           lb2,ub2,lb1,ub1,heatFlux)
      call put(heatFlux,-1)
!        call get(heatFlux,-1)
!        call nemoTimestep(heatFlux,lb1,ub1,lb2,ub2)
      call commsSync()
    end do

    call finaliseComms()

    deallocate(solar)
    deallocate(blue)
    deallocate(sum1)
    deallocate(sum2)
    deallocate(solarmean)
    deallocate(bluemean)
    deallocate(landSeaMask)
    deallocate(longwave)
    deallocate(sensible)
    deallocate(evap)
    deallocate(heatFlux)

  end program BFG2Main

  subroutine nmlread(lb1,ub1,lb2,ub2)
    integer :: lb1,ub1,lb2,ub2
    namelist /data/ lb1,ub1,lb2,ub2
    open(unit=10,file='BFG2Data.nam')
    read(10,data)
    close(10)
  end subroutine nmlread
