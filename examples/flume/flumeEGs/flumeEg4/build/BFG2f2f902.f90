      subroutine get(data,id)
      use BFG2Target, only : getpartition
      integer id
      real :: data ! should be void
      integer lb1,ub1,lb2,ub2
        call getpartition(lb1,ub1,lb2,ub2)
        call get_r2d_heatflux(data,lb1,ub1,lb2,ub2,id)
      end subroutine

      subroutine get_r2d_heatflux(data,lb1,ub1,lb2,ub2,id)
      use BFG2Target, only : bfgget
      integer id
      integer lb1,ub1,lb2,ub2
      real data(lb1:ub1,lb2:ub2)
        call bfgget(data,lb1,ub1,lb2,ub2,id)
      end subroutine
