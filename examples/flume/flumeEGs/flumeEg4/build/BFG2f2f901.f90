      subroutine put(data,id)
      use BFG2Target, only : getpartition
      integer id
      real :: data ! should be void
      integer lb1,ub1,lb2,ub2
 !   parallel partition 1x4 for 4 pes
        call getpartition(lb1,ub1,lb2,ub2)
        call put_r2d_heatflux(data,lb1,ub1,lb2,ub2,id)
      end subroutine

      subroutine put_r2d_heatflux(data,lb1,ub1,lb2,ub2,id)
      use BFG2Target, only : bfgput
      integer id
      integer lb1,ub1,lb2,ub2
      real :: data(lb1:ub1,lb2:ub2)
        call bfgput(data,lb1,ub1,lb2,ub2,id)
      end subroutine

