! dummy nemo ocean model
module nemo
!
private
public nemoTimestep
!
contains
!
  subroutine nemoTimestep(heatFlux,lb1,ub1,lb2,ub2)
    integer, intent(in) :: lb1,ub1
    integer, intent(in) :: lb2,ub2
    real, intent(in) :: heatFlux(lb1:ub1,lb2:ub2)
    print *,"nemo received heatFlux:"
    print *,heatFlux
  end subroutine nemoTimestep
!
end module nemo
