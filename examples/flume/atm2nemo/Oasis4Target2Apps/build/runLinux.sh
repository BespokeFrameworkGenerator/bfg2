#!/bin/sh
#export PSMILE_HOME=/home/cronus/rupert/OASIS4_291105/PRISM_Cpl
#export LD_LIBRARY64_PATH=${LD_LIBRARY64_PATH}:/home/cronus/rupert/libxml2-2.6.16/.libs
#MPIHOME=/local/rupert/install/mpich2
MPIHOME=/local/rupert/install/mpich-1.2.7p1
OASIS4HOME=/local/rupert/install/prism
export PATH=${MPIHOME}/bin:${PATH}
#mpiexec -n 1 ${OASIS4HOME}/Linux/bin/oasis4.MPI1.x : -n 1 atm2nemo1 : -n 1 atm2nemo2
mpirun -p4pg procgroup.txt atm2nemo2


