
  program BFG2Main
    use BFG2Target
    use atmos, only : atmosTimestep
    use calcMean, only : calcMean_init,&
                         calcMean_in,&
                         calcMean_out
    use calcHeatFlux, only : calcHeatFlux_ts

    implicit none

    ! Begin declaration of arguments
    real, allocatable :: solar(:,:)
    real, allocatable :: blue(:,:)
    integer :: count1
    integer :: count2
    real, allocatable :: sum1(:,:)
    real, allocatable :: sum2(:,:)
    integer :: dim1
    integer :: dim2
    real, allocatable :: solarmean(:,:)
    real, allocatable :: bluemean(:,:)
    real :: missingDataIndicator
    real :: latentHeatofCond
    integer, allocatable :: landSeaMask(:,:)
    real, allocatable :: longwave(:,:)
    real, allocatable :: sensible(:,:)
    real, allocatable :: evap(:,:)
    real, allocatable :: heatFlux(:,:)
    ! End declaration of arguments

    ! Begin declaration of Control
    integer :: i1,i2
    integer :: nts1,nts2
    namelist /time/ nts1,nts2
    ! End declaration of Control

    ! Begin control values file read
    open(unit=10,file='BFG2Control.nam')
    read(10,time)
    close(10)
    ! End control values file read

    ! Begin initial values namelist read
    call nmlread(dim1,dim2)
    ! End initial values namelist read

    allocate(solar(dim1,dim2))
    allocate(blue(dim1,dim2))
    allocate(sum1(dim1,dim2))
    allocate(sum2(dim1,dim2))
    allocate(solarmean(dim1,dim2))
    allocate(bluemean(dim1,dim2))
    allocate(landSeaMask(dim1,dim2))
    allocate(longwave(dim1,dim2))
    allocate(sensible(dim1,dim2))
    allocate(evap(dim1,dim2))
    allocate(heatFlux(dim1,dim2))

    ! Begin initial values data read
    landseamask=1
    missingdataindicator=-1
    longwave=1.0
    sensible=1.0
    latentHeatofCond=1.0
    evap=1.0
    ! End initial values data read

    call initComms()

    call calcMean_init(count1,sum1,dim1,dim2) ! instance 1
    call calcMean_init(count2,sum2,dim1,dim2) ! instance 2
    do i1=1,nts1 ! 1 hour timestep
      do i2=1,nts2 ! 20 minute timestep
        call atmosTimestep(solar,blue,dim1,dim2)
        call calcMean_in(count1,sum1,solar,dim1,dim2) ! instance 1
        call calcMean_in(count2,sum2,blue,dim1,dim2) ! instance 2
      end do
      call calcMean_out(count1,sum1,solarmean,dim1,dim2) ! instance 1
      call calcMean_out(count2,sum2,bluemean,dim1,dim2) ! instance 2
      call calcHeatFlux_ts(solarmean,bluemean,landSeaMask,&
                           missingDataIndicator,longwave,&
                           sensible,latentHeatofCond,evap,&
                           dim2,dim1,heatFlux)
      call put(heatFlux,-1)
!        call get(heatFlux,-1)
!        call nemoTimestep(heatFlux,dim1,dim2)
      call commsSync()
    end do

    call finaliseComms()

    deallocate(solar)
    deallocate(blue)
    deallocate(sum1)
    deallocate(sum2)
    deallocate(solarmean)
    deallocate(bluemean)
    deallocate(landSeaMask)
    deallocate(longwave)
    deallocate(sensible)
    deallocate(evap)
    deallocate(heatFlux)

  end program BFG2Main

  subroutine nmlread(dim1,dim2)
    integer :: dim1,dim2
    namelist /data/ dim1,dim2
    open(unit=10,file='BFG2Data.nam')
    read(10,data)
    close(10)
  end subroutine nmlread
