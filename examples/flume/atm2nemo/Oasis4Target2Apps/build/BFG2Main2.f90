
  program BFG2Main
    use BFG2Target

    use nemo, only : nemoTimestep

    implicit none

    ! Begin declaration of arguments
    integer :: dim1
    integer :: dim2
    real, allocatable :: heatFlux(:,:)
    ! End declaration of arguments

    ! Begin declaration of Control
    integer :: i1,i2
    integer :: nts1,nts2
    namelist /time/ nts1,nts2
    ! End declaration of Control

    ! Begin control values file read
    open(unit=10,file='BFG2Control.nam')
    read(10,time)
    close(10)
    ! End control values file read

    ! Begin initial values namelist read
    call nmlread(dim1,dim2)
    ! End initial values namelist read

    allocate(heatFlux(dim1,dim2))

    call initComms()

    do i1=1,nts1 ! 1 hour timestep
      do i2=1,nts2 ! 20 minute timestep
      end do
      call get(heatFlux,-1)
      call nemoTimestep(heatFlux,dim1,dim2)
      call commsSync()
    end do

    call finaliseComms()

    deallocate(heatFlux)

  end program BFG2Main

  subroutine nmlread(dim1,dim2)
    integer :: dim1,dim2
    namelist /data/ dim1,dim2
    open(unit=10,file='BFG2Data.nam')
    read(10,data)
    close(10)
  end subroutine nmlread
