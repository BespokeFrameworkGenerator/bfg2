      subroutine put(data,id)
      integer id
      real :: data ! should be void
      integer dim1,dim2
        dim1=2
        dim2=4
        call put_r2d_heatflux(data,dim1,dim2,id)
      end subroutine

      subroutine put_r2d_heatflux(data,dim1,dim2,id)
      use BFG2Target, only : bfgput
      integer id
      integer dim1,dim2
      real :: data(dim1,dim2)
        call bfgput(data,dim1,dim2,id)
      end subroutine

