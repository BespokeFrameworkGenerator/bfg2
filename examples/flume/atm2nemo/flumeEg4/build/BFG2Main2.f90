
  program BFG2Main
    use BFG2Target

    use nemo, only : nemoTimestep

    implicit none

    ! Begin declaration of arguments
    integer :: lb1,ub1
    integer :: lb2,ub2
    real, allocatable :: heatFlux(:,:)
    integer :: myid
    ! End declaration of arguments

    ! Begin declaration of Control
    integer :: i1,i2
    integer :: nts1,nts2
    namelist /time/ nts1,nts2
    ! End declaration of Control

    ! Begin control values file read
    open(unit=10,file='BFG2Control.nam')
    read(10,time)
    close(10)
    ! End control values file read

    ! Begin initial values namelist read
    call nmlread(lb1,ub1,lb2,ub2)
    ! End initial values namelist read

    call initComms()

!   parallel partition 2x1 for 2 pes
    myid=getmyid()
    lb1=myid+1
    ub1=myid+1
    call setpartition(lb1,ub1,lb2,ub2)

    allocate(heatFlux(lb1:ub1,lb2:ub2))

    do i1=1,nts1 ! 1 hour timestep
      do i2=1,nts2 ! 20 minute timestep
      end do
      call get(heatFlux,-1)
      call nemoTimestep(heatFlux,lb1,ub1,lb2,ub2)
      call commsSync()
    end do

    call finaliseComms()

    deallocate(heatFlux)

  end program BFG2Main

  subroutine nmlread(lb1,ub1,lb2,ub2)
    integer :: lb1,ub1,lb2,ub2
    namelist /data/ lb1,ub1,lb2,ub2
    open(unit=10,file='BFG2Data.nam')
    read(10,data)
    close(10)
  end subroutine nmlread
