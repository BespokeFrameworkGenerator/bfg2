
  module BFG2Target

  use PRISM

  implicit none

  interface bfgput
    module procedure put_r2d
  end interface

  interface bfgget
    module procedure get_r2d
  end interface

  private
  public initComms, finaliseComms, commsSync
  public bfgput,bfgget,getmyid
  public getpartition, setpartition

  integer :: myrank
  integer :: localComm
  integer :: ierror
  integer :: valid_shape(2,3)
  integer :: grid_id
  character(len=132) :: var_name,grid_name,point_name
  integer :: grid_type
  integer   :: var_id
  integer :: var_nodims(2)
  integer :: var_type
  Type(PRISM_Time_Struct) :: model_time
  Type(PRISM_Time_Struct) :: model_time_bounds(2)
  Double Precision :: dincr1, dincr2
  integer :: lb1,ub1,lb2,ub2

  contains

  integer function getmyid()
    getmyid=myrank
  end function getmyid

  subroutine getpartition(mylb1,myub1,mylb2,myub2)
    integer :: mylb1,myub1,mylb2,myub2
    mylb1=lb1
    myub1=ub1
    mylb2=lb2
    myub2=ub2
  end subroutine getpartition

  subroutine setpartition(mylb1,myub1,mylb2,myub2)
    integer :: mylb1,myub1,mylb2,myub2
    lb1=mylb1
    ub1=myub1
    lb2=mylb2
    ub2=myub2
  end subroutine setpartition

  subroutine commsSync()
!  call mpi_barrier(localcomm,ierror)
  end subroutine commsSync

  subroutine initComms()

  character(len=128) :: appl_name = 'nemo'
  character(len=128) :: comp_name = 'nemo'
  integer :: comp_id
  integer :: ranklist(1,3) ! assume only one rank per comp
  integer :: method_id

  call prism_init(appl_name,ierror)

  call prism_init_comp (comp_id, comp_name, ierror )

  call prism_get_localcomm (comp_id, localComm, ierror)

  call MPI_Comm_Rank ( localComm, myrank, ierror )
!
! define the grid
!
!   define a gridless grid for ocn
    grid_type = PRISM_Gridless
    grid_name  = 'gridless_grid'
    valid_shape(1,1) = 1+myrank
    valid_shape(2,1) = 1+myrank
    valid_shape(1,2) = 1
    valid_shape(2,2) = 4
    valid_shape(1,3) = 1
    valid_shape(2,3) = 1

! define the type of grid and its size
    call prism_def_grid ( grid_id, grid_name, comp_id, valid_shape, &
                          grid_type, ierror )
!
    call prism_set_points_Gridless (method_id, point_name, grid_id, &
                         .true., ierror)

    var_name = 'heat_flux'
    var_nodims(1) = 3
    var_nodims(2) = 0
    var_type = PRISM_Real
    call prism_def_var ( var_id,  var_name, grid_id, method_id, &
               PRISM_UNDEFINED, var_nodims, valid_shape, var_type, ierror )

! assert that all definitions are complete
  call prism_enddef(ierror)

! RF not sure about setting valid date ranges so just set once here?
  model_time           = PRISM_jobstart_date
  model_time_bounds(1) = PRISM_jobstart_date
  model_time_bounds(2) = PRISM_jobstart_date

  dincr1 = -3600.0
  dincr2 =  3600.0

  call PRISM_calc_newdate ( model_time_bounds(1), dincr1, ierror )
  call PRISM_calc_newdate ( model_time_bounds(2), dincr2, ierror )

  end subroutine initComms

  subroutine finaliseComms()
    call prism_terminate (ierror)
  end subroutine finaliseComms

  subroutine put_r2d(var,id)
    integer, intent(in) :: id
    real, intent(in) :: var(:,:)
    print *,"Hello from put_r2d with id ",id
!    call prism_put ( var_id, model_time, model_time_bounds, g_wind, &
!                     info, ierror )

  end subroutine put_r2d

  subroutine get_r2d(var,lb1,ub1,lb2,ub2,id)
    integer, intent(in) :: id
    integer :: lb1,ub1,lb2,ub2, info
    real, intent(out) :: var(lb1:ub1,lb2:ub2)
    real :: hackvar(lb1:ub1,lb2:ub2,1)
    integer :: i,j
    print *,"Hello from get_r2d with id ",id
    call prism_get ( var_id, model_time, model_time_bounds, hackvar, &
                     info, ierror )
    print *,"lb1,ub1,lb2,ub2 ",lb1,ub1,lb2,ub2
    print *,"Received hackvar ",hackvar
    do i=lb1,ub1
      do j=lb2,ub2
        var(i,j)=hackvar(i,j,1)
      end do
    end do

  end subroutine get_r2d

  end module BFG2Target

