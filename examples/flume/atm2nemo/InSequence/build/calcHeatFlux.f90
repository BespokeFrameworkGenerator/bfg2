! calcHeatFlux is an example of a stateless f90 transformation
module calcHeatFlux
!
private
public calcHeatFlux_ts
!
contains
!
subroutine calcHeatFlux_ts(solar,blue,landSeaMask,missingDataIndicator,longwave, &
                              sensible,latentHeatofCond,evap,jrowstp,rowLength,heatFlux)
!
  integer, intent(in) :: jrowstp,rowLength
  real, intent(in) :: missingDataIndicator, latentHeatofCond
  real, intent(in) :: solar(rowLength,jrowstp),blue(rowLength,jrowstp)
  integer, intent(in) :: landSeaMask(rowLength,jrowstp)
  real, intent(in) :: longwave(rowLength,jrowstp),sensible(rowLength,jrowstp)
  real, intent(in) :: evap(rowLength,jrowstp)
  real, intent(out) :: heatFlux(rowLength,jrowstp)
! local vars
  integer :: i,j

  ! Calculate surface heat flux at sea-points
  do j=1,jrowstp
     do i=1,rowLength
        if (landSeaMask(i,j)==1) Then
           heatFlux(i,j)=missingDataIndicator
        else
           heatFlux(i,j)=solar(i,j) - blue(i,j) + longwave(i,j) &
           - (sensible(i,j) + latentHeatOfCond * evap(i,j))
        endif
     end do
  end do
  return
  end subroutine calcHeatFlux_ts
!
end module
