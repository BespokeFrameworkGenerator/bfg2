
  module BFG2Target
    use atmos, only : atmosTimestep
    use nemo, only : nemoTimestep
    use calcMean, only : calcMean_init,&
                         calcMean_in,&
                         calcMean_out
    use calcHeatFlux, only : calcHeatFlux_ts


  contains
  logical function atmosThread()
    ! only one thread so always true
  atmosThread=.true.
  end function atmosThread
  logical function nemoThread()
    ! only one thread so always true
  nemoThread=.true.
  end function nemoThread
  logical function calcMeanThread()
    ! only one thread so always true
  calcMeanThread=.true.
  end function calcMeanThread
  logical function calcHeatFluxThread()
    ! only one thread so always true
  calcHeatFluxThread=.true.
  end function calcHeatFluxThread
  subroutine commsSync()
    ! running in sequence so no sync is required
  end subroutine commsSync

  subroutine initComms()
    ! nothing needed for sequential
  end subroutine initComms
  subroutine finaliseComms()
    ! nothing needed for sequential
  end subroutine finaliseComms

  end module BFG2Target

