! dummy UM atmos model
module atmos
!
private
public atmosTimestep
!
contains
!
  subroutine atmosTimestep(solar,blue,dim1,dim2)
    integer, intent(in) :: dim1
    integer, intent(in) :: dim2
    real, intent(out) :: solar(dim1,dim2)
    real, intent(out) ::  blue(dim1,dim2)
    solar=1.0
    blue=2.0
  end subroutine atmosTimestep
!
end module atmos
