Subroutine inita2nemo( &
#include <argd1/argd1.h>
#include <argsts/argsts.h>
#include <argduma/argduma.h>
#include <argdumo/argdumo.h>
#include <argptra/argptra.h>
#include <argptro/argptro.h>
#include <argaocpl/argaocpl.h>
     icode, &
     cmessage)

  Implicit none

!
! Description:
!
! Method:
!
! External Documentation:
!
! Current Code Owner : Marco Christoforou
!
!
! History:
! Version   Date     Comment
! -------   ----     -------
!
!
!
! Code Description:
!   Language: FORTRAN 90 + common extensions.
!   This code is written to TODO programming standards.
!
! System component covered:
! System Task:
!
! Declarations:
!
! Global variables (*CALLed COMDECKs etc...):
#include <csubmodl/csubmodl.h>
#include <cntlatm/cntlatm.h>
#include <parparm/parparm.h>
#include <typsize/typsize.h>
#include <typd1/typd1.h>
#include <typsts/typsts.h>
#include <typduma/typduma.h>
#include <typdumo/typdumo.h>
#include <typptra/typptra.h>
#include <typptro/typptro.h>
#include <typaocpl/typaocpl.h>
!
  Integer       icode       ! OUT - Error return code
  Character*(*) cmessage    ! OUT - Error return message
!*----------------------------------------------------------------------
!  Common blocks
!
#include <caoptr/caoptr.h>
#include <c_mdi/c_mdi.h>
#include <stparam/stparam.h>
#include <cntlocn/cntlocn.h>
!
!  Subroutines called
!
  External finptr
!
!  Local variables
!
  Integer :: process_code   ! Processing code
  Integer :: freq_code      ! Frequency code
  Integer :: start,end,period ! Start, end and period step
  Integer :: gridpt_code,weight_code ! Gridpt and weighting codes
  Integer :: bottom_level,top_level ! Bottom and top input level
  Integer :: grid_n,grid_s,grid_w,grid_e ! Grid corner definitions
  Integer :: stashmacro_tag ! STASHmacro tag number
  Integer :: im_ident       ! Internal model identifier
  Integer :: im_index       ! Internal model index for STASH arrays

  !-------------------------------------------------------------------
  ! 0.  Set grid definition information (undefined as search is on
  !     STASHmacro tag number)

  process_code=imdi
  freq_code   =imdi
  start       =imdi
  end         =imdi
  period      =imdi
  gridpt_code =imdi
  weight_code =imdi
  bottom_level=imdi
  top_level   =imdi
  grid_n      =imdi
  grid_s      =imdi
  grid_e      =imdi
  grid_w      =imdi

  ! Set up internal model identifier and STASH index
  ! I'm assuming I need these lines even these are not used in this file.
  im_ident = atmos_im
  im_index = internal_model_index(im_ident)

  !----------------------------------------------------------------------
  ! 1.  Get address for each field from its STASH section/item code
  !     and STASHmacro tag if a diagnostic, or from its primary pointer
  !     if prognostic or ancillary field
  !     Atmosphere -> Ocean (tag=10)
  !
  stashmacro_tag=11

  ! Marco Christoforou
  ! Code below structure in such a way as to avoid use of goto 999, as
  ! employed in other code.

  !L 1.7 Net integrated downward solar on atmos grid
  Call findptr( &
       atmos_im, &
       1,203, &
       process_code, &
       freq_code, &
       start, &
       end, &
       period, &
       gridpt_code, &
       weight_code, &
       bottom_level, &
       top_level, &
       grid_n, &
       grid_s, &
       grid_w, &
       grid_e, &
       stashmacro_tag, &
       imdi, &
       ja_solar, &
#include <argsts/argsts.h>
       icode, &
       cmessage)

  If (ja_solar==0) Then
     icode=1203
     cmessage="inita2nemo: Coupling field not enabled - Net solar"
  Endif

  If (icode==0) Then
!L 1.8 Net downward blueband solar on atmos grid
     If (l_ctile) Then
        Call findptr( &
             atmos_im, &
             1,260, &
             process_code, &
             freq_code, &
             start, &
             end, &
             period, &
             gridpt_code, &
             weight_code, &
             bottom_level, &
             top_level, &
             grid_n, &
             grid_s, &
             grid_w, &
             grid_e, &
             stashmacro_tag, &
             imdi, &
             ja_blue, &
#include <argsts/argsts.h>
             icode, &
             cmessage)
     Else
        Call findptr( &
             atmos_im, &
             1,204, &
             process_code, &
             freq_code, &
             start, &
             end, &
             period, &
             gridpt_code, &
             weight_code, &
             bottom_level, &
             top_level, &
             grid_n, &
             grid_s, &
             grid_w, &
             grid_e, &
             stashmacro_tag, &
             imdi, &
             ja_blue, &
#include <argsts/argsts.h>
             icode, &
             cmessage)
     Endif
     If (ja_blue==0) Then
        icode=1204
        cmessage= &
             "inita2nemo: Coupling field not enabled - Blue solar atm"
     Endif
  Endif

  If (icode==0) Then
     ! L 1.10 Surface evaporation over sea weighted by fractional leads
     Call findptr( &
          atmos_im, &
          3,232, &
          process_code, &
          freq_code, &
          start, &
          end, &
          period, &
          gridpt_code, &
          weight_code, &
          bottom_level, &
          top_level, &
          grid_n, &
          grid_s, &
          grid_w, &
          grid_e, &
          stashmacro_tag, &
          imdi, &
          ja_evap, &
#include <argsts/argsts.h>
          icode, &
          cmessage)
     If (ja_evap==0) Then
        icode=3232
        cmessage= &
             "inita2nemo: Coupling field not enabled - Evap over sea"
     Endif
  Endif

  If (icode==0) Then
     ! L 1.11 Net downward longwave on atmos grid
     Call findptr( &
          atmos_im, &
          2,203, &
          process_code, &
          freq_code, &
          start, &
          end, &
          period, &
          gridpt_code, &
          weight_code, &
          bottom_level, &
          top_level, &
          grid_n, &
          grid_s, &
          grid_w, &
          grid_e, &
          stashmacro_tag, &
          imdi, &
          ja_longwave, &
#include <argsts/argsts.h>
          icode, &
          cmessage)
     If (ja_longwave==0) Then
        icode=2203
        cmessage="inita2nemo: Coupling field not enabled - Longwave"
     Endif
  Endif

  If (icode==0) then
     ! L 1.12 Sensible heat on atmos grid, area mean over open sea
     Call findptr( &
          atmos_im, &
          3,228, &
          process_code, &
          freq_code, &
          start, &
          end, &
          period, &
          gridpt_code, &
          weight_code, &
          bottom_level, &
          top_level, &
          grid_n, &
          grid_s, &
          grid_w, &
          grid_e, &
          stashmacro_tag, &
          imdi, &
          ja_sensible, &
#include <argsts/argsts.h>
          icode, &
          cmessage)
     If (ja_sensible==0) Then
        icode=3228
        cmessage= &
             "inita2nemo: Coupling field not enabled - Sensible heat"
     Endif
  Endif

End Subroutine inita2nemo

! TODO What #defines may be needed here.
! *****************************COPYRIGHT*******************************
! (c) CROWN COPYRIGHT 2000, Met Office, All Rights Reserved.
! Please refer to file $UMDIR/vn$VN/copyright.txt for further details
! *****************************COPYRIGHT*******************************
!+ Replace atmos model data with ocean model data in coupled model.
!
! Subroutine Interface:
Subroutine puta2nemo( &
     g_p_field, &
#include <argd1/argd1.h>
     icode, &
     cmessage)

  Implicit None

!
! Description:
!
! Method:
!
! External Documentation:
!
! Current Code Owner : Marco Christoforou
!
!
! History:
! Version   Date     Comment
! -------   ----     -------
!           21/12/05 Initial compilable version.
!
!
! Code Description:
!   Language: FORTRAN 90 + common extensions.
!   This code is written to TODO programming standards.
!
! System component covered:
! System Task:
!
! Declarations:
!
! Global variables (*CALLed COMDECKs etc...):
#include <parvars/parvars.h>   ! Includes an include that defines halo_type
#include <decomptp/decomptp.h> ! Conatins definition of decomp_standard_atm
#include <decompdb/decompdb.h> ! decomp_db_glsize
#include <atm_lsm/atm_lsm.h>   ! Contains definitions for atmos_landmask_lo

#include <cmaxsize/cmaxsize.h> !
#include <typsize/typsize.h>   !
#include <typd1/typd1.h>       ! d1?
#include <typptra/typptra.h>   ! Contains jfrac_land but TODO don't know wh
#include <typcona/typcona.h>   !
#include <typlndm/typlndm.h>   !

#include <caoptr/caoptr.h>     ! Contains ja_solar etc. set in inita2nemo.
#include <c_lheat/c_lheat.h>   ! Contains code that set LC (Latent Heat Con
!#include <fldtype/fldtype.h>  ! Contains definition of fld_type_p but
!is included by another include, so not needed here.
#include <c_mdi/c_mdi.h>       ! Contains defination and setting of RMDI
#include <cntlatm/cntlatm.h>   ! l_ctile
#include <cntlocn/cntlocn.h>   !

!     Subroutine arguments:
  Integer, Intent(in)        :: g_p_field ! Global horiz domain for atm
  Integer, Intent(out)       :: icode     ! Error return code (=0 is OK
  Character*(*), Intent(out) :: cmessage  ! Error return message

!     Local dynamic arrays:
!     Coupling fields on atmosphere grid being sent into NEMO ocean model
!     One dimensional versions of array for setting from d1 array.
  Real :: solar(g_p_field)  ! Net solar shortwave heat flux from atmosp
  Real :: blue(g_p_field)   ! Net downward shortwave flux in 'blue' fre
  Real :: evap(g_p_field)   ! Surface evaporation over sea from water
  Real :: longwave(g_p_field) ! Net downward longwave heat flux
  Real :: sensible(g_p_field) ! Sensible heat flux (+ve upward)
  Logical :: amasktp(g_p_field) ! Atmos model land-sea mask for TP
!     Two dimensional versons that we will dynamically allocate
!     NOTE: In the atmos to ocean code on which this is all based, swapa2o
!     into transa2o where they suddenly becomes 2d array. I understand this
!     in f77. I'm going to try to do it properly.
  Real,dimension(:,:),allocatable :: solar2d
  Real,dimension(:,:),allocatable :: blue2d
  Real,dimension(:,:),allocatable :: evap2d
  Real,dimension(:,:),allocatable :: longwave2d
  Real,dimension(:,:),allocatable :: sensible2d
  Real,dimension(:,:),allocatable :: workAtmos
  Logical,dimension(:,:),allocatable :: amasktp2d ! Atmos model lamd-se

!#if defined(TRANGRID)
  Real    :: fland_loc(g_p_field)
  Real    :: fland_glo(g_p_field)
  Integer :: nlandpt
!#endif

  Integer,dimension(:,:),allocatable :: landSeaMask ! Land-sea mask on
  
  Real :: missingDataIndicator=rmdi
  Real :: latentHeatOfCond=lc
  
  Integer :: i              ! Loop counter
  Integer :: j              ! Loop counter
  Integer :: jrowstp
  Integer :: rowLength
  Integer :: info           ! Return code from MPP
  Integer :: gather_pe      ! Processor for gathering

!----------------------------------------------------------------------
!     Main Body starts here

!     Set the arrays from the d1 array. Note that ja_solar etc. are defined
  Do i=1,g_p_field
     solar(i)=d1(ja_solar+i-1)
     blue(i)=d1(ja_blue+i-1)
     evap(i)=d1(ja_evap+i-1)
     longwave(i)=d1(ja_longwave+i-1)
     sensible(i)=d1(ja_sensible+i-1)
  Enddo

  rowLength=decomp_db_glsize(1,fld_type_p,decomp_standard_atmos) !G_R
  jrowstp  =decomp_db_glsize(2,fld_type_p,decomp_standard_atmos) !G_P

!       Now that we know what the dimensions are going to be allocate the 2
  allocate(solar2d(rowLength,jrowstp))
  allocate(blue2d(rowLength,jrowstp))
  allocate(evap2d(rowLength,jrowstp))
  allocate(longwave2d(rowLength,jrowstp))
  allocate(sensible2d(rowLength,jrowstp))
  allocate(amasktp2d(rowLength,jrowstp))

!       Now reshape from 1d to 2d array.
  solar2d   =Reshape(solar,(/rowLength,jrowstp/))
  blue2d    =Reshape(blue,(/rowLength,jrowstp/))
  evap2d    =Reshape(evap,(/rowLength,jrowstp/))
  longwave2d=Reshape(longwave,(/rowLength,jrowstp/))
  sensible2d=Reshape(sensible,(/rowLength,jrowstp/))
  amasktp2d =Reshape(amasktp,(/rowLength,jrowstp/))

!       Construct a land-sea mask on the atmosphere grid.
! LAND MASK IS NOT PROPERLY DEALT WITH IN THIS USE CASE
  allocate(landSeaMask(rowLength,jrowstp))
  Do j=1,jrowstp
     Do i=1,rowLength
!            If (amasktp2d(i,j)) Then
!              landSeaMask(i,j)=1
!            Else
        landSeaMask(i,j)=0
!            Endif
     Enddo
   Enddo
        
!!       TODO combine loop above with loop below. Both are not needed.

!!       SECTION 6: NON-PENETRATIVE SURFACE HEAT FLUXES.
!!
!!       NOTICE THAT THE BLUE END OF THE SOLAR SPECTRUM HAS TO BE
!!       SUBTRACTED OUT HERE. IT SHOULD ALSO BE POINTED OUT THAT AT
!!       SEA-ICE POINTS (IF THEY EXIST), THE SENSIBLE HEAT FLUX AND
!!       THE EVAPORATION WERE ALREADY WEIGHTED BY THE FRACTIONAL AREA
!!       OF LEADS WHEN THEY WERE DIAGNOSED, SO NO SPECIAL CODE IS
!!       NECESSARY HERE.
!!
   allocate(workAtmos(rowLength,jrowstp))
   Do j=1,jrowstp
      Do i=1,rowLength
         If (landSeaMask(i,j)==1) Then
            workAtmos(i,j)=missingDataIndicator
         Else
            workAtmos(i,j)=solar2d(i,j)-blue2d(i,j)+longwave2d(i,j) &
                 -(sensible2d(i,j)+latentHeatOfCond*evap2d(i,j))
         Endif
      Enddo
   Enddo

!!       PUT HEAT FLUX 2 NEMO HERE

   ! THIS ROUTINE HAS NOT YET SET UP A WAY FOR GETTING heatFlux_id, date
   ! or date_bounds
   call PRISM_put(heatFlux_id, date, date_bounds, workAtmos, info, ierror)

!       Deallocate all the arrays to prevent memory leaks.
   deallocate(solar2d)
   deallocate(blue2d)
   deallocate(evap2d)
   deallocate(longwave2d)
   deallocate(sensible2d)
   deallocate(amasktp2d)
   deallocate(landSeaMask)
   deallocate(workAtmos)
!
!      Endif                     !icode/=0

 End Subroutine puta2nemo
*/-----
*DECLARE U_MODEL1
*/-----
*I ACB2F505.424
      write(6,*) 'MARCO 0'

*D U_MODEL1.122
      IF (LTIMER) CALL TIMER('ATM_STEP',4)
*I GDR2F502.235
      write(6,*) 'MARCO 1'

*I GDR2F502.258
      write(6,*) 'MARCO 2'
*I GRR2F305.651
      Call puta2nemo(
     &               g_theta_field_size,
#include <artd1/artd1.h>
     &               icode,
     &               cmessage)


*I KILLDATE.11393
C Marco Christoforou (START) 20/12/2005
C Put call to puta2nemo here as we are within the defined(ATMOS)
C but outside the defined(OCEAN)
      Write(6,*) 'MARCO: About to call to puta2nemo'
      If (ltimer) Call timer('puta2nemo',3)
      Call puta2nemo(
     &               g_theta_field_size,
#include <artd1/artd1.h>
     &               icode,
     &               cmessage)
      If (ltimer) Call timer('puta2nemo',4)
C Marco Christoforou (END)
*/-----
*DECLARE INITIAL1
*/-----
*I ACB2F505.289
! Marco Christoforou (START) 20/12/2005
! Put call to puta2nemo here as we are within the defined(ATMOS)
! but outside the defined(OCEAN)
      Write(6,*) 'MARCO: About to call inita2nemo'
      If (ltimer) Call timer('inita2nemo',3)
      Call inita2nemo(
#include <artd1/artd1.h>
#include <artsts/artsts.h>
#include <artduma/artduma.h>
#include <artdumo/artdumo.h>
#include <artptra/artptra.h>
#include <artptro/artptro.h>
#include <artaocpl/artaocpl.h>
     &                icode,
     &                cmessage)
      If (icode/=0) Then
        Write(6,*) 'Failure in call to inita2nemo'
        Call Ereport(RoutineName,icode,cmessage)
      Endif
      If (ltimer) Call timer('inita2nemo',4)
! Marco Christoforou (END)
*/-----
*DECLARE TRANA2O1
*/-----
*I TRANA2O1.296

       write(6,*) 'MARCO: Hello from TRANA2O'
*I CCD0F503.398
            if (i==1) then
              write(6,*) 'MARCO-TRANA2O:WORKA:',i,j,':',WORKA(I,J)
              write(6,*) 'MARCO: solar:       ',i,j,':',solarin(i,j)
              write(6,*) 'MARCO: blue:        ',i,j,':',bluein(i,j)
              write(6,*) 'MARCO: longwav      ',i,j,':',longwave(i,j)
              write(6,*) 'MARCO: sensible:    ',i,j,':',sensible(i,j)
              write(6,*) 'MARCO: evap:        ',i,j,':',evap(i,j)
            endif
