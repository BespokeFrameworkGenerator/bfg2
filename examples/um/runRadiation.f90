program runRadiation
!
use sizes
use radiation_mod, only : radiation
implicit none
integer :: i
real :: t_inc(row_length,rows,model_levels)=0.0
!
  do i=1,12
    call radiation(t_inc,i,3,2)
    print *,"timestep =",i," t_inc = ",t_inc
  end do
!
end program runRadiation
