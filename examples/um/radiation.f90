module radiation_mod
!
use sizes
use shortwave_mod, only : shortwave
use longwave_mod,  only : longwave
implicit none
private
public :: radiation
!
real :: sw_incs(row_length,rows,0:model_levels+1)=0.0
real :: lw_incs(row_length,rows,0:model_levels)=0.0
real :: delta_t(row_length,rows,model_levels)=0.0
real :: cos_zenith_angle(row_length,rows)=1.0
!
contains
!
  subroutine radiation(t_inc,timestep,sw_step,lw_step)
    ! called "glue_rad" in the UM
    ! combines shortwave and longwave temperature increments

    real,intent(inout) :: t_inc(row_length,rows,model_levels)
    integer,intent(in) :: timestep,sw_step,lw_step
    integer :: k

    if (mod(timestep,sw_step)==0) then
      call solang(cos_zenith_angle,timestep)
      ! Add in day frac as well?
      call shortwave(sw_incs)
      do k=1,model_levels
        delta_t(:,:,k) = sw_incs(:,:,k) * cos_zenith_angle(:,:)
      end do
    end if
    t_inc=t_inc+delta_t

    if (mod(timestep,lw_step)==0) then
      call longwave(lw_incs)
    end if
    do k=1,model_levels
      t_inc(:,:,k)=t_inc(:,:,k)+lw_incs(:,:,k)
    end do

  end subroutine radiation
!
end module radiation_mod
