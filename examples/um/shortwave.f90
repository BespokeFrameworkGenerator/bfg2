module shortwave_mod
use sizes
implicit none
private
public :: shortwave
real :: my_sw_incs(row_length,rows,0:model_levels+1)=0.0
contains
  ! called "r2_swrad*" in the UM
  ! computes a temperature increment
  subroutine shortwave(sw_incs)
    real,intent(out) :: sw_incs(row_length,rows,0:model_levels+1)
    my_sw_incs=my_sw_incs-1.0
    sw_incs=my_sw_incs
    print *,"shortwave called, sw_incs =",sw_incs(1,1,1)
  end subroutine shortwave

end module
