module longwave_mod
use sizes
implicit none
private
public :: longwave
real :: my_lw_incs(row_length,rows,0:model_levels)=0.0
contains
  ! called "r2_lwrad*" in the UM
  ! computes a temperature increment
  subroutine longwave(lw_incs)
    real,intent(out) :: lw_incs(row_length,rows,0:model_levels)
    my_lw_incs=my_lw_incs+1.0
    lw_incs=my_lw_incs
    print *,"longwave called, lw_incs =",lw_incs(1,1,1)
  end subroutine longwave

end module
