import os
import hashlib



def stat(filename):

    size = os.path.getsize(filename)
    hash = hashlib.md5(open(filename, 'r').read()).hexdigest()

    return (size, hash)



class TypeDesc(object):

    def __init__(self, attr):
        self.attr = attr

    def __get__(self, inst, instType):

        if inst is None:
            return instType.__name__

        return inst.__class__.__name__



class Container(object):

    propstr = "property(lambda self: list(sorted(self.{0}.keys())), lambda self, key, val: self.{0}[key].__setitem__(val) )"

    def __init__(self):

        for c in [t.container for t in self.contains]:
            setattr(self, c, {})
            setattr(self.__class__, c.lstrip("_"), eval(self.propstr.format(c)) )
#            print("Added in {0}... {1}".format(self.__class__.__name__, self.propstr.format(c)))

        self.containers = [ getattr(self, t.container) for t in self.contains ]



    def showAll(self):
        all = {}

        for t in self.contains:
            all.update(getattr(self, t.container))

        return all



    def containedTypes(self):

        containers = {}

        for cName in [t.container.lstrip('_') for t in self.contains]:
            container = getattr(self, cName)
            if container:
                containers[cName] = container

        return containers



    def retrieve(self, entry):

        for c in self.containers:
            if entry in c:
                return c[entry]



    def report(self):

        for c in self.containers:
            for i in c.values():
                yield i


    @property
    def entries(self):

        return [ c[e] for c in self.containers for e in c ]




class RealTagDesc(object):


    def __init__(self, attr):
        self.attr = attr

    def __get__(self, inst, instType):
        
        root = inst.xml.getroot()
        tag = root.tag

        subtypes = list(root.iter(tag='type'))
        tag += '/' + subtypes[0].text

        return RealTagDesc.Characteristics[tag]



