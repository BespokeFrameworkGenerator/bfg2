from lxml import etree as et

from couplingtypes import *


class Browser(object):

    def __init__(self, base):
        self.base = base
        self.hier = Hierarchy(base)
        self.populate()


    def __str__(self):
         return str(self.coupling)


    def populate(self):
        walker = os.walk(self.base)

        for dir in walker:
            for file in filter(lambda x: x.lower().endswith(".xml"), dir[2]):
                absFile = os.path.join(dir[0], file)
                self.hier.append(absFile)


    def __getattr__(self, attr):
        return getattr(self.hier, attr)

#    def asXml(self, item):
#        return self.hier.asXml(item)
#
#
#    def report(self):
#        return self.hier.report()
#
#    def retrieve(self, entry):
#        return self.hier.retrieve(entry)
#
#
#    @property
#    def entries(self):
#        return self.hier.entries
#
#    @property
#    def models(self):
#        return self.hier.models


    @property
    def name(self):
        return os.path.basename(self.base.rstrip(os.sep))


    def containedTypes(self):
        containers = {}
        for cName in [ t.container.lstrip("_") for t in Types ]:
            c = getattr(self.hier, cName)
            containers[cName] = c

        return containers


if __name__ == "__main__":

    libs = [ "GeminiE3AndGENIEem", "isenes" ]

    b = Browser("GeminiE3AndGENIEem")
#    b = Browser("isenes")

    print("\n")
#    print(b.asXml("genie_em_definition.xml"))

    for i, entry in enumerate(b.report()):
        print("[{0:2}] {1}".format(i+1, entry))

    print(b.models)
    print(b.entries)
    print("[containedTypes()]")
    print(b.containedTypes())

    print("[Cross-lib Models...]")
    allModels = {}

    for lib in libs:
        libModels = []
        b = Browser(lib)
        for m in [ b.retrieve(mName) for mName in b.models]:
            libModels.append(m)

        allModels[lib] = libModels

    for l, mods in allModels.items():
        print("{0} =>".format(l))
        for m in mods:
            print("....{0}".format(m.name))


    print(Model.type)
    m = Model("GeminiE3AndGENIEem/models/gemini_e3/gemini_e3_definition_runs.xml")
    print(m.type)
