# python main.py 0.9 0.1 2010
#
# Input:  1 -> temperature rise target in 2050
#         2 -> precision of the resulting increase temperature for the convergence loop
#         3 -> starting date for the climate polica

from math import log
import array
import csv
import struct
import sys
import os.path
import numpy
from numpy import matrix, mat, zeros, sqrt
import random

# Reading input arguments
target = float(sys.argv[1])      # Temperature target
precision = float(sys.argv[2])   # Precision for the convergence loop
fixed_year = int(sys.argv[3])    # Starting date for the algorithm


Temperature_increase = 1000  
nb_fixed_year = fixed_year - 2001

############# INITIALIZATION ####################
#   
# 1. Running Gemini-E3 without emissions constraints
#
# Created files: Gemini_CO2_results.txt
#                Gemini_NO2_results.txt
#                Gemini_CH4_results.txt
#
#################################################

# os.system("gams .\Variante_BAU.gms")



# Reading from files initial emissions profiles of GEMINI-E3 
  
CO2_base = zeros(nb_fixed_year)
N2O_base = zeros(nb_fixed_year)
CH4_base = zeros(nb_fixed_year)

reader1 = csv.reader(open("Gemini_CO2_results.txt","rb"))
reader2 = csv.reader(open("Gemini_NO2_results.txt","rb"))
reader3 = csv.reader(open("Gemini_CH4_results.txt","rb"))
list_reader1 = []
list_reader2 = []
list_reader3 = []
for row in reader1:
  list_reader1.append(row[0])
for row in reader2:
  list_reader2.append(row[0])
for row in reader3:
  list_reader3.append(row[0])
for i in xrange(nb_fixed_year):
  CO2_base[i] = float(list_reader1[i])
  N2O_base[i] = float(list_reader2[i])
  CH4_base[i] = float(list_reader3[i])

# Max emissions for 2050 from BAU
CO2_2050_max = float(list_reader1[49])
N2O_2050_max = float(list_reader2[49])
CH4_2050_max = float(list_reader3[49])


# Min possible emissions in 2050
CO2_2050_min = 0
N2O_2050_min = 0
CH4_2050_min = 0

# Setting current emissions in 2050 to max
CO2_2050_current = CO2_2050_max / 2
N2O_2050_current = N2O_2050_max / 2
CH4_2050_current = CH4_2050_max / 2


#################################################
#
# Main convergence loop
#
#################################################
while (abs(Temperature_increase - target) > precision):

  #################################################################
  # Preparing input files for the climate module of GEMINI-E3 
  c1 = open("CO2sum_data.gms", "wb")
  c2 = open("N2Osum_data.gms", "wb")
  c3 = open("CH4sum_data.gms", "wb")
  c1.write("parameter CO2sum(T)/\n")
  c2.write("parameter N20sum(T)/\n")
  c3.write("parameter CH4sum(T)/\n")
  for i in xrange(50):
    if i < nb_fixed_year - 1:
      c1.write(str(2001+i) + "    " + str(CO2_base[i]) + "\n")
      c2.write(str(2001+i) + "    " + str(N2O_base[i]) + "\n")
      c3.write(str(2001+i) + "    " + str(CH4_base[i]) + "\n")
    else:
      c1.write(str(2001+i) + "    " + str(CO2_base[nb_fixed_year-1] + (CO2_2050_current - CO2_base[nb_fixed_year-1])/49*i) + "\n")
      c2.write(str(2001+i) + "    " + str(N2O_base[nb_fixed_year-1] + (N2O_2050_current - N2O_base[nb_fixed_year-1])/49*i) + "\n")
      c3.write(str(2001+i) + "    " + str(CH4_base[nb_fixed_year-1] + (CH4_2050_current - CH4_base[nb_fixed_year-1])/49*i) + "\n")
  c1.write("/;")
  c2.write("/;")
  c3.write("/;")
  c1.close()
  c2.close()
  c3.close()


  #################################################
  #
  # 2. Running Climate module of Gemini-E3 
  #
  # Used files: CO2sum_data.gms, N2Osum_data.gms and CH4sum_data.gms
  #
  # Created files: concent-gemini.txt          
  #
  ################################################## 
  os.system("gams .\Climate_module\Climate_module2.1.gms")



  
  #################################################################
  # Preparing input files for GENIE
  file = "concent-gemini.txt"
  reader = csv.reader(open(file,"rb"))  
  list_reader = []
  for row in reader:
    list_reader.append(row[0])

  n = len(list_reader)
  concent_co2 = zeros(n)
  time = zeros(n)
  for i in xrange(n):
    line = list_reader[i].split(';')
    time[i] = line[0]
    concent_co2[i] = line[1]

  for i in xrange(n):
    time[i] = -1 + (time[i] - 2005)/50 

  poly1 = zeros(n)
  poly2 = zeros(n)
  poly3 = zeros(n)
  a11 = 0
  a22 = 0
  a33 = 0
  a12 = 0
  a13 = 0
  a23 = 0
  b = zeros(3)
  for i in xrange(n):
    poly1[i] = time[i] + 1
    poly2[i] = 2 * time[i]**2 - 2
    poly3[i] = 4 * time[i]**3 - 4 * time[i]
    a11 = a11 + poly1[i]**2
    a22 = a22 + poly2[i]**2
    a33 = a33 + poly3[i]**2
    a12 = a12 + poly1[i]*poly2[i]
    a13 = a13 + poly1[i]*poly3[i]
    a23 = a23 + poly2[i]*poly3[i]
    b[0] = b[0] + poly1[i] * (concent_co2[i] - concent_co2[0])
    b[1] = b[1] + poly2[i] * (concent_co2[i] - concent_co2[0])
    b[2] = b[2] + poly3[i] * (concent_co2[i] - concent_co2[0])

  e11 = (poly1[n-1]**2)/2
  e21 = (poly2[n-1]*poly1[n-1])/2
  e31 = (poly3[n-1]*poly1[n-1])/2

  if e21 == 0:
    e21 = 1e-6
  if e31 == 0:
    e31 = 1e-6

  e12 = poly1[n-1]*a12 - poly2[n-1]*a11
  e13 = poly1[n-1]*a13 - poly3[n-1]*a11
  f1 = poly1[n-1]*b[0] - (concent_co2[n-1] - concent_co2[0]) * a11

  e22 = poly1[n-1]*a22 - poly2[n-1]*a12
  e23 = poly1[n-1]*a23- poly3[n-1]*a12
  f2 = poly1[n-1]*b[1] - (concent_co2[n-1] - concent_co2[0]) * a12

  e32 = poly1[n-1]*a23 - poly2[n-1]*a13
  e33 = poly1[n-1]*a33- poly3[n-1]*a13
  f3 =  poly1[n-1]*b[2] - (concent_co2[n-1] - concent_co2[0]) * a13
  
  c11 = e12*e21 - e11*e22
  c12 = e13*e21 - e11*e23
  c21 = e12*e31 - e11*e32
  c22 = e13*e31 - e33*e11
  d1 = f1*e21 - f2*e11
  d2 = f1*e31 - f3*e11

  tcheb = zeros(3)
  tcheb[2] = (d1*c21 - d2*c11)/(c12*c21 - c11*c22)
  tcheb[1] = (d2 - c22 * tcheb[2]) / c21 
  delta = (f1 - e12 * tcheb[1] - e13 * tcheb[2]) / e11  
  tcheb[0] = ((concent_co2[n-1] - concent_co2[0]) - tcheb[1] * poly2[n-1] - tcheb[2] * poly3[n-1]) / poly1[n-1]

  tcheb[0] = 2*tcheb[0]
  tcheb[1] = 2*tcheb[1]
  tcheb[2] = 2*tcheb[2]


  # Writing chebyshev coefficients into the file ".\coeffs.txt"
  c = open( "Emul_Temperature_2050/coeffs.txt", "wb")
  c.write("TC1,TC2,TC3 \n")
  c.write(str(tcheb[0]) + "," + str(tcheb[1]) + "," + str(tcheb[2])+ "\n\n")
  c.close()

  #################################################
  #
  # 3. Running GENIE Emulator
  #
  # Used files: coeffs.txt
  #
  # Created files: outfile_genie2.txt          
  #
  ##################################################
  os.system("R --no-restore --no-save  <Emul_Temperature_2050/emul.R > Emul_Temperature_2050/out.txt")


  # Retrieving tempetaure increase from the emulator
  reader = csv.reader(open("Emul_Temperature_2050/outfile_genie2.txt","rb"))		  
  list_reader = []
  for row in reader:
    list_reader.append(row[0])

  line = list_reader[0].split(' ')
  Temperature_increase  = float(line[1])
  print Temperature_increase  


  # Updating the target emissions (min, max and current) of CO2, N2O and CH4 in 2050
  if abs(Temperature_increase - target) > precision:
    if Temperature_increase > target:
      CO2_2050_max = CO2_2050_current
      N2O_2050_max = N2O_2050_current
      CH4_2050_max = CH4_2050_current
    else:
      CO2_2050_min = CO2_2050_current
      N2O_2050_min = N2O_2050_current
      CH4_2050_min = CH4_2050_current
    CO2_2050_current = CO2_2050_min + 0.5 * (CO2_2050_max - CO2_2050_min)
    N2O_2050_current = N2O_2050_min + 0.5 * (N2O_2050_max - N2O_2050_min)
    CH4_2050_current = CH4_2050_min + 0.5 * (CH4_2050_max - CH4_2050_min)
    

############### FINAL CALL ######################
#
# 4. Running Gemini-E3 with emissions constraints
#
#  Used files: CO2sum_data.gms, N2Osum_data.gms and CH4sum_data.gms
#
#################################################

# os.system("gams .\Variante.gms")

os.system("del CO2sum_data.gms")
os.system("del CH4sum_data.gms")
os.system("del N2Osum_data.gms")
os.system("del concent-gemini.txt")
os.system("del Climate_module2.1.lst")

