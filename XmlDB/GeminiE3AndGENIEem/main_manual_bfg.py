#! /usr/bin/env python
import os
import sys

# hardcode initial conditions for the moment
target = 0.9      # Temperature rise target in 2050
precision = 0.001   # Precision for the convergence loop
fixed_year = 2010    # Starting date for the algorithm
Temperature_increase = 1000  # initialise to a large number so no convergence
base_year=2001
nb_fixed_year = fixed_year - base_year  # number of years to compute

# import our models and transformations
from gemini_e3_model import gemini_e3_model
from genie_em_model import genie_em_model
from convergence_test import convergence_test
from emissions_control import emissions_control

# Create model and transformation instances

os.chdir(os.path.dirname(sys.modules["gemini_e3_model"].__file__))
gemini_e3_instance=gemini_e3_model(nb_fixed_year,base_year)

os.chdir(os.path.dirname(sys.modules["genie_em_model"].__file__))
genie_em_instance=genie_em_model()

os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
converge_instance=convergence_test(target,precision)

os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
emissions_instance=emissions_control(nb_fixed_year,target,precision)

# initial convergence check
os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
converged=converge_instance.converged(Temperature_increase)

# Main convergence loop (must be a single variable)
while (not(converged)):

  # get current emissions
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  CO2,N2O,CH4=emissions_instance.get_emissions()

  # run gemini with current emissions
  os.chdir(os.path.dirname(sys.modules["gemini_e3_model"].__file__))
  time,concent_co2=gemini_e3_instance.run(CO2,N2O,CH4)

  # run genie with gemini CO2 output
  os.chdir(os.path.dirname(sys.modules["genie_em_model"].__file__))
  Temperature_increase=genie_em_instance.run(time,concent_co2)

  # update emissions 2050 target
  os.chdir(os.path.dirname(sys.modules["emissions_control"].__file__))
  emissions_instance.update_2050_emissions(Temperature_increase)

  # convergence check
  os.chdir(os.path.dirname(sys.modules["convergence_test"].__file__))
  converged=converge_instance.converged(Temperature_increase)

print "Run Complete"
print "Target="+str(target)
print "Temperature_increase="+str(Temperature_increase)
print "precision="+str(precision)

exit(0)
