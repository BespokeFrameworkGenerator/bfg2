from numpy import zeros
import csv
class emissions_control:

    def __init__(self,nb_fixed_year,target,precision):

        self.nb_fixed_year=nb_fixed_year
        self.target=target
        self.precision=precision

        # Reading from files initial emissions profiles of GEMINI-E3 

        self.CO2_base = zeros(self.nb_fixed_year)
        self.N2O_base = zeros(self.nb_fixed_year)
        self.CH4_base = zeros(self.nb_fixed_year)

        reader1 = csv.reader(open("data/Gemini_CO2_results.txt","rb"))
        reader2 = csv.reader(open("data/Gemini_NO2_results.txt","rb"))
        reader3 = csv.reader(open("data/Gemini_CH4_results.txt","rb"))

        list_reader1 = []
        list_reader2 = []
        list_reader3 = []
        for row in reader1:
          list_reader1.append(row[0])
        for row in reader2:
          list_reader2.append(row[0])
        for row in reader3:
          list_reader3.append(row[0])
        for i in xrange(self.nb_fixed_year):
          self.CO2_base[i] = float(list_reader1[i])
          self.N2O_base[i] = float(list_reader2[i])
          self.CH4_base[i] = float(list_reader3[i])

        # Max emissions for 2050 from BAU
        self.CO2_2050_max = float(list_reader1[49])
        self.N2O_2050_max = float(list_reader2[49])
        self.CH4_2050_max = float(list_reader3[49])

        # Min possible emissions in 2050
        self.CO2_2050_min = 0
        self.N2O_2050_min = 0
        self.CH4_2050_min = 0

        # Setting current emissions in 2050 to max
        self.CO2_2050_current = self.CO2_2050_max / 2
        self.N2O_2050_current = self.N2O_2050_max / 2
        self.CH4_2050_current = self.CH4_2050_max / 2

    def get_emissions(self):
        CO2=zeros(50)
        N2O=zeros(50)
        CH4=zeros(50)
        for i in xrange(50):
            if i < self.nb_fixed_year - 1:
                CO2[i]=self.CO2_base[i]
                N2O[i]=self.N2O_base[i]
                CH4[i]=self.CH4_base[i]
            else:
                CO2[i]=self.CO2_base[self.nb_fixed_year-1] + (self.CO2_2050_current - self.CO2_base[self.nb_fixed_year-1])/49*i
                N2O[i]=self.N2O_base[self.nb_fixed_year-1] + (self.N2O_2050_current - self.N2O_base[self.nb_fixed_year-1])/49*i
                CH4[i]=self.CH4_base[self.nb_fixed_year-1] + (self.CH4_2050_current - self.CH4_base[self.nb_fixed_year-1])/49*i
        return CO2,N2O,CH4

    def update_2050_emissions(self,Temperature_increase):

        if abs(Temperature_increase - self.target) > self.precision:
            if Temperature_increase > self.target:
                self.CO2_2050_max = self.CO2_2050_current
                self.N2O_2050_max = self.N2O_2050_current
                self.CH4_2050_max = self.CH4_2050_current
            else:
                self.CO2_2050_min = self.CO2_2050_current
                self.N2O_2050_min = self.N2O_2050_current
                self.CH4_2050_min = self.CH4_2050_current
            self.CO2_2050_current = self.CO2_2050_min + 0.5 * (self.CO2_2050_max - self.CO2_2050_min)
            self.N2O_2050_current = self.N2O_2050_min + 0.5 * (self.N2O_2050_max - self.N2O_2050_min)
            self.CH4_2050_current = self.CH4_2050_min + 0.5 * (self.CH4_2050_max - self.CH4_2050_min)
