import os
from lxml import etree as et
from utils import stat, TypeDesc, Container, RealTagDesc




class BfgEntry(object):

    contains = []
    type = TypeDesc("type")


    def __init__(self, file="", xml=None):
        self.file = file
        self.size, self.hash = stat(file)
        if not xml:
            self.parse()
        else:
            self._xmlroot = xml

    @property
    def name(self):
        return os.path.basename(self.file)

    def __str__(self):
        return "[{0}] {1} => {2} ({3} bytes hash:{4})".format(self.type, self.name, self.file, self.size, self.hash)

    def parse(self):
        self._xmlroot = et.parse(self.file)

    @property
    def xml(self):
        return self._xmlroot

    @property
    def dir(self):
        return os.path.dirname(self.file) 

    @property
    def tag(self):
        return self._xmlroot.getroot().tag




class Model(BfgEntry):

    tag = "definition/scientific"
    subtag = "scientific"
    container = "_models"

    def __init__(self, file="", xml=None):
        super(self.__class__, self).__init__(file, xml=None)



class Composition(BfgEntry):

    tag = "composition"
    container = "_compositions"

    def __init__(self, file="", xml=None):
        super(self.__class__, self).__init__(file, xml=None)



class Deployment(BfgEntry):

    tag = "deployment"
    container = "_deployments"

    def __init__(self, file="", xml=None):
        super(self.__class__, self).__init__(file, xml=None)



class Transformation(BfgEntry):

    tag = "definition/transformation"
    subtag = "transformation"
    container = "_transformations"

    def __init__(self, file="", xml=None):
        super(self.__class__, self).__init__(file, xml=None)



class Coupling(BfgEntry, Container):

    tag = "coupled"
    container = "_couplings"
    contains = [ Model, Composition, Deployment, Transformation ]

    def __init__(self, file="", xml=None):

        Container.__init__(self)
        BfgEntry.__init__(self, file, xml=None)


    def __str__(self):

        return str(super(self.__class__, self).__str__())



    def parse(self):
        self._xmlroot = et.parse(self.file)

        for t in self.contains:
            for e in self._xmlroot.xpath("//"+t.__name__.lower()):
                file = os.path.join(self.dir, e.text)
                if(hasattr(t, "subtag")):
                    print("{0} has subtype {1}".format(t.type, t.subtag))
                    base = BfgEntry(file)
                    print(".... ", base)
                    t = base.subtype
                    inst = t(file, xml=base.xml)
                    print("........ Subtyped to {0}".format(inst.type))
                else:
                    inst = t(file)


                getattr(self, t.container)[inst.name] = inst


#        print("[self.showAll() - {0}]\n{1}".format(self.name, self.showAll()))
        print("[self.containedTypes() - {0}]\n{1}".format(self.name, self.containedTypes()))


Types = (Coupling, Model, Composition, Deployment, Transformation)
setattr(RealTagDesc, "Characteristics", dict( [ (t.tag, t) for t in Types ] ))
setattr(BfgEntry, "subtype", RealTagDesc("subtype"))




class Hierarchy(Container):

# { "Element.tag" : [ Type : "container" ] }
#    EntryTypes = {"definition/scientific" : (Model, "_models"),
#                  "coupled" : (Coupling, "_couplings"),
#                  "composition" : (Composition, "_compositions"),
#                  "definition/transformation" : (Transformation, "_transformations"),
#                  "deployment" : (Deployment, "_deployments") }


    contains = Types
    EntryTypes = dict([ (t.tag, (t, t.container)) for t in Types])


    def __init__(self, base):
        self.base = base
        Container.__init__(self)


    def append(self, file):

        xmlroot = et.parse(file)
        root = xmlroot.getroot()
        tag = root.tag
  
        subtypes = list(root.iter(tag='type'))
        if subtypes:
            tag += '/' + subtypes[0].text
        
        fileOb = Hierarchy.EntryTypes[tag][0](file, xml=xmlroot)  # Prevent redundant parsing
        getattr(self, Hierarchy.EntryTypes[tag][1])[fileOb.name] = fileOb
        


    def asXml(self, item):

        for c in self.containers:
            if item in c:
                return(et.tostring(c[item].xml, pretty_print=True))

        return("")




